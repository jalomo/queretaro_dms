const Devolucion = {
    template:  
        `<div class="container-fluid panel-body">
            <h1>Ventas Realizadas</h1>
            <h2>Seleccionar venta para devolucion</h2>
            <div>
                <label>Folio:</label>
                <input class="form-control" v-model="folio"/>
            </div>
            <div>
                <label>Seleccionar cliente:</label>
                <select class="form-control" v-model="cliente">
                    <option value="nombre">Nombre</option>
                </select>
            </div>
        </div>`,
    data(){
        return {
            cliente: [],
            folio: null,
            message: "Hola mundo desde data"
        }
    }
    
}