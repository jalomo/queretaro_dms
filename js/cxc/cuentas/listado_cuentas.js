function inicializaTabla() {
    var tbl_cxc = $('#tbl_cxc').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        footerCallback: function(row, data, start, end, display) {
            var api = this.api(),
                data;
            var intVal = function(i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
            };
            // Total over all pages
            total_saldo_neto = api
                .column(6)
                .data()
                .reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            $("#total_saldo_neto_tabla").html('$' + total_saldo_neto.toFixed(2));
            total_pago = api
                .column(7)
                .data()
                .reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            $("#total_pagar_tabla").html('$' + total_pago.toFixed(2));

            total_abonado = api
                .column(8)
                .data()
                .reduce(function(a, b) {
                    console.log(a);
                    return intVal(a) + intVal(b);
                }, 0);
            $("#total_abonado_tabla").html('$' + total_abonado.toFixed(2));
        },
        "ajax": {
            url: PATH_API + "api/cuentas-por-cobrar",
            type: 'GET',
        },
        columns: [{
                title: "Folio",
                data: 'folio',
            },
            {
                title: "Concepto",
                data: 'concepto',
            },
            {
                title: "Cliente",
                render: function(data, type, row) {
                    let nombre_cliente = row.nombre_cliente + ' ' + row.apellido_paterno + ' ' + row.apellido_materno;
                    let cliente = nombre_cliente.length > 2 ? nombre_cliente : row.nombre_empresa;
                    return row.numero_cliente + ' - ' + cliente;
                }
            },
            {
                title: "Plazo credito",
                data: 'plazo_credito',
            },
            {
                title: "Estatus",
                data: 'estatus_cuenta',
                render: function(data, type, row) {
                    return row.estatus_cuenta;
                }
            },
            {
                title: "Fecha",
                render: function(data, type, row) {
                    return obtenerFechaMostrar(row.fecha)
                }
            },
            {
                title: "Saldo neto",
                data: 'importe',
            },
            {
                title: "Monto a pagar",
                data: 'total',
            },
            {
                title: "Monto pagado",
                data: 'saldo_acomulado',
            },
            {
                title: '-',
                render: function(data, type, row) {
                    return '<button title="Detalle venta" onclick="detallePago(this)" data-id="' + row.id + '" class="btn btn-default"><i class="fas fa-list"></i></button>';

                }
            }
        ],
        "createdRow": function(row, data, dataIndex) {
            switch (data['estatus_cuenta_id']) {
                case 2:
                    $(row).find('td:eq(4)').css('background-color', '#8cdd8c');
                    break;
                case 3:
                    $(row).find('td:eq(4)').css('background-color', '#f6ffa4');
                    break;
                case 4:
                    $(row).find('td:eq(4)').css('background-color', '#e37f7f');
                    break;
                default:
                    break;
            }
        }
    });
}

function detallePago(_this) {
    setTimeout(() => {
        var cuenta_por_cobrar = $(_this).data('id');
        window.location.href = PATH + '/cxc/index/detalle_pago/' + cuenta_por_cobrar;
    }, 200);
}

function obtenerFechaMostrar(fecha) {
    const dia = 2,
        mes = 1,
        anio = 0;
    fecha = fecha.split('T');
    fecha = fecha[0].split('-');
    return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
}

function filtrar() {
    var params = $.param({
        'folio': $("#folio").val(),
        'cliente_id': $("#cliente_id option:selected").val(),
        'estatus_cuenta_id': $("#estatus_cuenta_id option:selected").val(),

    });

    $('#tbl_cxc').DataTable().ajax.url(PATH_API + 'api/cuentas-por-cobrar?' + params).load()
}

function limpiarfiltro() {
    $("#folio").val('');
    $("#cliente_id").val('');
    $("#estatus_cuenta_id").val('');
    $("#cliente_id").trigger('change');
    var params = $.param({
        'folio': '',
        'cliente_id': '',
        'estatus_cuenta_id': ''
    });

    $('#tbl_cxc').DataTable().ajax.url(PATH_API + 'api/cuentas-por-cobrar?' + params).load()
}

this.inicializaTabla();
$("#cliente_id").select2();