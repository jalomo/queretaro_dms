$(document).ready(function() {
    //Recuperamos la medida de la pantalla
    // var alto = $( window ).height();
    // var ancho = $( window ).width();
    var alto = screen.height;
    var ancho = screen.width;

    //Verificamos el tamaño de la pantalla
    if (ancho < 501) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.20;
        var nvo_ancho = ancho * 0.80;
        console.log("Movil");
    } else if ((ancho > 499) && (ancho < 1030)) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.22;
        var nvo_ancho = ancho * 0.50;
        console.log("Tablet");
    } else {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.22;
        var nvo_ancho = ancho * 0.33;
    }

    // Asignamos los valores al recuadro de la firma
    $("#canvas").prop("width", nvo_ancho.toFixed(2));
    $("#canvas").prop("height", "200");
    //////////////////////////////////////////////


    var canvas = document.getElementById("canvas_auto");
    var context = canvas.getContext("2d");

    var img = new Image();
    img.onload = function() {
        context.drawImage(img, 0, 0, 450, 250);
    };
    img.src = PATH_BASE + 'img/inventario/car_2.jpg'

    //Lienzo para firma general
    var signaturePad = new SignaturePad(document.getElementById('canvas'));

    var singCanvasAuto = new SignaturePad(document.getElementById('canvas_auto'));

    $("#btnSign").on('click', function() {
        //Recuperamos la ruta de la imagen
        var data = signaturePad.toDataURL('image/png');
        //Comprobamos a donde se enviara
        var destino = $('#firma_actual').val();
        var ruta = "";
        // Comprobamos el tipo de usuario que firmara
        if (destino == "firma_diagnostico") {
            $("#firma_elabora_diagnostico_img").attr("src", data);
            $('#firma_elabora').val(data);

            // ruta = guardar_firma(data,"1");

        } else {
            $("#firma_consumidor_img").attr("src", data);
            $('#firma_consumidor').val(data);
            // ruta = guardar_firma(data,"2");
        }

        signaturePad.clear();
    });

    $('#limpiar').on('click', function() {
        signaturePad.clear();
    });

    $('#cerrarCuadroFirma').on('click', function() {
        signaturePad.clear();
    });

    $("#btnCanvasImg").on('click', function() {
        //Recuperamos la ruta de la imagen
        var data = singCanvasAuto.toDataURL('image/png');
        $('#carroceria_img').val(data);
    });
});

$(".cuadroFirma").on('click', function() {
    var firma = $(this).data("value");
    $('#firma_actual').val(firma);
    // Comprobamos el tipo de usuario que firmara
    if (firma == "firma_diagnostico") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("Firma Diagnóstico.");
    } else {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("Firma del Consumidor.");
    }
});