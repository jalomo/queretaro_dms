// filtro_estatus_id
var tabla_piezas = $('#tabla_piezas').DataTable({
    language: {
        url: PATH_LANGUAGE
    },
    "ajax": {
        url: PATH_API + "api/pedidoproducto/listado",
        type: 'GET',
        data: {
            proveedor_id: () => $('#filtro_proveedor_id').val(),
            ma_pedido_id: () => $('#ma_pedido_id').val()
        }
    },
    columns: [{
            title: "#",
            data: 'id',
        },
        {
            title: "No. de pieza",
            data: 'no_identificacion',
        },
        {
            title: "Descripcion",
            data: 'descripcion',
        },
        {
            title: "Precio unitario",
            data: 'valor_unitario',
        },
        {
            title: "Unidad",
            data: 'unidad',
        },
        {
            title: "Proveedor",
            data: ({ proveedor_nombre }) => proveedor_nombre ? proveedor_nombre : 'sin asignar'
        },
        {
            title: "Estatus",
            data: function({ estatus_id, cantidad_backorder }) {
                // console.log(data)
                if (estatus_id == 1) {
                    return 'En proceso'
                } else if (estatus_id == 2) {
                    return 'Pedidas'
                } else if (estatus_id == 3) {
                    return 'No pedidas'
                } else if (estatus_id == 4) {
                    return 'Backorder: ' + cantidad_backorder
                } else if (estatus_id == 9) {
                    return 'Pedido Sugerido'

                }

            }
        },
        {
            title: "Piezas a pedir",
            data: 'cantidad_solicitada',
        },
        {
            title: "Acciones",
            data: ({ id, estatus_id, producto_id, proveedor_id, cantidad_solicitada }) => {
                let buttons = '';
                // if (estatus_id == 9) {
                    buttons += `<button class='btn btn-primary mx-1' onclick="updateProductoPedido(${id},${producto_id},${proveedor_id},${cantidad_solicitada})" data-id="${id}">
                        <i class='fas fa-edit'></i>
                    </button>`;
                // }
                // console.log(proveedor_id);
                buttons += `<button class='btn btn-danger mx-1' onclick="borrar(${id})">
                    <i class='fas fa-trash'></i>
                    </button>`

                return buttons;


            }
        }
    ],
    "createdRow": function(row, data, dataIndex) {
        if (data.estatus_id == 2) {
            $(row).find('td:eq(6)').css('background-color', '#8cdd8c');
        } else if (data.estatus_id == 4) {
            $(row).find('td:eq(6)').css('background-color', '#db524d80');
        } else if (data.estatus_id == 9) { //pedido sugerido
            $(row).find('td:eq(6)').css('background-color', '#ffa500ba');
        }

        if (!data.proveedor_nombre) {
            $(row).find('td:eq(5)').css('background-color', '#db524d80');
        }
    }
});

var tabla_stock = $('#tabla_stock').DataTable({
    language: {
        url: PATH_LANGUAGE
    },
    processing: true,
            serverSide: true,
            bFilter: false,
    ajax: {
        url: PATH_API + "api/productos/listadoStock",
        type: 'GET',
    },
    columns: [{
            title: "#",
            data: 'id',
        },
        {
            title: "No. de pieza",
            data: 'no_identificacion',
        },
        {
            title: "Descripcion",
            data: 'descripcion',
        },
        {
            title: "Precio unitario",
            data: 'valor_unitario',
        },
        {
            title: "Unidad",
            data: 'unidad',
        },
        {
            title: "Cantidad actual",
            data: 'cantidad_actual',
        },
        {
            data: function(data) {
                return "<button class='btn btn-primary btn-modal' data-id=" + data.id + "><i class='fas fa-list'></i></a>";
            }
        }

    ]
});

const updateProductoPedido = (id, producto_id, proveedor_id,cantidad_solicitada) => {
    $("#modaldetalle").modal('show');
    $("#estatus_id").val(1);
    $("#producto_pedido_id").val(id);

    ajax.get(`api/productos/stockActual?producto_id=${producto_id}`, {}, function(response, headers) {
        if (headers.status == 201 || headers.status == 200) {
            let data = response.aaData[0];
            $("#producto_id").val(producto_id);
            $("#descripcion").val(data.descripcion);
            $("#no_identificacion").val(data.no_identificacion);
            $("#valor_unitario").val(data.valor_unitario);
            $("#cantidad_solicitada").val(cantidad_solicitada);
            $("#proveedor_id").val(proveedor_id);
        }
    })
}

$('#tabla_stock').on('click', '.btn-modal', function() {
    let id = $(this).data('id');
    $("#modaldetalle").modal('show');
    ajax.get(`api/productos/stockActual?producto_id=${id}`, {}, function(response, headers) {
        
        if (headers.status == 201 || headers.status == 200) {
            let data = response.aaData[0];
            $("#producto_id").val(id);
            $("#descripcion").val(data.descripcion);
            $("#no_identificacion").val(data.no_identificacion);
            $("#valor_unitario").val(data.valor_unitario);
            
        }
    })
});


const borrar = (id) => {
    utils.displayWarningDialog("Deseas borrar el producto? ", "error", function(data) {
        if (data.value) {
            ajax.put('api/pedidoproducto/' + id, {
                estatus_id: 10
            }, (data, headers) => {
                if (headers.status == 204 || headers.status == 200) {
                    return tabla_piezas.ajax.reload();
                }
            })

        }
    })
}

$('#tabla_piezas').on('click', '.modal-estatus', function() {
    let id = $(this).data('id');
    $("#modalcambioestatus").modal('show');
    $("#id_pedido_producto").val(id);
});

$('#tabla_piezas').on('click', '.modal-proveedor', function() {
    let id = $(this).data('id');
    $("#frmmodalproveedor")[0].reset();
    $("#modalproveedor").modal('show');
    $("#selected_pedidoproducto_id").val(id);
    ajax.get(`api/proveedorpedidoproducto/getById/${id}`, {}, function(response, headers) {
        if (headers.status == 201 || headers.status == 200) {
            let lista = "<ul class='list-group'>"
            response.map(item => {
                lista += "<li class='list-group-item'><span style='color:black'>Proveedor :</span> " + item.proveedor_nombre + " <br><span style='color:black'>cantidad:</span> " + item.cantidad_proveedor + "</li>"
            })
            lista += "</ul>"
            $("#contendor_proveedores").html(lista);
        }
    })
});

$('#btn-filtrar').on('click', function() {
    tabla_piezas.ajax.reload();
});


$('#btn-confirmar').on('click', function() {

    if ($("#cantidad_solicitada").val() == '' || $("#cantidad_solicitada").val() < 1) {
        toastr.error("Indicar la cantidad!")
        return false;
    }

    if ($("#proveedor_id").val() == '') {
        toastr.error("Indicar proveedor!")
        return false;
    }


    let data = {
        "cantidad_solicitada": $("#cantidad_solicitada").val(),
        "producto_id": $("#producto_id").val(),
        "ma_pedido_id": $("#ma_pedido_id").val(),
        "proveedor_id": $("#proveedor_id").val()
    };


    if ($("#estatus_id").val() !== '') {
        data['estatus_id'] = $("#estatus_id").val();
    }

    if ($("#producto_pedido_id").val() !== '') {
        data['id'] = $("#producto_pedido_id").val();
    }


    ajax.post(`api/pedidoproducto`, data, function(response, headers) {
        if (headers.status == 201 || headers.status == 200) {
            $("#modaldetalle").modal('hide');
            $("#frm-producto")[0].reset();
            tabla_piezas.ajax.reload();
        }
    })
});


const updateproovedor = () => {
    const proveedor_id = document.getElementById('select_proveedor_id').value;
    selected_pedidoproducto_id = document.getElementById('selected_pedidoproducto_id').value
    if ($("#select_proveedor_id").val() == '') {
        toastr.error("Indicar proveedor!")
        return false;
    }

    if ($("#selected_pedidoproducto_id").val() == '') {
        toastr.error("no se ha cargado la informacion!")
        return false;
    }
    let dataupdate = {
        proveedor_id
    }

    ajax.put(`api/pedidoproducto/${selected_pedidoproducto_id}`, dataupdate, function(response, headers) {
        if (headers.status == 201 || headers.status == 200) {
            $("#modalproveedor").modal('hide');
            $("#frmmodalproveedor")[0].reset();
            tabla_piezas.ajax.reload();
        }
    })
}

const openmodalproveedor = (selected_pedidoproducto_id) => {
    document.getElementById('selected_pedidoproducto_id').value = selected_pedidoproducto_id
    $("#modalproveedor").modal('show');
}