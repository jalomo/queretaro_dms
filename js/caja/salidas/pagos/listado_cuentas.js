function inicializaTabla() {
    let total_pagar = 0;
    let tot = 0;
    var params = $.param({
        //'tipo_forma_pago_id': 2, //forma de pago credito
    });
    $('#tbl_cxp').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        footerCallback: function(row, data, start, end, display) {
            var api = this.api(),
                data;
            // Remove the formatting to get integer data for summation
            var intVal = function(i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
            };


            total_pago = api
                .column(4)
                .data()
                .reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            $("#total_pagar_tabla").html('$ <span class="money_format">' + total_pago + '</span>');

        },
        "ajax": {
            url: PATH_API + "api/cuentas-por-pagar?" + params,
            type: 'GET',
        },
        columns: [{
                title: "Folio",
                data: 'folio',
            },
            {
                title: "Concepto",
                data: 'concepto',
            },
            {
                title: "Proveedor",
                render: function(data, type, row) {
                    return row.numero_proveedor + ' - ' + row.proveedor;
                }
            },
            {
                title: "Fecha",
                render: function(data, type, row) {
                    return obtenerFechaMostrar(row.fecha)
                }
            },
            {
                title: "Monto a pagar",
                data: 'total',
                render: function(data, type, row) {
                    return utils.isDefined(data) ? '$ <span class="money_format">' + parseFloat(data).toFixed(2) + '</span>' : null;
                }
            },
            {
                title: '-',
                render: function(data, type, row) {
                    return '<button title="Detalle venta" onclick="detalleCredito(this)" data-id="' + row.id + '" class="btn btn-default"><i class="fas fa-list"></i></button>';

                }
            }
        ],
        "createdRow": function(row, data, dataIndex) {
            switch (data['estatus_cuenta_id']) {
                case 2:
                    $(row).find('td:eq(1)').css('background-color', '#8cdd8c');
                    break;
                case 3:
                    $(row).find('td:eq(1)').css('background-color', '#f6ffa4');
                    break;
                case 4:
                    $(row).find('td:eq(1)').css('background-color', '#e37f7f');
                    break;
                default:
                    break;
            }
        }
    });
}

function detalleCredito(_this) {
    setTimeout(() => {
        var cuenta_por_cobrar = $(_this).data('id');
        window.location.href = PATH + '/caja/salidas/detalle_pago/' + cuenta_por_cobrar;
    }, 200);
}

function obtenerFechaMostrar(fecha) {
    const dia = 2,
        mes = 1,
        anio = 0;
    fecha = fecha.split('T');
    fecha = fecha[0].split('-');
    return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
}

function filtrar() {
    var params = $.param({
        'folio': $("#folio").val(),
        'proveedor_id': $("#proveedor_id option:selected").val(),
        'estatus_cuenta_id': $("#estatus_cuenta_id option:selected").val()
    });

    $('#tbl_cxp').DataTable().ajax.url(PATH_API + 'api/cuentas-por-pagar?' + params).load()
}

function limpiarfiltro() {
    $("#folio").val('');
    $("#proveedor_id").val('');
    $("#estatus_cuenta_id").val('');
    $("#proveedor_id").trigger('change');
    var params = $.param({
        'folio': '',
        'proveedor_id': '',
        'estatus_cuenta_id': ''
    });

    $('#tbl_cxp').DataTable().ajax.url(PATH_API + 'api/cuentas-por-pagar?' + params).load()
}

this.inicializaTabla();
$("#proveedor_id").select2();