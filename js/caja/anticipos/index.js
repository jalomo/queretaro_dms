function crearTabla() {
    $('#table_anticipos').dataTable(this.configuracionTabla());
}

function configuracionTabla() {
    return {
        language: {
            url: PATH_LANGUAGE
        },
        order: [
            [0, 'asc']
        ],
        columns: [{
                title: "Folio",
                render: function(data, type, row) {
                    return '<small><b>' + row.folio + '</small>';
                },
            },
            {
                title: "Cliente",
                render: function(data, type, row) {
                    return '<small><b>' + row.numero_cliente + '</b><br/>' + row.nombre + ' ' + row.apellido_paterno + ' ' + row.apellido_materno + '</small>';
                },
            },
            {
                title: "Tipo",
                render: function(data, type, row) {
                    return '<small>' + row.proceso + '</small>';
                },
            },
            {
                title: "Total",
                data: 'total',
                render: function(data, type, row) {
                    return '<small class="money_format"><b>' + parseFloat(data).toFixed(2) + '</b></small>';
                },
            },
            {
                title: "Comentario",
                render: function(data, type, row) {
                    return '<small>' + row.comentario + '</small>';
                },
            },
            {
                title: "Estatus",
                render: function(data, type, row) {
                    let badge = '';
                    switch (parseInt(row.estatus_id)) {
                        case 1:
                            badge = "badge-success";
                            break;
                        case 2:
                            badge = "badge-warning";
                            break;
                        case 3:
                            badge = "badge-danger";
                            break;

                        default:
                            break;
                    }
                    return '<div class="badge ' + badge + '"><small>' + row.estatus + '</small></div>';
                },
            },
            {
                title: "Fecha creación",
                render: function(data, type, row) {
                    return '<small>' + this.obtenerFechaMostrar(row.fecha) + '</small>';
                },
            },
            {
                title: "Acciones",
                render: function(data, type, row) {
                    let btn_cancelar = '';
                    let btn_formato = '';
                    if (row.estatus_id == 1) {
                        btn_cancelar = '<button title="Cancelar" onclick="cambiarEstatus(this)" data-anticipo_id="' + row.id + '" data-estatus_id="3" type="button" class="btn btn-danger"><i class="fa fa-times fa-1x"></i></button>';
                        btn_formato = '<button title="Imprimir Formato" onclick="imprimirFormato(this)" data-anticipo_id="' + row.id + '" type="button" class="btn btn-primary"><i class="fa fa-file-pdf fa-1x"></i></button>';
                    }
                    return btn_cancelar + ' ' + btn_formato;
                }
            },
        ],

    }
}

function openModal() {
    $("#cliente_id_modal").val('');
    $("#tipo_proceso_id").val('');
    $("#total").val('');
    $("#modal-anticipo").modal("show");
    $('.select2Modal').select2({
        dropdownParent: $('#modal-anticipo')
    });
}

function filtrar() {
    var params = {
        'cliente_id': $("#cliente_id_ option:selected").val(),
        'estatus_id': $("#estatus_id option:selected").val()
    };
    this.getBusqueda(params);
}

function cambiarEstatus(_this) {

    let tipo = $(_this).data('estatus_id');
    let msj = tipo == 'APLICADO' ? ' aplicar ' : ' anular ';
    Swal.fire({
        icon: 'warning',
        title: '¿Esta seguro de' + msj + 'el anticipo?',
        showCancelButton: true,
        confirmButtonText: "Si",
        cancelButtonText: "No"
    }).then((result) => {
        if (result.value) {
            let params = {
                'estatus_id': $(_this).data('estatus_id')
            }
            ajax.put('api/anticipos/cambiar-estatus/' + $(_this).data('anticipo_id'), params, function(response, headers) {
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }
                utils.displayWarningDialog('Anticipo actualizado correctamente', "success", function(data) {
                    getBusqueda();
                })
            })
        }
    });
}

function imprimirFormato(_this) {
    window.open(
        PATH + '/caja/anticipos/comprobante_anticipo/' + $(_this).data('anticipo_id'),
        '_blank'
    );
}

function getBusqueda(params) {
    ajax.get("api/anticipos/get-filter", params, function(response, header) {
        var listado = $('table#table_anticipos').DataTable();
        console.log(response);
        listado.clear().draw();
        if (response && response.length > 0) {
            response.forEach(listado.row.add);
            listado.draw();
        }
    });
}

function obtenerFechaMostrar(fecha) {
    const dia = 2,
        mes = 1,
        anio = 0;
    fecha = fecha.split(' ');
    fecha = fecha[0].split('-');
    return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
}

function limpiarfiltro() {
    $("#fecha").val('');
    $("#estatus").val('');
    this.filtrar();
}

this.crearTabla();
this.filtrar();
$("#cliente_id_").select2();
$("#cliente_id").select2();
$('.money_format').mask("#,##0.00", { reverse: true });