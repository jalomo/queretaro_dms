function saveRegistroCorteCaja(params) {
    $(".invalid-feedback").html("");
    toastr.info("Guardando datos..");
    $.isLoading({
        text: "Realizando anticipo...."
    });
    let form = procesar_form('form_registro_corte');
    ajax.post(`api/registro-corte-caja`, form, function(response, headers) {
        if (headers.status == 400) {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        }
        $.isLoading("hide");
        utils.displayWarningDialog('Registro creado correctamente', "success", function(data) {
            getBusqueda();
        })
    })
}

function procesar_form(form_id) {
    let form = $('#' + form_id).serializeArray();
    var data = {};
    $(form).each(function(i, field) {
        data[field.name] = $.trim(field.value);
    });
    return data;
}

function busca_folio() {

    ajax.get(`api/folio/muestra-folio`, { folio: $("#folio").val() }, function(response, headers) {
        if (response.id) {
            $("#folio_id").val(response.id);
            busca_pago(response.id);
        } else {
            $('#form_registro_corte')[0].reset();
            toastr.error("No se encontrarón pagos para el folio " + $("#folio").val());
            return false;
        }
    })
}

function busca_pago(folio) {
    if (folio) {
        ajax.get(`api/abonos-por-cobrar/abono-por-folio`, { folio_id: folio, fecha_pago: $("#fecha_registro").val() }, function(response, headers) {
            if (headers.status == 204) {
                // $('#form_registro_corte')[0].reset();
                $("#total_cantidad_caja").val('');
                $("#tipo_pago_id").val('');
                $("#sobrantes").val('0.00');
                $("#faltantes").val('0.00');
                toastr.error("No se encontrarón pagos para el folio " + $("#folio").val() + " en la fecha seleccionada");
                return false;

            } else {
                $("#total_cantidad_caja").val(response.total_pago);
                $("#descripcion").val(response.concepto);
                $("#tipo_pago_id").val(response.tipo_pago_id);
                $("#sobrantes").val('0.00');
                $("#faltantes").val('0.00');
            }
        })
    }
}

function crearTabla() {
    $('#tabla_registro_corte').dataTable(this.configuracionTabla());
}

function configuracionTabla() {
    return {
        language: {
            url: PATH_LANGUAGE
        },
        order: [
            [0, 'asc']
        ],
        columns: [{
                title: "Folio",
                render: function(data, type, row) {
                    return '<small>' + row.folio + '</small>';
                },
            },
            {
                title: "Descripción",
                render: function(data, type, row) {
                    return '<small><b>' + row.descripcion + '</small>';
                },
            },
            {
                title: "Tipo de pago",
                render: function(data, type, row) {
                    return '<small><b>' + row.tipo_pago_id + '</small>';
                },
            },
            {
                title: "Cantidad en caja",
                render: function(data, type, row) {
                    return '<small>' + parseFloat(row.total_cantidad_caja).toFixed(2) + '</small>';
                },
            },
            {
                title: "Cantidad reportada",
                render: function(data, type, row) {
                    return '<small>' + parseFloat(row.total_cantidad_reportada).toFixed(2) + '</small>';
                },
            },
            {
                title: "Faltantes",
                render: function(data, type, row) {
                    return '<div class="text-danger"><small>' + parseFloat(row.faltantes).toFixed(2) + '</small></div>';
                },
            },
            {
                title: "Sobrantes",
                render: function(data, type, row) {
                    return '<div class="text-success"><small>' + parseFloat(row.sobrantes).toFixed(2) + '</small></div>';
                },
            },
            {
                title: "Fecha registro",
                render: function(data, type, row) {
                    return '<small>' + this.obtenerFechaMostrar(row.fecha_registro) + '</small>';
                },
            },
            {
                title: "Acciones",
                render: function(data, type, row) {
                    return '<button title="Cancelar" onclick="cambiarEstatus(this)" data-id="' + row.id + '" type="button" class="btn btn-danger"><i class="fa fa-trash fa-1x"></i></button>';
                }
            },
        ],

    }
}

function calcularTotales() {
    let total_cantidad_caja = 0;
    let total_cantidad_reportada = 0;
    $('table#tabla_registro_corte tbody tr').each(function() {
        total_cantidad_caja += parseFloat($(this).find("td").eq(3).text());
        total_cantidad_reportada += parseFloat($(this).find("td").eq(4).text());
        $(this).find("td").eq(3).addClass('money_format');
        $(this).find("td").eq(4).addClass('money_format');
    });
    $("#total_cantidad_caja_").html('$<span class="money_format">' + parseFloat(total_cantidad_caja).toFixed(2) + '</span>');
    $("#total_cantidad_reportada_").html('$<span class="money_format">' + parseFloat(total_cantidad_reportada).toFixed(2) + '</span>');

    $('.money_format').mask("#,##0.00", { reverse: true });
}

function filtrar() {
    var params = {
        'fecha': $("#fecha").val(),
    };
    this.getBusqueda(params).then(x => {
        this.calcularTotales();
    });
}

function obtenerFechaMostrar(fecha) {
    const dia = 2,
        mes = 1,
        anio = 0;
    fecha = fecha.split('T');
    fecha = fecha[0].split('-');
    return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
}


function getBusqueda(params) {
    return new Promise(function(resolve, reject) {
        params = {
            fecha_registro: $("#fecha").val()
        }
        $.ajax({
            dataType: "json",
            type: 'GET',
            data: params,
            url: PATH_API + 'api/registro-corte-caja/filtrar',
            success: function(response, _, _) {
                resolve(response);
                var listado = $('table#tabla_registro_corte').DataTable();
                listado.clear().draw();
                if (response && response.length > 0) {
                    response.forEach(listado.row.add);
                    listado.draw();
                }
            }
        });
    })
}

$("#folio").on('blur', function() {
    if ($("#folio").val().length > 4) {
        busca_folio();
    }
});

$("#fecha_registro").on('blur', function() {
    if ($("#fecha_registro").val().length > 4) {
        busca_pago($("#folio_id").val());
    }
});
$("#total_cantidad_reportada").on('blur', function() {
    if ($("#total_cantidad_reportada").val().length > 1) {
        let total_cantidad_reportada = $("#total_cantidad_reportada").val();
        let total_cantidad_caja = $("#total_cantidad_caja").val();
        let sobrantes = 0;
        let faltantes = 0;
        if (parseFloat(total_cantidad_reportada) > parseFloat(total_cantidad_caja)) {
            sobrantes = parseFloat(total_cantidad_reportada) - parseFloat(total_cantidad_caja);
            $("#sobrantes").val(sobrantes.toFixed(2));
        }
        if (parseFloat(total_cantidad_reportada) < parseFloat(total_cantidad_caja)) {
            faltantes = parseFloat(total_cantidad_caja) - parseFloat(total_cantidad_reportada);
            $("#faltantes").val(faltantes.toFixed(2));
        }
    }
});

this.crearTabla();
this.filtrar();