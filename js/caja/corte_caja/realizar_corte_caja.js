function saveRealizarCorteCaja(params) {
    $(".invalid-feedback").html("");
    toastr.info("Guardando datos..");
    $.isLoading({
        text: "Realizando anticipo...."
    });
    let form = procesar_form('form_registro_corte');
    form.fecha_registro = $("#fecha").val();
    ajax.post(`api/realizar-corte-caja`, form, function(response, headers) {
        if (headers.status == 400) {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        }
        $.isLoading("hide");
        utils.displayWarningDialog('Registro creado correctamente', "success", function(data) {
            filtrar();
            $('#form_registro_corte')[0].reset()
        })
    })
}

function procesar_form(form_id) {
    let form = $('#' + form_id).serializeArray();
    var data = {};
    $(form).each(function(i, field) {
        data[field.name] = $.trim(field.value);
    });
    return data;
}

function crearTabla() {
    $('#tabla_registro_corte').dataTable(this.configuracionTabla());
    $('#tabla_pagos_fecha').dataTable(this.configuracionTablaPagosFecha());
}

function configuracionTabla() {
    return {
        language: {
            url: PATH_LANGUAGE
        },
        order: [
            [0, 'asc']
        ],
        columns: [{
                title: "Número documento",
                render: function(data, type, row) {
                    return '<small>' + row.numero_documento + '</small>';
                },
            },
            {
                title: "Descripción",
                render: function(data, type, row) {
                    return '<small><b>' + row.descripcion + '</small>';
                },
            },
            {
                title: "Tipo de pago",
                render: function(data, type, row) {
                    return '<small><b>' + row.tipo_pago + '</small>';
                },
            },
            {
                title: "Cantidad en sistema",
                render: function(data, type, row) {
                    return '<small>' + parseFloat(row.cantidad_sistema).toFixed(2) + '</small>';
                },
            },
            {
                title: "Cantidad reportada",
                render: function(data, type, row) {
                    return '<small>' + parseFloat(row.cantidad).toFixed(2) + '</small>';
                },
            },
            {
                title: "Faltantes",
                render: function(data, type, row) {
                    return '<div class="text-danger"><small>' + parseFloat(row.faltantes).toFixed(2) + '</small></div>';
                },
            },
            {
                title: "Sobrantes",
                render: function(data, type, row) {
                    return '<div class="text-success"><small>' + parseFloat(row.sobrantes).toFixed(2) + '</small></div>';
                },
            },
            {
                title: "Fecha registro",
                render: function(data, type, row) {
                    return '<small>' + (row.fecha_registro) + '</small>';
                },
            },
            {
                title: "Acciones",
                render: function(data, type, row) {
                    return '<button title="Cancelar" onclick="eliminaRegistro(this)" data-id="' + row.id + '" type="button" class="btn btn-danger"><i class="fa fa-trash fa-1x"></i></button>';
                }
            },
        ],

    }
}

function eliminaRegistro(_this) {
    let id = $(_this).data('id');
    utils.displayWarningDialog("Esta seguro de  eliminar el registro?", "error", function(data) {
        if (data.value) {
            ajax.delete(`api/realizar-corte-caja/` + id, null, function(response, headers) {
                if (headers.status == 204) {
                    filtrar();
                    toastr.success('Registro eliminado correctamente!');
                }
            })
        }
    }, true)
}

function configuracionTablaPagosFecha() {
    return {
        language: {
            url: PATH_LANGUAGE
        },
        bFilter: false,
        lengthChange: false,
        bInfo: false,
        order: [
            [0, 'asc']
        ],
        columns: [{
                title: "Tipo de pago",
                render: function(data, type, row) {
                    return '<small><b>' + row.tipo_pago + '</small>';
                },
            },
            {
                title: "Cantidad",
                render: function(data, type, row) {
                    return '<small>' + parseFloat(row.monto_pagado).toFixed(2) + '</small>';
                },
            },

        ],

    }
}

function filtrar() {
    this.getBusquedaAbonosPagados()
    this.getBusqueda().then(x => {
        this.calcularTotales();
    });
    this.filtrarAsientos();
}

function calcularTotales() {
    let total_cantidad_caja = 0;
    let total_cantidad_reportada = 0;
    $('table#tabla_registro_corte tbody tr').each(function() {
        total_cantidad_caja += parseFloat($(this).find("td").eq(3).text());
        total_cantidad_reportada += parseFloat($(this).find("td").eq(4).text());
        $(this).find("td").eq(3).addClass('money_format');
        $(this).find("td").eq(4).addClass('money_format');
    });
    $("#total_cantidad_caja_").html('$<span class="money_format">' + parseFloat(total_cantidad_caja).toFixed(2) + '</span>');
    $("#total_cantidad_reportada_").html('$<span class="money_format">' + parseFloat(total_cantidad_reportada).toFixed(2) + '</span>');

    $('.money_format').mask("#,##0.00", { reverse: true });
}

function getBusqueda(params) {
    return new Promise(function(resolve, reject) {
        params = {
            fecha_registro: $("#fecha").val()
        }
        $.ajax({
            dataType: "json",
            type: 'GET',
            data: params,
            url: PATH_API + 'api/realizar-corte-caja/filtrar',
            success: function(response, _, _) {
                resolve(response);
                var listado = $('table#tabla_registro_corte').DataTable();
                listado.clear().draw();
                if (response && response.length > 0) {
                    response.forEach(listado.row.add);
                    listado.draw();
                }
            }
        });
    })
}

function getBusquedaAbonosPagados(params) {
    params = {
        fecha_pago: $("#fecha").val()
    }
    $.ajax({
        dataType: "json",
        type: 'GET',
        data: params,
        url: PATH_API + 'api/abonos-por-cobrar/abonos-pagados-fecha',
        success: function(response, _, _) {
            var listado = $('table#tabla_pagos_fecha').DataTable();
            listado.clear().draw();
            if (response && response.length > 0) {
                response.forEach(listado.row.add);
                listado.draw();
            } else {
                toastr.warning("Sin resultados para la fecha seleccionada")
            }
        }
    });
}

$("#tipo_pago_id").on('change', function() {
    $("#cantidad_sistema").val('0.00');
    $("#faltantes").val('0.00');
    $("#sobrantes").val('0.00');
    let fecha = $("#fecha").val();
    let tipo_pago_id = $("#tipo_pago_id").val();
    if (!fecha) {
        toastr.error("Falta indicar la fecha");
    }
    if (tipo_pago_id >= 1) {
        ajax.get(`api/abonos-por-cobrar/abonos-pagados-fecha`, { tipo_pago_id: $("#tipo_pago_id").val(), fecha_pago: fecha }, function(response, headers) {
            if (response && response.length > 0) {
                let datos = response.shift();
                $("#cantidad_sistema").val(parseFloat(datos.monto_pagado).toFixed(2));
                return false;
            } else {
                toastr.error("No se encontrarón pagos el tipo de pago y fecha seleccionada");
                $('#form_registro_corte')[0].reset();
            }
        })
    } else {
        toastr.error("Falta indicar un tipo de pago");
    }
});
$("#cantidad").on('blur', function() {
    $("#faltantes").val('0.00');
    $("#sobrantes").val('0.00');
    if ($("#cantidad").val().length > 1) {
        let total_cantidad_reportada = $("#cantidad").val();
        let total_cantidad_sistema = $("#cantidad_sistema").val();
        let sobrantes = 0;
        let faltantes = 0;
        if (parseFloat(total_cantidad_reportada) > parseFloat(total_cantidad_sistema)) {
            sobrantes = parseFloat(total_cantidad_reportada) - parseFloat(total_cantidad_sistema);
            $("#sobrantes").val(sobrantes.toFixed(2));
            $("#faltantes").val('0.00');
        }
        if (parseFloat(total_cantidad_reportada) < parseFloat(total_cantidad_sistema)) {
            faltantes = parseFloat(total_cantidad_sistema) - parseFloat(total_cantidad_reportada);
            $("#faltantes").val(faltantes.toFixed(2));
            $("#sobrantes").val('0.00');
        }
    }
});
$("#fecha").on('blur', function() {
    filtrar();
});
this.crearTabla();