function crearTablaAsientos() {
    $('#tabla_asientos').dataTable(this.configuracionTablaAsientos());
}

function configuracionTablaAsientos() {
    return {
        language: {
            url: PATH_LANGUAGE
        },
        paging: false,
        order: [
            [0, 'asc']
        ],
        columns: [{
                title: "Poliza",
                render: function(data, type, row) {
                    return '<small>' + row.poliza + '</small>';
                },
            },
            {
                title: "Cuenta",
                render: function(data, type, row) {
                    return '<small><b>' + row.cuenta + '</b><br/>' + row.cuenta_descripcion + '</small>';
                },
            },
            {
                title: "Cliente",
                render: function(data, type, row) {
                    return '<small>' + row.cliente + '</small>';
                },
            },
            {
                title: "Departamento",
                render: function(data, type, row) {
                    return '<small>' + row.departamento_descripcion + '</small>';
                },
            },
            {
                title: "Cargo",
                render: function(data, type, row) {
                    return '<small>' + parseFloat(row.cargo).toFixed(2) + '</small>';
                },
            },
            {
                title: "Abono",
                render: function(data, type, row) {
                    return '<small>' + parseFloat(row.abono).toFixed(2) + '</small>';
                },
            },
            {
                title: "Estatus",
                render: function(data, type, row) {
                    let badge = '';
                    switch (row.estatus_id) {
                        case 'POR_APLICAR':
                            badge = "badge-warning";
                            break;
                        case 'APLICADO':
                            badge = "badge-success";
                            break;
                        case 'ANULADO':
                            badge = "badge-danger";
                            break;

                        default:
                            break;
                    }
                    return '<div class="badge ' + badge + '"><small>' + row.estatus_id + '</small></div>';
                },
            },
            {
                title: "Fecha creación",
                render: function(data, type, row) {
                    return '<small>' + this.obtenerFechaMostrar(row.fecha_creacion) + '</small>';
                },
            },
            {
                title: "Acciones",
                render: function(data, type, row) {
                    let btn_autorizar = '';
                    let btn_cancelar = '';
                    if (row.estatus_id == 'POR_APLICAR') {
                        btn_cancelar = '<button title="Cancelar" onclick="cambiarEstatus(this)" data-asiento_id="' + row.asiento_id + '" data-estatus_id="ANULADO" type="button" class="btn btn-danger"><i class="fa fa-times fa-1x"></i></button>';
                        btn_autorizar = '<button title="Aplicar" onclick="cambiarEstatus(this)" data-asiento_id="' + row.asiento_id + '" data-estatus_id="APLICADO" type="button" class="btn btn-success"><i class="fa fa-check fa-1x"></i></button>';
                    }
                    return btn_autorizar + ' ' + btn_cancelar;
                }
            },
        ],

    }
}

function filtrarAsientos() {
    var params = {
        'nomenclatura': 'CG',
        'fecha': $("#fecha").val(),
        'estatus': 'POR_APLICAR'
    };
    this.getBusquedaAsientos(params).then(x => {
        this.calcularTotalesAsientos();
    });
}

function imprimir_reporte() {
    window.location.href = PATH + '/caja/reportes/imprimir_reporte?fecha_inicio=' + $("#fecha").val() + '&fecha_fin=' + $("#fecha").val()
}

function cambiarEstatus(_this) {
    let tipo = $(_this).data('estatus_id');
    let msj = tipo == 'APLICADO' ? ' aplicar ' : ' anular ';
    Swal.fire({
        icon: 'warning',
        title: '¿Esta seguro de' + msj + 'el asiento?',
        showCancelButton: true,
        confirmButtonText: "Si",
        cancelButtonText: "No"
    }).then((result) => {
        if (result.value) {
            $.ajax({
                dataType: "json",
                type: 'POST',
                url: API_CONTABILIDAD + '/asientos/api/aplicar_asiento',
                data: {
                    asiento_id: $(_this).data('asiento_id'),
                    estatus: $(_this).data('estatus_id')
                },
                success: function(response, _, _) {
                    if (response.status == 'success') {
                        utils.displayWarningDialog('Asiento actualizado correctamente', "success", function(data) {
                            this.filtrar();
                        })
                    }

                }
            });
        }
    });
}

function autorizacion_multiple() {
    var params = $.param({
        'nomenclatura': 'CG',
        'fecha': $("#fecha").val(),
        'estatus': 'POR_APLICAR'
    });

    $.ajax({
        dataType: "json",
        type: 'GET',
        data: params,
        url: API_CONTABILIDAD + '/asientos/api/detalle',
        success: function(response, _, _) {
            if (response.data) {
                $.isLoading({ text: "Cargando..." });
                toastr.info("Procesando información.....");
                let count = 0;
                $.each(response.data, function(index, value) {
                    toastr.clear();
                    $.ajax({
                        dataType: "json",
                        type: 'POST',
                        url: API_CONTABILIDAD + '/asientos/api/aplicar_asiento',
                        data: {
                            asiento_id: value.asiento_id,
                            estatus: 'APLICADO'
                        },
                        success: function(respuesta, _, _) {
                            if (respuesta.status == 'success') {
                                count++;
                                toastr.success("Asiento autorizado correctamente: " + value.asiento_id);

                                if (response.data.length == count) {
                                    $.isLoading("hide");
                                    utils.displayWarningDialog("Proceso de autorización concluido correctamente.!", "success", function(data) {
                                        this.filtrar();
                                    })
                                }
                            } else {
                                $.isLoading("hide");
                                toastr.error("Ocurrio un error al autorizar el asiento: " + value.asiento_id);
                            }
                        }
                    });
                });
            } else {
                $.isLoading("hide");
                toastr.info("No existen asientos por autorizar");
            }
        }
    });
}

function getBusquedaAsientos(params) {
    return new Promise(function(resolve, reject) {
        $.ajax({
            dataType: "json",
            type: 'GET',
            data: params,
            url: API_CONTABILIDAD + '/asientos/api/detalle',
            success: function(response, _, _) {
                resolve(response);
                var listado = $('table#tabla_asientos').DataTable();
                listado.clear().draw();
                if (response && response.data.length > 0) {
                    response.data.forEach(listado.row.add);
                    listado.draw();
                }
            }
        });
    })
}

function calcularTotalesAsientos() {
    let total_cargos = 0;
    let total_abonos = 0;
    $('table#tabla_asientos tbody tr').each(function() {
        total_cargos += parseFloat($(this).find("td").eq(4).text());
        total_abonos += parseFloat($(this).find("td").eq(5).text());
        $(this).find("td").eq(4).addClass('money_format');
        $(this).find("td").eq(5).addClass('money_format');
    });
    $("#total_cargos").html('$<span class="money_format">' + parseFloat(total_cargos).toFixed(2) + '</span>');
    $("#total_abonos").html('$<span class="money_format">' + parseFloat(total_abonos).toFixed(2) + '</span>');

    $('.money_format').mask("#,##0.00", { reverse: true });
}

function obtenerFechaMostrar(fecha) {
    const dia = 2,
        mes = 1,
        anio = 0;
    fecha = fecha.split('T');
    fecha = fecha[0].split('-');
    return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
}

// function limpiarfiltro() {
//     $("#fecha").val('');
//     $("#estatus").val('');
//     this.filtrar();
// }

this.crearTablaAsientos();
$('.money_format').mask("#,##0.00", { reverse: true });