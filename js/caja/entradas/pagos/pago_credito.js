function inicializaTabla() {
    $('#tbl_abonos').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        order: 2,
        "ajax": {
            url: PATH_API + "api/abonos-por-cobrar/listado-abonos-by-orden-entrada?orden_entrada_id=" + cuenta_por_cobrar_id,
            type: 'GET',
        },
        columns: [{
                title: "Num pago",
                render: function(data, type, row) {
                    if (row.tipo_abono_id == 2) {
                        let pagonum = count++;
                        return pagonum + '/' + cantidad_mes;
                    } else {
                        return '-';
                    }
                }
            },
            {
                title: "Tipo abono",
                data: "tipo_abono"
            },
            {
                title: "Fecha vencimiento",
                render: function(data, type, row) {
                    return obtenerFechaMostrar(row.fecha_vencimiento);
                }
            },
            {
                title: "Monto a abonar",
                data: "total_abono",
                render: function(data, type, row) {
                    return utils.isDefined(data) ? '$ <span class="money_format">' + parseFloat(data).toFixed(2) + '</span>' : '';
                }
            },
            {
                title: "Fecha pago",
                render: function(data, type, row) {
                    return utils.isDefined(row.fecha_pago) ? obtenerFechaMostrar(row.fecha_pago) : null;
                }
            },
            {
                title: "Monto pagado",
                data: "total_pago",
                render: function(data, type, row) {
                    return utils.isDefined(data) ? '$ <span class="money_format">' + parseFloat(data).toFixed(2) + '</span>' : null;
                }
            },
            {
                title: "Dias <br/>moratorios",
                data: 'dias_moratorios',
            },
            {
                title: "Monto moratorio",
                data: "monto_moratorio",
                render: function(data, type, row) {
                    return utils.isDefined(data) ? '$ <span class="money_format">' + parseFloat(data).toFixed(2) + '</span>' : null;
                }
            },
            {
                title: "Estatus abono",
                data: "estatus_abono"
            },
            {
                title: "CFDI",
                render: function(data, type, row) {
                    return utils.isDefined(row.clave_cfdi) ? row.clave_cfdi + ' - ' + row.descripcion_cfdi : null;
                }
            },
            {
                title: "Acciones",
                render: function(data, type, row) {
                    var btn_enganche = '';
                    var btn_abonar = '';
                    var btn_comprobante = '';
                    var btn_facturar = '';
                    if (estatus_cuentas_por_cobrar != 4) {
                        if (row.tipo_abono_id == 1) {
                            if (row.estatus_abono_id == 1 || row.estatus_abono_id == 2) {
                                var btn_enganche = '<button title="Pagar enganche" onclick="open_modal_enganche(this)" data-enganche_id="' + row.id + '" class="btn btn-default"><i class="fas fa-cash-register"></i></button>';
                            } else if (row.estatus_abono_id == 3) {
                                var btn_comprobante = '<button title="Imprimir comprobante"  onclick="imprimir_comprobante(this)" data-abono_id="' + row.id + '" data-cuenta_por_cobrar_id="' + row.cuenta_por_cobrar_id + '" class="btn btn-default"><i class="fas fa-file-pdf"></i></button>';
                                var btn_facturar = '<button title="Imprimir factura"  onclick="generar_factura(this)"  data-abono_id="' + row.id + '" data-cuenta_por_cobrar_id="' + row.cuenta_por_cobrar_id + '" class="btn btn-success"><i class="fas fa-file-excel"></i></button>';
                            }
                        } else if (row.tipo_abono_id == 2) {
                            if (row.estatus_abono_id == 1 || row.estatus_abono_id == 2) {
                                var btn_abonar = '<button title="Realizar pago" onclick="open_modal_abono(this)" data-abono_id="' + row.id + '" data-monto_moratorio="' + row.monto_moratorio + '" data-total_abono="' + row.total_abono + '" class="btn btn-default"><i class="fas fa-cash-register"></i></button>';
                            } else if (row.estatus_abono_id == 3) {
                                if (row.total_abono >= 1) {
                                    var btn_comprobante = '<button title="Imprimir comprobante"  onclick="imprimir_comprobante(this)" data-cuenta_por_cobrar_id="' + row.cuenta_por_cobrar_id + '"  data-abono_id="' + row.id + '" class="btn btn-default"><i class="fas fa-file-pdf"></i></button>';
                                    var btn_facturar = '<button title="Imprimir factura"  onclick="generar_factura(this)"  data-abono_id="' + row.id + '" data-cuenta_por_cobrar_id="' + row.cuenta_por_cobrar_id + '" class="btn btn-success"><i class="fas fa-file-excel"></i></button>';
                                }
                            }
                        }
                    } else {
                        if (row.tipo_abono_id == 1) {
                            if (row.estatus_abono_id == 3) {
                                var btn_comprobante = '<button title="Imprimir comprobante"  onclick="imprimir_comprobante(this)"  data-abono_id="' + row.id + '" data-cuenta_por_cobrar_id="' + row.cuenta_por_cobrar_id + '" class="btn btn-default"><i class="fas fa-file-pdf"></i></button>';
                                var btn_facturar = '<button title="Imprimir factura"  onclick="generar_factura(this)"  data-abono_id="' + row.id + '" data-cuenta_por_cobrar_id="' + row.cuenta_por_cobrar_id + '" class="btn btn-success"><i class="fas fa-file-excel"></i></button>';
                            }
                        } else if (row.tipo_abono_id == 2) {
                            if (row.estatus_abono_id == 3) {
                                var btn_comprobante = '<button title="Imprimir comprobante"  onclick="imprimir_comprobante(this)"  data-abono_id="' + row.id + '" data-cuenta_por_cobrar_id="' + row.cuenta_por_cobrar_id + '" class="btn btn-default"><i class="fas fa-file-pdf"></i></button>';
                                var btn_facturar = '<button title="Imprimir factura"  onclick="generar_factura(this)"  data-abono_id="' + row.id + '" data-cuenta_por_cobrar_id="' + row.cuenta_por_cobrar_id + '" class="btn btn-success"><i class="fas fa-file-excel"></i></button>';
                            }
                        }
                    }
                    return btn_enganche + btn_abonar + btn_comprobante + ' ' + btn_facturar;
                }
            },
        ],
        "createdRow": function(row, data, dataIndex) {
            switch (data['estatus_abono_id']) {
                case 2:
                    $(row).find('td:eq(8)').css('background-color', '#f6ffa4');
                    break;
                case 3:
                    $(row).find('td:eq(8)').css('background-color', '#8cdd8c');
                    if (data['total_abono'] < 1) {
                        $(row).hide();
                    }
                    break;
                default:
                    break;
            }
        }

    });
}

function imprimir_factura() {
    utils.displayWarningDialog('MÓDULO EN PROCESO', "success", function(data) {})
}

function imprimir_comprobante(_this) {
    abono_id = $(_this).data('abono_id');
    window.location.href = PATH + '/caja/entradas/imprime_comprobante?abono_id=' + window.btoa(abono_id);
}

function imprimir_estado_cuenta(_this) {
    cuenta_id = $(_this).data('cuenta_por_cobrar_id');
    window.location.href = PATH + '/caja/entradas/imprime_estado_cuenta?cuenta_id=' + window.btoa(cuenta_id);
}

function obtenerFechaMostrar(fecha) {
    const dia = 2,
        mes = 1,
        anio = 0;
    fecha = fecha.split('T');
    fecha = fecha[0].split('-');
    return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
}

$("#pago").on('blur', function() {
    let pago = $("#pago").val().replace(",", "");;
    let total_pago = $("#total_pago").val().replace(",", "");

    if (parseFloat(pago) < parseFloat(total_pago)) {
        toastr.error("El pago es menor al total a abonar")
        $("#btn-modal-abonar").attr('disabled', true);
        $("#pago").val(0);
    } else {
        let cambio = parseFloat(pago) - parseFloat(total_pago);
        $("#cambio").val(cambio.toFixed(2));
        $("#btn-modal-abonar").attr('disabled', false);
        $("#cambio").mask("#,##0.00");

    }
});

$("#pago_enganche").on('blur', function() {
    let pago = $("#pago_enganche").val().replace(",", "");;
    let total_pago = $("#total_pago_enganche").val().replace(",", "");
    if (parseFloat(pago) < parseFloat(total_pago)) {
        toastr.error("El pago es menor al total del enganche")
        $("#btn-modal-enganche").attr('disabled', true);
        $("#pago_enganche").val(0);
    } else {
        let cambio = parseFloat(pago) - parseFloat(total_pago);

        $("#cambio_enganche").val(cambio.toFixed(2));
        $("#btn-modal-enganche").attr('disabled', false);
    }
});

let form_abono = function() {
    //let total =  (parseFloat(document.getElementById("total_pago").value));
    let moratorio = $("#monto_moratorio").val() >= 1 ? $("#monto_moratorio").val().replace(",", "") : 0;
    let total = (parseFloat($("#total_pago").val().replace(",", "")) - parseFloat(moratorio));
    return {
        cuenta_por_cobrar_id: cuenta_por_cobrar_id,
        tipo_abono_id: document.getElementById("tipo_abono_id").value,
        total_pago: total,
        tipo_pago_id: document.getElementById("tipo_pago_id").value,
        cfdi_id: document.getElementById("cfdi_id").value,
        caja_id: document.getElementById("caja_id").value,
        banco_id: document.getElementById("banco_id").value,
        fecha_pago: document.getElementById("fecha_pago").value,
        estatus_abono_id: 3 //SE MARCA COMO PAGADO
    };
}

$("#btn-modal-abonar").on('click', function() {
    let total_pago = $("#total_pago").val().replace(",", "");;
    let minimo_abonar = $("#minimo_abonar").val().replace(",", "");;
    if (parseFloat(minimo_abonar) > parseFloat(total_pago)) {
        toastr.error("El monto minimo abonar es de: " + minimo_abonar);
        return false;
    }
    $(".invalid-feedback").html("");
    toastr.info("Realizando abono..");
    $.isLoading({
        text: "Realizando abono...."
    });
    let abono_id = $("#abono_id").val();
    ajax.put(`api/abonos-por-cobrar/${abono_id}`, form_abono(), function(response, headers) {
        if (headers.status == 400) {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        } else {
            $.isLoading("hide");
            utils.displayWarningDialog('Cuenta actualizada correctamente', "success", function(data) {
                $('#modal-abonar').modal('hide')
                window.location.reload();
            })
        }
    })
})

let form_enganche = function() {
    return {
        cuenta_por_cobrar_id: cuenta_por_cobrar_id,
        tipo_abono_id: document.getElementById("tipo_abono_id_enganche").value,
        total_pago: document.getElementById("total_pago_enganche").value,
        tipo_pago_id: document.getElementById("tipo_pago_id_enganche").value,
        cfdi_id: document.getElementById("cfdi_id_enganche").value,
        caja_id: document.getElementById("caja_id_enganche").value,
        fecha_pago: document.getElementById("fecha_pago_enganche").value,
        folio_id: document.getElementById('folio_id').value,
        banco_id: document.getElementById("banco_id_enganche").value,
        estatus_abono_id: 3 //SE MARCA COMO PAGADO
    };
}

$("#btn-modal-enganche").on('click', function() {
    $(".invalid-feedback").html("");
    toastr.info("Realizando enganche..");
    $.isLoading({
        text: "Realizando enganche...."
    });
    let enganche_id = $("#enganche_id").val();
    ajax.put(`api/abonos-por-cobrar/${enganche_id}`, form_enganche(), function(response, headers) {
        if (headers.status == 400) {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        }
        $.isLoading("hide");
        utils.displayWarningDialog('Enganche efectuado correctamente', "success", function(data) {
            $('#modal-enganche').modal('hide')
            window.location.reload();
        })
    })
})

function open_modal_abono(_this) {
    $('#modal-abonar').modal('show');

    $("#abono_id").val($(_this).data('abono_id'));
    let tot = $(_this).data('total_abono');
    let monto = $(_this).data('monto_moratorio');
    let moratorio = monto != null ? monto : 0;
    $("#monto_moratorio").val(monto);
    let total_pago = parseFloat(tot) + parseFloat(moratorio);
    $("#total_pago").val(total_pago.toFixed(2));
    $("#minimo_abonar").val(total_pago.toFixed(2));
}

function open_modal_enganche(_this) {
    $("#enganche_id").val($(_this).data('enganche_id'));
    $('#modal-enganche').modal('show');
}

function generar_factura(_this) {
    toastr.info("Procesando petición ..");
    $.isLoading({
        text: "Realizando petición ...."
    });
    ajax.post(`api/cuentas-por-cobrar/aplicar-factura`, { cuenta_por_cobrar_id: $(_this).data('id') }, function(response, headers) {
        if (headers.status == 400) {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        } else {
            $.isLoading("hide");
            utils.displayWarningDialog(headers.message, "success", function(data) {
                if (response.data.cfdi) {
                    window.open(response.data.cfdi, '_blank');
                }
                $.isLoading({
                    text: "Actualizando información"
                });
                window.location.reload();
            })
        }
    })
}

function crearTabla() {
    $('#table_anticipos').dataTable(this.configuracionTabla());
}

function configuracionTabla() {
    let estatus_cuenta_id = $("#estatus_cuenta_id").val();

    return {
        language: {
            url: PATH_LANGUAGE
        },
        order: [
            [0, 'asc']
        ],
        searching: false,
        bInfo: false,
        searching: false,
        bLengthChange: false,
        columns: [{
                title: "Folio anticipo",
                render: function(data, type, row) {
                    return '<small><b>' + ('0000' + row.folio_id).slice(-5) + '</small>';
                },
            },
            {
                title: "Tipo",
                render: function(data, type, row) {
                    return '<small>' + row.proceso + '</small>';
                },
            },
            {
                title: "Total",
                data: 'total',
                render: function(data, type, row) {
                    return '<small class="money_format"><b>' + parseFloat(data).toFixed(2) + '</b></small>';
                },
            },
            {
                title: "Estatus",
                render: function(data, type, row) {
                    let badge = '';
                    switch (parseInt(row.estatus_id)) {
                        case 1:
                            badge = "badge-success";
                            break;
                        case 2:
                            badge = "badge-warning";
                            break;
                        case 3:
                            badge = "badge-danger";
                            break;

                        default:
                            break;
                    }
                    return '<div class="badge ' + badge + '"><small>' + row.estatus + '</small></div>';
                },
            },
            {
                title: "Fecha creación",
                render: function(data, type, row) {
                    return '<small>' + this.obtenerFechaMostrar(row.fecha) + '</small>';
                },
            },
            {
                title: "Acciones",
                render: function(data, type, row) {
                    let btn_utilizar = '';
                    if (row.estatus_id == 1 && estatus_cuenta_id == 5) {
                        btn_utilizar = '<button title="Utilizar anticipo" onclick="aplicarAnticipo(this)" data-anticipo_id="' + row.id + '" type="button" class="btn btn-success"><i class="fa fa-cash-register fa-1x"></i></button>';
                    }
                    return btn_utilizar;
                }
            },
        ],

    }
}

function getBusqueda() {
    let params = {
        'cliente_id': $("#cliente_id").val(),
        'tipo_proceso_id': $("#tipo_proceso_id").val(),
    }
    ajax.get("api/anticipos/get-filter", params, function(response, header) {
        var listado = $('table#table_anticipos').DataTable();
        listado.clear().draw();
        if (response && response.length > 0) {
            response.forEach(listado.row.add);
            listado.draw();
        }
    });
}

function obtenerFechaMostrar(fecha) {
    const dia = 2,
        mes = 1,
        anio = 0;
    fecha = fecha.split(' ');
    fecha = fecha[0].split('-');
    return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
}

function aplicarAnticipo(_this) {

    Swal.fire({
        icon: 'warning',
        title: '¿Esta seguro de aplicar el anticipo?',
        showCancelButton: true,
        confirmButtonText: "Si",
        cancelButtonText: "No"
    }).then((result) => {
        if (result.value) {
            let params = {
                'folio_id': $("#folio_id").val()
            }
            ajax.put('api/anticipos/aplicar-anticipo/' + $(_this).data('anticipo_id'), params, function(response, headers) {
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }
                utils.displayWarningDialog('Anticipo aplicado correctamente', "success", function(data) {
                    window.location.reload();
                })
            })
        }
    });
}

$("#porcentaje_1").on('blur', function() {
    let porcentaje = $("#porcentaje_1").val();

    if (!$.isNumeric(porcentaje)) {
        toastr.error("El campo no es un número")
    }
    let total_aplicar = $("#total_aplicar_1").val().replace(",", "");
    let cantidad_descontada = total_aplicar * porcentaje / 100;
    let total_descuento = total_aplicar - cantidad_descontada;
    $("#total_descuento_1").val(parseFloat(total_descuento).toFixed(2));
    $("#cantidad_descontada_1").val(parseFloat(cantidad_descontada).toFixed(2));
});

$("#porcentaje_2").on('blur', function() {
    let porcentaje = $("#porcentaje_2").val();

    if (!$.isNumeric(porcentaje)) {
        toastr.error("El campo no es un número")
    }
    let total_aplicar = $("#total_aplicar_2").val().replace(",", "");
    let cantidad_descontada = total_aplicar * porcentaje / 100;
    let total_descuento = total_aplicar - cantidad_descontada;
    $("#total_descuento_2").val(parseFloat(total_descuento).toFixed(2));
    $("#cantidad_descontada_2").val(parseFloat(cantidad_descontada).toFixed(2));
});

$("#btn-aplicar-1").on('click', function() {
    $.isLoading({
        text: "Realizando petición ...."
    });
    dataForm = {
        folio_id: $("#folio_id").val(),
        total_aplicar: document.getElementById("total_aplicar_1").value.replace(",", ""),
        tipo_descuento: 1,
        porcentaje: document.getElementById("porcentaje_1").value,
        cantidad_descontada: document.getElementById("cantidad_descontada_1").value.replace(",", ""),
        total_descuento: document.getElementById("total_descuento_1").value.replace(",", ""),
        comentario: document.getElementById("comentario_1").value,
        fecha: strDate
    }

    ajax.post(`api/descuentos/guardar-descuento`, dataForm, function(response, headers) {
        if (headers.status == 200 || headers.status == 201) {
            $.isLoading("hide");
            utils.displayWarningDialog(headers.message, "success", function(data) {
                window.location.reload();
            })
        } else {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        }
    })
});

$("#btn-aplicar-2").on('click', function() {
    $.isLoading({
        text: "Realizando petición ...."
    });
    dataForm = {
        folio_id: $("#folio_id").val(),
        total_aplicar: document.getElementById("total_aplicar_2").value.replace(",", ""),
        tipo_descuento: 2,
        porcentaje: document.getElementById("porcentaje_2").value,
        cantidad_descontada: document.getElementById("cantidad_descontada_2").value.replace(",", ""),
        total_descuento: document.getElementById("total_descuento_2").value.replace(",", ""),
        comentario: document.getElementById("comentario_2").value,
        fecha: strDate
    }

    ajax.post(`api/descuentos/guardar-descuento`, dataForm, function(response, headers) {
        if (headers.status == 200 || headers.status == 201) {
            $.isLoading("hide");
            utils.displayWarningDialog(headers.message, "success", function(data) {
                window.location.reload();
            })
        } else {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        }
    })
});

this.inicializaTabla();
var d = new Date();
var strDate = d.getFullYear() + "/" + (d.getMonth() + 1) + "/" + d.getDate();
$("#proveedor_id").select2();
$('.money_format').mask("#,##0.00", { reverse: true });
this.crearTabla();
this.getBusqueda();