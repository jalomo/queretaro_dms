var d = new Date();
var strDate = d.getFullYear() + "/" + (d.getMonth() + 1) + "/" + d.getDate();
let cliente_id = null;
$("#btn-pagar").on('click', function() {
    $.isLoading({
        text: "Realizando petición ...."
    });
    dataForm = {
        cuenta_por_cobrar_id: orden_entrada_id,
        total_pago: document.getElementById("importe").value.replace(",", ""),
        tipo_abono_id: 3,
        tipo_pago_id: document.getElementById("tipo_pago").value.replace(",", ""),
        cfdi_id: document.getElementById("cfdi_id").value,
        caja_id: document.getElementById("caja_id").value,
        caja_id: document.getElementById("caja_id").value,
        banco_id: document.getElementById("banco_id").value,
        regimen_fiscal: document.getElementById("regimen_fiscal_").value,
        fecha_pago: strDate,
        estatus_abono_id: 3,
        fecha_vencimiento: strDate,
        banco_pagar_id: $("#banco_pagar_id").val(),
        referencia_tarjeta: $("#referencia_tarjeta").val(),
        referencia_cheque: $("#referencia_cheque").val()
    }
    let pago = $("#pago").val().replace(",", "");;
    if (!parseFloat(pago) && pago < 1) {
        $.isLoading("hide");
        toastr.error("Falta indicar el pago")
        $.isLoading("hide");
        return false;
    }
    let abono_id = $("#abono_id").val();

    procesarAsientos().then(x => {
        if (x == true) {
            ajax.put(`api/abonos-por-cobrar/${abono_id}`, dataForm, function(response, headers) {
                if (headers.status == 200 || headers.status == 201) {
                    $.isLoading("hide");
                    ajax.put(`api/cuentas-por-cobrar/updatestatus/${orden_entrada_id}`, {
                        estatus_cuenta_id: 2
                    }, function(response, header) {
                        if (header.status == 400) {
                            $.isLoading("hide");
                            return ajax.showValidations(header);
                        }
                        utils.displayWarningDialog('Pago realizado correctamente!!', "success", function(data) {
                            toastr.info("Reedireccionando ...");
                            $.isLoading({
                                text: "Realizando petición ...."
                            });
                            window.location.reload();
                        })
                    })
                } else {
                    $.isLoading("hide");
                    return ajax.showValidations(headers);
                }
            });
        } else {
            $.isLoading("hide");
            utils.displayWarningDialog('Ocurrio un error al procesar los asientos', "error", function(data) {});
        }
    });

});

$("#pago").on('blur', function() {
    let pago = $("#pago").val().replace(",", "");
    let importe = $("#importe").val().replace(",", "");
    if (parseFloat(pago) < parseFloat(importe)) {
        toastr.error("El pago es menor al total a pagar")
        $("#btn-pagar").attr('disabled', true);
        $("#pago").val(0);
    } else {
        let cambio = parseFloat(pago) - parseFloat(importe);
        $("#cambio").val(cambio.toFixed(2));
        $("#btn-pagar").attr('disabled', false);
    }
});

function validarTipoPago(_this) {
    let tipo_pago = parseInt($(_this).val());
    switch (tipo_pago) {
        case 1:
            $(".banco_pagar_id").hide();
            $(".referencia_tarjeta").hide();
            $(".referencia_cheque").hide();
            break;
        case 2:
            $(".banco_pagar_id").hide();
            $(".referencia_tarjeta").hide();
            $(".referencia_cheque").show();
            break;
        default:
            $(".banco_pagar_id").show();
            $(".referencia_tarjeta").show();
            $(".referencia_cheque").hide();
            break;
    }
}

function imprimir_comprobante(_this) {
    abono_id = $(_this).data('abono_id');
    window.open(window.location.href = PATH + '/caja/entradas/imprime_comprobante?abono_id=' + window.btoa(abono_id), '_blank');
}

function imprimir_estado_cuenta(_this) {
    cuenta_id = $(_this).data('cuenta_por_pagar_id');
    window.open(window.location.href = PATH + '/caja/entradas/imprime_estado_cuenta?cuenta_id=' + window.btoa(cuenta_id), '_blank');
}

function updateClienteCuentaPorCobrar(id_cliente) {
    $.isLoading({
        text: "Realizando petición ...."
    });
    ajax.put(`api/cuentas-por-cobrar/actualizar-cliente/` + cuenta_por_cobrar_id, {
        cliente_id: id_cliente
    }, function(response, headers) {
        if (headers.status == 200 || headers.status == 201) {
            $.isLoading("hide");
            utils.displayWarningDialog('Cliente actualizado correctamente', "success", function(data) {
                $.isLoading({
                    text: "Procesando datos y recuperando ...."
                });
                window.location.reload();
            })
        } else {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        }
    })
}

function guardarCliente() {
    let cliente_id = $("select[name*='cliente_id'] option:selected").val();

    if (!utils.isDefined(cliente_id) && cliente_id < 1) {
        toastr.error("Falta seleccionar al cliente");
        return false;
    }
    $.isLoading({
        text: "Realizando petición ...."
    });
    let form = $('#form-cliente').serializeArray();
    var data = {};
    $(form).each(function(i, field) {
        data[field.name] = $.trim(field.value);
    });
    ajax.put(`api/clientes/actualizar-datos-cliente/` + cliente_id, data, function(response, headers) {
        if (headers.status == 200 || headers.status == 201) {
            $.isLoading("hide");
            this.updateClienteCuentaPorCobrar(cliente_id);
        } else {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        }
    })
}

function updateCliente() {
    let cliente_id = $("#cliente_id").val();
    let form = procesar_form('form-cliente');
    ajax.put(`api/clientes/update-cliente-modal/` + cliente_id, form, function(response, header) {
        if (header.status == 200) {
            utils.displayWarningDialog('Cliente actualizado correctamente', 'success', function(result) {
                $("#modal-editar-cliente").modal("hide");
            });
        }
    });
}

function procesar_form(form_id) {
    let form = $('#' + form_id).serializeArray();
    var data = {};
    $(form).each(function(i, field) {
        data[field.name] = $.trim(field.value);
    });
    return data;
}

function openDatosCliente(_this) {
    $.isLoading({
        text: "Realizando petición ...."
    });
    toastr.info("Obteniendo información");
    cliente_id = $(_this).data('cliente_id');
    setTimeout(() => {
        ajax.get("api/clientes/" + cliente_id, {}, function(response, header) {
            if (header.status == 200) {
                $.each(response.shift(), function(indexInArray, valueOfElement) {
                    $("[name='" + indexInArray + "']").val(valueOfElement);
                });
                $("#cliente_id").val(cliente_id);
            }
        });
        $("#datos_credito").show();
        ajax.post('api/clientes/tiene-credito', { id: cliente_id }, function(response, header) {
            if (header.status == 200) {
                $("#aplica_credito").val(utils.isDefined(response.aplica_credito) && response.aplica_credito == 1 ? 'Tiene Crédito' : 'Sin Crédito');
                $("#limite_credito").val(utils.isDefined(response.limite_credito) ? parseFloat(response.limite_credito).toFixed(2) : '');
                $("#plazo_credito").val(utils.isDefined(response.plazo_credito) ? response.plazo_credito : '');
                $("#credito_actual").val(utils.isDefined(response.credito_actual) ? parseFloat(response.credito_actual).toFixed(2) : '');
            }
        }, false, false);
        $.isLoading("hide");
        $("#modal-editar-cliente").modal("show");
    }, 1000);
}

function procesarAsientos(_this) {
    return new Promise(function(resolve, reject) {
        let band = false;
        $.isLoading("hide");

        $.isLoading({
            text: "Procesando asientos contabilidad ...."
        });
        if ($("#tipo_proceso_id").val() == 1) {
            console.log("entra tipo proceso 1");
            if ($('#tipo_forma_pago_id').val() == 2) {
                console.log("entra forma de pago credito");
                ajax.post(`api/asientos/poliza-venta-mostrador-credito`, {
                    cuenta_por_cobrar_id: cuenta_por_cobrar_id,
                    cuenta_bancaria_id: document.getElementById("banco_id").value,
                }, function(response, headers) {
                    if (headers.status != 201) {
                        $.isLoading("hide");
                        utils.displayWarningDialog('Ocurrio un error al generar los asientos', "error", function(data) {});
                        band = false;
                        resolve(false);
                    } else {
                        console.log("Procesa asientos correctamente crédito");
                        resolve(true);
                    }
                })
            } else {
                ajax.post(`api/asientos/poliza-venta-mostrador`, {
                    cuenta_por_cobrar_id: cuenta_por_cobrar_id,
                    cuenta_bancaria_id: document.getElementById("banco_id").value,
                }, function(response, headers) {
                    if (headers.status != 201) {
                        $.isLoading("hide");
                        utils.displayWarningDialog('Ocurrio un error al generar los asientos', "error", function(data) {});
                        resolve(false);
                    } else {
                        resolve(true);
                    }
                })
            }
        } else if ($("#tipo_proceso_id").val() == 8 || $("#tipo_proceso_id").val() == 9) {
            resolve(true);
        }

    });
}

$('#cliente_id').on('select2:selecting', function(e) {
    $.isLoading({
        text: "Realizando petición ...."
    });
    toastr.info("Obteniendo información");
    setTimeout(() => {
        let cliente_id = $("select[name*='cliente_id'] option:selected").val();
        ajax.get("api/clientes/" + cliente_id, {}, function(response, header) {
            if (header.status == 200) {
                return this.dataCliente(response.shift());
            }
        });
    }, 1000);

});

function goToFacturaMultiple(_this) {
    setTimeout(() => {
        window.location.href = PATH + '/caja/entradas/pago_factura/' + cuenta_por_cobrar_id;
    }, 200);
}

function generar_factura(_this) {
    toastr.info("Procesando petición ..");
    $.isLoading({
        text: "Realizando petición ...."
    });
    ajax.post(`api/cuentas-por-cobrar/aplicar-factura`, { cuenta_por_cobrar_id: $(_this).data('id') }, function(response, headers) {
        if (headers.status == 400) {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        } else {
            $.isLoading("hide");
            utils.displayWarningDialog(headers.message, "success", function(data) {
                if (response.data.cfdi) {
                    window.open(response.data.cfdi, '_blank');
                }
                $.isLoading({
                    text: "Actualizando información"
                });
                window.location.reload();
            })
        }
    })
}

function imprimir_comprobante_fiscal(_this) {
    ajax.get(`api/cuentas-por-cobrar/comprobante-fiscal`, { folio: $(_this).data('folio') }, function(response, headers) {
        console.log($(_this).data('folio'));
        if (utils.isDefined(response.data) && response.data.pdf) {
            window.open(response.data.pdf, '_blank');
        } else {
            utils.displayWarningDialog('Ocurrio un error al descargar el comprobante fiscal', "error", function(data) {});
        }
    })
}

function dataCliente(data) {

    setTimeout(() => {
        $("#tipo_registro").val(data.tipo_registro);
        $("#regimen_fiscal").val(data.regimen_fiscal);
        $("#nombre_empresa").val(data.nombre_empresa);
        $("#rfc").val(data.rfc);
        $("#direccion").val(data.direccion);
        $("#numero_ext").val(data.numero_ext);
        $("#numero_int").val(data.numero_int);
        $("#colonia").val(data.colonia);
        $("#codigo_postal").val(data.codigo_postal);
        $("#municipio").val(data.municipio);
        $("#estado").val(data.estado);
        $.isLoading("hide");
    }, 1000);

}

$(".select2").select2();
ajax.getCatalogo('tipo-pago', $("select[name*='tipo_pago']"), true);
ajax.getCatalogo('cfdi', $("select[name*='cfdi_id']"), true);
ajax.getCatalogo('catalogo-caja', $("select[name*='caja_id']"), true);
ajax.getCatalogo('catalogo-cuentas/cuentas-bancarias', $("select[name*='banco_id']"), true);
ajax.getCatalogo('catalogo-bancos', $("select[name*='banco_pagar_id']"), true);

$('.money_format').mask("#,##0.00", {
    reverse: true
});