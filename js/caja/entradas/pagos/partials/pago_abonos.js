function inicializaTabla() {
    let parcialidad = 0;
    $('#tbl_abonos').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        order: 2,
        "ajax": {
            url: PATH_API + "api/abonos-por-cobrar/listado-abonos-by-orden-entrada?orden_entrada_id=" + cuenta_por_cobrar_id,
            type: 'GET',
        },
        columns: [{
                title: "Num pago",
                render: function(data, type, row) {
                    if (row.tipo_abono_id == 2) {
                        let pagonum = count++;
                        return pagonum + '/' + cantidad_mes;
                    } else {
                        return '-';
                    }
                }
            },
            {
                title: "Tipo abono",
                data: "tipo_abono"
            },
            {
                title: "Fecha vencimiento",
                render: function(data, type, row) {
                    return obtenerFechaMostrar(row.fecha_vencimiento);
                }
            },
            {
                title: "Monto a abonar",
                data: "total_abono",
                render: function(data, type, row) {
                    return utils.isDefined(data) ? '$ <span class="money_format">' + parseFloat(data).toFixed(2) + '</span>' : '';
                }
            },
            {
                title: "Fecha pago",
                render: function(data, type, row) {
                    return utils.isDefined(row.fecha_pago) ? obtenerFechaMostrar(row.fecha_pago) : null;
                }
            },
            {
                title: "Monto pagado",
                data: "total_pago",
                render: function(data, type, row) {
                    return utils.isDefined(data) ? '$ <span class="money_format">' + parseFloat(data).toFixed(2) + '</span>' : null;
                }
            },
            {
                title: "Monto moratorio",
                data: "monto_moratorio",
                render: function(data, type, row) {
                    return utils.isDefined(data) ? '$ <span class="money_format">' + parseFloat(data).toFixed(2) + '</span>' : null;
                }
            },
            {
                title: "Estatus abono",
                data: "estatus_abono"
            },
            {
                title: "CFDI",
                render: function(data, type, row) {
                    return utils.isDefined(row.clave_cfdi) ? row.clave_cfdi + ' - ' + row.descripcion_cfdi : null;
                }
            },
            {
                title: "Acciones",
                render: function(data, type, row) {
                    parcialidad = parcialidad + 1;

                    var btn_enganche = '';
                    var btn_abonar = '';
                    var btn_comprobante = '';
                    var btn_genera_complemento = '';
                    var btn_descargar_factura = '';
                    if (row.tipo_abono_id == 1) {
                        if (row.estatus_abono_id == 1 || row.estatus_abono_id == 2) {
                            var btn_enganche = '<button title="Pagar enganche" onclick="open_modal_enganche(this)" data-enganche_id="' + row.id + '" class="btn btn-default"><i class="fas fa-cash-register"></i></button>';
                        } else if (row.estatus_abono_id == 3) {
                            var btn_comprobante = '<button title="Imprimir comprobante"  onclick="imprimir_comprobante(this)" data-abono_id="' + row.id + '" data-cuenta_por_cobrar_id="' + row.cuenta_por_cobrar_id + '" class="btn btn-default"><i class="fas fa-file-pdf"></i></button>';
                            var btn_genera_complemento = '<button title="Imprimir factura"  onclick="generar_complemento(this)"  data-abono_id="' + row.id + '" data-cuenta_por_cobrar_id="' + row.cuenta_por_cobrar_id + '" class="btn btn-default"><i class="fas fa-file-cogs"></i></button>';
                        }
                    } else if (row.tipo_abono_id == 2) {
                        if (row.estatus_abono_id == 1 || row.estatus_abono_id == 2) {
                            var btn_abonar = '<button title="Realizar pago" onclick="open_modal_abono(this)" data-abono_id="' + row.id + '" data-monto_moratorio="' + row.monto_moratorio + '" data-total_abono="' + row.total_abono + '" class="btn btn-default"><i class="fas fa-cash-register"></i></button>';
                        } else if (row.estatus_abono_id == 3) {
                            if (row.total_abono >= 1) {
                                var btn_comprobante = '<button title="Imprimir comprobante"  onclick="imprimir_comprobante(this)" data-cuenta_por_cobrar_id="' + row.cuenta_por_cobrar_id + '"  data-abono_id="' + row.id + '" class="btn btn-default"><i class="fas fa-file-pdf text-danger"></i></button>';
                                var btn_genera_complemento = '<button title="Generar complemento"  onclick="generar_complemento(this)"  data-abono_id="' + row.id + '" data-cuenta_por_cobrar_id="' + row.cuenta_por_cobrar_id + '" data-parcialidad="' + parcialidad + '" class="btn btn-default"><i class="fas fa-cogs text-primary"></i></button>';
                            }
                        } else if (row.estatus_abono_id == 4) {
                            var btn_descargar_factura = '<button title="Descarga complemento"  onclick="descargar_complemento(this)"  data-abono_id="' + row.id + '" data-cuenta_por_cobrar_id="' + row.cuenta_por_cobrar_id + '" class="btn btn-default"><i class="fas fa-folder text-primary"></i></button>';
                        }
                    }

                    return btn_enganche + ' ' + btn_abonar + ' ' + btn_comprobante + ' ' + btn_genera_complemento + ' ' + btn_descargar_factura
                }
            },
        ],
        "createdRow": function(row, data, dataIndex) {
            switch (data['estatus_abono_id']) {
                case 2:
                    $(row).find('td:eq(0)').css('background-color', '#f6ffa4');
                    break;
                case 3:
                    $(row).find('td:eq(0)').css('background-color', '#8cdd8c');
                    if (data['total_abono'] < 1) {
                        $(row).hide();
                    }
                    break;
                default:
                    break;
            }
        }

    });
}

function obtenerFechaMostrar(fecha) {
    const dia = 2,
        mes = 1,
        anio = 0;
    fecha = fecha.split('T');
    fecha = fecha[0].split('-');
    return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
}

$("#pago").on('blur', function() {
    let pago = $("#pago").val().replace(",", "");;
    let total_pago = $("#total_pago").val().replace(",", "");

    if (parseFloat(pago) < parseFloat(total_pago)) {
        toastr.error("El pago es menor al total a abonar")
        $("#btn-modal-abonar").attr('disabled', true);
        $("#pago").val(0);
    } else {
        let cambio = parseFloat(pago) - parseFloat(total_pago);
        $("#cambio").val(cambio.toFixed(2));
        $("#btn-modal-abonar").attr('disabled', false);
        //$("#cambio").mask("#,##0.00");

    }
});

$("#pago_enganche").on('blur', function() {
    let pago = $("#pago_enganche").val().replace(",", "");;
    let total_pago = $("#total_pago_enganche").val().replace(",", "");
    if (parseFloat(pago) < parseFloat(total_pago)) {
        toastr.error("El pago es menor al total del enganche")
        $("#btn-modal-enganche").attr('disabled', true);
        $("#pago_enganche").val(0);
    } else {
        let cambio = parseFloat(pago) - parseFloat(total_pago);

        $("#cambio_enganche").val(cambio.toFixed(2));
        $("#btn-modal-enganche").attr('disabled', false);
    }
});

let form_abono = function() {
    let moratorio = $("#monto_moratorio").val() >= 1 ? $("#monto_moratorio").val().replace(",", "") : 0;
    let total = (parseFloat($("#total_pago").val().replace(",", "")) - parseFloat(moratorio));
    return {
        cuenta_por_cobrar_id: cuenta_por_cobrar_id,
        tipo_abono_id: document.getElementById("tipo_abono_id").value,
        total_pago: total,
        tipo_pago_id: document.getElementById("tipo_pago_id").value,
        cfdi_id: document.getElementById("cfdi_id").value,
        caja_id: document.getElementById("caja_id").value,
        banco_id: document.getElementById("banco_id").value,
        fecha_pago: document.getElementById("fecha_pago").value,
        estatus_abono_id: 3
    };
}

$("#btn-modal-abonar").on('click', function() {
    let total_pago = $("#total_pago").val().replace(",", "");;
    let minimo_abonar = $("#minimo_abonar").val().replace(",", "");;
    if (parseFloat(minimo_abonar) > parseFloat(total_pago)) {
        toastr.error("El monto minimo abonar es de: " + minimo_abonar);
        return false;
    }
    $(".invalid-feedback").html("");
    toastr.info("Realizando abono..");
    $.isLoading({
        text: "Realizando abono...."
    });
    let abono_id = $("#abono_id").val();
    ajax.put(`api/abonos-por-cobrar/${abono_id}`, form_abono(), function(response, headers) {
        if (headers.status == 400) {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        } else {
            $.isLoading("hide");
            utils.displayWarningDialog('Cuenta actualizada correctamente', "success", function(data) {
                $('#modal-abonar').modal('hide');
                $.isLoading({
                    text: "Procesando información...."
                });
                window.location.reload();
            })
        }
    })
})

let form_enganche = function() {
    return {
        cuenta_por_cobrar_id: cuenta_por_cobrar_id,
        tipo_abono_id: document.getElementById("tipo_abono_id_enganche").value,
        total_pago: document.getElementById("total_pago_enganche").value,
        tipo_pago_id: document.getElementById("tipo_pago_id_enganche").value,
        cfdi_id: document.getElementById("cfdi_id_enganche").value,
        caja_id: document.getElementById("caja_id_enganche").value,
        fecha_pago: document.getElementById("fecha_pago_enganche").value,
        folio_id: document.getElementById('folio_id').value,
        banco_id: document.getElementById("banco_id_enganche").value,
        estatus_abono_id: 3
    };
}

$("#btn-modal-enganche").on('click', function() {
    $(".invalid-feedback").html("");
    toastr.info("Realizando enganche..");
    $.isLoading({
        text: "Realizando enganche...."
    });
    let enganche_id = $("#enganche_id").val();
    ajax.put(`api/abonos-por-cobrar/${enganche_id}`, form_enganche(), function(response, headers) {
        if (headers.status == 400) {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        }
        $.isLoading("hide");
        utils.displayWarningDialog('Enganche efectuado correctamente', "success", function(data) {
            $('#modal-enganche').modal('hide');
            $.isLoading({
                text: "Procesando información...."
            });
            window.location.reload();
        })
    })
})

function open_modal_abono(_this) {
    $('#modal-abonar').modal('show');
    $("#abono_id").val($(_this).data('abono_id'));
    let tot = $(_this).data('total_abono');
    let monto = $(_this).data('monto_moratorio');
    let moratorio = monto != null ? monto : 0;
    $("#monto_moratorio").val(monto);
    let total_pago = parseFloat(tot) + parseFloat(moratorio);
    $("#total_pago").val(total_pago.toFixed(2));
    $("#minimo_abonar").val(total_pago.toFixed(2));
}

function open_modal_enganche(_this) {
    $("#enganche_id").val($(_this).data('enganche_id'));
    $('#modal-enganche').modal('show');
}

function open_modal_genera_factura_credito() {
    $("#modal-genera-factura").modal('show');
}

function generar_factura(_this) {
    toastr.info("Procesando petición ..");
    $.isLoading({
        text: "Realizando petición ...."
    });
    ajax.post(`api/cuentas-por-cobrar/aplicar-factura-credito`, { cuenta_por_cobrar_id: $(_this).data('id') }, function(response, headers) {
        if (headers.status == 400) {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        } else {
            $.isLoading("hide");
            utils.displayWarningDialog(headers.message, "success", function(data) {
                if (response.data.cfdi) {
                    window.open(response.data.cfdi, '_blank');
                }
                $.isLoading({
                    text: "Actualizando información"
                });
                window.location.reload();
            })
        }
    })
}

function generar_complemento(_this) {
    toastr.info("Procesando petición ..");
    $.isLoading({
        text: "Realizando petición ...."
    });
    let params = {
        cuenta_por_cobrar_id: $(_this).data('cuenta_por_cobrar_id'),
        abono_id: $(_this).data('abono_id'),
        parcialidad: $(_this).data('parcialidad')
    }
    ajax.post(`api/cuentas-por-cobrar/genera-complemento`, params, function(response, headers) {
        if (headers.status == 400) {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        } else {
            console.log(response);
            $.isLoading("hide");
            utils.displayWarningDialog('Complemento creado correctamente', "success", function(data) {
                if (response.pdf) {
                    window.open(response.pdf, '_blank');
                }
                var table = $('table#tbl_abonos').DataTable();
                table.ajax.reload(null, false);
            })
        }
    })
}

function descargar_complemento(_this) {
    let params = {
        cuenta_por_cobrar_id: $(_this).data('cuenta_por_cobrar_id'),
        abono_id: $(_this).data('abono_id'),
    }
    ajax.post(`api/cuentas-por-cobrar/descargar-complemento`, params, function(response, headers) {
        if (utils.isDefined(response.data) && response.data.pdf) {
            window.open(response.data.pdf, '_blank');
        } else {
            utils.displayWarningDialog('Ocurrio un error al descargar el comprobante fiscal', "error", function(data) {});
        }
    })
}

function imprimir_comprobante_fiscal(_this) {
    ajax.get(`api/cuentas-por-cobrar/comprobante-fiscal`, { folio: $(_this).data('folio') }, function(response, headers) {
        console.log($(_this).data('folio'));
        if (utils.isDefined(response.data) && response.data.pdf) {
            window.open(response.data.pdf, '_blank');
        } else {
            utils.displayWarningDialog('Ocurrio un error al descargar el comprobante fiscal', "error", function(data) {});
        }
    })
}


function imprimir_comprobante(_this) {
    abono_id = $(_this).data('abono_id');
    window.location.href = PATH + '/caja/entradas/imprime_comprobante?abono_id=' + window.btoa(abono_id);
}

function imprimir_estado_cuenta(_this) {
    cuenta_id = $(_this).data('cuenta_por_cobrar_id');
    window.location.href = PATH + '/caja/entradas/imprime_estado_cuenta?cuenta_id=' + window.btoa(cuenta_id);
}

function validarTipoPago(_this) {
    let tipo_pago = parseInt($(_this).val());
    switch (tipo_pago) {
        case 1:
            $(".banco_pagar_id").hide();
            $(".referencia_tarjeta").hide();
            $(".referencia_cheque").hide();
            $(".banco_pagar_id_").hide();
            $(".referencia_tarjeta_").hide();
            $(".referencia_cheque_").hide();
            break;
        case 2:
            $(".banco_pagar_id").hide();
            $(".referencia_tarjeta").hide();
            $(".referencia_cheque").show();
            $(".banco_pagar_id_").hide();
            $(".referencia_tarjeta_").hide();
            $(".referencia_cheque_").show();
            break;
        default:
            $(".banco_pagar_id").show();
            $(".referencia_tarjeta").show();
            $(".referencia_cheque").hide();
            $(".banco_pagar_id_").show();
            $(".referencia_tarjeta_").show();
            $(".referencia_cheque_").hide();
            break;
    }
}

function openDatosCliente(_this) {
    $.isLoading({
        text: "Realizando petición ...."
    });
    toastr.info("Obteniendo información");
    cliente_id = $(_this).data('cliente_id');
    setTimeout(() => {
        ajax.get("api/clientes/" + cliente_id, {}, function(response, header) {
            if (header.status == 200) {
                $.each(response.shift(), function(indexInArray, valueOfElement) {
                    $("[name='" + indexInArray + "']").val(valueOfElement);
                });
                $("#cliente_id").val(cliente_id);
            }
        });
        $("#datos_credito").show();
        ajax.post('api/clientes/tiene-credito', { id: cliente_id }, function(response, header) {
            if (header.status == 200) {
                $("#aplica_credito").val(utils.isDefined(response.aplica_credito) && response.aplica_credito == 1 ? 'Tiene Crédito' : 'Sin Crédito');
                $("#limite_credito").val(utils.isDefined(response.limite_credito) ? parseFloat(response.limite_credito).toFixed(2) : '');
                $("#plazo_credito").val(utils.isDefined(response.plazo_credito) ? response.plazo_credito : '');
                $("#credito_actual").val(utils.isDefined(response.credito_actual) ? parseFloat(response.credito_actual).toFixed(2) : '');
            }
        }, false, false);
        $.isLoading("hide");
        $("#modal-editar-cliente").modal("show");
    }, 1000);
}

this.inicializaTabla();
$("#proveedor_id").select2();
// $('.money_format').mask("#,##0.00", { reverse: true });
$(".select2").select2();
ajax.getCatalogo('tipo-pago', $("select[name*='tipo_pago']"), true);
ajax.getCatalogo('cfdi', $("select[name*='cfdi_id']"), true);
ajax.getCatalogo('catalogo-caja', $("select[name*='caja_id']"), true);
ajax.getCatalogo('catalogo-cuentas/cuentas-bancarias', $("select[name*='banco_id']"), true);
ajax.getCatalogo('catalogo-bancos', $("select[name*='banco_pagar_id']"), true);