$("#porcentaje_1").on('blur', function() {
    let porcentaje = $("#porcentaje_1").val();

    if (!$.isNumeric(porcentaje)) {
        toastr.error("El campo no es un número");
        return false;
    }
    let total_aplicar = $("#total_aplicar_1").val().replace(",", "");
    let cantidad_descontada = total_aplicar * porcentaje / 100;
    //let iva_recalculado = cantidad_descontada * 0.16;
    let iva_recalculado = 0;
    let total_descuento = total_aplicar - cantidad_descontada;
    $("#total_descuento_1").val(parseFloat(total_descuento + iva_recalculado).toFixed(2));
    $("#cantidad_descontada_1").val(parseFloat(cantidad_descontada + iva_recalculado).toFixed(2));
});

$("#porcentaje_2").on('blur', function() {
    let porcentaje = $("#porcentaje_2").val();

    if (!$.isNumeric(porcentaje)) {
        toastr.error("El campo no es un número");
        return false;
    }
    let total_aplicar = $("#total_aplicar_2").val().replace(",", "");
    let cantidad_descontada = total_aplicar * porcentaje / 100;
    let total_descuento = total_aplicar - cantidad_descontada;
    //let iva_recalculado = cantidad_descontada * 0.16;
    let iva_recalculado = 0;
    $("#total_descuento_2").val(parseFloat(total_descuento + iva_recalculado).toFixed(2));
    $("#cantidad_descontada_2").val(parseFloat(cantidad_descontada + iva_recalculado).toFixed(2));
});

$("#porcentaje_3").on('blur', function() {
    let porcentaje = $("#porcentaje_3").val();

    if (!$.isNumeric(porcentaje)) {
        toastr.error("El campo no es un número");
        return false;
    }
    let total_aplicar = $("#total_aplicar_3").val().replace(",", "");
    let cantidad_descontada = total_aplicar * porcentaje / 100;
    let total_descuento = total_aplicar - cantidad_descontada;
    //let iva_recalculado = cantidad_descontada * 0.16;
    let iva_recalculado = 0;
    $("#total_descuento_3").val(parseFloat(total_descuento + iva_recalculado).toFixed(2));
    $("#total_iva").val(parseFloat($("#total_descuento_3").val() * 1.16).toFixed(2));
    $("#cantidad_descontada_3").val(parseFloat(cantidad_descontada + iva_recalculado).toFixed(2));
});

$("#btn-aplicar-1").on('click', function() {
    $.isLoading({
        text: "Realizando petición ...."
    });
    dataForm = {
        folio_id: $("#folio_id").val(),
        total_aplicar: document.getElementById("total_aplicar_1").value.replace(",", ""),
        tipo_descuento: 1,
        porcentaje: document.getElementById("porcentaje_1").value,
        cantidad_descontada: document.getElementById("cantidad_descontada_1").value.replace(",", ""),
        total_descuento: document.getElementById("total_descuento_1").value.replace(",", ""),
        comentario: document.getElementById("comentario_1").value,
        fecha: strDate
    }
    ajax.post(`api/descuentos/guardar-descuento`, dataForm, function(response, headers) {
        if (headers.status == 200 || headers.status == 201) {
            $.isLoading("hide");
            utils.displayWarningDialog(headers.message, "success", function(data) {
                $.isLoading({
                    text: "Procesando datos y recuperando ...."
                });
                window.location.reload();
            })
        } else {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        }
    })
});

$("#btn-aplicar-2").on('click', function() {
    $.isLoading({
        text: "Realizando petición ...."
    });
    dataForm = {
        folio_id: $("#folio_id").val(),
        total_aplicar: document.getElementById("total_aplicar_2").value.replace(",", ""),
        tipo_descuento: 2,
        porcentaje: document.getElementById("porcentaje_2").value,
        cantidad_descontada: document.getElementById("cantidad_descontada_2").value.replace(",", ""),
        total_descuento: document.getElementById("total_descuento_2").value.replace(",", ""),
        comentario: document.getElementById("comentario_2").value,
        fecha: strDate
    }

    ajax.post(`api/descuentos/guardar-descuento`, dataForm, function(response, headers) {
        if (headers.status == 200 || headers.status == 201) {
            $.isLoading("hide");
            utils.displayWarningDialog(headers.message, "success", function(data) {
                $.isLoading({
                    text: "Procesando datos y recuperando ...."
                });
                window.location.reload();
            })
        } else {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        }
    })
});
$("#btn-aplicar-3").on('click', function() {
    $.isLoading({
        text: "Realizando petición ...."
    });
    dataForm = {
        folio_id: $("#folio_id").val(),
        total_aplicar: document.getElementById("total_aplicar_3").value.replace(",", ""),
        tipo_descuento: 3,
        porcentaje: document.getElementById("porcentaje_3").value,
        cantidad_descontada: document.getElementById("cantidad_descontada_3").value.replace(",", ""),
        total_descuento: document.getElementById("total_descuento_3").value.replace(",", ""),
        comentario: document.getElementById("comentario_3").value,
        fecha: strDate
    }

    ajax.post(`api/descuentos/guardar-descuento`, dataForm, function(response, headers) {
        if (headers.status == 200 || headers.status == 201) {
            $.isLoading("hide");
            utils.displayWarningDialog(headers.message, "success", function(data) {
                $.isLoading({
                    text: "Procesando datos y recuperando ...."
                });
                window.location.reload();
            })
        } else {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        }
    })
});

// $("#btn-descuento-general").on('click', function() {
//     let porcentaje = $("#descuento_general").val();
//     console.log(porcentaje);
//     if (!$.isNumeric(porcentaje)) {
//         toastr.error("El campo no es un número");
//         return false;
//     }
//     let total_refacciones_new = 0;
//     let total_mano_obra_new = 0;

//     let total_refacciones = $("#total_aplicar_1").val().replace(",", "");
//     let total_mano_obra = $("#total_aplicar_2").val().replace(",", "");

//     total_refacciones_new = total_refacciones * porcentaje / 100;
//     total_mano_obra_new = total_mano_obra * porcentaje / 100;

//     $("#total_aplicar_1").val(parseFloat(total_refacciones_new).toFixed(2));
//     $("#total_aplicar_2").val(parseFloat(total_mano_obra_new).toFixed(2));
//     let total_aplicar = total_refacciones + total_mano_obra;
//     let total_descuento = total_refacciones_new + total_mano_obra_new;
//     let cantidad_descontada = total_aplicar - total_descuento;
//     dataForm = {
//         folio_id: $("#folio_id").val(),
//         total_aplicar: total_aplicar,
//         tipo_descuento: 3,
//         porcentaje: porcentaje,
//         cantidad_descontada: cantidad_descontada,
//         total_descuento: total_descuento,
//         comentario: 'general',
//         fecha: strDate
//     }
//     return false;
//     ajax.post(`api/descuentos/descuento-general`, dataForm, function(response, headers) {
//         if (headers.status == 200 || headers.status == 201) {
//             $.isLoading("hide");
//             utils.displayWarningDialog(headers.message, "success", function(data) {
//                 $.isLoading({
//                     text: "Procesando datos y recuperando ...."
//                 });
//                 window.location.reload();
//             })
//         } else {
//             $.isLoading("hide");
//             return ajax.showValidations(headers);
//         }
//     })
// });