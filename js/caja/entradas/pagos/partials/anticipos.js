function crearTabla() {
    $('#table_anticipos').dataTable(this.configuracionTabla());
}

function configuracionTabla() {
    let estatus_cuenta_id = $("#estatus_cuenta_id").val();

    return {
        language: {
            url: PATH_LANGUAGE
        },
        order: [
            [0, 'asc']
        ],
        searching: false,
        bInfo: false,
        searching: false,
        bLengthChange: false,
        columns: [{
                title: "Folio anticipo",
                render: function(data, type, row) {
                    return '<small><b>' + ('0000' + row.folio_id).slice(-5) + '</small>';
                },
            },
            {
                title: "Tipo",
                render: function(data, type, row) {
                    return '<small>' + row.proceso + '</small>';
                },
            },
            {
                title: "Comentario",
                render: function(data, type, row) {
                    return '<small>' + row.comentario + '</small>';
                },
            },
            {
                title: "Total",
                data: 'total',
                render: function(data, type, row) {
                    return '<small class="money_format"><b>' + parseFloat(data).toFixed(2) + '</b></small>';
                },
            },
            {
                title: "Estatus",
                render: function(data, type, row) {
                    let badge = '';
                    switch (parseInt(row.estatus_id)) {
                        case 1:
                            badge = "badge-info";
                            break;
                        case 2:
                            badge = "badge-success";
                            break;
                        case 3:
                            badge = "badge-danger";
                            break;

                        default:
                            break;
                    }
                    return '<div class="badge ' + badge + '"><small>' + row.estatus + '</small></div>';
                },
            },
            {
                title: "Fecha creación",
                render: function(data, type, row) {
                    return '<small>' + this.obtenerFechaMostrar(row.fecha) + '</small>';
                },
            },
            {
                title: "Acciones",
                render: function(data, type, row) {
                    let btn_utilizar = '';
                    if (row.estatus_id == 1 && estatus_cuenta_id == 5) {
                        btn_utilizar = '<button title="Utilizar anticipo" onclick="aplicarAnticipo(this)" data-total_anticipo="' + row.total + '" data-anticipo_id="' + row.id + '" type="button" class="btn btn-success"><i class="fa fa-cash-register fa-1x"></i></button>';
                    }
                    return btn_utilizar;
                }
            },
        ],

    }
}

function getBusqueda() {
    let params = {
        'cliente_id': $("#cliente_id_").val(),
        'tipo_proceso_id': $("#tipo_proceso_id").val(),
    }
    ajax.get("api/anticipos/get-filter", params, function(response, header) {
        var listado = $('table#table_anticipos').DataTable();
        listado.clear().draw();
        if (response && response.length > 0) {
            response.forEach(listado.row.add);
            listado.draw();
        } else {

        }
    });
}

function obtenerFechaMostrar(fecha) {
    const dia = 2,
        mes = 1,
        anio = 0;
    fecha = fecha.split(' ');
    fecha = fecha[0].split('-');
    return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
}

function aplicarAnticipo(_this) {

    total_pagar = document.getElementById("importe").value.replace(",", "");
    total_anticipo = $(_this).data('total_anticipo');

    if (parseFloat(total_anticipo) > parseFloat(total_pagar)) {
        utils.displayWarningDialog('El anticipo es mayor al total de la compra', "warning", function(data) {
            return false;
        })
    } else {
        Swal.fire({
            icon: 'warning',
            title: '¿Esta seguro de aplicar el anticipo?',
            showCancelButton: true,
            confirmButtonText: "Si",
            cancelButtonText: "No"
        }).then((result) => {
            if (result.value) {
                let params = {
                    'folio_id': $("#folio_id").val()
                }
                ajax.put('api/anticipos/aplicar-anticipo/' + $(_this).data('anticipo_id'), params, function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    utils.displayWarningDialog('Anticipo aplicado correctamente', "success", function(data) {
                        $.isLoading({
                            text: "Procesando datos y recuperando ...."
                        });
                        window.location.reload();
                    })
                })
            }
        });
    }
}

this.crearTabla();
this.getBusqueda();