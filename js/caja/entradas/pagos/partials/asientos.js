crearTablaAsientos();
getBusquedaAsientos();

$("#btn-agregar").on('click', function() {
    if ($('small.form-text.text-danger').length > 0) {
        $('small.form-text.text-danger').each(function() {
            $(this).empty();
        });
    }
    $.isLoading({
        text: "Realizando petición ...."
    });
    let dataForm = {
        concepto: document.getElementById("referencia").value,
        total_pago: document.getElementById("total_pago").value.replace(",", ""),
        cuenta_id: document.getElementById("cuenta_id").value,
        tipo_asiento_id: document.getElementById("tipo_asiento_id").value,
        folio_id: document.getElementById("folio_id").value,
        clave_poliza: 'CG',
        cliente_id: document.getElementById("cliente_id").value,
        estatus: 'POR_APLICAR',
        departamento: 3
    }

    let total_pago = $("#total_pago").val();
    let importe = $("#importe").val();
    if (parseFloat(total_pago) > parseFloat(importe)) {
        toastr.error("El total de pago no puede ser mayor al importe de la cuenta " + importe)
        return false;
    }
    ajax.post(`api/asientos/contabilidad`, dataForm, function(response, headers) {
        if (headers.status == 200 || headers.status == 201) {
            $.isLoading("hide");
            utils.displayWarningDialog('Asiento registrado correctamente', "success", function(data) {
                $("#cuenta_id").val('');
                $("#total_pago").val('');
                $("#tipo_asiento_id").val('');
                $("#fecha_pago").val('');
                $("#total_pago").trigger('change');
                $("#cuenta_id").trigger('change');
                $("#tipo_asiento_id").trigger('change');
                getBusquedaAsientos();
            })
        } else {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        }
    })
});

function crearTablaAsientos() {
    $('table#tbl_asientos').dataTable(this.configuracionTablaAsientos());
}

function configuracionTablaAsientos() {
    return {
        language: {
            url: PATH_LANGUAGE
        },
        bInfo: false,
        paging: false,
        order: [
            [0, 'asc']
        ],
        columns: [{
                title: "Poliza",
                render: function(data, type, row) {
                    return '<small>' + row.poliza + '</small>';
                },
            },
            {
                title: "Cuenta",
                render: function(data, type, row) {
                    return '<small><b>' + row.cuenta + '</b><br/>' + row.cuenta_descripcion + '</small>';
                },
            },
            {
                title: "Departamento",
                render: function(data, type, row) {
                    return '<small>' + row.departamento_descripcion + '</small>';
                },
            },
            {
                title: "Cargo",
                render: function(data, type, row) {
                    return '<small>' + parseFloat(row.cargo).toFixed(2) + '</small>';
                },
            },
            {
                title: "Abono",
                render: function(data, type, row) {
                    return '<small>' + parseFloat(row.abono).toFixed(2) + '</small>';
                },
            },

            {
                title: "Estatus",
                render: function(data, type, row) {
                    let badge = '';
                    switch (row.estatus_id) {
                        case 'POR_APLICAR':
                            badge = "badge-warning";
                            break;
                        case 'APLICADO':
                            badge = "badge-success";
                            break;
                        case 'ANULADO':
                            badge = "badge-danger";
                            break;

                        default:
                            break;
                    }
                    return '<div class="badge ' + badge + '"><small>' + row.estatus_id + '</small></div>';
                },
            },
            {
                title: "Fecha registro",
                render: function(data, type, row) {
                    return '<small>' + row.fecha_creacion + '</small>';
                },
            },
        ],
    }
}

function getBusquedaAsientos() {
    let total_cargos = 0;
    let total_abonos = 0;
    let total_asientos = 0;
    var params = $.param({
        'folio': $("#folio_id").val(),
        'nomenclatura': $("#tipo_poliza option:selected").val()
    });
    $.ajax({
        dataType: "json",
        type: 'GET',
        data: params,
        url: API_CONTABILIDAD + '/asientos/api/detalle',
        success: function(response, _, _) {
            var listado = $('table#tbl_asientos').DataTable();
            listado.clear().draw();
            if (response && response.data.length > 0) {
                response.data.forEach(listado.row.add);
                listado.draw();
            }
            $('table#tbl_asientos tbody tr').each(function() {
                total_cargos += parseFloat($(this).find("td").eq(3).text());
                total_abonos += parseFloat($(this).find("td").eq(4).text());
                $(this).find("td").eq(3).addClass('money_format');
                $(this).find("td").eq(4).addClass('money_format');
            });
            $("#total_cargos").html('$<span class="money_format">' + parseFloat(total_cargos).toFixed(2) + '</span>');
            $("#total_abonos").html('$<span class="money_format">' + parseFloat(total_abonos).toFixed(2) + '</span>');
        }
    });
}

// $(".select2").select2();
// $('.money_format').mask("#,##0.00", {
//     reverse: true
// });