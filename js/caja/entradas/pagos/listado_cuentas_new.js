function inicializaTabla() {
    $('#tbl_cxc').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        processing: true,
        serverSide: true,
        searching: false,
        bFilter: false,
        lengthChange: false,
        bInfo: true,
        order: [
            [0, 'asc']
        ],
        "ajax": {
            url: PATH_API + "api/cuentas-por-cobrar",
            type: 'GET',
        },
        /*footerCallback: function(row, data, start, end, display) {
            var api = this.api(),
                data;
            var intVal = function(i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
            };

            total_pago = api
                .column(6)
                .data()
                .reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            $("#total_pagar_tabla").html('$ <span class="money_format">' + total_pago.toFixed(2) + '</span>');
        },*/
        columns: [{
                title: "#",
                render: function(data, type, row) {
                    return '<small>' + row.id + '</small>'
                }
            },
            {
                title: "Folio",
                render: function(data, type, row) {
                    return '<b>' + row.folio + '</b>'
                }
            },
            {
                title: "Concepto",
                render: function(data, type, row) {
                    return '<small>' + row.concepto + '</small>'
                }
            },
            {
                title: "Cliente",
                render: function(data, type, row) {
                    let numero_cliente = utils.isDefined(row.numero_cliente) ? row.numero_cliente : 's/n';
                    let nombre_completo = row.nombre_cliente + ' ' + row.apellido_paterno + ' ' + row.apellido_materno;
                    return '<small><b>' + numero_cliente + '</b> - ' + nombre_completo + '</small>'
                }
            },
            {
                title: "Número orden",
                render: function(data, type, row) {
                    return '<small> ' + utils.isDefined(row.numero_orden) ? row.numero_orden : '-' + '</small>'
                }
            },
            {
                title: "Fecha",
                render: function(data, type, row) {
                    return obtenerFechaMostrar(row.fecha)
                }
            },
            {
                title: "Monto a pagar",
                data: 'total',
                render: function(data, type, row) {
                    return data >= 1 ? '$ <small class="money_format">' + parseFloat(data).toFixed(2) + '</small>' : '';
                }
            },
            {
                title: "Forma pago",
                render: function(data, type, row) {
                    return '<small>' + row.tipo_forma_pago + '</small>'
                }
            },
            {
                title: '-',
                width: '120px',
                render: function(data, type, row) {
                    let btn_tipo = '';
                    let btn_detalle = '';
                    let btn_asientos = '';
                    let btn_factura = '';
                    let btn_cancelar = '';
                    let btn_estado_cuenta = '';
                    let btn_comprobante_fiscal = '';
                    let btn_eliminar = '';
                    let btn_factura_multiple = '';
                    let btn_devolucion_factura = '';
                    let btn_devolver_servicio = '';
                    let btn_devolver_mostrador = '';

                    btn_eliminar = '<button title="Eliminar registro" onclick="eliminarRegistro(this)" data-id="' + row.id + '" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>';
                    if (row.estatus_cuenta_id == 1) { //VENTA NUEVA
                        btn_tipo = '<button title="Comenzar venta" onclick="tipoPago(this)" data-folio_id="' + row.folio_id + '" class="btn btn-dark btn-sm"><i class="fas fa-play"></i></button>';
                    }
                    if (row.tipo_forma_pago_id == 1) {
                        if (row.estatus_cuenta_id == 5 || row.estatus_cuenta_id == 3) { //VENTA PROCESO PASAR A PAGAR
                            btn_detalle = '<button title="Pagar cuenta" onclick="detallePago(this)" data-id="' + row.id + '" class="btn btn-warning btn-sm"><i class="fas fa-cash-register "></i></button>';
                        }
                        if (row.estatus_cuenta_id == 2) { // GENERAR FACTURA
                            if (row.tipo_forma_pago_id == 1) {
                                btn_factura = '<button title="Generar factura simple" onclick="generar_factura(this)" data-id="' + row.id + '" class="btn btn-primary btn-sm"><i class="fas fa-cog"></i></button>';
                                btn_factura_multiple = '<button title="Generar factura multiple" onclick="goToFacturaMultiple(this)" data-id="' + row.id + '" class="btn btn-success btn-sm"><i class="fa-solid fa-cogs"></i></button>';
                            }
                        }
                        if (row.estatus_cuenta_id == 2 || row.estatus_cuenta_id >= 6) { // SI SE ENCUENTRA PAGADO PUEDES VER EL ESTADO DE CUENTA
                            btn_estado_cuenta = '<button title="Estado de cuenta" onclick="imprimir_estado_cuenta(this)" data-id="' + row.id + '" class="btn btn-danger btn-sm"><i class="fas fa-file-pdf"></i></button>';
                            btn_detalle = '<button title="Detalle cuenta" onclick="detallePago(this)" data-id="' + row.id + '" class="btn btn-primary btn-sm"><i class="fas fa-book "></i></button>';
                        }
                        if (row.estatus_cuenta_id == 6) { // VER COMPROBANTE FISCAL
                            btn_comprobante_fiscal = '<button title="Comprobante fiscal digital" onclick="imprimir_comprobante_fiscal(this)" data-folio_id="' + row.folio_id + '" class="btn btn-primary btn-sm"><i class="fas fa-folder"></i></button>';
                            btn_devolucion_factura = '<button title="Cancelar factura" onclick="cancelar_factura(this)" data-id="' + row.id + '" data-folio_id="' + row.folio_id + '" class="btn btn-danger btn-sm"><i class="fas fa-times"></i></button>';
                        }
                        if (row.estatus_cuenta_id == 8) { // VER COMPROBANTE FISCAL
                            btn_factura_multiple = '<button title="Factura multiple" onclick="goToFacturaMultiple(this)" data-id="' + row.id + '" class="btn btn-success btn-sm"><i class="fa-solid fa-folder-tree"></i></button>';
                        }
                        if (row.estatus_cuenta_id == 9) { // DEVOLVER SERVICIO
                            if (row.tipo_proceso_id == 8 || row.tipo_proceso_id == 9) {
                                btn_devolver_servicio = '<button title="Devolución servicio" onclick="devolucion_servicio(this)" data-id="' + row.id + '" class="btn btn-primary btn-sm"><i class="fa-solid fa-tag"></i></button>';
                            } else if (row.tipo_proceso_id == 1) {
                                btn_devolver_mostrador = '<button title="Devolución mostrador" onclick="devolucion_mostrador(this)" data-id="' + row.id + '" class="btn btn-primary btn-sm"><i class="fa-solid fa-tag"></i></button>';
                            }
                        }
                    } else {
                        if (row.estatus_cuenta_id == 6 || row.estatus_cuenta_id == 3 || row.estatus_cuenta_id == 5) { //VENTA PROCESO PASAR A PAGAR
                            btn_detalle = '<button title="Pagar cuenta" onclick="detallePago(this)" data-id="' + row.id + '" class="btn btn-warning btn-sm"><i class="fas fa-cash-register "></i></button>';
                        }
                        if (row.estatus_cuenta_id == 2) { // SI SE ENCUENTRA PAGADO PUEDES VER EL ESTADO DE CUENTA
                            btn_estado_cuenta = '<button title="Estado de cuenta" onclick="imprimir_estado_cuenta(this)" data-id="' + row.id + '" class="btn btn-danger btn-sm"><i class="fas fa-file-pdf"></i></button>';
                            btn_detalle = '<button title="Detalle cuenta" onclick="detallePago(this)" data-id="' + row.id + '" class="btn btn-primary btn-sm"><i class="fas fa-book "></i></button>';
                        }
                    }
                    return btn_tipo + ' ' + btn_detalle + ' ' + btn_estado_cuenta + ' ' + btn_factura + ' ' + btn_factura_multiple + ' ' + btn_cancelar + ' ' + btn_comprobante_fiscal + ' ' + btn_devolucion_factura + ' ' + btn_devolver_servicio + ' ' + btn_devolver_mostrador + ' ' + btn_eliminar

                }
            }
        ],
        "createdRow": function(row, data, dataIndex) {
            $(row).find('td:eq(0)').attr('data-title', data['proceso']);

            if (data['asientos']) {
                $(row).find('td:eq(1)').attr('data-title', data['estatus_cuenta']);
            } else if (!data['asientos'] && data['tipo_proceso_id'] != 1) {
                $(row).find('td:eq(1)').attr('data-title', 'No se realizo la conexión con contabilidad');
            } else if (!data['asientos'] && data['tipo_proceso_id'] == 1 && data['estatus_cuenta_id'] == 5) {
                $(row).find('td:eq(1)').attr('data-title', 'No se realizo la conexión con contabilidad');
            }

            if (!data['asientos'] && data['tipo_proceso_id'] != 1) {
                $(row).find('td:eq(1)').css('background-color', '#f6a5a5');
            }
            if (!data['asientos'] && data['tipo_proceso_id'] == 1 && data['estatus_cuenta_id'] == 5) {
                $(row).find('td:eq(1)').css('background-color', '#f6a5a5');
            }
            switch (data['estatus_cuenta_id']) {
                case 2: // PAGO LIQUIDADO AMARILLO
                    $(row).find('td:eq(1)').css('color', '#bda500');
                    break;
                case 3: // PAGO ATRASADO ROJO
                    $(row).find('td:eq(1)').css('color', '#e37f7f');
                    break;
                case 4: // PAGO CANCELADO ROJO
                    $(row).find('td:eq(1)').css('color', '#e37f7f');
                    break;
                case 5: // PAGO EN PROCESO NARANJA
                    $(row).find('td:eq(1)').css('color', '#f9841f');
                    break;
                case 6: // FACTURADO VERDE
                    $(row).find('td:eq(1)').css('color', '#39ca07');
                    break;
                case 7: // FACTURADO VERDE
                    $(row).find('td:eq(1)').css('color', '#39ca07');
                    break;
                case 9: // PAGO CANCELADO ROJO
                    $(row).find('td:eq(1)').css('color', '#e37f7f');
                    break;
                case 10: // DEVOLUCION SERVICIO
                    $(row).find('td:eq(1)').css('color', '#e37f7f');
                    break;
                default:
                    break;
            }
        }
    });
}

function iniciarVenta(_this) {
    var cuenta_por_cobrar = $(_this).data('id');
    utils.displayWarningDialog("¿Desea comenzar el proceso de pago?", "warning", function(data) {
        if (data.value) {
            ajax.put(`api/cuentas-por-cobrar/updatestatus/` + cuenta_por_cobrar, { estatus_cuenta_id: 5 }, function(response, header) {
                if (header.status != 400) {
                    $.isLoading({
                        text: "Aplicando asientos poliza  ...."
                    });
                    toastr.info('Aplicando asientos poliza  ....');
                    if ($(_this).data('tipo_proceso_id') == 1) {
                        if ($(_this).data('tipo_forma_pago_id') == 2) {
                            ajax.post(`api/asientos/poliza-venta-mostrador-credito`, {
                                cuenta_por_cobrar_id: cuenta_por_cobrar
                            }, function(response, headers) {
                                if (headers.status == 201 || headers.status == 200) {
                                    $.isLoading("hide");
                                    utils.displayWarningDialog('Proceso realizado correctamente..', 'success', function(result) {
                                        $.isLoading({
                                            text: "Reedireccionando al pago ...."
                                        });
                                        toastr.info('Espere un momento por favor');
                                        window.location.href = PATH + 'caja/entradas/detalle_pago/' + cuenta_por_cobrar;
                                    });
                                } else {
                                    $.isLoading("hide");
                                }
                            })
                        } else {
                            ajax.post(`api/asientos/poliza-venta-mostrador`, {
                                cuenta_por_cobrar_id: cuenta_por_cobrar,
                            }, function(response, headers) {
                                if (headers.status == 201 || headers.status == 200) {
                                    $.isLoading("hide");
                                    utils.displayWarningDialog('Proceso realizado correctamente..', 'success', function(result) {
                                        $.isLoading({
                                            text: "Reedireccionando al pago ...."
                                        });
                                        toastr.info('Espere un momento por favor');
                                        window.location.href = PATH + 'caja/entradas/detalle_pago/' + cuenta_por_cobrar;
                                    });
                                } else {
                                    $.isLoading("hide");
                                }
                            })
                        }

                    } else {
                        $.isLoading("hide");
                        utils.displayWarningDialog('Proceso realizado correctamente..', 'success', function(result) {
                            $.isLoading({
                                text: "Reedireccionando al pago ...."
                            });
                            toastr.info('Espere un momento por favor');
                            window.location.href = PATH + 'caja/entradas/detalle_pago/' + cuenta_por_cobrar;
                        });
                    }
                }
            })
        }
    }, true)

}

function cancelar_factura(_this) {
    let params = {
        cuenta_por_cobrar_id: $(_this).data('id')
    }
    utils.displayWarningDialog("¿Desea cancelar la factura?", "warning", function(data) {
        if (data.value) {
            $.isLoading({
                text: "Cancelando factura ...."
            });
            ajax.get(`api/cuentas-por-cobrar/cancela-factura`, params, function(response, header) {
                if (header.status == 200) {
                    $.isLoading("hide");
                    utils.displayWarningDialog('Factura cancelada correctamente', 'success', function(result) {
                        filtrar();
                    });
                } else {
                    $.isLoading("hide");
                    return ajax.showValidations(header);
                }
            })
        }
    }, true)

}

function detallePago(_this) {
    setTimeout(() => {
        var cuenta_por_cobrar = $(_this).data('id');
        window.location.href = PATH + '/caja/entradas/detalle_pago/' + cuenta_por_cobrar;
    }, 200);
}

function asientos(_this) {
    setTimeout(() => {
        var cuenta_por_cobrar = $(_this).data('id');
        window.location.href = PATH + '/caja/entradas/asientos/' + cuenta_por_cobrar;
    }, 200);
}

function tipoPago(_this) {
    setTimeout(() => {
        var folio_id = $(_this).data('folio_id');
        window.location.href = PATH + '/caja/entradas/tipoPago/' + folio_id;
    }, 200);
}

function asientos_cuenta(_this) {
    setTimeout(() => {
        var cuenta_por_cobrar = $(_this).data('id');
        window.location.href = PATH + '/caja/entradas/asientos_cuenta/' + cuenta_por_cobrar;
    }, 200);
}

function goToFacturaMultiple(_this) {
    setTimeout(() => {
        var cuenta_por_cobrar = $(_this).data('id');
        window.location.href = PATH + '/caja/entradas/pago_factura/' + cuenta_por_cobrar;
    }, 200);
}

function obtenerFechaMostrar(fecha) {
    const dia = 2,
        mes = 1,
        anio = 0;
    fecha = fecha.split('T');
    fecha = fecha[0].split('-');
    return '<small>' + fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio] + '</small>'
}


function filtrar() {
    var params = $.param({
        'folio': $("#folio").val(),
        'numero_orden': $("#numero_orden").val(),
        'nombre': $("#nombre").val(),
        'apellidos': $("#apellidos").val(),
        'numero_cliente': $("#numero_cliente").val(),
        'rfc': $("#rfc").val(),
        'fecha_inicio': $("#fecha_inicio").val(),
        'fecha_fin': $("#fecha_fin").val(),
        'cliente_id': $("#cliente_id option:selected").val(),
        'estatus_cuenta_id': $("#estatus_cuenta_id option:selected").val(),
        'tipo_proceso_id': $("#tipo_proceso_id option:selected").val(),
    });

    $('#tbl_cxc').DataTable().ajax.url(PATH_API + 'api/cuentas-por-cobrar?' + params).load()
}

function limpiarfiltro() {
    $("#folio").val('');
    $("#numero_orden").val('');
    $("#nombre").val('');
    $("#apellidos").val('');
    $("#numero_cliente").val('');
    $("#rfc").val('');
    $("#fecha_inicio").val('');
    $("#fecha_fin").val();
    $("#estatus_cuenta_id").val('');
    $("#tipo_proceso_id").val('');
    var params = $.param({});

    $('#tbl_cxc').DataTable().ajax.url(PATH_API + 'api/cuentas-por-cobrar?' + params).load()
}

function openModal() {
    $('#modal-cuenta').modal('show');
    $('.select2Modal').select2({
        dropdownParent: $('#modal-cuenta')
    });
}


function imprimir_estado_cuenta(_this) {
    cuenta_id = $(_this).data('id');
    window.open(PATH + '/caja/entradas/imprime_estado_cuenta?cuenta_id=' + window.btoa(cuenta_id), '_blank');

}

function generar_factura(_this) {
    toastr.info("Procesando petición ..");
    $.isLoading({
        text: "Realizando petición ...."
    });
    ajax.post(`api/cuentas-por-cobrar/aplicar-factura`, { cuenta_por_cobrar_id: $(_this).data('id') }, function(response, headers) {
        if (headers.status == 400) {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        } else {
            $.isLoading("hide");
            utils.displayWarningDialog(headers.message, "success", function(data) {
                if (response.data.cfdi) {
                    window.open(response.data.cfdi, '_blank');
                }
                filtrar();
            })
        }
    })
}

function devolucion_servicio(_this) {
    toastr.info("Procesando petición ..");
    $.isLoading({
        text: "Realizando petición ...."
    });
    ajax.post(`api/asientos/poliza-devolucion-servicio`, { cuenta_por_cobrar_id: $(_this).data('id') }, function(response, headers) {
        if (headers.status == 400) {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        } else {
            $.isLoading("hide");
            utils.displayWarningDialog(headers.message, "success", function(data) {
                filtrar();
            })
        }
    })
}

function devolucion_mostrador(_this) {
    toastr.info("Procesando petición ..");
    $.isLoading({
        text: "Realizando petición ...."
    });
    ajax.post(`api/asientos/poliza-devolucion-mostrador`, { cuenta_por_cobrar_id: $(_this).data('id') }, function(response, headers) {
        if (headers.status == 400) {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        } else {
            $.isLoading("hide");
            utils.displayWarningDialog(headers.message, "success", function(data) {
                filtrar();
            })
        }
    })
}

function imprimir_comprobante_fiscal(_this) {

    ajax.get(`api/cuentas-por-cobrar/comprobante-fiscal`, { folio: $(_this).data('folio_id') }, function(response, headers) {
        if (utils.isDefined(response.data) && response.data.pdf) {
            window.open(response.data.pdf, '_blank');
        } else {
            utils.displayWarningDialog('Ocurrio un error al descargar el comprobante fiscal', "error", function(data) {});
        }
    })
}

let form_cuenta = function() {
    return {
        tipo_proceso: 7, //checar catalogo de procesos.
        cliente_id: document.getElementById("cliente_modal_id").value,
        estatus_cuenta_id: 1, //EN PROCESO
        tipo_forma_pago_id: 1, //Contado
        tipo_pago_id: 1, //Efectivo
        plazo_credito_id: 14, // una sola exhibicion
        concepto: document.getElementById("concepto").value,
        fecha: document.getElementById("fecha").value,
        venta_total: document.getElementById("venta_total").value,
    };
}

$("#btn-agregar_cuenta").on('click', function() {
    $("#tipo_pago_id").val();
    $("#concepto").val();
    $("#venta_total").val();

    $(".invalid-feedback").html("");
    toastr.info("Procesando petición ..");
    $.isLoading({
        text: "Realizando petición ...."
    });
    ajax.post(`api/cuentas-por-cobrar/set-complemento-cxc`, form_cuenta(), function(response, headers) {
        if (headers.status == 400) {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        } else {
            $.isLoading("hide");
            utils.displayWarningDialog('Cuenta registrada correctamente', "success", function(data) {
                $('#modal-cuenta').modal('hide')
                filtrar();
            })
        }
    })
})

function eliminarRegistro(_this) {
    let cuenta_id = $(_this).data('id');
    utils.displayWarningDialog("Desea eliminar la cuenta?", "error", function(data) {
        if (data.value) {
            ajax.delete(`api/cuentas-por-cobrar/` + cuenta_id, null, function(response, headers) {
                if (headers.status != 400) {
                    toastr.success('Registro eliminado correctamente!');
                }
                filtrar();
            })
        }
    }, true)
}

this.inicializaTabla();
$(".select2").select2();
$('.money_format').mask("#,##0.00", { reverse: true });