function guardarPagoFactura() {
    if ($('small.form-text.text-danger').length > 0) {
        $('small.form-text.text-danger').each(function() {
            $(this).empty();
        });
    }
    $.isLoading({
        text: "Realizando petición ...."
    });
    var data = {};
    data = this.procesar_form('form-procesar-factura');
    data.cuenta_por_cobrar_id = cuenta_por_cobrar_id;
    ajax.post(`api/pago-factura/guardar`, data, function(response, headers) {
        if (response.status == 'success') {
            toastr.success(response.message);
            this.getBusqueda();
            $.isLoading("hide");
        } else {
            $.isLoading("hide");
            // toastr.error(response.message);
            utils.displayWarningDialog(response.message, "error", function(data) {});

            return ajax.showValidations(headers);

        }
    })
}

function procesar_form(form_id) {
    let form = $('#' + form_id).serializeArray();
    var data = {};
    $(form).each(function(i, field) {
        data[field.name] = $.trim(field.value);
    });
    return data;
}

function crearTabla() {
    $('table#tabla_pago_factura').dataTable(this.configuracionTabla());
}

function configuracionTabla() {
    return {
        language: {
            url: PATH_LANGUAGE
        },
        order: [
            [0, 'asc']
        ],
        columns: [{
                title: '#',
                data: 'id'
            }, {
                title: "Cuenta por cobrar",
                data: "cuenta_por_cobrar_id"
            },
            {
                title: "Tipo de pago",
                data: "tipo_pago"
            },
            {
                title: "Uso de CFDI",
                data: "cfdi_descripcion"
            },
            {
                title: "Importe",
                data: "importe"
            },
            {
                title: "Acciones",
                render: function(data, type, row) {
                    var btn_descargar_factura = '';
                    var btn_eliminar = '';
                    var btn_genera_factura = '';
                    if (row.facturado == 1) {
                        btn_descargar_factura = '<button title="Descarga comprobante fiscal"  onclick="imprimir_comprobante_fiscal(this)"  data-sat_pdf=' + row.sat_pdf + '  class="btn btn-default"><i class="fas fa-folder text-primary"></i></button>';
                    } else {
                        btn_genera_factura = '<button title="Generar factura" data-id=' + row.id + '  onclick="generar_factura(this)"  class="btn btn-default"><i class="fas fa-cogs text-primary"></i></button>';
                        btn_eliminar = '<button title="Eliminar"  onclick="eliminar(this)" data-id=' + row.id + ' class="btn btn-default"><i class="fas fa-times text-danger"></i></button>';
                    }
                    return btn_genera_factura + ' ' + btn_descargar_factura + ' ' + btn_eliminar
                }
            },
        ],
    }
}

function getBusqueda() {
    $("#tipo_pago_id").val('').trigger('change');
    $("#cfdi_id").val('').trigger('change');
    $("#importe").val('');

    ajax.get("api/pago-factura/cuenta-id/" + cuenta_por_cobrar_id, false, function(response, header) {
        var listado = $('table#tabla_pago_factura').DataTable();
        listado.clear().draw();
        if (response && response.length > 0) {
            response.forEach(listado.row.add);
            listado.draw();
        }
    });
}

function generar_factura(_this) {
    toastr.info("Procesando petición ..");
    $.isLoading({
        text: "Realizando petición ...."
    });
    ajax.post(`api/cuentas-por-cobrar/aplicar-factura-multiple`, { pagos_factura_id: $(_this).data('id') }, function(response, headers) {
        if (headers.status == 400) {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        } else {
            $.isLoading("hide");
            utils.displayWarningDialog(headers.message, "success", function(data) {
                this.getBusqueda();
                if (response.facturado == 1) {
                    window.open(response.sat_pdf, '_blank');
                }
            })
        }
    })
}

function eliminar(_this) {
    let id = $(_this).data('id');
    utils.displayWarningDialog("Esta seguro de  eliminar el registro?", "error", function(data) {
        if (data.value) {
            ajax.delete(`api/pago-factura/eliminar/` + id, null, function(response, headers) {
                if (response.status == 'success') {
                    this.getBusqueda();
                    toastr.success('Registro eliminado correctamente!');
                }
            })
        }
    }, true)
}

function imprimir_comprobante_fiscal(_this) {
    if (utils.isDefined($(_this).data('sat_pdf')) && $(_this).data('sat_pdf')) {
        window.open($(_this).data('sat_pdf'), '_blank');
    } else {
        utils.displayWarningDialog('Ocurrio un error al descargar el comprobante fiscal', "error", function(data) {});
    }
}

this.crearTabla();
this.getBusqueda();
$(".select2").select2();
ajax.getCatalogo('tipo-pago', $("select[name*='tipo_pago']"), true);
ajax.getCatalogo('cfdi', $("select[name*='cfdi_id']"), true);