class crudJavascript {

    constructor() {}
    crearTabla(configTable) {
        $(this.tabla).dataTable(configTable);
    }

    openModalEditar(_this, callback) {
        app.cleanData();
        $.isLoading({
            text: "Realizando petición ...."
        });
        let id = $(_this).data('id');
        if (utils.isDefined(id) && id >= 1) {
            app.getData(id, callback);
        }
        $("#btn-editar").show();
        $("#" + this.modal).modal('show');
    }

    openModal(callback = false) {
        app.cleanData();
        $("#btn-agregar").show();
        if (callback) {
            callback();
        }
        $("#" + this.modal).modal('show');
    }

    getData(id, callback = false) {
        ajax.get(this.url_api + '/' + id, null, function(response, headers) {
            if (headers.status == 200) {
                app.identificador = id;
                app.handleFormResponse(response).then(x => {
                    if (callback) {
                        console.log("llega");
                        callback();
                    }
                    $.isLoading("hide");
                });
            }
        })
    }
    handleFormResponse(response) {
        return new Promise(function(resolve, reject) {
            let form = app.procesar_form(app.form);
            let array = [];
            let tagname = '';
            $.each(form, function(indexInArray, valueOfElement) {
                array.push(indexInArray);
            });
            $.each(response, function(indexInArray, valueOfElement) {
                if ($.inArray(indexInArray, array) !== -1) {
                    tagname = $('[name="' + indexInArray + '"]').prop("tagName");
                    if (tagname == "SELECT") {
                        $('select[name="' + indexInArray + '"]').attr('data', valueOfElement);
                    } else {
                        $('[name="' + indexInArray + '"]').val(valueOfElement);
                    }
                }
            });
            resolve(true);
        });
    }

    closeModal() {
        $("#" + this.modal).modal('hide');
        app.cleanData();
    }

    insert(_this) {
        let form = this.procesar_form(this.form);
        ajax.post(this.url_api, form, function(response, headers) {
            if (headers.status == 400) {
                return ajax.showValidations(headers);
            } else {
                app.closeModal();
                utils.displayWarningDialog(app.message_success_insert, "success", function(data) {
                    app.getBusqueda();
                })
            }
        });
    }

    update() {
        let form = this.procesar_form(this.form);
        ajax.put(this.url_api + '/' + this.identificador, form, function(response, headers) {
            if (headers.status == 400) {
                return ajax.showValidations(headers);
            } else {
                app.closeModal();
                utils.displayWarningDialog(app.message_success_update, "success", function(data) {
                    app.getBusqueda();
                })
            }
        })
    }

    cleanData() {
        ajax.destruir()
        $("#btn-agregar").hide();
        $("#btn-editar").hide();
        let tagname = '';
        let form = app.procesar_form(app.form);
        $.each(form, function(indexInArray, valueOfElement) {
            tagname = $('[name="' + indexInArray + '"]').prop("tagName");
            if (tagname == "SELECT") {
                $('select[name="' + indexInArray + '"]').attr('data', false);
                $('select[name="' + indexInArray + '"]').val('').change();
            } else {
                $('[name="' + indexInArray + '"]').val('');
            }
        });
        $("#" + app.form)[0].reset();
        app.identificador = false;
    }

    procesar_form(form_id) {
        let form = $('#' + form_id).serializeArray();
        var data = {};
        $(form).each(function(i, field) {
            data[field.name] = $.trim(field.value);
        });
        return data;
    }
    eliminar(_this) {
        utils.displayWarningDialog("¿Desea eliminar el registro?", "warning", function(data) {
            if (data.value) {
                ajax.delete(app.url_api + '/' + $(_this).data('id'), null, function(response, headers) {
                    if (headers.status == 204) {
                        utils.displayWarningDialog('Registro eliminado correctamente', 'success', function(result) {
                            app.getBusqueda();
                        });
                    }
                })
            }
        }, true)

    }

    getBusqueda(params) {
        $.ajax({
            dataType: "json",
            type: 'GET',
            data: params,
            url: PATH_API + this.url_api,
            success: function(response, _, s) {
                var listado = $(app.tabla).DataTable();
                listado.clear().draw();
                if (response && response.length > 0) {
                    response.forEach(listado.row.add);
                    listado.draw();
                }
            }
        });
    }
}
const app = new crudJavascript();