class Usuarios extends crudJavascript {
    constructor() {
        super();
        app.tabla = '#tabla-usuarios';
        app.form = 'form-modulo';
        app.modal = 'modal-usuarios';
        app.identificador = false;
        app.url_api = 'api/usuarios'
        app.message_success_insert = 'Usuario creado correctamente';
        app.message_success_update = 'Usuario actualizado correctamente';
        app.crearTabla(this.configuracionTabla());
        app.getBusqueda();
        this.user_id = false;
    }

    configuracionTabla() {
        return {
            language: {
                url: PATH_LANGUAGE
            },
            order: [
                [0, 'asc']
            ],
            columns: [{
                    title: "Nombre usuario",
                    render: function(data, type, row) {
                        return row.nombre + ' ' + row.apellido_paterno + ' ' + row.apellido_materno
                    }
                },
                {
                    title: "Teléfono",
                    data: 'telefono'
                },
                {
                    title: "Usuario",
                    data: 'usuario'
                },
                {
                    title: "Email",
                    data: 'email'
                },
                {
                    title: "Rol",
                    data: 'rol'
                },
                // {
                //     title: "Clave vendedor",
                //     data: 'clave_vendedor'
                // },
                // {
                //     title: "Clave STARS",
                //     data: 'clave_stars'
                // },
                {
                    title: "Acciones",
                    width: "80px",
                    render: function(data, type, row) {
                        let btn_editar = '<button type="button" title="Editar" onclick="app.openModalEditar(this, usuarios.callbackResponseEditar)" data-id="' + row.id + '" class="btn btn-default btn-sm"><i class="fa fa-pencil text-warning"></i></button>';
                        let btn_password = '<button type="button" title="Cambiar contraseña" data-modal="modal-password" data-form="form-cambiar-password" onclick="usuarios.openModalPassword(this)" data-id="' + row.id + '" class="btn btn-default btn-sm"><i class="fa fa-lock text-primary"></i></button>';
                        let btn_eliminar = '<button type="button" title="Eliminar" onclick="app.eliminar(this)" data-id="' + row.id + '" class="btn btn-default btn-sm"><i class="fa fa-trash text-danger"></i></button>';
                        return btn_editar + ' ' + btn_password + ' ' + btn_eliminar;
                    },

                }
            ],

        }
    }
    openModalPassword(_this) {
        $("input[name*='password']").attr('disabled', false);
        $("input[name*='password']").show();
        let form_name = 'form-cambiar-password'
        $("#" + form_name)[0].reset();
        this.user_id = $(_this).data('id');
        $("#" + $(_this).data('modal')).modal('show');

    }
    cambiar_password(_this) {
        let form_name = 'form-cambiar-password'
        let form = app.procesar_form(form_name);
        ajax.put(app.url_api + '/cambiar-password/' + this.user_id, form, function(response, headers) {
            if (headers.status == 400) {
                return ajax.showValidations(headers);
            } else {
                $("#modal-password").modal('hide');
                utils.displayWarningDialog(headers.message, "success", function(data) {
                    app.getBusqueda();
                })
            }
        });
    }

    callbackResponseNuevo() {
        ajax.getCatalogo('roles', $("select[name*='rol_id']"));
        $("input[name*='usuario']").attr('disabled', false);
        $("input[name*='password']").attr('disabled', false);
        $("input[name*='password']").show();
    }
    callbackResponseEditar() {
        ajax.getCatalogo('roles', $("select[name*='rol_id']"), true);
        $("input[name*='usuario']").attr('disabled', true);
        $("input[name*='password']").attr('disabled', true);
        $("input[name*='password']").hide();
    }

}
const usuarios = new Usuarios();