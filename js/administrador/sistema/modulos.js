class Modulos extends crudJavascript {
    constructor() {
        super();
        app.tabla = '#tabla-administrador';
        app.form = 'form-modulo';
        app.modal = 'modal-administrador';
        app.identificador = false;
        app.url_api = 'api/menu/modulos'
        app.message_success_insert = 'Modulo creado correctamente';
        app.message_success_update = 'Modulo actualizado correctamente';
        app.crearTabla(this.configuracionTabla());
        app.getBusqueda();

    }

    configuracionTabla() {
        return {
            language: {
                url: PATH_LANGUAGE
            },
            order: [
                [0, 'asc']
            ],
            columns: [{
                    title: "Orden",
                    data: 'orden',
                    width: "30px",
                },
                {
                    title: "Nombre módulo",
                    data: 'nombre'
                },

                {
                    title: "Icono",
                    data: icono,
                    width: "30px",
                    render: function(data, type, row) {
                        return '<i class="' + row.icono + '"></i>';
                    },
                },
                {
                    title: "Acciones",
                    width: "50px",
                    render: function(data, type, row) {
                        let btn_editar = '<button type="button" onclick="app.openModalEditar(this)" data-id="' + row.id + '" class="btn btn-warning"><i class="fa fa-pencil"></i> Editar</button>';
                        return btn_editar;
                    },

                }
            ],

        }
    }

}
const modulos = new Modulos();