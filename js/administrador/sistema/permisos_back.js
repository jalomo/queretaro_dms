function crearTabla() {
    $('#tbl_permisos_rol').dataTable(this.configuracionTabla());
}

function configuracionTabla() {
    return {
        language: {
            url: PATH_LANGUAGE
        },
        order: [
            [0, 'asc']
        ],
        columns: [{
                title: "#",
                width: "20px",
                render: function(data, type, row) {
                    let checked = row.checked == true ? 'checked="true"' : false;
                    if (checked) {
                        return '<div onClick="eliminar(this)" data-modulo_id="' + row.modulo_id + '"  data-seccion_id="' + row.seccion_id + '" data-submenu_id="' + row.submenu_id + '" data-vista_id="' + row.vista_id + '" class="pt-2 pb-2"><input checked="true" type="checkbox" name="modulo_id"/></div>';
                    } else {
                        return '<div onClick="insert(this)" data-modulo_id="' + row.modulo_id + '"  data-seccion_id="' + row.seccion_id + '" data-submenu_id="' + row.submenu_id + '" data-vista_id="' + row.vista_id + '" class="pt-2 pb-2"><input type="checkbox" name="modulo_id"/></div>';
                    }
                }
            },
            {
                title: "Módulo",
                render: function(data, type, row) {
                    return '<small>' + row.modulo + '</small>';
                },
            },
            {
                title: "Sección",
                render: function(data, type, row) {
                    return '<small>' + row.seccion + '</small>';
                },
            },
            {
                title: "Apartado",
                render: function(data, type, row) {
                    return '<small>' + row.submenu + '</small>';
                },
            },
            {
                title: "Vista",
                render: function(data, type, row) {
                    return '<small>' + row.vista + '</small>';
                },
            },
        ],

    }
}

function insert(_this) {
    let dataForm = {
        rol_id: $("[name*='rol_id'] option:selected").val(),
        modulo_id: $(_this).data('modulo_id'),
        seccion_id: $(_this).data('seccion_id'),
        submenu_id: $(_this).data('submenu_id'),
        vista_id: $(_this).data('vista_id')
    }
    $.isLoading({
        text: "Realizando petición ...."
    });
    ajax.post(`api/permisos`, dataForm, function(response, headers) {
        if (headers.status == 200 || headers.status == 201) {
            $.isLoading("hide");
            toastr.success(headers.message);
            this.filtrar();
        } else {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        }
    })
}

function eliminar(_this) {
    let dataForm = {
        rol_id: $("[name*='rol_id'] option:selected").val(),
        modulo_id: $(_this).data('modulo_id'),
        seccion_id: $(_this).data('seccion_id'),
        submenu_id: $(_this).data('submenu_id'),
        vista_id: $(_this).data('vista_id')
    }
    $.isLoading({
        text: "Realizando petición ...."
    });
    ajax.delete(`api/permisos/eliminar-permiso-rol`, dataForm, function(response, headers) {
        if (headers.status == 200 || headers.status == 201) {
            $.isLoading("hide");
            toastr.success(headers.message);
            this.filtrar();
        } else {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        }
    })
}

function filtrar() {
    var params = {
        modulo_id: $("[name*='modulo_id'] option:selected").val(),
        rol_id: $("[name*='rol_id'] option:selected").val()
    };
    this.getBusqueda(params);
}

function getBusqueda(params) {
    return new Promise(function(resolve, reject) {
        $.ajax({
            dataType: "json",
            type: 'GET',
            data: params,
            url: PATH_API + "api/permisos",
            success: function(response, _, _) {
                resolve(response);
                var listado = $('table#tbl_permisos_rol').DataTable();
                listado.clear().draw();
                if (response && response.length > 0) {
                    response.forEach(listado.row.add);
                    listado.draw();
                }
            }
        });
    })
}

function limpiarfiltro() {
    $("#fecha").val('');
    this.filtrar();
}

this.crearTabla();
this.filtrar();