class Secciones extends crudJavascript {
    constructor() {
        super();
        app.tabla = '#tabla-administrador';
        app.form = 'form-modulo';
        app.modal = 'modal-administrador';
        app.identificador = false;
        app.url_api = 'api/menu-secciones'
        app.message_success_insert = 'Sección creada correctamente';
        app.message_success_update = 'Sección actualizada correctamente';
        app.crearTabla(this.configuracionTabla());
        app.getBusqueda();
    }

    configuracionTabla() {
        return {
            language: {
                url: PATH_LANGUAGE
            },
            order: [
                [0, 'asc']
            ],
            columns: [{
                    title: "Módulo",
                    data: 'modulo_nombre'
                },
                {
                    title: "Sección",
                    data: 'seccion_nombre'
                },
                {
                    title: "Acciones",
                    width: "80px",
                    render: function(data, type, row) {
                        let btn_editar = '<button type="button" title="Editar" onclick="app.openModalEditar(this, secciones.callbackResponseEditar)" data-id="' + row.id + '" class="btn btn-default btn-sm"><i class="fa fa-pencil text-warning"></i></button>';
                        let btn_eliminar = '<button type="button" title="Eliminar" onclick="app.eliminar(this)" data-id="' + row.id + '" class="btn btn-default btn-sm"><i class="fa fa-trash text-danger"></i></button>';
                        return btn_editar + ' ' + btn_eliminar;
                    },

                }
            ],

        }
    }

    callbackResponseNuevo() {
        ajax.getCatalogo('menu/modulos/', $("select[name*='modulo_id']"));
    }
    callbackResponseEditar() {
        ajax.getCatalogo('menu/modulos/', $("select[name*='modulo_id']"), true);
    }



}
const secciones = new Secciones();