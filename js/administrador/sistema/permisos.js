class Permisos extends crudJavascript {
    constructor() {
        super();
        app.tabla = '#tbl_permisos_rol';
        app.url_api = 'api/permisos'
        app.crearTabla(this.configuracionTabla());
        this.filtrar();
    }

    configuracionTabla() {
        return {
            language: {
                url: PATH_LANGUAGE
            },
            order: [
                [0, 'asc']
            ],
            columns: [{
                    title: "#",
                    width: "20px",
                    render: function(data, type, row) {
                        let checked = row.checked == true ? 'checked="true"' : false;
                        if (checked) {
                            return '<div onClick="permisos.eliminar(this)" data-modulo_id="' + row.modulo_id + '"  data-seccion_id="' + row.seccion_id + '" data-submenu_id="' + row.submenu_id + '" data-vista_id="' + row.vista_id + '" class="pt-2 pb-2"><input checked="true" type="checkbox" name="modulo_id"/></div>';
                        } else {
                            return '<div onClick="permisos.insert(this)" data-modulo_id="' + row.modulo_id + '"  data-seccion_id="' + row.seccion_id + '" data-submenu_id="' + row.submenu_id + '" data-vista_id="' + row.vista_id + '" class="pt-2 pb-2"><input type="checkbox" name="modulo_id"/></div>';
                        }
                    }
                },
                {
                    title: "Módulo",
                    render: function(data, type, row) {
                        return '<small>' + row.modulo + '</small>';
                    },
                },
                {
                    title: "Sección",
                    render: function(data, type, row) {
                        return '<small>' + row.seccion + '</small>';
                    },
                },
                {
                    title: "Apartado",
                    render: function(data, type, row) {
                        return '<small>' + row.submenu + '</small>';
                    },
                },
                {
                    title: "Vista",
                    render: function(data, type, row) {
                        return '<small>' + row.vista + '</small>';
                    },
                },
            ],

        }
    }

    filtrar() {
        var params = {
            modulo_id: $("[name*='modulo_id'] option:selected").val(),
            rol_id: $("[name*='rol_id'] option:selected").val()
        };
        app.getBusqueda(params);
    }

    insert(_this) {
        $.isLoading({
            text: "Realizando petición ...."
        });
        ajax.post(app.url_api, permisos.dataform(_this), function(response, headers) {
            if (headers.status == 200 || headers.status == 201) {
                $.isLoading("hide");
                toastr.success(headers.message);
                permisos.filtrar();
            } else {
                $.isLoading("hide");
                return ajax.showValidations(headers);
            }
        })
    }

    eliminar(_this) {
        $.isLoading({
            text: "Realizando petición ...."
        });
        ajax.delete(app.url_api + '/eliminar-permiso-rol', permisos.dataform(_this), function(response, headers) {
            if (headers.status == 200 || headers.status == 201) {
                $.isLoading("hide");
                toastr.success(headers.message);
                permisos.filtrar();
            } else {
                $.isLoading("hide");
                return ajax.showValidations(headers);
            }
        })
    }

    dataform = (_this) => {
        return {
            rol_id: $("[name*='rol_id'] option:selected").val(),
            modulo_id: $(_this).data('modulo_id'),
            seccion_id: $(_this).data('seccion_id'),
            submenu_id: $(_this).data('submenu_id'),
            vista_id: $(_this).data('vista_id')
        }
    }

}
const permisos = new Permisos();