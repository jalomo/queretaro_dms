class Compras {
    constructor() {
        this.tabla = '#tabla-administrador';
        this.form = 'form-modulo';
        this.modal = 'modal-administrador';
        this.identificador = false;
        this.url_api = 'api/ereact/compras';
        this.file = '';
        this.crearTabla(this.configuracionTabla());
        this.getBusqueda();
    }
    crearTabla(configTable) {
        $(this.tabla).dataTable(configTable);
    }

    configuracionTabla() {
        return {
            language: {
                url: PATH_LANGUAGE
            },
            bFilter: false,
            order: [
                [0, 'asc']
            ],
            columns: [{
                    title: "Num identificación",
                    data: 'no_identificacion'
                },
                {
                    title: "Prefijo",
                    data: 'prefijo'
                },
                {
                    title: "Básico",
                    data: 'basico'
                },
                {
                    title: "Sufijo",
                    data: 'sufijo'
                },
                {
                    title: "Descripción",
                    data: 'descripcion'
                },
                {
                    title: "Proveedor",
                    data: 'proveedor'
                },
                {
                    title: "RFC",
                    data: 'rfc'
                },
                {
                    title: "Tipo venta",
                    data: 'tipo_venta'
                },
                {
                    title: "Total",
                    data: 'total'
                },
                {
                    title: "Cantidad",
                    data: 'cantidad'
                },
                {
                    title: "IVA",
                    data: 'iva'
                }
            ],

        }
    }

    getBusqueda(params) {
        let form = this.procesar_form('form-ereact');
        $.ajax({
            dataType: "json",
            type: 'GET',
            data: form,
            url: PATH_API + this.url_api,
            success: function(response, _, s) {
                var listado = $(compras.tabla).DataTable();
                listado.clear().draw();
                console.log(response);
                if (response && response.length > 0) {
                    response.forEach(listado.row.add);
                    listado.draw();
                }
            }
        });
    }

    procesar_form(form_id) {
        let form = $('#' + form_id).serializeArray();
        var data = {};
        $(form).each(function(i, field) {
            data[field.name] = $.trim(field.value);
        });
        return data;
    }

    descargar() {
        window.location.href = PATH_API + this.url_api + '/download';
    }

}
const compras = new Compras();