class Inventario {
    constructor() {
        this.tabla = '#tabla-administrador';
        this.form = 'form-modulo';
        this.modal = 'modal-administrador';
        this.identificador = false;
        this.url_api = 'api/ereact/inventario';
        this.file = '';
        this.crearTabla(this.configuracionTabla());
        this.getBusqueda();
    }
    crearTabla(configTable) {
        $(this.tabla).dataTable(configTable);
    }

    configuracionTabla() {
        return {
            language: {
                url: PATH_LANGUAGE
            },
            bFilter: false,
            order: [
                [0, 'asc']
            ],
            columns: [{
                    title: "Num identificación",
                    data: 'no_identificacion'
                },
                {
                    title: "Prefijo",
                    data: 'prefijo'
                },
                {
                    title: "Básico",
                    data: 'basico'
                },
                {
                    title: "Sufijo",
                    data: 'sufijo'
                },
                {
                    title: "Descripción",
                    data: 'descripcion'
                },
                {
                    title: "Ubicación",
                    data: 'ubicacion'
                },
                {
                    title: "Mes 1",
                    data: 'mes_1'
                },
                {
                    title: "Mes 2",
                    data: 'mes_2'
                },
                {
                    title: "Mes 3",
                    data: 'mes_3'
                },
                {
                    title: "Mes 4",
                    data: 'mes_4'
                },
                {
                    title: "Mes 5",
                    data: 'mes_5'
                },
                {
                    title: "Mes 6",
                    data: 'mes_6'
                },
                {
                    title: "Mes 7",
                    data: 'mes_7'
                },
                {
                    title: "Mes 8",
                    data: 'mes_8'
                },
                {
                    title: "Mes 9",
                    data: 'mes_9'
                },
                {
                    title: "Mes 10",
                    data: 'mes_10'
                },
                {
                    title: "Mes 11",
                    data: 'mes_11'
                },
                {
                    title: "Mes 12",
                    data: 'mes_12'
                },
                {
                    title: "Total",
                    data: 'total'
                },
                {
                    title: "Existencia",
                    data: 'existencia'
                }
            ],

        }
    }

    getBusqueda(params) {
        let form = this.procesar_form('form-ereact');
        $.ajax({
            dataType: "json",
            type: 'GET',
            data: form,
            url: PATH_API + this.url_api,
            success: function(response, _, s) {
                var listado = $(inventario.tabla).DataTable();
                listado.clear().draw();
                console.log(response);
                if (response && response.length > 0) {
                    response.forEach(listado.row.add);
                    listado.draw();
                }
            }
        });
    }

    procesar_form(form_id) {
        let form = $('#' + form_id).serializeArray();
        var data = {};
        $(form).each(function(i, field) {
            data[field.name] = $.trim(field.value);
        });
        return data;
    }

    descargar() {
        window.location.href = PATH_API + 'api/ereact/inventario/download'
    }

}
const inventario = new Inventario();