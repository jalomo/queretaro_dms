class Salida {
    constructor() {
        this.tabla = '#tabla-administrador';
        this.form = 'form-modulo';
        this.modal = 'modal-administrador';
        this.identificador = false;
        this.url_api = 'api/ereact/';
        this.file = '';
        this.crearTabla(this.configuracionTabla());
        this.getBusqueda();
    }
    crearTabla(configTable) {
        $(this.tabla).dataTable(configTable);
    }

    configuracionTabla() {
        return {
            ajax: {
                url: PATH_API + this.url_api + 'ventas',
                type: 'GET',
                data: function(data) {
                    data.fecha_inicio = document.getElementById('fecha_inicio').value;
                    data.fecha_fin = document.getElementById('fecha_fin').value;
                },
            },
            language: {
                url: PATH_LANGUAGE
            },
            processing: true,
            serverSide: true,
            bFilter: false,
            order: [
                [0, 'asc']
            ],
            columns: [{
                    title: "Número parte",
                    data: 'no_identificacion'
                },
                {
                    title: "Prefijo",
                    data: 'prefijo'
                },
                {
                    title: "Básico",
                    data: 'basico'
                },
                {
                    title: "Sufijo",
                    data: 'sufijo'
                },
                {
                    title: "Descripción",
                    data: 'descripcion'
                },
                {
                    title: "Clave star",
                    data: 'clave_stars'
                },
                {
                    title: "Vendedor",
                    data: 'nombre_vendedor'
                },
                {
                    title: "Clave cliente",
                    data: 'numero_cliente'
                },
                {
                    title: "Cliente",
                    data: 'nombre_cliente'
                },
                {
                    title: "RFC cliente",
                    data: 'rfc'
                },
                {
                    title: "Costo",
                    render: function(data, type, row) {
                        if (row.estatus_ventas_id == 5) {
                            return '<span class="text-danger">' + row.cantidad + ' ' + row.venta_total_producto + row.costo_promedio + '</span>'
                        } else {
                            return row.cantidad + ' ' + row.venta_total_producto + row.costo_promedio
                        }
                    }
                },
                {
                    title: "Folio",
                    data: 'folio'
                },
                {
                    title: "Tipo",
                    render: function(data, type, row) {
                        return 1
                    }
                },
                {
                    title: "IVA",
                    data: 'iva'
                },
                {
                    title: "Codigo postal",
                    data: 'codigo_postal'
                },
            ],

        }
    }

    getBusqueda(params) {
        $(this.tabla).DataTable().ajax.reload()
    }
    generateEreactVenta(params) {
        let form = this.procesar_form('form-ereact');
        $.ajax({
            dataType: "json",
            type: 'GET',
            data: form,
            url: PATH_API + this.url_api + 'genera-ereact-ventas',
            success: function(response, _, s) {
                salida.file = response;
                salida.descargar();
            }
        });
    }

    procesar_form(form_id) {
        let form = $('#' + form_id).serializeArray();
        var data = {};
        $(form).each(function(i, field) {
            data[field.name] = $.trim(field.value);
        });
        return data;
    }

    descargar() {
        window.location.href = PATH_API + 'api/file/?path=ereact/' + salida.file
    }

}
const salida = new Salida();