var _appsFunction = function () {
	this.init = function () {

		$('div#content_DescuentoMensual').hide();
		$('select#id_MovAplicacion').attr('onchange','Apps.actualizaCampos()');
		var change_input = {
			"FechaInicio": {min: FechaInicio,onblur:'Apps.actualizaFechaTermino()'},
			"MontoLimite": {value: '0.00',type:'number',step:'0.01',min:0},
			"MontoAcumulado": {value: '0.00',type:'number',step:'0.01',min:0},
		};
		update_inputs(change_input);
		
		$('form#general_form select').each(function( index ) {
			try {
				var id = $(this).attr('attr-id').toLowerCase();
			} catch (error) {
				var id = $(this).attr('attr-id');
			}
			if($.trim(id).length > 0){
				$(this).find('option[value="'+id+'"]').attr("selected", "selected");
				$(this).select2({    
					language: {
					noResults: function() { return "No hay resultados"; },
					searching: function() { return "Buscando.."; }
					}
				});
			}else{
				$(this).select2({    
					language: {
					noResults: function() { return "No hay resultados"; },
					searching: function() { return "Buscando.."; }
					}
				}).val('').change();
			}                        
		});

		Apps.actualizaCampos();

	},
	this.actualizaCampos = function(){
		var option = $('select#id_MovAplicacion option:selected').val();
		if(option > 0){
			switch (option) {
				case '1':
					$('div#content_FechaInicio').show();
					$('div#content_FechaTermino').hide();
					$('div#content_FormulaMonto').show();
					$('div#content_MontoLimite').show();
					$('div#MontoAcumulado').show();
					break;
				case '2':
					$('div#content_FechaInicio').show();
					$('div#content_FechaTermino').show();
					$('div#content_FormulaMonto').show();
					$('div#content_MontoLimite').hide();
					$('div#MontoAcumulado').show();
					break;
				case '3':
					$('div#content_FechaInicio').show();
					$('div#content_FechaTermino').hide();
					$('div#content_FormulaMonto').show();
					$('div#content_MontoLimite').hide();
					$('div#MontoAcumulado').show();
					break;
				case '4':
					$('div#content_FechaInicio').hide();
					$('div#content_FechaTermino').hide();
					$('div#content_FormulaMonto').show();
					$('div#content_MontoLimite').hide();
					$('div#MontoAcumulado').show();
					break;
			}
			
		}
	},
	this.actualizaFechaTermino = function(){
		var change_input = {
			"FechaTermino": {min: $('input[name=FechaInicio]').val()},
		};
		update_inputs(change_input);
	},
    this.regresar = function () {
		window.location.href = PATH+'/nomina/inicio/movientos_nomina/index/'+id_trabajador;
    }	
	this.guardar = function () {
	   var data_send =  $('form#general_form').serializeArray();
	   //data_send.push({ 'name': 'id_Trabajador','value':id_trabajador });
	   data_send.push({ 'name': 'id','value':id_movimiento });

        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/inicio/movientos_nomina/alta_actualizar',
            data: data_send,
            success: function (response, status, xhr) {

				if (response.status == 'success') {
					Swal.fire({
						icon: 'success',
						title: '',
						text: response.message,
						confirmButtonText: "Aceptar"
					}).then((result) => {
						Apps.regresar();
					});
				}

            }

        });
    }		
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.init();
});