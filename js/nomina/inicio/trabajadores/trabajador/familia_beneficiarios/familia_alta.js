var _appsFunction = function () {
    this.fecha_hoy,
    this.init = function () {
        
        $('form#general_form select').each(function( index ) {
            var id = $(this).attr('attr-id');
            if($.trim(id).length > 0){
                $(this).find('option[value="'+id+'"]').attr("selected", "selected");
                $(this).select2({    
                    language: {
                    noResults: function() { return "No hay resultados"; },
                    searching: function() { return "Buscando.."; }
                    }
                });
            }else{
                $(this).select2({    
                    language: {
                    noResults: function() { return "No hay resultados"; },
                    searching: function() { return "Buscando.."; }
                    }
                }).val('').change();
            }                        
        });

    },
    
    this.guardar = function () {
        
        var dataSend = $('form#general_form').serializeArray();
        dataSend.push({name: 'id_Trabajador', value: identity});
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/inicio/trabajador/familia_beneficiarios_post',
            data: dataSend,
            success: function (response, status, xhr) {
                if (response.status == 'success') {
					Swal.fire({
						icon: 'success',
						title: '',
						text: response.message,
						confirmButtonText: "Aceptar"
					}).then((result) => {
                        window.location.href = PATH + '/nomina/inicio/trabajador/familia_beneficiarios?id=' + identity;
                    });
				}
            }
        });
    }
}

var Apps;
$(function () {
    Apps = new _appsFunction();
    Apps.init();
});
