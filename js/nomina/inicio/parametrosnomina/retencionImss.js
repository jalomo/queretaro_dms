var _appsFunction = function () {
	this.init = function () {
		
		$('form#general_form select').each(function( index ) {
			try {
				var id = $(this).attr('attr-id').toLowerCase();
			} catch (error) {
				var id = $(this).attr('attr-id');
			}
			if($.trim(id).length > 0){
				$(this).find('option[value="'+id+'"]').attr("selected", "selected");
				$(this).select2({    
					language: {
					noResults: function() { return "No hay resultados"; },
					searching: function() { return "Buscando.."; }
					}
				});
			}else{
				$(this).select2({    
					language: {
					noResults: function() { return "No hay resultados"; },
					searching: function() { return "Buscando.."; }
					}
				}).val('').change();
			}                        
		});

	},
	this.guardar = function () {
	   var data_send =  $('form#general_form').serializeArray();
	   data_send.push({ 'name': 'id','value':identity });

        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/inicio/parametrosnomina/guardar_retencionImss',
            data: data_send,
            success: function (response, status, xhr) {

				if (response.status == 'success') {
					Swal.fire({
						icon: 'success',
						title: '',
						text: response.message,
						confirmButtonText: "Aceptar"
					});
				}

            }

        });
    }		
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.init();
});
