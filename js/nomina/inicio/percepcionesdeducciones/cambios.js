var _appsFunction = function () {

	this.zfill = function(number, width) {
		var numberOutput = Math.abs(number); /* Valor absoluto del número */
		var length = number.toString().length; /* Largo del número */ 
		var zero = "0"; /* String de cero */  
		
		if (width <= length) {
			if (number < 0) {
				 return ("-" + numberOutput.toString()); 
			} else {
				 return numberOutput.toString(); 
			}
		} else {
			if (number < 0) {
				return ("-" + (zero.repeat(width - length)) + numberOutput.toString()); 
			} else {
				return ((zero.repeat(width - length)) + numberOutput.toString()); 
			}
		}
	},


	this.init = function () {
		
		$('form#general_form select').each(function( index ) {
			try {
				var id = $(this).attr('attr-id').toLowerCase();
			} catch (error) {
				var id = $(this).attr('attr-id');
			}
			if( $(this).attr('name') != 'cve_pd' ){
				if($.trim(id).length > 0){
					$(this).find('option[value="'+id+'"]').attr("selected", "selected");
					$(this).select2({    
						language: {
						noResults: function() { return "No hay resultados"; },
						searching: function() { return "Buscando.."; }
						}
					});
				}else{
					$(this).select2({    
						language: {
						noResults: function() { return "No hay resultados"; },
						searching: function() { return "Buscando.."; }
						}
					}).val('').change();
				}
			}
		});

		// $("input#Clave").blur(function(e) {
		// 	var $this = this;
		// 	$($this).val( Apps.zfill( parseInt($this.value) ,3) );
        // });
        
        $('input[name=Clave]').removeAttr('type');
        $('input[name=Clave]').removeClass('form-control');
        $('input[name=Clave]').addClass('form-control-plaintext');
        $('input[name=Clave]').attr('readonly', true);

	},
	this.regresar = function () {
		window.location.href = PATH+'/nomina/inicio/percepcionesdeducciones';
    }		
	this.guardar = function () {
		var data_send =  $('form#general_form').serializeArray();
		var data_build = [];   
        data_build.push({ 'name': 'id','value':identity });
        
		$.each(data_send, function( index, value ) {
			if(value.name != 'id_PyDTipo' && value.name != 'Clave'){
				data_build.push(value);
			}
		});

	   	var selectedCheck = new Array();
        var n = jQuery("input[name=PyDTipo]:checked").length;
        if (n > 0){
            $("input[name=PyDTipo]:checked").each(function(){
                selectedCheck.push($(this).val());
			});
			data_build.push({ 'name': 'PyDTipo','value':JSON.stringify(selectedCheck) });
		}

        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/inicio/percepcionesdeducciones/actualizar',
            data: data_build,
            success: function (response, status, xhr) {

				if (response.status == 'success') {
					Swal.fire({
						icon: 'success',
						title: '',
						text: response.message,
						confirmButtonText: "Aceptar"
					}).then((result) => {
						window.location.href = PATH + '/nomina/inicio/percepcionesdeducciones';
					});
				}

            }

        });
    }		
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.init();
});
