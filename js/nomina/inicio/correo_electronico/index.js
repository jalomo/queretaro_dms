var _appsFunction = function () {
	this.init = function () {},
	this.cargarDatos = function(){
		var table = $('table#listado').DataTable();
		table.ajax.reload(null, false);
	},

	this.buscar_periodos = function(){
		
		var param1 = $('select#id_tipo_periodo option:selected').val();
		
		// $.ajax({
		// 	dataType: "json",
		// 	type: 'GET',
		// 	url: PATH + '/nomina/inicio/correo_electronico/get_periodos',
		// 	data: {id_tipo_periodo: param1},
		// 	success: function (response, status, xhr) {

		// 		if (response.status == 'success') {
		// 			$('select#id_periodo option').remove()
		// 			if(response.data.length > 0){
		// 				response.data.forEach(element => {
		// 					$('select#id_periodo').append('<option value="'+element.id+'">'+moment(element.FechaInicio).format("DD/MM/YYYY")+' al '+moment(element.FechaFin).format("DD/MM/YYYY")+'</option>');
		// 				});
		// 			}
		// 		}
		// 	}

		// });

		$.ajax({
			dataType: "json",
			type: 'GET',
			// url: PATH + '/nomina/inicio/correo_electronico/get_periodos',
			url: PATH + 'nomina/inicio/tablero_total_percepciones/periodos_cerrados_anio_transacciones',
			data: {id_tipo_periodo: param1},
			success: function (response, status, xhr) {

				if (response.status == 'success') {
					$('select#id_periodo option').remove()
					if(response.data.length > 0){
						response.data.forEach(element => {
							$('select#id_periodo').append('<option value="'+element.id+'">'+moment(element.FechaInicio).format("DD/MM/YYYY")+' al '+moment(element.FechaFin).format("DD/MM/YYYY")+'</option>');
						});
                    }else{
						toastr.warning('No hay periodos pagados para el tipo de periodo seleccionado');
					}
				}
			}

		});
	},
	this.mostrarRecibo = function ($this) {
		var id = $($this).attr('data-id_trabajadorpago');
		var id2 = $($this).attr('data-id_trabajdor');
		var id3 = $($this).attr('data-id_periodo');

		window.location.href = PATH + '/nomina/inicio/correo_electronico/detalle/'+id+'/'+id2+'/'+id3;
	},
	this.descargarRecibo = function ($this) {
		var id = $($this).attr('data-id_trabajadorpago');
		var id2 = $($this).attr('data-id_trabajdor');
		var id3 = $($this).attr('data-id_periodo');

		window.location.href = PATH + '/nomina/inicio/correo_electronico/recibo/'+id+'/'+id2+'/'+id3;
	},
	this.descargarXML = function ($this) {
		var id = $($this).attr('data-id_trabajadorpago');
		var id2 = $($this).attr('data-id_trabajdor');
		var id3 = $($this).attr('data-id_periodo');

		window.location.href = PATH + '/nomina/inicio/correo_electronico/xml/'+id+'/'+id2+'/'+id3;
	},

	this.timbrado = function($this){

		var id2 = $($this).attr('data-id_trabajdor');
		var id3 = $($this).attr('data-id_periodo');

		const ipAPI = PATH+'/nomina/inicio/correo_electronico/timbrado?id_Trabajador='+id2+'&id_Periodo='+id3;
		console.log(ipAPI);

		Swal.queue([{
		title: 'Timbrar nomina',
		showCancelButton: true,
		cancelButtonText: 'Cancelar',
		confirmButtonText: 'Si',
		text:
			'' +
			'',
		showLoaderOnConfirm: true,
		showLoaderOnCancel: false,
		preConfirm: () => {
			return fetch(ipAPI)
			.then(response => response.json())
			.then(data => Swal.insertQueueStep('El timbrado ha finalizado correctamente'))
			.catch(() => {
				Swal.insertQueueStep({
				icon: 'error',
				title: 'Lo sentimos, no hemos podido procesar tu solicitud en este momento.'
				})
			})
			.finally(function() { 
				setTimeout('Apps.reload_table()', 1000);
			 });
		}
		}])
	},

	this.reload_table = function(){
		var tables = $('table#listado').DataTable();
		tables.ajax.reload( null, false );
	},

	this.get = function () {
		
		// if(id_trabajador != false){
		// 	$('select[name="trabajador"]').attr('attr-id',id_trabajador);
		// }
		
		// $('form#general_form select').each(function( index ) {
		// 	try {
		// 		var id = $(this).attr('attr-id').toLowerCase();
		// 	} catch (error) {
		// 		var id = $(this).attr('attr-id');
		// 	}
		// 	if($.trim(id).length > 0){
		// 		$(this).find('option[value="'+id+'"]').attr("selected", "selected");
		// 	}                        
		// });

		
		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			order: [[ 0, "desc" ]],
			ajax: {
				url: PATH + '/nomina/inicio/correo_electronico/index_get',
				type: "POST",
				data: function(){
					return {
						id_periodo: $('select#id_periodo option:selected').val()
					};
				}
			},
			columns: [
				{
					title: 'Clave',
					data: 'Clave'
				},
				{
					title: 'Nombre',
					'data': function(data){
						var row = [
							data.Nombre,
							data.Apellido_1,
							data.Apellido_2
						];
						return row.join(' ');
					}
				},
				{
					title: 'RFC',
					data: 'rfc'
				},
				{
					title: 'T. Percepciones',
					data: 'total_percepciones',
					render: function ( data, type, row, meta ) {
						return '$'+ (new Intl.NumberFormat('en-US').format(data));
					}
				},
				{
					title: 'F. Emisión',
					data: 'fecha_calculo',
					render: function ( data, type, row, meta ) {
						var fecha = moment(data);
						if(fecha.isValid()){
							return fecha.format('DD/MM/YYYY');
						}
						return '';
					}
				},
				{
					title: 'F. Timbrado',
					data: 'fecha_timbrado',
					render: function ( data, type, row, meta ) {
						var fecha = moment(data);
						if(fecha.isValid()){
							return fecha.format('DD/MM/YYYY');
						}
						return '';
					}
				},
				// {
				// 	title: 'UUID',
				// 	data: 'sello'
				// },
				{
					title: 'Estado CFDI',
					data: 'id_comprobantenomina',
					render: function ( data, type, row, meta ) {
						if(data != null){
							return 'Timbrado';
						}else{
							return 'No generado';
						}
					}
				},
				{
					title: '-',
					width: "80px",
					data: 'id_comprobantenomina',
					render: function ( data, type, row, meta ) {
						var timbrado = '';
						var pdf = '';
						var xml = '';

						if(data == null){
							// timbrado =  "<span  style='font-size: 2em;cursor: pointer;' onclick='Apps.timbrado(this);' class='mr-1 ml-1' data-id_trabajadorpago=" + row.id_trabajadorpago + " data-id_trabajdor=" + row.id_trabajador + " data-id_periodo=" + row.id_Periodo + "><i class='fas fa-qrcode'></i></span>";
						}else{
							var recibo = "";//"<span  style='font-size: 2em;cursor: pointer;' onclick='Apps.mostrarRecibo(this);' class='mr-1 ml-1' data-id_trabajadorpago=" + row.id_trabajadorpago + " data-id_trabajdor=" + row.id_trabajador + " data-id_periodo=" + row.id_Periodo + "><i class='far fa-list-alt text-dark'></i></span>";
							// pdf =  "<span  style='font-size: 2em;cursor: pointer;' onclick='Apps.descargarRecibo(this);' class='mr-1 ml-1' data-id_trabajadorpago=" + row.id_trabajadorpago + " data-id_trabajdor=" + row.id_trabajador + " data-id_periodo=" + row.id_Periodo + "><i class='fas fa-file-pdf text-danger' ></i></span>";
							xml = "<span  style='font-size: 2em;cursor: pointer;' onclick='Apps.descargarXML(this);' class='mr-1 ml-1' data-id_trabajadorpago=" + row.id_trabajadorpago + " data-id_trabajdor=" + row.id_trabajador + " data-id_periodo=" + row.id_Periodo + "><i class='fas fa-paper-plane text-info' ></i></span>";
						}

						return timbrado + pdf + xml ;
					}
				}
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
