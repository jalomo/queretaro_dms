var _appsFunction = function () {
    this.init = function () {

		var change_input = {
			"id_RiesgoTrabajo": {type: "number",'min':'0'},
            "CP": {type: "number",'max':'99999','maxlength':5}
		};
		update_inputs(change_input);
		update_select2('form#general_form select');
    },
    
    this.guardar = function(){

        var data_send =  $('form#general_form').serializeArray();
        data_send.push({ 'name': 'id_Empresa','value':id_Empresa });
        data_send.push({ 'name': 'SalarioMinimo','value':salario_minimo });

       $.ajax({
           dataType: "json",
           cache: false,
           type: 'POST',
           url: PATH + '/nomina/configuracion/datos_empresa/datos_patron_alta_guardar',
           data: data_send,
           success: function (response, status, xhr) {

            if (response.status == 'success') {
                Swal.fire({
                    icon: 'success',
                    title: '',
                    text: response.message,
                    confirmButtonText: "Aceptar"
                }).then((result) => {
                    window.location.href = PATH + '/nomina/configuracion/datos_empresa/datos_patron/'+id_Empresa;
                });
            }

           }

       });
   }
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.init();
});
