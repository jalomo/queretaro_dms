var _appsFunction = function () {
	this.init = function () {},
	this.cargarDatos = function(){
		var table = $('table#listado').DataTable();
		table.ajax.reload(null, false);
	},

	this.get = function () {
		
		if($id != false){
			$('select[name="trabajador"]').attr('attr-id',$id);
		}
		
		$('form#general_form select').each(function( index ) {
			try {
				var id = $(this).attr('attr-id').toLowerCase();
			} catch (error) {
				var id = $(this).attr('attr-id');
			}
			if($.trim(id).length > 0){
				$(this).find('option[value="'+id+'"]').attr("selected", "selected");
			}                        
		});

		
		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			order: [[ 0, "desc" ]],
			ajax: {
				url: PATH + '/nomina/catalogosconsultas/percepcciones_base_fiscal/index_get',
				type: "POST",
				data: function(){
					return {
						id: $('select[name=trabajador] option:selected').val()
					};
				}
			},
			columns: [
				{
					title: 'Clave',
					data: 'Clave'
				},
				{
					title: 'Descripción',
					data: 'PyD'
				},
				{
					title: 'Aplicación',
					data: 'Montos_Descripcion'
				},
				{
					title: 'Fórmula',
					data: 'Formula'
				},
				{
					title: '-',
					'data': function (data) {
						return "<a class='btn btn-info btn-sm' href='" + PATH + '/nomina/catalogosconsultas/percepcciones_base_fiscal/editar/'+data.id+ '/' + data.BaseFiscalTipo_id + "' title='' > <i class='fas fa-pencil-alt'></i> </a>";
					}
				}
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
