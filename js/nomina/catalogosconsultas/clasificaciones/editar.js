var _appsFunction = function () {
	this.init = function () {},

		this.guardar = function () {

			var data_save = $('form#formContent').serializeArray();
			data_save.push({ name: "id", value: identity });

			$.ajax({
				dataType: "json",
				type: 'post',
				url: PATH + '/nomina/api/api/runner/clasificaciones/put',
				data: data_save,
				success: function (response, status, xhr) {
					if (response.status == 'success') {
						Swal.fire({
							icon: 'success',
							title: '',
							text: response.message,
							confirmButtonText: "Aceptar"
						}).then((result) => {
							window.location.href = PATH + '/nomina/catalogosconsultas/clasificaciones/index?id=' + response.data.id;
						});
					}
				}

			});
		},
		this.render = function(){
			$.ajax({
				dataType: "json",
				type: 'post',
				url: PATH + '/nomina/api/api/runner/clasificaciones/find',
				data: { id: identity },
				success: function (response, status, xhr) {
					if(response.status == 'success'){
						var template = $('script#template').html()
						var storange = {
							Clave: response.data.Clave,
							Descripcion: response.data.Descripcion
						};
						var renderedContent = Mustache.render(template,storange);
						$('form#formContent > div#contenedor').html(renderedContent);
					}
				}

			});
		}

}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.render();
});
