let tipo_cliente_id = $("#tipo_cliente_id").val();
var table_clientes = 'table#tbl_cliente';
let cliente_id_ = '';
class ClassDatosVenta {

    constructor() {
        this.create_table_clientes();
        this.filtrarTablaClientes();

    }

    openModalAltaCliente() {
        $("#form-cliente")[0].reset();
        $('#telefono').mask('0000000000');
        $('#codigo_postal').mask('99999');
        $('#rfc').mask('AAAAAAAAAAAAA');
        $("#modal-alta-cliente").modal('show');
        $("#title_modal_cliente").html('Agregar cliente');
        $("#btn_editar_cliente").hide();
        $("#btn_agregar_cliente").show();
        $("#datos_credito").hide();
    }
    changeTipoCliente(_this) {
        tipo_cliente_id = $(_this).val();
        app.tipoCliente($(_this).val());
    }
    tipoCliente(tipo_cliente) {
        if (tipo_cliente == 2) { // Registrado
            ajax.getCatalogo('clientes/select-catalogo?aplica_credito=false', $(".select_cliente"), true);
        } else if (tipo_cliente == 3) { // Con crédito
            ajax.getCatalogo('clientes/select-catalogo?aplica_credito=true', $(".select_cliente"), true);
        }
    }
    guardarCliente() {
        let form = this.procesar_form('form-cliente');
        ajax.post(`api/clientes/crear-cliente-modal`, form, function(response, header) {
            if (header.status == 200) {
                $("#numero_cliente_").val(response.numero_cliente);
                app.filtrarTablaClientes();
                $("#modal-alta-cliente").modal('hide');
            }
        });
    }
    updateCliente() {
        let form = this.procesar_form('form-cliente');
        //cliente_id = $('input[name=cliente_id]:checked').val();
        ajax.put(`api/clientes/update-cliente-modal/` + cliente_id_, form, function(response, header) {
            if (header.status == 200) {
                utils.displayWarningDialog('Cliente actualizado correctamente', 'success', function(result) {
                    $("#modal-alta-cliente").modal('hide');
                    app.filtrarTablaClientes();
                    app.getDatosCliente();
                });
            }
        });
    }
    getDatosCliente(_this) {
        $("#telefono_cliente").val('');
        $("#correo_cliente").val('');
        $("[name='marca']").val('');
        $("[name='modelo']").val('');
        $("[name='anio']").val('');
        $("[name='no_serie']").val('');
        cliente_id = $(_this).val() ? $(_this).val() : $(".select_cliente option:selected").val();
    }
    openModalEditarCliente(_this) {
        $("#form-cliente")[0].reset();
        $.isLoading({ text: "Obteniendo datos cliente...." });
        let datos = [];
        cliente_id_ = $(_this).data('cliente_id');
        if (!cliente_id_) {
            $.isLoading("hide");
            toastr.warning("Favor de seleccionar un cliente");
            return false;
        }
        $("#modal-alta-cliente").modal('show');
        $('#telefono').mask('0000000000');
        $('#codigo_postal').mask('99999');
        $('#rfc').mask('AAAAAAAAAAAAA');
        $("#title_modal_cliente").html('Editar cliente');
        $("#btn_editar_cliente").show();
        $("#btn_agregar_cliente").hide();
        ajax.get(`api/clientes/` + cliente_id_, {}, function(response, header) {
            if (header.status == 200) {
                datos = response.shift();
                $.each(datos, function(indexInArray, valueOfElement) {
                    $("[name='" + indexInArray + "']").val(valueOfElement);
                });
            }
        });
        $("#datos_credito").show();
        ajax.post('api/clientes/tiene-credito', { id: cliente_id_ }, function(response, header) {
            if (header.status == 200) {
                $("#aplica_credito").val(utils.isDefined(response.aplica_credito) && response.aplica_credito == 1 ? 'Tiene Crédito' : 'Sin Crédito');
                $("#limite_credito").val(utils.isDefined(response.limite_credito) ? parseFloat(response.limite_credito).toFixed(2) : '');
                $("#plazo_credito").val(utils.isDefined(response.plazo_credito) ? response.plazo_credito : '');
                $("#credito_actual").val(utils.isDefined(response.credito_actual) ? parseFloat(response.credito_actual).toFixed(2) : '');
            }
        }, false, false);
        $.isLoading("hide");

    }
    procesar_form(form_id) {
        let form = $('#' + form_id).serializeArray();
        var data = {};
        $(form).each(function(i, field) {
            data[field.name] = $.trim(field.value);
        });
        return data;
    }
    changeRegimen = () => {
        let nombre = $("#nombre").val();
        let apellidoPaterno = $("#apellido_paterno").val();
        let apellidoMaterno = $("#apellido_materno").val();
        let nombreCompleto = `${nombre} ${apellidoPaterno} ${apellidoMaterno}`;
        let params = '';
        let regimenFiscal = document.getElementById('regimen_fiscal').value;
        if (regimenFiscal === 'M') {
            $("#nombre_empresa").val("");
            $("#nombre_empresa").attr('readonly', false);
            params = { persona_moral: true };
        } else if (regimenFiscal === 'F') {
            $("#nombre_empresa").attr('readonly', false);
            $("#nombre_empresa").val(nombreCompleto);
            params = { persona_fisica: true };
        } else if (regimenFiscal === 'A') {
            params = { persona_fisica: true, persona_moral: true };
        }

        ajax.getCatalogo('regimen-fiscal?' + jQuery.param(params), $("select[name*='regimen_fiscal_id']"), true);
    }

    create_table_clientes() {
        $(table_clientes).DataTable(this.settings_table());
    }

    filtrarTablaClientes = () => {
        console.log("auq");
        $(table_clientes).DataTable().ajax.reload()
    }

    settings_table() {
        return {
            ajax: {
                url: PATH_API + 'api/clientes/busqueda',
                type: 'GET',
                data: function(data) {
                    data.id = document.getElementById('cliente_id_').value;
                    data.numero_cliente = document.getElementById('numero_cliente_').value;
                    data.nombre = document.getElementById('nombre_completo_').value;
                    data.rfc = document.getElementById('rfc_').value;
                },
            },
            language: {
                url: PATH_LANGUAGE
            },
            processing: true,
            serverSide: true,
            searching: false,
            bFilter: false,
            lengthChange: false,
            bInfo: true,
            order: [
                [0, 'asc']
            ],
            columns: [{
                    title: "#",
                    width: "20px",
                    render: function(data, type, row) {
                        let checked = row.id == $("#cliente_id_").val() ? 'checked="true"' : false;
                        return '<div class="pt-2 pb-2"><input ' + checked + ' type="radio" onclick="app.getDatosCliente(this)" class="seleccion" name="cliente_id" id="cliente_id" data-aplica_credito="' + row.aplica_credito + '" value="' + row.id + '"/></div>';
                    }
                },
                {
                    title: 'Número cliente',
                    data: function(data) {
                        return data.numero_cliente
                    }
                },
                {
                    title: 'Razón social',
                    data: 'nombre_empresa'

                },
                {
                    title: 'Nombre cliente',
                    data: function(data) {
                        return data.nombre + ' ' + data.apellido_paterno + ' ' + data.apellido_materno
                    }
                },
                {
                    title: 'RFC',
                    data: 'rfc'
                },
                {
                    title: 'Teléfono',
                    data: 'telefono',
                },
                {
                    title: 'Tiene crédito',
                    data: 'aplica_credito',
                    render: function(data, type, row) {
                        return data && data == true ? 'Si' : 'No';
                    }
                },
                {
                    title: 'Plazo crédito',
                    data: 'plazo_credito',
                    render: function(data, type, row) {
                        return data && data.id ? data.nombre : 'Sin plazo';
                    }
                },
                {
                    title: 'Límite crédito',
                    data: 'limite_credito',
                    render: function(data, type, row) {
                        return data ? '<span class="money_format">' + data + '</span>' : '';
                    }
                },
                {
                    title: 'Acciones',
                    data: 'aplica_credito',
                    render: function(data, type, row) {
                        return '<button type="button" title="Editar Cliente" data-cliente_id="' + row.id + '" onclick="app.openModalEditarCliente(this)" class="btn btn-warning mt-1"><i class="fa fa-edit" style="font-size:18px"></i></button>'
                    }

                },
            ]
        }
    }
}

const app = new ClassDatosVenta();