function inicializaTabla() {
    var tabla_productos = $('#listadoPolizaVentas').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        "ajax": {
            url: PATH_API + "api/polizas/ventas/listado",
            type: 'POST',
            data: {
                fecha_inicio: $("#fecha_inicio").val(),
                fecha_fin: $("#fecha_fin").val()
            }
        },
        columns: [{
                title: "folio",
                data: 'folio',
            },
            {
                title: "Clave<br>producto",
                data: 'no_identificacion',
            },
            {
                title: "Descrición",
                data: 'descripcion',
            },
            {
                title: "Unidad",
                data: 'unidad',
            },
            {
                title: "Cantidad",
                render: function(data, type, row) {
                    if (row.estatusId == 2 || row.estatusId == 3) {
                        return '<span class="text-success text-center">' + '+ ' + row.cantidad + '</span>';
                    } else {
                        return '<span class="text-danger text-center">' + '- ' + row.cantidad + '</span>';
                    }
                }
            },
            {
                title: "V/Unitario",
                data: 'valor_unitario',
                render: function(data, type, row) {
                    return utils.isDefined(data) ? '$ <span class="money_format">' + parseFloat(data).toFixed(2) + '</span>' : null;
                }
            },
            {
                title: "Total",
                data: 'venta_total',
                render: function(data, type, row) {
                    return utils.isDefined(data) ? '$ <span class="money_format">' + parseFloat(data).toFixed(2) + '</span>' : null;
                }
            },
            {
                title: "Estatus",
                data: 'estatusVenta',
            },
        ]
    });
}

function getTotalesVentas() {
    ajax.post(`api/polizas/ventas/totales`, {
        fecha_inicio: $("#fecha_inicio").val(),
        fecha_fin: $("#fecha_fin").val()
    }, function(response, headers) {
        if (headers.status == 200) {
            $("#totalVentas").html(utils.isDefined(response.total_ventas) ? response.total_ventas : '');
            $("#totalCantidad").html(utils.isDefined(response.total_cantidad) ? response.total_cantidad : '');
            $("#sumaVUnitario").html(utils.isDefined(response.sum_valor_unitario) ? response.sum_valor_unitario : '');
            $("#sumaTotal").html(utils.isDefined(response.sum_venta_total) ? '$ <span class="money_format">' + response.sum_venta_total + '</span>' : '');
        }
    });
}

this.inicializaTabla();
this.getTotalesVentas();