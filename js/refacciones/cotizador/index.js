class ClassListadoCotizador {
    constructor() {
        this.crearTabla();
        this.getBusqueda_();
        $('.money_format').mask("#,##0.00", { reverse: true });
    }

    crearTabla() {
        $('#tbl_cotizaciones').dataTable(this.configuracionTabla());
        $('#tabla_historico_proactivo').dataTable(this.configuracionTablaComentarioProactivo());
    }

    configuracionTablaComentarioProactivo() {
        return {
            language: {
                url: PATH_LANGUAGE
            },
            searching: false,
            bInfo: false,
            bLengthChange: false,
            paging: false,
            order: [
                [0, 'desc']
            ],
            columns: [{
                    title: "Comentario",
                    data: 'descripcion',
                },
                {
                    title: "Usuario",
                    data: 'usuarios',
                    render: function(data, type, row) {
                        return data.nombre + ' ' + data.apellido_paterno + data.apellido_materno
                    }
                },
                {
                    title: "Fecha",
                    data: 'created_at',
                },
            ]
        }
    }

    configuracionTabla() {
        return {
            language: {
                url: PATH_LANGUAGE
            },
            searching: false,
            order: [
                [0, 'desc']
            ],
            columns: [{
                    width: "30px",
                    title: "Folio",
                    data: 'venta',
                    render: function(data, type, row) {
                        return utils.isDefined(data) && data.folio ? '<b>' + data.folio.folio + '</b>' : ''
                    }
                },
                {
                    width: "200px",
                    title: "Cliente",
                    data: 'venta',
                    render: function(data, type, row) {
                        return utils.isDefined(data) && data.cliente ? data.cliente.nombre_cliente : ''
                    }
                },
                {
                    title: "Marca",
                    data: 'marca',
                    render: function(data, type, row) {
                        return utils.isDefined(data) && data.nombre ? data.nombre : 'S/M'
                    }
                },
                {
                    title: "Modelo",
                    data: 'modelo',
                    render: function(data, type, row) {
                        return utils.isDefined(data) && data.nombre ? data.nombre : 'S/M'
                    }
                },
                {
                    title: "Total",
                    data: 'venta',
                    render: function(data, type, row) {
                        return utils.isDefined(data) ? '$ <span class="money_format">' + utils.isDefined(data.venta_total) ? parseFloat(data.venta_total).toFixed(2) : '' + '</span>' : null;
                    }
                },
                {
                    title: "Fecha registro",
                    render: function(data, type, row) {
                        return app.obtenerFechaMostrar(row.created_at)

                    }
                },
                {
                    title: "Acciones",
                    data: "venta",
                    render: function(data, type, row) {
                        let venta_id = utils.isDefined(data) && data.id ? data.id : '';
                        let tipo_venta_id = utils.isDefined(data) && data.tipo_venta_id ? data.tipo_venta_id : '';
                        let btn_cotizar = '';
                        let btn_comprobante = '';
                        let btn_change_venta = '';
                        let btn_whatsapo = '';
                        let btn_comments = '';
                        let btn_historico = '';
                        btn_comprobante = "<button title='Imprimir comprobante' onclick='app.imprimirComprobanteCotizador(this)' data-id='" + venta_id + "'  class='btn btn-default btn-sm'><i class='fas fa-file-pdf text-danger'></i></button>";
                        btn_whatsapo = "<button title='Mandar whastapp' onclick='app.goToWhatsapp(this)' data-id='" + venta_id + "'  class='btn btn-default btn-sm mt-2'><i class='fa-brands fa-whatsapp text-success'></i></button>";
                        btn_comments = "<button title='Comentario' onclick='app.comentarioProactivo(this)' data-id='" + row.id + "'  class='btn btn-default mt-2 btn-sm'><i class='fa fa-envelope text-warning'></i></button>";
                        btn_historico = "<button title='Comentario' onclick='app.verHistorioProactivo(this)' data-id='" + row.id + "'  class='btn btn-default mt-2 btn-sm'><i class='fa-solid fa-list text-primary'></i></button>";
                        if (tipo_venta_id == 3) {
                            let folio = utils.isDefined(row.venta) && row.venta.folio ? row.venta.folio.folio : ''
                            btn_cotizar = "<button title='Ir al formato cotización' onclick='app.goToForm(this)' data-id='" + venta_id + "'  class='btn btn-default btn-sm'><i class='fas fa-edit text-warning'></i></button>";
                            btn_change_venta = "<button title='Convertir cotización a venta' onclick='app.updateTipoVenta(this)' data-id='" + venta_id + "' data-folio='" + folio + "'  class='btn btn-default btn-sm text-primary'><i class='fa fa-reply-all'></i></button>";
                        }
                        return btn_cotizar + ' ' + btn_comprobante + ' ' + btn_change_venta + '<br/>' + btn_whatsapo + ' ' + btn_comments + ' ' + btn_historico;
                    }
                }
            ],
            createdRow: function(row, data, dataIndex) {
                let tipo_venta_id = utils.isDefined(data['venta']) && data['venta']['tipo_venta_id'] ? data['venta']['tipo_venta_id'] : '';
                if (tipo_venta_id == 1) {
                    $(row).find('td:eq(0)').css('background-color', '#3fd035');
                }
            }

        }
    }
    goToForm(_this) {
        window.location.href = PATH + '/refacciones/salidas/cotizador/' + window.btoa($(_this).data('id'));
    }
    imprimirComprobanteCotizador(_this) {
        window.location.href = PATH + '/refacciones/salidas/generarComprobanteCotizacion/' + window.btoa($(_this).data('id'));
    }
    goToVentas(folio) {
        window.open(
            PATH + '/refacciones/salidas/listadoVentas?folio=' + folio, '_blank'
        );
    }
    comentarioProactivo(_this) {
        $("#form-comentario-proactivo")[0].reset();
        $("#cotizador_refaccion_id").val($(_this).data('id'));
        $("#usuario_id").val(user_id);
        $("#modal-comentario").modal('show');
    }
    verHistorioProactivo(_this) {
        $("#modal-comentario-historico").modal('show');
        app.getBusquedaComentariosProactivo($(_this).data('id'));
    }
    goToWhatsapp(_this) {
        ajax.get("api/cotizador-refacciones/buscar-por-venta", { 'venta_id': $(_this).data('id') }, function(response, header) {
            let cliente = '';
            let folio = '';
            if (response) {
                cliente = (response.venta.cliente.nombre_cliente);
                folio = (response.venta.folio.folio);
                let msg = 'El cliente: ' + cliente + ' con el número de folio: ' + folio + ' realizo una cotización';
                window.open('https://wa.me/5213320208791?text=' + msg, '_blank');
            }
        });
    }
    getBusqueda_() {
        ajax.get("api/cotizador-refacciones", {}, function(response, header) {
            var listado = $('table#tbl_cotizaciones').DataTable();
            listado.clear().draw();
            if (response && response.length > 0) {
                $(response).each(function(i, value) {
                    if (utils.isDefined(value.venta) && value.venta) {
                        listado.row.add(value)
                    }
                });
                listado.draw();
            }
        });
    }
    updateTipoVenta(_this) {
        utils.displayWarningDialog("Desea convertir la cotización a venta?", "info", function(data) {
            if (data.value) {
                ajax.put(`api/ventas/update-tipo-venta/` + $(_this).data('id'), { tipo_venta_id: 1 }, function(response, headers) {
                    if (headers.status == 200) {
                        utils.displayWarningDialog('Proceso realizado correctamente', 'success', function(result) {
                            app.goToVentas($(_this).data('folio'));

                        });
                    }
                })
            }
        }, true)
    }

    obtenerFechaMostrar(fecha) {
        const dia = 2,
            mes = 1,
            anio = 0;
        fecha = fecha.split('T');
        fecha = fecha[0].split('-');
        return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
    }

    saveComentario() {
        $.isLoading({
            text: "Procesando información ..."
        });
        let form = $('#form-comentario-proactivo').serializeArray();
        var data = {};
        $(form).each(function(i, field) {
            data[field.name] = $.trim(field.value);
        });
        ajax.post(`api/comentario-proactivo`, data, function(response, header) {
            if (header.status == 201 || header.status == 200) {
                $.isLoading("hide");
                utils.displayWarningDialog(header.message, "success", function(data) {
                    $("#modal-comentario").modal('hide');
                })
            } else {
                $.isLoading("hide");
                return ajax.showValidations(header);
            }
        }, false, false)
    }


    getBusquedaComentariosProactivo(cotizador_refaccion_id_) {
        var listado = $('table#tabla_historico_proactivo').DataTable();
        listado.clear().draw();
        ajax.get("api/comentario-proactivo/buscar-por-cotizador", { cotizador_refaccion_id: cotizador_refaccion_id_ }, function(response, header) {
            if (response && response.length > 0) {
                $(response).each(function(i, value) {
                    listado.row.add(value)
                });
                listado.draw();
            }
        });
    }


}

const app = new ClassListadoCotizador();