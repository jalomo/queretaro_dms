class ClassCreateCotizador {
    constructor() {
        this.crearTabla();
        this.getBusqueda();
        $('.money_format').mask("#,##0.00", { reverse: true });
    }

    crearTabla() {
        $('#tbl_detalle_cotizacion').dataTable(this.configuracionTabla());
    }

    configuracionTabla() {
        return {
            language: {
                url: PATH_LANGUAGE
            },
            "searching": false,
            order: [
                [0, 'asc']
            ],
            columns: [{
                    title: "Descripción",
                    data: 'descripcion',
                },
                {
                    title: "Número parte",
                    data: 'numero_parte',
                },
                {
                    title: "Cantidad",
                    data: 'cantidad',
                },
                {
                    title: "Precio neto",
                    data: 'precio_neto',
                    render: function(data, type, row) {
                        return '<span class="money_format">' + utils.isDefined(row.precio_neto) ? row.precio_neto : '0.00' + '</span>'
                    }
                },
                {
                    title: "Total",
                    render: function(data, type, row) {
                        let tot = row.cantidad * row.precio_neto
                        return '<span class="money_format">' + tot.toFixed(2) + '</span>'
                    }
                },

                {
                    title: "Acciones",
                    render: function(data, type, row) {
                        return "<button onclick='app.eliminarDetalleCotizacion(this)' data-id='" + row.id + "' class='btn btn-danger'><i class='fas fa-trash'></i></button>";
                    }
                }
            ],

        }
    }

    eliminarDetalleCotizacion(_this) {
        let registro_id = $(_this).data('id');
        utils.displayWarningDialog("Desea eliminar el registro?", "error", function(data) {
            if (data.value) {
                ajax.delete(`api/detalle-cotizador-refacciones/` + registro_id, null, function(response, headers) {
                    if (headers.status != 400) {
                        toastr.success('Registro eliminado correctamente!');
                    }
                    app.getBusqueda();
                })
            }
        }, true)
    }

    getBusqueda() {
        let total = 0;
        ajax.get("api/detalle-cotizador-refacciones/buscar-por-cotizacion", { cotizador_id: id_cotizador }, function(response, header) {
            var listado = $('table#tbl_detalle_cotizacion').DataTable();
            listado.clear().draw();
            if (response && response.length > 0) {
                $(response).each(function(i, value) {
                    total += parseFloat(value.precio_neto) * parseFloat(value.cantidad);
                    listado.row.add(value)
                });
                listado.draw();
            }
            $("#total_cotizador").html(total.toFixed(2));
            $("#total_cotizador").unmask();
        });
    }

    createCotizacion() {
        $.isLoading({
            text: "Almacenando información ..."
        });
        let form = $('#form-cotizador').serializeArray();
        var data = {};
        $(form).each(function(i, field) {
            data[field.name] = $.trim(field.value);
        });
        ajax.post(`api/cotizador-refacciones`, data, function(response, header) {
            if (header.status == 201 || header.status == 200) {
                window.location.href = PATH + '/refacciones/cotizador/form?cotizador_id=' + response.id
            } else {
                $.isLoading("hide");
                return ajax.showValidations(header);
            }
        }, false, false)
    }

    updateCotizacion() {
        $.isLoading({
            text: "Actualizando información ..."
        });
        let form = $('#form-cotizador').serializeArray();
        var data = {};
        $(form).each(function(i, field) {
            data[field.name] = $.trim(field.value);
        });
        ajax.put(`api/cotizador-refacciones/` + cotizador_id, data, function(response, header) {
            if (header.status == 201 || header.status == 200) {
                $.isLoading("hide");
                toastr.success(header.message);
            } else {
                $.isLoading("hide");
                return ajax.showValidations(header);
            }
        }, false, false)
    }

    openModal() {
        $("#form-producto-cotizador")[0].reset();
        $("#modal-producto-cotizar").modal('show');
    }

    agregarProducto() {
        $.isLoading({
            text: "Procesando información ..."
        });
        let form = $('#form-producto-cotizador').serializeArray();
        var data = {};
        $(form).each(function(i, field) {
            data[field.name] = $.trim(field.value);
        });
        data.cotizador_id = cotizador_id;
        ajax.post(`api/detalle-cotizador-refacciones`, data, function(response, header) {
            if (header.status == 201 || header.status == 200) {
                $.isLoading("hide");
                utils.displayWarningDialog(header.message, "success", function(data) {
                    $("#modal-producto-cotizar").modal('hide');
                    app.getBusqueda();
                })

            } else {
                $.isLoading("hide");
                return ajax.showValidations(header);
            }
        }, false, false)
    }
}

const app = new ClassCreateCotizador();
let cotizador_id = utils.isDefined(id_cotizador) ? id_cotizador : null;
let total = 0;