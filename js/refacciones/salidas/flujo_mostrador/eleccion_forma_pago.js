let tipo_forma_pago_id = $("#tipo_forma_pago_id").val();
class ClassFormaPago {
    constructor() {
        $(".container-forma-pago").hide();
        //this.tipoPago();

    }

    changeTipoPago(_this) {
        let tipo = $(_this).val();
        if (tipo == 2) {
            $(".container-forma-pago").show();
        } else if (tipo == 1) {
            $(".container-forma-pago").hide();
            $("#plazo_credito_id option[value=14]").attr('selected', true);
        }
    }
    tipoPago() {
        return false; //Se quita validación a petición de Angel 24/06/2022
        let tipo_cliente_id = $('input[name=cliente_id]:checked').data('aplica_credito');
        tipo_cliente_id = tipo_cliente_id == 1 ? 3 : 2;

        if (tipo_cliente_id == 2) {
            $("#tipo_forma_pago_id option[value=2]").hide();
        } else {
            $("#tipo_forma_pago_id option[value=2]").show();
        }
        if (tipo_forma_pago_id == 2) {
            $(".container-forma-pago").show();
        } else if (tipo_forma_pago_id == 1) {
            $(".container-forma-pago").hide();
            $("#plazo_credito_id option[value=14]").attr('selected', true);
        }
    }
    validarVenta() {
        cliente_id = $('input[name=cliente_id]:checked').val();
        let tipo_forma_pago_id = $("#tipo_forma_pago_id").val();
        $.isLoading({
            text: "Realizando proceso de venta ...."
        });
        if (parseInt(tipo_forma_pago_id) == 1) {
            app_paso3.procesarCuenta();
        } else if (parseInt(tipo_forma_pago_id) == 2) {
            if (!$("#plazo_credito_id").val()) {
                $.isLoading("hide");
                toastr.error('Favor de elegir plazo de credito');
                return false;
            }
            ajax.post(`api/clientes/tiene-credito`, {
                id: cliente_id
            }, function(response, headers) {
                if (headers.status == 201 || headers.status == 200) {
                    //return app_paso3.validarAplicaCredito(response); Se paso directo a petición de Angel 24/06/2022
                    app_paso3.procesarCuenta();

                }
            });
        } else {
            $.isLoading("hide");
            toastr.error('Favor de elegir si es compra de credito ó contado');
        }
    }

    validarAplicaCredito = (response) => {
        if (!response.aplica_credito) {
            $.isLoading("hide");
            utils.displayWarningDialog("El cliente no cuenta con credito", "warning", function(data) {
                if (data.value) {
                    return false;
                }
            }, true)
        } else if (parseFloat($("#venta_total_").html()) > parseFloat(response.credito_actual)) {
            $.isLoading("hide");
            utils.displayWarningDialog("El cliente solo cuenta con un límite de crédito de: " + response.credito_actual, "warning", function(data) {
                if (data.value) {
                    return false;
                }
            })
        } else if (parseInt($("#plazo_credito_id option:selected").data('cantidad_mes')) > parseInt(response.cantidad_mes ? response.cantidad_mes : 0)) {
            $.isLoading("hide");
            let cantidad_meses = response.cantidad_mes >= 1 ? response.cantidad_mes : 0;
            utils.displayWarningDialog("El cliente solo cuenta con un plazo de credito de " + cantidad_meses + ' meses', "warning", function(data) {
                if (data.value) {
                    return false;
                }
            })
        } else {
            app_paso3.procesarCuenta();
        }

    }
    procesarCuenta() {
        $.isLoading({ text: "Realizando proceso ...." });
        toastr.info('Realizando proceso');
        let data = app_paso3.form();
        if (utils.isDefined(cuenta_cobrar_id) && cuenta_cobrar_id) {
            app_paso3.updateCuenta(data);
        } else {
            app_paso3.createCuenta(data);
        }

    }
    updateCuenta(data) {
        ajax.put(`api/cuentas-por-cobrar/update-cuenta-venta/` + cuenta_cobrar_id, data, function(response, headers) {
            if (headers.status == 201 || headers.status == 200) {
                $.isLoading("hide");
                utils.displayWarningDialog('El registro se ha actualizado en módulo de cajas correctamente', 'success', function(result) {
                    window.location.href = PATH + '/refacciones/salidas/listadoVentas'
                });
            } else {
                $.isLoading("hide");
            }
        })
    }
    createCuenta(data) {
        $.isLoading("hide");
        ajax.post(`api/cuentas-por-cobrar/crear-cuenta-venta`, data, function(response, headers) {
            if (headers.status == 201 || headers.status == 200) {
                utils.displayWarningDialog('El registro se envio al módulo de cajas correctamente', 'success', function(result) {
                    window.location.href = PATH + '/refacciones/salidas/listadoVentas'
                });
            } else {
                $.isLoading("hide");
            }
        })
    }
    getProductosConcepto() {
        let row = [];
        ajax.get(`api/refacciones/venta/` + venta_id, {}, function(response, header) {
            if (header.status == 200) {
                row = utils.isDefined(response.aaData) ? response.aaData.shift() : [];
                $("#concepto").val(utils.isDefined(row.descripcion_producto) ? row.no_identificacion + ' - ' + row.descripcion_producto : '');
            }
        });
    }
    form = () => {
        return {
            folio_id: folio_id,
            tipo_forma_pago_id: $('#tipo_forma_pago_id').val(),
            plazo_credito_id: $('#plazo_credito_id option:selected').val() ? $('#plazo_credito_id option:selected').val() : 14,
            concepto: $('#concepto').val(),
        }
    }

}

const app_paso3 = new ClassFormaPago();