class ClassEleccionProductos {

    constructor() {
        this.getBusquedaProductosDisponibles();
        this.getTotales();
        setTimeout(() => {
            this.getCarritoCompras();
        }, 1000);

        setTimeout(() => {
            if (utils.isDefined(estatus_cuenta_id) && estatus_cuenta_id > 1) {
                $(".btn-flujo").html("");
                $(".btn-flujo").hide();
            }
        }, 3000);


    }
    configuracionTabla() {
        return {
            ajax: {
                url: PATH_API + "api/productos/listadoStock",
                type: 'GET',
                data: function(data) {
                    data.descripcion = document.getElementById('descripcion_form').value
                    data.no_identificacion = document.getElementById('no_identificacion_form').value
                },
            },
            language: {
                url: PATH_LANGUAGE
            },
            processing: true,
            serverSide: true,
            bFilter: false,
            order: [
                [0, 'asc']
            ],
            columns: [{
                    title: "#",
                    data: 'id',
                },
                {
                    title: "No. identificación",
                    data: 'no_identificacion',
                },
                {
                    title: "Descripcion",
                    data: 'descripcion',
                },
                {
                    title: "Valor unitario",
                    data: 'valor_unitario',
                },
                {
                    title: "Ubicación producto",
                    data: 'ubicacionProducto',
                },
                {
                    title: "Existencia <br />total producto",
                    data: 'cantidad_actual',
                },
                {
                    title: "Existencia <br />almacen principal",
                    data: 'cantidad_almacen_primario',
                },
                {
                    title: "Existencia <br />almacen secundario",
                    data: 'cantidad_almacen_secundario',
                },
                {
                    title: "Acciones",
                    render: function(data, type, row) {
                        return "<button onclick='app_paso2.modalProducto(this)' data-product_name='" + row.descripcion + "' data-product_id='" + row.id + "' data-valor_unitario='" + row.valor_unitario + "' data-costo_promedio='" + row.costo_promedio + "' data-toggle='modal'  class='btn btn-success btn-flujo'><i class='fas fa-shopping-cart'></i></button>";
                    }
                }
            ]
        }
    }
    getBusquedaProductosDisponibles() {
        return $('#tbl_productos').DataTable(this.configuracionTabla());
    }

    filtrarProductosDisponibles = () => {
        $('#tbl_productos').DataTable().ajax.reload()
    }
    getCarritoCompras = () => {
        return $('#tbl_carrito').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            searching: false,
            bFilter: false,
            order: [
                [0, 'asc']
            ],
            "ajax": {
                url: base_url + "refacciones/salidas/ajax_detalle_venta_carrito",
                type: 'POST',
                data: { id: venta_id }
            },
            columns: [{
                    title: 'id',
                    data: 'id',
                    visible: false
                },
                {
                    title: 'No identificación',
                    render: function(data, type, row) {
                        return utils.isDefined(row.no_identificacion) && row.no_identificacion ? row
                            .no_identificacion : null
                    }
                },
                {
                    title: 'Descripción',
                    render: function(data, type, row) {
                        return utils.isDefined(row.descripcion_producto) && row.descripcion_producto ? row
                            .descripcion_producto : null
                    }
                },
                {
                    title: 'Precio',
                    render: function(data, type, row) {
                        return utils.isDefined(row.valor_unitario) ? "$ " + row.valor_unitario : null
                    }
                },
                {
                    title: 'Cantidad',
                    render: function(data, type, row) {
                        return utils.isDefined(row.cantidad) && row.cantidad ? row.cantidad : null
                    }
                },
                {
                    title: 'Total',
                    render: function(data, type, row) {
                        return utils.isDefined(row.venta_total) ? "$ " + row.venta_total : null
                    }
                },
                {
                    title: 'Unidad',
                    render: function(data, type, row) {
                        return utils.isDefined(row.unidad) && row.unidad ? row.unidad : null
                    }
                },
                {
                    title: 'Core',
                    render: function(data, type, row) {
                        return utils.isDefined(row.core) ? "$ " + row.core : null
                    }
                },
                {
                    title: 'Ubicación producto',
                    render: function(data, type, row) {
                        return utils.isDefined(row.ubicacion_producto) && row.ubicacion_producto ? row.ubicacion_producto : null
                    }
                },
                {
                    render: function(data, type, row) {
                        return "<button type='button' onclick='app_paso2.borrar(this)' class='btn-borrar btn btn-primary btn-flujo' data-id=" + row.id + "><i class='fas fa-trash'></i></button>";
                    }
                }
            ]
        });
    }
    modalProducto(_this) {
        var producto_name = $(_this).data('product_name');
        $("#producto_id").val($(_this).data('product_id'));
        $("#valor_unitario").val($(_this).data('valor_unitario'));
        $("#costo_promedio").val($(_this).data('costo_promedio'));
        $("#costo_original").val($(_this).data('valor_unitario'));
        $("#tipo_precio").val('');
        $("#precio_costo").val('');
        $(".total_producto").val('');
        $("#core").val('0');
        $("#costo_adicional").val('');
        $("#porcentaje_manual").val('');
        $("#precio_costo_manual").val('');
        $("#title_modal_producto").text(producto_name);
        $("#modal-producto-detalle").modal('show');
        app_paso2.changeTipoPrecio(this)
    }
    agregarProducto() {
        if (!$("#tipo_precio").val()) {
            toastr.error("Falta seleccionar un tipo de precio");
            return false;
        }
        let valor_unitario = $('#tipo_precio option:selected').val() == 100 ? $("#precio_costo_manual").val() : $('#precio_costo').val();
        if (valor_unitario == '') {
            toastr.error("Falta indicar un precio");
            return false;
        }
        // app_paso2.validateAgregarProducto();
        toastr.info("Añadiendo producto al carro de compras");
        $.isLoading({
            text: "Añadiendo producto al carro de compras...."
        });
        let form = this.procesar_form('form-producto');
        form.valor_unitario = $('#tipo_precio option:selected').val() == 100 ? $("#precio_costo_manual").val() : $('#precio_costo').val()
        form.folio_id = folio_id;
        form.venta_id = venta_id;
        form.cliente_id = cliente_id;
        form.tipo_descuento_id = $("#tipo_precio").val();
        form.core = $("#core").val();
        ajax.post(`api/venta-producto`, form, function(response, headers) {
            if (headers.status == 201 || headers.status == 200) {
                app_paso2.actualizarTotales();
                $("#modal-producto-detalle").modal('hide');
                $.isLoading("hide");
                utils.displayWarningDialog("Producto registrado correctamente!", 'success', function(result) {
                    $('#tbl_carrito').DataTable().ajax.reload()
                });
            } else {
                $.isLoading("hide");
            }
        })
    }
    validateAgregarProducto = () => {
        if (!$("#tipo_precio").val()) {
            toastr.info("Falta seleccionar un tipo de precio");
            return false;
        }
        let valor_unitario = $('#tipo_precio option:selected').val() == 100 ? $("#precio_costo_manual").val() : $('#precio_costo').val();
        if (valor_unitario == '') {
            toastr.info("Falta indicar un precio");
            return false;
        }
    }
    changeTipoPrecio(_this) {
        let core = 0;
        let calcula_total = 0;
        let tipo = $("#tipo_precio option:selected").val();
        let porcentaje = $("#tipo_precio option:selected").data('porcentaje');
        let cantidad = $("#cantidad").val();
        core = $("#core").val();
        let total = app_paso2.calcularPrecioCosto(porcentaje);
        let precio_total = 0;
        if (tipo == 100) {
            $(".total_producto").val('');
            $(".precio_costo").hide();
            $(".precio_manual").show();
        } else {
            $(".precio_costo").show();
            $(".precio_manual").hide();
            if (!isNaN(total)) {
                $("#precio_costo").val(total);
                calcula_total = ((parseFloat(total) * parseInt(cantidad)));
                if (core > calcula_total) {
                    toastr.warning("El core no puede ser mayor al total");
                    return false;
                }
                precio_total = ((parseFloat(total) * parseInt(cantidad)) - parseFloat(core));

                $(".total_producto").val(precio_total.toFixed(2));
            }
        }
    }
    calcularPrecioCosto(porcentaje_value) {
        let calculo = 0;
        let precio_original = parseFloat($("#costo_original").val());
        let precio_distribuidora = parseFloat($("#costo_promedio").val());
        let precio_factura = parseFloat($("#valor_unitario").val());
        let porcentaje = parseFloat(porcentaje_value);
        if (porcentaje >= 0.1) {
            if (precio_original == precio_factura) {
                calculo = precio_distribuidora / porcentaje;
            } else {
                calculo = precio_factura / porcentaje;
            }
        } else {

            calculo = precio_factura;
        }
        return calculo.toFixed(2);
    }
    calculoManual() {
        let core = 0;
        let calcula_total = 0;
        let porcentaje_manual = $("#porcentaje_manual").val();
        let cantidad = $("#cantidad").val();
        let precio_total = 0;
        let costo_adicional = 0;
        core = $("#core").val();
        if (porcentaje_manual > 99) {
            toastr.error("Valor ingresado no valio");
            return $("#porcentaje_manual").val('0');
        } else {
            let total = app_paso2.calcularPrecioCosto(porcentaje_manual);
            if (!isNaN(total)) {
                costo_adicional = $("#costo_adicional").val();
                precio_total = (parseFloat(total) * parseInt(cantidad));
                if (!isNaN(costo_adicional) && costo_adicional >= 1) {
                    let total_manual = parseFloat(total) + parseFloat(costo_adicional);
                    calcula_total = ((parseFloat(total) * parseInt(cantidad)) + parseFloat(costo_adicional));
                    precio_total = ((parseFloat(total) * parseInt(cantidad)) + parseFloat(costo_adicional) - parseFloat(core));
                    if (core > calcula_total) {
                        toastr.warning("El core no puede ser mayor al total");
                        return false;
                    }
                    $("#precio_costo_manual").val(total_manual);
                } else {
                    $("#precio_costo_manual").val(total);
                }
                $(".total_producto").val(precio_total.toFixed(2));
            }
        }
    }
    procesar_form(form_id) {
        let form = $('#' + form_id).serializeArray();
        var data = {};
        $(form).each(function(i, field) {
            data[field.name] = $.trim(field.value);
        });
        return data;
    }
    actualizarTotales() {
        let form = app_paso2.procesar_form('form-venta');
        form.cliente_id = $('input[name=cliente_id]:checked').val();
        let tipo_cliente_id = $('input[name=cliente_id]:checked').data('aplica_credito');
        form.tipo_cliente_id = tipo_cliente_id == 1 ? 3 : 2;
        form.folio_id = folio_id;
        form.vendedor_id = vendedor_id;
        ajax.put('api/ventas/editar-solo-venta/' + venta_id, form, function(response, header) {
            app_paso2.getTotales();
        }, false, false)
    }
    modificarVenta() {
        let table_carrito_length = $("#tbl_carrito").dataTable().fnSettings().aoData.length
        if (table_carrito_length == 0) {
            utils.displayWarningDialog("No se ha seleccionado ningún producto");
            return false;
        }
        $.isLoading({
            text: "Actualizando venta ..."
        });
        let form = app_paso2.procesar_form('form-venta');
        form.cliente_id = $('input[name=cliente_id]:checked').val();
        let tipo_cliente_id = $('input[name=cliente_id]:checked').data('aplica_credito');
        form.tipo_cliente_id = tipo_cliente_id == 1 ? 3 : 2;
        form.vendedor_id = vendedor_id;
        // form.folio_id = folio_id;
        ajax.put('api/ventas/editar-solo-venta/' + venta_id, form, function(response, header) {
            if (header.status == 201 || header.status == 200) {
                $.isLoading("hide");
                if (tipo_venta_id == 1) {
                    utils.displayWarningDialog("Venta procesada correctamente", 'success', function(result) {
                        app.goToTab(3);
                    });
                } else {
                    utils.displayWarningDialog("Cotización procesada correctamente", 'success', function(result) {
                        window.location.href = PATH + 'refacciones/cotizador/index';
                    });
                }
            } else {
                $.isLoading("hide");
            }
        })
    }
    getTotales() {
        ajax.get(`api/ventas/venta-totales/` + venta_id, false, function(response, headers) {
            if (headers.status == 201 || headers.status == 200) {
                $("#subtotal_").html(utils.isDefined(response.subtotal) ? parseFloat(response.subtotal).toFixed(2) : '0.00');
                $("#iva_").html(utils.isDefined(response.iva) ? parseFloat(response.iva).toFixed(2) : '0.00');
                $("#venta_total_").html(utils.isDefined(response.venta_total) ? parseFloat(response.venta_total).toFixed(2) : '0.00');
            }
        }, false, false)
    }
    borrar(_this) {
        utils.displayWarningDialog("Desea quitar el producto?", "warning", function(data) {
            if (data.value) {
                ajax.delete(`api/venta-producto/` + $(_this).data('id'), null, function(response, headers) {
                    if (headers.status == 204) {
                        utils.displayWarningDialog('Registro eliminado correctamente', 'success', function(result) {
                            app_paso2.actualizarTotales();
                            $('#tbl_carrito').DataTable().ajax.reload()
                        });
                    }
                })
            }
        }, true)
    }
}
const app_paso2 = new ClassEleccionProductos();

$("#valor_unitario").on('blur', function() {
    app_paso2.changeTipoPrecio();
    if ($("#tipo_precio option:selected").val() == 100) {
        app_paso2.calculoManual();
    }
});
$("#cantidad").on('blur', function() {
    app_paso2.changeTipoPrecio();
    if ($("#tipo_precio option:selected").val() == 100) {
        app_paso2.calculoManual();
    }
});

$("#core").on('blur', function() {
    app_paso2.changeTipoPrecio();
    if ($("#tipo_precio option:selected").val() == 100) {
        app_paso2.calculoManual();
    }
});

$("#porcentaje_manual").on("keyup", function() {
    return app_paso2.calculoManual();
});
$("#costo_adicional").on("keyup", function() {
    return app_paso2.calculoManual();
});

$(".buscar_enter").keypress(function(e) {
    if (e.which == 13) {
        app_paso2.filtrarProductosDisponibles();
    }
});