let tipo_cliente_id = $("#tipo_cliente_id").val();
let tipo_venta_id = $("#tipo_venta_id").val();
var table_clientes = 'table#tbl_cliente';
let cliente_id_ = '';
class ClassDatosVenta {

    constructor() {
        if (utils.isDefined(estatus_cuenta_id) && estatus_cuenta_id > 1) {
            $(".btn-flujo").html("");
            $(".btn-flujo").hide();
        }
        if (utils.isDefined(venta_id) && venta_id >= 1) {
            this.tipoCliente(tipo_cliente_id);
            this.tipoModeloByMarca();
            this.goToTab(2);
        } else {
            this.goToTab(1);
            // ajax.getCatalogo('clientes/select-catalogo?aplica_credito=false', $(".select_cliente"), true);
        }
        this.create_table_clientes();
        this.filtrarTablaClientes();
        this.tipoVenta(tipo_venta_id);
        $('#no_serie_').mask('AAAAAAAAAAAAAAAAA');

    }
    generateVenta() {
        $.isLoading({
            text: "Generando proceso ..."
        });
        if (!$("#fecha_venta").val()) {
            $.isLoading("hide");
            $("#fecha_venta").focus();
            toastr.error("Favor de indicar la fecha de venta");
            return false;
        }
        if ($('input[name=cliente_id]').is(":checked") != true) {
            $.isLoading("hide");
            utils.displayWarningDialog('Falta seleccionar el cliente', 'warning', function(result) {
                return false;
            });
        } else {
            let form = this.procesar_form('form-venta');
            let tipo_cliente_id = $('input[name=cliente_id]:checked').data('aplica_credito');
            form.tipo_cliente_id = tipo_cliente_id == 1 ? 3 : 2;
            form.vendedor_id = vendedor_id;
            ajax.post(`api/ventas/generar-venta`, form, function(response, header) {
                if (header.status == 201 || header.status == 200) {
                    if (tipo_venta_id == 1) {
                        toastr.success("Generando venta");
                        window.location.href = PATH + '/refacciones/salidas/venta_mostrador/' + window.btoa(response.id)

                    } else {
                        toastr.success("Generando cotización");
                        window.location.href = PATH + '/refacciones/salidas/cotizador/' + window.btoa(response.id)
                    }
                } else {
                    $.isLoading("hide");
                }
            })
        }
    }
    validaRFC(rfcstring) {
        console.log(document.getElementById('regimen_fiscal').value);
        if (!document.getElementById('regimen_fiscal').value) {
            toastr.error("Favor de elegir el Régimen Fiscal");
            return false;
        }
        var strCorrecta;
        strCorrecta = rfcstring;
        if (document.getElementById('regimen_fiscal').value == 'F') {
            var valid = '^(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
        } else {
            var valid = '^(([A-Z]|[a-z]|\s){1})(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
        }
        var validRfc = new RegExp(valid);
        var matchArray = strCorrecta.match(validRfc);
        if (matchArray == null) {
            toastr.error("Cadena incorrectas");
            return false;
        } else {
            toastr.success("Cadena correcta: " + strCorrecta);
            return true;
        }

    }
    modificarVenta() {
        $.isLoading({
            text: "Actualizando información ..."
        });
        if (!$("#fecha_venta").val()) {
            $.isLoading("hide");
            $("#fecha_venta").focus();
            toastr.error("Favor de indicar la fecha de venta");
            return false;
        }
        let form = this.procesar_form('form-venta');
        let tipo_cliente_id = $('input[name=cliente_id]:checked').data('aplica_credito');
        form.tipo_cliente_id = tipo_cliente_id == 1 ? 3 : 2;
        form.folio_id = folio_id;
        form.vendedor_id = vendedor_id;
        ajax.put('api/ventas/editar-solo-venta/' + venta_id, form, function(response, header) {
            if (header.status == 201 || header.status == 200) {
                toastr.success("Información actualizada");
                app.goToTab(2);
                $.isLoading("hide");
            } else {
                $.isLoading("hide");
            }
        })
    }
    openModalAltaCliente() {
        $("#form-cliente")[0].reset();
        $("#regimen_fiscal_id").val('').trigger('change');
        $('#telefono').mask('0000000000');
        $('#codigo_postal').mask('99999');
        $('#rfc').mask('AAAAAAAAAAAAA');
        $("#modal-alta-cliente").modal('show');
        $("#title_modal_cliente").html('Agregar cliente');
        $("#btn_editar_cliente").hide();
        $("#btn_agregar_cliente").show();
        $("#datos_credito").hide();
        // setTimeout(() => {
        //     this.generar_numero_cliente();
        // }, 1000);
    }
    changeTipoCliente(_this) {
        tipo_cliente_id = $(_this).val();
        app.tipoCliente($(_this).val());
    }
    changeTipoVenta(_this) {
        app.tipoVenta($(_this).val());
    }
    tipoVenta(tipo_venta) {
        if (tipo_venta == 3) { // Tipo cotización
            ajax.getCatalogo('catalogo-marcas/catalogo', $("select[name*='marca_id']"), true);
            $("#datos_cotizador").show();
            $("#btn_comprobante_cotizacion").show();
        } else if (tipo_venta == 1) {
            $("#btn_comprobante_cotizacion").hide();
            $("#datos_cotizador").hide();
        }
    }
    tipoModeloByMarca(_this) {
        let marca_id = utils.isDefined($(_this).val()) ? $(_this).val() : $("select[name*='marca_id']").val();
        if (marca_id) {
            ajax.getCatalogo('catalogo-modelos/buscar-por-marca?id_marca=' + marca_id, $("select[name*='modelo_id']"), true);
        }
    }
    tipoCliente(tipo_cliente) {
        if (tipo_cliente == 2) { // Registrado
            ajax.getCatalogo('clientes/select-catalogo?aplica_credito=false', $(".select_cliente"), true);
        } else if (tipo_cliente == 3) { // Con crédito
            ajax.getCatalogo('clientes/select-catalogo?aplica_credito=true', $(".select_cliente"), true);
        }
    }
    guardarCliente() {
        let form = this.procesar_form('form-cliente');
        ajax.post(`api/clientes/crear-cliente-modal`, form, function(response, header) {
            if (header.status == 200) {
                $("#numero_cliente_").val(response.numero_cliente);
                app.filtrarTablaClientes();
                $("#modal-alta-cliente").modal('hide');
            }
        });
    }
    updateCliente() {
        let form = this.procesar_form('form-cliente');
        //cliente_id = $('input[name=cliente_id]:checked').val();
        ajax.put(`api/clientes/update-cliente-modal/` + cliente_id_, form, function(response, header) {
            if (header.status == 200) {
                utils.displayWarningDialog('Cliente actualizado correctamente', 'success', function(result) {
                    $("#modal-alta-cliente").modal('hide');
                    app.filtrarTablaClientes();
                    app.getDatosCliente();
                });
            }
        });
    }
    getDatosCliente(_this) {
        $("#telefono_cliente").val('');
        $("#correo_cliente").val('');
        $("[name='marca']").val('');
        $("[name='modelo']").val('');
        $("[name='anio']").val('');
        $("[name='no_serie']").val('');

        let datos = [];
        let cliente_id = $(_this).val() ? $(_this).val() : $(".select_cliente option:selected").val();
        if (cliente_id) {
            ajax.get(`api/clientes/` + cliente_id, {}, function(response, header) {
                if (header.status == 200) {
                    datos = response.shift();
                    $("#telefono_cliente").val(utils.isDefined(datos.telefono) ? datos.telefono : '');
                    $("#correo_cliente").val(utils.isDefined(datos.correo_electronico) ? datos.correo_electronico.toLowerCase() : '');
                }
            });
            if (tipo_venta_id == 3) {
                ajax.get(`api/ventas/venta-cotizacion-por-cliente/` + cliente_id, {}, function(response, header) {
                    let cotizacion = [];
                    if (header.status == 200) {
                        cotizacion = utils.isDefined(response.cotizacion) ? response.cotizacion : [];
                        $.each(cotizacion, function(indexInArray, valueOfElement) {
                            $("[name='" + indexInArray + "']").val(valueOfElement);
                        });
                        $("select[name*='marca_id']").trigger('change');
                    }
                });
            }
        }
    }
    openModalEditarCliente(_this) {
        $("#form-cliente")[0].reset();
        $("#regimen_fiscal_id").val('').trigger('change');
        $.isLoading({ text: "Obteniendo datos cliente...." });
        let datos = [];
        cliente_id_ = $(_this).data('cliente_id');

        if (!cliente_id_) {
            $.isLoading("hide");
            toastr.warning("Favor de seleccionar un cliente");
            return false;
        }

        $("#modal-alta-cliente").modal('show');
        $('#telefono').mask('0000000000');
        $('#codigo_postal').mask('99999');
        $('#rfc').mask('AAAAAAAAAAAAA');
        $("#title_modal_cliente").html('Editar cliente');
        $("#btn_editar_cliente").show();
        $("#btn_agregar_cliente").hide();
        // Obtener los datos del cliente y completar el formulario con ellos
        const obtenerDatosCliente = () => {
            return new Promise((resolve, reject) => {
                ajax.get(`api/clientes/` + cliente_id_, {}, function(response, header) {
                    if (header.status == 200) {
                        datos = response.shift();
                        $.each(datos, function(indexInArray, valueOfElement) {
                            $("[name='" + indexInArray + "']").val(valueOfElement);

                            if (indexInArray == 'regimen_fiscal_id') {
                                $("[name='" + indexInArray + "']").attr('data', valueOfElement);
                            }
                        });
                        resolve();
                    } else {
                        reject();
                    }
                });
            });
        };
        // Verificar si el cliente tiene crédito y actualizar los campos correspondientes
        const obtenerDatosCredito = () => {
            return new Promise((resolve, reject) => {
                ajax.post('api/clientes/tiene-credito', { id: cliente_id_ }, function(response, header) {
                    if (header.status == 200) {
                        $("#aplica_credito").val(utils.isDefined(response.aplica_credito) && response.aplica_credito == 1 ? 'Tiene Crédito' : 'Sin Crédito');
                        $("#limite_credito").val(utils.isDefined(response.limite_credito) ? parseFloat(response.limite_credito).toFixed(2) : '');
                        $("#plazo_credito").val(utils.isDefined(response.plazo_credito) ? response.plazo_credito : '');
                        $("#credito_actual").val(utils.isDefined(response.credito_actual) ? parseFloat(response.credito_actual).toFixed(2) : '');
                        resolve();
                    } else {
                        reject();
                    }
                }, false, false);
            });
        };

        const ejecutarChangeRegimen = () => {
            return new Promise((resolve, reject) => {
                this.changeRegimen();
                resolve();
            });
        };

        (async() => {
            try {
                await obtenerDatosCliente();
                $("#datos_credito").show();
                await obtenerDatosCredito();
                await ejecutarChangeRegimen();
            } catch (error) {
                console.error(error);
            } finally {
                $.isLoading("hide");
            }
        })();

    }

    procesar_form(form_id) {
        let form = $('#' + form_id).serializeArray();
        var data = {};
        $(form).each(function(i, field) {
            data[field.name] = $.trim(field.value);
        });
        return data;
    }
    changeRegimen = () => {
        let nombre = $("#nombre").val();
        let apellidoPaterno = $("#apellido_paterno").val();
        let apellidoMaterno = $("#apellido_materno").val();
        let nombreCompleto = `${nombre} ${apellidoPaterno} ${apellidoMaterno}`;
        let params = '';
        let regimenFiscal = document.getElementById('regimen_fiscal').value;
        if (regimenFiscal === 'M') {
            params = { persona_moral: true };
        } else if (regimenFiscal === 'F') {
            $("#nombre_empresa").val(nombreCompleto);
            params = { persona_fisica: true };
        } else if (regimenFiscal === 'A') {
            params = { persona_fisica: true, persona_moral: true };
        }
        console.log(params);
        ajax.getCatalogo('regimen-fiscal?' + jQuery.param(params), $("select[name*='regimen_fiscal_id']"), true);
    }

    create_table_clientes() {
        $(table_clientes).DataTable(this.settings_table());
    }

    filtrarTablaClientes = () => {
        $(table_clientes).DataTable().ajax.reload()
    }

    settings_table() {
        return {
            ajax: {
                url: PATH_API + 'api/clientes/busqueda',
                type: 'GET',
                data: function(data) {
                    data.id = document.getElementById('cliente_id_').value;
                    data.numero_cliente = document.getElementById('numero_cliente_').value;
                    data.nombre = document.getElementById('nombre_').value;
                    data.apellido_paterno = document.getElementById('apellido_paterno_').value;
                    data.apellido_materno = document.getElementById('apellido_materno_').value;
                    data.rfc = document.getElementById('rfc_').value;
                    //data.aplica_credito = document.getElementById('tipo_cliente_id').value == 3 ? 1 : false;
                },
            },
            language: {
                url: PATH_LANGUAGE
            },
            processing: true,
            serverSide: true,
            searching: false,
            bFilter: false,
            lengthChange: false,
            bInfo: true,
            order: [
                [0, 'asc']
            ],
            columns: [{
                    title: "#",
                    width: "20px",
                    render: function(data, type, row) {
                        let checked = row.id == $("#cliente_id_").val() ? 'checked="true"' : false;
                        return '<div class="pt-2 pb-2"><input ' + checked + ' type="radio" onclick="app.getDatosCliente(this)" class="seleccion" name="cliente_id" id="cliente_id" data-aplica_credito="' + row.aplica_credito + '" value="' + row.id + '"/></div>';
                    }
                },
                {
                    title: 'Número cliente',
                    data: function(data) {
                        return data.numero_cliente
                    }
                },
                {
                    title: 'Razón social',
                    data: 'nombre_empresa'

                },
                {
                    title: 'Nombre cliente',
                    data: function(data) {
                        return data.nombre + ' ' + data.apellido_paterno + ' ' + data.apellido_materno
                    }
                },
                {
                    title: 'RFC',
                    data: 'rfc'
                },
                {
                    title: 'Teléfono',
                    data: 'telefono',
                },
                {
                    title: 'Tiene crédito',
                    data: 'aplica_credito',
                    render: function(data, type, row) {
                        return data && data == true ? 'Si' : 'No';
                    }
                },
                {
                    title: 'Plazo crédito',
                    data: 'plazo_credito',
                    render: function(data, type, row) {
                        return data && data.id ? data.nombre : 'Sin plazo';
                    }
                },
                {
                    title: 'Límite crédito',
                    data: 'limite_credito',
                    render: function(data, type, row) {
                        return data ? '<span class="money_format">' + data + '</span>' : '';
                    }
                },
                {
                    title: 'Acciones',
                    data: 'aplica_credito',
                    render: function(data, type, row) {
                        return '<button type="button" title="Editar Cliente" data-cliente_id="' + row.id + '" onclick="app.openModalEditarCliente(this)" class="btn btn-warning mt-1"><i class="fa fa-edit" style="font-size:18px"></i></button>'
                    }

                },
            ]
        }
    }


    goToTab = (paso) => {
        $(".nav-link").removeClass('active');
        $(".tab-pane").removeClass('active');
        $("#nav_paso" + paso).addClass('active')
        $("#paso" + paso).addClass('active')
        if (paso == 3) {
            $("body").scrollTop(50)
            app_paso3.getProductosConcepto();
            app_paso3.tipoPago();
        }
    }
}

const app = new ClassDatosVenta();