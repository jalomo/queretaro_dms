function generar_numero_cliente() {
    var cat_clave = document.getElementById("tipo_registro");
    var categoria = cat_clave.options[cat_clave.selectedIndex].text;
    var tipo_cliente = categoria.split("-");
    var consecutivo = Math.floor(Math.random() * 1000);
    var folio = "";
    consecutivo++;
    if (consecutivo < 10) {
        folio = tipo_cliente[0] + "0000" + consecutivo;
    } else if ((consecutivo > 9) && (consecutivo < 100)) {
        folio = tipo_cliente[0] + "000" + consecutivo;
    } else if ((consecutivo > 99) && (consecutivo < 1000)) {
        folio = tipo_cliente[0] + "00" + consecutivo;
    } else if ((consecutivo > 9999) && (consecutivo < 10000)) {
        folio = tipo_cliente[0] + "0" + consecutivo;
    } else {
        folio = tipo_cliente[0] + "" + consecutivo;
    }
    $("#numero_cliente").val(folio);
}

function guardarCliente() {
    generar_numero_cliente();
    ajax.post(`api/clientes/crear-regresar`, {
        tipo_registro: document.getElementById('tipo_registro').value,
        numero_cliente: document.getElementById('numero_cliente').value,
        nombre: document.getElementById('nombre').value,
        apellido_paterno: document.getElementById('apellido_paterno').value,
        apellido_materno: document.getElementById('apellido_materno').value,
        regimen_fiscal: document.getElementById('regimen_fiscal').value,
        rfc: document.getElementById('rfc').value,
        nombre_empresa: document.getElementById('nombre_empresa').value,
        direccion: document.getElementById('direccion').value
    }, function(response, header) {
        if (header.status == 200) {
            $(".form-registrar-cliente-esporadico #cliente_id").val(response.id);
            $(".form-registrar-cliente-esporadico #nombre_cliente_esporadico").val(response.numero_cliente + ' - ' + response.nombre + ' ' + response.apellido_paterno + ' ' + response.apellido_materno);
            $("#modal-agregar-usuario").modal('hide');
        }
    });
}

function changeTipoPrecio(_this) {
    let tipo = $("#tipo_precio option:selected").val();
    let porcentaje = $("#tipo_precio option:selected").data('porcentaje');
    let total = calcularPrecioCosto(porcentaje);

    if (tipo == 100) {
        $(".precio_costo").hide();
        $(".precio_manual").show();
    } else {
        $(".precio_costo").show();
        $(".precio_manual").hide();
        if (total > 1) {
            $("#precio_costo").val(total);
        }
    }
}

function calcularPrecioCosto(porcentaje_value) {
    let precio_factura = parseFloat($("#precio_factura").val());
    let porcentaje = parseFloat(porcentaje_value);
    let calculo = precio_factura / porcentaje;
    return calculo.toFixed(2);
}

$("#precio_factura").on('blur', function() {
    changeTipoPrecio();
    calculoManual();
});

$("#porcentaje_manual").on("keyup", function() {
    return calculoManual();
});

function calculoManual() {
    let porcentaje_manual = $("#porcentaje_manual").val();
    if (porcentaje_manual > 99) {
        toastr.error("Valor ingresado no valio");
        return $("#porcentaje_manual").val('0');
    } else {
        let total = calcularPrecioCosto(porcentaje_manual);
        if (total >= 1) {
            $("#precio_costo_manual").val(total);
        }
    }
}

function cerrarmodales() {
    let default_input = $(".tipo_precio")[0];
    $(default_input).prop("checked", true);
}

function openModalDetalleCliente() {
    let cliente_id = $(".select_cliente option:selected").val();
    if (!cliente_id) {
        toastr.warning("Favor de seleccionar un cliente");
        return false;
    }
    ajax.post('api/clientes/tiene-credito', { id: cliente_id }, function(response, header) {
        if (header.status == 200) {
            console.log(response);
            $(".detalle-numero_cliente").html(utils.isDefined(response.numero_cliente) ? response.numero_cliente : '');
            $(".detalle-nombre_cliente").html(utils.isDefined(response.nombre_cliente) ? response.nombre_cliente : '');
            $(".detalle-rfc").html(utils.isDefined(response.rfc) ? response.rfc : '');
            $(".detalle-aplica_credito").html(utils.isDefined(response.aplica_credito) && response.aplica_credito == 1 ? 'Tiene Crédito' : 'Sin Crédito');
            $(".detalle-limite_credito").html(utils.isDefined(response.limite_credito) ? response.limite_credito : '');
            $(".detalle-plazo_credito").html(utils.isDefined(response.plazo_credito) ? response.plazo_credito : '');
            $(".detalle-credito_actual").html(utils.isDefined(response.credito_actual) ? response.credito_actual : '');
            $("#modal-detalle-cliente").modal('show');
        }
    }, false, false);
}

$(document).ready(function() {
    $(".cliente-registrado #cliente_id").select2();
    $(".form-registrar-cliente-esporadico").hide();
    $(".cliente-registrado").hide();

    $("#tipo_cliente_id").on("change", function() {
        if ($("#tipo_cliente_id").val() == '') {
            $(".form-registrar-cliente-esporadico").hide();
            $(".cliente-registrado").hide();
        } else if ($("#tipo_cliente_id").val() == 1) {
            $("#modal-agregar-usuario").modal('show');
            setTimeout(() => {
                generar_numero_cliente();
            }, 1000);
            $(".cliente-registrado").hide();
            $(".form-registrar-cliente-esporadico").show();
        } else if ($("#tipo_cliente_id").val() == 2) {
            $(".form-registrar-cliente-esporadico").hide();
            $(".cliente-registrado").show();
            ajax.getCatalogo('clientes/select-catalogo?aplica_credito=false', $(".select_cliente"), true);
        } else if ($("#tipo_cliente_id").val() == 3) {
            $(".form-registrar-cliente-esporadico").hide();
            $(".cliente-registrado").show();
            ajax.getCatalogo('clientes/select-catalogo?aplica_credito=true', $(".select_cliente"), true);
        }
    });



    $("#agregar_vender").on("click", function() {
        if ($("#cantidad").val() == 0) {
            return utils.displayWarningDialog("Indicar numero de piezas", "warning", function(data) {})
        }

        if (document.getElementById("cliente_id").value == '' && $('.hidden-dinamic-client-id').val() == '') {
            $("#tipo_cliente_id_error").text('seleccionar tipo de cliente').css('display', 'block');
            $('html,body').animate({
                scrollTop: $('#tipo_cliente_id_error').offset().top
            }, 1000);
            return false;
        }
        $(".invalid-feedback").html("");
        let cantidad = $("#cantidad").val();
        let producto_id = $("#producto_id").val();

        ajax.get(`api/desglose-producto/getStockByProducto?producto_id=` + producto_id, {}, function(response, headers) {
            if (headers.status == 200) {
                if (response.cantidad_almacen_primario >= cantidad) {
                    procesarCompra()
                } else {
                    let cantidad_agregada_pedido = cantidad - response.cantidad_almacen_primario;
                    toastr.error("No existe cantidad sufiente en el stock");
                    utils.displayWarningDialog("Se agregara a pedido " + cantidad_agregada_pedido + " pzas.", "warning", function(data) {
                        procesarCompra()
                    })
                }
            }
        })
    });

    function procesarCompra() {
        let valor_unitario = $('#tipo_precio option:selected').val() == 100 ? $("#precio_costo_manual").val() : $('#precio_costo').val();
        if (!valor_unitario) {
            return utils.displayWarningDialog("Indicar un tipo de precio", "warning", function(data) {});
        }
        let valor_cliente_id = $("#tipo_cliente_id").val() == 1 ? $(".form-registrar-cliente-esporadico #cliente_id").val() : $(".cliente-registrado #cliente_id").val()
        ajax.post(`api/ventas`, {
            cliente_id: valor_cliente_id,
            venta_total: 0,
            valor_unitario: valor_unitario,
            tipo_venta_id: document.getElementById("tipo_venta_id").value,
            precio_id: document.getElementById("precio_id").value,
            vendedor_id: document.getElementById("vendedor_id").value,
            producto_id: document.getElementById("producto_id").value,
            cantidad: document.getElementById("cantidad").value,
            almacen_id: document.getElementById("almacen_id").value,
            tipo_precio_id: $('#tipo_precio option:selected').val()
        }, function(response, headers) {
            if (headers.status == 200 || headers.status == 201) {
                utils.displayWarningDialog('Procesando compra..', "success", function(data) {
                    if (response.folio_id) {
                        $.isLoading({
                            text: "Procesando ...."
                        });
                        return window.location.href = base_url + `refacciones/salidas/detalleVenta/` + response.folio_id;
                    }
                })
            } else {
                return false;
            }
        });
    }

    $("#modal-permiso").on('click', function() {
        ajax.post(`api/usuarios/login`, {
            usuario: document.getElementById('usuario').value,
            password: document.getElementById('password').value
        }, function(response, header) {
            if (header.status == 200) {
                aplicarPrecio(response);
            } else {
                utils.displayWarningDialog("Credenciales incorrectas",
                    "warning",
                    function(data) {})
            }
        });

    });

    $("#btn-modal-permiso-porcentaje").on('click', function() {
        ajax.post(`api/usuarios/login`, {
            usuario: document.getElementById('usuario').value,
            password: document.getElementById('password').value
        }, function(response, header) {
            if (header.status == 200) {
                $("#modal-permiso-precio").modal('hide');
            } else {
                utils.displayWarningDialog("Credenciales incorrectas", "warning", function(data) {
                    let default_input = $(".tipo_precio")[0];
                    $(default_input).prop("checked", true);
                    $("#modal-permiso-precio").modal('hide');
                })
            }
        });

    });
});