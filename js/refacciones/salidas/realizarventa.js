crearTabla();
let descripcion_producto = '';
////////////////////////////////////////////////////////////////////////////////////////
////// CALCULO MANUAL //TO DO 
////////////////////////////////////////////////////////////////////////////////////////
function changeTipoPrecio(_this) {
    let tipo = $("#tipo_precio option:selected").val();
    let porcentaje = $("#tipo_precio option:selected").data('porcentaje');
    let total = calcularPrecioCosto(porcentaje);

    if (tipo == 100) {
        $(".precio_costo").hide();
        $(".precio_manual").show();
    } else {
        $(".precio_costo").show();
        $(".precio_manual").hide();
        if (!isNaN(total)) {
            $("#precio_costo").val(total);
        }
    }
}

function calcularPrecioCosto(porcentaje_value) {
    let calculo = 0;
    let precio_factura = parseFloat($("#valor_unitario").val());
    let porcentaje = parseFloat(porcentaje_value);
    if (porcentaje >= 0.1) {
        calculo = precio_factura / porcentaje;
    }
    return calculo.toFixed(2);
}

$("#valor_unitario").on('blur', function() {
    changeTipoPrecio();
    calculoManual();
});

$("#porcentaje_manual").on("keyup", function() {
    return calculoManual();
});

function calculoManual() {
    let porcentaje_manual = $("#porcentaje_manual").val();
    if (porcentaje_manual > 99) {
        toastr.error("Valor ingresado no valio");
        return $("#porcentaje_manual").val('0');
    } else {
        let total = calcularPrecioCosto(porcentaje_manual);
        if (!isNaN(total)) {
            $("#precio_costo_manual").val(total);
        }
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////
$(".buscar_enter").keypress(function(e) {
    if (e.which == 13) {
        getBusquedaProductosDisponibles();
    }
});
/** PRODUCTOS DISPONIBLES PARA ELEGIR  */
let estatus_cuenta_id = $("#estatus_cuenta_id").val();
let estatus_venta_id = $('#estatus_ventas_id').val();

function crearTabla() {
    $('#tbl_productos').dataTable(this.configuracionTabla());
}

function configuracionTabla() {
    return {
        "ajax": {
            url: PATH_API + "api/productos/listadoStock",
            type: 'GET',
            data: function(data) {
                data.descripcion = document.getElementById('descripcion_form').value
                data.no_identificacion = document.getElementById('no_identificacion_form').value
            },
        },
        language: {
            url: PATH_LANGUAGE
        },
        searching: false,
        processing: true,
        serverSide: true,
        bFilter: false,
        order: [
            [0, 'asc']
        ],
        columns: [{
                title: "#",
                data: 'id',
            },
            {
                title: "No. identificación",
                data: 'no_identificacion',
            },
            {
                title: "Descripcion",
                data: 'descripcion',
            },
            {
                title: "Valor unitario",
                data: 'valor_unitario',
            },
            {
                title: "Ubicación producto",
                data: 'ubicacionProducto',
            },
            {
                title: "Existencia <br />total producto",
                data: 'cantidad_actual',
            },
            {
                title: "Existencia <br />almacen principal",
                data: 'cantidad_almacen_primario',
            },
            {
                title: "Existencia <br />almacen secundario",
                data: 'cantidad_almacen_secundario',
            },
            {
                title: "Acciones",
                render: function(data, type, row) {
                    let btn_modal = '';
                    if (utils.isDefined(estatus_cuenta_id) && !estatus_cuenta_id) {
                        btn_modal = "<button onclick='modalDetalle(this)' data-product_name='" + row.descripcion + "' data-product_id='" + row.id + "' data-no_identificacion='" + row.no_identificacion + "' data-cantidad_almacen_primario= '" + row.cantidad_almacen_primario + "' data-valor_unitario='" + row.valor_unitario + "' data-toggle='modal'  class='btn btn-success'><i class='fas fa-shopping-cart'></i></button>";
                    } else if (estatus_cuenta_id == 1) {
                        btn_modal = "<button onclick='modalDetalle(this)' data-product_name='" + row.descripcion + "' data-product_id='" + row.id + "' data-no_identificacion='" + row.no_identificacion + "' data-cantidad_almacen_primario= '" + row.cantidad_almacen_primario + "' data-valor_unitario='" + row.valor_unitario + "' data-toggle='modal'  class='btn btn-success'><i class='fas fa-shopping-cart'></i></button>";
                    } else {
                        btn_modal = '--';
                    }
                    return btn_modal;
                }
            }
        ],

    }
}

function getBusquedaProductosDisponibles() {
    let almacen = $('#almacenes').val()
    let path = almacen == 1 ? PATH_API + 'api/productos/listadoStock' : PATH_API_SANJUAN + "api/productos/listadoStock"

    console.log(path, almacen)
    $('#tbl_productos').DataTable().ajax.url(path).load()

}

/* TABLA DE LO QUE HAY EN EL CARRITO */
var tabla_carrito = $('#tbl_carrito').DataTable({
    language: {
        url: PATH_LANGUAGE
    },
    processing: true,
    serverSide: true,
    bFilter: false,
    ajax: {
        url: PATH_API + "api/refacciones/venta/" + $('#venta_id').val(),
        type: 'GET',
        data: {
            id: function() {
                return $('#venta_id').val()
            }
        },
    },
    columns: [{
            'data': 'id'
        },
        {
            'data': function(data) {
                return utils.isDefined(data.no_identificacion) && data.no_identificacion ? data
                    .no_identificacion : null
            }
        },
        {
            'data': function(data) {
                descripcion_producto = utils.isDefined(data.descripcion_producto) ? data.descripcion_producto : null;
                if (estatus_venta_id == 1) {
                    $('#concepto').val(descripcion_producto);
                }
                return descripcion_producto
            }
        },
        {
            'data': function(data) {
                return utils.isDefined(data.valor_unitario) ? "$ " + data.valor_unitario : null
            }
        },
        {
            'data': function(data) {
                return utils.isDefined(data.cantidad) && data.cantidad ? data.cantidad : null
            }
        },
        {
            'data': function(data) {
                return utils.isDefined(data.venta_total) ? "$ " + data.venta_total : null
            }
        },
        {
            'data': function(data) {
                return utils.isDefined(data.unidad) && data.unidad ? data.unidad : null
            }
        },
        {
            'data': function(data) {
                return utils.isDefined(data.ubicacion_producto) && data.ubicacion_producto ? data.ubicacion_producto : null
            }
        },
        {
            'data': 'ubicacion_almacen'
        },
        {
            'data': function(data) {
                if (utils.isDefined(estatus_cuenta_id) && !estatus_cuenta_id) {
                    return "<button type='button' class='btn-borrar btn btn-primary' data-id=" + data.id + "><i class='fas fa-trash'></i></button>";
                } else if (estatus_cuenta_id == 1) {
                    return "<button type='button' class='btn-borrar btn btn-primary' data-id=" + data.id + "><i class='fas fa-trash'></i></button>";
                } else {
                    return '--';
                }
            }
        }
    ]
});

/*$("#btn-finalizar").on("click", function() {
    $.isLoading({
        text: "Realizando proceso de venta ...."
    });
    let table_carrito_length = $("#tbl_carrito").dataTable().fnSettings().aoData.length
    if (table_carrito_length == 0) {
        utils.displayWarningDialog("Seleccionar elementos para comprar");
        return false;
    }
    ajax.post(`api/venta-producto/validarprecioapartado`, {
        venta_id: $('#venta_id').val()
    }, function(response, headers) {
        if (headers.status == 201 || headers.status == 200) {
            response.map(item => {
                toastr.info(`La pieza: ${item.no_identificacion} tiene un nuevo precio de $${item.valor_actualizado}`)
            })
        }
    });
    realizarVenta();
});*/

$("#btn-editar").on("click", function() {
    $.isLoading({
        text: "Realizando proceso de venta ...."
    });
    let table_carrito_length = $("#tbl_carrito").dataTable().fnSettings().aoData.length
    if (table_carrito_length == 0) {
        utils.displayWarningDialog("Seleccionar elementos para comprar");
        return false;
    }
    updateVenta();

});

function dataVenta() {
    return {
        folio_id: $('#folio_id').val(),
        venta_total: $('#venta_total').val(),
        tipo_forma_pago_id: $('#tipo_forma_pago_id').val(),
        concepto: $('#concepto').val(),
        tipo_pago_id: 22, // POR DEFINIR
        cliente_id: $('#cliente_id').val(),
        plazo_credito_id: $('#plazo_credito_id').val(),
        enganche: $('#enganche').val(),
        tasa_interes: $('#tasa_interes').val(),
        usuario_gestor_id: utils.isDefined($('#usuario_gestor_id').val()) ? $('#usuario_gestor_id').val() : null,
    }
}

function realizarVenta() {
    $.isLoading({ text: "Realizando proceso de venta ...." });
    toastr.info('Realizando proceso de venta');

    let id_venta = $('#venta_id').val();
    let data = dataVenta();
    ajax.put(`api/ventas/finalizar/${id_venta}`, data, function(response, headers) {
        if (headers.status == 201 || headers.status == 200) {
            $.isLoading("hide");
            utils.displayWarningDialog('Proceso de venta completado correctamente!', 'success', function(result) {
                $.isLoading({
                    text: "Espere un momento"
                });
                window.location.reload();
            });
        } else {
            $.isLoading("hide");
        }
    })
}

function updateVenta() {
    $.isLoading({
        text: "Realizando proceso de venta ...."
    });
    let id_venta = $('#venta_id').val();
    let data = dataVenta();
    ajax.put(`api/ventas/editar-venta/${id_venta}`, data, function(response, headers) {
        console.log(headers);
        if (headers.status == 201 || headers.status == 200) {
            let titulo = "Venta actualizada correctamente"
            $.isLoading("hide");
            utils.displayWarningDialog(titulo, 'success', function(result) {
                //window.location.reload();
                $('#tbl_carrito').DataTable().ajax.reload()
                $('#tbl_productos').DataTable().ajax.reload()
            });
        } else {
            $.isLoading("hide");
            utils.displayWarningDialog(headers.message, 'error', function(result) {
                $.isLoading({
                    text: "Procesando información"
                });
                window.location.reload();
            });
        }
    })
}

function modalDetalle(_this) {
    $("#producto_id").val($(_this).data('product_id'));
    // let precio_factura = $(_this).data('valor_unitario');
    $("#valor_unitario").val($(_this).data('valor_unitario'));
    $("#no_identificacion").val($(_this).data('no_identificacion'));
    $("#cantidad_almacen_primario").val($(_this).data('cantidad_almacen_primario'));
    var producto_name = $(_this).data('product_name');
    $("#title_modal").text(producto_name);
    $("#modal-producto-detalle").modal('show');
    //aplicarPrecio(precio_factura, 1);
}

$("#cantidad_bk").on('blur', function() {
    $("#total_cantidad_label").unmask();
    let cantidad = $("#cantidad").val();
    let valor_unitario_total = parseFloat($("#valor_unitario_total").val()).toFixed(2);
    let cantidad_almacen_primario = $("#cantidad_almacen_primario").val();
    let total_cantidad = cantidad * valor_unitario_total;
    $("#total_cantidad").val(parseFloat(total_cantidad).toFixed(2));
    $("#total_cantidad_label").html(parseFloat(total_cantidad).toFixed(2));
    if (parseInt(cantidad_almacen_primario) >= parseInt(cantidad)) {
        $(".validation_error").html('');
        $("#btn-modal-agregar").attr('disabled', false);
    } else {
        toastr.error("No existe cantidad sufiente en el stock " + cantidad_almacen_primario);
        $("#btn-modal-agregar").attr('disabled', true);
        $("#cantidad").val(1);
        $("#cantidad").focus();
    }
    $('#total_cantidad_label').mask('#,##0.00', {
        reverse: true
    });
});

$("#cantidad").on('blur', function() {
    let cantidad = $("#cantidad").val();
    let cantidad_almacen_primario = $("#cantidad_almacen_primario").val();

    if (parseInt(cantidad_almacen_primario) >= parseInt(cantidad)) {
        $(".validation_error").html('');
        $("#btn-modal-agregar").attr('disabled', false);
    } else {
        toastr.error("No existe cantidad sufiente en el stock " + cantidad_almacen_primario);
        $("#btn-modal-agregar").attr('disabled', true);
        $("#cantidad").val(0);
        $("#cantidad").focus();
    }

});

$("#tbl_carrito").on("click", ".btn-borrar", function() {
    var id = $(this).data('id')
    borrar(id)
});

function agregarproducto() {
    if ($("#tipo_precio").val() == '') {
        toastr.info("Falta seleccionar un tipo de precio");
        return false;
    }
    let valor_unitario = $('#tipo_precio option:selected').val() == 100 ? $("#precio_costo_manual").val() : $('#precio_costo').val();
    if (valor_unitario == '') {
        toastr.info("Falta indicar un precio 1");
        return false;
    }

    let data = data_producto();
    toastr.info("Añadiendo producto al carro de compras");

    $.isLoading({
        text: "Añadiendo producto al carro de compras...."
    });
    ajax.post(`api/venta-producto`, data, function(response, headers) {
        $("#modal-producto-detalle").modal('hide');
        if (headers.status == 201 || headers.status == 200) {
            //ajax.get('api/desglose-producto/actualizaStockByProducto?producto_id=' + data.producto_id, false,
            // function(response, headers) {
            if (estatus_venta_id > 1) {
                return updateVenta();
            } else {
                $.isLoading("hide");
                utils.displayWarningDialog("Producto registrado correctamente!", 'success', function(result) {
                    $.isLoading({
                        text: "Procesando información...."
                    });
                    window.location.reload();
                });

            }
            // }, false, false)
        } else {
            $.isLoading("hide");
        }
    })
}

function data_producto() {
    let valor_unitario = $('#tipo_precio option:selected').val() == 100 ? $("#precio_costo_manual").val() : $('#precio_costo').val();
    let folio_id = $('#folio_id').val();
    let producto_id = $("#producto_id").val();
    let cliente_id = $("#cliente_id").val();
    let precio_id = $("#precio_id").val();
    let cantidad = $("#cantidad").val();
    let venta_id = $("#venta_id").val();
    let almacen_id = $("#almacenes").val();

    return {
        folio_id,
        producto_id,
        valor_unitario,
        precio_id,
        cliente_id,
        cantidad,
        venta_id,
        almacen_id,
    }
}

/*function borrar(id) {
    utils.displayWarningDialog("Desea quitar el producto?", "warning", function(data) {
        if (data.value) {
            ajax.delete(`api/venta-producto/${id}`, null, function(response, headers) {
                ajax.get('api/desglose-producto/actualizaStockByProducto?producto_id=' + id, false, function(response, headers) {
                    if (headers.status == 200) {
                        $.isLoading({
                            text: "Refrescando contenido..."
                        });
                        if (estatus_venta_id > 1) {
                            updateVenta();
                        } else {
                            utils.displayWarningDialog('Registro eliminado correctamente', 'success', function(result) {
                                $.isLoading({});
                                window.location.reload();
                            });
                        }
                    }
                }, false, false);

            })
        }
    }, true)
}*/

const apartarbutton = () => {
    const check = document.querySelector('.check_apartar').checked;
    if (check) {
        Swal.fire({
            title: 'Alerta',
            text: "¿Esta seguro de apartar las piezas ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmar!'
        }).then((result) => {
            if (result.value) {
                $("#btn-apartar").removeClass('d-none');
                $("#btn-finalizar").addClass('d-none');
                document.getElementById("concepto").readOnly = true;
                $('#tipo_forma_pago_id').attr("disabled", true);
                $('#tipo_pago_id').attr("disabled", true);
                $(".container-forma-pago").hide();
                $("#plazo_credito_id option[value=14]").attr('selected', true);
            }
        })
    } else {
        $("#btn-apartar").addClass('d-none');
        $("#btn-finalizar").removeClass('d-none');
        document.getElementById("concepto").readOnly = false;
        $('#tipo_forma_pago_id').attr("disabled", false);
        $('#tipo_pago_id').attr("disabled", false);

        $(".container-forma-pago").hide();
        $("#plazo_credito_id option[value=14]").attr('selected', true);

    }

}

const apartarVenta = () => {
    ajax.post('api/ventas/apartar', {
        venta_id: $('#venta_id').val()
    }, function(response, header) {
        if (header.status == 200 || header.status == 204) {
            let titulo = "Piezas apartadas"
            utils.displayWarningDialog(titulo, 'success', function(result) {
                return window.location.href = base_url + "refacciones/salidas/ventasMostrador";
            });
        }
    })
}


function cerrarmodales() {
    let default_input = $(".tipo_precio")[0];
    $(default_input).prop("checked", true);
}

$("#btn-finalizar").on("click", function() {
    $.isLoading({
        text: "Realizando proceso de venta ...."
    });
    let table_carrito_length = $("#tbl_carrito").dataTable().fnSettings().aoData.length
    if (table_carrito_length == 0) {
        utils.displayWarningDialog("Seleccionar elementos para comprar");
        return false;
    }
    /*ajax.post(`api/venta-producto/validarprecioapartado`, {
        venta_id: $('#venta_id').val()
    }, function(response, headers) {
        if (headers.status == 201 || headers.status == 200) {
            response.map(item => {
                toastr.info(`La pieza: ${item.no_identificacion} tiene un nuevo precio de $${item.valor_actualizado}`)
            })
        }
    });*/

    if (tipo_forma_pago_id.value == 1) {
        realizarVenta();
    } else if (tipo_forma_pago_id.value == 2) {
        ajax.post(`api/clientes/tiene-credito`, {
            id: cliente_id.value
        }, function(response, headers) {
            if (headers.status == 201 || headers.status == 200) {
                $.isLoading("hide");
                if (!response.aplica_credito) {
                    utils.displayWarningDialog("El cliente no cuenta con credito. ¿Desea continuar?", "warning", function(data) {
                        if (data.value) {
                            realizarVenta();
                        }
                    }, true)
                } else {
                    if (response.limite_credito && venta_total.value) {
                        if (parseFloat(venta_total.value) > parseFloat(response.limite_credito)) {
                            utils.displayWarningDialog("El cliente solo cuenta con un límite de crédito de: " + response.limite_credito + " ¿Desea continuar?", "warning", function(data) {
                                if (data.value) {
                                    realizarVenta();
                                }
                            }, true)
                        }
                    }
                    if (plazo_credito_id.value && response.cantidad_mes) {
                        if (parseInt($("#plazo_credito_id option:selected").data('cantidad_mes')) > parseInt(response.cantidad_mes)) {
                            utils.displayWarningDialog("El cliente solo cuenta con un plazo de credito de: " + response.plazo_credito + " ¿Desea continuar?", "warning", function(data) {
                                if (data.value) {
                                    realizarVenta();
                                }
                            }, true)
                        }
                    }
                }
            }
        });
    } else {
        $.isLoading("hide");
        toastr.error('Favor de elegir si es compra de credito ó contado');
    }
});

$(".container-forma-pago").hide();
if ($("#tipo_forma_pago_id").val() == 2) {
    $(".container-forma-pago").show();
}
$("#tipo_forma_pago_id").on("change", function() {
    let tipo_forma_pago_id = $("#tipo_forma_pago_id").val();
    if (tipo_forma_pago_id == 2) {
        $(".container-forma-pago").show();
        $("#plazo_credito_id option[value='']").attr('selected', true);
    } else {
        $(".container-forma-pago").hide();
        $("#plazo_credito_id option[value=14]").attr('selected', true);
    }
});