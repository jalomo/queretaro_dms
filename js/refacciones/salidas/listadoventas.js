let venta_realizada = 2;
let venta_mostrador = 1;

function inicializaTabla() {
    $('#tabla_ventas').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        processing: true,
        serverSide: true,
        searching: false,
        bFilter: false,
        lengthChange: false,
        bInfo: true,
        order: [
            [0, 'asc']
        ],
        "ajax": {
            url: PATH_API + "api/ventas/busqueda-ventas?tipo_venta_id=" + venta_mostrador,
            type: 'GET',
        },
        columns: [{
                title: 'id',
                data: 'id',
                visible: false,
            },
            {
                title: 'Folio',
                data: 'folio',
                render: function(data, type, row) {
                    let icon = ' ';
                    if (jQuery.inArray(row.estatus_cuenta_id, [2, 6, 7]) != -1) {
                        icon = '<i class="fa fa-thumbs-up" style="color:#23a01b"></i>';
                    } else if (jQuery.inArray(row.estatus_cuenta_id, [3, 4, 5]) != -1) {
                        icon = '<i class="fa fa-exclamation-triangle" style="color:#f25e21"></i>';
                    }
                    return data + ' ' + icon;
                }
            },
            {
                title: "Cliente",
                render: function(data, type, row) {
                    let numero_cliente = utils.isDefined(row.numero_cliente) ? row.numero_cliente : 'SN';
                    return numero_cliente + ' - ' + row.cliente;
                }
            },
            {
                title: "Total venta",
                data: 'venta_total',
                render: function(data, type, row) {
                    return utils.isDefined(data) ? '$ <span class="money_format">' + parseFloat(data).toFixed(2) + '</span>' : null;
                }
            },

            {
                title: "Estatus",
                data: 'estatus',
                render: function(data, type, row) {
                    let estatus_venta = row.estatus;
                    if (row.tipo_venta_id == 1) {
                        let estatus_cuentas = ' La venta no se ha mandado al módulo de cajas';
                        if (row.estatus_cuenta_id) {
                            if (jQuery.inArray(row.estatus_cuenta_id, [2, 6, 7]) != -1) {
                                estatus_cuentas = ' La venta se ha liquidado en módulo de cajas';
                            } else if (jQuery.inArray(row.estatus_cuenta_id, [3, 4, 5]) != -1) {
                                estatus_cuentas = ' La venta se encuentra en proceso de pago';
                            } else {
                                estatus_cuentas = ' La venta se encuentrá en el módulo de cajas';
                            }
                        }
                        return estatus_venta + '<br/><small>' + estatus_cuentas + '</small>';
                    } else {
                        return estatus_venta;

                    }

                }
            },
            {
                title: "Vendedor",
                render: function(data, type, row) {
                    return row.vendedor

                }
            },
            {
                title: "Fecha de venta",
                render: function(data, type, row) {
                    return row.fecha_venta ? obtenerFechaMostrar(row.fecha_venta) : obtenerFechaMostrar(row.fecha_venta_created)

                }
            },
            {
                title: "Fecha hora registro",
                data: 'created_at',
            },
            {
                title: '-',
                width: '120px',
                render: function(data, type, row) {
                    let btn_continuar = '';
                    let enviar_comprobante_fiscal = '';
                    let btn_comprobante_fiscal = '';
                    if (row.tipo_venta_id == 1) {
                        if (!row.estatus_cuenta_id || row.estatus_cuenta_id == 1) {
                            btn_continuar = '<button title="Ir a venta mostrador" onclick="detalleVenta(this)" data-id="' + row.id + '" data-folio_id="' + row.folio_id + '" class="btn btn-default"><i class="fas fa-edit text-warning"></i></button>';
                        } else {
                            btn_continuar = '<button title="Descargar comprobante pago" onclick="generarPolizaVentasPDF(this)" data-id="' + row.id + '" data-folio_id="' + row.folio_id + '" class="btn btn-default"><i class="fas fa-file-pdf text-danger"></i></button>';
                        }
                        if (row.estatus_cuenta_id >= 6 && row.estatus_cuenta_id <= 9) {
                            btn_comprobante_fiscal = '<button title="Comprobante fiscal digital" onclick="imprimir_comprobante_fiscal(this)" data-folio_id="' + row.folio_id + '" class="btn btn-default"><i class="fas fa-folder text-primary"></i></button>';
                            enviar_comprobante_fiscal = '<button title="Enviar comprobante fiscal" onclick="send_comprobante_fiscal(this)" data-cuenta_por_cobrar_id="' + row.cuenta_por_cobrar_id + '" class="btn btn-default"><i class="fas fa-envelope text-primary"></i></button>';
                        }
                    }
                    return btn_continuar + ' ' + btn_comprobante_fiscal;
                }
            }
        ],
        createdRow: function(row, data, dataIndex) {
            let estatus_cuenta_id = data['estatus_cuenta_id'];
            if (estatus_cuenta_id) {
                if (jQuery.inArray(estatus_cuenta_id, [2, 6, 7]) != -1) {
                    $(row).find('td:eq(0)').css('color', '#23a01b');
                } else if (jQuery.inArray(estatus_cuenta_id, [3, 4, 5]) != -1) {
                    $(row).find('td:eq(0)').css('color', '#f25e21');
                }
            }

        }
    });
}

function detalleVenta(_this) {
    setTimeout(() => {
        var venta_id = $(_this).data('id');
        window.location.href = PATH + '/refacciones/salidas/venta_mostrador/' + window.btoa(venta_id);
    }, 200);
}

function detalleVentaService(_this) {
    setTimeout(() => {
        var folio_id = $(_this).data('folio_id');
        window.location.href = PATH + '/refacciones/salidas/detalleVentaServiceExcellent/' + folio_id;
    }, 200);
}

function generarPolizaVentasPDF(_this) {
    var folio_id = $(_this).data('folio_id');
    window.open(
        PATH + '/refacciones/polizas/generarPolizaVentasPDF?folio_id=' + window.btoa(folio_id), '_blank'
    );
}

function obtenerFechaMostrar(fecha) {
    const dia = 2,
        mes = 1,
        anio = 0;
    fecha = fecha.split('T');
    fecha = fecha[0].split('-');
    return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
}

function filtrar() {
    var params = $.param({
        'tipo_venta_id': venta_mostrador,
        'folio': $("#folio").val(),
        'nombre': $("#nombre").val(),
        'apellidos': $("#apellidos").val(),
    });

    $('#tabla_ventas').DataTable().ajax.url(PATH_API + 'api/ventas/busqueda-ventas?' + params).load()
}

function limpiarFiltro() {
    $("#folio").val('');
    $("#cliente_id").val('');
    var params = $.param({
        'tipo_venta_id': venta_mostrador,
        'folio': '',
        'nombre': '',
        'apellidos': ''
    });

    $('#tabla_ventas').DataTable().ajax.url(PATH_API + 'api/productos/listadoStock?' + params).load()
}

function generar_factura(_this) {
    toastr.info("Procesando petición ..");
    $.isLoading({
        text: "Realizando petición ...."
    });
    ajax.post(`api/cuentas-por-cobrar/aplicar-factura`, { cuenta_por_cobrar_id: $(_this).data('cuenta_por_cobrar_id') }, function(response, headers) {
        if (headers.status == 400) {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        } else {
            $.isLoading("hide");
            utils.displayWarningDialog(headers.message, "success", function(data) {
                if (response.data.cfdi) {
                    window.open(response.data.cfdi, '_blank');
                }
                filtrar();
            })
        }
    })
}

function imprimir_comprobante_fiscal(_this) {
    $.isLoading({
        text: "Realizando petición ...."
    });
    ajax.get(`api/cuentas-por-cobrar/comprobante-fiscal`, { folio: $(_this).data('folio_id') }, function(response, headers) {
        $.isLoading("hide");
        if (utils.isDefined(response.data) && response.data.pdf) {
            window.open(response.data.pdf, '_blank');
        } else {
            utils.displayWarningDialog('Ocurrio un error al descargar el comprobante fiscal', "error", function(data) {});
        }
    })
}

function send_comprobante_fiscal(_this) {
    $.isLoading({
        text: "Realizando petición ...."
    });
    ajax.post(`api/cuentas-por-cobrar/send-comprobante-fiscal`, { cuenta_por_cobrar_id: $(_this).data('cuenta_por_cobrar_id') }, function(response, headers) {
        $.isLoading("hide");
        if (headers.status == 200) {
            utils.displayWarningDialog('Correo enviado a la dirección ' + response.cliente.correo_electronico, "success", function(data) {});
        }
    })
}



this.inicializaTabla();
$("#cliente_id").select2();
$(".buscar_enter").keypress(function(e) {
    if (e.which == 13) {
        filtrar();
    }
});