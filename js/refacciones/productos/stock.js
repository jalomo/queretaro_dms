var tabla_stock = $('#tabla_stock').DataTable({
    language: {
        url: PATH_LANGUAGE
    },
    processing: true,
    serverSide: true,
    bFilter: false,
    "ajax": {
        url: PATH_API + 'api/productos/listadoStock',
        type: 'GET',
        data: function(data){
            data.descripcion = document.getElementById('descripcion').value
            data.no_identificacion = document.getElementById('no_identificacion').value
        },
    },
    columns: [{
            title: "#",
            data: 'id',
        },
        {
            title: "Prefijo",
            data: 'prefijo',
        },
        {
            title: "Básico",
            data: 'basico',
        },
        {
            title: "Sufijo",
            data: 'sufijo',
        },
        {
            title: "No. de pieza",
            data: 'no_identificacion',
        },
        {
            title: "Descripcion",
            data: 'descripcion',
        },
        {
            title: "Precio unitario",
            data: 'valor_unitario',
        },
        {
            title: "Unidad",
            data: 'unidad',
        },
        {
            title: "Cantidad actual",
            data: 'cantidad_actual',
        },
        {
            title: "Almacen primario",
            data: 'cantidad_almacen_primario'
        },
        {
            title: "Almacen secundario",
            data: 'cantidad_almacen_secundario'
        }

    ]
});



const buscarproducto = () => {

    let almacen = $('#almacenes').val()
    let path = almacen == 1? PATH_API + 'api/productos/listadoStock' : PATH_API_SANJUAN + "api/productos/listadoStock"

    $('#tabla_stock').DataTable().ajax.url(path).load()
}