$(document).ready(function () {
    $("#subirfactura").on('click', function (e) {
        let factura = $('#archivo_factura')[0].files[0];
        let paqueteDeDatos = new FormData();
        paqueteDeDatos.append('archivo_factura', factura);
        if (!utils.isDefined(factura) && !factura) {
            return toastr.error("Adjuntar una factura");
        }

        ajax.postFile(`api/upload-factura`, paqueteDeDatos, function (response, header) {
            console.log(response, header)
            if (header.status == 200 || header.status == 204) {
                let titulo = "Factura subida con exito!"
                utils.displayWarningDialog(titulo, 'success', function (result) {
                    return window.location.href = base_url + "refacciones/factura/listado/";
                });
            }
            if (header.status == 500) {
                let titulo = "Error subiendo factura!"
                utils.displayWarningDialog(titulo, 'warning', function (result) { });
            }
        })

    })

});