$('.tb1_ck1').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck1').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='extintor_ck']:radio").is(':checked')) {
        var valor = $("input[name='extintor_ck']:checked").val();
        $("input[name='extintor']").val(valor);
    }
});

$('.tb1_ck2').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck2').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='gato_ck']:radio").is(':checked')) {
        var valor = $("input[name='gato_ck']:checked").val();
        $("input[name='gato']").val(valor);
    }
});

$('.tb1_ck3').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck3').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='llanta_ck']:radio").is(':checked')) {
        var valor = $("input[name='llanta_ck']:checked").val();
        $("input[name='llanta']").val(valor);
    }
});

$('.tb1_ck4').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck4').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='reflejantes_ck']:radio").is(':checked')) {
        var valor = $("input[name='reflejantes_ck']:checked").val();
        $("input[name='reflejantes']").val(valor);
    }
});

$('.tb1_ck5').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck5').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='llave_l_ck']:radio").is(':checked')) {
        var valor = $("input[name='llave_l_ck']:checked").val();
        $("input[name='llave_l']").val(valor);
    }
});

$('.tb1_ck6').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck6').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='cables_corriente_ck']:radio").is(':checked')) {
        var valor = $("input[name='cables_corriente_ck']:checked").val();
        $("input[name='cables_corriente']").val(valor);
    }
});

$('.tb1_ck7').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck7').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='controles_ck']:radio").is(':checked')) {
        var valor = $("input[name='controles_ck']:checked").val();
        $("input[name='controles']").val(valor);
    }
});

$('.tb1_ck8').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck8').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='seguro_ruedas_ck']:radio").is(':checked')) {
        var valor = $("input[name='seguro_ruedas_ck']:checked").val();
        $("input[name='seguro_ruedas']").val(valor);
    }
});

$('.tb1_ck9').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck9').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='tapones_ck']:radio").is(':checked')) {
        var valor = $("input[name='tapones_ck']:checked").val();
        $("input[name='tapones']").val(valor);
    }
});

$('.tb1_ck10').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck10').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='rines_ck']:radio").is(':checked')) {
        var valor = $("input[name='rines_ck']:checked").val();
        $("input[name='rines']").val(valor);
    }
});

$('.tb1_ck11').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck11').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='antena_ck']:radio").is(':checked')) {
        var valor = $("input[name='antena_ck']:checked").val();
        $("input[name='antena']").val(valor);
    }
});

$('.tb1_ck12').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck12').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='tapon_gasolina_ck']:radio").is(':checked')) {
        var valor = $("input[name='tapon_gasolina_ck']:checked").val();
        $("input[name='tapon_gasolina']").val(valor);
    }
});

$('.tb1_ck13').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck13').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='encendendor_ck']:radio").is(':checked')) {
        var valor = $("input[name='encendendor_ck']:checked").val();
        $("input[name='encendendor']").val(valor);
    }
});

$('.tb1_ck14').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck14').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='cenicero_ck']:radio").is(':checked')) {
        var valor = $("input[name='cenicero_ck']:checked").val();
        $("input[name='cenicero']").val(valor);
    }
});

$('.tb1_ck15').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck15').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='tapetes_ck']:radio").is(':checked')) {
        var valor = $("input[name='tapetes_ck']:checked").val();
        $("input[name='tapetes']").val(valor);
    }
});

$('.tb1_ck16').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck16').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='embudo_ck']:radio").is(':checked')) {
        var valor = $("input[name='embudo_ck']:checked").val();
        $("input[name='embudo']").val(valor);
    }
});

$('.tb1_ck17').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck17').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='llaves_2_ck']:radio").is(':checked')) {
        var valor = $("input[name='llaves_2_ck']:checked").val();
        $("input[name='llaves_2']").val(valor);
    }
});

$('.tb1_ck18').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck18').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='luces_ck']:radio").is(':checked')) {
        var valor = $("input[name='luces_ck']:checked").val();
        $("input[name='luces']").val(valor);
    }
});

/*********************************************************************************/
$('.tb2_ck1').change(function(){
    if($(this).is(':checked')){
        $('.tb2_ck1').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='tarjeta_acceso_ck']:radio").is(':checked')) {
        var valor = $("input[name='tarjeta_acceso_ck']:checked").val();
        $("input[name='tarjeta_acceso']").val(valor);
    }
});

$('.tb2_ck2').change(function(){
    if($(this).is(':checked')){
        $('.tb2_ck2').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='poliza_garantia_ck']:radio").is(':checked')) {
        var valor = $("input[name='poliza_garantia_ck']:checked").val();
        $("input[name='poliza_garantia']").val(valor);
    }
});

$('.tb2_ck3').change(function(){
    if($(this).is(':checked')){
        $('.tb2_ck3').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='tarjeta_asistencia_ck']:radio").is(':checked')) {
        var valor = $("input[name='tarjeta_asistencia_ck']:checked").val();
        $("input[name='tarjeta_asistencia']").val(valor);
    }
});

$('.tb2_ck4').change(function(){
    if($(this).is(':checked')){
        $('.tb2_ck4').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='manual_ck']:radio").is(':checked')) {
        var valor = $("input[name='manual_ck']:checked").val();
        $("input[name='manual']").val(valor);
    }
});

/*********************************************************************************/
$('.tb3_ck1').change(function(){
    if($(this).is(':checked')){
        $('.tb3_ck1').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='motor_ck']:radio").is(':checked')) {
        var valor = $("input[name='motor_ck']:checked").val();
        $("input[name='motor']").val(valor);
    }
});

$('.tb3_ck2').change(function(){
    if($(this).is(':checked')){
        $('.tb3_ck2').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='carroceria_ck']:radio").is(':checked')) {
        var valor = $("input[name='carroceria_ck']:checked").val();
        $("input[name='carroceria']").val(valor);
    }
});

$('.tb3_ck3').change(function(){
    if($(this).is(':checked')){
        $('.tb3_ck3').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='golpes_ck']:radio").is(':checked')) {
        var valor = $("input[name='golpes_ck']:checked").val();
        $("input[name='golpes']").val(valor);
    }
});

$('.tb3_ck4').change(function(){
    if($(this).is(':checked')){
        $('.tb3_ck4').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='rayones_ck']:radio").is(':checked')) {
        var valor = $("input[name='rayones_ck']:checked").val();
        $("input[name='rayones']").val(valor);
    }
});

$('.tb3_ck5').change(function(){
    if($(this).is(':checked')){
        $('.tb3_ck5').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='interiores_ck']:radio").is(':checked')) {
        var valor = $("input[name='interiores_ck']:checked").val();
        $("input[name='interiores']").val(valor);
    }
});

$('.tb3_ck6').change(function(){
    if($(this).is(':checked')){
        $('.tb3_ck6').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='interiores_ck']:radio").is(':checked')) {
        var valor = $("input[name='interiores_ck']:checked").val();
        $("input[name='interiores']").val(valor);
    }
});

/*********************************************************************************/
$('.tb4_ck1').change(function(){
    if($(this).is(':checked')){
        $('.tb4_ck1').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='accesorios_ck']:radio").is(':checked')) {
        var valor = $("input[name='accesorios_ck']:checked").val();
        $("input[name='accesorios']").val(valor);
    }
});

/*********************************************************************************/
$('.tb5_ck1').change(function(){
    if($(this).is(':checked')){
        $('.tb5_ck1').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='poliza_ck']:radio").is(':checked')) {
        var valor = $("input[name='poliza_ck']:checked").val();
        $("input[name='poliza']").val(valor);
    }
});

$('.tb5_ck2').change(function(){
    if($(this).is(':checked')){
        $('.tb5_ck2').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='carta_factura_ck']:radio").is(':checked')) {
        var valor = $("input[name='carta_factura_ck']:checked").val();
        $("input[name='carta_factura']").val(valor);
    }
});

$('.tb5_ck3').change(function(){
    if($(this).is(':checked')){
        $('.tb5_ck3').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='seguro_ck']:radio").is(':checked')) {
        var valor = $("input[name='seguro_ck']:checked").val();
        $("input[name='seguro']").val(valor);
    }
});

$('.tb5_ck4').change(function(){
    if($(this).is(':checked')){
        $('.tb5_ck4').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='permisos_ck']:radio").is(':checked')) {
        var valor = $("input[name='permisos_ck']:checked").val();
        $("input[name='permiso']").val(valor);
    }
});

$('.tb5_ck5').change(function(){
    if($(this).is(':checked')){
        $('.tb5_ck5').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='placas_ck']:radio").is(':checked')) {
        var valor = $("input[name='placas_ck']:checked").val();
        $("input[name='placas']").val(valor);
    }
});

$('.tb5_ck6').change(function(){
    if($(this).is(':checked')){
        $('.tb5_ck6').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='verificacion_ck']:radio").is(':checked')) {
        var valor = $("input[name='verificacion_ck']:checked").val();
        $("input[name='verificacion']").val(valor);
    }
});

$('.tb5_ck7').change(function(){
    if($(this).is(':checked')){
        $('.tb5_ck7').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='papeleria_ck']:radio").is(':checked')) {
        var valor = $("input[name='papeleria_ck']:checked").val();
        $("input[name='papeleria']").val(valor);
    }
});

/*********************************************************************************/
$('.tb6_ck1').change(function(){
    if($(this).is(':checked')){
        $('.tb6_ck1').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='autorizacion_ck']:radio").is(':checked')) {
        var valor = $("input[name='autorizacion_ck']:checked").val();
        $("input[name='autorizacion']").val(valor);
    }
});

/*********************************************************************************/