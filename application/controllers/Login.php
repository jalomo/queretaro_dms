<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time', 300);
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function index()
    {
        $this->load->library(['curl', 'form_validation']);
        $this->load->helper(['form', 'url', 'general','debug']);
        $this->form_validation->set_error_delimiters('<div class="error_container">', '</div>');
        $this->form_validation->set_rules('usuario', 'Usuario', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $data['msg'] = "";
        if ($this->form_validation->run() == FALSE) {
            $this->blade->render('login/index', $data);
        } else {

            
            $login = $this->curl->curlPost('api/usuarios/login', [
                'usuario' => $this->input->post('usuario'),
                'password' => $this->input->post('password'),
                'direccion_ip' =>  $this->getUserIpAddr(),
            ]);
            
            if(empty($login)){
                $data['msg'] = "Hubo un problema con el servidor";
                return $this->blade->render('login/index', $data);
            }

            if (isset($login) && isset($login['status_code']) == 400) {
                $data['msg'] = "Las credenciales son incorrectas";
                return $this->blade->render('login/index', $data);
            }



            $usuario = procesarResponseApiJsonToArray($login);
            $array_session = [
                'id' => $usuario->id,
                'usuario' => $usuario->usuario,
                'nombre' => $usuario->nombre,
                'apellido_paterno' => $usuario->apellido_paterno,
                'apellido_materno' => $usuario->apellido_materno,
                'email' => $usuario->email,
                'telefono' => $usuario->telefono,
                'rol_id' => $usuario->rol_id,
                'api_token' => $usuario->api_token,
                'ip_adress' => $usuario->ip_adress,
                'ultimo_acceso' => $usuario->ultimo_acceso,
                'access_token' => $usuario->access_token,
                'type_token' => $usuario->type_token,
                'expires_at' => $usuario->expires_at
            ];

            $this->session->set_userdata($array_session);

            $menu = $this->curl->curlGet('api/menu?usuario_id=' . $usuario->id);
            $data_menu = procesarResponseApiJsonToArray($menu);
            $array_session['menu'] = !empty($data_menu) ? $data_menu : [];
            
            $this->session->set_userdata($array_session);

            redirect('inicio'); 
        }
    }

    public function test()
    {
        $data = $this->getUserIpAddr();
        utils::pre($data);
    }

    public function cerrar_sesion()
    {
        $session = $this->session->all_userdata();
        
        if(isset($session['id'])){
            $login = $this->curl->curlPost('api/usuarios/logout', [
                'usuario_id' => $session['id'],
                'direccion_ip' =>  $this->getUserIpAddr(),
                'descripcion' => "CERRANDO SESION"
            ]);
            
            if (isset($login) && isset($login['status_code']) == 400) {
                $data['msg'] = "Las credenciales son incorrectas";
                return $this->blade->render('login/index', $data);
            }else{
                // utils::pre("cerrando");
                $this->session->unset_userdata('usuario');
                $this->session->sess_destroy();
                redirect('login');
            }
        }else{
            $this->session->unset_userdata('usuario');
            $this->session->sess_destroy();
            redirect('login');
        }

    }

    public function getUserIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function registro()
    {
        $this->load->library('curl');
        $this->load->helper('general');
        $data['modulo'] = "";
        $data['subtitulo']  = "";
        $data['titulo'] = "";
        $this->blade->render('registro/registro', $data);
    }
}
