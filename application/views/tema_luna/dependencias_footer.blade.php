<script>
    paceOptions = {
        ajax: {ignoreURLs: ['https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/lista_notificaciones']}
    }
</script>
<script src="{{ base_url('luna/librerias/pacejs/pace.min.js'); }}"></script>
<?php if($this->uri->segment(1) != 'nomina'){ ?>
<script src="<?php echo base_url();?>luna/librerias/jquery/dist/jquery.min.js"></script>
<?php } ?>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js"></script>
<script src="<?php echo base_url();?>luna/librerias/bootstrap/js/bootstrap.min.js"></script>
<!-- App scripts -->
<script src="<?php echo base_url();?>luna/scripts/luna.js"></script>
<script src="<?php echo base_url('assets/template/dist/js/scripts.js') ?> "></script>

{{-- Custom scripts --}}
<script src="{{ base_url('js/custom/jquery.dataTable.1.10.20.min.js') }}" crossorigin="anonymous"></script>
<script src="{{ base_url('js/custom/dataTables.bootstrap4.min.js') }}" crossorigin="anonymous"></script>
<script src="<?php echo base_url('assets/template/dist/assets/demo/datatables-demo.js') ?>"></script>
<script>
    var site_url = '<?php echo site_url(); ?>';
    var base_url = "<?php echo base_url(); ?>";
</script>
<script src="<?php echo base_url('assets/libraries/sweetalert2/dist/sweetalert2.min.js') ?>"></script>
<script src="<?php echo base_url('js/custom/isloading.js') ?>"></script>
<script src="<?php echo base_url('js/custom/utils.js') ?>"></script>


<script src="<?php echo base_url()?>luna/librerias/toastr/toastr.min.js"></script>
<script src="<?php echo base_url()?>luna/librerias/sparkline/index.js"></script>
<script src="<?php echo base_url()?>luna/librerias/flot/jquery.flot.min.js"></script>
<script src="<?php echo base_url()?>luna/librerias/flot/jquery.flot.resize.min.js"></script>
<script src="<?php echo base_url()?>luna/librerias/flot/jquery.flot.spline.js"></script>


<script src="{{ base_url('assets/libraries/select2/select2.min.js') }}" ></script>
<script src="{{ base_url('assets/libraries/select2/dist/js/i18n/es.js') }}" ></script>
<script src="{{ base_url('assets/libraries/mask/src/jquery.mask.js') }}" crossorigin="anonymous"></script>




