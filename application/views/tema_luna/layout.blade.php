<!DOCTYPE html>
<html>

<head>
    @include('tema_luna/head')
    <script type="text/javascript">
        var PATH = "<?php echo site_url(); ?>";
        var PATH_BASE = "<?php echo base_url(); ?>";
        var PATH_API = "<?php echo API_URL_DEV ?>";
        var PATH_API_SANJUAN = "<?php echo API_URL_SANJUAN; ?>";
        var API_CONTABILIDAD = "<?php echo API_CONTABILIDAD ?>";
        var PATH_LANGUAGE = "<?php echo base_url('js/spanish.json'); ?>";
        var CONTROLLER = "<?php echo $this->router->class; ?>";
        var FUNCTION = "<?php echo $this->router->method; ?>";
        </script>
        @if (!empty($this->session->userdata('id')) )
            <script>
                var telefono = "<?php echo $this->session->userdata('telefono'); ?>";
                var user_name = "<?php echo $this->session->userdata('nombre'); ?>";
                var user_id = "<?php echo $this->session->userdata('id'); ?>";
                var access_token = "<?php echo $this->session->userdata('access_token') ?>";
            </script>
        @endif
</head>

<body>
    <!-- Wrapper-->
    <div class="wrapper">
        <!-- Header-->
        <nav class="navbar navbar-expand-md navbar-default fixed-top">
            <div class="navbar-header">
                <div id="mobile-menu">
                    <div class="left-nav-toggle">
                        <a href="#">
                            <i class="stroke-hamburgermenu"></i>
                        </a>
                    </div>
                </div>
                <a class="navbar-brand" href="<?php echo accessToDashboard() == 1 ? base_url() . 'inicio' : '#'; ?>" style="background: #6ba0cb; padding: 0; display: flex; text-align: center; align-items: center; justify-content: center;">
                    <img src="<?php echo base_url() ?>luna/logodms.png" width="90%">
                    <!--span>v.1.4</span-->
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <div class="left-nav-toggle">
                    <a href="">
                        <i class="stroke-hamburgermenu"></i>
                    </a>
                </div>
                <form class="navbar-form  mr-auto">
                </form>
                <ul class="nav navbar-nav">
                 
                    <li class="nav-item profil-link">
                        <img src="<?php echo base_url() ?>luna/images/profile.jpg" class="rounded-circle" alt="">
                        
                        <a title="Mis notificaciones" href="<?php echo base_url('sistemas/notificaciones/mis_notificaciones') ?>">
                            <span style="font-size:15px" id="total_notify" class="label label-warning pull-left"> - </span>
                        </a>
                        <a href="<?php echo base_url('usuarios/perfil') ?>"><span style="color: #fff;">
                            {{ ucfirst($this->session->userdata('nombre')) }}
                        </span></a>
                        
                    </li>
                    <li class="nav-item">
                        <a href="{{ base_url('sistemas/notificaciones/mis_contactos') }}" title="Mis contactos" class="nav-link">
                            <i class="fas fa-users fa-2x" style="color:white;"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ base_url('soporte/listado') }}" title="Agregar ticket" class="nav-link">
                            <i class="fas fa-ticket-alt fa-2x" style="color:white;"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ base_url('login/cerrar_sesion') }}" title="Cerrar Sesión" class="nav-link">
                            <i class="fas fa-sign-out-alt fa-2x" style="color:white;"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- End header-->

        <!-- Navigation-->
        <aside class="navigation">
            @include('tema_luna/menu')
        </aside>
        <!-- End navigation-->
        <!-- Main content-->
        <section class="content">
            <div class="container-fluid">
                @yield('contenido')
            </div>
        </section>
        <!-- End main content-->
    </div>
    <!-- End wrapper-->
    <!-- Vendor scripts -->
    @include('tema_luna/dependencias_footer')
    @yield('modal')
    <script>
        $(document).ready(function(){
            $.ajaxSetup({
                headers:{
                    Authorization: `Bearer ${access_token}`
                }
            });
        })
    </script>
    @yield('scripts')
</body>

</html>