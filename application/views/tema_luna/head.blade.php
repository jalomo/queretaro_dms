<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
{{-- <link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/comic-neue" type="text/css"/> --}}

    <!-- Page title -->
<title>DMS SOHEX</title>
<link rel="shortcut icon" type="image/png" href="<?php echo base_url()?>img/icondms.png"/>

    <!-- Vendor styles -->
<!-- <link rel="stylesheet" href="<?php echo base_url();?>luna/librerias/fontawesome/css/font-awesome.css"/> -->
<link rel="stylesheet" href="<?php echo base_url('assets/libraries/fontawesome-free/css/all.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/libraries/datatables/dataTables.bootstrap4.min.css') ?>"/>
<link rel="stylesheet" href="<?php echo base_url('assets/libraries/sweetalert2/dist/sweetalert2.min.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/libraries/select2/dist/css/select2.css') ?>">
<!-- App styles -->
<link rel="stylesheet" href="<?php echo base_url();?>luna/librerias/animate.css/animate.css"/>
<link rel="stylesheet" href="<?php echo base_url();?>luna/librerias/bootstrap/css/bootstrap.css"/>
<link rel="stylesheet" href="<?php echo base_url();?>luna/styles/pe-icons/pe-icon-7-stroke.css"/>
<link rel="stylesheet" href="<?php echo base_url();?>luna/styles/pe-icons/helper.css"/>
<link rel="stylesheet" href="<?php echo base_url();?>luna/styles/stroke-icons/style.css"/>
<link rel="stylesheet" href="<?php echo base_url('assets/libraries/toastr/toastr.css') ?>">

<link rel="stylesheet" href="<?php echo base_url();?>luna/styles/style.css">
<script src="<?php echo base_url('assets/libraries/jquery/jquery.min.js') ?>"></script>
<script src="{{ base_url('assets/libraries/toastr/toastr.js') }}" crossorigin="anonymous"></script>
<script>
    var API_URL = "<?php echo API_URL_DEV; ?>";
</script>