<!DOCTYPE html>
<html>
<head>
    @include('tema_luna/head')
    <script type="text/javascript">
        var PATH = "<?php echo site_url(); ?>";
        var PATH_BASE = "<?php echo base_url(); ?>";
        var PATH_API = "<?php echo API_URL_DEV ?>";
        var PATH_API_SANJUAN = "<?php echo API_URL_SANJUAN; ?>";
        var PATH_LANGUAGE = "<?php echo base_url('js/spanish.json'); ?>";
        var CONTROLLER = "<?php echo $this->router->class; ?>";
        var FUNCTION = "<?php echo $this->router->method; ?>";
        </script>
        @if (!empty($this->session->userdata('id')) )
            <script>
                var telefono = "<?php echo $this->session->userdata('telefono'); ?>";
                var user_name = "<?php echo $this->session->userdata('nombre'); ?>";
                var user_id = "<?php echo $this->session->userdata('id'); ?>";

            </script>
        @endif
</head>

<body>
    <!-- Wrapper-->
    <div class="wrapper">

        <!-- End navigation-->
        <!-- Main content-->
        <section class="content" style="margin: 0 auto">
            <div class="container-fluid">
                @yield('contenido')
            </div>
        </section>
        <!-- End main content-->
    </div>
    <!-- End wrapper-->
    <!-- Vendor scripts -->
    @include('tema_luna/dependencias_footer')
    @yield('modal')
    @yield('scripts')
</body>

</html>