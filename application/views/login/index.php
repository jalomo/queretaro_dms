<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

    <!-- Page title -->
    <title>DMS SOHEX</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="<?php echo base_url()?>luna/librerias/fontawesome/css/font-awesome.css"/>
    <link rel="stylesheet" href="<?php echo base_url()?>luna/librerias/animate.css/animate.css"/>
    <link rel="stylesheet" href="<?php echo base_url()?>luna/librerias/bootstrap/css/bootstrap.css"/>

    <!-- App styles -->
    <link rel="stylesheet" href="<?php echo base_url()?>luna/styles/pe-icons/pe-icon-7-stroke.css"/>
    <link rel="stylesheet" href="<?php echo base_url()?>luna/styles/pe-icons/helper.css"/>
    <link rel="stylesheet" href="<?php echo base_url()?>luna/styles/stroke-icons/style.css"/>
    <link rel="stylesheet" href="<?php echo base_url()?>luna/styles/style.css">
</head>
<body class="blank">

<!-- Wrapper-->
<div class="wrapper">


    <!-- Main content-->
    <section class="content">
       

        <div class="container-center animated slideInDown">

            
                <img src="<?php echo base_url()?>luna/images/sohexlogo.png" width="100%">
            
            <div class="view-header">
                <div class="header-icon">
                    <i class="pe page-header-icon pe-7s-unlock"></i>
                </div>
                <div class="header-title">
                    <h3>Login DMS</h3>
                    <small>
                       Introduce tus credenciales
                       <?php if($mensaje !=null):?>
                       <div class="alert alert-danger" role="alert">
                          <?php echo $mensaje;?>
                        </div>
                    <?php endif;?>
                    </small>
                </div>
            </div>

            <div class="panel panel-filled">
                <div class="panel-body">
                    <form action="<?php echo base_url()?>login" id="loginForm" method="post" novalidate>
                        <div class="form-group">
                            <label class="col-form-label" for="username">Usuario</label>
                            <input type="text" placeholder="example@gmail.com" title="Please enter you username" required="" value="" name="username" id="username" class="form-control">
                            <span class="form-text small">Introduce tu usuario</span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label" for="password">Contraseña</label>
                            <input type="password" title="Please enter your password" placeholder="******" required="" value="" name="password" id="password" class="form-control">
                            <span class="form-text small">Introduce tu contraseña</span>
                        </div>
                        <div>

                            <button class="btn btn-accent" type="sumbit">Entrar</button>
                            <a class="btn btn-default" href="<?php echo base_url()?>registro">Registro</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="content" style="text-align: center">
                <img src="<?php echo base_url()?>luna/images/dmslogo.png" width="90%">
            </div>

        </div>
    </section>
    <!-- End main content-->

</div>
<!-- End wrapper-->

<!-- Vendor scripts -->
<!-- <script src="<?php //echo base_url()?>luna/librerias/pacejs/pace.min.js"></script> -->
<script src="<?php echo base_url()?>luna/librerias/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url()?>luna/librerias/bootstrap/js/bootstrap.min.js"></script>

<!-- App scripts -->
<script src="<?php echo base_url()?>luna/scripts/luna.js"></script>

</body>

</html>