<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

    <!-- Page title -->
    <title>DMS SOHEX</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="<?php echo base_url() ?>luna/librerias/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>luna/librerias/animate.css/animate.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>luna/librerias/bootstrap/css/bootstrap.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
    <!-- App styles -->
    <link rel="stylesheet" href="<?php echo base_url() ?>luna/styles/pe-icons/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>luna/styles/pe-icons/helper.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>luna/styles/stroke-icons/style.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>luna/styles/style.css">
    <script>
        var API_URL = "{{ API_URL_DEV }}";
    </script>
</head>

<body class="blank">

    <!-- Wrapper-->
    <div class="wrapper">


        <!-- Main content-->
        <section class="content">


            <div class="container-center animated slideInDown">


                <img src="<?php echo base_url() ?>luna/images/sohexlogo.png" width="100%">

                <div class="view-header">
                    <div class="header-icon">
                        <i class="pe page-header-icon pe-7s-unlock"></i>
                    </div>
                    <div class="header-title">
                        <h3><?php echo NOMBRE_SUCURSAL; ?></h3>
                        <small>
                            Introduce tus credenciales
                        </small>
                    </div>
                </div>

                <div class="panel panel-filled">
                    <div class="panel-body">
                        <form id="loginForm" action="{{ base_url('login') }}" method="post">
                            <div class="form-group">
                                <label class="col-form-label" for="usuario">Usuario:</label>
                                <input type="text" placeholder="example@gmail.com" title="Introduzca usuario" value="<?php echo set_value('usuario'); ?>" name="usuario" id="usuario" class="form-control">
                                <?php echo form_error('usuario'); ?>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="password">Contraseña</label>
                                <input type="password" title="Please enter your password" placeholder="******" value="<?php echo set_value('password'); ?>" name="password" id="password" class="form-control">
                                <?php echo form_error('password'); ?>
                            </div>
                            <?php if ($msg != null) : ?>
                                <div class="alert alert-danger" role="alert">
                                    <?php echo $msg; ?>
                                </div>
                            <?php endif; ?>
                            <div>
                                <button class="btn btn-primary btn-block btn-large" id="btn-login" type="submit">Entrar al sistema</button>
                                <!-- <a class="btn btn-default" href="{{  base_url('login/registro') }} ">Registro</a> -->
                            </div>
                        </form>
                    </div>
                </div>
                <div class="content">
                    <div class="row justify-content-center">
                        <div class="col-2 text-center">
                            <a href="https://www.linkedin.com/company/sohexdms" class="icon linkedin" target="_blank">
                                 <i class="fab fa-linkedin"></i>
                            </a>
                        </div>
                        <div class="col-2 text-center">
                            <a class="icon instagram" href="https://instagram.com/sohex_dms?igshid=1akazi92c80of" target="_blank">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </div>
                        <div class="col-2 text-center">
                            <a  class="icon twitter" href="https://twitter.com/SohexDms?s=09" target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </div>
                        <div class="col-2 text-center">
                            <a class="icon facebook" href="https://www.facebook.com/SohexDms-104197548089939/" target="_blank">
                                <i class="fab fa-facebook-square"></i>
                            </a>
                        </div>
                        <div class="col-2 text-center">
                            <a class="icon whatsapp" href="https://wa.me/message/FM4ZZEXPZ2T6H1" target="_blank">
                                <i class="fab fa-whatsapp"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="content" style="text-align: center">
                    <img src="<?php echo base_url() ?>luna/images/dmslogo.png" width="90%">
                </div>

            </div>
        </section>
        <!-- End main content-->
    </div>
    <!-- End wrapper-->
    <!-- Vendor scripts -->
    <script src="<?php echo base_url() ?>luna/librerias/pacejs/pace.min.js"></script>
    <script src="<?php echo base_url() ?>luna/librerias/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>luna/librerias/bootstrap/js/bootstrap.min.js"></script>
    <!-- App scripts -->
    <script src="<?php echo base_url() ?>luna/scripts/luna.js"></script>
    <script src="<?php echo base_url('assets/libraries/sweetalert2/dist/sweetalert2.min.js') ?>"></script>
    <script src="<?php echo base_url('js/custom/isloading.js') ?>"></script>
    <script src="<?php echo base_url('js/custom/utils.js') ?>"></script>

</body>

</html>