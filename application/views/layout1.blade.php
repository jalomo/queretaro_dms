<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <base href="{{base_url()}}">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Ford Plasencia</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template-->
  <link href="vendor/components/font-awesome/css/fontawesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/datatables/media/css/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">

   <!-- CSS CUSTOM-->
  <link href="css/custom/bootstrap-datetimepicker.css" rel="stylesheet">
  <!--<link href="css/custom/datepicker3.css" rel="stylesheet">-->
  <link href="css/custom/isloading.css" rel="stylesheet">
  <link href="css/custom/style.css" rel="stylesheet">
  <link href="css/custom/bootstrap-switch.css" rel="stylesheet">
  <link href="css/custom/clockpicker.css" rel="stylesheet">
  <link href="css/custom/pagination.css" rel="stylesheet">
  <link href="css/custom/bootstrap-multiselect.css" rel="stylesheet">
  <link rel="shortcut icon" type="image/png" href="<?php echo base_url()?>img/so.png"/>
  <style>
  .minusculas{
    text-transform: lowercase;
  }
</style>
</head>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  @if(!isset($sinmenu))
  @include('nav')
  @endif
  <div class="content-wrapper" style="{{(isset($sinmenu))?'margin-left: 0px !important':''}}" >
    <div class="container-fluid">
      @yield('contenido')
    </div>
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/components/jquery/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Core plugin JavaScript-->
    <!-- <script src="vendor/jquery-easing/jquery.easing.min.js"></script> -->
    <!-- Page level plugin JavaScript-->
    <script src="vendor/datatables/datatables/media/js/jquery.dataTables.js"></script>
    <script src="vendor/datatables/datatables/media/js/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-datatables.min.js"></script>

    <!--scripts mios -->
	 <script src="js/custom/bootbox.min.js"></script>
	 <script src="js/custom/general.js"></script>
   <script src="js/custom/isloading.js"></script>
   <script src="js/custom/moment.js"></script>
   <script src="js/custom/bootstrap-datetimepicker.js"></script>

   <script src="js/custom/bootstrap-switch.js"></script>
  <script src="js/custom/clockpicker.js"></script>
  <script src="js/custom/bootstrap-multiselect.js"></script>
  <script>
    var site_url = '<?php echo site_url(); ?>';
    var base_url = "<?php echo base_url(); ?>";
    var api_url = "<?php API_URL_DEV ?>";
  </script>
  <script src="<?php echo base_url('js/custom/utilidades.js') ?>"></script>
  @yield('scripts')
  </div>
</body>

</html>
