<?php

class MY_Controller extends MX_Controller {

    protected $middlewares = [];

    public function __construct()
    {
        parent::__construct();
        $this->_run_middlewares();
    }

    public function output($html){
		$this->blade->render('build', array(
            'scripts' => $this->scripts,
            'titulo_modulo' => $this->title,
            'modulo' => 'Nómina',
            'breadcrumb' => $this->breadcrumb,
            'content'=>$html
        ));
    }

    public function response($data = array(),$status = 200){
        $this->output
            ->set_status_header($status)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($data))
            ->_display();
        exit;
    }

    protected function middleware(){
        return [];
    }

    protected function _run_middlewares(){
        $this->load->helper('inflector');
        $middlewares = $this->middleware();

        foreach($middlewares as $middleware)
        {
            $middlewareArray = explode('|', str_replace(' ', '', $middleware));
            $middlewareName = $middlewareArray[0];
            $runMiddleware = true;

            if(isset($middlewareArray[1]))
            {
                $options = explode(':', $middlewareArray[1]);
                $type = $options[0];
                $methods = explode(',', $options[1]);

                if ($type == 'except' && in_array($this->router->method, $methods)) 
                {
                    $runMiddleware = false;
                } 
                elseif ($type == 'only' && ! in_array($this->router->method, $methods)) 
                {
                    $runMiddleware = false;
                }
            }
            
            if ($runMiddleware == true) 
            {
                $filename = ucfirst(camelize($middlewareName)) . 'Middleware';

                if(! file_exists(APPPATH . 'middlewares/' . $filename . '.php'))
                {
                    ENVIRONMENT == 'development' ? show_error('Unable to load middleware: ' . $filename . '.php') : show_error('Uknown error on middleware.');
                }

                require APPPATH . 'middlewares/' . $filename . '.php';

                $ci = &get_instance();
                $object = new $filename($this, $ci);
                $object->run();
                $this->middlewares[$middlewareName] = $object;
            }
        }
    }
}