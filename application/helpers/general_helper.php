<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function procesarResponseApiJsonToArray($response_data)
{
	return json_decode($response_data);
}


function accessToDashboard()
{
	$ci = &get_instance();
	if (isset($ci->session->all_userdata()['menu'])) {
		$menu = $ci->session->all_userdata()['menu'];
		foreach ($menu as $key => $item) {
			if ($item->modulo_id == 1) {
				return 1;
			}
		}
	}

	return 0;
}

function renderInputText($type, $name, $label, $value, $reaonly = false, $placeholder = '', $jsevent = '')
{
	echo "<div class='form-group'>";
	echo "<label for='" . $name . "'>" . $label . "</label>";
	if ($reaonly) {
		echo "<input id='" . $name . "' name='" . $name . "' readonly type='" . $type . "' class='form-control' value='" . $value . "'>";
	} else {
		echo "<input  " . $jsevent . "  id='" . $name . "' placeholder='" . $placeholder . "' name='" . $name . "' type='" . $type . "' class='form-control' value='" . $value . "'>";
	}
	echo "<div id='" . $name . "_error' class='invalid-feedback'></div>";
	echo "</div>";
}

function renderInputTextArea($name, $label, $value, $reaonly = false)
{
	echo "<div class='form-group'>";
	echo "<label for='" . $name . "'>" . $label . "</label>";
	if ($reaonly) {
		echo form_textarea('', $value, 'class="form-control" readonly');
	} else {
		echo form_textarea(['id' => $name, 'name' => $name], $value, 'class="form-control"');
	}
	echo "<div id='" . $name . "_error' class='invalid-feedback'></div>";
	echo "</div>";
}

function renderFileUpload($name, $label, $id = '')
{

	echo "<div class='form-group'>";
	echo "<label for='" . $name . "'><strong>" . $label . "</strong></label>";
	echo form_upload(['name' => $name, 'id' => empty($id) ? $name : $id], '', ['class' => 'form-control-file']);
	echo "<div id='" . $name . "_error' class='invalid-feedback'></div>";
	echo "</div>";
}

function porcentajeProducto($descuento, $precio_producto, $cantidad = 1)
{
	$des = (int) $descuento / 100;
	$precio_total = (100 * $des) + $precio_producto;
	return $precio_total * $cantidad;
}

//porcentaje = Mandar valor de precio en tabla
function obtenerPorcentaje($porcentaje, $precio_producto, $cantidad = 1)
{
	return money_format('%.2n', ($porcentaje / 100 * $precio_producto + $precio_producto) * $cantidad);
}

function totalPrecioCantidadProducto($precio_producto, $cantidad)
{
	return $precio_producto * $cantidad;
}

function obtenerFechaEnLetra($fecha)
{
	$num = date("j", strtotime($fecha));
	$anno = date("Y", strtotime($fecha));
	$mes = array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
	$mes = $mes[(date('m', strtotime($fecha)) * 1) - 1];

	return  $num . ' de ' . $mes . ' del ' . $anno;
}

function renderSelectArray($name, $label = null, $dataArray, $nameValue, $nameOption, $selected = null, $disabled = false, $jsevent = '', $hidden = false)
{
	echo $hidden? "<div class='form-group d-none'>" : "<div class='form-group'>";
	if ($label != null) {
		echo "<label for='" . $name . "'>" . $label . "</label>";
	}
	$disabled = $disabled ? "disabled" : null;

	echo "<select " . $disabled . " " . $jsevent . " class='form-control' name='" . $name . "' id='" . $name . "'>";
	if (is_array($dataArray)) {
		echo "<option value=''> Seleccionar ..</option>";
		foreach ($dataArray as $row) :
			if ($selected != null) {
				if ($row->{$nameValue} == $selected) {
					echo "<option value='" . $row->{$nameValue} . "' selected>" . $row->{$nameOption} . "</option>";
				} else {
					echo "<option value='" . $row->{$nameValue} . "'>" . $row->{$nameOption} . "</option>";
				}
			} else {
				echo "<option value='" . $row->{$nameValue} . "'>" . $row->{$nameOption} . "</option>";
			}
		endforeach;
	}
	echo "</select>";
	echo "<div id='" . $name . "_error' class='invalid-feedback'></div>";
	echo "</div>";
}
function dateDiffMinutes($fecha_inicio = '', $fecha_fin = '')
{
	if ($fecha_inicio == '0000-00-00 00:00:00' || $fecha_fin == '0000-00-00 00:00:00') {
		return 0;
	}
	//NUEVA VERSIÓN
	// ESTABLISH THE MINUTES PER DAY FROM START AND END TIMES
	$start_time = '08:00:00';
	$end_time = '19:00:00';

	$start_ts = strtotime($start_time);
	$end_ts = strtotime($end_time);
	$minutes_per_day = (int)(($end_ts - $start_ts) / 60) + 1;

	// ESTABLISH THE HOLIDAYS
	$holidays = array(
		//'Feb 04', // MLK Day
	);

	// CONVERT HOLIDAYS TO ISO DATES
	foreach ($holidays as $x => $holiday) {
		$holidays[$x] = date('Y-m-d', strtotime($holiday));
	}

	$fecha_sol = $fecha_inicio;
	$fecha_menor = $fecha_fin;

	//var_dump($fecha_sol,$fecha_menor);die();
	// CHECK FOR VALID DATES
	$start = strtotime($fecha_sol);
	$end = strtotime($fecha_menor);
	$start_p = date('Y-m-d H:i:s', $start);
	$end_p = date('Y-m-d H:i:s', $end);

	// MAKE AN ARRAY OF DATES
	$workdays = array();
	$workminutes = array();
	// ITERATE OVER THE DAYS
	$start = $start - 60;
	while ($start < $end) {
		$start = $start + 60;
		// ELIMINATE WEEKENDS - SAT AND SUN
		$weekday = date('D', $start);
		//echo $weekday;die();
		//if (substr($weekday,0,1) == 'S') continue;
		// ELIMINATE HOURS BEFORE BUSINESS HOURS
		$daytime = date('H:i:s', $start);
		if (($daytime < date('H:i:s', $start_ts))) continue;
		// ELIMINATE HOURS PAST BUSINESS HOURS
		$daytime = date('H:i:s', $start);
		if (($daytime > date('H:i:s', $end_ts))) continue;
		// ELIMINATE HOLIDAYS
		$iso_date = date('Y-m-d', $start);
		if (in_array($iso_date, $holidays)) continue;
		$workminutes[] = $iso_date;
		// END ITERATOR
	}

	//
	$number_of_workminutes = (count($workminutes));
	$number_of_minutes = number_format($minutes_per_day);
	$horas_habiles = number_format($number_of_workminutes / 60, 2);

	if ($number_of_workminutes > 0) {
		$number_of_minutes = $number_of_workminutes - 1;
		return $number_of_minutes;
	} else {
		return $number_of_workminutes;
	}
}
