<?php

class AuditoriaInventarioMiddleware
{

    protected $ci;
    protected $controller;

    public function __construct($controller, $ci)
    {
        $this->controller = $controller;
        $this->ci = $ci;    
    }

    public function run()
    {
        $this->ci->load->library('curl');
        $this->ci->load->library('session');

        $response = $this->ci->curl->curlGet(API_URL_BACKEND . 'api/auditoria/activa', true);
        
        $json = json_decode($response, false);

        if($json->auditorias > 0)
        {
            show_error('Proceso de inventario activo no se pueden realizar movimientos de inventario en este momento.', 400, '- Inventario en Proceso -');
        }
    }
}