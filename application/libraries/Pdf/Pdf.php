<?php

namespace Application\Libraries\Pdf;

use Mpdf\Mpdf;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;

class Pdf implements PdfInterface
{
    private $mpdf;

    public function __construct()
    {
        $defaultConfig = (new ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $this->mpdf = new Mpdf([ 
            'tempDir' => APPPATH . 'tmp',
            'fontDir' => array_merge($fontDirs, [
                APPPATH . 'fonts'
            ]),
            'fontdata' => $fontData + [
                'Arial' => [
                    'R' => 'ARIAL.TTF',
                    'B' => 'ARIALBD.TTF',
                ]
            ],
            // 'default_font' => 'Arial',
        ]);

        // $this->mpdf->debug = true;
    }

    public function html($raw)
    {
        $this->mpdf->writeHTML($raw);
    }

    public function output()
    {
        $this->mpdf->Output();
    }

    public function download($filename)
    {
        $this->mpdf->Output($filename, 'D');
    }
}