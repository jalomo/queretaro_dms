<?php

namespace Application\Libraries\Pdf;

Interface PdfInterface
{
    /**
     * Pasamos raw html a esta interface
     *
     * @return void
     */
    public function html($raw);

    /**
     * Retorna el pdf
     *
     * @return PDF
     */
    public function output();

    /**
     * Retorna el pdf en linea
     * 
     * @return PDF
     */
    public function download($filename);
}