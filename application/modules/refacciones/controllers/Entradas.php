<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Entradas extends MY_Controller
{

    public function __construct()
    {
        $this->load->library('curl');
        $this->load->helper('general');
    }

    protected function middleware()
    {
        // MiddlewareName|except:method1,method2
        // MiddlewareName|only:method1,method2
        return ['AuditoriaInventario|only:listadoDevoluciones,crearDevolucion'];
    }

    public function index()
    {
    }

    public function ordencompra()
    {
        // dd($this->input->get('pedido_id'));
        $data['titulo'] = 'Orden de Compra';
        $cat_proveedor = $this->curl->curlGet('api/catalogo-proveedor');
        $data['cat_proveedor'] = procesarResponseApiJsonToArray($cat_proveedor);

        $detalle_pedido = $this->curl->curlGet('api/masterpedidoproducto/' . $this->input->get('pedido_id'));
        $data['detalle_pedido'] = procesarResponseApiJsonToArray($detalle_pedido);

        $cat_tipo_pago = $this->curl->curlGet('api/tipo-pago');
        $data['cat_tipo_pago'] = procesarResponseApiJsonToArray($cat_tipo_pago);


        $this->blade->render('entradas/compras/formulario', $data);
    }

    public function compra($orden_compra_id = '')
    {
        if ($orden_compra_id == '') {
            return $this->ordencompra();
        }

        // $cat_productos = $this->curl->curlGet('api/productos/listadoStock');
        // $data['listado_productos'] = procesarResponseApiJsonToArray($cat_productos);
        $cat_productos = $this->curl->curlGet('api/orden-compra/' . $orden_compra_id);

        $data['orden_compra'] = procesarResponseApiJsonToArray($cat_productos);
        $total = $this->curl->curlGet('api/productos-orden-compra/total-by-orden-compra/' . $orden_compra_id);
        $data['total'] = procesarResponseApiJsonToArray($total);
        $data['orden_compra_id'] = $orden_compra_id;
        $data['titulo'] = 'Orden compra';

        $ubicacion_producto = $this->curl->curlGet('api/catalogo-ubicacion-producto');
        $plazo_credito = $this->curl->curlGet('api/catalogo-plazo-credito');
        $tipo_forma_pago = $this->curl->curlGet('api/catalogo-tipo-forma-pago');
        $tipo_pago = $this->curl->curlGet('api/tipo-pago');
        $cuentas_por_pagar = $this->curl->curlGet('api/cuentas-por-pagar/buscar-por-folio-id/' . $data['orden_compra']->folio_id);

        $decode_plazo_credito = procesarResponseApiJsonToArray($plazo_credito);
        $decode_forma_pago = procesarResponseApiJsonToArray($tipo_forma_pago);
        $decode_tipo_pago = procesarResponseApiJsonToArray($tipo_pago);
        $decode_cuentas = procesarResponseApiJsonToArray($cuentas_por_pagar);

        $data['ubicacion_producto'] = procesarResponseApiJsonToArray($ubicacion_producto);
        $data['plazo_credito'] = $decode_plazo_credito;
        $data['tipo_forma_pago'] = $decode_forma_pago;
        $data['tipo_pago'] = $decode_tipo_pago;
        $data['cxp'] = $decode_cuentas;
        $this->blade->render('entradas/compras/lista_compras', $data);
    }

    public function ajax_total_carrito($orden_compra_id = '')
    {

        echo $this->curl->curlGet('api/productos-orden-compra/total-by-orden-compra/' . $orden_compra_id);
    }

    public function ajax_productos_carrito($orden_compra_id = '')
    {
        $cat_productos_carrito = $this->curl->curlGet('api/orden-compra/lista-productos-carrito/' . $orden_compra_id);
        $data_api = procesarResponseApiJsonToArray($cat_productos_carrito);
        $data['data'] = isset($data_api) ? $data_api->productos : [];
        echo json_encode($data);
    }

    public function detalleCompra($id)
    {
        $data['titulo'] = 'Detalle de venta';
        $data['lista_compras'] = [];
        $this->blade->render('entradas/compras/lista_detalle_compra', $data);
    }

    public function devVentas($venta_id)
    {
        $venta_detalle = $this->curl->curlGet('api/ventas/' . $venta_id);
        $data_api = procesarResponseApiJsonToArray($venta_detalle);
        $data['detalle_venta'] = count($data_api) > 0 ? $data_api[0] : [];
        $detalle_devolucion = $this->curl->curlGet('api/devolucion-venta/get-by-venta-id/' . $venta_id);
        $data_api_devolucion = procesarResponseApiJsonToArray($detalle_devolucion);
        $data['detalle_devolucion'] = count($data_api_devolucion) > 0 ? $data_api_devolucion[0] : [];
        $vendedor = procesarResponseApiJsonToArray($this->curl->curlPost('api/usuarios/usuario-by-rol', [
            'rol_id' => 2
        ]));
    

        // $vendedo$r = $this->curl->curlGet('api/vendedor');
        $data['cat_vendedor'] = $vendedor;
        $data['titulo'] = 'Devolución de ventas';
        $this->blade->render('entradas/compras/dev_ventas/formulario', $data);
    }

    public function listadoDevoluciones()
    {
        $cat_clientes = $this->curl->curlGet('api/clientes');
        $data['cat_clientes'] = procesarResponseApiJsonToArray($cat_clientes);
        $data['titulo'] = 'Devolución de ventas';
        $data['subtitulo'] = 'Lista de devoluciones';

        $this->blade->render('entradas/compras/dev_ventas/listado_devoluciones', $data);
    }

    public function ajax_ventas()
    {
        $parametros = [];
        if ($this->input->post('cliente_id') != '') {
            $parametros['cliente_id'] = $this->input->post('cliente_id');
        }

        if ($this->input->post('estatus_compra') != '') {
            $parametros['estatus_compra'] = [$this->input->post('estatus_compra')];
        }

        $ventas = $this->curl->curlPost('api/ventas/ventas-by-statuscompra', $parametros);
        $detalle = procesarResponseApiJsonToArray($ventas);

        $data['data'] = count($detalle) > 0 ? $detalle : [];
        echo json_encode($data);
    }

    public function crearDevolucion()
    {
        $cat_clientes = $this->curl->curlGet('api/clientes');
        $data['cat_clientes'] = procesarResponseApiJsonToArray($cat_clientes);
        $data['titulo'] = 'Ventas realizadas';
        $data['subtitulo'] = 'Seleccionar venta para devolucion';

        $this->blade->render('entradas/compras/dev_ventas/crear_devolucion', $data);
    }

    public function listadoEntradas()
    {
        $data['titulo'] = "Lista de compras";
        $cat_proveedor = $this->curl->curlGet('api/catalogo-proveedor');
        $data['cat_proveedor'] = procesarResponseApiJsonToArray($cat_proveedor);
        $this->blade->render('entradas/compras/listado', $data);
    }

    public function detalleOrden($numero_orden = '')
    {
        $data['titulo'] = "Detalle de orden";
        $listado = $this->curl->curlGet('api/orden-compra/lista-productos-carrito/' . $numero_orden);
        $data['detalle'] = procesarResponseApiJsonToArray($listado);

        $ordenCompra = $this->curl->curlGet('api/orden-compra/' . $numero_orden);
        $decode_orden_compra = procesarResponseApiJsonToArray($ordenCompra);

        $cuentas_por_pagar = $this->curl->curlGet('api/cuentas-por-pagar/buscar-por-folio-id/' . $decode_orden_compra->folio_id);
        $decode_cuentas = procesarResponseApiJsonToArray($cuentas_por_pagar);
        $data['orden_compra'] = $decode_orden_compra;
        $data['cxp'] = $decode_cuentas;
        $this->blade->render('salidas/detalle_orden', $data);
    }

    public function ajax_detalle_productos()
    {
        $cat_productos_carrito = $this->curl->curlGet('api/venta-producto/detalle-by-id/' . $this->input->post('id'));
        $data_api = procesarResponseApiJsonToArray($cat_productos_carrito);
        $data['data'] = isset($data_api) ? $data_api : [];
        echo json_encode($data);
    }

    public function polizaCompras()
    {
        $data['titulo'] = 'Poliza de compras';
        $this->blade->render('entradas/compras/polizas_compras/reporte', $data);
    }

    public function polizaDevMostrador()
    {
        $data['titulo'] = 'Poliza devolucion de mostrador';
        $data['menu'] = 'menu_entradas_poliza_mostrador';
        $this->blade->render('entradas/compras/polizas_compras/poliza_dev_mostrador', $data);
    }

    public function polizaTraspasos()
    {
        $data['titulo'] = 'Poliza de traspasos';
        $data['menu'] = 'menu_entradas_poliza_traspasos';
        $this->blade->render('entradas/compras/polizas_compras/poliza_dev_mostrador', $data);
    }

    public function autorizarpedido()
    {
        $data['titulo'] = 'Autorizar';
        $this->blade->render('entradas/pedidos/index', $data);
    }

    public function pedidopiezas($pedido_id = '')
    {
        $data['titulo'] = "Pedido de piezas";
        $data['bread_active'] = "pedido";
        $data['pedido_id'] = $pedido_id;

        $catalogoProveedor = $this->curl->curlGet('api/catalogo-proveedor');
        $data['proveedores'] = procesarResponseApiJsonToArray($catalogoProveedor);

        $infopedido = $this->curl->curlGet('api/masterpedidoproducto/' . $pedido_id);
        $data['infopedido'] = procesarResponseApiJsonToArray($infopedido);

        $this->blade->render('entradas/pedidos/detalle_pedido', $data);
    }
}

/* End of file Entradas.php */
