<?php defined('BASEPATH') or exit('No direct script access allowed');

class Cotizador extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->load->library('curl');
    }

    public function index()
    {
        $this->blade->render('refacciones/cotizador/index', ['titulo' => 'Listado cotizaciones']);
    }
    public function form()
    {
        $data['titulo'] = 'Crear cotización';
        $cotizador_id = $this->input->get('cotizador_id');
        if ($cotizador_id) {
            $api_cotizador = $this->curl->curlGet('api/cotizador-refacciones/' . $cotizador_id);
            $data['datos'] = procesarResponseApiJsonToArray($api_cotizador);
        }
        $this->blade->render('cotizador/form', $data);
    }

    public function details()
    {
        $this->blade->render('cotizador/create');
    }
}
