<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Polizas extends MX_Controller
{

    public function __construct()
    {
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function index()
    {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $dataFromApi = $this->curl->curlGet('api/catalogo/tipo-poliza');
        $cat_tipo_poliza = procesarResponseApiJsonToArray($dataFromApi);
        $this->form_validation->set_error_delimiters('<div class="error_container">', '</div>');
        $this->form_validation->set_rules('fecha_inicio', 'fecha de inicio', 'required');
        $this->form_validation->set_rules('fecha_fin', 'fecha de fin', 'required');
        $this->form_validation->set_rules('tipo_poliza_id', 'Tipo de poliza', 'required');
        $data['titulo'] = 'Polizas';
        $data['tipo_poliza'] =  $this->input->get('tipo_poliza');
        $data['cat_tipo_poliza'] = isset($cat_tipo_poliza) ? $cat_tipo_poliza : [];
        
        
        if ($this->form_validation->run() == FALSE) {
            $this->blade->render('polizas/index', $data);
        } else {

            switch ($this->input->post('tipo_poliza_id')) {
                case 1:
                    $this->polizaVentas();
                    break;
                case 2:
                    $this->polizaDevolucionProveedor();
                    break;
                case 4:
                    $this->polizaCompras();
                    break;
                case 5:
                    $this->polizaDevolucionMostrador();
                    break;
                case 6:
                    $this->polizaTraspasos();
                    break;

                default:
                    die("default");
                    break;
            }
        }
    }

    public function polizaTraspasos()
    {
        $data['titulo'] = 'Detalle de poliza';
        $dataFromApi = $this->curl->curlPost('api/polizas/traspasos/filtrar', [
            'fecha_inicio' => $this->input->post('fecha_inicio'),
            'fecha_fin' => $this->input->post('fecha_fin'),
        ]);

        $dataTraspaso = procesarResponseApiJsonToArray($dataFromApi);
        
        $data['detalle_movimientos'] = isset($dataTraspaso->data) && count($dataTraspaso->data) > 0 ? $dataTraspaso->data : [];
        $data['total_generado'] = isset($dataTraspaso->data) && count($dataTraspaso->data) > 0 ? $dataTraspaso->total_generado : 0;
        $data['poliza'] = isset($dataTraspaso->poliza) && count($dataTraspaso->poliza) > 0 ? $dataTraspaso->poliza : [];
        $data['fecha_inicio'] = $this->input->post('fecha_inicio');
        $data['fecha_fin'] = $this->input->post('fecha_fin');
        $this->blade->render('polizas/poliza_traspaso/detalles', $data);
    }

    public function polizaCompras()
    {
        $data['titulo'] = 'Detalle de poliza compras';
        
        $data['fecha_inicio'] = $this->input->post('fecha_inicio');
        $data['fecha_fin'] = $this->input->post('fecha_fin');
        $this->blade->render('polizas/poliza_compras/detalles', $data);
    }

    public function polizaVentas()
    {
        $data['titulo'] = 'Detalle de poliza ventas';
        
        $data['fecha_inicio'] = $this->input->post('fecha_inicio');
        $data['fecha_fin'] = $this->input->post('fecha_fin');
        $this->blade->render('polizas/poliza_ventas/detalles', $data);
    }

    public function polizaDevolucionMostrador()
    {
        $data['titulo'] = 'Detalle de poliza devolución mostrador';
        
        $data['fecha_inicio'] = $this->input->post('fecha_inicio');
        $data['fecha_fin'] = $this->input->post('fecha_fin');
        $this->blade->render('polizas/poliza_devolucionMostrador/detalles', $data);
    }
    public function polizaDevolucionProveedor()
    {
        $data['titulo'] = 'Detalle de poliza devolución proveedor';
        
        $data['fecha_inicio'] = $this->input->post('fecha_inicio');
        $data['fecha_fin'] = $this->input->post('fecha_fin');
        $this->blade->render('polizas/poliza_devolucionProveedor/detalles', $data);
    }

    public function generarPolizaTraspasosPDF()
    {
        $fecha_inicio = $this->input->get('fecha_inicio');
        $fecha_fin = $this->input->get('fecha_fin');
        $folio_id = $this->input->get('folio_id');

        $dataFromApi = $this->curl->curlPost('api/polizas/traspasos/filtrar', [
            'fecha_inicio' => $fecha_inicio,
            'fecha_fin' => $fecha_fin,
            'folio_id' => $folio_id
        ]);
        $dataTraspaso = procesarResponseApiJsonToArray($dataFromApi);
        $data['detalle_movimientos'] = isset($dataTraspaso->data) && count($dataTraspaso->data) > 0 ? $dataTraspaso->data : [];
        $data['total_generado'] = isset($dataTraspaso->data) && count($dataTraspaso->data) > 0 ? $dataTraspaso->total_generado : 0;
        $data['poliza'] = isset($dataTraspaso->poliza) && count($dataTraspaso->poliza) > 0 ? $dataTraspaso->poliza : [];
        $data['fecha_inicio'] = $fecha_inicio;
        $data['fecha_fin'] = $fecha_fin;
        $view = $this->load->view('polizas/poliza_traspaso/pdfByFechas', $data, true);
        $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
    }

    public function generarPolizaComprasPDF()
    {
        $fecha_inicio = $this->input->get('fecha_inicio');
        $fecha_fin = $this->input->get('fecha_fin');
        $folio_id = $this->input->get('folio_id');
        $proveedor_id = $this->input->get('proveedor_id');
        if ($proveedor_id) {
            $apiProveedor = $this->curl->curlGet('api/catalogo-proveedor/' . $proveedor_id);
            $proveedor = procesarResponseApiJsonToArray($apiProveedor);
        }

        $apiListadoCompras = $this->curl->curlPost('api/polizas/compras/listado', [
            'fecha_inicio' => $fecha_inicio,
            'fecha_fin' => $fecha_fin,
            'folio_id' => $folio_id
        ]);

        $apiListadoTotales = $this->curl->curlPost('api/polizas/compras/totales', [
            'fecha_inicio' => $fecha_inicio,
            'fecha_fin' => $fecha_fin,
            'folio_id' => $folio_id
        ]);

        $listadoCompras = procesarResponseApiJsonToArray($apiListadoCompras);
        $totalesCompras = procesarResponseApiJsonToArray($apiListadoTotales);
        $data = [
            'compras' => isset($listadoCompras->data) && count($listadoCompras->data) > 0 ? $listadoCompras->data : [],
            'totales' => isset($totalesCompras) ? $totalesCompras : [],
            'fecha_inicio' => $fecha_inicio,
            'fecha_fin' => $fecha_fin,
        ];
        if(isset($proveedor)){
            $data['proveedor'] = current($proveedor);
        }
        //TODO: PDF
        // $this->load->view('polizas/poliza_compras/pdfByFechas', $data);
        $view = $this->load->view('polizas/poliza_compras/pdfByFechas', $data, true);
        // dd(base64_encode($view));
        $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
    }

    public function generarPolizaVentasPDF()
    {
        $fecha_inicio = $this->input->get('fecha_inicio');
        $fecha_fin = $this->input->get('fecha_fin');
        $folio_id = base64_decode($this->input->get('folio_id'));

        $apiListadoVentas = $this->curl->curlPost('api/polizas/ventas/listado', [
            'fecha_inicio' => $fecha_inicio,
            'fecha_fin' => $fecha_fin,
            'folio_id' => $folio_id
        ]); 

        $apiTotalesVentas = $this->curl->curlPost('api/polizas/ventas/totales', [
            'fecha_inicio' => $fecha_inicio,
            'fecha_fin' => $fecha_fin,
            'folio_id' => $folio_id
        ]);
        $folio_data = $this->curl->curlGet('api/ventas/venta-by-folio/' . $folio_id);
        $listado = procesarResponseApiJsonToArray($apiListadoVentas);
        $totales = procesarResponseApiJsonToArray($apiTotalesVentas);
        $venta = procesarResponseApiJsonToArray($folio_data);

        $data = [
            'ventas' => isset($listado->data) && count($listado->data) > 0 ? $listado->data : [],
            'servicios' => isset($servicios) ?  $servicios : [],
            'totales' => isset($totales) ? $totales : [],
            'fecha_inicio' => $fecha_inicio,
            'fecha_fin' => $fecha_fin,
            'datos' => current($venta)
        ];
        // TODO : NO GENERA PDF
        $view = $this->load->view('polizas/poliza_ventas/comprobante_pago', $data, true);
        $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
    }

    public function generarPolizaVentasServicioPDF()
    {
        $fecha_inicio = $this->input->get('fecha_inicio');
        $fecha_fin = $this->input->get('fecha_fin');
        $folio_id = $this->input->get('folio_id');

        $apiListadoVentas = $this->curl->curlPost('api/polizas/ventas/listado', [
            'fecha_inicio' => $fecha_inicio,
            'fecha_fin' => $fecha_fin,
            'folio_id' => $folio_id
        ]); 

        $apiTotalesVentas = $this->curl->curlPost('api/polizas/ventas/totales', [
            'fecha_inicio' => $fecha_inicio,
            'fecha_fin' => $fecha_fin,
            'folio_id' => $folio_id
        ]);

        $apiListadoServicios = $this->curl->curlPost('api/polizas/ventas/servicios', [
            'folio_id' => $folio_id
        ]); 

        $listado = procesarResponseApiJsonToArray($apiListadoVentas);
        $servicios = procesarResponseApiJsonToArray($apiListadoServicios);
        $totales = procesarResponseApiJsonToArray($apiTotalesVentas);

        $data = [
            'ventas' => isset($listado->data) && count($listado->data) > 0 ? $listado->data : [],
            'totales' => isset($totales) ? $totales : [],
            'servicios' => isset($servicios) ?  $servicios : [],
            'fecha_inicio' => $fecha_inicio,
            'fecha_fin' => $fecha_fin
        ];
        // TODO : NO GENERA PDF
        // $this->load->view('polizas/poliza_ventas/pdfByFechas', $data);
        $view = $this->load->view('polizas/poliza_ventas_servicios/pdfByFechas', $data, true);
        $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
    }

    public function generarPolizaDevolucionMostradorPDF()
    {
        $fecha_inicio = $this->input->get('fecha_inicio');
        $fecha_fin = $this->input->get('fecha_fin');
        $folio_id = $this->input->get('folio_id');
        $apiListadoVentas = $this->curl->curlPost('api/polizas/devolucion_ventas/listado', [
            'fecha_inicio' => $fecha_inicio,
            'fecha_fin' => $fecha_fin,
            'folio_id' => $folio_id
        ]); 

        $apiTotalesVentas = $this->curl->curlPost('api/polizas/devolucion_ventas/totales', [
            'fecha_inicio' => $fecha_inicio,
            'fecha_fin' => $fecha_fin,
            'folio_id' => $folio_id
        ]);

        $listado = procesarResponseApiJsonToArray($apiListadoVentas);
        $totales = procesarResponseApiJsonToArray($apiTotalesVentas);

        $data = [
            'devolucion_ventas' => isset($listado->data) && count($listado->data) > 0 ? $listado->data : [],
            'totales' => isset($totales) ? $totales : [],
            'fecha_inicio' => $fecha_inicio,
            'fecha_fin' => $fecha_fin
        ];
        
        // TODO : NO GENERA PDF
        // $this->load->view('polizas/poliza_ventas/pdfByFechas', $data);
        $view = $this->load->view('polizas/poliza_devolucionMostrador/pdfByFechas', $data, true);
        $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
    }

    
    public function generarPolizaDevolucionProveedorPDF()
    {
        $fecha_inicio = $this->input->get('fecha_inicio');
        $fecha_fin = $this->input->get('fecha_fin');
        $folio_id = $this->input->get('folio_id');
        $apiListadoVentas = $this->curl->curlPost('api/polizas/devolucion_proveedor/listado', [
            'fecha_inicio' => $fecha_inicio,
            'fecha_fin' => $fecha_fin,
            'folio_id' => $folio_id
        ]); 

        $apiTotalesVentas = $this->curl->curlPost('api/polizas/devolucion_proveedor/totales', [
            'fecha_inicio' => $fecha_inicio,
            'fecha_fin' => $fecha_fin,
            'folio_id' => $folio_id
        ]);

        $listado = procesarResponseApiJsonToArray($apiListadoVentas);
        $totales = procesarResponseApiJsonToArray($apiTotalesVentas);

        $data = [
            'devolucion_proveedor' => isset($listado->data) && count($listado->data) > 0 ? $listado->data : [],
            'totales' => isset($totales) ? $totales : [],
            'fecha_inicio' => $fecha_inicio,
            'fecha_fin' => $fecha_fin
        ];
        // utils::pre($data);
        
        // TODO : NO GENERA PDF
        // $this->load->view('polizas/poliza_ventas/pdfByFechas', $data);
        $view = $this->load->view('polizas/poliza_devolucionProveedor/pdfByFechas', $data, true);
        $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
    }
}

/* End of file Polizas.php */
