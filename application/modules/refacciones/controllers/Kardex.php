<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kardex extends MX_Controller
{

    public function __construct()
    {
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function index()
    {
        $data['titulo'] = 'Kardex';
        $this->load->helper(array('form', 'url'));
        $cat_clientes = $this->curl->curlGet('api/clientes');
        $data['cat_clientes'] = procesarResponseApiJsonToArray($cat_clientes);
        $this->blade->render('kardex/kardex_busqueda', $data);
    }

    public function kardexproducto()
    {
        $parametros = [
            'id' => $this->input->get('producto_id'),
            'fecha_inicio' => $this->input->get('fecha_inicio'),
            'fecha_termino' =>$this->input->get('fecha_termino'),
        ];

        $data['titulo'] = 'Detalle de movimientos';
        $dataFromApi = $this->curl->curlGet("api/kardex/detalle/{$parametros['id']}?" . http_build_query($parametros));
        $data['kardex'] =  procesarResponseApiJsonToArray($dataFromApi);  
        
        $this->blade->render('kardex/resultado', $data);
    }

    public function kardexTraspasos()
    {
        $data['titulo'] = 'Detalle de movimientos';
        $dataFromApi = $this->curl->curlPost('api/cardex/traspasos/filtrar', [
            'no_identificacion' => $this->input->post('no_identificacion'),
            'fecha_inicio' => $this->input->post('fecha_inicio'),
            'fecha_fin' => $this->input->post('fecha_fin'),
            'movimiento_id' => $this->input->post('movimiento_id')
        ]);
        $dataTraspaso = procesarResponseApiJsonToArray($dataFromApi);

        $data['detalle_movimientos'] = isset($dataTraspaso->data) && count($dataTraspaso->data) > 0 ? $dataTraspaso->data : [];
        $data['total_generado'] = isset($dataTraspaso->data) && count($dataTraspaso->data) > 0 ? $dataTraspaso->total_generado : 0;
        $data['poliza'] = isset($dataTraspaso->poliza) && count($dataTraspaso->poliza) > 0 ? $dataTraspaso->poliza : [];
        $data['fecha_inicio'] = $this->input->post('fecha_inicio');
        $data['fecha_fin'] = $this->input->post('fecha_fin');
        $this->blade->render('kardex/detalles', $data);
    }


    public function detallemovimientos()
    {

        $data['titulo'] = 'Kardex';
        $dataFromApi = $this->curl->curlPost('api/cardex/traspasos/filtrar', [
            // 'no_identificacion' => $this->input->post('no_identificacion'),
            'fecha_inicio' => '2020-05-1',
            'fecha_fin' => '2020-05-31',
            // 'movimiento_id' => $this->input->post('movimiento_id')
        ]);
        $dataTraspaso = procesarResponseApiJsonToArray($dataFromApi);
        $data['detalle_movimientos'] = isset($dataTraspaso->data) && count($dataTraspaso->data) > 0 ? $dataTraspaso->data : [];
        $data['total_generado'] = isset($dataTraspaso->data) && count($dataTraspaso->data) > 0 ? $dataTraspaso->total_generado : 0;
        $data['fecha_inicio'] = date('d/m/yy');
        $data['fecha_fin'] = date('d/m/yy');
        $this->blade->render('kardex/detalles', $data);
    }

    public function ajax_get_lista_kardex()
    {
        if ($this->input->post('no_identificacion') == '') {
            $data['data'] = [];
            echo json_encode($data);
            die();
        }
        $dataApi = $this->curl->curlPost('api/productos/buscar-piezas', ['no_identificacion' => $this->input->post('no_identificacion')]);

        $data['data'] = isset($dataApi) ? json_decode($dataApi)  : [];
        echo json_encode($data);
    }
}

/* End of file Kardex.php */
