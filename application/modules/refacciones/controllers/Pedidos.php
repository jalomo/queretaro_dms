<?php defined('BASEPATH') or exit('No direct script access allowed');

class Pedidos extends MY_Controller
{

    protected function middleware()
    {
        // MiddlewareName|except:method1,method2
        // MiddlewareName|only:method1,method2
        return ['AuditoriaInventario'];
    }
    
    public function index()
    {
        $this->blade->render('pedidos/index');
    }

    public function nuevo()
    {
        $this->blade->render('pedidos/create');
    }

    public function editar($id)
    {
        $this->blade->render('pedidos/edit', [
            'id' => $id
        ]);
    }

    public function venta($id)
    {
        $this->blade->render('pedidos/venta', [
            'id' => $id,
        ]);
    }

    public function cargamento($id)
    {
        $this->blade->render('pedidos/cargamento', [
            'id' => $id
        ]);
    }

    public function crear_cargamento()
    {
        $this->blade->render('pedidos/crear-cargamento');
    }
}