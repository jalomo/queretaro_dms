<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Salidas extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('general');
        $this->load->library('curl');
    }

    protected function middleware()
    {
        // MiddlewareName|except:method1,method2
        // MiddlewareName|only:method1,method2
        return ['AuditoriaInventario|only:detalleVentaServiceExcellent,listadoVentas,ventasMostrador'];
    }

    public function listadoVentas()
    {
        $cat_clientes = $this->curl->curlGet('api/clientes/select-catalogo');
        $data['cat_clientes'] = procesarResponseApiJsonToArray($cat_clientes);
        $data['titulo'] = "Listado de ventas";

        $this->blade->render('ventas/listado_ventas', $data);
    }

    public function ventanillaTaller()
    {

        $dataFromApi = $this->curl->curlGet('api/productos/stockActual?compra_realizada=true');
        $data['listado'] = procesarResponseApiJsonToArray($dataFromApi);
        $data['titulo'] = "Ventanilla taller";
        $data['menu'] = "ventanilla_taller";
        $this->blade->render('ventanilla_taller/listado_productos', $data);
    }

    public function presupuestoMultipunto()
    {
        $data['titulo'] = "Presupuestos Ventanilla";

        $this->blade->render('ventanilla_taller/panel_ventanilla', $data);
    }

    public function psniClientes()
    {
        $data['titulo'] = "PSNI Cliente";

        $this->blade->render('ventanilla_taller/panel_psni', $data);
    }

    public function psniGarantias()
    {
        $data['titulo'] = "PSNI Garantía";

        $this->blade->render('ventanilla_taller/panel_psni_garantia', $data);
    }

    public function ventasMostrador()
    {
        // se cambia funcionalidad por función venta_mostrador
        return $this->venta_mostrador();
        $dataFromApi = $this->curl->curlGet('api/productos/stockActual');
        $data['listado'] = procesarResponseApiJsonToArray($dataFromApi);
        $data['titulo'] = "Ventas mostrador";
        $data['menu'] = "ventas_mostrador";
        $this->blade->render('ventas_mostrador/listado_productos', $data);
    }

    public function realizarVenta($id_producto, $almacen_id = 1)
    {
        $this->load->helper('general');
        $producto = $this->curl->curlGet('api/productos/stockByProductoId/' . $id_producto);
        $cat_talleres = $this->curl->curlGet('api/talleres');
        $cat_precios = $this->curl->curlGet('api/precios?id_vista=1');
        $cat_tipo_clientes = $this->curl->curlGet('api/catalogo-tipo-cliente');

        $data['producto'] = procesarResponseApiJsonToArray($producto);
        $data['cat_tipo_clientes'] = procesarResponseApiJsonToArray($cat_tipo_clientes);
        $data['vendedor_id'] = $this->session->userdata('id');
        $data['almacen_id'] = $almacen_id;

        $data['cat_talleres'] = procesarResponseApiJsonToArray($cat_talleres);
        $data['cat_precios'] = procesarResponseApiJsonToArray($cat_precios);
        $data['titulo'] = "Vender producto";
        $data['tipo_venta_id'] = $this->uri->segment(5) && $this->uri->segment(5) == 'ventanilla_taller' ? 2 : 1;

        //Cargamos el catalogo de claves de cliente
        $clave_cliente = $this->curl->curlGet('api/catalogo-clave-cliente');
        $data['cat_clave'] = procesarResponseApiJsonToArray($clave_cliente);
        $this->blade->render('ventas_mostrador/ventas', $data);
    }

    public function reporteventas()
    {
        // $data['titulo'] = "Ventas {$this->session->userdata('nombre')} {$this->session->userdata('apellido_paterno')}";
        $api_usuarios = $this->curl->curlGet('api/usuarios');
        $data['vendedores'] = procesarResponseApiJsonToArray($api_usuarios);
        // $data['user_nombre'] = "{$this->session->userdata('nombre')} {$this->session->userdata('apellido_paterno')}";

        // dd($this->session->all_userdata());
        $this->blade->render('ventas/reporte_ventas', $data);
    }

    public function otrasSalidas()
    {
        $data['titulo'] = "Otras Salidas";

        $cat_clientes = $this->curl->curlGet('api/clientes/tipo-clave?clave=CV');
        $data['catalogo_clientes'] = procesarResponseApiJsonToArray($cat_clientes);

        $producto = $this->curl->curlGet('api/productos');
        $validar = procesarResponseApiJsonToArray($producto);
        $data['producto'] = count($validar) > 0 ? $validar : [];

        $this->blade->render('almacenes/otras_salidas/formulario', $data);
    }

    public function ajax_ordenes_abiertas()
    {
        //Cargamos las ordenes abiertas
        $ordenes = $this->curl->curlGet('api/ventas/listado-ordenes');
        echo $ordenes;
    }


    #precarga informacion de la orden - cliente asesor etc.
    public function ajax_data_salidas($id_cita = '')
    {
        $this->load->model('MConsultas');
        //$data['asesor'] = $this->MConsultas->getCita_info($id_cita);
        $query = $this->MConsultas->getCita_info_completa($id_cita);
        $data['asesor'] = $query;
        echo json_encode($data);
    }

    #precarga productos de 1 orden
    public function ajax_data_salidas_producto()
    {

        if (!$this->input->post('orden')  || $this->input->post('orden') == '') {
            $data['data'] = [];
            echo json_encode($data);
            die();
        }

        $request = $this->curl->curlGet('api/ventas/taller/orden/' . $this->input->post('orden'));
        $detalle = procesarResponseApiJsonToArray($request);
        $data['data'] = isset($detalle) ? $detalle : [];
        echo json_encode($data);
    }

    public function ajax_busqueda_pieza()
    {
        $data_search = [];
        if (!empty($this->input->post('descripcion_pieza'))) {
            $data_search['descripcion'] = $this->input->post('descripcion_pieza');
        }

        if (!empty($this->input->post('no_identificacion'))) {
            $data_search['no_identificacion'] = $this->input->post('no_identificacion');
        }
        $request = $this->curl->curlGet('api/productos/listadoStock?cantidad_actual=true&' . http_build_query($data_search));


        if (isset($request) && isset($request['status_code']) == 400) {
            $data['data'] = [];
            echo json_encode($data);
            die();
        }

        $detalle = procesarResponseApiJsonToArray($request);
        $data['data'] = isset($detalle) ? $detalle : [];
        echo $request;
        // echo json_encode($request);
    }

    public function ajax_data_id_cliente($numero_cliente = '')
    {

        $totalVenta = $this->curl->curlPost('api/clientes/numero-cliente', [
            'numero_cliente' => $numero_cliente
        ]);
        $data['cliente'] = procesarResponseApiJsonToArray($totalVenta);
        echo json_encode($data);
    }

    public function ajax_data_cliente($id_cliente = '')
    {
        $totalVenta = $this->curl->curlGet('api/clientes/' . $id_cliente);
        $data['cliente'] = procesarResponseApiJsonToArray($totalVenta);
        echo json_encode($data);
    }

    public function devolucion()
    {
        $data['titulo'] = "Devolución a proveedor";
        $cat_proveedor = $this->curl->curlGet('api/catalogo-proveedor');
        $cat_estatus_compras = $this->curl->curlGet('api/catalogo-estatus-compras');
        $data['cat_proveedor'] = procesarResponseApiJsonToArray($cat_proveedor);
        $data['cat_estatus_compras'] = procesarResponseApiJsonToArray($cat_estatus_compras);

        $this->blade->render('almacenes/dev_proveedor/listado_devolucion_proveedor', $data);
    }

    public function listadoDevoluciones()
    {
        $data['titulo'] = "Devoluciones a proveedor realizadas";
        $cat_proveedor = $this->curl->curlGet('api/catalogo-proveedor');
        $data['cat_proveedor'] = procesarResponseApiJsonToArray($cat_proveedor);

        $this->blade->render('almacenes/dev_proveedor/devoluciones_realizadas', $data);
    }

    public function detallesDevolucion()
    {
        $data['titulo'] = "Devolucion a proveedor";
        $orden_compra_id = $this->input->get('orden_compra_id');
        $estatus_compra_id = $this->input->get('estatus_compra_id');
        $apiOrdenCompra = $this->curl->curlGet('api/orden-compra/busqueda-compras?estatus_compra_id=' . $estatus_compra_id . '&orden_compra_id=' . $orden_compra_id);
        $apiDevolucionesByOrdenCompra = $this->curl->curlGet('api/devoluciones/get-by-compra_id/' . $orden_compra_id);
        $arrayOrdenCompra = procesarResponseApiJsonToArray($apiOrdenCompra);
        $data['orden_compra'] = isset($arrayOrdenCompra->data) ? current($arrayOrdenCompra->data) : false;
        $data['devolucion'] = procesarResponseApiJsonToArray($apiDevolucionesByOrdenCompra);
        $listado = $this->curl->curlGet('api/orden-compra/lista-productos-carrito/' . $orden_compra_id);
        $data['detalle'] = procesarResponseApiJsonToArray($listado);
        $this->blade->render('almacenes/dev_proveedor/formulario', $data);
    }

    public function polizaOtrasSalidas()
    {
        $data['titulo'] = "Devolucion a proveedor";

        $cat_proveedor = $this->curl->curlGet('api/talleres');
        $data['cat_proveedor'] = procesarResponseApiJsonToArray($cat_proveedor);
        $this->blade->render('almacenes/poliza_ventas/formulario', $data);
    }

    public function polizadevProveedor()
    {
        $data['titulo'] = 'Reporte de poliza';
        $this->blade->render('almacenes/poliza_dev_proveedor/reporte', $data);
    }

    // TODO: poliza por movimiento
    public function polizaVentas($id)
    {
        $data['titulo'] = "ventas mostrador";
        $detalleVenta = $this->curl->curlGet('api/venta-producto/detalle-by-id/' . $id);
        $detalle = procesarResponseApiJsonToArray($detalleVenta);
        $folio_data = $this->curl->curlGet('api/ventas/venta-by-folio/' . $id);
        $data['folio'] = procesarResponseApiJsonToArray($folio_data);
        $data['ventas'] = isset($detalle) ? $detalle : [];

        $this->load->view('polizas/poliza_ventas/template', $data);
    }

    public function ajax_detalle_venta_carrito()
    {
        $detalleVenta = $this->curl->curlGet('api/refacciones/venta/' . $this->input->post('id'));
        $detalle = procesarResponseApiJsonToArray($detalleVenta);

        echo json_encode($detalle);
    }


    public function ajax_detalle_venta_mpm()
    {

        if ($this->input->post('id') == '') {
            $data['data'] = [];
            echo json_encode($data);
            die();
        }

        $detalleVenta = $this->curl->curlGet('api/ventas/detalle-mpm/' . $this->input->post('id'));
        $detalle = procesarResponseApiJsonToArray($detalleVenta);

        // $newArrayData = [];
        // if ($detalle) {
        //     foreach ($detalle as $key => $item) {
        //         if (isset($item->estatus_producto_orden)) {
        //             if ($item->estatus_producto_orden !== 'B') {
        //                 $newArrayData[] = $item;
        //             }
        //         }
        //     }
        // }

        // $data['data'] = count($newArrayData) > 0 ? $newArrayData : [];
        echo json_encode([
            'data' => $detalle,
        ]);
    }

    public function refacciones_operacion()
    {

        if ($this->input->post('venta_id') == '') {
            $data['data'] = [];
            echo json_encode($data);
            die();
        }

        $detalleVenta = $this->curl->curlPost('api/ventas/detalle-operacion', [
            'venta_id' => $this->input->post('venta_id')
        ]);
        $detalle = procesarResponseApiJsonToArray($detalleVenta);
        $data['data'] = count($detalle) > 0 ? $detalle : [];
        echo json_encode($data);
    }

    public function detalleVentaServiceExcellent($folio_id)
    {
        $folio_data = $this->curl->curlGet('api/ventas/venta-by-folio/' . $folio_id);
        $parsed_folio = procesarResponseApiJsonToArray($folio_data);
        $data['folio'] = $parsed_folio;
        $numero_orden = $parsed_folio->numero_orden;
        
        $infoVehiculo = $this->curl->curlGet('api/ventas/taller/info-vehiculo/' . $numero_orden);
        $decode_infoVehiculo = procesarResponseApiJsonToArray($infoVehiculo);
        
        $cat_precios = $this->curl->curlGet('api/precios');
        $data['cat_precios'] = procesarResponseApiJsonToArray($cat_precios);
        
        $data['info_vehiculo'] = $decode_infoVehiculo;
        $data['numero_orden'] = $numero_orden;
        $data['venta_id'] = $parsed_folio->id;
        $data['titulo'] = "Detalle";
        $data['user_id'] = $this->session->id;
        $data['role'] = $this->session->rol_id == 1? 'administrador': '';
        
        $this->blade->render('ventas_mostrador/detalleventa_serviceexcelent', $data);
    }

    public function detalleVentaServiceExcellentInfo($numero_orden)
    {
        $this->blade->render('ventas_mostrador/detalleventa_serviceexcelent_info', [ 'numero_orden' => $numero_orden]);
    }

    public function detalleventa($folio_id)
    {

        $data['titulo'] = "Completar venta mostrador";
        $api_folio_data = $this->curl->curlGet('api/ventas/venta-by-folio/' . $folio_id);
        $cat_precios = $this->curl->curlGet('api/precios');
        $cuentas_por_cobrar = $this->curl->curlGet('api/cuentas-por-cobrar/buscar-por-folio-id/' . $folio_id);
        $apiTotalVenta = $this->curl->curlPost('api/polizas/ventas/totales', ['folio_id' => $folio_id]);
        $api_clientes = $this->curl->curlGet('api/clientes/select-catalogo');
        $api_cuentas_por_cobrar = $this->curl->curlGet('api/cuentas-por-cobrar/buscar-por-folio-id/' . $folio_id);

        $tipo_forma_pago = $this->curl->curlGet('api/catalogo-tipo-forma-pago');
        $tipo_pago = $this->curl->curlGet('api/tipo-pago');
        $plazo_credito = $this->curl->curlGet('api/catalogo-plazo-credito');
        $usuariosGestores = $this->curl->curlPost('api/usuarios/usuario-by-rol', [
            'rol_id' => 5,
        ]);

        $data_folio = procesarResponseApiJsonToArray($api_folio_data);

        $data['datos'] = $data_folio ? current($data_folio) : [];
        $data['cat_precios'] = procesarResponseApiJsonToArray($cat_precios);
        $data['cat_clientes'] = procesarResponseApiJsonToArray($api_clientes);
        $data['cxc'] = procesarResponseApiJsonToArray($cuentas_por_cobrar);
        $data['totales'] = procesarResponseApiJsonToArray($apiTotalVenta);
        $data['plazo_credito'] = procesarResponseApiJsonToArray($plazo_credito);
        $data['tipo_forma_pago'] = procesarResponseApiJsonToArray($tipo_forma_pago);
        $data['tipo_pago'] = procesarResponseApiJsonToArray($tipo_pago);
        $data['gestores'] = procesarResponseApiJsonToArray($usuariosGestores);
        $data['cxc'] = procesarResponseApiJsonToArray($api_cuentas_por_cobrar);
        $this->blade->render('ventas_mostrador/detalleventa', $data);
    }

    public function ajax_calcular_venta_total($folio_id = '')
    {
        $totalVenta = $this->curl->curlGet('api/venta-producto/total-venta-by-folio/' . $folio_id);
        $data['venta_total'] = procesarResponseApiJsonToArray($totalVenta);
        echo json_encode($data);
    }

    public function test()
    {

        $this->blade->render('test/respaldo/detalle');
    }

    public function listadoOtrasSalidas()
    {
        $data['titulo'] = "Otras salidas serv Exce.";
        $this->blade->render('salidas/otras_salidas/listado', $data);
    }

    public function ajax_busqueda_presupuesto()
    {
        $this->load->model('MConsultas');
        if ($this->input->post('campo') !== '') {
            $consulta_data = $this->MConsultas->listaPresupuestoCampo($this->input->post('campo'));
        } else if ($this->input->post('filtro_ventanillas') !== '') {
            $consulta_data = $this->MConsultas->listaCotizacionEstatus($this->input->post('filtro_ventanillas'));
        } else if ($this->input->post('campo') !== '' && $this->input->post('filtro_ventanillas') !== '') {
            $consulta_data = $this->MConsultas->listaPresupuestoEstatusCampo($this->input->post('campo'), $this->input->post('filtro_ventanillas'));
        } else {
            $consulta_data = $this->MConsultas->listaPresupuestoCampo();
        }

        $data_final = [];
        foreach ($consulta_data as $key => $row) {
            if ($row->envia_ventanilla == "0") {
                $color = "";
                if ($row->acepta_cliente != "No") {
                    if ($row->estado_refacciones == "0") {
                        $color = "#f5acaa;";
                    } elseif ($row->estado_refacciones == "1") {
                        $color = "#f5ff51;";
                    } elseif ($row->estado_refacciones == "2") {
                        $color  = "#5bc0de;";
                    } else {
                        $color  = "#c7ecc7;";
                    }
                } else {
                    $color  = "red";
                }

                $acepta_cliente = "";
                if ($row->acepta_cliente == "") {
                    $acepta_cliente = "SIN CONFIRMAR";
                } else if ($row->acepta_cliente == "Si") {
                    $acepta_cliente = "CONFIRMADA";
                } else if ($row->acepta_cliente == "Val") {
                    $acepta_cliente = "DETALLADA";
                } else {
                    $acepta_cliente = "RECHAZADA";
                }

                $estado_refaccion = "";
                if (($row->estado_refacciones == "0") && ($row->acepta_cliente != "No")) {
                    $estado_refaccion = "SIN SOLICITAR";
                } else if ($row->acepta_cliente == "No") {
                    $estado_refaccion = "RECHAZADA";
                } else if ($row->acepta_cliente == "1") {
                    $estado_refaccion = "SOLICITADAS";
                } else if ($row->acepta_cliente == "2") {
                    $estado_refaccion = "RECIBIDAS";
                } else if ($row->acepta_cliente == "3") {
                    $estado_refaccion = "ENTREGADAS";
                } else {
                    $estado_refaccion = "SIN SOLICITAR";
                }

                array_push($data_final, [
                    'id_cita' => $row->id_cita,
                    "tipo" => (((int) $row->ref_garantias > 0) ? "CON GARANTÍA" : "TRADICIONAL"),
                    'ruta' => "1",
                    "envia_ventanilla" => $row->envia_ventanilla,
                    "estado_refacciones" => $estado_refaccion,
                    "estado_refacciones_garantias" => $row->estado_refacciones_garantias,
                    "firma_asesor" => $row->firma_asesor,
                    "envio_jdt" => $row->envio_jdt,
                    "firma_requisicion" => $row->firma_requisicion,
                    "firma_requisicion_garantias" => $row->firma_requisicion_garantias,
                    "ubicacion_proceso" => $row->ubicacion_proceso,
                    "acepta_cliente" => $acepta_cliente,
                    "serie" => $row->serie,
                    "tecnico" => $row->tecnico,
                    "asesor" => $row->asesor,
                    "modelo" => $row->vehiculo_modelo,
                    "placas" => $row->vehiculo_placas,
                    "color" => $color
                ]);
            } else {
                if ((int) $row->ref_tradicional > 0) {
                    $color = "";
                    if ($row->acepta_cliente != "No") {
                        if ($row->estado_refacciones == "0") {
                            $color = "#f5acaa;";
                        } elseif ($row->estado_refacciones == "1") {
                            $color = "#f5ff51;";
                        } elseif ($row->estado_refacciones == "2") {
                            $color  = "#5bc0de;";
                        } else {
                            $color  = "#c7ecc7;";
                        }
                    } else {
                        $color  = "red";
                    }

                    $acepta_cliente = "";
                    if ($row->acepta_cliente == "") {
                        $acepta_cliente = "SIN CONFIRMAR";
                    } else if ($row->acepta_cliente == "Si") {
                        $acepta_cliente = "CONFIRMADA";
                    } else if ($row->acepta_cliente == "Val") {
                        $acepta_cliente = "DETALLADA";
                    } else {
                        $acepta_cliente = "RECHAZADA";
                    }

                    $estado_refaccion = "";
                    if (($row->estado_refacciones == "0") && ($row->acepta_cliente != "No")) {
                        $estado_refaccion = "SIN SOLICITAR";
                    } else if ($row->acepta_cliente == "No") {
                        $estado_refaccion = "RECHAZADA";
                    } else if ($row->acepta_cliente == "1") {
                        $estado_refaccion = "SOLICITADAS";
                    } else if ($row->acepta_cliente == "2") {
                        $estado_refaccion = "RECIBIDAS";
                    } else if ($row->acepta_cliente == "3") {
                        $estado_refaccion = "ENTREGADAS";
                    } else {
                        $estado_refaccion = "SIN SOLICITAR";
                    }

                    array_push($data_final, [
                        'id_cita' => $row->id_cita,
                        "tipo" => "TRADICIONAL",
                        'ruta' => "1",
                        "envia_ventanilla" => $row->envia_ventanilla,
                        "estado_refacciones" => $estado_refaccion,
                        "estado_refacciones_garantias" => $row->estado_refacciones_garantias,
                        "firma_asesor" => (($row->firma_asesor != "") ? "SI" : "NO"),
                        "envio_jdt" => $row->envio_jdt,
                        "firma_requisicion" => $row->firma_requisicion,
                        "firma_requisicion_garantias" => $row->firma_requisicion_garantias,
                        "ubicacion_proceso" => $row->ubicacion_proceso,
                        "acepta_cliente" => $acepta_cliente,
                        "serie" => $row->serie,
                        "tecnico" => $row->tecnico,
                        "asesor" => $row->asesor,
                        "modelo" => $row->vehiculo_modelo,
                        "placas" => $row->vehiculo_placas,
                        "color" => $color
                    ]);
                }
            }
        }
        $data['data'] = $data_final;
        echo json_encode($data);
    }

    public function devolverPieza()
    {
        $data['titulo'] = "Devolucion por pieza";
        $orden_compra_id = $this->input->get('orden_compra_id');
        $estatus_compra_id = $this->input->get('estatus_compra_id');
        $data['orden_compra_id'] = $orden_compra_id;

        $query = [];
        if ($this->input->get('orden_compra_id')) {
            $query['orden_compra_id'] = $this->input->get('orden_compra_id');
        }

        if ($this->input->post('estatus_compra_id')) {
            $query['estatus_compra_id'] = $this->input->post('estatus_compra_id');
        }

        $apiOrdenCompra = $this->curl->curlGet('api/orden-compra/busqueda-compras?' . http_build_query($query));
        $arrayOrdenCompra = procesarResponseApiJsonToArray($apiOrdenCompra);

        $data['orden_compra'] = isset($arrayOrdenCompra->data) ? current($arrayOrdenCompra->data) : false;
        $listado = $this->curl->curlGet('api/orden-compra/lista-productos-carrito/' . $orden_compra_id);
        $data['detalle'] = procesarResponseApiJsonToArray($listado);

        $this->blade->render('almacenes/dev_proveedor/devolucion_pieza', $data);
    }

    public function venta_mostrador($id = null)
    {
        $data = $this->getDataVenta(base64_decode($id));
        $data['tipo_venta_id'] = 1;
        $data['titulo'] = 'Venta mostrador refacciones';
        if (isset($data['venta']->folio)) {
            $api_cuentas_por_cobrar = $this->curl->curlGet('api/cuentas-por-cobrar/buscar-por-folio-id/' . $data['venta']->folio->id);
            $data['cxc'] = procesarResponseApiJsonToArray($api_cuentas_por_cobrar);
        }
        $tipo_forma_pago = $this->curl->curlGet('api/catalogo-tipo-forma-pago');
        $plazo_credito = $this->curl->curlGet('api/catalogo-plazo-credito');
        $data['plazo_credito'] = procesarResponseApiJsonToArray($plazo_credito);
        $data['tipo_forma_pago'] = procesarResponseApiJsonToArray($tipo_forma_pago);
        $this->blade->render('ventas_mostrador/flujo_venta_mostrador', $data);
    }

    public function cotizador($id = null)
    {

        $data = $this->getDataVenta(base64_decode($id));
        $data['tipo_venta_id'] = 3;
        $data['titulo'] = 'Cotizador refacciones';
        $this->blade->render('ventas_mostrador/flujo_venta_mostrador', $data);
    }

    public function generarComprobanteCotizacion($id)
    {
        if (!$id) {
            return [];
        }
        $venta_id = base64_decode($id);
        $data = $this->getDataVenta($venta_id);

        $api_cliente = $this->curl->curlGet('api/clientes/' . $data['venta']->cliente_id);
        $api_productos_venta = $this->curl->curlGet('api/refacciones/venta/' . $venta_id);
        $data['cliente'] = current(procesarResponseApiJsonToArray($api_cliente));
        $data['productos'] = procesarResponseApiJsonToArray($api_productos_venta)->aaData ?? [];
        $view = $this->load->view('polizas/poliza_ventas/comprobante_cotizacion', $data, true);
        $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
    }

    private function getDataVenta($venta_id)
    {
        $data['venta'] = [];
        $data['cotizador'] = [];
        if ($venta_id) {
            $api_venta = $this->curl->curlGet('api/ventas/busqueda-venta/' . $venta_id);
            $data['venta'] = procesarResponseApiJsonToArray($api_venta);
            $api_cotizador = $this->curl->curlGet('api/cotizador-refacciones/buscar-por-venta?venta_id=' . $venta_id);
            $data['cotizador'] = procesarResponseApiJsonToArray($api_cotizador);

            // utils::pre($data['cotizador']->marca->nombre);
        }

        $cat_precios = $this->curl->curlGet('api/precios?id_vista=1');
        $cat_tipo_clientes = $this->curl->curlGet('api/catalogo-tipo-cliente');
        $cat_precios = $this->curl->curlGet('api/precios');
        $clave_cliente = $this->curl->curlGet('api/catalogo-clave-cliente');

        $api_cliente = $data['venta']->cliente_id ? $this->curl->curlGet('api/clientes/' . $data['venta']->cliente_id) : [];

        $data['cliente'] = current(procesarResponseApiJsonToArray($api_cliente));
        $data['cat_clave'] = procesarResponseApiJsonToArray($clave_cliente);
        $data['cat_tipo_clientes'] = procesarResponseApiJsonToArray($cat_tipo_clientes);
        $data['vendedor_id'] = $this->session->userdata('id');
        $data['cat_precios'] = procesarResponseApiJsonToArray($cat_precios);

        return $data;
    }
}

/* End of file Salidas.php */
