@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Devolución proveedor </li>
    </ol>
    <div class="row mb-3">
        <div class="col-12 text-right">
            <a class="btn btn-primary" href="<?php echo base_url('refacciones/salidas/listadoDevoluciones') ?>">Listado
                devoluciones</a>
        </div>
    </div>
    <hr />

    <div class="row">
        <div class="col-5 col-md-5">
            <h3>Devolucion orden compra</h3>
            <table class="table mt-5" style="background-color:#fff !important; border:1px solid #eee !important">
                <tr>
                    <td>No. orden compra</td>
                    <td><?php echo isset($orden_compra->id) ? $orden_compra->id : ''; ?></td>
                </tr>
                <tr>
                    <td>Folio</td>
                    <td><?php echo isset($orden_compra->folio) ? $orden_compra->folio : ''; ?></td>
                </tr>
                <tr>
                    <td>Proveedor</td>
                    <td><?php echo isset($orden_compra->proveedor_nombre) ? $orden_compra->proveedor_nombre : ''; ?>
                    </td>
                </tr>
                <tr>
                    <td>Tipo pago</td>
                    <td><?php echo isset($orden_compra->tipoPago) ? $orden_compra->tipoPago : ''; ?></td>
                </tr>
                <tr>
                    <td>Estatus</td>
                    <td><?php echo isset($orden_compra->estatusCompra) ? $orden_compra->estatusCompra : ''; ?></td>
                </tr>
                <tr>
                    <td>Observaciones</td>
                    <td><?php echo isset($orden_compra->observaciones) ? $orden_compra->observaciones : ''; ?></td>
                </tr>
            </table>
        </div>
        <div class="col-md-7">
            <h3>Formulario devolución</h3>
            <form id="frm-devolucion" class="mt-3">
                <div class="row">
                    <div class="col-md-6">
                        <?php echo renderInputText("text", "orden_entrada", "No. orden compra", isset($orden_compra->id) ? $orden_compra->id : ''); ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo renderInputText("text", "factura", "No de folio factura", isset($orden_compra->folioFactura) ? $orden_compra->folioFactura : ''); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php echo renderInputText("date", "fecha", "Fecha", isset($devolucion->fecha) ? $devolucion->fecha : ''); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php echo renderInputTextArea("observaciones", "Observaciones", isset($devolucion->observaciones) ? $devolucion->observaciones : ''); ?>
                    </div>
                </div>
                <input type="hidden" name="orden_compra_id" id="orden_compra_id"
                    value="<?php echo isset($orden_compra->id) ? $orden_compra->id : ''; ?>" />
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h3>Productos</h3>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered" id="tabla_ventas" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Num. Orden</th>
                        <th>Descripcion</th>
                        <th>Cantidad</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($detalle->productos as $item)
                    <tr>
                        <td>{{ $item->productos->id }}</td>
                        <td>{{ $item->productos->no_identificacion }}</td>
                        <td>{{ $item->productos->descripcion }}</td>
                        <td>{{ $item->productos_orden_compra->cantidad }}</td>
                        <td>{{ $item->productos_orden_compra->total }}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Num. Orden</th>
                        <th>Descripcion</th>
                        <th>Cantidad</th>
                        <th>Total</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="row mt-3 mb-3">
        <?php
        if (isset($orden_compra->id_estatus_compra) && $orden_compra->id_estatus_compra == 2) { ?>
        <div class="col-12 text-right">
            <button id="btn-devolucion" class="btn btn-primary col-4" type="button">
                <i class="fas fa-shopping-cart"></i> Devolucion
            </button>
            <?php } ?>
            <?php if (isset($orden_compra->id_estatus_compra) && $orden_compra->id_estatus_compra == 5) { ?>
            <div class="col-12 text-right">
                <a target="_black"
                    href="{{ base_url('refacciones/polizas/generarPolizaDevolucionProveedorPDF?folio_id='.$devolucion->folio_id) }}"
                    id="btn-poliza" class="btn btn-primary col-4" type="button">
                    <i class="fas fa-print"></i> Poliza
                </a>
            </div>
            <?php } ?>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
$(document).ready(function() {
    let orden_compra_id = document.getElementById("orden_compra_id").value;

    $("#btn-devolucion").on('click', function() {
        $.isLoading({
            text: "Realizando proceso de devolución ...."
        });
        ajax.post('api/devoluciones', {
            fecha: document.getElementById("fecha").value,
            orden_compra_id: orden_compra_id,
            factura: document.getElementById("factura").value,
            observaciones: document.getElementById("observaciones").value,
        }, function(response, header) {
            if (header.status == 200 || header.status == 201) {
                ajax.put('api/cuentas-por-pagar/cancelar-cuenta/' + orden_compra_id, {},
                    function(response2, header2) {
                        $.isLoading("hide");
                        if (header2.status == 200) {
                            utils.displayWarningDialog(header.message, 'success', function(
                                result) {
                                $.isLoading({
                                    text: "Retornando a listado de devoluicones ...."
                                });
                                window.location.href = base_url +
                                    'refacciones/salidas/listadoDevoluciones';
                            });
                        }
                    });
            }
            if (header.status == 500) {
                $.isLoading("hide");
                let titulo = "Error en devolución!"
                utils.displayWarningDialog(titulo, 'warning', function(result) {});
            }
        });
    })
});

$("#menu_refacciones").addClass("show");
$("#refacciones_salidas").addClass("show");
$("#refacciones_salidas").addClass("active");
$("#refacciones_salidas_sub").addClass("show");
$("#refacciones_salidas_sub").addClass("active");
$("#dev_a_prov").addClass("active");
$("#M02").addClass("active");
</script>
@endsection