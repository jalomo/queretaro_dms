@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
	<h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item active">Devolución proveedor </li>
	</ol>
	<hr />
	<div class="row">
		<div class="col-12 col-md-12">
			<h3>Detalle de la orden compra</h3>
			
			<table class="table mt-5" style="background-color:#fff !important; border:1px solid #eee !important">
				<tr>
					<td>No. orden compra</td>
                    <td><?php echo isset($orden_compra->id) ? $orden_compra->id : $data['orden_compra_id']; ?></td>
                    <input type="hidden" id="orden_compra_id" value="<?php echo isset($data['orden_compra_id']) ? $data['orden_compra_id'] : ''; ?>" />
				</tr>
				<tr>
					<td>Folio</td>
					<td><?php echo isset($orden_compra->folio) ? $orden_compra->folio : ''; ?></td>
				</tr>
				<tr>
					<td>Proveedor</td>
					<td><?php echo isset($orden_compra->proveedor_nombre) ? $orden_compra->proveedor_nombre : ''; ?>
					</td>
				</tr>
				<tr>
					<td>Tipo pago</td>
					<td><?php echo isset($orden_compra->tipoPago) ? $orden_compra->tipoPago : ''; ?></td>
				</tr>
				<tr>
					<td>Estatus</td>
					<td><?php echo isset($orden_compra->estatusCompra) ? $orden_compra->estatusCompra : ''; ?></td>
				</tr>
				<tr>
					<td>Observaciones</td>
					<td><?php echo isset($orden_compra->observaciones) ? $orden_compra->observaciones : ''; ?></td>
				</tr>
			</table>
		</div>
        <hr/>
		<div class="col-md-12">
			<h3>Listado de productos de orden compra</h3>
			<table class="table table-bordered" id="tabla_devolucion_pieza" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>#</th>
						<th>Num. Orden</th>
						<th>Descripcion</th>
						<th>Cantidad</th>
						<th>Total</th>
						<th>Devolver</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($detalle->productos as $item)
					<tr>
						<td>{{ $item->productos->id }}</td>
						<td>{{ $item->productos->no_identificacion }}</td>
						<td>{{ $item->productos->descripcion }}</td>
						<td>{{ $item->productos_orden_compra->cantidad }}</td>
						<td>{{ $item->productos_orden_compra->total }}</td>
						<td>
							<button onclick="openModal(this)"
								data-orden_compra_id="{{ $item->productos_orden_compra->orden_compra_id }}"
								data-producto_compra_id="{{ $item->productos_orden_compra->id }}"
								data-producto_id="{{ $item->productos_orden_compra->producto_id }}"
								data-no_identificacion="{{ $item->productos->no_identificacion}}"
								data-descripcion="{{ $item->productos->descripcion}}"
								data-cantidad="{{ $item->productos_orden_compra->cantidad}}"
								data-total="{{ $item->productos_orden_compra->total}}" class="btn btn-danger"> <i
									class="fas fa-times"></i> Devolver pieza </button>
						</td>
					</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<th>#</th>
						<th>Num. Orden</th>
						<th>Descripcion</th>
						<th>Cantidad</th>
						<th>Total</th>
						<th>Devolver</th>
					</tr>
				</tfoot>
			</table>
		</div>
        <hr/>
		<div class="col-md-12 mt-3">
			<h3>Productos devueltos de la orden compra</h3>
			<table class="table table-bordered" id="tabla_piezas_devueltas" width="100%" cellspacing="0"></table>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-md-12 mt-4 text-right">
            <a class="btn btn-primary" href="<?php echo base_url('refacciones/salidas/devolucion') ?>"><i class="fa fa-arrow-left"></i>&nbsp;Regresar</a>
        </div>
    </div>
	@endsection


	@section('modal')
	<div class="modal fade" id="modal-devolver-pieza" data-toggle="modal" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="title_modal">Devolver pieza</h5>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<input type="hidden" id="producto_id_modal">
							<div class="form-group">
								<label for="select">Número de orden</label>
								<input class="form-control" id="orden_compra_id_modal" readonly="readonly" />
							</div>
							<div class="form-group">
								<label for="select">No identificación</label>
								<input class="form-control" id="no_identificacion_modal" readonly="readonly" />
							</div>
							<div class="form-group">
								<label for="select">Cantidad</label>
								<input class="form-control" id="cantidad_modal" readonly="readonly" />
							</div>
							<div class="form-group">
								<label for="select">Total</label>
								<input class="form-control" id="total_modal" readonly="readonly" />
							</div>
							<div class="form-group">
								<label for="select">Descripción</label>
								<input class="form-control" id="descripcion_modal" readonly="readonly" />
							</div>
							<div class="form-group">
								<label for="select">Nota de credito</label>
								<input class="form-control" id="nota_credito_modal" />
							</div>
							<div class="form-group">
								<label for="select">Observaciones</label>
								<input class="form-control" id="observaciones_modal" />
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" id="producto_compra_id_modal" />
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<button onclick="devolver_producto()" id="btn-modal-agregar" type="button"
						class="btn btn-primary"><i class="fas fa-file"></i> Devolver</button>
				</div>
			</div>
		</div>
	</div>
	@endsection

	@section('scripts')
	<script>
		$(document).ready(function () {

            $('#tabla_devolucion_pieza').DataTable({
                language: {
                    url: PATH_LANGUAGE
                } 
            })
            
            $('#tabla_piezas_devueltas').DataTable({
                language: {
                    url: PATH_LANGUAGE
                },
                "ajax": {
                    url: PATH_API + "api/devolucion-pieza/por-orden-compra/" + $("#orden_compra_id").val(),
                    type: 'GET',
                    dataSrc: "",
                },
                columns: [
                    {
                        title: "No. de pieza",
                        data: 'no_identificacion',
                    },
                    {
                        title: "Descripción",
                        data: 'descripcion',
                    },
                    {
                        title: "Nota credito",
                        data: 'nota_credito',
                    },
                    {
                        title: "Observaciones",
                        data: 'observaciones',
                    },
                    {
                        title: "Cantidad",
                        data: 'cantidad',
                    },
                    {
                        title: "Total",
                        data: 'total',
                    },
                    {
                        title: "Fecha de devolución",
                        render: function(data, type, row) {
                            return obtenerFechaMostrar(row.created_at)
                        }
                    },
                ]
            });
        });

        function obtenerFechaMostrar(fecha) {
                const dia = 2,
                mes = 1,
                anio = 0;
            fecha = fecha.split('T');
            fecha = fecha[0].split('-');
            return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
        }

		function openModal(_this) {
			$("#producto_id_modal").val($(_this).data('producto_id'));
			$("#orden_compra_id_modal").val($(_this).data('orden_compra_id'));
			$("#no_identificacion_modal").val($(_this).data('no_identificacion'));
			$("#descripcion_modal").val($(_this).data('descripcion'));
			$("#cantidad_modal").val($(_this).data('cantidad'));
			$("#total_modal").val($(_this).data('total'));
			$("#producto_compra_id_modal").val($(_this).data('producto_compra_id'));
			$("#modal-devolver-pieza").modal("show");
		}

		function devolver_producto(id) {
			toastr.info("Proceso devolución de pieza en proceso");
			$.isLoading({
				text: "Proceso devolución de pieza en proceso...."
			});

			let producto_compra_id = $("#producto_compra_id_modal").val();
			let producto_id = $("#producto_id_modal").val();
			let orden_compra_id = $("#orden_compra_id_modal").val();
			let nota_credito = $("#nota_credito_modal").val();
			let descripcion = $("#descripcion_modal").val();
			let cantidad = $("#cantidad_modal").val();
			let total = $("#total_modal").val();
			let data = {
				producto_id,
				orden_compra_id,
				nota_credito,
				cantidad,
				total
			}
			ajax.post(`api/devolucion-pieza`, data, function (response, headers) {
				if (headers.status == 201 || headers.status == 200) {
					$("#modal-devolver-pieza").modal('hide');
					ajax.delete(`api/productos-orden-compra/${producto_compra_id}`, null, function (response,headers) {
						if (headers.status != 204) {
							$.isLoading("hide");
							return utils.displayWarningDialog(headers.message)
						} else {
							//TO-DO DEPRECADA ESTA FUNCION
							/*ajax.get('api/desglose-producto/actualizaStockByProducto?producto_id=' + producto_id, {},
								function (response, headers) {
									if (headers.status == 200) {
										$.isLoading("hide");
										toastr.info("Inventario actualizado para el producto " +
											descripcion);
										utils.displayWarningDialog("Pieza devuelta correctamente",
											'success',
											function (result) {
												window.location.reload();
											});
									}
								})*/
						}
					})

				} else {
					$.isLoading("hide");
				}
			})
		}

	</script>
	@endsection
