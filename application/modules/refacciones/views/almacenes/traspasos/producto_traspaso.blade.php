@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Traspasar producto</li>
    </ol>
    <div class="row mb-4 mt-3">
        <div class="col-md-9"></div>
        <div class="col-md-3 text-right ">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form id="frm-traspaso">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="almacen_actual">Almacen Actual:</label>
                            <select disabled name="almacen_origen_id" class="form-control" id="almacen_origen_id">
                                <option value=""> Seleccionar almacen</option>
                                @foreach ($almacenes as $almacen)
                                @if (isset($traspaso_detalle->almacen_origen_id) && $traspaso_detalle->almacen_origen_id == $almacen->id)
                                <option selected value="{{ $almacen->id}}"> {{ $almacen->nombre}}</option>
                                @else
                                <option value="{{ $almacen->id}}"> {{ $almacen->nombre}}</option>
                                @endif
                                @endforeach
                            </select>
                            <div id="almacen_origen_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="producto_id_sat">Almacen Destino:</label>
                            <select disabled name="almacen_destino_id" class="form-control" id="almacen_destino_id">
                                <option value=""> Seleccionar almacen</option>
                                @foreach ($almacenes as $almacen)
                                @if (isset($traspaso_detalle->almacen_destino_id) && $traspaso_detalle->almacen_destino_id == $almacen->id)
                                <option selected value="{{ $almacen->id}}"> {{ $almacen->nombre}}</option>
                                @else
                                <option value="{{ $almacen->id}}"> {{ $almacen->nombre}}</option>
                                @endif
                                @endforeach
                            </select>
                            <input type="hidden" name="traspaso_id" id="traspaso_id" value="{{ isset($traspaso_detalle->id) ? $traspaso_detalle->id : '' }}">
                            <input type="hidden" name="estatus_id" id="estatus_id" value="{{ isset($traspaso_detalle->estatus) ? $traspaso_detalle->estatus : '' }}">
                            <input type="hidden" id="user_id" name="user_id" value="{{ !empty($this->session->userdata('id')) ? $this->session->userdata('id') : ''}}">
                            <div id="almacen_destino_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Estatus traspaso</label>
                            <input type="text" disabled class="form-control" value="{{ isset($traspaso_detalle->estatus) && $traspaso_detalle->estatus == 0 ? "EN PROCESO" : 'FINALIZADO'}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Folio</label>
                            <input type="text" disabled class="form-control" value="{{ isset($traspaso_detalle->folios) ? $traspaso_detalle->folios->folio : 'NO-GENERADO'}}">
                            <input type="hidden" name="folio_id" id="folio_id" disabled class="form-control" value="{{ isset($traspaso_detalle->folios) ? $traspaso_detalle->folios->id : 'NO-GENERADO'}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        @if ($traspaso_detalle->estatus == 0)
                        <button id="btn-traspaso" class="btn btn-primary col-md-4" type="button"> Confirmar </button>
                        @else
                        <a id="btn-poliza" target="blank" class="btn btn-primary mb-3" href="{{ isset($traspaso_detalle->id) ? base_url('refacciones/almacenes/generarPolizaTraspaso/'.$traspaso_detalle->id) : '#'  }}">
                            <i class="fas fa-file-pdf"></i> Imprimir poliza
                        </a>
                        <br>
                        <a target="_blank" class="btn btn-primary mb-3" href="{{ isset($traspaso_detalle->id) ? base_url('refacciones/almacenes/generarReporteTraspaso/'.$traspaso_detalle->id) : '#'  }}">
                            <i class="fas fa-file-pdf"></i> Reporte de traspaso
                        </a>
                        <br>
                        <button id="btn-factura" class="btn btn-primary mb-3" type="button">
                            <i class="fas fa-receipt"></i> Generar Factura
                        </button>

                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
    <br>
    <h3>Productos seleccionados</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_carrito_traspasos" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>No identificación</th>
                            <th>Descripcion</th>
                            <th>{{ $traspaso_detalle->estatus == 0 ? "Cantidad por<br/>traspasar" :"Cantidad <br/>traspasada" }}</th>
                            <th> {{ isset($traspaso_detalle->almacen_origen->codigo_almacen) ? $traspaso_detalle->almacen_origen->codigo_almacen : "almacen envio"}}</th>
                            <th>{{ isset($traspaso_detalle->almacen_destino->codigo_almacen) ? $traspaso_detalle->almacen_destino->codigo_almacen : "almacen recibe"}}</th>
                            <th>Valor unitario</th>
                            <th>Total</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>No identificación</th>
                            <th>Descripcion</th>
                            <th>{{ $traspaso_detalle->estatus == 0 ? "Cantidad por<br/>traspasar" :"Cantidad <br/>traspasada" }}</th>
                            <th>{{ isset($traspaso_detalle->almacen_origen->codigo_almacen) ? $traspaso_detalle->almacen_origen->codigo_almacen : "almacen envio"}}</th>
                            <th>{{ isset($traspaso_detalle->almacen_destino->codigo_almacen) ? $traspaso_detalle->almacen_destino->codigo_almacen : "almacen recibe"}}</th>
                            <th>Valor unitario</th>
                            <th>Total</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <hr>
    <h3>Listado de Productos</h3>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_productos" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>No identificación</th>
                            <th>Descripcion</th>
                            <th>Existencia almacenes</th>
                            <th>Almacen {{ $traspaso_detalle->almacen_origen->codigo_almacen}}</th>
                            <th>Almacen {{ $traspaso_detalle->almacen_destino->codigo_almacen}}</th>
                            <th>Valor unitario</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (isset($productos))
                        @foreach ($productos as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->no_identificacion }}</td>
                            <td>{{ $item->descripcion }}</td>
                            <td>{{ $item->cantidad_actual }}</td>
                            <td>{{ $item->cantidad_almacen_primario }}</td>
                            <td>{{ $item->cantidad_almacen_secundario}}</td>
                            <td>
                                $ {{ $item->valor_unitario }}
                            </td>
                            <td>
                                @if ($traspaso_detalle->estatus == 0 && $item->cantidad_actual > 0 )
                                <button data-target="#modal-producto-detalle" data-toggle="modal" data-precio="{{ $item->valor_unitario}}" data-no_identificacion="{{ $item->no_identificacion}}" data-product_name="{{ $item->descripcion}}" data-id="{{ $item->id}}" data-cantidad_almacen_primario="{{ $item->cantidad_almacen_primario}}" data-cantidad_almacen_secundario="{{ $item->cantidad_almacen_secundario}}" class="btn btn-success item-producto">
                                    <i class="fas fa-shopping-cart"></i>
                                </button>
                                @elseif($item->cantidad_actual == 0)
                                - -
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>No identificación</th>
                            <th>Descripcion</th>
                            <th>Existencia almacenes</th>
                            <th>Almacen {{ $traspaso_detalle->almacen_origen->codigo_almacen}}</th>
                            <th>Almacen {{ $traspaso_detalle->almacen_destino->codigo_almacen}}</th>
                            <th>Valor unitario</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $('#tbl_productos').DataTable({
        language: {
			url: PATH_LANGUAGE
		}
    })
    var tabla_carrito_traspaso = $('#tbl_carrito_traspasos').DataTable({
        language: {
			url: PATH_LANGUAGE
		},
        "ajax": {
            url: base_url + "refacciones/almacenes/ajax_get_lista_traspaso",
            type: 'POST',
            data: {
                traspaso_id: function() {
                    return $('#traspaso_id').val()
                }
            }
        },
        columns: [{
                'data': 'id'
            },
            {
                'data': function(data) {
                    return data.no_identificacion
                }
            },
            {
                'data': function(data) {
                    return data.descripcion
                }
            },
            {
                'data': function(data) {
                    return data.cantidad
                }
            },
            {
                'data': function(data) {
                    return data.cantidad_almacen_primario
                }
            },
            {
                'data': function(data) {
                    return data.cantidad_almacen_secundario
                }
            },
            {
                'data': function(data) {
                    return data.valor_unitario
                }
            },
            {
                'data': function(data) {
                    return data.total_valor
                }
            },
            {
                'data': function(data) {
                    if ($("#estatus_id").val() == 0) {
                        return "<button type='button' class='btn-borrar btn btn-primary' data-id=" + data.id + "><i class='fas fa-trash'></i></button>";
                    } else {
                        return '--'
                    }
                }
            }
        ]
    });

    $("#btn-traspaso").on('click', function() {
        let table_carrito_length = $("#tbl_carrito_traspasos").dataTable().fnSettings().aoData.length
        if (table_carrito_length == 0) {
            utils.displayWarningDialog("Seleccionar elementos para traspaso");
            return false;
        }

        ajax.post(`api/producto-traspaso-almacen/confirmar-traspaso`, {
            traspaso_id: $('#traspaso_id').val(),
            folio_id: $('#folio_id').val(),
            user_id: $('#user_id').val()
        }, function(response, headers) {
            if (headers.status == 201 || headers.status == 200) {
                $.isLoading({ text: "Realizando traspaso ...." });
                var productosTraspasar = $('#tbl_carrito_traspasos').DataTable();
                let titulo = "Traspasos realizado correctamente!"
				toastr.info("Actualizando inventario espere un momento....");
                let total =  productosTraspasar.data().count();
                let count = 0;
                $(productosTraspasar.data()).each(function( index, product ) {
                    //TO-DO DEPRECADA ESTA FUNCION
                    /*ajax.get('api/desglose-producto/actualizaStockByProducto?producto_id=' + product.producto_id,{}, function (response, headers) {
						if (headers.status == 200) {
							count++;
							toastr.info("Inventario actualizado para el producto " + product.descripcion);
                            if(total == count){
                                $.isLoading("hide");
                                utils.displayWarningDialog(titulo, 'success', function(result) {
                                    window.location.reload();
                                });
                            }
                        }
					})*/
                });
            }
        })
    });

    $("#tbl_carrito_traspasos").on('click', '.btn-borrar', function() {
        let id_traspaso = $(this).data('id');
        borrarElementoTraspaso(id_traspaso);
    });

    function borrarElementoTraspaso(id) {
        ajax.delete(`api/producto-traspaso-almacen/${id}`, {}, function(response, headers) {
            if (headers.status == 201 || headers.status == 204) {
                tabla_carrito_traspaso.ajax.reload();
            }
        })
    }

    $("#btn-agrega-carrito").on('click', function() {
        $.isLoading({ text: "Añandiendo producto al traspaso ...." });
        let titulo = "Producto añadido correctamente";
        let data = {
            producto_id: document.getElementById("producto_id").value,
            valor_unitario: $("#valor_unitario").val(),
            traspaso_id: document.getElementById("traspaso_id").value,
            almacen_destino_id: document.getElementById("almacen_destino_id").value,
            almacen_origen_id: document.getElementById("almacen_origen_id").value,
            cantidad: $("#cantidad").val(),
            activo: 1
        };

        if ($("#cantidad").val() == 0) {
            utils.displayWarningDialog("Indicar la cantidad");
            return false;
        }
        ajax.post(`api/producto-traspaso-almacen`, data, function(response, headers) {
            if (headers.status == 201 || headers.status == 200) {
                $("#modal-producto-detalle").modal('hide');
                $.isLoading("hide");
                utils.displayWarningDialog(titulo, 'success', function(result) {
                    window.location.reload();
                });
            }
        })
    });

    $("#cantidad").on('blur', function() {
        let cantidad = $("#cantidad").val();
        let almacen_id = $("#almacen_origen_id").val();
        let producto_id = $("#producto_id").val();
        let cantidad_almacen_primario = $("#cantidad_almacen_primario").val();
        let cantidad_almacen_secundario = $("#cantidad_almacen_secundario").val();
        validar_cantidad_productos(cantidad, almacen_id, producto_id, cantidad_almacen_primario, cantidad_almacen_secundario)

    });

    function validar_cantidad_productos(cantidad, almacen_id, producto_id, cantidad_almacen_primario, cantidad_almacen_secundario) {

        if (almacen_id == 1){
            cantidad_almacen = cantidad_almacen_primario;
        } else {
            cantidad_almacen = cantidad_almacen_secundario;
        }
        if (parseInt(cantidad_almacen) >= parseInt(cantidad)) {
            $(".validation_error").html('');
            $("#btn-agrega-carrito").attr('disabled', false);
        } else {
            toastr.error("No existe cantidad sufiente para el traspaso " + cantidad_almacen);
            $("#btn-agrega-carrito").attr('disabled', true);
            $("#cantidad").val(0);
            $("#cantidad").focus();
            return false;
        }
    }


    $("#tbl_productos").on('click', '.item-producto', function() {
        let producto_id = $(this).data('id');
        let valor_unitario = $(this).data('precio');
        let product_name = $(this).data('product_name');
        let no_identificacion = $(this).data('no_identificacion');
        let cantidad_almacen_primario = $(this).data('cantidad_almacen_primario');
        let cantidad_almacen_secundario = $(this).data('cantidad_almacen_secundario');

        $("#title_modal").text("Cantidad de producto");
        $("#producto").val(product_name);
        $("#no_identificacion").val(no_identificacion);
        $("#valor_unitario").val(valor_unitario);
        $("#producto_id").val(producto_id);
        $("#cantidad_almacen_primario").val(cantidad_almacen_primario);
        $("#cantidad_almacen_secundario").val(cantidad_almacen_secundario);
    });
</script>
@endsection


@section('modal')
<div class="modal fade" id="modal-producto-detalle" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="modalCantidadeProductos" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo renderInputText("text", "producto", "Producto", 1, true); ?>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Cantidad</label>
                            <input type="number" name="cantidad" value="0" id="cantidad" class="form-control">
                            <input type="hidden" name="producto_id" id="producto_id">
                            <input type="hidden" name="valor_unitario" id="valor_unitario">
                            <input type="hidden" name="no_identificacion" id="no_identificacion">
                            <input type="hidden" name="cantidad_almacen_primario" id="cantidad_almacen_primario">
                            <input type="hidden" name="cantidad_almacen_secundario" id="cantidad_almacen_secundario">
                            <div id="cantidad_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Cerrar
                </button>
                <button id="btn-agrega-carrito" type="button" class="btn btn-primary">
                    <i class="fas fa-shopping-cart"></i> Agregar
                </button>
            </div>
        </div>
    </div>
</div>
@endsection