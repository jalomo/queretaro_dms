@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
            <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
            <li class="breadcrumb-item active">Otras salidas Serv. Exce</li>
        </ol>

        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="numero_orden">No. de Orden</label>
                    <input id="numero_orden" placeholder="" name="numero_orden" type="text" class="form-control" value="" onchange="buscarOrdenes()">
                    <div id="numero_orden_error" class="invalid-feedback"></div>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="fecha_inicio">Fecha Inicio</label>
                    <input id="fecha_inicio" placeholder="" name="fecha_inicio" type="date" class="form-control" value="" onchange="buscarOrdenes()">
                    <div id="fecha_inicio_error" class="invalid-feedback"></div>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="fecha_final">Fecha Final</label>
                    <input id="fecha_final" placeholder="" name="fecha_final" type="date" class="form-control" value="" onchange="buscarOrdenes()">
                    <div id="fecha_final_error" class="invalid-feedback"></div>
                </div>
            </div>
            <div class="d-flex align-items-end">
                <div class="mb-3">
                    <button onclick="buscarOrdenes()" class="btn btn-primary"> 
                        <i class="fa fa-search"></i> Buscar
                    </button>
                </div>
            </div>
        </div>
        <hr>
        <h3>Ordenes Abiertas</h3>
        <div class="row">
            <div class="col-md-12 table-responsive">
                <table class=" table table-bordered" id="tbl_ordenes_abiertas">
                    <thead>
                        <tr>
                            <th>No. Orden</th>
                            <th>Cliente</th>
                            <th>Asesor</th>
                            <th>Técnico</th>
                            <th>Serie</th>
                            <th>Placas</th>
                            <th>Estatus</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No. Orden</th>
                            <th>Cliente</th>
                            <th>Asesor</th>
                            <th>Técnico</th>
                            <th>Serie</th>
                            <th>Placas</th>
                            <th>Estatus</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        //continuar flujo de venta
        $("#tbl_ordenes_abiertas").on('click', '.btn-confirmar-venta-mpm', function() {

            toastr.warning('<strong>Procesando ...</strong>');

            let data = $(this).data()
            let patt = new RegExp(/^[0-9\s]+$/g);
            let numero_cliente = patt.test(data.cliente) ? data.cliente : data.cliente.substr(2);

            $.ajax({
                url: `${PATH_API}api/detalles/venta/${data.id}`,
                method: 'POST',
                dataType: 'json',
                data: {
                    numero_orden: data.id,
                    cliente_id: numero_cliente,
                    venta_total: 0,
                    tipo_venta_id: 2, //ventanilla taller
                    almacen_id: 1,
                    tipo_precio_id: 1,
                    precio_id: 1,
                    tipo_orden: data.tipoOrden,
                },
            })
            .done(response => {
                
                utils.displayWarningDialog('Procesando orden ...', "success", function(data_alert) {
                    if (response.folio_id) {
                        window.location.href = PATH +
                            "/refacciones/salidas/detalleVentaServiceExcellent/" + response.folio_id
                    } else {
                        toastr.error('<strong>Algo salio mal ...</strong>');
                    }
                })
            })
            .fail(error => {
                try{
                    let messages = JSON.parse(error.getResponseHeader('X-Message'))

                    utils.displayWarningDialog(Object.values(messages)[0], "error", function(){})
                } catch(e) {
                    utils.displayWarningDialog('Ocurrio un error al crear la venta', "error", function(){})
                }
            })
        });

        var tabla_ordenes = $('#tbl_ordenes_abiertas').DataTable({
            ajax: {
                url: PATH_API + "api/ventas/listado-ordenes",
                type: 'GET',
                data: {
                    numero_orden: function() { return $('#numero_orden').val() },
                    fecha_inicio: function() { return $('#fecha_inicio').val() },
                    fecha_final: function() { return $('#fecha_final').val() },
                }
            },
            processing: true,
            serverSide: true,
            bFilter: false,
            columns: [{
                    'data': function(data) {
                        let text = ''

                        if(! data.numero_cliente)
                            text += `<i class="fa fa-user-slash mr-2 text-danger" title="No trae número de cliente"></i>`

                        return text += `${data.id_cita}`
                    }
                },
                {
                    'data': function(data) {
                        return `${data.cliente}<br>${data.fecha_recepcion} ${data.hora_recepcion}`
                    }
                },
                {
                    'data': function(data) {
                        return data.asesor
                    }
                },
                {
                    'data': function(data) {
                        return data.tecnico
                    }
                },
                {
                    'data': function(data) {
                        return data.serie
                    }
                },
                {
                    'data': function(data) {
                        return data.placas
                    }
                },
                {
                    'data': function(data) {
                        return `${data.estatus_orden}<br>${data.servicio_unidad}`
                    }
                },
                {
                    'data': function(data) {
                        let btn = ''

                        if(data.requisicion){
                            btn += `<a target="_blank" href="${data.requisicion}" class="btn btn-primary" title="Requisición">
                                <i class="fa fa-file-pdf"></i>
                            </a>`
                        }

                        btn += `<button type="button" 
                            data-id="${data.id_cita}" data-cliente="${data.numero_cliente}" data-tipo-orden="${data.tipo_orden}"
                            class="btn btn-primary btn-confirmar-venta-mpm">
                                <i class="fas fa-tasks"></i>
                            </button>`;

                        return btn
                    }
                }
            ],"createdRow": function(row, data, dataIndex) {
                if (data['orden_venta']) {
                    // $(row).find('td').css('background-color', '#8cdd8c');
                    $(row).find('td:eq(0)').css('background-color', '#8cdd8c');
                }
            }
        });

        const buscarOrdenes = () => {
            tabla_ordenes.ajax.reload()
        }

    </script>
@endsection
