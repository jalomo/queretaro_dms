@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
        <li class="breadcrumb-item active">{{ $bread_active}}</li>
    </ol>
    <div class="d-flex mb-4">
        <div class="form-group mb-0 col-4 pl-0">
            <input id="no_identificacion" type="text" class="form-control" value="" placeholder="Buscar Reemplazo">
        </div>
        <button class="btn btn-primary btn-lg" onclick="buscar()">
            <i class="fa fa-search"></i> Buscar
        </button>
        <button id="reemplazar" class="btn btn-primary btn-lg ml-auto">
            <i class="fa fa-plus"></i> Reemplazo
        </button>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_productos" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>No. Identificación</th>
                            <th>No. Identificación Reemplazo</th>
                            <th>Precio</th>
                            <th>Precio Reemplazo</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>                           
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-agregar-reemplazo" data-toggle="modal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="title_modal">
                        <i class="fa fa-sync"></i>
                        Reemplazar
                    </h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col">
                            <label for="">Producto</label>
                            <input id="producto" type="text" class="form-control">
                        </div>                    
                    </div>
                    <div class="row">
                        <div class="form-group col">
                            <label for="">Reemplazo</label>
                            <input id="reemplazo" type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button onClick="guardarReemplazo()" type="button" class="btn btn-primary">
                        <i class="fas fa-lock-open"></i> Guardar
                    </button>
    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#tbl_productos').DataTable({
            processing: true,
            serverSide: true,
            bFilter: false,
            bSort: false,
            ajax: `${PATH_API}api/reemplazos`,
            columns:[
                { data: 'id' },
                { data: 'producto.no_identificacion' },
                { data: 'reemplazo.no_identificacion' },
                { data: 'producto.valor_unitario' },
                { data: 'reemplazo.valor_unitario' },
                { data: function(data){
                    html = ''

                    html += `<button type='button' class='btn-borrar btn btn-danger' data-id="${data.id}">
                            <i class="fa fa-trash-alt"></i>
                        </button>`

                    return html
                }}
            ]
        })
    });

    $("#tbl_productos").on("click", ".btn-borrar", function() {
        var id = $(this).data('id')
        borrar(id)
    });

    function borrar(id) {
        utils.displayWarningDialog("¿Desea borrar el registro?", "warning", function(data) {
            if (data.value) {
                ajax.delete(`api/reemplazo/${id}/delete`, null, function(response, headers) {
                    if (headers.status != 200) {
                        return utils.displayWarningDialog(response.message)
                    }
                    location.reload(true)
                })

            }
        }, true)
    }

    const buscar = () =>{
        let params = {
            no_identificacion: $('#no_identificacion').val()
        }

        let query = $.param(params)
        let path = PATH_API + 'api/reemplazos?'+ query

        $('#tbl_productos').DataTable().ajax.url(path).load()
    }

    $('#reemplazar').click(function(){
        $('#modal-agregar-reemplazo').modal('show')
    })

    const guardarReemplazo = () => {
        toastr.info("Procesando Reemplazo.");

        ajax.post(`api/reemplazo`, {
            producto: $('#producto').val(),
            reemplazo: $('#reemplazo').val()
        }, function(response, headers){
            
            if(headers.status == 201){
                toastr.success(response.message)
                window.location.reload()
            }
        })
    }

</script>
@endsection