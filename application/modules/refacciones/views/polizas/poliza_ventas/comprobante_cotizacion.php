<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Comprobante de pago</title>
	<style>
		#title_comprobante {
			background-color: #323232 !important;
			color: #fff !important;
			padding: 5px !important;
			border-radius: 6px !important;
			margin-bottom: 2px !important;
		}

		.contenedor {
			width: 100%;
		}

		#footer {
			position: absolute !important;
			bottom: 0px;
		}

		td {
			font-size: 11px !important;
		}

		.col-12 {
			width: 100%;
			padding: 3px;
		}

		.col-4 {
			float: left;
			width: 30%;
			padding: 3px;
		}

		.col-6 {
			float: left;
			width: 49%;
			padding: 3px;
		}

		.col-8 {
			float: left;
			width: 68%;
			padding: 3px;
		}

		.col-12 {
			width: 98%;
			padding: 3px;
		}

		.col-5 {
			float: left;
			width: 40%;
			padding: 3px;
		}

		table {
			border-collapse: collapse;
			width: 100%;
		}

		.text-right {
			text-align: right;
		}

		.text-center {
			text-align: center;
		}

		table#detalle_cuenta,
		td,
		td {
			border-bottom: 0.1px solid #233a74;
			margin-bottom: 12px
		}

		.sep30 {
			width: 100%;
			clear: both;
			height: 30px !important;
		}

		.sep100 {
			width: 100%;
			clear: both;
			height: 100px !important;
		}

		.sep50 {
			width: 100%;
			clear: both;
			height: 50px !important;
		}

		.table_border {
			border: 0.1px solid #233a74;
			border-radius: 9px !important;
			width: 100%;
		}
	</style>
</head>

<body>
	<div class="contenedor">
		<table style="width:95%;" class="table" cellpadding="5">
			<tr>
				<td style="width:30%; float:left">
					<img src="<?php echo base_url() . 'img/logo_queretaro_2.png' ?>" alt="" style="width: 200px;">
				</td>
				<td style="width:10%; float:left">
					<img src="<?php echo base_url() . 'img/logo.png' ?>" alt="" style="width: 100px; margin-left:50px; !important">
				</td>
				<td style="width:60%; float:left; text-align:right; font-size:14px">
					<?php
					echo 'Folio: <b>' . $venta->folio->folio . '</b><br/>';
					echo 'Santiago de Querataro, Qro, ' . utils::aFecha(date('Y-m-d')) . '<br/>';
					echo NOMBRE_SUCURSAL . ' S.A de C.V <br/>';
					echo 'Av. Constituyentes Ote 42, Col. Villas del Sol, CP: 76046 <br/>';
					echo 'MYBO20125CM3 <br/>';
					echo 'Tel(442) 238 7400 Ext. 403 y 490 <br/>';
					echo 'REFACCIONES MOSTRADOR <br/>';
					?>
				</td>
			</tr>
		</table>
		<div class="col-12" style="margin-bottom:50px !important">
			<h4 id="title_comprobante">Datos cotización</h4>
			<table style="width:100%;" class="table" cellpadding="5">
				<tr>
					<td style="font-weight:bold">Cliente:</td>
					<td><?php echo isset($cliente->nombre) ? $cliente->nombre . ' ' . $cliente->apellido_paterno . ' ' . $cliente->apellido_materno : ''; ?></td>
					<td style="font-weight:bold">RFC:</td>
					<td><?php echo isset($cliente->rfc) ? $cliente->rfc : ''; ?></td>
				</tr>
				<tr>
					<td style="font-weight:bold">Correo electrónico:</td>
					<td><?php echo isset($cliente->correo_electronico) ? $cliente->correo_electronico : ''; ?></td>
					<td style="font-weight:bold">Teléfono:</td>
					<td><?php echo isset($cliente->telefono) ? $cliente->telefono : ''; ?></td>
				</tr>
				<tr>
					<td style="font-weight:bold">Marca:</td>
					<td><?php echo isset($cotizador->marca) ? $cotizador->marca->nombre : ''; ?></td>
					<td style="font-weight:bold">Modelo:</td>
					<td><?php echo isset($cotizador->modelo) ? $cotizador->modelo->nombre : ''; ?></td>
				</tr>
				<tr>
					<td style="font-weight:bold">Año:</td>
					<td><?php echo isset($cotizador->anio) ? $cotizador->anio : ''; ?></td>
					<td style="font-weight:bold">Número de serie:</td>
					<td><?php echo isset($cotizador->no_serie) ? $cotizador->no_serie : ''; ?></td>
				</tr>
			</table>
		</div>
		<div class="sep30"></div>
		<div class="contenedor">
			<div class="col-12">
				<table id="detalle_cuenta" style="width:100%; margin-top:10px !important" class="table table_border" cellpadding="10" spacing="10">
					<tr id="title_comprobante">
						<td style="font-size: 11px; font-weight:bold; color:#fff" scope="col">Cantidad</td>
						<td style="font-size: 11px; font-weight:bold; color:#fff;" scope="col">Clave unidad</td>
						<td style="font-size: 11px; font-weight:bold; color:#fff" scope="col">Descripción</td>
						<td style="font-size: 11px; font-weight:bold; color:#fff" scope="col">Valor unitario</td>
						<td style="font-size: 11px; font-weight:bold; color:#fff" scope="col">Importe</td>
					</tr>
					<?php
					if ($productos && count($productos) >= 1) {
						foreach ($productos as $key => $item) { ?>
							<tr>
								<td><?php echo  $item->cantidad; ?></td>
								<td><?php echo  $item->unidad; ?></td>
								<td><?php echo  $item->descripcion_producto; ?></td>
								<td><?php echo  '$' . number_format($item->valor_unitario, 2); ?></td>
								<td><?php echo  '$' . number_format($item->venta_total, 2); ?></td>
							</tr>
					<?php  }
					} ?>
				</table>
			</div>
			<div class="sep30"></div>
			<div class="sep30"></div>
			<div class="col-6">
				<table class="table" cellpadding="5">
					<tr>
						<td style="font-weight:bold">TOTAL EN LETRA</td>
					</tr>
					<tr>
						<td>
							<?php echo strtoupper(Numeros::convertirPesosEnLetras(round($venta->venta_total, 2))); ?>
						</td>
					</tr>
				</table>
			</div>
			<div class="col-6">
				<table class="table table_border" cellpadding="5">
					<tr>
						<td id="title_comprobante" style="font-weight:bold">Subtotal</td>
						<td style="font-weight:bold; text-align:right"><?php echo isset($venta->subtotal) ? '$' . number_format($venta->subtotal, 2) : ''; ?></td>
					</tr>
					<tr>
						<td id="title_comprobante" style="font-weight:bold">IVA 0.16</td>
						<td style="font-weight:bold; text-align:right"><?php echo isset($venta->iva) ? '$' . number_format($venta->iva, 2) : ''; ?></td>
					</tr>
					<tr>
						<td id="title_comprobante" style="font-weight:bold">Total</td>
						<td style="font-weight:bold; text-align:right"><?php echo isset($venta->venta_total) ? '$' . number_format($venta->venta_total, 2) : ''; ?></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="sep50"></div>
		<?php $this->load->view("polizas/poliza_ventas/cuentas"); ?>
		<div class="sep100"></div>
		<div id="footer">
			<div class="text-center" style="font-size: 10px;">NO SE ACEPTAN CAMBIOS NI DEVOLUCIONES EN PIEZAS SOBRE PEDIDO<br />PRECIOS SUJETOS A CAMBIOS SIN PREVIO AVISO</div>
		</div>
</body>

</html>