<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Poliza ventas</title>
	<style>
		.contenedor {
			width: 100%;
		}

		.col-12 {
			width: 100%;
			padding: 3px;
		}
		td{
			font-size:11px !important;
			text-align: right !important;
		}

		.col-6 {
			float: left;
			width: 49%;
			padding: 3px;
		}

		.col-5 {
			float: left;
			width: 40%;
			padding: 3px;
		}

		table {
			border-collapse: collapse;
			width: 100%;
		}

		table,
		th,
		td {
			border: 1px solid #233a74;
			margin-bottom: 12px
		}

	</style>
</head>

<body>
	<div class="contenedor">
		<div class="col-12">
			Mexico D.F. <?php echo obtenerFechaEnLetra(date('yy-m-d')); ?>
		</div>
		<div class="col-12">
			<h2>Poliza de Compras</h2>
		</div>
	</div>
	<div class="contenedor">
		<div class="col-12">
			<table class="table" cellpadding="5">
				<thead>
					<tr>
						<th style="font-size: 11px" scope="col">Folio</th>
						<th style="font-size: 11px" scope="col">Cve Producto</th>
						<th style="font-size: 11px" scope="col">Descripcion</th>
						<th style="font-size: 11px" scope="col">Unidad</th>
						<th style="font-size: 11px" scope="col">Cantidad</th>
						<th style="font-size: 11px" scope="col">V/Unitario</th>
						<th style="font-size: 11px" scope="col">Total</th>
						<th style="font-size: 11px" scope="col">Estatus</th>
						<th style="font-size: 11px" scope="col">Fecha venta</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					foreach ($ventas as $key => $item) { ?>
					<tr>
						<td><?php echo  $item->folio; ?></td>
						<td><?php echo  $item->no_identificacion; ?></td>
						<td><?php echo  $item->descripcion; ?></td>
						<td><?php echo  $item->unidad; ?></td>
						<td><?php echo  in_array($item->estatusId, [2,3]) ?  $item->cantidad : '-'.$item->cantidad; ?></td>
						<td><?php echo  '$'. number_format($item->valor_unitario); ?></td>
						<td><?php echo  '$'. number_format($item->venta_total); ?></td>
						<td><?php echo  $item->estatusVenta; ?></td>
						<td>
							<?php echo date('d-m-yy', strtotime($item->created_at)) ?>
						</td>
					</tr>
					<?php  } ?>
				</tbody>
				<tfoot>
					<tr>
						<th style="font-size: 11px" scope="col">No Orden</th>
						<th style="font-size: 11px" scope="col">Cantidad</th>
						<th style="font-size: 11px" scope="col">Cve Producto</th>
						<th style="font-size: 11px" scope="col">Descripcion</th>
						<th style="font-size: 11px" scope="col">Unidad</th>
						<th style="font-size: 11px" scope="col">V/Unitario</th>
						<th style="font-size: 11px" scope="col">Total</th>
						<th style="font-size: 11px" scope="col">Estatus</th>
						<th style="font-size: 11px" scope="col">Fecha compra</th>
					</tr>
				</tfoot>
			</table>
		</div>
		<div class="col-12">
			<h3>Resultados totales</h3>
			<table style="width:80%;" align="right" class="table" cellpadding="5">
				<thead>
					<tr>
						<th style="font-size: 11px" scope="col">Total Ordenes</th>
						<th style="font-size: 11px" scope="col">Productos vendidos</th>
						<th style="font-size: 11px" scope="col">Total ventas</th>
						<?php if ($fecha_inicio && $fecha_fin) { ?>
						<th style="font-size: 11px" scope="col">Periodo</th>
						<?php } ?>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td id="totalOrdenes"><?php echo isset($totales->total_ventas) ? $totales->total_ventas : '';?>
						</td>
						<td id="totalCantidad"><?php echo isset($totales->total_cantidad) ? $totales->total_cantidad : '';?>
						</td>
						<td id="sumaTotal"><?php echo isset($totales->sum_venta_total) ? '$'. number_format($totales->sum_venta_total) : '';?></td>
						<?php if ($fecha_inicio && $fecha_fin) { ?>
						<td>
							<span id=""><?php echo obtenerFechaEnLetra($fecha_inicio); ?></span> AL <span id="">
								<?php echo obtenerFechaEnLetra($fecha_fin); ?>
						</td>
						<?php } ?>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>

</html>
