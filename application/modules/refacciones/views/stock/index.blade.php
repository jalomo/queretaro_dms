@layout('tema_luna/layout')

@section('contenido')
    <div id="app">
        <div class="container-fluid panel-body rounded">
            <h1 class="font-weight-bold">Actualizar Stock</h1>
            <ul class="nav nav-tabs mb-2">
                <li class="nav-item">
                    <router-link class="nav-link" :class="$route.path == '/' ? 'bg-primary text-white' : 'bg-white text-dark'" to="/">CSV</router-link>
                </li>
                <li class="nav-item">
                    <router-link class="nav-link" :class="$route.path == '/text' ? 'bg-primary text-white' : 'bg-white text-dark'" to="/text">TXT</router-link>
                </li>
                <li class="nav-item">
                    <router-link class="nav-link" :class="$route.path == '/clientes' ? 'bg-primary text-white' : 'bg-white text-dark'" to="/clientes">Clientes</router-link>
                </li>
            </ul>
            <router-view></router-view>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-router/2.0.2/vue-router.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.26.0/axios.min.js"></script>
    <script src="{{ base_url("/js/custom/moment.js") }}"></script>
    <script src="{{ base_url("/assets/components/refacciones/stock.js") }}"></script>
    <script src="{{ base_url("/assets/components/refacciones/text.js") }}"></script>
    <script src="{{ base_url("/assets/components/refacciones/clientes.js") }}"></script>
    <script src="{{ base_url("/assets/components/refacciones/table.js") }}"></script>
    <script>
        var routes = [
            { path: "/", component: Stock },
            { path: "/text", component: Text },
            { path: "/clientes", component: Clientes },
        ]

        var router = new VueRouter({
            routes
        })

        Vue.component('table-stock', TableStock)
        Vue.prototype.moment = moment

        new Vue({
            el: "#app",
            router: router,
        })

    </script>
@endsection