@layout('tema_luna/layout')

@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : "" }}</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right mb-2">
                <a href="{{ site_url('refacciones/salidas/cotizador') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Nueva cotización</a>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="tbl_cotizaciones" width="100%" cellspacing="0"></table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
<div class="modal fade" id="modal-comentario" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal">Comentario Proactivo</h5>
            </div>
            <div class="p-4">
                <form id="form-comentario-proactivo">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>*Comentario</label>
                                <textarea name="descripcion" id="descripcion" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="cotizador_refaccion_id" id="cotizador_refaccion_id" />
                    <input type="hidden" name="usuario_id" id="usuario_id" />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button onclick="app.saveComentario()" id="btn-modal-agregar" type="button" class="btn btn-primary"><i class="fas fa-save"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-comentario-historico" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal">Historico comentario</h5>
            </div>
            <div class="p-4">
                <table class="table table-striped" id="tabla_historico_proactivo"></table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ base_url('js/refacciones/cotizador/index.js') }}"></script>
@endsection