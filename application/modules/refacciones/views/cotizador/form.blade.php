@layout('tema_luna/layout')

@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : "" }}</h1>
    <form id="form-cotizador">
        <div class="row mt-3">
            <div class="col-md-4">
                <div class="form-group">
                    <label>*Nombre cliente</label>
                    <input type="text" name="nombre_contacto" class="form-control" value="{{ isset($datos->nombre_contacto) ? $datos->nombre_contacto : '' }} " />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Teléfono</label>
                    <input type="text" name="telefono" class="form-control" value="{{ isset($datos->telefono) ? $datos->telefono : '' }} " />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Marca2</label>
                    <input type="text" name="marca" class="form-control" value="{{ isset($datos->marca) ? $datos->marca : '' }} " />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Modelo</label>
                    <input type="text" name="modelo" class="form-control" value="{{ isset($datos->modelo) ? $datos->modelo : '' }} " />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Año</label>
                    <select name="anio" class="form-control" id="anio">
                        <?php $year = date("Y");
                        for ($i = 1980; $i < $year; $i++) { ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Número serie</label>
                    <input type="text" name="no_serie" class="form-control" value="{{ isset($datos->no_serie) ? $datos->no_serie : '' }} " />
                </div>
            </div>

            <div class="col-md-12">
                <div class="pull-right mb-2">
                    @if($this->input->get('cotizador_id'))
                    <button type="button" class="btn btn-primary" onclick="app.updateCotizacion()"><i class="fa fa-edit"></i> Modificar</button>
                    @else
                    <button type="button" class="btn btn-primary" onclick="app.createCotizacion()"><i class="fa fa-arrow-right"></i> Siguiente </a>
                        @endif
                </div>
            </div>
        </div>
    </form>
    <hr />
    @if($this->input->get('cotizador_id'))
    <div class="col-md-12">
        <div class="pull-right mb-2">
            <button type="button" class="btn btn-primary" onclick="app.openModal()"><i class="fa fa-plus"></i> Agregar producto</button>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="tbl_detalle_cotizacion" width="100%" cellspacing="0"></table>
        </div>
        <div class="pull-right mb-2 mt-2">
            <h2>Total: <span id="total_cotizador" class="money_format"></span></h2>
        </div>
    </div>
    <div class="col-md-12">
        <div class="pull-left mt-3">
            <a href="{{ site_url('refacciones/cotizador/index') }}" class="btn btn-dark"><i class="fa fa-arrow-left"></i> Regresar listado</a>
        </div>
    </div>
    <div class="clearfix"></div>

    @endif
</div>
@endsection
@section('modal')
<div class="modal fade" id="modal-producto-cotizar" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal">Agregar Producto</h5>
            </div>
            <div class="modal-body">
                <form id="form-producto-cotizador">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>*Número parte</label>
                                <input type="text" name="numero_parte" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>*Descripción</label>
                                <input type="text" name="descripcion" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>*Cantidad</label>
                                <input type="number" name="cantidad" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>*Precio neto</label>
                                <input type="text" name="precio_neto" class="form-control" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button onclick="app.agregarProducto()" id="btn-modal-agregar" type="button" class="btn btn-primary"><i class="fas fa-shopping-cart"></i> Agregar</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    let id_cotizador = "{{ isset($datos->id) ? $datos->id : null }}";
</script>
<script src="{{ base_url('js/refacciones/cotizador/form.js') }}"></script>
@endsection