@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
	<h1 class="mt-4">{{ isset($titulo) ? $titulo : "" }}</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item active">{{ isset($titulo) ? $titulo : "" }}</li>
	</ol>
	<div class="row mt-2">
		<?php
			$subtotal = $datos->subtotal ? $datos->subtotal : $totales->sum_venta_total;
			$iva = $datos->iva ? $datos->iva : $subtotal * 0.16;
			$total = $subtotal + $iva;
		?>
		<div class="col-md-3">
			<?php echo renderInputText("text", "folio", "Folio", isset($datos) ? $datos->folio->folio : '', true); ?>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="select">Cliente</label>
				<select name="cliente_id" class="form-control select2" id="cliente_id">
					<option value=""> Seleccionar cliente</option>
					@foreach ($cat_clientes as $cliente)
					<?php
					$aplica_credito = $cliente->aplica_credito && $cliente->aplica_credito == true ? 'Con credito' : 'Sin credito';
					$cliente_nombre = $cliente->nombre . ' ' . $cliente->apellido_paterno . ' | ' . $aplica_credito; ?>
					@if($cliente->id == $datos->cliente_id)
					<option selected="selected" value="{{ $cliente->id}}"> {{ $cliente_nombre }}</option>
					@else
					<option value="{{ $cliente->id}}"> {{ $cliente_nombre }}</option>
					@endif
					@endforeach
				</select>
				<div id='cliente_id_error' class='invalid-feedback'></div>
			</div>
		</div>
		<div class="col-md-2">
			<label>Subtotal:</label>
			<input type="text" class="form-control" id="subtotal" readonly="readonly" value="<?php echo isset($subtotal) ? number_format($subtotal, 2) : ''; ?>" />
		</div>
		<div class="col-md-2">
			<label>IVA (0.16):</label>
			<input type="text" class="form-control" id="iva" readonly="readonly" value="<?php echo isset($iva) ? number_format($iva, 2) : ''; ?>" />
		</div>
		<div class="col-md-2">
			<label>Total</label>
			<input type="text" class="form-control" id="total" readonly="readonly" value="<?php echo isset($total) ? number_format($total, 2) : ''; ?>" />
		</div>
	</div>
	<div class="row mt-2">
		<div class="col-md-3">
			<div class="form-group">
				<label for="select">¿Compra de credito ó contado?</label>
				<select name="tipo_forma_pago_id" class="form-control " id="tipo_forma_pago_id">
					@foreach ($tipo_forma_pago as $tipo)
					@if($tipo->id == $cxc->tipo_forma_pago_id)
					<option value="{{ $tipo->id}}" selected="selected"> {{ $tipo->descripcion}} </option>
					@else
					<option value="{{ $tipo->id}}"> {{ $tipo->descripcion}} </option>
					@endif
					@endforeach
				</select>
				<div id='tipo_forma_pago_id_error' class='invalid-feedback'></div>
			</div>
		</div>
		<div class="col-md-3 container-forma-pago">
			<div class="form-group">
				<label for="select">Plazo de credito</label>
				<select name="plazo_credito_id" class="form-control " id="plazo_credito_id">
					<option value=""> Seleccionar</option>
					@foreach ($plazo_credito as $plazo)
					@if($plazo->id != 14)
					@if($plazo->id == $cxc->plazo_credito_id)
					<option selected="selected" data-cantidad_mes="{{$plazo->cantidad_mes}}" value="{{ $plazo->id }}"> {{ $plazo->nombre}} </option>
					@else
					<option value="{{ $plazo->id}}"> {{ $plazo->nombre}} </option>
					@endif
					@endif
					@endforeach
				</select>
				<div id='plazo_credito_id_error' class='invalid-feedback'></div>
			</div>
		</div>
		<div class="col-md-2 container-forma-pago">
			<div class="form-group">
				<label for="select">Tasa de interes %</label>
				<input type="number" name="tasa_interes" id="tasa_interes" class="form-control" value="<?php echo isset($cxc->tasa_interes) ? $cxc->tasa_interes : '0'; ?>" />
				<div id='tasa_interes_error' class='invalid-feedback'></div>
			</div>
		</div>
		<div class="col-md-2 container-forma-pago">
			<div class="form-group">
				<label for="select">Total enganche</label>
				<input type="number" name="enganche" id="enganche" class="form-control" value="<?php echo isset($cxc->enganche) ? $cxc->enganche : '0'; ?>" />
				<div id='enganche_error' class='invalid-feedback'></div>
			</div>
		</div>
		<div class="col-md-2 container-forma-pago">
			<div class="form-group">
				<label for="select">Cobrador asignar</label>
				<select name="usuario_gestor_id" class="form-control " id="usuario_gestor_id">
					<option value=""> Seleccionar</option>
					@foreach ($gestores as $gestor)
					@if(isset($cxc->usuario_gestor_id) && $cxc->usuario_gestor_id == $gestor->id)
					<option value="{{ $gestor->id}}" selected="selected"> {{ $gestor->nombre }} {{ $gestor->apellido_paterno}} </option>
					@else
					<option value="{{ $gestor->id}}"> {{ $gestor->nombre }} {{ $gestor->apellido_paterno}} </option>
					@endif
					@endforeach
				</select>
				<div id='usuario_gestor_id_error' class='invalid-feedback'></div>
			</div>
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="col-md-4">
			<?php echo renderInputText("text", "concepto", "Concepto", isset($cxc->concepto) ? $cxc->concepto :  ''); ?>
		</div>
		<?php if (!empty($datos) && $datos->re_ventas_estatus->estatus_ventas_id == 1) : ?>
			<div class="col-md-4 mt-4">
				<div class="custom-control custom-checkbox">
					<input type="checkbox" class="custom-control-input check_apartar" onchange="apartarbutton()" id="check_apartar">
					<label class="custom-control-label" for="check_apartar">Confirmar apartar piezas</label>
				</div>
			</div>
		<?php endif ?>
	</div>
	<input type="hidden" value="{{ isset($datos) ? $datos->folio_id : '' }}" id="folio_id">
	<input type="hidden" value="{{ isset($datos) ? $datos->id : '' }}" id="venta_id">
	<input type="hidden" value="{{ isset($datos) ? $datos->almacen_id : '' }}" name="almacen_id" id="almacen_id">
	<input type="hidden" value="{{ isset($datos->re_ventas_estatus) ? $datos->re_ventas_estatus->estatus_ventas_id : '' }}" name="estatus_ventas_id" id="estatus_ventas_id">
	<input type="hidden" value="{{ isset($cxc->estatus_cuenta_id) ? $cxc->estatus_cuenta_id : '' }}" name="estatus_cuenta_id" id="estatus_cuenta_id">
	<input type="hidden" value="{{ isset( $cxc->id) ? $cxc->id : '' }}" name="cuenta_por_cobrar_id" id="cuenta_por_cobrar_id">

	<input type="hidden" id="venta_total" name="venta_total" value="{{ isset($total) ? $total : '' }}" />
	<div class="row mb-4 mt-4">
		<div class="col-md-12 text-right">
			<?php 
			if (isset($datos) && ($datos->re_ventas_estatus->estatus_ventas_id == 1 ||
				$datos->re_ventas_estatus->estatus_ventas_id == 6)) { ?>
				<button title="Terminar proceso de venta y mandar a pagar al módulo de cajas" class="btn btn-primary col-md-2" id="btn-finalizar">
					<i class="fas fa-shopping-cart"></i>
					Terminar y enviar a cajas
				</button>
				<button onclick="apartarVenta()" class="btn btn-primary col-md-4 d-none" id="btn-apartar">
					<i class="fas fa-handshake"></i>
					Apartar
				</button>
			<?php } else { 
				if (isset($cxc->estatus_cuenta_id) && $cxc->estatus_cuenta_id == 1) { ?>
					<button title="Modificar contenido de la venta" class="btn btn-primary col-md-2" id="btn-editar">
						<i class="fas fa-pencil-alt"></i>
						Modificar venta
					</button>
				<?php } else { ?>
				<a target="_blank" title="Imprimir comprobante de pago" class="btn btn-success col-md-2" id="btn-poliza" href="{{ base_url('refacciones/polizas/generarPolizaVentasPDF?folio_id='.$datos->folio_id) }}">
					<i class="fas fa-print"></i> Comprobante pago
				</a>
			<?php }
			}
			?>
		</div>
	</div>
	<br>
	<h3>Productos seleccionados</h3>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-bordered table-striped" id="tbl_carrito" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>#</th>
							<th>No identificación</th>
							<th>Descripcion</th>
							<th>Precio</th>
							<th>Cantidad</th>
							<th>Total</th>
							<th>Unidad</th>
							<th>Ubicación producto</th>
							<th>Almacen</th>
							<th>-</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>#</th>
							<th>No identificación</th>
							<th>Descripcion</th>
							<th>Valor unitario</th>
							<th>Cantidad</th>
							<th>Total</th>
							<th>Unidad</th>
							<th>Ubicación producto</th>
							<th>Almacen</th>
							<th>-</th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
	<hr>
	<h3>Productos disponibles</h3>
	<hr>
	<div class="row align-items-end mb-2">
		<div class="col-md-3">
			<div class="form-group mb-0">
				<label>No identificación</label>
				<input type="text" name="no_identificacion" id="no_identificacion_form" class="form-control buscar_enter" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group mb-0">
				<label>Descripción</label>
				<input type="text" name="descripcion" id="descripcion_form" class="form-control buscar_enter" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group mb-0">
				<label>Almacen</label>
				<select class="form-control" id="almacenes">
					<option value="1" selected>{{ getenv('QUERETARO') == 'TRUE'? 'Queretaro' : 'San Juan' }}</option>
					<option value="2">{{ getenv('SAN_JUAN') == 'FALSE'? 'San Juan' : 'Queretaro' }}</option>
				</select>
			</div>
		</div>
		<div class="col-md-2 mt-4">
			<button type="button" onclick="getBusquedaProductosDisponibles()" class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-bordered table-striped" id="tbl_productos" width="100%" cellspacing="0"></table>
			</div>
		</div>
	</div>
	<a href="{{ site_url('refacciones/salidas/listadoVentas') }}" class="btn btn-dark mt-4"><i class="fa fa-arrow-left"></i> Regresar listado venta</a>
</div>
@endsection

@section('scripts')
<script src="{{ base_url('js/refacciones/salidas/realizarventa.js') }}"></script>
@endsection

@section('modal')
<div class="modal fade" id="modal-producto-detalle" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="title_modal"></h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<?php echo renderInputText("number", "cantidad", "Cantidad", 1); ?>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label for="select">Aplicar precio</label>
							<select name="precio_id" class="form-control" id="precio_id">
								@foreach ($cat_precios as $precio)
								@if ($precio->id == 1)
								<option value="{{ $precio->id}}"> {{ $precio->descripcion}} </option>
								@endif
								@endforeach
							</select>

						</div>
					</div>
					<div class="col-md-12">
						<?php echo renderInputText("number", "valor_unitario", "Valor unitario", 1); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>* Seleccionar tipo de precio <span class="porcentaje_publico"></span></label>
							<select name="tipo_precio" onchange="changeTipoPrecio(this)" id="tipo_precio" class="form-control">
								<option value="">Seleccion un tipo de precio</option>
								<option value="1" data-porcentaje="0.0">Público (40%)</option>
								<option value="2" data-porcentaje="0.9">Costo + 10 (10%)</option>
								<option value="4" data-porcentaje="0.92">Costo+ 8 (8%)</option>
								<option value="5" data-porcentaje="0.8">Costo + 20 (20%)</option>
								<option value="100">Manual</option>
							</select>
						</div>
					</div>
					<div class="col-md-12 precio_costo" style="display:none">
						<div class="form-group">
							<label class="label_precio_costo">Precio costo</label>
							<input type="text" name="precio_costo" readonly="readonly" id="precio_costo" class="form-control" value="" />
						</div>
					</div>
					<div class="col-md-12 precio_manual" style="display:none">
						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
									<label>Porcentaje Manual</label>
									<input type="number" name="porcentaje_manual" id="porcentaje_manual" class="form-control" value="" />
								</div>
								<div class="col-md-6">
									<label>Precio costo</label>
									<input type="text" name="precio_costo_manual" readonly="readonly" id="precio_costo_manual" class="form-control" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="producto_id" id="producto_id">
				<input type="hidden" name="nuevo_valor_unitario" id="nuevo_valor_unitario">
				<input type="hidden" name="no_identificacion" id="no_identificacion">
				<input type="hidden" name="cantidad_almacen_primario" id="cantidad_almacen_primario">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				<button onclick="agregarproducto()" id="btn-modal-agregar" type="button" class="btn btn-primary"><i class="fas fa-shopping-cart"></i> Agregar</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-permiso-precio" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="title_modal">
					Permisos para asignar precios
				</h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<?php echo renderInputText("text", "usuario", "usuario", false); ?>
					</div>
					<div class="col-md-12">
						<?php echo renderInputText("password", "password", "password", false); ?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" id="btn_cerrar_permiso" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-secondary" id="btn_cerrar_porcentaje" onClick="cerrarmodales()" data-dismiss="modal">Cancelar</button>
				<button id="modal-permiso" type="button" class="btn btn-primary">
					<button id="btn-modal-permiso-porcentaje" type="button" class="btn btn-primary">
						<i class="fas fa-lock-open"></i> Continuar
					</button>
			</div>
		</div>
	</div>
</div>
@endsection