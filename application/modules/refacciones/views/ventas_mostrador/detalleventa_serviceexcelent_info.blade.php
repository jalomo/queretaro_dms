@layout('tema_luna/layout_iframes')
@section('contenido')
    <h3>Productos</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_carrito" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>No Pieza</th>
                            <th>Descripcion</th>
                            <th>Refacciones</th>
                            <th>Total Refacciones</th>
                            <th>Costo Mano de Obra</th>
                            <th>Total</th>
                            <th>Cantidad</th>
                            <th>Unidad</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script> 

    var tabla_carrito = $('#tbl_carrito').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        ajax: {
            url: PATH_API + "api/detalles/" + {{ $numero_orden }} + "/servicios",
            type: 'GET',
            dataSrc: function(json){

                return json.aaData.sort(function(a, b){

                    // Si no es servicio
                    if(a.servicio == false)
                        first = a.venta_servicio_id
                    else
                        first = a.id

                    // Si no es servicio
                    if(b.servicio == false)
                        second = b.venta_servicio_id
                    else
                        second = b.id

                    return second - first
                })
            }
        },
        processing: true,
        serverSide: true,
        paging:false,
        bFilter: false,
        bSort: false,
        columns: [{
                'data': 'id'
            },
            {
                'data': function(data) {

                    let text = data.no_identificacion
                    // text += data.reemplazo_no_identificacion? `<br><small class="text-danger">REEMPLAZO: ${data.reemplazo_no_identificacion}</small>` : ''
                    text += `<br>`
                    
                    if(data.presupuesto)
                        text += `<i class="fas fa-clipboard-list mr-2" title="Presupuesto"></i>`

                    if(data.preventivo)
                        text += `<i class="fa fa-bandage mr-2" title="Preventivo"></i>`

                    if(data.bodyshop)
                        text += `<i class="fa fa-store-alt mr-2" title="Bodyshop"></i>`
                    
                    if(data.afecta_paquete)
                        text += `<i class="fas fa-dolly-flatbed mr-2" title="Afecta Paquete"></i>`

                    if(data.afecta_mano_obra)
                        text += `<i class="fas fa-hand-paper mr-2" title="Afecta Mano de Obra"></i>`

                    if(data.detalles)
                        text += `<i class="fa fa-user-plus mr-2" title="Detalles"></i>`

                    return text
                }
            },
            {
                data: function(data){
                    let ubicacion = data.ubicacion? data.ubicacion : ''
                    let date = (new Date(data.created_at)).toLocaleString('es-MX');

                    if(data.servicio == true){
                        return `${data.descripcion}<br><i class="fa fa-clock"></i> ${date}`    
                    }

                    return `${data.descripcion}<br>${ubicacion} <i class="fa fa-clock ml-2"></i> ${date}`
                }
            },
            {
                data: function(data) {
                    mxn = Intl.NumberFormat('es-MX', {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                        currency: 'MXN',
                    })

                    return data.servicio == false ? `$ ${mxn.format(data.valor_unitario)}` : `$ ${mxn.format(data.refacciones)}`
                },
                className: 'text-right text-nowrap',
            },
            {
                data: function(data) {
                    mxn = Intl.NumberFormat('es-MX', {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                        currency: 'MXN',
                    })

                    return data.servicio == false? `$ ${mxn.format(data.valor_unitario * data.cantidad)}` : `$ ${mxn.format(data.refacciones)}`
                },
                className: 'text-right text-nowrap',
            },
            {
                data: function(data) {
                    mxn = Intl.NumberFormat('es-MX', {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                        currency: 'MXN',
                    })

                    return data.servicio == true? `$ ${mxn.format(data.costo_mano_obra)}` : `$ ${mxn.format(data.costo_mano_obra)}`
                },
                className: 'text-right text-nowrap',
            },
            {
                data: function(data) {
                    mxn = Intl.NumberFormat('es-MX', {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                        currency: 'MXN',
                    })

                    
                    return data.servicio == false ? `$ ${mxn.format((parseFloat(data.valor_unitario) * parseFloat(data.cantidad)) + parseFloat(data.costo_mano_obra))}` : `$ ${mxn.format(parseFloat(data.costo_mano_obra) + parseFloat(data.refacciones))}`
                },
                className: 'text-right text-nowrap',
            },
            {
                data: function(data) {
                    return data.cantidad
                },
                className: 'text-center'
            },
            {
                'data': function(data) {
                    return data.servicio == false? 'PIEZA' : '--'
                }
            },
        ],
        createdRow: function(row, data, dataIndex) {

            if(data.servicio == false && data.venta_servicio_id){
                $(row).find('td:lt(8)').css('background-color', '#BAD7E9')
            }

            if(data.servicio == true)
                $(row).find('td:lt(8)').css('background-color', '#F8EAD8')
        },
        fnInitComplete: function(oSettings, json){

            let costo_mano_obra = 0
            let costo_refacciones = 0
            let costo_refacciones_todas = 0
            let costo_refacciones_afectan = 0
            let total = 0
            let total_iva = 0
            
            costo_mano_obra = json.aaData.filter(operacion => operacion.servicio == false).reduce((costo_mano_obra, operacion) => costo_mano_obra + parseFloat(operacion.costo_mano_obra), costo_mano_obra)
            costo_mano_obra = json.aaData.filter(refaccion => refaccion.servicio == true).reduce((costo_mano_obra, refaccion) => costo_mano_obra + parseFloat(refaccion.costo_mano_obra), costo_mano_obra)

            costo_refacciones = json.aaData.filter(operacion => operacion.servicio == true).reduce((costo_refacciones, operacion) => costo_refacciones + parseFloat(operacion.refacciones), costo_refacciones)
            costo_refacciones = json.aaData
                .filter(refaccion => refaccion.servicio == false)
                .filter(refaccion => refaccion.afecta_mano_obra == false)
                .filter(refaccion => refaccion.afecta_paquete == false)
                .reduce((costo_refacciones, refaccion) => costo_refacciones + (parseFloat(refaccion.valor_unitario) * parseFloat(refaccion.cantidad)), costo_refacciones)

            costo_refacciones_todas = json.aaData.filter(operacion => operacion.servicio == true).reduce((costo_refacciones, operacion) => costo_refacciones + parseFloat(operacion.refacciones), costo_refacciones_todas)
            costo_refacciones_todas = json.aaData
                .filter(refaccion => refaccion.servicio == false)
                .reduce((costo_refacciones_todas, refaccion) => costo_refacciones_todas + (parseFloat(refaccion.valor_unitario) * parseFloat(refaccion.cantidad)), costo_refacciones_todas)

            costo_refacciones_afectan = json.aaData
                .filter(refaccion => refaccion.servicio == false)
                .filter(refaccion => refaccion.afecta_mano_obra == true || refaccion.afecta_paquete == true)
                .reduce((costo_refacciones_afectan, refaccion) => costo_refacciones_afectan + (parseFloat(refaccion.valor_unitario) * parseFloat(refaccion.cantidad)), costo_refacciones_afectan)

            total = costo_mano_obra + costo_refacciones
            total_iva = (costo_mano_obra + costo_refacciones) * 1.16

            mxn = Intl.NumberFormat('es-MX', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
                currency: 'MXN',
            })

            $('#tbl_carrito tr:last').after(`<tr>
                <td colspan="2" class="font-weight-bold">Subtotal:</td>
                <td class="text-right" colspan="3">
                    Total de Refacciones: $ ${mxn.format(costo_refacciones_todas)} <br>
                    Afectan Mano de Obra y Paquete: - $ ${mxn.format(costo_refacciones_afectan)}<br>
                    Total Refacciones a cobrar: $ ${mxn.format(costo_refacciones)}
                </td>
                <td class="text-right text-nowrap" colspan="1">$ ${mxn.format(costo_mano_obra)}</td>
                <td class="text-right text-nowrap" colspan="1">$ ${mxn.format(total)}</td>
                <td colspan="3"></td>
                </tr>`
            )
            $('#tbl_carrito tr:last').after(`<tr>
                <td colspan="2" class="font-weight-bold">Total con IVA:</td>
                <td class="text-right text-nowrap" colspan="5">$ ${mxn.format(total_iva)}</td>
                <td colspan="3"></td>
                </tr>`
            )
        } 
    });
</script>
@endsection