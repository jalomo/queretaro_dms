@layout('tema_luna/layout')
<style>
	.nav-fill .nav-item {
		-ms-flex: .3 1 auto !important;
		flex: .3 1 auto !important;
		text-align: center;
	}

	input[type="number"i] {
		background-color: #fff !important;
		border: 1px solid #323232;
	}
</style>
@section('contenido')
<link href="{{ base_url('css/custom/tabs_custom.css') }}" rel="stylesheet">
<div class="container-fluid panel-body">
	<h1 class="mt-4">{{ isset($titulo) ? $titulo : "" }}</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item active">{{ isset($titulo) ? $titulo : "" }}</li>
	</ol>
	<div class="col-md-12">
		@if (isset($tipo_venta_id) && $tipo_venta_id == 1)
		<a href="{{ site_url('refacciones/salidas/listadoVentas') }}" class="btn btn-dark mb-2"> <i class="fa fa-arrow-left"></i> Regresar listado </a>
		@else
		<a href="{{ site_url('refacciones/cotizador/index') }}" class="btn btn-dark mb-2"> <i class="fa fa-arrow-left"></i> Regresar listado </a>
		@endif
	</div>
	<?php
	$active_venta = isset($venta->id) && $venta->id  ? 'enabled' : 'disabled';
	$active_cajas = isset($cxc->id) && $cxc->id  ? 'enabled' : 'disabled';
	?>
	<div class="c_breadcrumb">
		<ul class="nav nav-pills nav-tabs nav-fill" role="tablist">
			<li class="nav-item">
				<a id="nav_paso1" class="nav-link" data-toggle="tab" href="#paso1">{{ isset($tipo_venta_id) && $tipo_venta_id == 1 ? 'Datos venta' : 'Datos cotización' }}</a>
			</li>
			<li class="nav-item">
				<a id="nav_paso2" class="nav-link {{ $active_venta }}" data-toggle="tab" href="#paso2">Elegir productos</a>
			</li>
			@if (isset($tipo_venta_id) && $tipo_venta_id == 1)
			<li class="nav-item">
				<a id="nav_paso3" class="nav-link disabled" data-toggle="tab" href="#paso3">Mandar a Cajas</a>
			</li>
			@endif
		</ul>
	</div>
	<div class="tab-content p-3" style="border:1px solid #d5d5d5; background-color:#fff;">
		<div class="tab-pane" id="paso1">
			@include('refacciones/ventas_mostrador/partials/datos_venta')
		</div>
		<div class="tab-pane" id="paso2">
			@include('refacciones/ventas_mostrador/partials/eleccion_productos')
		</div>
		@if (isset($tipo_venta_id) && $tipo_venta_id == 1)
		<div class="tab-pane" id="paso3">
			@include('refacciones/ventas_mostrador/partials/eleccion_pago')
		</div>
		@endif

	</div>
</div>
@endsection
@section('modal')
<div class="modal fade" id="modal-alta-cliente" data-backdrop="static" data-keyboard="false" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="modalagregarcliente" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="title_modal_cliente"> Agregar cliente</h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<form id="form-cliente" class="col-md-12">
						@include('refacciones/ventas_mostrador/partials/registro_cliente')
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" id="btn_cerrar_permiso" data-dismiss="modal">Cancelar</button>
				<button id="btn_agregar_cliente" onClick="app.guardarCliente()" type="button" class="btn btn-primary">
					<i class="fas fa-save"></i> Guardar
				</button>
				<button id="btn_editar_cliente" style="display:none" onClick="app.updateCliente()" type="button" class="btn btn-primary">
					<i class="fas fa-edit"></i> Actualizar
				</button>

			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-detalle-cliente" data-backdrop="static" data-keyboard="false" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="modalagregarcliente" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="title_modal"> Detalle cliente</h5>
			</div>
			<div class="modal-body">
				<div class="row" style="font-size:16px; color:#323232">
					<div class="col-12 mt-2"><b>Número cliente: </b><span class="detalle-numero_cliente"></span></div>
					<div class="col-12 mt-2"><b>Nombre cliente: </b><span class="detalle-nombre_cliente"></span></div>
					<div class="col-12 mt-2"><b>RFC: </b><span class="detalle-rfc"></span></div>
					<div class="col-12 mt-2"><b>Aplica crédito: </b><span class="detalle-aplica_credito"></span></div>
					<div class="col-12 mt-2"><b>Plazo crédito: </b><span class="detalle-plazo_credito"></span></div>
					<div class="col-12 mt-2"><b>Limite crédito: </b><span class="detalle-limite_credito"></span></div>
					<div class="col-12 mt-2"><b>Crédito actual: </b><span class="detalle-credito_actual"></span></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" id="btn_cerrar_permiso" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-producto-detalle" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="title_modal_producto"></h5>
			</div>
			<div class="modal-body">
				<form id="form-producto">
					<div class="row">
						<div class="col-md-6">
							<?php echo renderInputText("number", "cantidad", "Cantidad", 1); ?>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="select">Aplicar precio</label>
								<select name="precio_id" class="form-control" id="precio_id">
									@foreach ($cat_precios as $precio)
									@if ($precio->id == 1)
									<option value="{{ $precio->id}}"> {{ $precio->descripcion}} </option>
									@endif
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Costo distribuidor</label>
								<input type="text" id="costo_promedio" readonly="readonly" class="form-control" />
							</div>
						</div>
						<div class="col-md-6">
							<?php echo renderInputText("number", "valor_unitario", "Valor unitario", 1); ?>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>* Seleccionar tipo de precio <span class="porcentaje_publico"></span></label>
								<select onchange="app_paso2.changeTipoPrecio(this)" id="tipo_precio" class="form-control">
									<option value="">Seleccion un tipo de precio</option>
									<option value="1" data-porcentaje="0.0">Público (40%)</option>
									<option value="2" data-porcentaje="0.9">Costo + 10 (10%)</option>
									<option value="4" data-porcentaje="0.92">Costo+ 8 (8%)</option>
									<option value="5" data-porcentaje="0.8">Costo + 20 (20%)</option>
									<option value="100">Manual</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="">Core</label>
								<input type="text" name="core" id="core" class="form-control" value="0" />
							</div>
						</div>
						<div class="col-md-6 precio_costo" style="display:none">
							<div class="form-group">
								<label class="label_precio_costo">Precio costo</label>
								<input type="text" readonly="readonly" id="precio_costo" class="form-control" value="" />
							</div>
						</div>
						<div class="col-md-6 precio_costo" style="display:none">
							<div class="form-group">
								<label class="label_precio_costo">Total</label>
								<input type="text" readonly="readonly" class="form-control total_producto" value="" />
							</div>
						</div>
					</div>
					<div class="row precio_manual" style="display:none">
						<div class="col-md-6">
							<div class="form-group">
								<label>Porcentaje Manual</label>
								<input type="number" id="porcentaje_manual" class="form-control" value="" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Precio costo</label>
								<input type="text" readonly="readonly" id="precio_costo_manual" class="form-control" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="">Costo adicional</label>
								<input type="number" id="costo_adicional" name="costo_adicional" class="form-control" value="" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="label_precio_costo">Total</label>
								<input type="text" readonly="readonly" class="form-control total_producto" value="" />
							</div>
						</div>
					</div>
					<input type="hidden" name="producto_id" id="producto_id">
					<input type="hidden" id="costo_original">
				</form>
				<p>Al modificar el valor unitario, las operaciones se realizarán con ese nuevo precio.</p>
			</div>
			<div class="modal-footer">

				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				<button onclick="app_paso2.agregarProducto()" id="btn-modal-agregar" type="button" class="btn btn-primary"><i class="fas fa-shopping-cart"></i> Agregar</button>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	let vendedor_id = "{{ $this->session->userdata('id') }}";
	let venta_id = "{{ isset($venta->id) ? $venta->id : '' }}";
	let folio_id = "{{ isset($venta->folio_id) ? $venta->folio_id : '' }}";
	let cliente_id = "{{ isset($venta->cliente_id) ? $venta->cliente_id : '' }}";
	let cuenta_cobrar_id = "{{ isset($cxc->id) ? $cxc->id : '' }}";
	let estatus_cuenta_id = "{{ isset($cxc->estatus_cuenta_id) ? $cxc->estatus_cuenta_id : '' }}";
</script>
<script src="{{ base_url('js/refacciones/salidas/flujo_mostrador/datos_venta_.js') }}"></script>
@if (isset($venta->id))
<script src="{{ base_url('js/refacciones/salidas/flujo_mostrador/eleccion_productos.js') }}"></script>
@endif
@if (isset($tipo_venta_id) && $tipo_venta_id == 1)
<script src="{{ base_url('js/refacciones/salidas/flujo_mostrador/eleccion_forma_pago.js') }}"></script>
@endif
@endsection