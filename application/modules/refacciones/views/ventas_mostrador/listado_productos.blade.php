@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
        <li class="breadcrumb-item active">Lista</li>
    </ol>
    <div class="d-flex align-items-end mb-3">
        <div class="col-md-3">
            <label>No. de pieza</label>
            <input type="text" name="no_identificacion" id="no_identificacion" class="form-control buscar_enter"/>
        </div>
        <div class="col-md-3">
            <label>Descripción</label>
            <input type="text" name="descripcion" id="descripcion" class="form-control buscar_enter"/>
        </div>
        <div class="col-md-3">
			<div class="form-group mb-0">
				<label>Almacen</label>
				<select class="form-control" id="almacenes">
					<option value="1" selected>{{ getenv('QUERETARO') == 'TRUE'? 'Queretaro' : 'San Juan' }}</option>
					<option value="2">{{ getenv('SAN_JUAN') == 'FALSE'? 'San Juan' : 'Queretaro' }}</option>
				</select>
			</div>
		</div>
        <div class="col-md-2 mt-4">
            <button type="button" onclick="buscarproducto()" class="btn btn-primary">
                <i class="fa fa-search"></i> Buscar
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="tbl_productos" width="100%" cellspacing="0">
                </table>
            </div>
        </div>
    </div>


</div>
@endsection

@section('scripts')
<script>
    var band = false;
    ajax.get(`api/inventario`, {}, function(response, headers) {
        if (headers.status == 200) {
            $.each(response, function(index, value) {
                if (value.estatus_inventario_id == 1) {
                    band = true;
                    $(".btn_comprar").remove();
                    utils.displayWarningDialog(
                        "Existe un inventario en proceso. Favor de esperar a que finalize", "error",
                        function() {
                            return false;
                        });
                    return false;
                }
            });
        }
    });


    var tbl_productos = $('#tbl_productos').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        "ajax": {
            url: PATH_API + 'api/productos/listadoStock',
            type: 'GET',
        },
        columns: [{
                title: "#",
                data: 'id',
            },
            {
                title: "Prefijo",
                data: 'prefijo',
            },
            {
                title: "Sufijo",
                data: 'sufijo',
            },
            {
                title: "Basico",
                data: 'basico',
            },
            {
                title: "No. de pieza",
                data: 'no_identificacion',
            },
            {
                title: "Descripcion",
                data: 'descripcion',
            },
            {
                title: "Precio unitario",
                data: 'valor_unitario',
                render: function(data, type, S) {

                    return '$ <span class="money_format">' + parseFloat(data).toFixed(2) + '</div>';
                }
            },
            {
                title: "Unidad",
                data: 'unidad',
            },
            {
                title: "Existencia total",
                data: 'cantidad_actual',
            },
            {
                title: "Ubicación",
                data: 'ubicacionProducto'
            },
            {
                title: "-",

                render: function(data, type, {
                    id,
                    cantidad_almacen_primario
                }) {

                    btn = '<a class="btn btn-success btn_comprar"  href="' + base_url +
                        'refacciones/salidas/realizarVenta/' + id +
                        '"><i class="fas fa-shopping-cart"></i> </a>';
                    return btn;
                    //return (cantidad_almacen_primario && cantidad_almacen_primario >= 1) ? btn : '-';
                }
            }
        ]
    });
    $(".buscar_enter").keypress(function(e) {
        if (e.which == 13) {
            buscarproducto();
        }
    });
    const buscarproducto = () => {

        let object = {};
        if (document.getElementById('descripcion').value !== '') {
            object['descripcion'] = document.getElementById('descripcion').value;
        } else if (document.getElementById('no_identificacion').value !== '') {
            object['no_identificacion'] = document.getElementById('no_identificacion').value;
        }
        let parametros = $.param(object);

        let almacen = $('#almacenes').val()
        let path = almacen == 1? PATH_API + 'api/productos/listadoStock?'+ parametros : PATH_API_SANJUAN + "api/productos/listadoStock?" + parametros

        $('#tbl_productos').DataTable().ajax.url(path).load()
    }
</script>

@endsection