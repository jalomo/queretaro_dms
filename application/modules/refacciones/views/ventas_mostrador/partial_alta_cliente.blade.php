<div class="row">
    <div class="col-md-6">
        <label for="">Tipo de cliente:</label>
        <select class="form-control" id="tipo_registro" required onchange="generar_numero_cliente()" style="width: 100%;" <?php echo ((isset($data->id)) ? 'disabled' : ''); ?>>
            @if(!empty($cat_clave))
            @foreach ($cat_clave as $clave)
            <option value="{{$clave->id}}" <?php if (isset($data->tipo_registro)) {
                                                if ($data->tipo_registro == $clave->id) echo "selected";
                                            } ?>>{{$clave->clave}}-{{$clave->nombre}}</option>
            @endforeach
            @endif
        </select>
        <div id="tipo_registro_error" class="invalid-feedback"></div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">N° de Cliente:</label>
            <input type="text" class="form-control" value="" id="numero_cliente" name="numero_cliente" disabled>
            <div id="numero_cliente_error" class="invalid-feedback"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Nombre:</label>
            <input type="text" class="form-control" value="" id="nombre" name="nombre" placeholder="">
            <div id="nombre_error" class="invalid-feedback"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Apellido Paterno:</label>
            <input type="text" class="form-control" value="" id="apellido_paterno" name="apellido_paterno" placeholder="">
            <div id="apellido_paterno_error" class="invalid-feedback"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Apellido Materno:</label>
            <input type="text" class="form-control" value="" id="apellido_materno" name="apellido_materno" placeholder="">
            <div id="apellido_materno_error" class="invalid-feedback"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <label for="">Tipo persona:</label>
        <select class="form-control" id="regimen_fiscal" name="regimen_fiscal" onchange="onchangeregimen()" style="width: 100%;">
            <option value="">Selecionar ...</option>
            <option value="F">Fisica </option>
            <option value="M">Moral </option>
            <option value="A">Ambas </option>
        </select>
        <div id="regimen_fiscal_error" class="invalid-feedback"></div>
    </div>
    <div class="col-md-6">
        <label for="">Régimen fiscal:</label>
        <select class="form-control" name="regimen_fiscal_id" id="regimen_fiscal_id" style="width: 100%;">
        </select>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Razón Social:</label>
            <input type="text" class="form-control" readonly="readonly" value="" id="nombre_empresa" name="nombre_empresa" placeholder="">
            <div id="nombre_empresa_error" class="invalid-feedback"></div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="">RFC:</label>
            <input type="text" class="form-control" minlength="11" value="XAXX010101000" id="rfc" name="rfc" placeholder="">
            <div id="rfc_error" class="invalid-feedback"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="">Dirección:</label>
            <input type="text" class="form-control" value="S/D" id="direccion" name="direccion" placeholder="">
            <div id="direccion_error" class="invalid-feedback"></div>
        </div>
    </div>
</div>
<script>
    let onchangeregimen = () => {
        if (document.getElementById('regimen_fiscal').value == 'M') {
            $("#nombre_empresa").val("");
            $("#nombre_empresa").attr('readonly', false);
        } else {
            $("#nombre_empresa").attr('readonly', true);
            $("#nombre_empresa").val($("#rfc").val());
        }
    }
</script>