@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4 font-weight-bold">
        <i class="fa fa-shopping-cart"></i>
        {{ isset($titulo) ? $titulo : '' }}
    </h1>
    <hr>
    @if(! empty($folio->validaciones))
        <div class="alert alert-danger">
            @foreach($folio->validaciones as $validacion)
                <li>{{ $validacion }}</li>
            @endforeach
        </div>
    @endif
    <div>
        <div class="row mt-2">
            <div class="col px-0">
                <h4 class="col">
                    <i class="fa fa-info"></i>
                    Estatus de venta : <?php echo isset($folio) && isset($folio->re_ventas_estatus) ? $folio->re_ventas_estatus->estatus_venta->nombre : ''; ?>
                </h4>
                <div class="col">
                    <?php echo renderInputText('text', 'cliente', 'Cliente', isset($folio) && isset($folio->cliente) ? $folio->cliente->nombre : '', true); ?>
                    <input type="hidden" value="{{ isset($folio) && $folio->cliente ? $folio->cliente->id : '' }}" id="cliente_id" name="cliente_id">
                </div>
                <div class="col">
                    <?php echo renderInputText('text', 'orden', 'Número de orden', isset($folio) ? $numero_orden  : '', true); ?>
                    <input type="hidden" value="{{ $user_id }}" id="user_id">
                    <input type="hidden" value="{{ isset($folio) ? $folio->folio_id : '' }}" id="folio_id">
                    <input type="hidden" value="{{ isset($folio) ? $folio->id : '' }}" id="venta_id">
                    <input type="hidden" value="{{ isset($folio->re_ventas_estatus) ? $folio->re_ventas_estatus->estatus_venta->id : '' }}" id="estatus_venta_id">
                    <input type="hidden" value="{{ isset($folio) ? $folio->almacen_id : '' }}" name="almacen_id" id="almacen_id">
                    <input type="hidden" value="{{ isset($folio) ? $folio->completa? 1 : 0 : 0 }}" id="orden_completa" />
                    <input type="hidden" name="orden_incompleta" id="orden_incompleta" value="{{ isset($folio->incompleta)? $folio->incompleta? 1 : 0 : 0 }}">
                </div>
                <div class="col">
                    <?php echo renderInputText('text', 'venta_total', 'Total', isset($folio->venta_total) ? number_format($folio->venta_total, 2)  : '', true); ?>
                </div>
            </div>
            <div class="col">
                <h4>
                    <i class="fa fa-car"></i> 
                    Vehiculo
                </h4>
                <div class="row bg-white p-2 border rounded">
                    @foreach ($info_vehiculo as $inputKey => $itemValue)
                        <div class="col-md-12 my-1">
                            <b>{{ str_replace('_', ' ', mb_strtoupper($inputKey)) }}:</b> {{ $itemValue }}
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <hr />
    <div class="d-flex mb-2">
        <input type="hidden"value="{{$folio->re_ventas_estatus->estatus_ventas_id}}" id="statusorden">
        <input type="hidden" id="preventivo" />
        <a class="btn btn-lg btn-primary" href="{{ site_url('refacciones/salidas/otrasSalidas') }}">
            <i class="fas fa-long-arrow-alt-left"></i> Regresar
        </a>

        @if($folio->incompleta == true)
        @elseif($folio->completa == false)
            <div class="ml-auto"></div>
            <button id="btn-agregar-refaccion" class="btn btn-lg btn-primary ml-2">
            <i class="fa fa-cart-plus"></i>
                Agregar Refacción
            </button>
            <button id="btn-actualizar-operaciones" class="btn btn-lg btn-primary ml-2">
                <i class="fas fa-sync"></i>
                Actualizar Operaciones
            </button>
            <button id="modal-presupuesto" class="btn btn-lg btn-primary ml-2">
                <i class="fas fa-clipboard-list"></i>
                Presupuesto
            </button>
            <button id="solicitar-firma-tecnico" class="btn btn-lg btn-primary ml-2">
                <i class="fa fa-signature"></i>
                Solicitar Firma Técnico
            </button>
            <button id="btn-finalizar" class="btn btn-lg btn-success ml-2">
                <i class="fa fa-check text-success"></i>
                Orden Completa
            </button>
        @else
            <div class="ml-auto"></div>
            <button id="modal-presupuesto" class="btn btn-lg btn-primary ml-2">
                <i class="fas fa-clipboard-list"></i>
                Presupuesto
            </button>
            <button id="btn-abrir-orden" class="btn btn-lg btn-primary ml-2">
                <i class="fa fa-door-open"></i>
                Reabrir Orden y Modificar
            </button>
        @endif
    </div> 
    
    <h3>
        <i class="fa fa-list"></i> Productos de la Orden
    </h3>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_carrito" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>No Pieza</th>
                            <th>Descripcion</th>
                            <th>Refacciones</th>
                            <th>Total Refacciones</th>
                            <th>Costo Mano de Obra</th>
                            <th>Total</th>
                            <th>Cantidad</th>
                            <th>Unidad</th>
                            <th>-</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        let role = '{{ $role }}'
        let administrador = role == 'administrador'? true : false

        let estatus_venta_id = $('#estatus_venta_id').val();

        $('#tbl_carrito th').on('click', function (e) {
            e.preventDefault();
    
            // Get the column API object
            // var mano_obra = tabla_carrito.column(5);
            var total = tabla_carrito.column(6);
    
            // Toggle the visibility
            // mano_obra.visible(!mano_obra.visible());
            total.visible(!total.visible());
        });

        var tabla_carrito = $('#tbl_carrito').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            ajax: {
                url: PATH_API + "api/detalles/" + {{ $numero_orden }},
                type: 'GET',
                dataSrc: function(json){

                    return json.aaData.sort(function(a, b){

                        // Si no es servicio
                        if(a.servicio == false)
                            first = a.venta_servicio_id
                        else
                            first = a.id

                        // Si no es servicio
                        if(b.servicio == false)
                            second = b.venta_servicio_id
                        else
                            second = b.id

                        return second - first
                    })
                }
            },
            processing: true,
            serverSide: true,
            paging:false,
            bFilter: false,
            bSort: false,
            columns: [{
                    'data': 'id'
                },
                {
                    'data': function(data) {

                        let text = data.no_identificacion
                        // text += data.reemplazo_no_identificacion? `<br><small class="text-danger">REEMPLAZO: ${data.reemplazo_no_identificacion}</small>` : ''
                        text += `<br>`
                        
                        if(data.presupuesto)
                            text += `<i class="fas fa-clipboard-list mr-2" title="Presupuesto"></i>`

                        if(data.preventivo)
                            text += `<i class="fa fa-bandage mr-2" title="Preventivo"></i>`

                        if(data.bodyshop)
                            text += `<i class="fa fa-store-alt mr-2" title="Bodyshop"></i>`
                        
                        if(data.afecta_paquete)
                            text += `<i class="fas fa-dolly-flatbed mr-2" title="Afecta Paquete"></i>`

                        if(data.afecta_mano_obra)
                            text += `<i class="fas fa-hand-paper mr-2" title="Afecta Mano de Obra"></i>`

                        if(data.detalles)
                            text += `<i class="fa fa-user-plus mr-2" title="Detalles"></i>`

                        return text
                    }
                },
                {
                    data: function(data){
                        let ubicacion = data.ubicacion? data.ubicacion : ''
                        let date = (new Date(data.created_at)).toLocaleString('es-MX');

                        if(data.servicio == true){
                            return `${data.descripcion}<br><i class="fa fa-clock"></i> ${date}`    
                        }

                        return `${data.descripcion}<br>${ubicacion} <i class="fa fa-clock ml-2"></i> ${date}`
                    }
                },
                {
                    data: function(data) {
                        mxn = Intl.NumberFormat('es-MX', {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                            currency: 'MXN',
                        })

                        return data.servicio == false ? `$ ${mxn.format(data.valor_unitario)}` : `$ ${mxn.format(data.refacciones)}`
                    },
                    className: 'text-right text-nowrap'
                },
                {
                    data: function(data) {
                        mxn = Intl.NumberFormat('es-MX', {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                            currency: 'MXN',
                        })

                        return data.servicio == false? `$ ${mxn.format(data.valor_unitario * data.cantidad)}` : `$ ${mxn.format(data.refacciones)}`
                    },
                    className: 'text-right text-nowrap'
                },
                {
                    data: function(data) {
                        mxn = Intl.NumberFormat('es-MX', {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                            currency: 'MXN',
                        })

                        return data.servicio == true? `$ ${mxn.format(data.costo_mano_obra)}` : `$ ${mxn.format(data.costo_mano_obra)}`
                    },
                    className: 'text-right text-nowrap',
                },
                {
                    data: function(data) {
                        mxn = Intl.NumberFormat('es-MX', {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                            currency: 'MXN',
                        })

                        
                        return data.servicio == false ? `$ ${mxn.format((parseFloat(data.valor_unitario) * parseFloat(data.cantidad)) + parseFloat(data.costo_mano_obra))}` : `$ ${mxn.format(parseFloat(data.costo_mano_obra) + parseFloat(data.refacciones))}`
                    },
                    className: 'text-right text-nowrap',
                    visible: false,
                },
                {
                    data: function(data) {
                        return data.cantidad
                    },
                    className: 'text-center'
                },
                {
                    'data': function(data) {
                        return data.servicio == false? 'PIEZA' : '--'
                    }
                },
                {
                    'data': function(data) {

                        let orden_completa = $('#orden_completa').val()
                        let orden_incompleta = $('#orden_incompleta').val()

                        if(orden_completa == '1' || orden_incompleta == '1')
                            return '--'

                        if(data.servicio == true)
                            return `<button type="button" class="btn-agregar-refaccion-servicio btn btn-success"
                                data-id="${data.id}">
                                <i class="fa fa-cart-plus"></i>
                            </button>`

                        // if(data.servicio == false){
                        //     return `<button type="button" class="btn-edit btn btn-primary" 
                        //         data-id="${data.id}" 
                        //         data-no-identificacion="${data.no_identificacion}" 
                        //         data-cantidad="${data.cantidad}" 
                        //         data-descripcion-producto="${data.descripcion}"
                        //         data-valor-unitario="${data.valor_unitario}"
                        //         data-costo-promedio="${data.costo_promedio}"
                        //         data-producto-id="${data.producto_id}"
                        //         data-afecta-paquete="${data.afecta_paquete}"
                        //         data-preventivo="${data.preventivo}"
                        //         data-afecta-mano-obra="${data.afecta_mano_obra}"
                        //         data-presupuesto="${data.presupuesto}"
                        //         data-core="${data.core}"
                        //         >
                        //             <i class="fa fa-edit"></i>
                        //         </button>
                        //         <button type="button" class="btn-borrar btn btn-danger" data-id="${data.id}">
                        //             <i class="fas fa-trash"></i>
                        //         </button>`
                        // }

                        if(data.servicio == false){
                            return `
                            <button type="button" class="btn-borrar btn btn-danger" data-id="${data.id}">
                                <i class="fas fa-trash"></i>
                            </button>`;
                        }

                        return '--';
                    }
                }
            ],
            createdRow: function(row, data, dataIndex) {
                // if(data.producto_id && data.stock > 0){
                //     $(row).find('td:eq(5)').css('background-color', '#8cdd8c');
                // }
                // else if(data.producto_id && data.stock < 1){
                //     $(row).find('td:eq(5)').css('background-color', '#db524d80');
                // }
                // else{
                //     $(row).find('td:eq(5)').css('background-color', '#2f71d480');
                // }

                if(data.servicio == false && data.venta_servicio_id){
                    $(row).find('td:lt(8)').css('background-color', '#BAD7E9')
                }

                if(data.servicio == true)
                    $(row).find('td:lt(8)').css('background-color', '#F8EAD8')
            },
            fnInitComplete: function(oSettings, json){
                // console.log(oSettings, json)

                let costo_mano_obra = 0
                let costo_refacciones = 0
                let costo_refacciones_todas = 0
                let costo_refacciones_afectan = 0
                let total = 0
                let total_iva = 0
                
                costo_mano_obra = json.aaData.filter(operacion => operacion.servicio == false).reduce((costo_mano_obra, operacion) => costo_mano_obra + parseFloat(operacion.costo_mano_obra), costo_mano_obra)
                costo_mano_obra = json.aaData.filter(refaccion => refaccion.servicio == true).reduce((costo_mano_obra, refaccion) => costo_mano_obra + parseFloat(refaccion.costo_mano_obra), costo_mano_obra)

                costo_refacciones = json.aaData.filter(operacion => operacion.servicio == true).reduce((costo_refacciones, operacion) => costo_refacciones + parseFloat(operacion.refacciones), costo_refacciones)
                costo_refacciones = json.aaData
                    .filter(refaccion => refaccion.servicio == false)
                    .filter(refaccion => refaccion.afecta_mano_obra == false)
                    .filter(refaccion => refaccion.afecta_paquete == false)
                    .reduce((costo_refacciones, refaccion) => costo_refacciones + (parseFloat(refaccion.valor_unitario) * parseFloat(refaccion.cantidad)), costo_refacciones)

                costo_refacciones_todas = json.aaData.filter(operacion => operacion.servicio == true).reduce((costo_refacciones, operacion) => costo_refacciones + parseFloat(operacion.refacciones), costo_refacciones_todas)
                costo_refacciones_todas = json.aaData
                    .filter(refaccion => refaccion.servicio == false)
                    .reduce((costo_refacciones_todas, refaccion) => costo_refacciones_todas + (parseFloat(refaccion.valor_unitario) * parseFloat(refaccion.cantidad)), costo_refacciones_todas)

                costo_refacciones_afectan = json.aaData
                    .filter(refaccion => refaccion.servicio == false)
                    .filter(refaccion => refaccion.afecta_mano_obra == true || refaccion.afecta_paquete == true)
                    .reduce((costo_refacciones_afectan, refaccion) => costo_refacciones_afectan + (parseFloat(refaccion.valor_unitario) * parseFloat(refaccion.cantidad)), costo_refacciones_afectan)

                total = costo_mano_obra + costo_refacciones
                total_iva = (costo_mano_obra + costo_refacciones) * 1.16

                mxn = Intl.NumberFormat('es-MX', {
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2,
                    currency: 'MXN',
                })

                $('#tbl_carrito tr:last').after(`<tr>
                    <td colspan="2" class="font-weight-bold">Subtotal:</td>
                    <td class="text-right" colspan="3">
                        Total de Refacciones: $ ${mxn.format(costo_refacciones_todas)} <br>
                        Afectan Mano de Obra y Paquete: - $ ${mxn.format(costo_refacciones_afectan)}<br>
                        Total Refacciones a cobrar: $ ${mxn.format(costo_refacciones)}
                    </td>
                    <td class="text-right text-nowrap" colspan="1">$ ${mxn.format(costo_mano_obra)}</td>
                    <td class="text-right text-nowrap" colspan="1">$ ${mxn.format(total)}</td>
                    <td colspan="3"></td>
                    </tr>`
                )
                $('#tbl_carrito tr:last').after(`<tr>
                    <td colspan="2" class="font-weight-bold">Total con IVA:</td>
                    <td class="text-right text-nowrap" colspan="5">$ ${mxn.format(total_iva)}</td>
                    <td colspan="3"></td>
                    </tr>`
                )
            }
        });

        var tabla_presupuesto = $('#tbl_presupuesto').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            ajax: {
                url: PATH_API + "api/detalles/presupuesto/" + {{ $numero_orden }},
                type: 'GET',
            },
            processing: true,
            serverSide: true,
            paging:false,
            bFilter: false,
            bSort: false,
            columns: [{
                    data: 'id'
                },
                {
                    data: 'num_pieza'
                },
                {
                    data: 'descripcion'
                },
                {
                    data: function(data){
                        mxn = Intl.NumberFormat('es-MX', {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                            currency: 'MXN',
                        })

                        return mxn.format(data.precio_unitario)
                    },
                    className: 'text-right text-nowrap',
                },
                {
                    data: function(data){
                        mxn = Intl.NumberFormat('es-MX', {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                            currency: 'MXN',
                        })

                        return mxn.format(data.costo_refaccion)
                    },
                    className: 'text-right text-nowrap',
                },
                {
                    data: function(data){
                        mxn = Intl.NumberFormat('es-MX', {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                            currency: 'MXN',
                        })

                        return mxn.format(data.costo_mano_obra)
                    },
                    className: 'text-right text-nowrap',
                },
                {
                    data: function(data){
                        mxn = Intl.NumberFormat('es-MX', {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                            currency: 'MXN',
                        })

                        return mxn.format(data.costo_total_sin_iva)
                    },
                    className: 'text-right text-nowrap',
                },
                {
                    data: 'cantidad'
                },
            ]
        });
        
        $("#tbl_carrito").on("click", ".btn-borrar", function() {
            var id = $(this).data('id')
            var id_index_requisicion = $(this).data('id_index_requisicion')
            borrar(id, id_index_requisicion)
        });

        function modalDetalle(_this) {
            $("#producto_id").val($(_this).data('product_id'));
            $("#valor_unitario").val($(_this).data('valor_unitario'));
            $("#valor_unitario_nuevo").val($(_this).data('valor_unitario'));
            $("#total").val($(_this).data('valor_unitario'));
            $("#costo_promedio").val($(_this).data('costo_promedio'));
            $("#no_identificacion").val($(_this).data('no_identificacion'));
            $("#cantidad_almacen_primario").val($(_this).data('cantidad_almacen_primario'));
            var producto_name = $(_this).data('product_name');
            $("#cantidad").val(1)

            let settings = tabla_carrito.ajax.json();
            
            let preventivo = settings.aaData.some( element => typeof element.preventivo !== 'undefined' && element.preventivo == true )
            let generico = settings.aaData.some( element => typeof element.servicio != 'undefined' && element.servicio == true && element.no_identificacion == 'GEN.' )

            if(preventivo == false && generico == true){
                $('#afecta_paquete').prop('checked', false)
                $('#afecta_paquete').attr('disabled', true)

                $('#afecta_mano_obra').prop('checked', false)
                $('#afecta_mano_obra').attr('disabled', false)
            }

            if(preventivo == true && generico == false){
                $('#afecta_mano_obra').prop('checked', false)
                $('#afecta_mano_obra').attr('disabled', true)

                $('#afecta_paquete').prop('checked', false)
                $('#afecta_paquete').attr('disabled', false)
            }

            if(preventivo == true && generico == true){
                $('#afecta_mano_obra').prop('checked', false)
                $('#afecta_mano_obra').attr('disabled', false)

                $('#afecta_paquete').prop('checked', false)
                $('#afecta_paquete').attr('disabled', false)
            }

            $("#title_modal").text(producto_name);
            $("#modal-producto-detalle").modal('show');
        }

        $('#btn-agregar-refaccion').click(function(){
            $('#modal-producto-detalle').modal('show')
        })

        $("#tbl_carrito").on("click", ".btn-agregar-refaccion-servicio", function() {
            let id = $(this).data('id')

            $('#venta_servicio_id').val(id)

            $('#modal-producto-detalle').modal('show')
        });

        $("#modal-producto-detalle").on('hidden.bs.modal', function () {
            $('#venta_servicio_id').val('')
            $('#producto_id').val('')
            $('#cantidad').val(1)
            $('.search').val(null).trigger('change');
        });

        $('#modal-presupuesto').click(function(){
            $("#modal-consultar-presupuesto").modal('show')
        })

        
        $('#btn-modal-agregar').click(function(id) {
            toastr.info("Añadiendo producto al carro de compras");

            let producto_id = $("#producto_id").val()
            let folio_id = $('#folio_id').val()
            let venta_id = $("#venta_id").val()
            let cantidad = $("#cantidad").val()
            let venta_servicio_id = $('#venta_servicio_id').val()
            // let afecta_paquete = $("#afecta_paquete").is(':checked')? 1 : 0
            // let afecta_mano_obra = $("#afecta_mano_obra").is(':checked')? 1 : 0
            let core = $('#core').val()
            
            // let precio_id = $("#precio_id").val()
            let valor_unitario = $("#valor_unitario_nuevo").val()

            if(producto_id == ""){
                toastr.error('Seleccionar Refacción')
                return;
            }

            // if(precio_id == "" || precio_id < 1){
            //     toastr.error("Seleccionar el precio")
            //     return;
            // }

            if(cantidad == ""){
                toastr.error('Ingresar cantidad')
            }

            $.isLoading({
                text: "Añadiendo producto al carro de compras...."
            });

            let precio_manual = 0

            $.ajax({
                url: PATH_API + `api/detalles/refaccion`,
                method: 'POST',
                dataType: 'json',
                data: {
                    producto_id,
                    venta_id,
                    folio_id,
                    valor_unitario,
                    cantidad,
                    precio_manual,
                    venta_servicio_id,
                    // afecta_paquete,
                    // afecta_mano_obra,
                    core,
                },
            })
            .done(response => {
                $("#modal-producto-detalle").modal('hide')
                tabla_carrito.ajax.reload()
            })
            .fail((jqXHR) => {
                if(jqXHR.status === 400){
                    toastr.error(jqXHR.getResponseHeader('X-Message'));
                }
            })
            .always(() => {
                $.isLoading("hide");
            })
        })

        function borrar(id, id_index_requisicion) {
            // return console.log(id,id_index_requisicion);
            utils.displayWarningDialog("¿Desea quitar el producto?", "warning", function(data) {
                if (data.value) {
                    $.ajax({
                        url: PATH_API + `api/detalles/refaccion/${id}`,
                        method: 'DELETE',
                    })
                    .done(response => {
                        tabla_carrito.ajax.reload()
                        $.isLoading('hide')
                    }) 
                    .always(() => {
                        $.isLoading("hide");
                    })
                }
            }, true)
        }

        $('.search').select2({
            placeholder: 'Seleccionar Refacción',
            minimumInputLength: 3,
            maximumInputLength: 20,
            language: "es",
            delay: 500,
            ajax: {
                url: `${PATH_API}api/detalles/refacciones`,
                data: function (params) {
                    let query = {
                        no_identificacion: params.term,
                    }
                    
                    return query
                },
                processResults: function (data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'

                    let items = data.map(item => { return { id: item.id, text: `${item.no_identificacion} => ${item.valor_unitario}`, costo_ford: item.costo_ford, costo_promedio: item.costo_promedio, valor_unitario: item.valor_unitario, cantidad: 1 } })

                    return {
                        results: items? items : {}
                    }
                }
            }
        })

        $('.search').on('select2:select', function(event){
            let data = event.params.data

            $("#producto_id").val(data.id);
            $("#valor_unitario").val(data.valor_unitario);
            $("#valor_unitario_nuevo").val(data.valor_unitario);
            $("#total").val(data.valor_unitario);
            $("#costo_promedio").val(data.costo_promedio);
            $("#no_identificacion").val(data.no_identificacion);
            $("#cantidad_almacen_primario").val(data.cantidad);
            $("#precio_id").val(5) // Precio FORD
        })

        const buscarproducto = () => {
            let object = {};
            if (document.getElementById('descripcion').value !== '') {
                object['descripcion'] = document.getElementById('descripcion').value;
            } else if (document.getElementById('no_identificacion').value !== '') {
                object['no_identificacion'] = document.getElementById('no_identificacion').value;
            }

            let parametros = $.param(object);

            let almacen = $('#almacenes').val()
            let path = almacen == 1? PATH_API + 'api/productos/listadoStock?'+ parametros : PATH_API_SANJUAN + "api/productos/listadoStock?" + parametros

            $('#tbl_productos').DataTable().ajax.url(path).load()
        }

        $("#tbl_carrito").on("click", ".btn-edit", function() {
            $('#modal-editar-producto-detalle').modal('show')
            let data = $(this).data()

            $('#es-total-edit').prop( "checked", false )
            $('#no-identificacion-edit').val(data.noIdentificacion)
            $('#descripcion-edit').val(data.descripcionProducto)
            $('#cantidad-edit').val(data.cantidad)
            $('#almacen-id-edit').val(data.almacenId)
            $('#precio-id-edit').val(data.precioId)
            $('#old-valor-unitario-edit').val(data.valorUnitario)
            $('#valor-unitario-edit').val(parseFloat(data.valorUnitario).toFixed(2))
            $('#total-edit').val(parseFloat(data.valorUnitario * data.cantidad).toFixed(2))
            $('#core-edit').val(data.core)
            $('#core-edit-old').val(data.core)
            $('#costo-promedio-edit').val(data.costoPromedio)
            $('#id-edit').val(data.id)
            $('#producto-id-edit').val(data.productoId)
            $('#venta-id-edit').val(data.ventaId)
            $('#presupuesto-edit').val(data.presupuesto == true ? 1 : 0)

            let settings = tabla_carrito.ajax.json()
            let preventivo = settings.aaData.some( element => typeof element.preventivo !== 'undefined' && element.preventivo == true )
            let generico = settings.aaData.some( element => typeof element.servicio != 'undefined' && element.servicio == true && element.no_identificacion == 'GEN.' )
            
            if(preventivo == true && generico == false){
                $('#afecta_paquete_modificar').prop('checked', data.afectaPaquete)
                $('#afecta_paquete_modificar').attr('disabled', false)

                $('#afecta_mano_obra_modificar').prop('checked', data.afectaManoObra)
                $('#afecta_mano_obra_modificar').attr('disabled', true)
            } 

            if(preventivo == false && generico == true) {
                $('#afecta_mano_obra_modificar').prop('checked', data.afectaManoObra)
                $('#afecta_mano_obra_modificar').attr('disabled', false)

                $('#afecta_paquete_modificar').prop('checked', data.afectaPaquete)
                $('#afecta_paquete_modificar').attr('disabled', true)
            }

            if(preventivo == true && generico == true) {
                $('#afecta_mano_obra_modificar').prop('checked', data.afectaManoObra)
                $('#afecta_mano_obra_modificar').attr('disabled', false)

                $('#afecta_paquete_modificar').prop('checked', data.afectaPaquete)
                $('#afecta_paquete_modificar').attr('disabled', false)
            } 

            $('#core-edit').attr('readonly', true);
            $('#tiene-core-edit').prop('checked', false);
        })

        $("#btn-modificar").click(function() {
            let data = {
                producto_id: $('#producto-id-edit').val(),
                cantidad: $('#cantidad-edit').val(),
                valor_unitario: $('#valor-unitario-edit').val(),
                precio_manual: 0,
                afecta_paquete: $("#afecta_paquete_modificar").is(':checked')? 1 : 0,
                afecta_mano_obra: $("#afecta_mano_obra_modificar").is(':checked')? 1 : 0,
                core: $('#core-edit').val(),
            }

            let id = $('#id-edit').val()
            toastr.info("Actualizando inventario espere un momento....");

            $.isLoading('hide')
            
            $.ajax({
                url: PATH_API + `api/detalles/refaccion/${id}`,
                method: 'PATCH',
                data: data,
            })
            .done(response => {
                toastr.success('Se modifico el producto correctamente.')
                $('#modal-editar-producto-detalle').modal('hide')
                tabla_carrito.ajax.reload()
            })
            .fail( error => {
                if(error.status === 400){
                    toastr.error(error.getResponseHeader('X-Message'));
                }
            })
            .always(() => {
                $.isLoading('hide')
            })
        })

        $('#btn-actualizar-operaciones').click(function(){

            toastr.info("Actualizando Operaciones...");

            let numero_orden = $('#orden').val()

            $.ajax({
                url: PATH_API + `api/detalles/recuperar-operaciones/${numero_orden}`,
                method: 'GET',
            })
            .done(response => {
                toastr.success('Actualización de operaciones exitosa.')
                tabla_carrito.ajax.reload()
            })
            .fail( error => {
                toastr.error('Ocurrio al actualizar las operaciones.')
            })
        })

        $('#btn-finalizar').click(function(){
            let id = $('#venta_id').val()

            toastr.info("Completando...");

            $.ajax({
                url: PATH_API + `api/detalles/venta/${id}/completar`,
                dataType: 'json',
                method: 'POST',
            })
            .done(response => {
                toastr.success('Se cambio el estatus de la orden.')
                window.location.reload()
            })
            .fail( error => {
                try{
                    toastr.error(error.responseJSON.message)
                } catch(e){
                    toastr.error('Ocurrio un error al cambiar el estatus.')
                }
                $.isLoading('hide')
            })
            .always(() => {
                $.isLoading('hide')
            })
        })

        $('#btn-abrir-orden').click(function(){

            let id = $('#venta_id').val()
            let folio_id = $('#folio_id').val()

            toastr.info('Reabriendo orden para su modificación')

            $.post(PATH_API + `api/ventas/${id}/abrir-orden-completa`)
            .done(response => {
                toastr.success(response.message)
                window.location = PATH + `/refacciones/salidas/detalleVentaServiceExcellent/${folio_id}`
            })
            .fail(error => {
                try{
                    toastr.error(error.responseJSON.message)
                } catch(e){
                    toastr.error('Ocurrio un error al intentar abrir la orden')
                }
                
                console.log(error)
            })
        })

        $('#solicitar-firma-tecnico').click(function(){
            let numero_orden = $('#orden').val()

            toastr.info('Solicitando la firma por el técnico')

            $.post(PATH_API + `api/ventas/${numero_orden}/habilitar-firma-requisicion`)
            .done(response => {
                toastr.success(response.message)
                $('#solicitar-firma-tecnico').prop('disabled', true)
            })
            .fail(error => {
                toastr.error('Ocurrio un error al solicitar la firma de la requisicion')
            })
        })

        $("#precio_id").change(calcularPrecio).keyup(calcularPrecio)

        $("#precio_manual").change(calcularPrecio).keyup(calcularPrecio)
        
        $("#cantidad").change(calcularPrecio).keyup(calcularPrecio)

        $("#valor_unitario_nuevo").change(calcularPrecio).keyup(calcularPrecio)

        function calcularPrecio(){
            let porcentaje = $("#precio_id option:selected").data()
            let precio_id = $("#precio_id").val()

            // if(precio_id == "" || precio_id < 1){
            //     toastr.error('Seleccione el precio.')
            //     return;
            // }

            // if(precio_id == 100){
            //     $("#precio_manual").attr("disabled", false)
            //     porcentaje.porcentaje = $('#precio_manual').val() / 100
            // } else {
            //     $("#precio_manual").attr("disabled", true)
            //     $("#precio_manual").val(0)
            // }

            let valor_unitario = $("#valor_unitario_nuevo").val()
            let cantidad = $("#cantidad").val()

            // if(precio_id == 1){
            //     $("#total").val($('#valor_unitario_nuevo').val() * cantidad)
            //     return;
            // }

            // $('#valor_unitario_nuevo').val(
            //     parseFloat(valor_unitario).toFixed(2)
            // )
            
            $("#total").val(
                parseFloat(valor_unitario * cantidad).toFixed(2)
            )
        }

        $('#cantidad-edit').keyup(function (){
            calcularPrecioEditar()
        })

        $('#precio_id_edit').change(function (){
            calcularPrecioEditar()
        })

        $("#precio_manual_edit").keyup(function(){
            calcularPrecioEditar()
        })

        $("#valor-unitario-edit").keyup(function(){
            calcularPrecioEditar()
        })

        function calcularPrecioEditar(){
            let porcentaje = $("#precio_id_edit option:selected").data()
            // let precio_id = $("#precio_id_edit").val()
            let isTotalChecked = $("#es-total-edit").is(':checked')? 1 : 0

            // if(precio_id == "" || precio_id < 1){
            //     toastr.error('Seleccione el precio.')
            //     return;
            // }

            // if(precio_id == 100){
            //     $("#precio_manual_edit").attr("disabled", false)
            //     porcentaje.porcentaje = $('#precio_manual_edit').val() / 100
            // } else {
            //     $("#precio_manual_edit").attr("disabled", true)
            // }

            let valor_unitario = $("#valor-unitario-edit").val()
            let cantidad = $("#cantidad-edit").val() > 0? $("#cantidad-edit").val() : 1

            // if(! isTotalChecked){
            //     $('#valor-unitario-edit').val($('#old-valor-unitario-edit').val())
            //     return;
            // }
            
            // if(precio_id == 1){
            //     parseFloat($('#valor-unitario-edit').val($('#old-valor-unitario-edit').val() * cantidad)).toFixed(2);
            //     return;
            // }

            // if(! isTotalChecked){
            //     $("#valor-unitario-edit").val(
            //         parseFloat(valor_unitario).toFixed(2)
            //     )
            //     return;
            // }

            // $("#valor-unitario-edit").val(
            //     parseFloat(valor_unitario).toFixed(2)
            // )

            $("#total-edit").val(
                parseFloat(valor_unitario * cantidad).toFixed(2)
            )
        }

        $('#tiene_core').change(function(){
            let isCheked = $(this).is(':checked')

            if(isCheked)
                $('#core').show()
            else{
                $('#core').hide()
                $('#core').val(0)
            }
        })

        $('#tiene-core-edit').change(function(){
            let isCheked = $(this).is(':checked')

            if(isCheked){
                $('#core-edit').attr('readonly', false);
            }
            else{
                $('#core-edit').attr('readonly', true)
                $('#core-edit').val($('#core-edit-old').val())
            }
        })
    })

</script>
@endsection

@section('modal')
    <div class="modal fade" id="modal-producto-detalle" data-toggle="modal" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold mt-0" id="title_modal">
                        <i class="fa fa-plus"></i>
                        Refacción
                    </h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="" class="w-100">Refacción</label>
                            <select name="search" id="search" class="search text-uppercase form-select mb-2" style="width:100%; margin-bottom:0.5rem;"></select>
                        </div>
                        <div class="col-md-12 mt-2">
                            <?php echo renderInputText("number", "cantidad", "Cantidad", 1); ?>
                        </div>
                        <input type="hidden" name="producto_id" id="producto_id">
                        <input type="hidden" name="venta_servicio_id" id="venta_servicio_id">
                        <div class="col-md-12">
                            <!-- <div class="form-group">
                                <label for="select">Aplicar precio</label>
                                <select name="precio_id" class="form-control" id="precio_id">
                                    @foreach ($cat_precios as $index => $precio)
                                    <option {{ $precio->id == 5? 'selected' : '' }} value="{{ $precio->id}}" data-porcentaje="{{ ($precio->precio_publico / 100) }}">
                                        {{ $precio->descripcion}} {{$precio->precio_publico}} %
                                    </option>
                                    @endforeach
                                    <option value="100">Manual</option>
                                </select>
                            </div> -->
                            
                            <input type="hidden" name="valor_unitario" id="valor_unitario">
                            <input type="hidden" name="costo_promedio" id="costo_promedio">
                            <input type="hidden" name="no_identificacion" id="no_identificacion">
                            <input type="hidden" name="cantidad_almacen_primario" id="cantidad_almacen_primario">
                            <div id='precio_id_error' class='invalid-feedback'></div>
                            
                            <?php echo renderInputText("number", "valor_unitario_nuevo", "Valor unitario", ''); ?>
                            <?php echo renderInputText("number", "total", "Total", ''); ?>

                            <!-- <label for="precio_manual">Porcentaje Manual %:</label>
                            <input id="precio_manual" placeholder="Porcentaje" name="precio_manual" type="number" class="form-control mb-3" value="0" min="0" max="100" step="1"> -->

                            <div class="form-group">
                                <input id="tiene_core" name="tiene_core" type="checkbox" value="1" />
                                <label class="mb-2 ml-2" for="core">Agregar Core</label>
                                <input id="core" class="form-control" name="core" value="0" type="number" style="display: none">
                            </div>
                        </div>
    
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button id="btn-modal-agregar" type="button" class="btn btn-primary"><i class="fas fa-shopping-cart"></i> Agregar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-editar-producto-detalle" data-toggle="modal" tabindex="-1" role="dialog"
        aria-labelledby="modalProductos" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="title_modal">
                        <i class="fa fa-edit"></i>
                        Editar Refacción
                    </h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>No. identificacion</label>
                                <input id="no-identificacion-edit" readonly type="text" class="form-control text-uppercase">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Descripcion</label>
                                <input id="descripcion-edit" type="text" readonly class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Cantidad</label>
                                <input id="cantidad-edit" type="number" class="form-control" min="1">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- <div class="col-md-12">
                            <div class="form-group">
                                <label for="select">Aplicar precio</label>
                                <select name="precio_id_edit" class="form-control" id="precio_id_edit">
                                    @foreach ($cat_precios as $index => $precio)
                                    <option {{ $index == 0? 'selected' : '' }} value="{{ $precio->id}}" data-porcentaje="{{ ($precio->precio_publico / 100) }}">
                                        {{ $precio->descripcion}} {{$precio->precio_publico}} %
                                    </option>
                                    @endforeach
                                    <option value="100">Manual</option>
                                </select>
                            </div>
                        </div> -->
                        <!--  <div class="col-md-12">
                            <?php echo renderInputText("number", "precio_manual_edit", "Precio Manual %", 0); ?>
                        </div> -->
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="es-total-edit">Valor Unitario</label>
                                {{-- <label for="es-total-edit" class="d-none">Precio - ¿Es el valor total?</label> --}}
                                <input id="es-total-edit" type="checkbox" value="1" class="d-none">
                                <input id="valor-unitario-edit" class="form-control" type="number">
                                <input id="old-valor-unitario-edit" class="form-control" type="hidden">
                                <input id="costo-promedio-edit" class="form-control" type="hidden">
                                <input id="presupuesto-edit" class="form-control" type="hidden">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Total</label>
                                <input id="total-edit" type="number" class="form-control" min="0">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="tiene-core-edit">Modificar Core</label>
                                <input id="tiene-core-edit" type="checkbox" value="1">
                                <input id="core-edit" class="form-control" type="number" readonly>
                                <input id="core-edit-old" class="form-control" type="hidden">
                            </div>
                        </div>
                        <input type="hidden" name="id" id="id-edit">
                        <input type="hidden" name="producto_id" id="producto-id-edit">
                        <input type="hidden" name="precio_id" id="precio-id-edit">
                        <input type="hidden" name="almacen_id" id="almacen-id-edit">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Cerrar
                    </button>
                    
                    <button id="btn-modificar" type="button" class="btn btn-primary">
                        <i class="fa fa-shopping-cart"></i> Modificar
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-editar-producto-presupuesto" data-toggle="modal" tabindex="-1" role="dialog"
        aria-labelledby="modalProductos" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal">
                        <i class="fas fa-clipboard-list mr-2" title="Presupuesto"></i>
                        Editar Producto Presupuesto
                    </h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>
                                    No. identificación 
                                    <input type="checkbox" name="nuevo-presupuesto" id="nuevo-presupuesto"> Agregar
                                </label>
                                <input id="no-identificacion-edit-presupuesto" readonly type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Descripción</label>
                                <input id="descripcion-edit-presupuesto" type="text" readonly class="form-control">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Cantidad</label>
                                <input id="cantidad-edit-presupuesto" type="text" readonly class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Precio de Venta</label>
                                <input id="valor-unitario-edit-presupuesto" class="form-control" type="number">
                                <input id="old-valor-unitario-edit-presupuesto" class="form-control" type="hidden">
                            </div>
                        </div>
                        <!-- <div class="col-md-3">
                            <div class="form-group">
                                <label>Costo Mano de Obra</label>
                                <input id="costo-mo-edit-presupuesto" class="form-control" type="number">
                            </div>
                        </div> -->
                        <input type="hidden" name="id" id="id-edit-presupuesto">
                        <input type="hidden" name="producto_id" id="producto-id-edit-presupuesto">
                    </div>
                    <fieldset class="border rounded p-4">
                        <legend class="d-inline-flex w-auto text-gray">Validación de Credenciales</legend>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Usuario:</label>
                                    <input id="usuario-presupuesto" class="form-control" type="text">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Contraseña:</label>
                                    <input id="password-presupuesto" class="form-control" type="password">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Cerrar
                    </button>
                    
                    <button id="btn-modificar-presupuesto" type="button" class="btn btn-primary">
                        <i class="fas fa-edit"></i> Modificar
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-eliminar-producto-presupuesto" data-toggle="modal" tabindex="-1" role="dialog"
        aria-labelledby="modalProductos" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal">
                        <i class="fas fa-trash mr-2" title="Presupuesto"></i>
                        Eliminar Producto de Presupuesto
                    </h5>
                </div>
                <div class="modal-body">
                    <fieldset class="border rounded p-4">
                        <legend class="d-inline-flex w-auto text-gray">Validación de Credenciales</legend>
                        <input type="hidden" name="id" id="id-elimina-presupuesto">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Usuario:</label>
                                    <input id="usuario-elimina-presupuesto" class="form-control" type="text">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Contraseña:</label>
                                    <input id="password-elimina-presupuesto" class="form-control" type="password">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Cerrar
                    </button>
                    
                    <button id="btn-eliminar-presupuesto" type="button" class="btn btn-danger">
                        <i class="fas fa-edit"></i> Eliminar
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-consultar-presupuesto" data-toggle="modal" tabindex="-1" role="dialog"
        aria-labelledby="modalProductos" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal">
                        <i class="fas fa-clipboard-list"></i>
                        Presupuesto
                    </h5>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered" id="tbl_presupuesto" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>No Pieza</th>
                                <th>Descripcion</th>
                                <th>Refacciones</th>
                                <th>Total Refacciones</th>
                                <th>Costo Mano de Obra</th>
                                <th>Total</th>
                                <th>Cantidad</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection