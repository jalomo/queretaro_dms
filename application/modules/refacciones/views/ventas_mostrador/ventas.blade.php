@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
	<h1 class="mt-4">{{ isset($titulo) ? $titulo : "" }}</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item active">{{ isset($titulo) ? $titulo : "" }}</li>
	</ol>
	<div class="row-fluid">
		<div class="col-md-12 text-right">
			<a class="btn btn-primary" href="{{ base_url('refacciones/salidas/ventasMostrador') }}">
				<i class="fa fa-arrow-left"></i> Regresar</a>
		</div>
		<div class="col-md-12 mt-3">
			<form id="frm-producto">
				<div style="border:2px solid #ccc; background-color:#f9f9f9; padding:10px; border-radius:6px; margin-bottom:10px;">
					<h4>Datos del producto</h4>
					<div class="row">
						<div class="col-md-6">
							<?php echo renderInputText("text", "descripcion", "Descripcion", isset($producto->descripcion) ? $producto->descripcion : '', true); ?>
							<input type="hidden" name="producto_id" id="producto_id" value="{{ $producto->id}}">
						</div>
						<div class="col-md-3">
							<?php echo renderInputText("text", "unidad", "Unidad", isset($producto->unidad) ? $producto->unidad : '', true); ?>
						</div>
						<div class="col-md-3">
							<?php echo renderInputText("text", "no_identificacion", "No de producto", isset($producto->no_identificacion) ? $producto->no_identificacion : '', true); ?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<?php echo renderInputText("text", "ubicacion_producto", "Ubicación producto", isset($producto->ubicacion->nombre) ? $producto->ubicacion->nombre : '', true); ?>
						</div>
						<div class="col-md-3">
							<?php echo renderInputText("number", "existencia", "Existencia total", isset($producto->desglose_producto->cantidad_actual) ? $producto->desglose_producto->cantidad_actual : '', true); ?>
						</div>
						<div class="col-md-3">
							<?php echo renderInputText("number", "existencia_almacen_principal", "Existencia Almacen principal", isset($producto->desglose_producto->cantidad_almacen_primario) ? $producto->desglose_producto->cantidad_almacen_primario : '', true); ?>
						</div>
						<div class="col-md-3">
							<?php echo renderInputText("number", "existencia_almacen_secundario", "Existencia Almacen secundario", isset($producto->desglose_producto->cantidad_almacen_secundario) ? $producto->desglose_producto->cantidad_almacen_secundario : '', true); ?>
						</div>
					</div>
					<div class="row" style="display:none">
						<div class="col-md-6">
							<?php echo renderInputText("text", "nombre_taller", "Taller", isset($producto->taller) ? $producto->taller->nombre : '', true); ?>
						</div>
						<div class="col-md-6">
							<?php echo renderInputText("text", "ubicacion", "Ubicación taller", isset($producto->taller) ? $producto->taller->ubicacion : '', true); ?>
						</div>
					</div>
				</div>
				<h4 class="mt-3 ">Datos para la venta</h4>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="select">Tipo cliente</label>
							<select name="tipo_cliente_id" class="form-control " id="tipo_cliente_id">
								<option value=""> Seleccionar</option>
								@foreach ($cat_tipo_clientes as $tipo)
								<option value="{{ $tipo->id}}"> {{ $tipo->nombre}} </option>
								@endforeach
							</select>
							<div id='tipo_cliente_id_error' class='invalid-feedback'></div>
						</div>
					</div>
					<div class="col-md-6 form-registrar-cliente-esporadico">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Cliente</label>
									<input type="text" class="form-control" readonly="readonly" id="nombre_cliente_esporadico" />
								</div>
							</div>
							<input type="hidden" readonly="readonly" id="cliente_id" name="cliente_id" style="display:none" />
						</div>
					</div>
					<div class="col-md-6 cliente-registrado">
						<div class="row">
							<div class="col-md-10">
								<div class="form-group">
									<label for="select">Cliente</label>
									<select name="cliente_id" class="form-control select_cliente" id="cliente_id">
									</select>
									<div id='cliente_id_error' class='invalid-feedback'></div>
								</div>
							</div>
							<div class="col-md-2 mt-1">
								<button type="button" onclick="openModalDetalleCliente()" class="btn btn-primary mt-4"><i class="fa fa-info"></i></button>
							</div>
						</div>
					</div>
				</div>

				<br>
				<div class="row">
					<div class="col-md-6">
						<?php echo renderInputText("number", "precio_factura", "Valor unitario", isset($producto->valor_unitario) ? $producto->valor_unitario : '', false); ?>
					</div>
					<div class="col-md-6">
						<?php echo renderInputText("number", "cantidad", "Cantidad", 0); ?>
					</div>
				</div>
				<div class="form-group d-none">
					<label for="select">Aplicar precio</label>
					<select name="precio_id" class="form-control" id="precio_id">
						@foreach ($cat_precios as $precio)
						@if (isset($producto->precio_id) && $producto->precio_id == $precio->id)
						<option selected value="{{ $precio->id}}"> {{ $precio->descripcion}} </option>
						@else
						<option value="{{ $precio->id}}"> {{ $precio->descripcion}} - {{ $precio->precio_publico}}% </option>
						@endif
						@endforeach
					</select>
					<div id='precio_id_error' class='invalid-feedback'></div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>* Seleccionar tipo de precio <span class="porcentaje_publico"></span></label>
							<select name="tipo_precio" onchange="changeTipoPrecio(this)" id="tipo_precio" class="form-control">
								<option value="">Seleccion un tipo de precio</option>
								<option value="1" data-porcentaje="0.6">Público (40%)</option>
								<option value="2" data-porcentaje="0.9">Costo + 10 (10%)</option>
								<option value="4" data-porcentaje="0.92">Costo+ 8 (8%)</option>
								<option value="5" data-porcentaje="0.8">Costo + 20 (20%)</option>
								<option value="100">Manual</option>
							</select>
						</div>
					</div>
					<div class="col-md-6 precio_costo" style="display:none">
						<div class="form-group">
							<label class="label_precio_costo">Precio costo</label>
							<input type="text" name="precio_costo" id="precio_costo" class="form-control" value="" />
						</div>
					</div>
					<div class="col-md-6 precio_manual" style="display:none">
						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
									<label>Porcentaje Manual</label>
									<input type="number" name="porcentaje_manual" id="porcentaje_manual" class="form-control" value="" />
								</div>
								<div class="col-md-6">
									<label>Precio</label>
									<input type="text" name="precio_costo_manual" id="precio_costo_manual" class="form-control" />
								</div>
							</div>
						</div>
					</div>
				</div>

				<input type="hidden" id="status" name="status" value="2">
				<input type="hidden" id="vendedor_id" name="vendedor_id" value="{{ $vendedor_id }}">
				<input type="hidden" id="almacen_id"  value="{{ $almacen_id }}"/>
				<input type="hidden" value="<?php echo isset($tipo_venta_id) ? $tipo_venta_id : false; ?>" name="tipo_venta_id" id="tipo_venta_id">

				<div class="row mt-5 mb-4">
					<div class="col-md-12 text-right">
						<button class="btn btn-primary col-md-4" id="agregar_vender" type="button"> <i class="fas fa-shopping-cart"></i> Continuar </button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection

@section('scripts')
<script src="{{ base_url('js/refacciones/salidas/ventas.js') }}"></script>
@endsection

@section('modal')
<div class="modal fade" id="modal-permiso-precio" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="title_modal">
					Permisos para asignar precios
				</h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<?php echo renderInputText("text", "usuario", "usuario", false); ?>
					</div>
					<div class="col-md-12">
						<?php echo renderInputText("password", "password", "password", false); ?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" id="btn_cerrar_permiso" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-secondary" id="btn_cerrar_porcentaje" onClick="cerrarmodales()" data-dismiss="modal">Cancelar</button>
				<button id="modal-permiso" type="button" class="btn btn-primary">
					<button id="btn-modal-permiso-porcentaje" type="button" class="btn btn-primary">
						<i class="fas fa-lock-open"></i> Continuar
					</button>

			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-agregar-usuario" data-backdrop="static" data-keyboard="false" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="modalagregarcliente" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="title_modal"> Agregar cliente</h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<form id="form-cliente" class="col-md-12">
						@include('ventas_mostrador/partial_alta_cliente')
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" id="btn_cerrar_permiso" data-dismiss="modal">Cancelar</button>
				<button id="btn-modal-permiso-porcentaje" onClick="guardarCliente()" type="button" class="btn btn-primary">
					<i class="fas fa-lock-open"></i> Guardar
				</button>

			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-detalle-cliente" data-backdrop="static" data-keyboard="false" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="modalagregarcliente" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="title_modal"> Detalle cliente</h5>
			</div>
			<div class="modal-body">
				<div class="row" style="font-size:16px; color:#323232">
					<div class="col-12 mt-2"><b>Número cliente: </b><span class="detalle-numero_cliente"></span></div>
					<div class="col-12 mt-2"><b>Nombre cliente: </b><span class="detalle-nombre_cliente"></span></div>
					<div class="col-12 mt-2"><b>RFC: </b><span class="detalle-rfc"></span></div>
					<div class="col-12 mt-2"><b>Aplica crédito: </b><span class="detalle-aplica_credito"></span></div>
					<div class="col-12 mt-2"><b>Plazo crédito: </b><span class="detalle-plazo_credito"></span></div>
					<div class="col-12 mt-2"><b>Limite crédito: </b><span class="detalle-limite_credito"></span></div>
					<div class="col-12 mt-2"><b>Crédito actual: </b><span class="detalle-credito_actual"></span></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" id="btn_cerrar_permiso" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
@endsection