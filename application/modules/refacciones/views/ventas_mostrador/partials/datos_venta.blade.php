    <form id="form-venta">
        <input type="hidden" class="form-control" value="{{ isset($tipo_venta_id) ? $tipo_venta_id : '' }}" name="tipo_venta_id" id="tipo_venta_id">
        <input type="hidden" class="form-control" value="{{ isset($venta->cliente_id) ? $venta->cliente_id  : '' }}" id="cliente_id_">
        <p class="bold"><b>Buscar y seleccionar el cliente al cual asignar la venta</b></p>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Número cliente</label>
                    <input type="text" id="numero_cliente_" value="{{ $cliente->numero_cliente ? $cliente->numero_cliente : '' }}" class="form-control buscar_enter" />
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>RFC</label>
                    <input type="text" id="rfc_" class="form-control buscar_enter" />
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Nombres</label>
                    <input type="text" id="nombre_" class="form-control buscar_enter" />
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Primer apellido</label>
                    <input type="text" id="apellido_paterno_" class="form-control buscar_enter" />
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Segundo apellido</label>
                    <input type="text" id="apellido_materno_" class="form-control buscar_enter" />
                </div>
            </div>
            <div class="col-md-2 mt-4">
                <button type="button" onclick="app.filtrarTablaClientes()" class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>*Fecha venta</label>
                    <input type="date" id="fecha_venta" name="fecha_venta" value="<?php echo isset($venta->fecha_venta) ? $venta->fecha_venta : ''; ?>" class=" form-control" />
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-12 text-right">
                Si no existe el cliente dar clic en el botón
                <button class="btn btn-dark btn-flujo" title="Agregar cliente" onclick="app.openModalAltaCliente()" type="button">
                    <i class="fa fa-plus-circle" style="font-size:18px"></i>
                    Agregar cliente
                </button>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="tbl_cliente" width="100%" cellspacing="0"></table>
                </div>
            </div>
        </div>

        <div id="datos_cotizador" style="display:none" class="row mt-3">
            @include('refacciones/ventas_mostrador/partials/datos_cotizador')
        </div>
    </form>


    <div class="row mb-4 mt-4">
        <div class="col-md-12 text-right">
            @if (isset($venta->id) && $venta->id)
            <button onclick="app.modificarVenta()" class="btn btn-primary btn-flujo" id="btn-modificar-venta">
                <i class="fas fa-arrow-right"></i>
                Guardar y continuar
            </button>
            @else
            <button onclick="app.generateVenta()" class="btn btn-primary btn-flujo" id="btn-guardar-venta">
                <i class="fas fa-arrow-right"></i>
                Continuar y guardar
            </button>
            @endif
        </div>
    </div>