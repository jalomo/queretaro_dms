<div class="pull-right"><b style="font-size:16px">Folio: <span class="text-danger">{{ isset($venta->folio) ? $venta->folio->folio : '' }}</span></b></div>
<h3>Productos seleccionados</h3>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="tbl_carrito" width="100%" cellspacing="0">
            </table>
        </div>
    </div>
</div>
<div class="pull-right p-2 mt-2 mb-2 pr-4">
    <b style="font-size:16px" class="text-info">Totales venta</b><br />
    <strong>Sub total: <span id="subtotal_" class="money_format">0.00</span></strong><br />
    <strong>IVA: <span id="iva_" class="money_format">0.00</span></strong><br />
    <strong>Total: <span id="venta_total_" class="money_format">0.00</strong></span><br />
</div>
<div class="clearfix"></div>
<hr>
<h3>Productos disponibles</h3>
<hr>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>No identificación</label>
            <input type="text" name="no_identificacion" id="no_identificacion_form" class="form-control buscar_enter" />
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Descripción</label>
            <input type="text" name="descripcion" id="descripcion_form" class="form-control buscar_enter" />
        </div>
    </div>
    <div class="col-md-2 mt-4">
        <button type="button" onclick="app_paso2.filtrarProductosDisponibles()" class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="tbl_productos" width="100%" cellspacing="0"></table>
        </div>
    </div>
</div>
<div class="row mt-5">
    <div class="col-md-12">
        <div class="pull-right">
            @if (isset($tipo_venta_id) && $tipo_venta_id == 3)
            <a target="_blank" style="display:nombre" id="btn_comprobante_cotizacion" href="{{ site_url('refacciones/salidas/generarComprobanteCotizacion?venta_id=' .$venta->id) }}" class="btn btn-danger"> <i class="fa fa-file-pdf"></i> Imprimir Comprobante </a>
            <button type="button" class="btn btn-primary btn-flujo" onclick="app_paso2.modificarVenta()"> <i class="fa fa-save"></i> Finalizar cotización </button>
            @else
            <button type="button" class="btn btn-primary btn-flujo" onclick="app_paso2.modificarVenta()"> <i class="fa fa-arrow-right"></i> Guardar y continuar </button>
            @endif
        </div>
    </div>
</div>