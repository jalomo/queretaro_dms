<div class="col-md-3">
    <div class="form-group">
        <label>Teléfono</label>
        <input type="text" id="telefono_cliente" readonly="readonly" class="form-control" value="{{ isset($cliente->telefono) ? $cliente->telefono : '' }} " />
    </div>
</div>
<div class="col-md-3">
    <div class="form-group">
        <label>Correo electrónico</label>
        <input type="text" readonly="readonly" id="correo_cliente" class="form-control" value="{{ isset($cliente->telefono) ? $cliente->telefono : '' }} " />
    </div>
</div>
<div class="col-md-3">
    <div class="form-group">
        <label>Marca</label>
        <select type="text" name="marca_id" onchange="app.tipoModeloByMarca(this)" data="{{ isset($cotizador->marca_id) ? $cotizador->marca_id : '' }}" class="form-control">
            <option value="">Seleccione una opción</option>
        </select>
    </div>
</div>
<div class="col-md-3">
    <div class="form-group">
        <label>Modelo</label>
        <select type="text" name="modelo_id" data="{{ isset($cotizador->modelo_id) ? $cotizador->modelo_id : '' }}" class="form-control">
        </select>
    </div>
</div>
<div class="col-md-3">
    <div class="form-group">
        <label>Año</label>
        <select name="anio" class="form-control" id="anio">
            <?php
            for ($i = date("Y"); $i >= date("Y") - 40; $i--) { ?>
                <option <?php isset($cotizador->anio) && $i == $cotizador->anio ? 'selected="selected"' : ''; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="col-md-3">
    <div class="form-group">
        <label>Número serie</label>
        <input type="text" name="no_serie" id="no_serie_" maxlenght="17" class="form-control" value="{{ isset($cotizador->no_serie) ? $cotizador->no_serie : '' }}" />
    </div>
</div>