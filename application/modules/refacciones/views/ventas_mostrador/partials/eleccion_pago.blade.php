<div class="row mt-2">
    <div class="col-md-3">
        <div class="form-group">
            <label for="select">¿Compra de credito ó contado?</label>
            <select name="tipo_forma_pago_id" class="form-control" onchange="app_paso3.changeTipoPago(this)" id="tipo_forma_pago_id">
                <option value=""></option>
                @foreach ($tipo_forma_pago as $tipo)
                @if($tipo->id == $cxc->tipo_forma_pago_id)
                <option value="{{ $tipo->id}}" selected="selected"> {{ $tipo->descripcion}} </option>
                @else
                <option value="{{ $tipo->id}}"> {{ $tipo->descripcion}} </option>
                @endif
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-3 container-forma-pago">
        <div class="form-group">
            <label for="select">Plazo de credito</label>
            <select name="plazo_credito_id" class="form-control " id="plazo_credito_id">
                <option value=""> Seleccionar</option>
                @foreach ($plazo_credito as $plazo)
                @if($plazo->id != 14)
                @if($plazo->id == $cxc->plazo_credito_id)
                <option selected="selected" data-cantidad_mes="{{$plazo->cantidad_mes}}" value="{{ $plazo->id }}"> {{ $plazo->nombre}} </option>
                @else
                <option data-cantidad_mes="{{$plazo->cantidad_mes}}" value="{{ $plazo->id}}"> {{ $plazo->nombre}} </option>
                @endif
                @endif
                @endforeach
            </select>
        </div>
    </div>
</div>
<hr />
<div class="row">
    <div class="col-md-6">
        <label>*Concepto</label>
        <textarea class="form-control" name="concepto" id="concepto" maxlength="1000" rows="5">{{ isset($cxc->concepto) ? $cxc->concepto :  '' }}</textarea>
    </div>
</div>

<div class="row mb-4 mt-4">
    <div class="col-md-12 text-right">
        @if (isset($cxc->id) && $cxc->id)
        <button onclick="app_paso3.validarVenta()" class="btn btn-primary btn-flujo" id="btn-modificar-cuenta">
            <i class="fas fa-arrow-right"></i>
            Actualizar caja
        </button>
        @else
        <button onclick="app_paso3.validarVenta()" class="btn btn-primary btn-flujo" id="btn-guardar-cuenta">
            <i class="fas fa-arrow-right"></i>
            Mandar a caja
        </button>
        @endif
    </div>
</div>