@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4 font-weight-bold">
            <i class="fas fa-dolly-flatbed"></i>
            Pedidos
        </h1>
        <hr>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
            <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
            <li class="breadcrumb-item active">Listado</li>
        </ol>
        <div class="d-flex mb-2">
            <div class="btn-group ml-auto">
                <button type="button" class="btn btn-lg btn-success" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-file-import"></i>
                    Entradas de Pedidos
                </button>
                <button class="btn btn-success rounded-right" data-toggle="dropdown">
                    <i class="fa fa-caret-down"></i>
                </button>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                    <button id="btn-cargar-pedido-xml" class="dropdown-item py-2 cursor-pointer" type="button">
                        <i class="fa fa-file-invoice mr-2"></i>
                        XML 3.0
                    </button>
                    <button id="btn-cargar-pedido-xml-40" class="dropdown-item py-2 cursor-pointer" type="button">
                        <i class="fa fa-file-invoice mr-2"></i>
                        XML 4.0
                    </button>
                    <button id="btn-cargar-pedido-csv" class="dropdown-item py-2 cursor-pointer" type="button">
                        <i class="fa fa-file-csv mr-2"></i>
                        CSV
                    </button>
                    <a href="{{ base_url('refacciones/pedidos/crear_cargamento') }}" class="dropdown-item py-2 cursor-pointer">
                        <i class="fa fa-hand mr-2"></i>
                        Manual
                    </a>
                </div>
            </div>

            <div class="btn-group ml-2">
                <button type="button" class="btn btn-lg btn-primary" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-plus"></i>
                    Crear Pedido
                </button>
                <button class="btn btn-primary rounded-right" data-toggle="dropdown">
                    <i class="fa fa-caret-down"></i>
                </button>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                    <a href="{{ base_url('refacciones/pedidos/nuevo') }}" class="dropdown-item py-2 cursor-pointer">
                        <i class="fa fa-hand mr-2"></i>
                        Crear Pedido
                    </a>
                </div>
            </div>
        </div>
        <table id="tbl_pedidos" class="table table-bordered w-100">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Fecha</th>
                    <th>Autorizado</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>

        <div id="modal-autorizar" class="modal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-light">
                        <div class="modal-title my-0">
                            <i class="fa fa-user-lock"></i>
                            Autorizar Pedido
                            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <input class="form-control" type="hidden" name="pedido_id" id="pedido_id">
                        <div class="form-group">
                            <label for="">Usuario</label>
                            <input class="form-control" type="text" name="usuario" id="usuario">
                        </div>
                        <div class="form-group">
                            <label for="">Contraseña</label>
                            <input class="form-control" type="password" name="password" id="password">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" onclick="autorizar()">
                            <i class="fa fa-user-lock"></i>
                            Autorizar
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal-cargar-pedido-csv" class="modal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-light">
                        <div class="modal-title my-0">
                            <i class="fa fa-file-csv"></i>
                            Cargar Pedido CSV
                            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <select name="csv-proveedor" id="csv-proveedor" class="form-control">
                                <option value="">Seleccionar</option>
                                <option value="FM">FORD MOTORS</option>
                                <option value="LL">LLANTAS</option>
                                <option value="MC">MOTOR CRAFT</option>
                                <option value="NF">NO FORD</option>
                                <option value="VA">VARIOS</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="file" name="csv-cargar-pedido" id="csv-cargar-pedido">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">Cancelar</span>
                        </button>
                        <button id="uploadPedidoCargamento" class="btn btn-primary" onclick="uploadPedidoCargamento()">
                            <i class="fa fa-upload"></i>
                            Subir Documento
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal-cargar-pedido-xml" class="modal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-light">
                        <div class="modal-title my-0">
                            <i class="fa fa-file-invoice"></i>
                            Cargar Pedido XML 3.0
                            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <select name="csv-proveedor" id="xml-30-proveedor" class="form-control">
                                <option value="">Seleccionar</option>
                                <option value="FM">FORD MOTORS</option>
                                <option value="LL">LLANTAS</option>
                                <option value="MC">MOTOR CRAFT</option>
                                <option value="NF">NO FORD</option>
                                <option value="VA">VARIOS</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="file" name="xml-cargar-pedido" id="xml-cargar-pedido">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">Cancelar</span>
                        </button>
                        <button id="uploadPedidoCargamentoXML" class="btn btn-primary" onclick="uploadPedidoCargamentoXML()">
                            <i class="fa fa-upload"></i>
                            Subir Documento
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal-cargar-pedido-xml-40" class="modal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-light">
                        <div class="modal-title my-0">
                            <i class="fa fa-file-invoice"></i>
                            Cargar Pedido XML 4.0
                            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <select name="csv-proveedor" id="xml-40-proveedor" class="form-control">
                                <option value="">Seleccionar</option>
                                <option value="FM">FORD MOTORS</option>
                                <option value="LL">LLANTAS</option>
                                <option value="MC">MOTOR CRAFT</option>
                                <option value="NF">NO FORD</option>
                                <option value="VA">VARIOS</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="file" name="xml-cargar-pedido-40" id="xml-cargar-pedido-40">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">Cancelar</span>
                        </button>
                        <button id="uploadPedidoCargamentoXML40"  class="btn btn-primary" onclick="uploadPedidoCargamentoXML40()">
                            <i class="fa fa-upload"></i>
                            Subir Documento
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script>

    $('#tbl_pedidos').on('click', '.btn-autorizar', function(){
        let data = $(this).data();
        
        $('#pedido_id').val(data.id)
        $('#usuario').val('')
        $('#password').val('')
        $('#modal-autorizar').modal('show')
    })

    $('#btn-cargar-pedido-csv').click(function(){
        $('#modal-cargar-pedido-csv').modal('show');
    })

    $('#btn-cargar-pedido-xml').click(function(){
        $('#modal-cargar-pedido-xml').modal('show');
    })

    $('#btn-cargar-pedido-xml-40').click(function(){
        $('#modal-cargar-pedido-xml-40').modal('show');
    })

    const autorizar = () => {
        $.isLoading({ text: "Autorizando pedido." });
        let id = $('#pedido_id').val()

        let form = {
            username: $('#usuario').val(),
            password: $('#password').val()
        }

        $.post(`${PATH_API}api/pedido/${id}/autorizar`, form)
            .done(response => {
                toastr.success('Pedido autorizado.')
                $('#modal-autorizar').modal('hide')
                tbl_pedidos.api().ajax.reload()
            })
            .fail(error => {
                toastr.error('Verifique las credenciales.')
            })
            .always(() => {
                $.isLoading('hide');
            })
    }

    const downloadPDF = (id) => {
        $.isLoading({ text: "Descargando poliza de traspaso." });

        $.ajax({
            url: `${PATH_API}api/pedido/${id}/csv`,
            type: 'GET',    
            // xhrFields: { responseType: 'arraybuffer'},
            xhr: () => {
                let xhr = new XMLHttpRequest()
                xhr.onreadystatechange = () => {
                    if(xhr.readyState == 2){
                        xhr.responseType = "arraybuffer"
                    } else {
                        xhr.responseType = "json"
                    }
                }

                return xhr
            }
        })
        .done((response, status, xhr) => {
            let disposition = xhr.getResponseHeader("Content-Disposition")
            let filename = 'pedido.csv'
            let type = xhr.getResponseHeader("Content-Type")

            let blobFile = new Blob([response], { type: type })
            let URL = window.URL || window.webkitURL
            let downloadURL = URL.createObjectURL(blobFile)

            if(typeof window.navigator.msSaveBlob !== 'undefined'){
                window.navigator.msSaveBlob(blobFile, filename)

                let link = document.createElement('a')
                link.href = downloadURL
                link.download = filename
                document.body.appendChild(link)
                link.click()
                return ;
            }            

            window.open(downloadURL)
        })
        .fail((error) => {
            toastr.error("Ocurrio un error al descargar la poliza de traspaso.")
        })
        .always(() => {
            $.isLoading('hide');
        })
    }

    let tbl_pedidos = $('#tbl_pedidos').dataTable({
        language: {
            url: PATH_LANGUAGE
        },
        processing: true,
        serverSide: true,
        bFilter: false,
        ajax: {
            url: PATH_API + "api/pedidos",
            type: 'GET',
        },
        columns: [
            {
                data: 'id'
            },
            {
                data: function(data){
                    return (new Date(data.created_at)).toLocaleString('ES-MX', {
                        dateStyle: 'full',
                        timeStyle: 'short',
                    })
                }
            },
            {
                data: function(data){
                    if(! data.usuario && ! data.entrada)
                        return 'Sin autorizar'

                    if(data.entrada)
                        return 'Entrada de Cargamento'

                    return `${data.usuario.nombre} ${data.usuario.apellido_paterno} ${data.usuario.apellido_materno}`
                }
            },
            {
                'data': function(data) {
                    console.log(data)
                    btn = ''

                    if(! data.autorizado && ! data.entrada)
                        btn += `<a href="${base_url}refacciones/pedidos/editar/${data.id}" title="Editar Pedido" class="btn btn-primary mx-1">
                            <i class="fa fa-edit"></i>
                        </a>`

                    // if(data.autorizado && ! data.entrada)
                    //     btn += `<button class="btn btn-primary btn-subir-csv mx-1" title="Subir CSV" data-id="${data.id}">
                    //             <i class="fas fa-file-csv"></i>
                    //         </button>`

                    if(data.autorizado && ! data.entrada)
                        btn += `<button class="btn btn-primary mx-1" title="Descargar CSV" onclick="downloadPDF(${data.id})">
                                <i class="fas fa-download"></i>
                            </button>`
                    
                    if(! data.autorizado && ! data.entrada)
                        btn += `<button class="btn btn-primary btn-autorizar mx-1" title="Autorizar Pedido" data-id="${data.id}">
                                <i class="fa fa-lock"></i>
                            </button>`

                    if(data.entrada)
                        btn += `<a href="${base_url}refacciones/pedidos/cargamento/${data.id}" class="btn btn-primary mx-1" title="Cargamento">
                                <i class="fa fa-list"></i>
                            </a>`
                    
                    return btn
                }
            }
        ]
    })

    const uploadPedidoCargamento = () => {
        $.isLoading({ text: "Subiendo archivo csv" });
        let csv = $('#csv-cargar-pedido')[0].files[0];

        let form = new FormData()
        form.append('csv', csv)
        form.append('proveedor', $('#csv-proveedor').val())

        $('#uploadPedidoCargamento').prop('disabled', true)

        ajax.postFile(`api/pedido/cargar-pedido/csv`, form, function(response, header){

            try{
                let type = header.status == 200? 'success': 'warning'

                utils.displayWarningDialog(response.message, type, function() {})
                
                $.isLoading('hide')
                $('#modal-cargar-pedido-csv').modal('hide')
                tbl_pedidos.api().ajax.reload()
            } catch(e){
                utils.displayWarningDialog('Ocurrio un error al subir el archivo csv del pedido.', 'warning', function(response) {});
                $.isLoading('hide')
            }

            $('#uploadPedidoCargamento').prop('disabled', false)
        })
    }

    const uploadPedidoCargamentoXML = () => {
        $.isLoading({ text: "Subiendo archivo XML" });
        let xml = $('#xml-cargar-pedido')[0].files[0];

        let form = new FormData()
        form.append('xml', xml)
        form.append('proveedor', $('#xml-30-proveedor').val())

        $('#uploadPedidoCargamentoXML').prop('disabled', true)

        ajax.postFile(`api/pedido/cargar-pedido/xml`, form, function(response, header){

            try{
                let type = header.status == 200? 'success': 'warning'

                utils.displayWarningDialog(response.message, type, function() {})
                
                $.isLoading('hide')
                $('#modal-cargar-pedido-xml').modal('hide')
                tbl_pedidos.api().ajax.reload()

            } catch(e){
                utils.displayWarningDialog('Ocurrio un error al subir el archivo xml del pedido.', 'warning', function(response) {});
                $.isLoading('hide')
            }
            
            $('#uploadPedidoCargamentoXML').prop('disabled', false)
        })
    }

    const uploadPedidoCargamentoXML40 = () => {
        $.isLoading({ text: "Subiendo archivo XML" });
        let xml = $('#xml-cargar-pedido-40')[0].files[0];

        let form = new FormData()
        form.append('xml', xml)
        form.append('proveedor', $('#xml-40-proveedor').val())

        $('#uploadPedidoCargamentoXML40').prop('disabled', true)

        ajax.postFile(`api/pedido/cargar-pedido/xml`, form, function(response, header){

            try{
                let type = header.status == 200? 'success': 'warning'

                utils.displayWarningDialog(response.message, type, function() {})
                
                $.isLoading('hide')
                $('#modal-cargar-pedido-xml-40').modal('hide')
                tbl_pedidos.api().ajax.reload()
            } catch(e){
                utils.displayWarningDialog('Ocurrio un error al subir el archivo xml del pedido.', 'warning', function(response) {});
                $.isLoading('hide')
            }

            $('#uploadPedidoCargamentoXML40').prop('disabled', false)
        })
    }
</script>
@endsection