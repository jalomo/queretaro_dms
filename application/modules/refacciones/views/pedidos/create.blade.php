@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="d-flex mt-4 font-weight-bold">
            <i class="fas fa-plus mr-2"></i>
            Crear Pedido
            <a href="{{ base_url("/refacciones/pedidos") }}" class="btn btn-lg btn-primary ml-auto">
                <i class="fa fa-arrow-left"></i>
                Regresar
            </a>
        </h1>
        <hr>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
            <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
            <li class="breadcrumb-item active">Nuevo</li>
        </ol>

        <div class="d-flex align-items-end mb-3">
            <div class="col px-0">
                <div class="form-group">
                    <label for="no_identificacion">No. de pieza</label>
                    <input id="no_identificacion" placeholder="" name="no_identificacion" type="text" class="form-control" value="">
                    <div id="no_identificacion_error" class="invalid-feedback"></div>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="descripcion">Descripción</label>
                    <input id="descripcion" placeholder="" name="descripcion" type="text" class="form-control" value="">
                    <div id="descripcion_error" class="invalid-feedback"></div>
                </div>            
            </div>
            <div class="col-md-2 mb-3">
                <button type="button" onclick="buscarproducto()" class="btn btn-primary">
                    <svg class="svg-inline--fa fa-search fa-w-16" aria-hidden="true" focusable="false" data-prefix="fa" data-icon="search" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path></svg><!-- <i class="fa fa-search"></i> --> Buscar
                </button>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered" id="productos" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>No. Identificación</th>
                        <th>Descripción</th>
                        <th>Existencia Queretaro</th>
                        <th>Existencia San Juan</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
            </table>
        </div>

        <hr>
        <h2 class="d-flex font-weight-bold">
            <i class="fas fa-clipboard-list mr-2"></i>
            Pedido
            <button class="btn btn-lg btn-success ml-auto" onclick="guardarPedido()">
                <i class="fa fa-save"></i>
                Generar Pedido
            </button>
        </h2>
        <hr>
        <table id="pedido" class="table table-striped">
            <thead>
                <th>#</th>
                <th>No. Identificación</th>
                <th>Descripción</th>
                <th>Cantidad</th>
                <th>Acciones</th>
            </thead>
            <tbody>
                <tr></tr>
            </tbody>
        </table>

        <div id="modal-agregar-producto" class="modal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-light">
                        <div class="modal-title my-0">
                            <i class="fa fa-plus"></i>
                            Agregar Producto a Pedido
                            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <input class="form-control" type="hidden" name="id_producto" id="id_producto">
                        <div class="form-group">
                            <label for="">Clave</label>
                            <input class="form-control" type="text" name="no_identificacion_producto" id="no_identificacion_producto">
                        </div>
                        <div class="form-group">
                            <label for="">Descripción</label>
                            <input class="form-control" type="text" name="descripcion_producto" id="descripcion_producto" readonly>
                        </div>
                        <div class="form-group">
                            <label for="">Cantidad</label>
                            <input class="form-control" type="number" name="cantidad_producto" id="cantidad_producto">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" onclick="agregarProducto()">
                            <i class="fa fa-plus"></i>
                            Agregar Producto
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script>

    let productos = []

    const buscarproducto = () => {

        let descripcion = $("#descripcion").val()
        let no_identificacion = $('#no_identificacion').val()

        let parametros = $.param({ descripcion, no_identificacion });

        $('#productos').DataTable().ajax.url(PATH_API + 'api/productos/listadoStock?'+ parametros).load()
    }

    $('#productos').on('click', '.btn-agregar-producto', function(){
        let data = $(this).data();
        
        $('#id_producto').val(data.id)
        $('#no_identificacion_producto').val(data.noIdentificacion)
        $('#descripcion_producto').val(data.descripcion)
        $('#cantidad_producto').val(1)
        $('#modal-agregar-producto').modal('show')
    })

    const agregarProducto = () => {

        let data = {
            id: $('#id_producto').val(),
            no_identificacion: $('#no_identificacion_producto').val(),
            descripcion: $('#descripcion_producto').val(),
            cantidad: $('#cantidad_producto').val(),    
        }

        let index = productos.findIndex( element => element.id == data.id)


        if(index != -1){
            data.cantidad = parseInt(productos[index].cantidad) + parseInt(data.cantidad)
            productos.splice(index, 1, data)
            $('#pedido').DataTable().row(index).remove()
        } else {
            productos.push(data)                
        }
        console.log(productos)

        $('#pedido').DataTable().row.add(data)
        $('#pedido').DataTable().draw()
        $('#modal-agregar-producto').modal('hide')
    }

    const deleteProducto = (index) => {
        productos.splice(index, 1)
        $('#pedido').DataTable().clear()
        $('#pedido').DataTable().rows.add(productos).draw()

        console.log(index)
    }

    const guardarPedido = () => {
        $.isLoading({ text: "Creando pedido de los productos seleccionados." });

        $.post(`${PATH_API}api/pedido`, {
            productos
        })
        .done(response => {
            toastr.success('Se realizo la petición de nuevo pedido.')
            window.location.href = base_url + 'refacciones/pedidos'
        })
        .fail((error) => {
            toastr.error("Ocurrio un error al crear el pedido.")
        })
        .always(() => {
            $.isLoading('hide');
        })
    }

    $(document).ready(function(){
        var tbl_pedido = $('#pedido').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            data: productos,
            columns: [
                { data: 'id' },
                { data: 'no_identificacion' },
                { data: 'descripcion' },
                { data: 'cantidad' },
                {
                    render: function(data, type, row, meta) {
                        return `<button class="btn btn-danger" onclick="deleteProducto(${meta.row})">
                                <i class="fa fa-trash-alt"></i>
                            </button>`
                    }
                }
            ]
        })
        
        var tbl_productos = $('#productos').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            ajax: {
                url: PATH_API + 'api/productos/listadoStock',
                type: 'GET',
            },
            processing: true,
            serverSide: true,
            bFilter: false,
            columns: [
                {
                    data: 'id',
                },
                {
                    data: 'no_identificacion',
                },
                {
                    data: 'descripcion',
                },
                {
                    data: 'cantidad_actual',
                },
                {
                    data: 'cantidad_almacen_secundario',
                },
                {
                    render: function(data, type, row) {
                        return `<button class="btn-agregar-producto btn btn-success"
                            data-id="${row.id}"
                            data-no-identificacion="${row.no_identificacion}"
                            data-descripcion="${row.descripcion}"
                        >
                                <i class="fas fa-cart-plus"></i>
                            </button>`
                    }
                }
            ]
        })
    })
</script>
@endsection