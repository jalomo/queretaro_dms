@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="d-flex mt-4 font-weight-bold">
            <i class="fas fa-truck mr-2"></i>
            Entradas de Refacciones
            <button class="btn btn-primary ml-auto" onClick="downloadPDF({{ $id }})">
                <i class="fa fa-download"></i>
                Descargar Listado
            </button>
            <button id="subir-factura" class="btn btn-success ml-2" onClick="modalSubir({{ $id }})">
                <i class="fa fa-upload"></i>
                Subir Factura
            </button>
            <a href="{{ base_url("/refacciones/pedidos") }}" class="btn btn-lg btn-primary ml-2">
                <i class="fa fa-arrow-left"></i>
                Regresar
            </a>
        </h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
            <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
            <li class="breadcrumb-item active">Cargamento</li>
        </ol>
        <hr>
        <h4 class="text-center text-dark">
            <b>PROVEEDOR: </b>
            <span id="proveedor">SIN ASIGNAR</span>
        </h4>
        <h4 class="text-center text-dark">
            <b>TOTAL SIN IVA: </b>
            <span id="total"></span>
        </h4>
        <hr>
        <table id="pedido" class="table table-striped">
            <thead>
                <th>#</th>
                <th>No. Identificación</th>
                <th>Descripción</th>
                <th>Ubicación</th>
                <th>Cantidad Entrante</th>
                <th>Acciones</th>
            </thead>
            <tbody>
                <tr></tr>
            </tbody>
        </table>

        <div id="modal-editar" class="modal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-light">
                        <div class="modal-title my-0">
                            <i class="fa fa-edit"></i>
                            Editar Producto
                            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <input class="form-control" type="hidden" name="id" id="id">
                        <input class="form-control" type="hidden" name="producto_id" id="producto_id">
                        <input class="form-control" type="hidden" name="no_identificacion" id="no_identificacion">
                        <div class="form-group">
                            <label for="">Producto</label>
                            <div>
                                <select id="select-productos" class="form-control select-productos" style="width:100%" placeholder="Producto">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Descripción</label>
                            <input class="form-control" type="text" name="descripcion" id="descripcion">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" onclick="editar()">
                            <i class="fa fa-edit"></i>
                            Editar
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal-subir" class="modal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-light">
                        <div class="modal-title my-0">
                            <i class="fa fa-upload"></i>
                            Subir Factura
                            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Factura</label>
                            <input class="form-control" type="file" name="factura" id="factura">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" onclick="subir()">
                            <i class="fa fa-upload"></i>
                            Subir
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal-eliminar" class="modal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-light">
                        <div class="modal-title my-0 text-danger">
                            <i class="fa fa-trash"></i>
                            Devolución
                            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <h1 class="text-center">
                            ¿Estas seguro de devolver la pieza una vez realizado este procedimiento no es posible deshacerlo?
                        </h1>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" onclick="eliminar()">
                            <i class="fa fa-trash"></i>
                            Devolver
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script>

    let productos = []

    mxn = Intl.NumberFormat('es-MX', {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
        currency: 'MXN',
    })

    const getProductos = async () => {
        $.isLoading({ text: "Buscando refacciones asignadas al pedido." });

        $.get(`${PATH_API}api/pedido/{{ $id }}/cargamento`)
        .done(response => {
            toastr.success('Se encontraron refacciones.')
            productos = response.aaData

            let factura = productos.some( producto => producto.factura )
            let proveedor = productos.find( (producto, index) => index == 0).proveedor || 'SIN ASIGNAR'
            
            $('#proveedor').text(
                proveedor
            )

            $('#total').text(
                mxn.format(productos.reduce((total, producto) => total + parseFloat(producto.total), 0 ))
            )

            if(factura) $('#subir-factura').addClass('d-none')

            $('#pedido').DataTable().clear()
            $('#pedido').DataTable().rows.add(productos).draw()
        })
        .fail((error) => {
            toastr.error("Ocurrio un error al cargar las refacciones.")
        })
        .always(() => {
            $.isLoading('hide');
        })
    }

    function downloadPDF(pedido_id){
        $.isLoading({ text: "Descargando poliza de traspaso." });

        $.ajax({
            url: `${PATH_API}api/pedido/${pedido_id}/cargamento/pdf`,
            type: 'GET',    
            // xhrFields: { responseType: 'arraybuffer'},
            xhr: () => {
                let xhr = new XMLHttpRequest()
                xhr.onreadystatechange = () => {
                    if(xhr.readyState == 2){
                        xhr.responseType = "arraybuffer"
                    } else {
                        xhr.responseType = "json"
                    }
                }

                return xhr
            }
        })
        .done((response, status, xhr) => {
            let disposition = xhr.getResponseHeader("Content-Disposition")
            let filename = 'poliza-cargamento.pdf'
            let type = xhr.getResponseHeader("Content-Type")

            let blobFile = new Blob([response], { type: type })
            let URL = window.URL || window.webkitURL
            let downloadURL = URL.createObjectURL(blobFile)

            if(typeof window.navigator.msSaveBlob !== 'undefined'){
                window.navigator.msSaveBlob(blobFile, filename)

                let link = document.createElement('a')
                link.href = downloadURL
                link.download = filename
                document.body.appendChild(link)
                link.click()
                return ;
            }            

            window.open(downloadURL)
        })
        .fail((error) => {
            toastr.error("Ocurrio un error al descargar la poliza de traspaso.")
        })
        .always(() => {
            $.isLoading('hide');
        })
    }

    const modalEditar = (id, producto_id, no_identificacion, descripcion) => {
        $('#id').val(id)
        $('#producto_id').val(producto_id)
        $('#descripcion').val(descripcion)
        $('#no_identificacion').val(no_identificacion)

        let newOption = new Option(no_identificacion, producto_id, true, true)
        $('#select-productos').append(newOption).trigger('change')
        
        $('#modal-editar').modal('show')
    }

    const modalSubir = (id) => {
        $('#id').val(id)
        $('#modal-subir').modal('show')
    }

    const modalEliminar = (id) => {
        $('#id').val(id)
        $('#modal-eliminar').modal('show')
    }

    const editar = () => {
        let id = $('#id').val()
        let producto_id = $('#select-productos').val()
        let descripcion = $('#descripcion').val()

        ajax.post(`api/pedido/${id}/producto`, {
            producto_id: producto_id,
            descripcion: descripcion,
        }, function(headers, response){
            if(response.status == 200){
                $('#modal-editar').modal('hide')
                getProductos()
                toastr.success('Se actualizo correctamente')
            }
        })
        
    }

    const subir = () => {
        $.isLoading({ text: "Subiendo factura." });
        let id = $('#id').val()
        let factura = $('#factura')[0].files[0];

        let form = new FormData()
        form.append('factura', factura)

        ajax.postFile(`api/pedido/${id}/subir-factura`, form, function(response, header){

            try{
                let type = header.status == 200? 'success': 'warning'

                utils.displayWarningDialog(response.message, type, function() {})
                
                $.isLoading('hide')
                $('#modal-subir').modal('hide')
                window.location.href=""
            } catch(e){
                utils.displayWarningDialog('Ocurrio un error al subir la factura del pedido.', 'warning', function(response) {});
                $.isLoading('hide')
            }
        })
    }

    const eliminar = () => {
        $.isLoading({text: 'Realizando devolucion de pieza, descontando de inventario'})
        let id = $('#id').val()

        ajax.post(`api/pedido/${id}/devolver`, {}, function(headers, response){
            if(response.status == 200){
                $('#modal-eliminar').modal('hide')
                getProductos()
                toastr.success('Devolución realizada correctamente.')
            }
        })
    }

    $(document).ready(function(){

        productos = getProductos()

        var tbl_pedido = $('#pedido').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            data: productos,
            columns: [
                { data: 'id' }, 
                { data: 'no_identificacion' },
                { data: 'descripcion' },
                { data: 'ubicacion' },
                { data: 'cantidad_entregada' },
                {
                    data: function(data){

                        if(data.deleted_at)
                            return `<span class="text-danger text-uppercase font-weight-bold">Devuelto</span>`

                        return `
                            <button class="btn btn-primary" onClick="modalEditar(${data.id}, ${data.producto_id}, '${data.no_identificacion}', '${data.descripcion}')">
                                <i class="fa fa-edit"></i>
                            </button>
                            <button class="btn btn-danger" onClick="modalEliminar(${data.id})">
                                <i class="fa fa-trash-alt"></i>
                            </button>`
                    }
                }
            ],
        })

        $('.select-productos').select2({
            minimumInputLength: 3,
            ajax: {
                delay: 250,
                url: PATH_API + 'api/productos/listado',
                data: function(params){
                    let query = {
                        no_identificacion: params.term
                    }

                    return query
                },
                processResults: function(data, params){
                    let id = $('#id').val()
                    let producto_id = $('#producto_id').val()
                    let no_identificacion = $('#no_identificacion').val()
                    let descripcion = $('#descripcion').val()

                    let newOption = { id: producto_id, text: no_identificacion, descripcion: descripcion, selected: true }

                    return {
                        results: data.aaData.map(object => { return { id: object.id, text: object.no_identificacion, descripcion: object.descripcion } } ).concat(newOption)
                    }
                }
            },
        })

        $('.select-productos').on('select2:selecting', function(e){
            let data = e.params.args.data

            $('#descripcion').val(data.descripcion)
        })
    })
</script>
@endsection