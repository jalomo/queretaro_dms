@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="d-flex mt-4 font-weight-bold">
            <i class="fas fa-plus mr-2"></i>
            Crear Pedido desde Venta
            <button onClick="window.history.back()" class="btn btn-lg btn-primary ml-auto">
                <i class="fa fa-arrow-left"></i>
                Regresar
            </button>
        </h1>
        <hr>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
            <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
            <li class="breadcrumb-item active">Venta</li>
        </ol>
        <h2 class="d-flex font-weight-bold">
            <i class="fas fa-clipboard-list mr-2"></i>
            Pedido
            <button class="btn btn-lg btn-success ml-auto" onclick="guardarPedido()">
                <i class="fa fa-save"></i>
                Generar Pedido
            </button>
        </h2>
        <hr>
        <table id="pedido" class="table table-striped">
            <thead>
                <th>#</th>
                <th>No. Identificación</th>
                <th>Descripción</th>
                <th>Cantidad</th>
                <th>Fecha</th>
                <th>Origen</th>
                <th>Acciones</th>
            </thead>
            <tbody>
                <tr></tr>
            </tbody>
        </table>
    </div>
@endsection
@section('scripts')
<script>

    let productos = []

    const getProductos = async () => {
        $.isLoading({ text: "Buscando refacciones asignadas a la venta." });

        $.get(`${PATH_API}api/pedido/{{ $id }}/ventas`, {
            productos
        })
        .done(response => {
            toastr.success('Se encontraron refacciones.')
            productos = response.aaData

            $('#pedido').DataTable().clear()
            $('#pedido').DataTable().rows.add(productos).draw()
        })
        .fail((error) => {
            toastr.error("Ocurrio un error al cargar las refacciones.")
        })
        .always(() => {
            $.isLoading('hide');
        })
    }

    const deleteProducto = (index) => {
        productos.splice(index, 1)
        $('#pedido').DataTable().clear()
        $('#pedido').DataTable().rows.add(productos).draw()

        console.log(index)
    }

    const guardarPedido = () => {
        $.isLoading({ text: "Creando pedido de los productos seleccionados." });

        $.post(`${PATH_API}api/pedido/del-dia`, {
            productos
        })
        .done(response => {
            toastr.success('Se realizo la petición de nuevo pedido.')
            window.location.href = base_url + 'refacciones/pedidos'
        })
        .fail((error) => {
            toastr.error("Ocurrio un error al crear el pedido.")
        })
        .always(() => {
            $.isLoading('hide');
        })
    }

    $(document).ready(function(){
        
        productos = getProductos()

        var tbl_pedido = $('#pedido').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            data: productos,
            columns: [
                { data: 'id' },
                { data: 'no_identificacion' },
                { data: 'descripcion' },
                { data: 'cantidad' },
                { data: 'created_at' },
                { data: 'origen' },
                {
                    render: function(data, type, row, meta) {
                        return `<button class="btn btn-danger" onclick="deleteProducto(${meta.row})">
                                <i class="fa fa-trash-alt"></i>
                            </button>`
                    }
                }
            ]
        })
    })
</script>
@endsection