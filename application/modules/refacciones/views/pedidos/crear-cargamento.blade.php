@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="d-flex mt-4 font-weight-bold">
            <i class="fas fa-plus mr-2"></i>
            Crear Cargamento
            <a href="{{ base_url("/refacciones/pedidos") }}" class="btn btn-lg btn-primary ml-auto">
                <i class="fa fa-arrow-left"></i>
                Regresar
            </a>
        </h1>
        <hr>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
            <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
            <li class="breadcrumb-item active">Cargamento Nuevo</li>
        </ol>

        <div class="alert alert-warning">
            <i class="fa fa-warning mr-2"></i>
            Cuando el proveedor es FORD los precios se deben cargar con IVA.<br>
        </div>
        <div class="form-group">
            <label for="descripcion_search">Tipo de Proveedor:</label>
            <select id="ford" name="ford" class="form-control">
                <option value="">Seleccionar</option>
                <option value="FM">FORD MOTORS</option>
                <option value="LL">LLANTAS</option>
                <option value="MC">MOTOR CRAFT</option>
                <option value="NF">NO FORD</option>
                <option value="VA">VARIOS</option>
            </select>
            <div id="descripcion_search_error" class="invalid-feedback"></div>
        </div> 

        <div id="datos_producto" class="border p-2 rounded d-none">
            <legend class="font-weight-bold mb-0">
                <i class="fa fa-list"></i>
                Datos de Producto
            </legend>
            <hr class="mt-2">
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="no_identificacion">No. de pieza</label>
                        <div class="input-group">
                            <input id="no_identificacion" placeholder="" name="no_identificacion" type="text" class="form-control" value="">
                            <input id="id_producto" placeholder="" name="id_producto" type="hidden" class="form-control" value="">
                            <div id="no_identificacion_error" class="invalid-feedback"></div>
                            <div class="input-group-append">
                                <button type="button" onclick="buscarproducto()" class="btn btn-primary">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-8">
                    <div class="form-group">
                        <label for="descripcion">Descripción</label>
                        <input id="descripcion" placeholder="" name="descripcion" type="text" class="form-control" value="">
                        <div id="descripcion_error" class="invalid-feedback"></div>
                    </div>            
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="grupo">Grupo:</label>
                        <select id="grupo" name="grupo" class="form-control">
                            <option value="">Seleccionar</option>
                            <option value="FM">FM - FORD MOTORS</option>
                            <option value="LL">LL - LLANTAS</option>
                            <option value="MC">MC - MOTOR CRAFT</option>
                            <option value="NF">NF - NO FORD</option>
                            <option value="VA">VA - VARIOS</option>
                        </select>
                        <div id="grupo_error" class="invalid-feedback"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="prefijo">Prefijo</label>
                        <input id="prefijo" class="form-control" type="text" name="prefijo" value="">
                        <div id="prefijo_error" class="invalid-feedback"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="basico">Básico</label>
                        <input id="basico" class="form-control" type="text" name="basico" value="">
                        <div id="basico_error" class="invalid-feedback"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="sufijo">Sufijo</label>
                        <input id="sufijo" class="form-control" type="text" name="sufijo" value="">
                        <div id="sufijo_error" class="invalid-feedback"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="cantidad">Cantidad</label>
                        <input id="cantidad" class="form-control" type="number" name="cantidad" value="1">
                        <div id="cantidad_error" class="invalid-feedback"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="valor_unitario">Valor Unitario:</label>
                        <input id="valor_unitario" class="form-control" type="number" name="valor_unitario" value="0">
                        <div id="valor_unitario_error" class="invalid-feedback"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="ubicacion">Ubicación</label>
                        <input id="ubicacion" class="form-control" type="text" name="ubicacion" value="GEN.">
                        <div id="ubicacion_error" class="invalid-feedback"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form group">
                        <label for="">Proveedor</label>
                        <input type="text" id="proveedor" class="form-control" name="proveedor" value="">
                        <div id="proveedor_error" class="invalid-feedback"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="form group">
                        <label for="">Remisión o Factura</label>
                        <input type="text" id="factura" class="form-control" name="factura" value="">
                        <div id="factura_error" class="invalid-feedback"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="form group">
                        <label for="">Clave SAT</label>
                        <input type="text" id="clave_sat" class="form-control" name="clave_sat" value="">
                        <div id="unidad_sat_error" class="invalid-feedback"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="form group">
                        <label for="">Unidad SAT</label>
                        <input type="text" id="unidad_sat" class="form-control" name="unidad_sat" value="">
                        <div id="unidad_sat_error" class="invalid-feedback"></div>
                    </div>
                </div>
            </div>
    
            <div class="d-flex my-3">
                <button class="btn btn-lg btn-secondary" id="clear">
                    <i class="fa fa-eraser"></i>
                    Limpiar
                </button>
                <button class="btn btn-lg btn-primary ml-auto" onclick="agregarProducto()">
                    <i class="fa fa-save"></i>
                    Agregar Pieza
                </button>
            </div>
        </div>

        <hr>
        <h2 class="d-flex font-weight-bold">
            <i class="fas fa-clipboard-list mr-2"></i>
            Pedido
        </h2>
        <hr>
        <table id="pedido" class="table table-striped">
            <thead>
                <th>#</th>
                <th>No. Identificación</th>
                <th>Descripción</th>
                <th>Cantidad</th>
                <th>Acciones</th>
            </thead>
            <tbody>
                <tr></tr>
            </tbody>
        </table>

        <hr>
        <div class="d-flex">
            <button class="btn btn-lg btn-success ml-auto" onclick="guardarPedido()">
                <i class="fa fa-save"></i>
                Generar Cargamento
            </button>
        </div>

        <div id="modal-seleccionar-producto" class="modal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-light">
                        <div class="modal-title my-0">
                            <i class="fa fa-plus"></i>
                            Seleccionar Producto
                            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="tbl_productos" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>No. Identificación</th>
                                        <th>Descripción</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script>

    let listado = []
    let productos = []

    const buscarproducto = () => {

        let descripcion = $("#descripcion_search").val()
        let no_identificacion = $('#no_identificacion').val()

        let parametros = $.param({ descripcion, no_identificacion });
        getListado(parametros)   

        $('#grupo').css('pointer-events', 'none')
        $('#grupo').attr('readonly', true)
        $('#prefijo').attr('readonly', true)
        $('#basico').attr('readonly', true)
        $('#sufijo').attr('readonly', true)
    }

    const getListado = (parametros) => {
        toastr.info('Buscando piezas')

        $.get(`${PATH_API}api/productos/listado?`+ parametros)
        .done(response => {
            listado = response.aaData

            if(listado.length > 0)
                toastr.success('Se encontraron productos')
            else
                toastr.error('No se encontraron coincidencias')

            $('#tbl_productos').DataTable().clear()
            $('#tbl_productos').DataTable().rows.add(listado).draw()

            $('#modal-seleccionar-producto').modal('show')
        })
        .fail(error => {
            toastr.error('Error no se encontraron productos.')
        })
    }

    $('#ford').on('change', function(){
        let show = $(this).val() == ""? false : true

        if(show){
            $('#datos_producto').removeClass('d-none')
            $('#busqueda').addClass('d-flex')
            $('#busqueda').removeClass('d-none')
        } else {
            $('#datos_producto').addClass('d-none')
            $('#busqueda').removeClass('d-flex')
            $('#busqueda').addClass('d-none')
        }
    })

    $('#tbl_productos').on('click', '.btn-seleccionar-producto', function(){
        let data = $(this).data();
        
        $('#id_producto').val(data.id)
        $('#no_identificacion').val(data.noIdentificacion)
        $('#grupo').val(data.grupo)
        $('#basico').val(data.basico)
        $('#prefijo').val(data.prefijo)
        $('#sufijo').val(data.sufijo)
        $('#descripcion').val(data.descripcion)
        $('#ubicacion').val(data.ubicacion)
        $('#cantidad').val(1)
        
        $('#modal-seleccionar-producto').modal('hide')
    })

    const deleteProducto = (index) => {
        productos.splice(index, 1)
        $('#pedido').DataTable().clear()
        $('#pedido').DataTable().rows.add(productos).draw()
    }

    const guardarPedido = () => {
        $.isLoading({ text: "Creando pedido de los productos seleccionados." });

        $.post(`${PATH_API}api/pedido/cargamento`, {
            productos,
            ford: $('#ford').val(),
        })
        .done(response => {
            toastr.success('Se realizo la petición de nuevo pedido.')
            window.location.href = base_url + 'refacciones/pedidos'
        })
        .fail((error) => {
            
            if(jqXHR.status === 400){
                toastr.error(jqXHR.getResponseHeader('X-Message'));
                return;
            }

            toastr.error("Ocurrio un error al crear el pedido.")
        })
        .always(() => {
            $.isLoading('hide');
        })
    }

    // Se vacian los campos
    $('#clear').click(function(){
        $('#id_producto').val(''),
        $('#no_identificacion').val(''),
        $('#grupo').val(''),
        $('#basico').val(''),
        $('#prefijo').val(''),
        $('#sufijo').val(''),
        $('#descripcion').val(''),
        $('#cantidad').val('1'),
        $('#ubicacion').val('GEN.'),
        $('#factura').val(''),
        $('#proveedor').val('')
        $('#clave_sat').val('')
        $('#unidad_sat').val('')

        $('#grupo').css('pointer-events', 'all')
        $('#grupo').attr('readonly', false)
        $('#prefijo').attr('readonly', false)
        $('#basico').attr('readonly', false)
        $('#sufijo').attr('readonly', false)
    })

    const agregarProducto = () => {

        let producto = {
            id: $('#id_producto').val(),
            no_identificacion: $('#no_identificacion').val(),
            grupo: $('#grupo').val(),
            basico: $('#basico').val(),
            prefijo: $('#prefijo').val(),
            sufijo: $('#sufijo').val(),
            descripcion: $('#descripcion').val(),
            cantidad: $('#cantidad').val(),
            valor_unitario: $('#valor_unitario').val(),
            factura: $('#factura').val(),
            proveedor: $('#proveedor').val(),
            ubicacion: $('#ubicacion').val(),
            clave_sat: $('#clave_sat').val(),
            unidad_sat: $('#unidad_sat').val(),
        }

        if(isEmpty(producto.id) && (
            isEmpty(producto.basico)
        )){
            toastr.error('Debe seleccionar un producto o llenar el campo básico.')
            return;
        }
        
        if(isEmpty(producto.cantidad) || parseFloat(producto.cantidad) < 1){
            toastr.error('Debe llenar el campo cantidad y debe ser mayor a 0')
            return;
        }

        if(isEmpty(producto.valor_unitario) || parseFloat(producto.valor_unitario) <= 0){
            toastr.error('Debe llenar el campo valor unitario y debe ser mayor a 0')
            return;
        }

        if(isEmpty(producto.factura)){
            toastr.error('Debe llenar el campo factura')
            return;
        }

        if(isEmpty(producto.proveedor)){
            toastr.error('Debe llenar el campo proveedor')
            return;
        }

        if(isEmpty(producto.ubicacion)){
            toastr.error('Debe llenar el campo ubicación')
            return;
        }

        if(isEmpty(producto.descripcion)){
            toastr.error('Debe llenar el campo descripción')
            return;
        }

        productos.push(producto)

        $('#id_producto').val(''),
        $('#no_identificacion').val(''),
        $('#grupo').val(''),
        $('#basico').val(''),
        $('#prefijo').val(''),
        $('#sufijo').val(''),
        $('#descripcion').val(''),
        $('#cantidad').val('1'),
        $('#ubicacion').val('GEN.'),
        $('#factura').val(''),
        $('#proveedor').val('')
        $('#clave_sat').val('')
        $('#unidad_sat').val('')

        $('#grupo').css('pointer-events', 'all')
        $('#grupo').attr('readonly', false)
        $('#prefijo').attr('readonly', false)
        $('#basico').attr('readonly', false)
        $('#sufijo').attr('readonly', false)

        $('#pedido').DataTable().clear()
        $('#pedido').DataTable().rows.add(productos).draw()

        toastr.success('Producto se agrego al pedido correctamente.')
    }

    const isEmpty = (value, field) => {
        if(! value || value.length === 0){
            return true
        }

        return false
    }

    $(document).ready(function(){

        var tbl_productos = $('#tbl_productos').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            data: listado,
            columns: [
                {
                    data: 'id',
                },
                {
                    data: 'no_identificacion',
                },
                {
                    data: 'descripcion',
                },
                {
                    render: function(data, type, row) {
                        return `<button class="btn-seleccionar-producto btn btn-success"
                            data-id="${row.id}"
                            data-no-identificacion="${row.no_identificacion}"
                            data-descripcion="${row.descripcion}"
                            data-prefijo="${row.prefijo}"
                            data-basico="${row.basico}"
                            data-sufijo="${row.sufijo}"
                            data-grupo="${row.grupo}"
                            data-ubicacion="${row.ubicacion}"
                        >
                                <i class="fas fa-cart-plus"></i>
                            </button>`
                    }
                }
            ]
        })

        var tbl_pedido = $('#pedido').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            data: productos,
            columns: [
                { data: 'id' },
                { data: 'no_identificacion' },
                { data: 'descripcion' },
                { data: 'cantidad' },
                {
                    render: function(data, type, row, meta) {
                        return `<button class="btn btn-danger" onclick="deleteProducto(${meta.row})">
                                <i class="fa fa-trash-alt"></i>
                            </button>`
                    }
                }
            ]
        })
    })
</script>
@endsection