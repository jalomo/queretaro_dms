@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : "" }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Lista</li>
    </ol>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_productos" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>No. de pieza</th>
                            <th>Descripcion</th>
                            <th>Precio unitario</th>
                            <th>Precio público (70%)</th>
                            <th>Existencia</th>
                            <th>Unidad</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($listado as $item)    
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->no_identificacion }}</td>
                                <td>{{ $item->descripcion }}</td>
                                <td>$ {{ $item->valor_unitario }}</td>
                                <td>
                                    $ {{ obtenerPorcentaje($item->rel_precio->precio_publico,$item->valor_unitario)}}
                                </td>
                                <td>{{$item->stockActual}}</td>
                                <td>{{ $item->unidad }}</td>
                                <td>
                                    @if ($item->stockActual == 0)
                                    {{ '--' }}
                                    @else
                                    <a class="btn btn-success" href="{{ site_url('refacciones/salidas/ventasVentanillaTaller/'.$item->id.'/'.$menu) }}"> 
                                        <i class="fas fa-shopping-cart"></i> 
                                    </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>No. de pieza</th>
                            <th>Descripcion</th>
                            <th>Precio unitario</th>
                            <th>Precio público (70%)</th>
                            <th>Existencia</th>
                            <th>Unidad</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#tbl_productos').DataTable({})
    });

    $("#tbl_productos").on("click", ".btn-borrar", function() {
        var id = $(this).data('id')
        borrar(id)
    });

    function borrar(id) {
        utils.displayWarningDialog("Desea borrar el registro??", "warning", function(data) {
            if (data.value) {
                ajax.delete(`/api/productos/${id}`, null, function(response, headers) {
                    if (headers.status != 204) {
                        return utils.displayWarningDialog(headers.message)
                    }
                    location.reload(true)
                })

            }
        }, true)
    }

</script>
<script type="text/javascript">
        $(document).ready(function(){
            $("#menu_refacciones").addClass("show");
            $("#refacciones_salidas").addClass("show");
            $("#refacciones_salidas").addClass("active");
            $("#refacciones_salidas_sub").addClass("show");
            $("#refacciones_salidas_sub").addClass("active");
            $(<?php echo isset($menu) ? $menu : "" ?>).addClass("active");
            $("#M02").addClass("active");
        });
</script>
@endsection