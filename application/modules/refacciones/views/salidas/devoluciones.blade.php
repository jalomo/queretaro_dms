@layout('tema_luna/layout')
@section('contenido')
    <div id="app"></div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.0"></script>
    <script src="https://unpkg.com/vue-router@2.0.0"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="/js/components/devolucion.js"></script>
    <script>
        Vue.component('Devolucion', Devolucion)

        var app = new Vue({
            el: "#app",
            data(){
                return {
                    message: 'HOla Mundo'
                }
            },
            template: '<Devolucion />'
        })
    </script>
@endsection