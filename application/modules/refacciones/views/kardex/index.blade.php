@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>

    <form action="{{ base_url('refacciones/kardex') }}" method="post" id="frm-buscar" class="row">
       
        <div class="col-md-6">
            <div class="form-group">
                <label for="select">Tipo movimiento *</label>
                <select name="movimiento_id" class="form-control" id="movimiento_id">
                    <option value=""> Seleccionar</option>
                        @foreach ($cat_movimientos as $item)

                            <option value="{{ $item->id}}"> {{ $item->nombre}} </option>
                        @endforeach
                </select>
                <?php echo form_error('movimiento_id'); ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Fecha inicio *</label>
                <input type="date" name="fecha_inicio" id="fecha_inicio" value="<?php echo set_value('fecha_inicio'); ?>"  class="form-control">
                <?php echo form_error('fecha_inicio'); ?>
            </div>
        </div>
        <div class="col-md-6 ">
            <div class="form-group">
                <label for="">Fecha fin *</label>
                <input type="date" name="fecha_fin" id="fecha_fin" value="<?php echo set_value('fecha_fin'); ?>"  class="form-control">
                <?php echo form_error('fecha_fin'); ?>
            </div>
        </div>
        <div class="col-md-12 mt-4 text-right">
            <button class="btn btn-primary col-md-4" id="btn-buscars" type="submit"> <i class="fa fa-search"></i> Buscar</button>
        </div>
    </form>
    <hr>
</div>
@endsection

@section('scripts')
<script>
    var tabla_productos = $('#tabla_kardex').DataTable({
        "ajax": {
            url: base_url + "refacciones/kardex/ajax_get_lista_kardex",
            type: 'POST',
            data: {
                no_identificacion: function() {
                    return $('#no_identificacion').val()
                }
            }
        },
        columns: [{
                'data': 'id'
            },
            {
                'data': function(data) {
                    return data.no_identificacion
                }
            },
            {
                'data': function(data) {
                    return data.movimiento.nombre
                }
            },
            {
                'data': function(data) {
                    return data.folio.folio
                }
            },
            {
                'data': function(data) {
                    return "<a href='" + site_url + 'refacciones/kardex/detallemovimientos/' + data.id + "' class='btn btn-primary' data-id=" + data.id + "><i class='fas fa-list'></i></a>";
                }
            }
        ]
    });

</script>
@endsection