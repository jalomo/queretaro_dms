@layout('tema_luna/layout')
<style>
    	th{
			font-size:12px !important;
		}
    	td{
			font-size:12px !important;
		}
</style>
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <hr>
    <div class="row">
        <!-- <div class="col-md-12 text-right">
            <a href="#" class="btn btn-primary" onclick="alert('en proceso');">
                <i class="fas fa-file-pdf" aria-hidden="true"></i> Generar PDF
            </a>
        </div> -->
        <div class="col-md-12 mt-2 text-right">
            <a href="{{ base_url('refacciones/kardex/') }}" class="btn btn-primary">
                <i class="fas fa-list" aria-hidden="true"></i> Regresar
            </a>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-4">
            <?php renderInputText("text", "no_identificacion", "No. Identificacion",  isset($kardex->producto->no_identificacion) ? $kardex->producto->no_identificacion : '', true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "descripcion", "Descripcion",  isset($kardex->producto->descripcion) ? $kardex->producto->descripcion : '', true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "precio_unitario", "Precio Unitario",  isset($kardex->producto->valor_unitario) ? $kardex->producto->valor_unitario : '', true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "existencia_inicial", "Existencia Inicial",  isset($kardex->detalle[0]->existe) ? $kardex->detalle[0]->existia : '0', true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "existencia_final", "Existencia Final",  isset($kardex->producto->desglose_producto->cantidad_actual) ? $kardex->producto->desglose_producto->cantidad_actual : '', true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "ubicacion", "Ubicación",  isset($kardex->producto->ubicacion->nombre) ? $kardex->producto->ubicacion->nombre : 'GEN.', true); ?>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered" id="tabla_ventas" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Folio</th>
                        <th>No. Identificación</th>
                        <th>Fecha</th>
                        <th>Movimiento</th>
                        <th>Entrada</th>
                        <th>Salida</th>
                        <th>Exist.</th>
                        <th>Valor Unitario</th>
                        <th>Costo Promedio</th>
                        <th>Total Movimiento</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = $compra = $venta = $salida = $entrada = 0; $total = count($kardex->detalle); ?>
                    @foreach ($kardex->detalle as $item)
                        <tr>
                            <th>{{ ++$i }}</th>
                            <th>{{ $item->folio?? $item->model_id }}</th>
                            <td>{{ $item->producto->no_identificacion }}</td>
                            <td>{{ $item->created_at}}</td>
                            <td>{{ $item->tipo_movimiento->nombre}}</td>
                            <td>
                                @if(in_array($item->tipo_movimiento->id, [1, 3, 5, 6, 11, 12]))
                                    {{ $item->cantidad }}
                                @else
                                    -
                                @endif
                            </td>
                            <td>
                                @if( in_array($item->tipo_movimiento->id, [2, 4, 7, 10, 13]))
                                    {{ $item->cantidad }}
                                @else
                                    -
                                @endif
                            </td>
                            <td>{{ $item->existia }}</td>
                            <td>{{ '$'. number_format($item->precio, 2) }}</td>
                            <td>{{ '$'. number_format($item->costo_promedio, 2) }}</td>
                            <td>{{ '$'. number_format($item->precio * $item->cantidad, 2) }}</td>
                        </tr>
                        @if($i == $total)
                            <th>{{ ++$i }}</th>
                            <td>-</td>
                            <td>{{ $item->producto->no_identificacion }}</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>{{ $item->existe }}</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        @endif
                        <?php
                        
                            if( in_array($item->tipo_movimiento->id, [1, 3, 4, 6, 11])){
                                $compra += $item->precio * $item->cantidad;
                                $entrada += $item->cantidad;
                            }

                            if( in_array($item->tipo_movimiento->id, [2, 5, 7, 10])){
                                $venta += $item->precio * $item->cantidad;
                                $salida += $item->cantidad;
                            }
                        ?>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-6 mt-3">
			<h4 class="text-center font-weight-bold">Movimientos de Inventario</h4>
			<table style="width:100%;" class="table" cellpadding="5">
				<thead>
					<tr>
						<th class="text-center" style="font-size: 11px" scope="col">Entradas</th>
						<th class="text-center" style="font-size: 11px" scope="col">Salidas</th>
						<th class="text-center" style="font-size: 11px" scope="col">Total Movimientos</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-center">{{ $entrada }}</td>
						<td class="text-center">{{ $salida }}</td>
						<td class="text-center">{{ $entrada + $salida }}</td>
					</tr>
				</tbody>
			</table>
		</div>
        <div class="col-6 mt-3">
			<h4 class="text-center font-weight-bold">Saldos</h4>
			<table style="width:100%;" class="table" cellpadding="5">
				<thead>
					<tr>
						<th class="text-center" style="font-size: 11px" scope="col">Saldo en Compras</th>
						<th class="text-center" style="font-size: 11px" scope="col">Saldo en Ventas</th>
						<th class="text-center" style="font-size: 11px" scope="col">Diferencia Compras vs Ventas</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-center">{{ number_format($compra, 2) }}</td>
						<td class="text-center">{{ number_format($venta, 2) }}</td>
						<td class="text-center">{{ number_format($compra - $venta, 2) }}</td>
					</tr>
				</tbody>
			</table>
		</div>
    </div>
</div>
@endsection
