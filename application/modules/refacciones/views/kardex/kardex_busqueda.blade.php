@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>

    <form action="{{ base_url('refacciones/kardex') }}" method="post" id="frm-buscar" class="row">
        <div class="col-md-6">
            <?php renderInputText("text", "no_identificacion", "Numero de producto", '', false); ?>
        </div>
        <div class="col-md-6 mt-4">
            <button class="btn btn-primary col-md-4" id="btn-buscar" type="button"> <i class="fa fa-search"></i> Buscar</button>
        </div>
    </form>
    <div class="row mt-4">
        <div class="col-md-12">
            <table class="table" id="tabla-productos">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">No. de pieza</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Precio unitario</th>
                        <th scope="col">-</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">No. de pieza</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Precio unitario</th>
                        <th scope="col">-</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <hr>
</div>
@endsection

@section('scripts')
<script>
    var tabla_productos = $('#tabla-productos').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        processing: true,
        serverSide: true,
        bFilter: false,
        bSort: false,
        "ajax": {
            url: PATH_API + "api/kardex/productos",
            type: 'GET',

            data: {
                no_identificacion: function() {
                    return $('#no_identificacion').val()
                }
            }
        },
        columns: [
            {
                'data': 'id'
            },
            {
                'data': function(data) {
                    return data.no_identificacion
                }
            },
            {
                'data': function(data) {
                    return data.descripcion
                }
            },
            {
                'data': function(data) {
                    return data.valor_unitario
                }
            },
            {
                'data': function(data) {
                    return "<button class='btn btn-primary btn-modal' data-id=" + data.id + "><i class='fas fa-list'></i></a>";
                }
            }
        ]
    });

    $("#btn-confirmar").on('click',function(){
        let fecha_rango_inicio = $("#fecha_inicio").val()
        let fecha_rango_fin = $("#fecha_fin").val()
        let id = $('#producto_id').val()

        if(fecha_rango_inicio == ''){
            let titulo = "Indica una fecha de inicio!"
           return  utils.displayWarningDialog(titulo, 'warning');
        }

        if(fecha_rango_fin == ''){
            let titulo = "Indica una fecha de limite!"
            return utils.displayWarningDialog(titulo, 'warning');
        }

        let params = new URLSearchParams({
            producto_id: id,
            fecha_inicio: fecha_rango_inicio,
            fecha_termino: fecha_rango_fin,
        })

        window.location = site_url + '/refacciones/kardex/kardexproducto?'+ params.toString()
    });

    $('#tabla-productos').on('click','.btn-modal',function(){
        let id = $(this).data('id');
        $("#modalfechas").modal('show');
        $("#producto_id").val(id);
    });

    $("#btn-buscar").on('click',function(){
        let no_identificacion = $("#no_identificacion").val();

        if(no_identificacion == ''){
            let titulo = "Ingresa numero de producto!"
            return  utils.displayWarningDialog(titulo, 'warning');
        }

        tabla_productos.ajax.reload(function(){
            if(tabla_productos.data().count() < 1){
                utils.displayWarningDialog("No se encontró el producto", 'warning')
            }
        });

    });

</script>
@endsection

@section('modal')
<div class="modal fade" id="modalfechas" tabindex="-1" role="dialog" aria-labelledby="modalcomprasLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="title_modal">Kardex</h5>
        </div>
        <div class="modal-body">
            <div class="col-md-12">
                <?php echo renderInputText("date", "fecha_inicio", "Fecha de inicio", ''); ?>
                <input type="hidden" name="producto_id" id="producto_id">
            </div>
            <div class="col-md-12">
                <?php echo renderInputText("date", "fecha_fin", "Fecha de fin", ''); ?>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button id="btn-confirmar" type="button" class="btn btn-primary">
                <i class="fas fa-list"></i>  Aceptar
            </button>
        </div>
      </div>
    </div>
  </div>
@endsection