@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h2 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h2>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
        <li class="breadcrumb-item"> Rerporte de ventas </li>
    </ol>
    <h1 class="font-weight-bold">
        <i class="fa fa-chart-line"></i>
        Reporte Ventas Mostrador
    </h1>
    <div class="col-md-12 text-right">
        <a class="btn btn-primary mb-4" href="{{ base_url('refacciones/salidas/listadoVentas') }}"><i class="fa fa-arrow-left"></i> Regresar</a>
    </div>
    <form class="border rounded p-3 mb-3">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Vendedor:</label>
                    <select class="form-control select2" id="vendedor_id" name="vendedor_id" style="width: 100%;">
                        <option value="">Todos</option>
                        @if(!empty($vendedores))
                        @foreach ($vendedores as $vendedor)
                        <option value="{{ $vendedor->id}}"> {{ $vendedor->nombre  . ' ' .  $vendedor->apellido_paterno . ' ' . $vendedor->apellido_materno }}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Folio:</label>
                    <input type="text" id="folio" name="folio" class="form-control" />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Fecha inicio:</label>
                    <input type="date" id="fecha_inicio" name="fecha_inicio" class="form-control" />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Fecha fin:</label>
                    <input type="date" id="fecha_fin" name="fecha_fin" class="form-control" />
                </div>
            </div>
            <div class="col-12 text-right">
                <button type="button" id="btn-buscar" onclick="filtrarTablaVentas()" class="btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i> Filtrar
                </button>
                <button type="button" id="download" class="btn btn-primary ml-auto">
                    <i class="fa fa-download"></i>
                    Descargar
                </button>
            </div>
        </div>
    </form>
    <hr>
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-bordered" id="tbl" width="100%" cellspacing="0">
            </table>
        </div>
    </div>
</div>
</div>
@endsection

@section('scripts')
<script>
    $('#tbl').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        // dom: 'Bfrtip',
        // buttons: [
        //     'copy', 'csv', 'excel', 'pdf', 'print'
        // ],
        processing: true,
        serverSide: true,
        searching: false,
        bFilter: false,
        lengthChange: false,
        bInfo: true,
        order: [
            [0, 'asc']
        ],
        "ajax": {
            url: PATH_API + "api/ventas/busqueda-ventas",
            type: 'GET',
            data: function(data) {
                data.tipo_venta_id = 1;
                data.vendedor_id = document.getElementById('vendedor_id').value;
                data.folio = document.getElementById('folio').value;
                data.fecha_inicio = document.getElementById('fecha_inicio').value;
                data.fecha_fin = document.getElementById('fecha_fin').value;
            },
        },
        columns: [{
                title: 'Folio.',
                render: function(data, type, row) {
                    return '<span>' + row.folio + '</span>'
                }
            },
            {
                title: "Cliente",
                render: function(data, type, row) {
                    return '<span>' + row.cliente + '</span>'
                }
            },
            {
                title: "Total",
                render: function(data, type, row) {
                    return '<span>' + row.venta_total + '</span>'
                }
            },
            {
                title: "Vendedor",
                render: function(data, type, row) {
                    return '<span>' + row.vendedor + '</span>'
                }
            },
            {
                title: "Estatus",
                data: 'estatus',
                render: function(data, type, row) {
                    let estatus_venta = row.estatus;
                    if (row.tipo_venta_id == 1) {
                        let estatus_cuentas = ' La venta no se ha mandado al módulo de cajas';
                        if (row.estatus_cuenta_id) {
                            if (jQuery.inArray(row.estatus_cuenta_id, [2, 6, 7]) != -1) {
                                estatus_cuentas = ' La venta se ha liquidado en módulo de cajas';
                            } else if (jQuery.inArray(row.estatus_cuenta_id, [3, 4, 5]) != -1) {
                                estatus_cuentas = ' La venta se encuentra en proceso de pago';
                            } else {
                                estatus_cuentas = ' La venta se encuentrá en el módulo de cajas';
                            }
                        }
                        return estatus_venta + '<br/><small>' + estatus_cuentas + '</small>';
                    } else {
                        return estatus_venta;

                    }
                }
            },
            {
                title: "Fecha",
                data: 'created_at',
                render: function(data, type, row) {
                    return row.fecha_venta ? obtenerFechaMostrar(row.fecha_venta) : obtenerFechaMostrar(row.fecha_venta_created)
                }
            },
        ]
    });

    filtrarTablaVentas = () => {
        $('#tbl').DataTable().ajax.reload()
    }

    $('#download').click(function(event) {
        event.preventDefault()
        let search = {
            tipo_venta_id: 1,
            vendedor_id: document.getElementById('vendedor_id').value,
            folio: document.getElementById('folio').value,
            fecha_inicio: document.getElementById('fecha_inicio').value,
            fecha_fin: document.getElementById('fecha_fin').value
        }

        $.isLoading({
            text: "Descargando Reporte."
        });

        $.ajax({
                url: `${PATH_API}api/ventas/reporte-ventas`,
                type: 'GET',
                data: search,
                xhr: () => {
                    let xhr = new XMLHttpRequest()
                    xhr.onreadystatechange = () => {
                        if (xhr.readyState == 2) {
                            xhr.responseType = "arraybuffer"
                        } else {
                            xhr.responseType = "json"
                        }
                    }

                    return xhr
                }
            })
            .done((response, status, xhr) => {
                let disposition = xhr.getResponseHeader("Content-Disposition")
                let filename = 'reporte-venta-mostrador.pdf'
                let type = xhr.getResponseHeader("Content-Type")

                let blobFile = new Blob([response], {
                    type: type
                })
                let URL = window.URL || window.webkitURL
                let downloadURL = URL.createObjectURL(blobFile)

                if (typeof window.navigator.msSaveBlob !== 'undefined') {
                    window.navigator.msSaveBlob(blobFile, filename)

                    let link = document.createElement('a')
                    link.href = downloadURL
                    link.download = filename
                    document.body.appendChild(link)
                    link.click()
                    return;
                }

                window.open(downloadURL)
            })
            .fail((error) => {
                toastr.error("Ocurrio un error al descargar el reporte.")
            })
            .always(() => {
                $.isLoading('hide');
            })
    })

    function obtenerFechaMostrar(fecha) {
        const dia = 2,
            mes = 1,
            anio = 0;
        fecha = fecha.split('T');
        fecha = fecha[0].split('-');
        return '<span>' + fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio] + '</span>'
    }

    $(".select2").select2();
</script>
@endsection