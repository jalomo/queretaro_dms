@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h2 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h2>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
        <li class="breadcrumb-item"> Listado de ventas </li>
    </ol>
    <div class="row mt-4">
        <div class="col-md-12 text-right">
            <a class="btn btn-primary" href="{{ base_url('refacciones/salidas/reporteventas') }}"><i class="fa fa-file-excel"></i> Reporte de ventas</a>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Folio:</label>
                <input type="text" name="folio" id="folio" class="form-control buscar_enter" value="{{ ($this->input->get('folio')) ? $this->input->get('folio') : '' }}" />
                <div id="folio_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Nombre cliente:</label>
                <input type="text" name="nombre" id="nombre" class="form-control buscar_enter" value="" />
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Apellidos cliente:</label>
                <input type="text" name="apellidos" id="apellidos" class="form-control buscar_enter" value="" />
            </div>
        </div>
      
        <div class="col-md-4 mt-4">
            <button type="button" id="btn-limpiar" onclick="limpiarFiltro()" class="btn btn-primary">
                <i class="fa fa-eraser" aria-hidden="true"></i> Limpiar
            </button>
            <button type="button" id="btn-buscar" onclick="filtrar()" class="btn btn-primary">
                <i class="fa fa-search" aria-hidden="true"></i> Filtrar
            </button>
        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="tabla_ventas" width="100%" cellspacing="0">
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ base_url('js/refacciones/salidas/listadoventas.js') }}"></script>
@endsection