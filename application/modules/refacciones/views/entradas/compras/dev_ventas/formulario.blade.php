@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>
    <div class="row mb-4">
        <div class="col-md-10"></div>
        <div class="col-md-2">
            <a href="{{ base_url('refacciones/entradas/listadoDevoluciones') }}" class="btn btn-primary col-md-12"> Listado</a>
        </div>
    </div>
    <h3>Detalle de venta</h3>
    <div class="row">
        <div class="col-md-6">
            <?php renderInputText("text", "folio", "Folio", isset($detalle_venta) ? $detalle_venta->folio->folio : '', true); ?>
            <input type="hidden" id="folio_id" value="{{ $detalle_venta->folio_id }}">
            <input type="hidden" id="cxc_id" value="{{ $detalle_venta->folio->cuenta_por_cobrar->id }}">
        </div>
        <div class="col-md-6">
            <?php renderInputText("text", "fecha_compra", "Fecha de compra", isset($detalle_venta) ? date('d/m/yy', strtotime($detalle_venta->created_at)) : '', true); ?>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <?php renderInputText("text", "cliente", "Cliente", isset($detalle_venta) ? $detalle_venta->cliente->nombre . ' ' . $detalle_venta->cliente->apellido_paterno . ' ' . $detalle_venta->cliente->apellido_materno : '', true); ?>
            <input type="hidden" name="cliente_id" id="cliente_id" value="{{ isset($detalle_venta) ? $detalle_venta->cliente->id : ''}}">
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?php renderInputText("text", "venta_total", "Total de venta", isset($detalle_venta) ? $detalle_venta->venta_total : '', true); ?>
        </div>
        <div class="col-md-8">

            <?php renderInputText("text", "estatus", "Estatus de venta", isset($detalle_venta->ventas_finalizadas) ? $detalle_venta->ventas_finalizadas->estatus_venta->nombre : '', true); ?>
            <input type="hidden" name="estatus_id" id="estatus_id" value="{{ isset($detalle_venta->ventas_finalizadas)  ? $detalle_venta->ventas_finalizadas->estatus_ventas_id : ''}}">
            <input type="hidden" name="venta_id" id="venta_id" value="{{ isset($detalle_venta) ? $detalle_venta->id : ''}}">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="producto_id_sat">Vendedor:</label>
                <select {{ $detalle_venta->ventas_finalizadas->estatus_ventas_id == 5 ? "disabled": "" }} name="vendedor_id" class="form-control" id="vendedor_id">
                    <option value=""> Seleccionar vendedor</option>
                    @foreach ($cat_vendedor as $vendedor)
                    @if (isset($detalle_devolucion) && $vendedor->id == $detalle_devolucion->vendedor_id)
                    <option selected value="{{ $vendedor->id}}"> {{ $vendedor->nombre . ' ' . $vendedor->apellido_paterno . ' ' .  $vendedor->apellido_materno }}</option>
                    @else
                    <option value="{{ $vendedor->id}}"> {{ $vendedor->nombre . ' ' . $vendedor->apellido_paterno . ' ' .  $vendedor->apellido_materno }}</option>

                    @endif
                    @endforeach
                </select>
                <div id="proveedor_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-12">
            <?php renderInputTextArea("observaciones", "Observaciones",  isset($detalle_devolucion->observaciones) ? $detalle_devolucion->observaciones : '', $detalle_venta->ventas_finalizadas->estatus_ventas_id == 5 ? "disabled" : ""); ?>
        </div>
        <div class="col-md-12 mt-4 text-right">
            @if ($detalle_venta->ventas_finalizadas->estatus_ventas_id == 5)
            <!-- <a href="{{ base_url('refacciones/polizas/generarPolizaDevolucionMostradorPDF?folio_id='.$detalle_venta->folio->id) }}" class="btn btn-primary right" id="btn-imprimir-poliza"> Imprimir poliza</a> -->
            @else
            <button class="btn btn-primary right" id="btn-devolver" type="button"> Generar devolucion</button>
            @endif
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <table class="table" id="tabla-detalle">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Total</th>
                        <th scope="col">Subtotal</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Cantidad</th>
                        <th scope="col">Valor unitario</th>
                        <th scope="col">-</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Total</th>
                        <th scope="col">Subtotal</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Cantidad</th>
                        <th scope="col">Valor unitario</th>
                        <th scope="col">-</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var tabla_compras = $('#tabla-detalle').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        "ajax": {
            url: base_url + "refacciones/entradas/ajax_detalle_productos",
            type: 'POST',
            data: {
                id: function() {
                    return $("#venta_id").val()
                }
            }
        },
        columns: [{
                'data': function(data) {
                    return data.producto_id;
                }
            },
            {
                'data': function(data) {
                    return `$ ${data.venta_total}`;
                }
            },
            {
                'data': function(data) {
                    return data.no_identificacion;
                }
            },
            {
                'data': function(data) {
                    return data.descripcion_producto;
                }
            },
            {
                'data': function(data) {
                    return data.cantidad;
                }
            },
            {
                'data': function(data) {
                    return `$ ${data.valor_unitario}`;
                }
            },
            {
                'data': function(data) {
                    return '--';
                }
            }
        ]
    });
    // btn-devolver
    $("#btn-devolver").on("click", function() {
        var cuenta_por_cobrar_id = $("#cxc_id").val();
        var productos = $('#tabla-detalle').DataTable();
        ajax.post(`api/devolucion-venta`, {
            folio_id: $("#folio_id").val(),
            observaciones: $("#observaciones").val(),
            venta_id: $("#venta_id").val(),
            vendedor_id: $("#vendedor_id").val(),
            total_devolucion: $("#venta_total").val(),
            cliente_id: $("#cliente_id").val(),
        }, function(response, headers) {
            if (headers.status == 201 || headers.status == 200) {
                let titulo = "Devolución realizada correctamente!";
                $.isLoading({
                    text: "Procesando devolución de venta...."
                });
                toastr.info("Procesando devolución de venta espere un momento ...");
                let total = productos.data().count();
                let count = 0;
                toastr.clear()
                $.isLoading("hide");
                ajax.put('api/cuentas-por-cobrar/cancelar-cuenta/' + cuenta_por_cobrar_id, {}, function(response, header) {
                    if (header.status == 200) {
                        utils.displayWarningDialog(titulo, 'success', function(result) {
                            window.location.reload();
                        });
                    }
                });

            }
        })
    });
</script>
@endsection