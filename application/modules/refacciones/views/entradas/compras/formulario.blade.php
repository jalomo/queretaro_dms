@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>
    <div class="row">
        <div class="col-md-12">
            <form id="frm-orden-compra" enctype="multipart/form-data" id="subir_factura">
                <div class="row">
                    <div class="col-md-6">
                    <input type="hidden" name="autorizado" id="autorizado" value="{{ isset($data['detalle_pedido']->autorizado) ? 1 : 0  }}">
                        <?php renderInputText("date", "fecha_pago", "Fecha de pago",  isset($data->fecha_pago) ? $data->fecha_pago : ''); ?>
                    </div>
                    <div class="col-md-6">
                        <?php renderInputText("date", "fecha", "Fecha",  isset($data->fecha) ? $data->fecha : ''); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="proveedor">Proveedor:</label>
                            <select name="proveedor_id" class="form-control" id="proveedor_id">
                                <option value=""> Seleccionar proveedor</option>
                                @foreach ($cat_proveedor as $proveedor)
                                <option value="{{ $proveedor->id}}"> {{ $proveedor->proveedor_numero }} - {{ $proveedor->proveedor_nombre }}</option>
                                @endforeach
                            </select>
                            <div id="proveedor_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php renderInputText("text", "factura", "Folio de factura",  isset($data->factura) ? $data->factura : '', false); ?>
                    </div>
                    <div class="col-md-12">
                        <?php renderInputTextArea("observaciones", "Observaciones",  isset($data->observaciones) ? $data->observaciones : '', false); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="factura">Factura de compra</label>
                            <input type="file" class="form-control-file" id="archivo_factura" name="archivo_factura">
                            <div id="archivo_factura_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="factura">Archivo CSV</label>
                            <input type="file" class="form-control-file" id="archivo_csv" name="archivo_csv">
                            <div id="archivo_csv_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-right">
                            <button id="btn-orden-compra" class="btn btn-primary col-md-3" type="button"> Continuar </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script>
    if(parseInt(document.getElementById('autorizado').value) == 0){
        utils.displayWarningDialog("Pedido no autorizado", 'warning', function(result) {
            return window.location.href = base_url + "refacciones/productos/crearPedido";
        });

    }
    $(document).ready(function() {
        $("#proveedor_id").select2();
        $("#tipo_pago_id").select2();
        $("#btn-orden-compra").on('click', function() {
            $.isLoading({ text: "Procesando orden compra...." });
            let factura = $('#archivo_factura')[0].files[0];
            let csv = $('#archivo_csv')[0].files[0];

            var paqueteDeDatos = new FormData();
            paqueteDeDatos.append('fecha_pago', $("#fecha_pago").val());
            paqueteDeDatos.append('fecha', $("#fecha").val());
            paqueteDeDatos.append('proveedor_id', $("#proveedor_id").val());
            paqueteDeDatos.append('observaciones', $("#observaciones").val());

            if (utils.isDefined(factura) && factura){
                paqueteDeDatos.append('archivo_factura', factura);
                paqueteDeDatos.append('factura', $("#factura").val());
                ajax.postFile(`api/orden-compra/factura`, paqueteDeDatos, function(response, header) {
                    return responseOrdenCompra(response, header)
                })

                return;
            } 

            if(csv){
                paqueteDeDatos.append('archivo_csv', csv)

                ajax.postFile(`api/orden-compra/csv`, paqueteDeDatos, function(response, header){
                    try{
                        console.log(response)
                        let type = header.status == 200? 'success' : 'warning'

                        utils.displayWarningDialog(response.message, type, function() {})
                        $.isLoading('hide')
                        
                        if(header.status == 200){
                            utils.displayWarningDialog('Orden de compra cargada con exito!', 'success', function() {
                                window.location.href = `${base_url}refacciones/entradas/compra/${response.id}`
                            })
                        }
                    } catch{
                        $.isLoading('hide')

                        utils.displayWarningDialog('Ocurrio un error al guardar el orden de compra.', 'warning', function(result) {});
                    }
                })

                return
            }
            
            ajax.postFile(`api/orden-compra`, paqueteDeDatos, function(response, header) {
                return responseOrdenCompra(response, header)
            });
        })
    });
    function responseOrdenCompra(response, header){
                if(header.status == 400){
                    let data = JSON.parse(header.message);
                    $.isLoading( "hide" );
                    if(utils.isDefined(data.error))
                        utils.displayWarningDialog(data.error, 'warning', function(result) {});
                }

                if (header.status == 200) {
                    $("#btn-orden-compra").hide();
                    let titulo = "Orden de compra cargada con exito!"
                    $.isLoading( "hide" );
                    utils.displayWarningDialog(titulo, 'success', function(result) {
                        window.location.href = base_url + "refacciones/entradas/compra/"+response.id;
                    });
                }
                
                if (header.status == 500) {
                    let titulo = "Ha ocurrido un problema!"
                    $.isLoading( "hide" );
                   utils.displayWarningDialog(titulo, 'warning', function(result) {});
                }
            }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#menu_refacciones").addClass("show");
        $("#refacciones_entradas").addClass("show");
        $("#refacciones_entradas").addClass("active");
        $("#tables").addClass("show");
        $("#tables").addClass("active");
        $("#menu_entradas_compras").addClass("active");
        $("#M02").addClass("active");
    });
</script>
@endsection