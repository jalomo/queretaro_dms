@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>
    <h4>Estatus de la compra : <?php echo isset($orden_compra) && isset($orden_compra->re_compras_estatus) ? $orden_compra->re_compras_estatus->estatus_compra->nombre : ''; ?></h4>
    <div class="row mt-2">
        <div class="col-md-4">
            <?php echo renderInputText("text", "folio", "Folio", isset($orden_compra->folios) ? $orden_compra->folios->folio : '', true); ?>
            <input type="hidden" value="{{ isset($orden_compra->folio_id) ? $orden_compra->folio_id : '' }}" id="folio_id">
        </div>
        <div class="col-md-4">
            <?php echo renderInputText("number", "total", "Total", isset($total) ? $total->total : '', true); ?>
        </div>
        <div class="col-md-4">
            <?php echo renderInputText("text", "proveedor", "Proveedor", isset($orden_compra->proveedor) ? $orden_compra->proveedor->proveedor_nombre : '', true); ?>
            <input type="hidden" value="{{ isset($orden_compra->proveedor_id) ? $orden_compra->proveedor_id : '' }}" id="proveedor_id" name="proveedor_id">
            <input type="hidden" value="{{ isset($orden_compra->fecha_pago) ? $orden_compra->fecha_pago : '' }}" id="fecha_pago" name="fecha_pago">
            <input type="hidden" value="{{ isset($orden_compra->re_compras_estatus->estatus_compra_id) ? $orden_compra->re_compras_estatus->estatus_compra_id : '' }}" id="estatus_compra" name="estatus_compra">
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="select">Concepto</label>
                <textarea name="concepto" id="concepto" class="form-control" rows="3" style="min-height:100px" maxlength="500"><?php echo isset($cxp->concepto) ? $cxp->concepto : ''; ?></textarea>
                <div id='concepto_error' class='invalid-feedback'></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="select">¿Compra de credito ó contado?</label>
                <select name="tipo_forma_pago_id" class="form-control " id="tipo_forma_pago_id">
                    <option value=""> Seleccionar</option>
                    @foreach ($tipo_forma_pago as $tipo)
                    @if(isset($cxp->tipo_forma_pago_id) && $cxp->tipo_forma_pago_id == $tipo->id)
                    <option value="{{ $tipo->id}}" selected="selected"> {{ $tipo->descripcion}} </option>
                    @else
                    <option value="{{ $tipo->id}}"> {{ $tipo->descripcion}} </option>
                    @endif
                    @endforeach
                </select>
                <div id='tipo_forma_pago_id_error' class='invalid-feedback'></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="select">¿De que manera realizará el pago?</label>
                <select name="tipo_pago_id" class="form-control " id="tipo_pago_id">
                    <option value=""> Seleccionar</option>
                    @foreach ($tipo_pago as $tipo)
                    @if(isset($cxp->tipo_pago_id) && $cxp->tipo_pago_id == $tipo->id)
                    <option value="{{ $tipo->id}}" selected="selected"> {{ $tipo->clave}} {{ $tipo->nombre}} </option>
                    @else
                    <option value="{{ $tipo->id}}"> {{ $tipo->clave}} {{ $tipo->nombre}} </option>
                    @endif
                    @endforeach
                </select>
                <div id='tipo_pago_id_error' class='invalid-feedback'></div>
            </div>
        </div>
        <div class="col-md-4 container-forma-pago">
            <div class="form-group">
                <label for="select">Plazo de credito</label>
                <select name="plazo_credito_id" class="form-control " id="plazo_credito_id">
                    <option value=""> Seleccionar</option>
                    @foreach ($plazo_credito as $plazo)
                    @if(isset($cxp->plazo_credito_id) && $cxp->plazo_credito_id == $plazo->id)
                    <option value="{{ $plazo->id}}" selected="selected"> {{ $plazo->nombre}} </option>
                    @else
                    <option value="{{ $plazo->id}}"> {{ $plazo->nombre}} </option>
                    @endif
                    @endforeach
                </select>
                <div id='plazo_credito_id_error' class='invalid-feedback'></div>
            </div>
        </div>
        <!-- SE QUITA A PETICIÓN DE HORACIO EN CUENTAS POR PAGAR NO SE DA ENGANCHE A PROVEEDORES -->
        <!-- <div class="col-md-4 container-forma-pago">
			<div class="form-group">
				<label for="select">Enganche</label>
				<input type="text" name="enganche" id="enganche" class="form-control" value="<?php echo isset($cxp->enganche) ? $cxp->enganche : ''; ?>"/>
				<div id='enganche_error' class='invalid-feedback'></div>
			</div>
		</div> -->
        <div class="col-md-4 container-forma-pago">
            <div class="form-group">
                <label for="select">Tasa de interes %</label>
                <input type="text" name="tasa_interes" id="tasa_interes" class="form-control" value="<?php echo isset($cxp->tasa_interes) ? $cxp->tasa_interes : ''; ?>" />
                <div id='tasa_interes_error' class='invalid-feedback'></div>
            </div>
        </div>
    </div>
    <div class="row">
        @if ($orden_compra->re_compras_estatus->estatus_compra_id == 2)
        <div class="col-md-12 text-right">
            <a href="{{ base_url('refacciones/polizas/generarPolizaComprasPDF?folio_id='.$orden_compra->folio_id.'&proveedor_id='.$orden_compra->proveedor_id) }}" class="btn btn-primary col-md-3 px-3" id="btn-genera-poliza"> <i class="far fa-file"></i> Poliza</a>
        </div>
        @endif
        @if ($orden_compra->re_compras_estatus->estatus_compra_id == 1)
        <div class="col-md-12 text-right">
            <button class="btn btn-primary col-md-3 pb-2 pt-2" type="button" id="btn-comprar"> <i class="fas fa-shopping-cart"></i> Finalizar</button>
        </div>
        @endif
    </div>
    <br>

    <h3 class="mt-5">Productos agregados</h3>
    <hr />
    <div class="row">
        <div class="col-md-12 mt-2">
            <input type="hidden" value="{{ $orden_compra->id }}" name="orden_compra_id" id="orden_compra_id">
            <table class="table" id="tabla-carrito-compras">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">No. de pieza</th>
                        <th scope="col">Clave unidad</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Cantidad</th>
                        <th scope="col">Precio unitario</th>
                        <th scope="col">Total</th>
                        <th scope="col">Ubicación</th>
                        <th scope="col">-</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">No. de pieza</th>
                        <th scope="col">Clave unidad</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Cantidad</th>
                        <th scope="col">Precio unitario</th>
                        <th scope="col">Total</th>
                        <th scope="col">Ubicación</th>
                        <th scope="col">-</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <hr>
      
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(".container-forma-pago").hide();

    if ($("#tipo_forma_pago_id").val() == 2) {
        $(".container-forma-pago").show();
    }

    $("#tipo_forma_pago_id").on("change", function() {
        let tipo_forma_pago_id = $("#tipo_forma_pago_id").val();
        if (tipo_forma_pago_id == 2) {
            $(".container-forma-pago").show();
            $("#plazo_credito_id option[value='']").attr('selected', true);
        } else {
            $(".container-forma-pago").hide();
            $("#plazo_credito_id option[value=14]").attr('selected', true);
        }
    });

    $("#ubicacion_producto_id").select2({
        dropdownParent: $("#modal_ubicacion")
    });
    var tabla_stock = $('#tabla_stock').DataTable({
		language: {
			url: PATH_LANGUAGE
		},
		"ajax": {
			url: PATH_API + "api/productos/listadoStock",
			type: 'GET',
		},
		columns: [{
				title: "#",
				data: 'id',
			},
			{
				title: "No. de pieza",
				data: 'no_identificacion',
			},
			{
				title: "Descripcion",
				data: 'descripcion',
			},
			{
				title: "Precio unitario",
				data: 'valor_unitario',
			},
			{
				title: "Unidad",
				data: 'unidad',
            },
            {
                title: "-",
                render: function (data, type, row) {
                    return  '<button  class="btn btn-primary add-carrito" onclick="agregarCarrito(this)" data-toggle="modal" data-target="#modalcompras" data-idproducto="' + row.id + '" data-precio="' + row.valor_unitario + '"><i class="fas fa-shopping-cart"></i> </button>';
				}
            }
        ]
	});
    let orden_compra_id = $('#orden_compra_id').val();
    var tabla_compras = $('#tabla-carrito-compras').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        ajax: base_url + "refacciones/entradas/ajax_productos_carrito/" + $("#orden_compra_id").val(),
        columns: [{
                'data': function(data) {
                    return data.productos.id;
                }
            },
            {
                'data': function(data) {
                    return data.productos.no_identificacion;
                }
            },
            {
                'data': function(data) {
                    return data.productos.clave_unidad;
                }
            },
            {
                'data': function(data) {
                    return data.productos.descripcion;
                }
            },
            {
                'data': function(data) {
                    return data.productos_orden_compra.cantidad;
                }
            },
            {
                'data': function(data) {
                    return data.productos.valor_unitario;
                }
            },
            {
                'data': function(data) {
                    return data.productos_orden_compra.total;
                }
            },
            {
                'data': function(data) {
                    return utils.isDefined(data.productos.ubicacion) && data.productos.ubicacion.nombre ? data.productos.ubicacion.nombre : null;
                }
            },
            {
                'data': function(data) {
                    if (parseInt($("#estatus_compra").val()) > 1) {
                        return '--'
                    } else {
                        btn_eliminar =  "<button data-id='" + data.productos_orden_compra.id + "' class='btn btn-danger btn-quitar'><i class='fas fa-trash'></i> </button>";
                        btn_ubicacion =  "<button  data-toggle='modal' onclick='modalUbicacion(this)' data-target='#modal_ubicacion' data-producto_id='" + data.productos.id + "' data-ubicacion_producto_id='" + data.productos.ubicacion_producto_id + "' class='btn btn-primary'><i class='fas fa-edit'></i> </button>";
                        return btn_eliminar + ' '+ btn_ubicacion;
                    }
                }
            }
        ],
        "createdRow": function (row, data, dataIndex) {
            console.log(data);
            if (utils.isDefined(data.productos.ubicacion) && data.productos.ubicacion.nombre) {
				$(row).find('td:eq(7)').css('background-color', '#8cdd8c');
			} else{
                $(row).find('td:eq(7)').css('background-color', '#e37f7f');

            }

		}
    });

    $("#btn-comprar").on("click", function() {
        $.isLoading({ text: "Procesando información...." });

        var productosAgregados = $('#tabla-carrito-compras').DataTable();

        if (productosAgregados.data().count() == 0) {
            $.isLoading("hide");
            toastr.error("Favor de agregar al menos un producto");
            return false;
        }
        var totales = new Array();
        var ubicaciones = new Array();
        var tot_ubicaciones = 0;
        var cantidad = 0;
        $(productosAgregados.data()).each(function(index, product) {
            totales[index] = (product.productos.id);
            if (utils.isDefined(product.productos.ubicacion_producto_id)) {
                tot_ubicaciones = tot_ubicaciones + 1;
            }
        });
        if (totales.length != tot_ubicaciones) {
            cantidad = totales.length - tot_ubicaciones;
            $.isLoading("hide");
            utils.displayWarningDialog("Falta indicar la ubicación de " + cantidad + " productos", 'info', function() {
            });
            return false;
        }
       

        let orden_compra_id = $("#orden_compra_id").val();
        ajax.post('api/cuentas-por-pagar', {
                folio_id: $("#folio_id").val(),
                proveedor_id: $("#proveedor_id").val(),
                estatus_cuenta_id: 1,
                concepto: 'Mostrador - ' + $("#concepto").val(),
                tipo_forma_pago_id: $("#tipo_forma_pago_id").val(),
                tipo_pago_id: $("#tipo_pago_id").val(),
                plazo_credito_id: $("#plazo_credito_id").val(),
                importe: $("#total").val(),
                enganche: null,
                tasa_interes: $("#tasa_interes").val(),
                fecha: $("#fecha_pago").val(),
                total: $("#total").val()
            },
            function(response, headers) {
                if (headers.status == 201) {
                    ajax.post('api/re-estatus-compra', {
                        orden_compra_id: orden_compra_id,
                        estatus_compra_id: 2
                    }, function(response, header) {
                        console.log(header);
                        if (headers.status == 201) {
                            $.isLoading("hide");
                            let titulo = "Orden compra realizada correctamente ...."
                            ajax.put('api/orden-compra/update-inventario-por-orden/' + orden_compra_id, {}, function(response, header) {
                                utils.displayWarningDialog(titulo, 'success', function(result) {
                                    window.location.href = PATH + "/refacciones/entradas/detalleOrden/" + orden_compra_id
                                });
                            });
                        } else {
                            $.isLoading("hide");
                            toastr.error("Ocurrio un error al intentar procesar la petición");
                        }
              
                        // toastr.info("Actualizando inventario espere un momento....");
                        // let total = productosAgregados.data().count();
                        // let count = 0;
                        // $(productosAgregados.data()).each(function(index, product) {
                        //     ajax.get('api/desglose-producto/actualizaStockByProducto?producto_id=' + product.productos.id, {}, function(response, headers) {
                        //         if (headers.status == 200) {
                        //             count++;
                        //             toastr.info("Inventario actualizado para el producto " + product.productos.descripcion);
                        //         }
                        //         if (total == count) {
                        //             $.isLoading("hide");
                        //             $("#btn-comprar").hide();
                        //             utils.displayWarningDialog(titulo, 'success', function(result) {
                        //                 window.location.href = PATH + "/refacciones/entradas/detalleOrden/" + orden_compra_id
                        //             });
                        //         }
                        //     })
                        // });
                    });
                }
            })
    });


    $("#tabla-carrito-compras").on("click", ".btn-quitar", function() {
        var id = $(this).data('id');
        ajax.delete(`api/productos-orden-compra/${id}`, null, function(response, headers) {
            if (headers.status == 204) {
                return utils.displayWarningDialog("Producto eliminado del carrito de compras!", 'warning', function(data) {
                    tabla_compras.ajax.reload();
                    calculaTotal();
                });
            }
        })
    });

    function calculaTotal() {
        var orden_compra_id = $("#orden_compra_id").val();
        ajax.get('api/productos-orden-compra/total-by-orden-compra/' + orden_compra_id, {}, function(response, headers) {
            if (headers.status == 200) {
                $("#total").val(response.total);
            }
        })
    }

    function agregarCarrito(_this) {
        let producto_id = $(_this).data('idproducto');
        let precio = $(_this).data('precio');

        $("#producto_id").val(producto_id);
        $("#precio").val(precio);
    }

    function modalUbicacion(_this) {
        let producto_id = $(_this).data('producto_id');
        let ubicacion_producto_id = $(_this).data('ubicacion_producto_id');

        $("#producto_id_modalubicacion").val(producto_id);
        $("#ubicacion_producto_id").val(ubicacion_producto_id).trigger('change');
    }

    function agregarproducto() {
        let orden_compra_id = $("#orden_compra_id").val();
        let cantidad = $("#cantidad").val();
        let precio = $("#precio").val();
        let producto_id = $("#producto_id").val();
        $("#modalcompras").modal('hide');
        ajax.post(`api/productos-orden-compra`, {
            producto_id,
            orden_compra_id,
            precio,
            cantidad,
        }, function(response, headers) {
            if (headers.status == 201 || headers.status == 200) {
                let titulo = "Producto agregado!"
                utils.displayWarningDialog(titulo, 'success', function(result) {
                    tabla_compras.ajax.reload();
                    calculaTotal();
                });

            }
            if (headers.status == 500) {
                let titulo = "Error traspasando producto!"
                utils.displayWarningDialog(titulo, 'warning', function(result) {});
            }
        })
    }

    function guardarUbicacion() {
        let producto_id = $("#producto_id_modalubicacion").val();
        toastr.info('Actualizando ubicación del producto')

		ajax.put(`api/productos/ubicacion/${producto_id}`, {
			ubicacion_producto_id: $('#ubicacion_producto_id').val(),
		}, function(response, headers) {
			if (headers.status == 200) {
                $("#modal_ubicacion").modal('hide');
                $('table#tabla-carrito-compras').DataTable().ajax.reload();
			}
		})
    }
</script>
@endsection



@section('modal')
<div class="modal fade" id="modalcompras" tabindex="-1" role="dialog" aria-labelledby="modalcomprasLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo renderInputText("number", "cantidad", "Cantidad", 1); ?>
                        <input type="hidden" name="producto_id" id="producto_id">
                        <input type="hidden" name="precio" id="precio">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button onclick="agregarproducto()" id="btn-modal-agregar" type="button" class="btn btn-primary"><i class="fas fa-shopping-cart"></i> Agregar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_ubicacion" tabindex="-1" role="dialog" aria-labelledby="modal_ubicacionLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal">Ubicación del producto</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <select name="ubicacion_producto_id" id="ubicacion_producto_id" class="form-control" style="width:100%">
                            <option value=""> Seleccionar</option>
                            @foreach ($ubicacion_producto as $ubicacion)
                            <option value="{{ $ubicacion->id}}"> {{ $ubicacion->nombre}} </option>
                            @endforeach
                        </select>
                        <input type="hidden" id="producto_id_modalubicacion">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button onclick="guardarUbicacion()" id="btn-modal-agregar" type="button" class="btn btn-primary"><i class="fas fa-save"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>
@endsection