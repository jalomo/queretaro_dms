@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">Productos</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
        <li class="breadcrumb-item active">Pedido de piezas</li>
    </ol>
    <div class="row">
        <div class="col-md-10 mb-4">
            <h2>Pedidos</h2>
        </div>
        <div class="col-md-2 mb-4">
            <input type="hidden" id="id_usuario_autorizo" value="{{  $this->session->userdata('id') }}">
            <input type="hidden" id="rol_id" value="{{  $this->session->userdata('rol_id') }}">
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="estatus_id">Estatus de pedido</label>
                <select class="form-control" name="estatus_id" id="estatus_id">
                    <option value=""> Seleccionar ..</option>
                    <option value="1">Proceso </option>
                    <option value="4">Backorder</option>
                    <option value="2">Finalizado</option>
                </select>
                <div id="estatus_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-2 mt-4">
            <button class="btn col-md-12 btn-primary" onclick="filtrarLista()"> Filtrar </button>
        </div>
        <div class="col-md-2 mt-4">
            <button class="btn btn-primary col-md-12" onclick="limpiarBusqueda()"> Limpiar </button>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tabla_pedido" width="100%" cellspacing="0"></table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    let tabla = $('#tabla_pedido').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        "ajax": {
            url: PATH_API + "api/masterpedidoproducto/filtrar",
            type: 'POST',
            data: {
                estatus_id: function() {
                    return $('#estatus_id').val()
                }
            }
        },
        columns: [{
                title: "#",
                data: 'id',
            },
            {
                title: "Autorizado",
                data: ({
                    autorizado
                }) => autorizado ? 'Autorizado' : 'Sin autorizar'
            },
            {
                title: "Quien autorizo",
                data: function({
                    rel_usuario
                }) {
                    if (rel_usuario) {
                        let {
                            nombre,
                            apellido_paterno,
                            apellido_materno
                        } = rel_usuario;
                        return `${nombre} ${apellido_paterno} ${apellido_materno} `
                    } else {
                        return '-'
                    }
                }
            },
            {
                title: "Fecha",
                data: ({
                    created_at
                }) => utils.dateToLetras(created_at),
            },
            {
                title: "-",
                data: function({
                    id,
                    autorizado
                }) {
                    let rol_id = parseInt(document.getElementById('rol_id').value);
                    let estatus_id = document.getElementById('estatus_id').value;
                    
                    let btn_detalle = "<a class='btn btn-primary' href='" + base_url + 'refacciones/entradas/pedidopiezas/' + id + "' ><i class='fas fa-list'></i></a>";
                    if (autorizado && estatus_id != 2) {
                        let btn_cargar_factura = `<a class='btn btn-primary' href="${base_url}refacciones/entradas/ordencompra?pedido_id=${id}"><i class='far fa-folder'></i></a>`;
                        btn_detalle += btn_cargar_factura
                    }

                    if ([1, 2, 3, 4].includes(rol_id) && !autorizado) {
                        let btn_autorizar = " <button onclick='openmodalautorizar(" + id + ")' class='btn btn-primary' ><i class='fas fa-user'></i></button>";
                        btn_detalle += btn_autorizar
                    }
                    return btn_detalle
                }

            }
        ],
        "createdRow": function(row, {
            autorizado,
            finalizado
        }) {

            if (autorizado) {
                $(row).find('td:eq(1)').css('background-color', '#8cdd8c');
            } else {
                $(row).find('td:eq(1)').css('background-color', '#db524d80');
            }

        }
    });
    let limpiarBusqueda = ()=>{
        $("#estatus_id").val('');
        tabla.ajax.reload();
    }

    let filtrarLista = () => {
        let estatus_id = document.getElementById('estatus_id').value;
        tabla.ajax.reload();
    }

    $("#btn_pedido").on('click', function() {
        ajax.post('api/masterpedidoproducto', {
            "autorizado": 0
        }, function(response, headers) {
            if (headers.status == 201 || headers.status == 200) {
                utils.displayWarningDialog("Creando pedido", "success", function(data) {
                    return window.location.href = base_url + 'refacciones/productos/pedidopiezas/' + response.id;
                })
            }
        });

    })

    let finalizarpedido = (id) => {
        utils.displayWarningDialog("Finalizar pedido", "warning", function(data) {
            if (data && data.value) {
                ajax.put('api/masterpedidoproducto/' + id, {
                    "finalizado": 1
                }, function(response, headers) {
                    tabla.ajax.reload();
                });
            }
        }, true)
    }


    openmodalautorizar = (id) => {
        $("#ma_pedido_id").val(id)
        $("#modalautorizacion").modal('show');
    }


    const validarLogin = async () => {
        return new Promise((resolve, reject) => {
            ajax.post(`api/usuarios/login`, {
                usuario: document.getElementById('usuario').value,
                password: document.getElementById('password').value
            }, function(response, headers) {
                if (headers.status == 201 || headers.status == 200) {
                    $("#modalcambioestatus").modal('hide');
                    $("#frmautorizar")[0].reset();
                    resolve(response)
                }

                if (headers.status == 400) {
                    reject('Parametros invalidos')
                }
            })
        })


    }

    validarAutorizar = () => {

        if (document.getElementById('usuario').value == '') {
            toastr.error("LLene la informacion para autorizar!")
            return false;
        }

        if (document.getElementById('password').value == '') {
            toastr.error("LLene la informacion para autorizar!")
            return false;
        }

        if (document.getElementById('ma_pedido_id').value == '') {
            toastr.error("No se ha cargado la informacion!")
            return false;
        }

        validarLogin()
            .then(response => {
                let id = $("#ma_pedido_id").val()
                ajax.put(`api/masterpedidoproducto/${id}`, {
                    "autorizado": 1,
                    "id_usuario_autorizo": 1
                }, function(response, headers) {
                    if (headers.status == 201 || headers.status == 200) {
                        $("#modalautorizacion").modal('hide');
                        $("#frmautorizar")[0].reset();
                        window.location.reload();
                    }
                })
            })
            .catch(err => toastr.error("no se ha autorizado"))
    }
</script>
@endsection


@section('modal')
@include('productos/pedidos/modalautorizar')
@endsection