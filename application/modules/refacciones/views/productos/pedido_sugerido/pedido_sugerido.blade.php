@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
        <li class="breadcrumb-item active">{{ $bread_active}}</li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <div id="inventariochart" style="width: 900px; height: 500px;"></div>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-md-10">
            <h3>Demanda promedio</h3>
            <h4>Minimo 4 demandas en 5 meses</h4>
        </div>
        <div class="col-md-2">
            <h3>Seccion A</h3>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered" id="tbl_pedido_sugerido" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>No identificación</th>
                        <th>Producto</th>
                        <th>Venta promedio</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>No identificación</th>
                        <th>Producto</th>
                        <th>Venta promedio</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="row mb-4">
        <div class="col-md-10">
            <h3>Demanda Lenta</h3>
            <h4>1 a 3 demandas en 5 meses</h4>
        </div>
        <div class="col-md-2">
            <h3>Seccion B</h3>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered" id="tbl_sugerido_lento" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>No identificación</th>
                        <th>Producto</th>
                        <th>Venta promedio</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>No identificación</th>
                        <th>Producto</th>
                        <th>Venta promedio</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-md-10">
            <h3>Inactivas</h3>
            <h4>Sin ventas en los ultimos 30 dias</h4>
        </div>
        <div class="col-md-2">
            <h3>Seccion C</h3>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered" id="tbl_sugerido_inactivo" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>No identificación</th>
                        <th>Producto</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>No identificación</th>
                        <th>Producto</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="row mb-4">
        <div class="col-md-10">
            <h3>Potencial Obsoletas</h3>
            <h4>Sin ventas en los ultimos 31 dias</h4>
        </div>
        <div class="col-md-2">
            <h3>Seccion C</h3>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered" id="tbl_sugerido_potencial_obsoleto" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>No identificación</th>
                        <th>Producto</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>No identificación</th>
                        <th>Producto</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="row mb-4">
        <div class="col-md-10">
            <h3>Estancado</h3>
            <h4>Sin ventas en 5 meses + 1 dia</h4>
        </div>
        <div class="col-md-2">
            <h3>Seccion C</h3>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered" id="tbl_sugerido_estancado" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>No identificación</th>
                        <th>Producto</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>No identificación</th>
                        <th>Producto</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="row mb-4">
        <div class="col-md-10">
            <h3>Obsoletas</h3>
            <h4>Sin ventas en el ultimo año</h4>
        </div>
        <div class="col-md-2">
            <h3>Seccion C</h3>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered" id="tbl_sugerido_obsoleto" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>No identificación</th>
                        <th>Producto</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>No identificación</th>
                        <th>Producto</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawChart);

    const grafica_values = () => {
        return new Promise((resolve, reject) => {
            ajax.get('api/pedido-sugerido/graficavalues', {}, function(response, headers) {
                if (headers.status == 200) {
                    resolve(response);
                } else {
                    reject("no se puede cargar informacion");
                }
            });
        });
    }

    function drawChart() {
        let grapValues = [];
        grafica_values().then(response => {
            let {
                seccion_a,
                seccion_b,
                seccion_c
            } = response;

            var data = google.visualization.arrayToDataTable([
                ['Task', 'Hours per Day'],
                ['A', seccion_a],
                ['B', seccion_b],
                ['C', seccion_c]
            ]);

            var options = {
                title: 'Administracion de inventario',
                colors: ['#008080', '#ffff01', '#ff0202']
            };

            var chart = new google.visualization.PieChart(document.getElementById('inventariochart'));
            chart.draw(data, options);
        })

    }
</script>
<script>
    $('#tbl_sugerido_lento').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        "ajax": {
            url: base_url + "refacciones/productos/ajax_sugerido_lento",
            type: 'GET',
        },
        columns: [{
                'data': 'producto_id'
            },
            {
                'data': 'no_identificacion'
            },
            {
                'data': 'nombre_producto'
            },
            {
                'data': 'promedio'
            }
        ],
        "createdRow": function(row, data, dataIndex) {
            $(row).find('td:eq(0)').css('background-color', 'yellow');
            $(row).find('td:eq(1)').css('background-color', 'yellow');
            $(row).find('td:eq(2)').css('background-color', 'yellow');
            $(row).find('td:eq(3)').css('background-color', 'yellow');
        }
    });

    $('#tbl_sugerido_estancado').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        "ajax": {
            url: base_url + "refacciones/productos/ajax_sugerido_estancado",
            type: 'GET'
        },
        columns: [{
                'data': 'producto_id'
            },
            {
                'data': 'no_identificacion'
            },
            {
                'data': 'nombre_producto'
            }
        ],
        "createdRow": function(row, data, dataIndex) {
            $(row).find('td:eq(0)').css('background-color', '#ff000075');
            $(row).find('td:eq(1)').css('background-color', '#ff000075');
            $(row).find('td:eq(2)').css('background-color', '#ff000075');
        }
    });

    $('#tbl_sugerido_potencial_obsoleto').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        "ajax": {
            url: base_url + "refacciones/productos/ajax_sugerido_potencial_obsoleto",
            type: 'GET'
        },
        columns: [{
                'data': 'producto_id'
            },
            {
                'data': 'no_identificacion'
            },
            {
                'data': 'nombre_producto'
            }
        ],
        "createdRow": function(row, data, dataIndex) {
            $(row).find('td:eq(0)').css('background-color', '#ff000075');
            $(row).find('td:eq(1)').css('background-color', '#ff000075');
            $(row).find('td:eq(2)').css('background-color', '#ff000075');
        }
    });

    $('#tbl_sugerido_obsoleto').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        "ajax": {
            url: base_url + "refacciones/productos/ajax_sugerido_obsoleto",
            type: 'GET'
        },
        columns: [{
                'data': 'producto_id'
            },
            {
                'data': 'no_identificacion'
            },
            {
                'data': 'nombre_producto'
            }
        ],
        "createdRow": function(row, data, dataIndex) {
            $(row).find('td:eq(0)').css('background-color', '#ff000075');
            $(row).find('td:eq(1)').css('background-color', '#ff000075');
            $(row).find('td:eq(2)').css('background-color', '#ff000075');
        }
    });


    $('#tbl_sugerido_inactivo').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        "ajax": {
            url: base_url + "refacciones/productos/ajax_sugerido_inactivo",
            type: 'GET'
        },
        columns: [{
                'data': 'producto_id'
            },
            {
                'data': 'no_identificacion'
            },
            {
                'data': 'nombre_producto'
            }
        ],
        "createdRow": function(row, data, dataIndex) {
            $(row).find('td:eq(0)').css('background-color', '#ff000075');
            $(row).find('td:eq(1)').css('background-color', '#ff000075');
            $(row).find('td:eq(2)').css('background-color', '#ff000075');
        }
    });

    $('#tbl_pedido_sugerido').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        "ajax": {
            url: base_url + "refacciones/productos/ajax_pedido_sugerido",
            type: 'GET'
        },
        columns: [{
                'data': 'producto_id'
            },

            {
                'data': 'no_identificacion'
            },
            {
                'data': 'nombre_producto'
            },
            {
                'data': 'promedio'
            }
        ],
        "createdRow": function(row, data, dataIndex) {
            $(row).find('td:eq(0)').css('background-color', '#8cdd8c');
            $(row).find('td:eq(1)').css('background-color', '#8cdd8c');
            $(row).find('td:eq(2)').css('background-color', '#8cdd8c');
            $(row).find('td:eq(3)').css('background-color', '#8cdd8c');
        }
    });
</script>
@endsection