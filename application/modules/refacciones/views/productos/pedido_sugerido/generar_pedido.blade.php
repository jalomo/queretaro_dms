@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
            <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
            <li class="breadcrumb-item active">{{ $bread_active }}</li>
        </ol>

        <div class="row">
            <div class="col-md-12 text-right mb-4 mt-3">
                <button onclick="generarlayout()" class="btn btn-primary">
                    <i class="fas fa-print"></i> Generar Layout
                </button>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered" id="tabla" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>No identificación</th>
                            <th>Producto</th>
                            <th>Tipo de pedido</th>
                            <th>Cantidad sugerida</th>
                            <th>Valor unitario</th>
                            <th>Peso</th>
                            <th>Proveedor</th>
                            <th> - </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script>
        let tabla = $('#tabla').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            "ajax": {
                url: base_url + "refacciones/productos/ajax_listado_pedido",
                type: 'GET',
            },
            columns: [{
                    'data': function(data) {
                        return data.id ? data.id : '-'
                    }
                },
                {
                    'data': 'no_identificacion'
                },
                {
                    'data': function(data) {
                        return data.descripcion ? data.descripcion : data.nombre_producto

                    }
                },
                {
                    'data': function(data) {
                        return data.tipo_pedido ? data.tipo_pedido : 'sin asignar'

                    }
                },
                {
                    'data': function(data) {
                        return data.cantidad_solicitada ? data.cantidad_solicitada : data.cantidad
                    }
                },
                {
                    'data': 'valor_unitario'
                },
                {
                    'data': function(data) {
                        return data.peso

                    }
                },
                {
                    'data': function(data) {
                        return data.proveedor_id ? data.proveedor_nombre : 'Sin asignar'

                    }
                },
                {
                    'data': function(data) {
                        btn_asignar = '<button class="btn btn-primary" data-datasrc="' + btoa(JSON
                                .stringify(data)) +
                            '" onclick="openmodal(this)" ><i class="fas fa-list"></i> </button>';
                        
                        btn_borrar = data.id ? '<button class="btn btn-danger ml-2" onclick="deleteitem(' + data.id +
                            ')" ><i class="fas fa-trash-alt"></i> </button>' : '';
                        return btn_asignar + btn_borrar;
                    }
                }
            ],
            "createdRow": function(row, data, dataIndex) {
                !data.proveedor_id && $(row).find('td:eq(7)').css('background-color', '#ff000075');
                !data.tipo_pedido && $(row).find('td:eq(3)').css('background-color', '#ff000075');

                data.proveedor_id && $(row).find('td:eq(7)').css('background-color', '#8cdd8c');
                data.tipo_pedido && $(row).find('td:eq(3)').css('background-color', '#8cdd8c');
            }
        });


        const openmodal = (_this) => {

            $("#modalpedido").modal('show');
            $("#frm")[0].reset();
            const data_params = $(_this).data('datasrc');
            const decode = JSON.parse(atob(data_params));
            let nombre_producto = decode.descripcion ? decode.descripcion : decode.nombre_producto;
            let no_identificacion = decode.no_identificacion;
            let producto_id = decode.producto_id;
            let cantidad = decode.cantidad_solicitada ? decode.cantidad_solicitada : decode.cantidad;

            $("#no_identificacion").val(no_identificacion);
            $("#nombre_producto").val(nombre_producto);
            $("#producto_id").val(producto_id);
            $("#cantidad").val(cantidad);
        }

        const handlepedido = () => {
            if ($("#cantidad").val() == '') {
                toastr.error("Indicar la cantidad de pedido.")
                return false;
            } else if ($("#proveedor_id").val() == '') {
                toastr.error("Indicar el proveedor.")
                return false;
            } else if ($("#producto_id").val() == '') {
                toastr.error("Indicar nombre de producto")
                return false;
            } else if ($("#estatus_id").val() == '') {
                toastr.error("Indicar estatus de producto")
                return false;
            }

            ajax.post('api/pedidoproducto', {
                "cantidad_solicitada": $("#cantidad").val(),
                "proveedor_id": $("#proveedor_id").val(),
                "producto_id": $("#producto_id").val(),
                "estatus_id": $("#estatus_id").val()
            }, function(response, headers) {
                if (headers.status == 201 || headers.status == 200) {
                    utils.displayWarningDialog("Creando pedido", "success", function(data) {
                        toastr.success("pedido actualizado")
                        $("#frm")[0].reset();
                        $("#modalpedido").modal('hide');
                        tabla.ajax.reload();
                    })
                }
            });

        }

        const generarlayout = () => {
            ajax.get('api/pedido-sugerido/generatelayout', {}, (data, headers) => {
                if (headers.status == 201 || headers.status == 200) {
                    return window.location.href = `${API_URL}api/pedido-sugerido/generatelayout`
                }
            })
        }

        const deleteitem = (id) => {
            utils.displayWarningDialog("Deseas borrar el producto ? ", "error", function(data) {
               
                if (data.value) {
                    ajax.delete('api/pedidoproducto/' + id, {}, (data, headers) => {
                        if (headers.status == 204 || headers.status == 200) {
                            return tabla.ajax.reload();
                        }
                    })

                }
            })
        }

    </script>
@endsection



@section('modal')
    <div class="modal fade" id="modalpedido" tabindex="-1" role="dialog" aria-labelledby="modalpedido" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal">Completar pedido</h5>
                </div>
                <div class="modal-body">
                    <form id="frm" class="row">
                        <div class="col-md-6">
                            <?php echo renderInputText('text', 'no_identificacion', 'No identificación', '',
                            true); ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo renderInputText('text', 'nombre_producto', 'Producto', '', true); ?>
                        </div>

                        <div class="col-md-8">
                            <?php renderSelectArray('proveedor_id', 'Proveedor', $data['catalogo_proveedor'],
                            'id', 'proveedor_nombre', null, false); ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo renderInputText('number', 'cantidad', 'Cantidad sugerida', '', false);
                            ?>
                            <input type="hidden" id="producto_id">
                        </div>
                        <div class="col-md-12">
                            <?php renderSelectArray('estatus_id', 'Tipo pedido', $data['tipo_pedido'], 'id',
                            'nombre', null, false); ?>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Cerrar
                    </button>
                    <button id="btn-confirmar" type="button" onclick="handlepedido()" class="btn btn-primary">
                        <i class="fas fa-check-circle"></i> Aceptar
                    </button>
                </div>
            </div>
        </div>
    </div>

@endsection
