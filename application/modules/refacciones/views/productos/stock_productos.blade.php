@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4 d-flex">
        Stock productos
    </h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
        <li class="breadcrumb-item active">{{ ucwords($this->uri->segment(3)) }}</li>
    </ol>

    <div class="d-flex align-items-end mb-3">
        <div class="col-md-3">
            <?php echo renderInputText("text", "no_identificacion", "No. de pieza", '', false); ?>
        </div>
        <div class="col-md-3">
            <?php echo renderInputText("text", "descripcion", "Descripción", '', false); ?>
        </div>
        <div class="col-md-3">
			<div class="form-group">
				<label>Almacen</label>
				<select class="form-control" id="almacenes">
					<option value="1" selected>{{ getenv('QUERETARO') == 'TRUE'? 'Queretaro' : 'San Juan' }}</option>
					<option value="2">{{ getenv('SAN_JUAN') == 'FALSE'? 'San Juan' : 'Queretaro' }}</option>
				</select>
			</div>
		</div>
        <div class="col-md-2 mb-3">
            <button onclick="buscarproducto()" class="btn btn-primary"> 
                <i class="fa fa-search"></i> Buscar
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="tabla_stock" width="100%" cellspacing="0">
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ base_url('js/refacciones/productos/stock.js') }}"></script>
@endsection