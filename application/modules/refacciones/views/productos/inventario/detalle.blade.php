@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
        <li class="breadcrumb-item active">{{ $bread_active}}</li>
    </ol>
    <div class="row mb-4">
        <div class="col-md-8">
            <?php echo renderInputText("text", "no_identificacion", "No. de pieza", isset($detalle_producto) ? $detalle_producto->no_identificacion : '' , true); ?>
        </div>
        <div class="col-md-4">
            <?php echo renderInputText("text", "unidad", "Unidad", isset($detalle_producto) ? $detalle_producto->unidad : '' , true); ?>
        </div>
        <div class="col-md-4">
            <?php echo renderInputText("text", "valor_unitario", "Valor unitario", isset($detalle_producto) ? $detalle_producto->valor_unitario : '' , true); ?>
        </div>
        <div class="col-md-8">
            <?php echo renderInputText("text", "descripcion", "Descripcion", isset($detalle_producto) ? $detalle_producto->descripcion : '' , true); ?>
        </div>
        <div class="col-md-3">
            <?php echo renderInputText("text", "cantidad_stock", "Cantidad Stock", isset($detalle_producto) ? $detalle_producto->cantidad_stock : '' , true); ?>
        </div>
        <div class="col-md-3">
            <?php echo renderInputText("text", "cantidad_inventario", "Inventario", isset($detalle_producto) ? $detalle_producto->cantidad_inventario : '' , true); ?>
        </div>
        <div class="col-md-3">
            <?php echo renderInputText("text", "diferencia", "Diferencia", isset($detalle_producto) ? $detalle_producto->diferencia : '' , true); ?>
        </div>
        <div class="col-md-3">
            <?php echo renderInputText("text", "valor_residuo", "Valor residuo", isset($detalle_producto) ? $detalle_producto->valor_residuo : '' , true); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <a href="{{ base_url('refacciones/productos/inventario') }}" class="btn btn-primary"> <i class='fa fa-arrow-left'></i>&nbsp; Regresar</a>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script></script>
@endsection

