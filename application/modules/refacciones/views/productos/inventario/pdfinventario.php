<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Comprobante pago</title>
    <style>
        .contenedor {
            width: 100%;
        }

        td {
            font-size: 11px !important;
        }

        .col-12 {
            width: 100%;
            padding: 3px;
        }

        .col-6 {
            float: left;
            width: 49%;
            padding: 3px;
        }

        .col-5 {
            float: left;
            width: 40%;
            padding: 3px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        .text-right {
            text-align: right;
        }

        table.detalle_cuenta,
        td,
        td {
            border-bottom: 0.1px solid #233a74;
            margin-bottom: 12px
        }
    </style>
</head>

<body>
    <div class="contenedor">
        <div class="col-12 text-right">
            <?php echo obtenerFechaEnLetra(date('yy-m-d')); ?>
        </div>
        <div class="col-12">
            <h2>FORD</h2>
            <h2><?php echo NOMBRE_SUCURSAL;?></h2>
            <h4>Reporte de inventario</h4>
        </div>
    </div>
    <div class="col-6">
        <div class="clear"></div>
    </div>
    <div class="col-6">
        <div class="clear"></div>
    </div>
    <div class="contenedor">
        <div class="col-12 mt-4">
            <h4>Detalle Pago</h4>
            <table style="width:100%;" class="table detalle_cuenta" cellpadding="5">
                <thead>
                    <tr>
                        <td width=""> #</td>
                        <td width=""> No. de pieza</td>
                        <td width=""> Descripcion</td>
                        <td width=""> Unidad</td>
                        <td width=""> Valor unitario</td>
                        <td width=""> Stock</td>
                        <td width=""> Valor</td>
                        <td width=""> Diferencia</td>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($data)) { ?>
                        <?php foreach ($data as $key => $item) {?>
                            <tr>
                                <td><?php echo $item->producto_id ?></td>
                                <td><?php echo $item->no_identificacion ?></td>
                                <td><?php echo $item->descripcion ?></td>
                                <td><?php echo $item->unidad ?></td>
                                <td><?php echo $item->valor_unitario ?></td>
                                <td><?php echo $item->cantidad_stock ?></td>
                                <td><?php echo $item->valor_residuo ?></td>
                                <td><?php echo $item->diferencia ?></td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </tbody>
            </table>
            <div class="clear"></div>
        </div>
        <div class="col-5">
        <table class="table detalle_cuenta" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Perdida</th>
                        <th>Diferencia</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th><?php echo $resultados['perdidas']  ?></th>
                        <th><?php echo $resultados['residuos']  ?></th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>