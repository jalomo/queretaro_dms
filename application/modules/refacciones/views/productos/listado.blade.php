@layout('tema_luna/layout')
<style>
    #qrcode {
        width: 160px;
        height: 160px;
        margin-top: 15px;
        border: 1px solid #ccc;
    }

</style>
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-2 mb-4 font-weight-bold">
            <i class="fa fa-clipboard-list"></i>
            Productos
        </h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
            <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
            <li class="breadcrumb-item active">{{ ucwords($this->uri->segment(3)) }}</li>
        </ol>
        <div class="row mb-3">
            <div class="col-md-8">
            </div>
            <div class="col-md-2">
                <a class="btn btn-primary col-md-12" href="<?php echo base_url('refacciones/stock'); ?>"><i
                        class="fa fa-file"></i>&nbsp;Actualizar precios</a>
            </div>
            <div class="col-md-2">
                <a class="btn btn-primary col-md-12" href="<?php echo base_url('refacciones/productos/crear'); ?>">
                    <i class="fa fa-plus"></i>&nbsp;Agregar
                </a>
            </div>
        </div>
        <div class="d-flex align-items-end mb-3">
            <div class="col-md-3">
                <?php echo renderInputText('text', 'no_identificacion', 'No. de pieza', '', false); ?>
            </div>
            <div class="col-md-3 ">
                <?php echo renderInputText('text', 'descripcion', 'Descripción', '', false); ?>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Almacen</label>
                    <select class="form-control" id="almacenes">
                        <option value="1" selected>{{ getenv('QUERETARO') == 'TRUE'? 'Queretaro' : 'San Juan' }}</option>
					    <option value="2">{{ getenv('SAN_JUAN') == 'FALSE'? 'San Juan' : 'Queretaro' }}</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2 mb-3">
                <button type="button" onclick="buscarproducto()" class="btn btn-primary">
                    <i class="fa fa-search"></i> Buscar
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered" id="tbl_productos" width="100%" cellspacing="0">
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ base_url('assets/libraries/qr/qrcode.js') }}"></script>
    <script>
        var tabla_stock = $('#tbl_productos').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            processing: true,
            serverSide: true,
            bFilter: false,
            ajax: {
                url: PATH_API + "api/productos/listadoStock",
                type: 'GET',
                data: (data) => {
                    data.descripcion = document.getElementById('descripcion').value
                    data.no_identificacion = document.getElementById('no_identificacion').value
                }
            },
            columns: [{
                    title: "QR",
                    data: 'id',
                    render: function(data, type, row) {
                        return '<div data-nombre_producto="' + row.descripcion + '" data-producto_id="' +
                            row.id + '" id="qr_' + row.id + '"></div>';
                    }
                },
                {
                    title: "#",
                    data: 'id',
                },
                {
                    title: "Prefijo",
                    data: 'prefijo',
                },
                {
                    title: "Basico",
                    data: 'basico',
                },
                {
                    title: "Sufijo",
                    data: 'sufijo',
                },
                {
                    title: "No. de pieza",
                    data: 'no_identificacion',
                },
                {
                    title: "Descripcion",
                    data: 'descripcion',
                },
                {
                    title: "Precio unitario",
                    data: 'valor_unitario',
                },
                {
                    title: "Costo Promedio",
                    data: 'costo_promedio',
                },
                {
                    title: "Costo Ford",
                    data: 'costo_ford',
                },
                {
                    title: 'Cantidad',
                    data: 'cantidad_actual'
                },
                {
                    title: "Unidad",
                    data: 'unidad',
                },
                {
                    title: "Ubicación",
                    data: 'ubicacionProducto',
                },
                {
                    title: "-",
                    render: function(data, type, row) {
                        btn_ficha =
                            '<button  class="btn btn-primary p-2" onclick="goTo(this)" data-producto_id="' + row
                            .id + '"><i class="fas fa-list"></i> </button>';
                        btn_editar =
                            '<button  class="btn btn-warning p-2" onclick="editar(this)" data-producto_id="' +
                            row.id + '"><i class="fas fa-edit"></i> </button>';
                        return btn_ficha + ' ' + btn_editar;
                    },
                    class: 'text-nowrap'
                }
            ],

            "fnDrawCallback": function(oSettings, json) {
                generandoQrs();
                $('#tbl_productos').on('page.dt', function() {
                    setTimeout(() => {
                        generandoQrs();
                    }, 500);
                });
            }
        });


        function makeCode(div, text) {
            return new QRCode(document.getElementById(div), {
                width: 80,
                height: 80,
                text: JSON.stringify(text)
            });
        }

        function goTo(_this) {
            window.location.href = PATH + 'refacciones/productos/ficha_tecnica/' + $(_this).data('producto_id');
        }

        function editar(_this) {
            window.location.href = PATH + 'refacciones/productos/editar/' + $(_this).data('producto_id');
        }

        function borrar(id) {
            utils.displayWarningDialog("Desea borrar el registro??", "warning", function(data) {
                if (data.value) {
                    ajax.delete(`/api/productos/${id}`, null, function(response, headers) {
                        if (headers.status != 204) {
                            return utils.displayWarningDialog(headers.message)
                        }
                        location.reload(true)
                    })

                }
            }, true)
        }

        const buscarproducto = () => {
            
            let almacen = $('#almacenes').val()
            let path = almacen == 1? PATH_API + 'api/productos/listadoStock' : PATH_API_SANJUAN + "api/productos/listadoStock"

            $('#tbl_productos').DataTable().ajax.url(path).load()
        }

        function generandoQrs() {
            $('table#tbl_productos tr td div:visible').each(function(index) {
                let identificador = $(this).attr('id');
                $('#'+ identificador).empty()

                console.log(identificador)
                let array = {
                    'producto_id': $(this).data('producto_id'),
                    'nombre_producto': $(this).data('nombre_producto'),
                };
                makeCode(identificador, array);
            });
        }
    </script>
@endsection
