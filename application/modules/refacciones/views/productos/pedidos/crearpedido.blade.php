@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">Productos</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
        <li class="breadcrumb-item active">Pedido de piezas</li>
    </ol>
    <div class="row">
        <div class="col-md-8 mb-4">
            <h2>Pedido de piezas</h2>
        </div>
        <div class="col-md-2 text-right">
            <a class="btn btn-primary" href="{{ base_url('refacciones/productos/generarpedidosugerido') }}">
                Generar Layout
            </a>
        </div>
        <div class="col-md-2 mb-4 text-center">
            <input type="hidden" id="id_usuario_autorizo" value="{{  $this->session->userdata('id') }}">
            <input type="hidden" id="rol_id" value="{{  $this->session->userdata('rol_id') }}">
            <button id="btn_pedido" class="btn btn-primary">
                <i class="fas fa-plus"></i> Nuevo pedido
            </button>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="estatus_id">Estatus de pedido</label>
                <select class="form-control" name="estatus_id" id="estatus_id">
                    <option value=""> Seleccionar ..</option>
                    <option value="1">Proceso </option>
                    <option value="4">Backorder</option>
                    <option value="2">Finalizado</option>
                </select>
                <div id="estatus_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-2 mt-4">
            <button class="btn col-md-12 btn-primary" onclick="filtrarLista()"> Filtrar </button>
        </div>
        <div class="col-md-2 mt-4">
            <button class="btn btn-primary col-md-12" onclick="limpiarBusqueda()"> Limpiar </button>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tabla_pedido" width="100%" cellspacing="0"></table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    let tabla = $('#tabla_pedido').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        "ajax": {
            url: PATH_API + "api/masterpedidoproducto/filtrar",
            type: 'POST',
            data: {
                estatus_id: function() {
                    return $('#estatus_id').val()
                }
            },
        },
        columns: [{
                title: "#",
                data: 'id',
            },
            {
                title: "Autorizado",
                data: ({
                    autorizado
                }) => autorizado ? 'Autorizado' : 'Sin autorizar'
            },
            {
                title: "Quien autorizo",
                data: function({
                    rel_usuario
                }) {
                    if (rel_usuario) {
                        let {
                            nombre,
                            apellido_paterno,
                            apellido_materno
                        } = rel_usuario;
                        return `${nombre} ${apellido_paterno} ${apellido_materno} `
                    } else {
                        return '-'
                    }
                }
            },
            {
                title: "Detalle de envio ( Remision )",
                data: (data) =>{
                    return data.id_detalle_envio ? data.numero_remision : 'No asignado'
                },
            },
            {
                title: "Fecha",
                data: ({
                    created_at
                }) => utils.dateToLetras(created_at),
            },
            {
                title: "-",
                data: function({
                    id,
                    autorizado,
                    id_detalle_envio
                }) {
                    let rol_id = parseInt(document.getElementById('rol_id').value);
                    let estatus_id = document.getElementById('estatus_id').value;
                    let btn_detalle = "<a class='btn btn-primary' href='" + base_url + 'refacciones/productos/pedidopiezas/' + id + "' ><i class='fas fa-list'></i></a>";
                    let btn_envio = `<button onclick='handlemodalenvio(${id},${id_detalle_envio})' class='btn btn-primary ml-2'><i class='fas fa-shipping-fast'></i></button>`;
                    return btn_detalle + btn_envio
                }

            }
        ],
        "createdRow": function(row, {
            autorizado,
            finalizado,
            id_detalle_envio
        }) {

            id_detalle_envio 
                ? $(row).find('td:eq(3)').css('background-color', '#8cdd8c') 
                : $(row).find('td:eq(3)').css('background-color', 'rgba(219, 82, 77, 0.5)');

            if (autorizado) {
                $(row).find('td:eq(1)').css('background-color', '#8cdd8c');
            } else {
                $(row).find('td:eq(1)').css('background-color', '#db524d80');
            }
        }
    });
    let limpiarBusqueda = ()=>{
        $("#estatus_id").val('');
        tabla.ajax.reload();
    }

    let filtrarLista = () => {
        let estatus_id = document.getElementById('estatus_id').value;
        tabla.ajax.reload();
    }

    $("#btn_pedido").on('click', function() {
        ajax.post('api/masterpedidoproducto', {
            "autorizado": 0
        }, function(response, headers) {
            if (headers.status == 201 || headers.status == 200) {
                utils.displayWarningDialog("Creando pedido", "success", function(data) {
                    return window.location.href = base_url + 'refacciones/productos/pedidopiezas/' + response.id;
                })
            }
        });

    })

    let finalizarpedido = (id) => {
        utils.displayWarningDialog("Finalizar pedido", "warning", function(data) {
            if (data && data.value) {
                ajax.put('api/masterpedidoproducto/' + id, {
                    "finalizado": 1
                }, function(response, headers) {
                    tabla.ajax.reload();
                });
            }
        }, true)
    }

    const handlemodalenvio = (pedido_id, id_detalle_envio = null) => {
            $("#modalenviopedido").modal('show');
            $("#frm_envio")[0].reset();
            $("#pedido_id").val(pedido_id);
            if(id_detalle_envio !== null){
                $("#id_detalle_envio").val(id_detalle_envio);
                ajax.get(`api/enviopedido/${id_detalle_envio}`, {}, (data, headers) => {
                    $("#via_envio").val(data.via_envio)
                    $("#ruta").val(data.ruta);
                    $("#lineas").val(data.lineas);
                    $("#total_piezas").val(data.total_piezas);
                    $("#peso_total").val(data.peso_total);
                    $("#numero_remision").val(data.numero_remision);
                })
            }
        }

        const handleenviopedido =() => {
            if($("#via_envio").val() == ''){
                toastr.error("Indicar la cantidad de pedido.")
                return false;
            }else if($("#dia_envio").val() == ''){
                toastr.error("Indicar dia de envio")
                return false;
            }else if($("#ruta").val() == ''){
                toastr.error("Indicar ruta")
                return false;
            }else if($("#lineas").val() == ''){
                toastr.error("Indicar lineas")
                return false;
            }else if($("#total_piezas").val() == ''){
                toastr.error("Indicar total de piezas")
                return false;
            }else if($("#peso_total").val() == ''){
                toastr.error("Indicar el peso total")
                return false;
            }
            
            if($("#id_detalle_envio").val()){
                update($("#id_detalle_envio").val())
               return false; 
            }
            
            ajax.post('api/enviopedido', {
                "via_envio":$("#via_envio").val(),
                "dia_envio":$("#dia_envio").val(),
                "ruta":$("#ruta").val(),
                "lineas":$("#lineas").val(),
                "total_piezas":$("#total_piezas").val(),
                "pedido_id":$("#pedido_id").val(),
                "peso_total":$("#peso_total").val(),
                "numero_remision":$("#numero_remision").val()
            }, function(response, headers) {
                if (headers.status == 201 || headers.status == 200) {
                    utils.displayWarningDialog("Detalle de envio actualizado.", "success", function(data) {
                        toastr.success("detalle de pedido actualizado")
                        $("#frm_envio")[0].reset();
                        $("#modalenviopedido").modal('hide');
                        tabla.ajax.reload();
                    })
                }
            });

        }

        const update = (id_detalle_envio) =>{
            ajax.put(`api/enviopedido/${id_detalle_envio}`, { 
                "via_envio":$("#via_envio").val(),
                "dia_envio":$("#dia_envio").val(),
                "ruta":$("#ruta").val(),
                "lineas":$("#lineas").val(),
                "total_piezas":$("#total_piezas").val(),
                "pedido_id":$("#pedido_id").val(),
                "peso_total":$("#peso_total").val(),
                "numero_remision":$("#numero_remision").val()
            }, function (response, header) {
                if (header.status == 400) {
                    return ajax.showValidations(header);
                }
                utils.displayWarningDialog(header.message, "success", function (data) {
                    $("#frm_envio")[0].reset();
                    $("#modalenviopedido").modal('hide');
                    tabla.ajax.reload();
                })
            })
        }


</script>
@endsection


@section('modal')

    <div class="modal fade" id="modalenviopedido" tabindex="-1" role="dialog" aria-labelledby="modalenviopedido" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal">Detalle de envio</h5>
                </div>
                <div class="modal-body">
                    <form id="frm_envio" class="row">
                        <div class="col-md-6">
                            <?php echo renderInputText('text', 'via_envio', 'Via embarque', '',false,  '# Embarcación'); ?>
                        </div>
                        <div class="col-md-6">
                            <input type="hidden" id="pedido_id">
                            <?php echo renderInputText('date', 'dia_envio', 'Dia', ''); ?>
                        </div>

                        <div class="col-md-6">
                            <?php echo renderInputText('text', 'ruta', 'Ruta', ''); ?>
                            <input type="hidden" name="id_detalle_envio" id="id_detalle_envio">
                        </div>
                        <div class="col-md-6">
                            <?php echo renderInputText('text', 'lineas', 'Lineas', ''); ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo renderInputText('number', 'total_piezas', 'Total de piezas', ''); ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo renderInputText('number', 'peso_total', 'Peso total', ''); ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo renderInputText('text', 'numero_remision', 'Número de remision', ''); ?>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Cerrar
                    </button>
                    <button id="btn-confirmar" type="button" onclick="handleenviopedido()" class="btn btn-primary">
                        <i class="fas fa-check-circle"></i> Aceptar
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection