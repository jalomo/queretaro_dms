@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
	<h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
	</ol>

	<div class="row">
		<div class="col-md-8">
			<div class="row">
				<div class="col-md-12">
					<?php echo renderInputText("text", "descripcion", "Descripcion", isset($data->descripcion) ? $data->descripcion : '', false); ?>
				</div>
				<div class="col-md-6">
					<?php echo renderInputText("text", "no_identificacion", "No. de pieza", isset($data->no_identificacion) ? $data->no_identificacion : '', false); ?>
				</div>
				<div class="col-md-6">
					<?php echo renderInputText("text", "clave_unidad", "Clave unidad", isset($data->clave_unidad) ? $data->clave_unidad : '', false); ?>
				</div>
				<div class="col-md-6">
					<?php echo renderInputText("text", "clave_prod_serv", "Clave producto servicio", isset($data->clave_prod_serv) ? $data->clave_prod_serv : '', false); ?>
				</div>
				<div class="col-md-6">
					<?php echo renderInputText("number", "valor_unitario", "Valor unitario", isset($data->valor_unitario) ? $data->valor_unitario : '', false); ?>
				</div>
				<div class="col-md-6">
					<?php echo renderInputText("number", "precio_factura", "Precio factura", isset($data->precio_factura) ? $data->precio_factura : '', isset($data->precio_factura) ?  true : false); ?>
				</div>
				<input type="hidden" id="producto_id" value="<?php echo isset($data->id) ? $data->id : ''?>" />
				<div class="col-md-6">
					<div class="form-group">
						<label for="select">Taller</label>
						<select name="taller_id" class="form-control" id="taller_id">
							<option value=""> Seleccionar taller</option>
							@foreach ($cat_talleres as $taller)

							@if (isset($data->taller_id) && $data->taller_id == $taller->id)
							<option selected value="{{ $taller->id}}"> {{ $taller->nombre}}</option>
							@else
							<option value="{{ $taller->id}}"> {{ $taller->nombre}}</option>
							@endif
							@endforeach
						</select>
						<div id='taller_id_error' class='invalid-feedback'></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="select">Almacen</label>
						<select name="almacen_id" class="form-control" id="almacen_id">
							<option value=""> Seleccionar almacen</option>
							@foreach ($cat_almacenes as $almacen)
							@if (isset($data->almacen_id) && $data->almacen_id == $almacen->id)
							<option selected value="{{ $almacen->id}}"> {{ $almacen->nombre}}</option>
							@else
							<option value="{{ $almacen->id}}"> {{ $almacen->nombre}}</option>
							@endif
							@endforeach
						</select>
						<div id='almacen_id_error' class='invalid-feedback'></div>
					</div>
				</div>
				<div class="col-md-6">
					<?php echo renderInputText("text", "", "Ubicación del producto", isset($stock->ubicacion->nombre) ? $stock->ubicacion->nombre : '', false); ?>
				</div>
				<div class="col-md-6">
					<?php echo renderInputText("text", "", "Cantidad actual", isset($stock->desglose_producto->cantidad_actual) ? $stock->desglose_producto->cantidad_actual : '', false); ?>
				</div>
				<div class="col-md-6">
					<?php echo renderInputText("text", "", "Cantidad almacen primario", isset($stock->desglose_producto->cantidad_almacen_primario) ? $stock->desglose_producto->cantidad_almacen_primario : '', false); ?>
				</div>
				<div class="col-md-6">
					<?php echo renderInputText("text", "", "Cantidad almacen secundario", isset($stock->desglose_producto->cantidad_almacen_secundario) ? $stock->desglose_producto->cantidad_almacen_secundario : '', false); ?>
				</div>
				<div class="col-md-6">
					<?php echo renderInputText("text", "", "Total de compras", isset($stock->desglose_producto->cantidad_compras) ? $stock->desglose_producto->cantidad_compras : '', false); ?>
				</div>
				<div class="col-md-6">
					<?php echo renderInputText("text", "", "Total de ventas", isset($stock->desglose_producto->cantidad_ventas) ? $stock->desglose_producto->cantidad_ventas : '', false); ?>
				</div>
			</div>
			<div class="row mt-4">
				@if (isset($categoria_sugerido->rapido) && count($categoria_sugerido->rapido) > 0)
				<div class="col-md-4">
					<h3>Demanda promedio</h3>
					<h4>seccion A </h4>
					<div class="p-3 mb-2 bg-success text-white">
						Demanda promedio: {{ $categoria_sugerido->rapido[0]->promedio }}
					</div>
				</div>
				@endif

				@if (isset($categoria_sugerido->lento) && !empty($categoria_sugerido->lento)>0)
				<div class="col-md-4">
					<h3>Demanda lenta</h3>
					<h4>seccion B </h4>
					<div class="p-3 mb-2 bg-warning text-white">
						Demanda promedio: {{ $categoria_sugerido->lento[0]->promedio }}
					</div>
				</div>
				@endif

				@if (isset($categoria_sugerido->inactivo) && $categoria_sugerido->inactivo)
				<div class="col-md-6">
					<h3>Demanda Inactiva</h3>
					<h4>seccion C </h4>
					<div class="p-3 mb-2 bg-danger text-white">
						Sin ventas en los ultimos 30 dias
					</div>
				</div>
				@endif
				@if (isset($categoria_sugerido->potencial_obsoleto) && $categoria_sugerido->potencial_obsoleto)
				<div class="col-md-6">
					<h3>Potencial obsoleto</h3>
					<h4>seccion C </h4>
					<div class="p-3 mb-2 bg-danger text-white">
						Sin ventas en los ultimos 31 dias
					</div>
				</div>
				@endif
				@if (isset($categoria_sugerido->obsoleto) && $categoria_sugerido->obsoleto)
				<div class="col-md-6">
					<h3>Obsoleto</h3>
					<h4>seccion C </h4>
					<div class="p-3 mb-2 bg-danger text-white">
						Sin ventas en el ultimo año
					</div>
				</div>
				@endif

			</div>
			<div class="mt-2 mb-3 text-right d-flex">
				<a href="{{ base_url('refacciones/productos') }}" class="btn btn-primary">
					<i class="fas fa-arrow-left" aria-hidden="true"></i> Regresar
				</a>
				<div class="ml-auto"></div>
				<button class="btn btn-primary" id="btnKardex">
					<i class="fas fa-list" aria-hidden="true"></i> Kardex
				</button>
			</div>
		</div>
		<div class="col-md-4" style="border-left:1px solid #323232;">
			<div class="row">
				<?php
				if (isset($ventas) && $ventas) { ?>
					<div class="col-md-10 ml-3">
						<h3>Ventas realizadas <?php echo date('Y'); ?></h3>
						<div class="row">
							<?php
							foreach ($ventas->lista_ventas as $venta) { ?>
								<div class="col-md-6">
									<div class="panel panel-filled" style="border:1px solid #e5e5e5;">
										<div class="panel-body">
											<h4 class="m-b-none">
												<?php echo $venta->mes; ?>
												<?php if ($venta->total_ventas >= 1) {
													echo '- <span class="text-success">' . $venta->total_ventas . '</span>';
												} else {
													echo '- <span class="text-danger">0</span>';
												} ?>
											</h4>
										</div>
									</div>
								</div>
							<?php } ?>
						</div>
						<h3>Total ventas: <?php echo current($ventas->totales)->total; ?></h3>
						<h3>Promedio: <?php echo current($ventas->totales)->promedio; ?></h3>
					</div>
				<?php } ?>
				<div class="col-md-12 mt-3">
					<center>
						<div id="qrcode" style="height:100%; width:100%; color:red"></div>
					</center>
				</div>
			</div>

		</div>
	</div>
</div>
@endsection

@section('modal')
<div class="modal fade" id="modalKardex" tabindex="-1" role="dialog" aria-labelledby="modalcomprasLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold" id="title_modal">
					<i class="fa fa-list"></i>
					Kardex
				</h5>
			</div>
			<div class="modal-body">
				<form method="GET" target="_blank" action="{{ site_url('refacciones/kardex/kardexproducto') }}" id="frm-kardex-buscar" class="row">
					<div class="col-md-12">
						<?php echo renderInputText("date", "fecha_inicio", "Fecha de inicio", date('Y') . '-01-01'); ?>
						<input type="hidden" name="producto_id" id="producto_id" value="{{ $data->id }}">
					</div>
					<div class="col-md-12">
						<?php echo renderInputText("date", "fecha_termino", "Fecha de fin", date('Y-m-d')); ?>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				<button id="btn-confirmar" type="button" class="btn btn-primary">
					Continuar 
					<i class="fas fa-arrow-right"></i>
				</button>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script src="{{ base_url('assets/libraries/qr/qrcode.js') }}"></script>
<script type="text/javascript">
	$("#btnKardex").on('click', function() {
		$("#modalKardex").modal("show");
	});

	makeCode();

	function makeCode() {
		let array = {
			'producto_id': $('#producto_id').val(),
			'nombre_producto': $('#descripcion').val(),
			'url' : PATH_BASE + 'refacciones/productos/ficha_tecnica/' + $('#producto_id').val()
		};
		return new QRCode(document.getElementById('qrcode'), {
			width: 180,
			height: 180,
			text: JSON.stringify(array)
		});
	}

	$("#btn-confirmar").on('click', function() {
		let fecha_rango_inicio = $("#fecha_inicio").val();
		let fecha_rango_fin = $("#fecha_fin").val();

		if (fecha_rango_inicio == '') {
			let titulo = "Indica una fecha de inicio!"
			return utils.displayWarningDialog(titulo, 'warning');
		}

		if (fecha_rango_fin == '') {
			let titulo = "Indica una fecha de limite!"
			return utils.displayWarningDialog(titulo, 'warning');
		}

		$("#frm-kardex-buscar").submit();
	});
</script>
@endsection