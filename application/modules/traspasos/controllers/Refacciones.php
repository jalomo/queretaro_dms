<?php defined('BASEPATH') or exit('No direct script access allowed');

class Refacciones extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
    }

    protected function middleware()
    {
        // MiddlewareName|except:method1,method2
        // MiddlewareName|only:method1,method2
        return ['AuditoriaInventario'];
    }

    public function index()
    {
        $this->blade->render('index');
    }

    public function nuevo()
    {
        $this->blade->render('create');
    }
}