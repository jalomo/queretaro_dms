@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1>
            <i class="fas fa-truck"></i>
            <span class="font-weight-bold">Traspasos</span>
        </h1>
        <input id="estatus_traspaso" type="hidden" value=""/>
        <div class="d-flex mb-2">
            <a href="{{ base_url('/traspasos/refacciones/nuevo') }}" class="btn btn-primary ml-auto">
                <i class="fas fa-file-import"></i>
                Solicitud de Traspaso
            </a>
            <button class="btn btn-primary ml-2" onclick="detailsRefaccionesVentas()">
                <i class="fas fa-store"></i>
                Traspaso desde ventas
            </button>
        </div>
        <table id="traspaso" class="table table-striped">
            <thead>
                <th>#</th>
                <th>Origen</th>
                <th>Destino</th>
                <th>Cantidad</th>
                <th>Estatus</th>
                <th>Fecha de Creación</th>
                <th>Acciones</th>
            </thead>
            <tbody>
                <tr></tr>
            </tbody>
        </table>
    </div>
    <div id="modal-traspaso-detalles" class="modal">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header bg-light">
                    <div class="modal-title my-0">
                        <i class="fa fa-list"></i>
                        Detalle Traspaso
                    </div>
                </div>
                <div class="modal-body">
                    <table id="refacciones" class="table table-striped">
                        <thead>
                            <th>No identificación</th>
                            <th>Descripción</th>
                            <th align="center">Cantidad Solicitada</th>
                            <th align="center">Cantidad Entregada</th>
                            <th align="center">Cantidad Devuelta</th>
                            <th>Valor Unitario</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>#</td>
                                <td>Descripción</td>
                                <td>Clave</td>
                                <td>Cantidad</td>
                                <td>Cantidad Entregada</td>
                                <td>Cantidad Devuelta</td>
                                <td>Acciones</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-refacciones-venta" class="modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-light">
                    <div class="modal-title my-0">
                        <i class="fas fa-store"></i>
                        Traspaso desde ventas
                    </div>
                </div>
                <div class="modal-body">
                    <table id="ventas" class="table table-striped">
                        <thead>
                            <th>Folio</th>
                            <th>Cantidad</th>
                            <th>Valor Unitario</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>#</td>
                                <td>Cantidad</td>
                                <td>Valor Unitario</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-solicita-refacciones" class="modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-light">
                    <div class="modal-title my-0">
                        Firmar Solicitud de Refacciones
                        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="modal-body">
                    <div id="firma-solicita" class="mx-auto"></div>
                    <input type="hidden" name="traspaso-solicitud-id" id="traspaso-solicitud-id">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" onclick="limpiarFirmaSolicitud()">
                        <i class="fa fa-eraser"></i>
                        Limpiar
                    </button>
                    <button class="btn btn-primary" onclick="saveFirmaSolicitud()">
                        <i class="fa fa-signature"></i>
                        Solicitar
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-autoriza-refacciones" class="modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-light">
                    <div class="modal-title my-0">
                        Firmar Autorización de Refacciones
                        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="modal-body">
                    <div id="firma-autorizar" class="mx-auto"></div>
                    <input type="hidden" name="traspaso-autorizar-id" id="traspaso-autorizar-id">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" onclick="limpiarFirmaAutoriza()">
                        <i class="fa fa-eraser"></i>
                        Limpiar
                    </button>
                    <button class="btn btn-primary" onclick="saveFirmaAutoriza()">
                        <i class="fa fa-signature"></i>
                        Autorizar
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-subir-evidencia" class="modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-light">
                    <div class="modal-title my-0">
                        <i class="fa fa-upload"></i>
                        Subir Evidencia
                        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="modal-body">
                    <input type="file" class="form-control" id="evidencia">
                    <input type="hidden" name="traspaso-evidencia-id" id="traspaso-evidencia-id">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" onclick="subirEvidencia()">
                        <i class="fa fa-save"></i>
                        Subir Evidencia
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="{{ base_url('/assets/libraries/signature/jq-signature.min.js') }}"></script>
<script>

    $('#firma-solicita').jqSignature({
        lineColor: '#000',
        width: 700,
        height: 300
    });

    $('#firma-autorizar').jqSignature({
        lineColor: '#000',
        width: 700,
        height: 300
    });

    function limpiarFirmaSolicitud(){
        $('#firma-solicita').jqSignature('clearCanvas')
    }

    function limpiarFirmaAutoriza(){
        $('#firma-autorizar').jqSignature('clearCanvas')
    }

    function saveFirmaSolicitud(){
        let traspaso_id = $('#traspaso-solicitud-id').val()
        let firma = $('#firma-solicita').jqSignature('getDataURL')

        $.isLoading({ text: "Solicitando firma de autorizacion." });

        $.post(`${PATH_API}api/traspaso/${traspaso_id}/pedido`, {
            firma: firma
        })
            .done(response => {
                toastr.success('Se realizo la autorización de traspaso.')
                $("#modal-solicita-refacciones").modal('hide');
                $('#traspaso').DataTable().ajax.reload()
            })
            .fail((error) => {
                toastr.error("Ocurrio un error al realizar la solicitud de autorizacion.")
            })
            .always(() => {
                $.isLoading('hide');
            })
    }

    function saveFirmaAutoriza(){
        let traspaso_id = $('#traspaso-autorizar-id').val()
        let firma = $('#firma-autorizar').jqSignature('getDataURL')

        $.isLoading({ text: "Firmando autorizacion de traspaso." });

        $.post(`${PATH_API}api/traspaso/${traspaso_id}/autorizar`, {
            firma: firma
        })
            .done(response => {
                toastr.success('Se realizo la autorización de traspaso.')
                $("#modal-autoriza-refacciones").modal('hide');
                $('#traspaso').DataTable().ajax.reload()
            })
            .fail((error) => {
                toastr.error("Ocurrio un error al realizar la autorizacion.")
            })
            .always(() => {
                $.isLoading('hide');
            })
    }

    function showFirmaSolicita(id){
        $('#modal-solicita-refacciones').modal('show')
        $('#traspaso-solicitud-id').val(id)
        limpiarFirmaSolicitud()
    }

    function showFirmaAutoriza(id){
        $('#modal-autoriza-refacciones').modal('show')
        $('#traspaso-autorizar-id').val(id)
        limpiarFirmaAutoriza()
    }

    function showSubirEvidencia(id){
        $('#modal-subir-evidencia').modal('show')
        $('#traspaso-evidencia-id').val(id)
    }

    function addTraspaso(venta_id){
        $.isLoading({ text: "Asignando productos a traspaso." });

        $.post(`${PATH_API}api/traspaso/${venta_id}/crear`)
            .done(response => {
                toastr.success("Se asignaron los productos al traspaso.")
                $('#traspaso').DataTable().ajax.reload()
            })
            .fail((error) => {
                toastr.error("Ocurrio un error al asignar productos al traspaso.")
            })
            .always(() => {
                $.isLoading('hide');
            })
    }

    function devueltoTraspaso(traspaso_id){
        $.isLoading({ text: "Procesando peticion de traspaso de productos." });

        $.post(`${PATH_API}api/traspaso/${traspaso_id}/devuelto`)
            .done(response => {
                toastr.success('Se realizo la petición de devolucion del traspaso.')
                $('#traspaso').DataTable().ajax.reload()
            })
            .fail((error) => {
                toastr.error("Ocurrio un error al realizar la petición de devolucion de traspaso.")
            })
            .always(() => {
                $.isLoading('hide');
            })
    }

    function cancelTraspaso(traspaso_id){
        $.isLoading({ text: "Procesando peticion de traspaso de productos." });

        $.ajax({
            url: `${PATH_API}api/traspaso/${traspaso_id}/cancelacion`,
            type: 'DELETE',    
        })
        .done(response => {
            toastr.success('Se cancelo el pedido de traspaso.')
            $('#traspaso').DataTable().ajax.reload()
        })
        .fail((error) => {
            toastr.error("Ocurrio un error al cancelar la petición de traspaso.")
        })
        .always(() => {
            $.isLoading('hide');
        })
    }

    function envioTraspaso(traspaso_id){
        $.isLoading({ text: "Procesando peticion de envio de productos." });

        $.ajax({
            url: `${PATH_API}api/traspaso/${traspaso_id}/envio`,
            type: 'POST',
            dataType: 'json',
        })
        .done(response => {
            toastr.success('Se envio mensaje de envio traspaso.')
            $('#traspaso').DataTable().ajax.reload()
        })
        .fail((error) => {

            if(error.status == 422){
                toastr.error(error.responseJSON.errors)
                return;
            }
            
            toastr.error("Ocurrio un error al enviar mensaje de envio de traspaso.")
        })
        .always(() => {
            $.isLoading('hide');
        })
    }

    function finalizarTraspaso(traspaso_id){
        $.isLoading({ text: "Procesando peticion de finalizacion de traspaso." });

        $.ajax({
            url: `${PATH_API}api/traspaso/${traspaso_id}/finalizar`,
            type: 'POST',    
        })
        .done(response => {
            toastr.success('Se finaliza traspaso.')
            $('#traspaso').DataTable().ajax.reload()
        })
        .fail((error) => {
            toastr.error("Ocurrio un error al finalizar el traspaso.")
        })
        .always(() => {
            $.isLoading('hide');
        })
    }

    function devolucionTraspaso(traspaso_id){
        $.isLoading({ text: "Procesando devolución de traspaso." });

        $.ajax({
            url: `${PATH_API}api/traspaso/${traspaso_id}/devolucion`,
            type: 'POST',    
        })
        .done(response => {
            toastr.success('Solicitud de devolución exitosa.')
            $('#traspaso').DataTable().ajax.reload()
        })
        .fail((error) => {
            toastr.error("Ocurrio un error al realizar petición de devolución.")
        })
        .always(() => {
            $.isLoading('hide');
        })
    }

    function subirEvidencia(){
        let id = $('#traspaso-evidencia-id').val()
        let archivo = $('#evidencia')[0].files[0]

        let form = new FormData()
        form.append('archivo', archivo)

        $.isLoading({ text: "Subiendo archivo evidencia." });

        ajax.postFile(`api/traspaso/${id}/subir-evidencia`, form, function(response, header) {
            try{
                if (header.status == 200) {
                    utils.displayWarningDialog(
                        "Evidencia guardada correctamente",
                        "success",
                        function(data) {
                            $('#traspaso').DataTable().ajax.reload()
                        })
                }

                $("#modal-subir-evidencia").modal('hide');
                $.isLoading('hide')
            } catch(e) {
                utils.displayWarningDialog(
                        "Ocurrio un error al guardar la evidencia",
                        "error",
                        function(data) {})

                $.isLoading('hide')
            }
        })
    }

    function downloadPDF(traspaso_id){
        $.isLoading({ text: "Descargando poliza de traspaso." });

        $.ajax({
            url: `${PATH_API}api/traspaso/${traspaso_id}/poliza`,
            type: 'GET',    
            // xhrFields: { responseType: 'arraybuffer'},
            xhr: () => {
                let xhr = new XMLHttpRequest()
                xhr.onreadystatechange = () => {
                    if(xhr.readyState == 2){
                        xhr.responseType = "arraybuffer"
                    } else {
                        xhr.responseType = "json"
                    }
                }

                return xhr
            }
        })
        .done((response, status, xhr) => {
            let disposition = xhr.getResponseHeader("Content-Disposition")
            let filename = 'poliza-traspaso.pdf'
            let type = xhr.getResponseHeader("Content-Type")

            let blobFile = new Blob([response], { type: type })
            let URL = window.URL || window.webkitURL
            let downloadURL = URL.createObjectURL(blobFile)

            if(typeof window.navigator.msSaveBlob !== 'undefined'){
                window.navigator.msSaveBlob(blobFile, filename)

                let link = document.createElement('a')
                link.href = downloadURL
                link.download = filename
                document.body.appendChild(link)
                link.click()
                return ;
            }            

            window.open(downloadURL)
        })
        .fail((error) => {
            toastr.error("Ocurrio un error al descargar la poliza de traspaso.")
        })
        .always(() => {
            $.isLoading('hide');
        })
    }

    function downloadEvidencia(traspaso_id){
        $.isLoading({ text: "Descargando poliza de traspaso." });

        $.ajax({
            url: `${PATH_API}api/traspaso/${traspaso_id}/descargar-evidencia`,
            type: 'GET',    
            // xhrFields: { responseType: 'arraybuffer'},
            xhr: () => {
                let xhr = new XMLHttpRequest()
                xhr.onreadystatechange = () => {
                    if(xhr.readyState == 2){
                        xhr.responseType = "arraybuffer"
                    } else {
                        xhr.responseType = "json"
                    }
                }

                return xhr
            }
        })
        .done((response, status, xhr) => {
            let disposition = xhr.getResponseHeader("Content-Disposition")
            let filename = 'poliza-traspaso.pdf'
            let type = xhr.getResponseHeader("Content-Type")

            let blobFile = new Blob([response], { type: type })
            let URL = window.URL || window.webkitURL
            let downloadURL = URL.createObjectURL(blobFile)

            if(typeof window.navigator.msSaveBlob !== 'undefined'){
                window.navigator.msSaveBlob(blobFile, filename)

                let link = document.createElement('a')
                link.href = downloadURL
                link.download = filename
                document.body.appendChild(link)
                link.click()
                return ;
            }            

            window.open(downloadURL)
        })
        .fail((error) => {
            toastr.error("Ocurrio un error al descargar la poliza de traspaso.")
        })
        .always(() => {
            $.isLoading('hide')
        })
    }

    function aumentar(id){
        toastr.info('Aumentando cantidad de piezas a pedir.')

        $.post(`${PATH_API}api/traspaso/${id}/aumentar`)
        .done(response => {
            toastr.success('El aumento de piezas se realizo correctamente.')
            $('#refacciones').DataTable().ajax.reload()
        })
        .fail(error => {
            toastr.error('Ocurrio un error al aumentar la cantidad de piezas.')
        })
    }

    function disminuir(id){
        toastr.info('Disminuyendo cantidad de piezas a pedir.')

        $.post(`${PATH_API}api/traspaso/${id}/disminuir`)
        .done(response => {
            toastr.success('La disminución de piezas se realizo correctamente.')
            $('#refacciones').DataTable().ajax.reload()
        })
        .fail(error => {
            toastr.error('Ocurrio un error al disminuir la cantidad de piezas.')
        })
    }

    function aumentar_entregado(id){
        toastr.info('Aumentando cantidad de piezas a pedir.')

        $.post(`${PATH_API}api/traspaso/${id}/aumentar/entregado`)
        .done(response => {
            toastr.success('El aumento de piezas se realizo correctamente.')
            $('#refacciones').DataTable().ajax.reload()
        })
        .fail(error => {
            toastr.error('Ocurrio un error al aumentar la cantidad de piezas.')
        })
    }

    function disminuir_entregado(id){
        toastr.info('Disminuyendo cantidad de piezas a pedir.')

        $.post(`${PATH_API}api/traspaso/${id}/disminuir/entregado`)
        .done(response => {
            toastr.success('La disminución de piezas se realizo correctamente.')
            $('#refacciones').DataTable().ajax.reload()
        })
        .fail(error => {
            toastr.error('Ocurrio un error al disminuir la cantidad de piezas.')
        })
    }

    function aumentar_devuelto(id){
        toastr.info('Aumentando cantidad de piezas a pedir.')

        $.post(`${PATH_API}api/traspaso/${id}/aumentar/devuelto`)
        .done(response => {
            toastr.success('El aumento de piezas se realizo correctamente.')
            $('#refacciones').DataTable().ajax.reload()
        })
        .fail(error => {
            toastr.error('Ocurrio un error al aumentar la cantidad de piezas.')
        })
    }

    function disminuir_devuelto(id){
        toastr.info('Disminuyendo cantidad de piezas a pedir.')

        $.post(`${PATH_API}api/traspaso/${id}/disminuir/devuelto`)
        .done(response => {
            toastr.success('La disminución de piezas se realizo correctamente.')
            $('#refacciones').DataTable().ajax.reload()
        })
        .fail(error => {
            toastr.error('Ocurrio un error al disminuir la cantidad de piezas.')
        })
    }

    function detailsTraspaso(traspaso_id, estatus){
        $("#modal-traspaso-detalles").modal('show');
        $('#estatus_traspaso').val(estatus)
        $('#refacciones').DataTable().ajax.url(`${PATH_API}api/traspaso/${traspaso_id}/refacciones`).load();   
    }

    function detailsRefaccionesVentas(){
        $("#modal-refacciones-venta").modal('show');
        $('#ventas').DataTable().ajax.url(`${PATH_API}api/traspaso/refacciones-ventas`).load();   
    }

    $(document).ready(function(){

        $("#refacciones").DataTable({
            aaSorting: [],
            language: {
                url: PATH_LANGUAGE
            },
            columns: [
                {   
                    data: 'no_identificacion',
                    defaultContent: 'S/N'
                },
                {   
                    data: 'descripcion',
                    defaultContent: 'S/N'
                },
                {   
                    data: 'cantidad',
                    defaultContent: 'Sin asignar'
                },
                {
                    data: 'cantidad_entregada',
                    defaultContent: 0,
                },
                {
                    data: 'cantidad_devuelta',
                    defaultContent: 0,
                },
                {   
                    data: 'valor_unitario',
                    defaultContent: 'Sin asignar'
                },
                {
                    data: function(data){
                        let estatus = $('#estatus_traspaso').val()
                        let btn = ''

                        if((data.venta_id || data.traspaso_directo) && estatus == 'GENERADO')
                            btn += `<button class="btn btn-danger mx-1" onclick="disminuir(${data.id})">
                                    <i class="fa fa-minus"></i>
                                </button>`

                        if((data.venta_id || data.traspaso_directo) && estatus == 'GENERADO')
                            btn += `<button class="btn btn-success mx-1" onclick="aumentar(${data.id})">
                                    <i class="fa fa-plus"></i>
                                </button>`

                        if((! data.venta_id || ! data.traspaso_directo) && estatus == 'TRASPASO AUTORIZADO')
                            btn += `<button class="btn btn-danger mx-1" onclick="disminuir_entregado(${data.id})">
                                    <i class="fa fa-minus"></i>
                                </button>`

                        if((! data.venta_id || ! data.traspaso_directo) && estatus == 'TRASPASO AUTORIZADO')
                            btn += `<button class="btn btn-success mx-1" onclick="aumentar_entregado(${data.id})">
                                    <i class="fa fa-plus"></i>
                                </button>`

                        if((data.venta_id || data.traspaso_directo) && estatus == 'ENVIADO' && data.cantidad_entregada > 0)
                            btn += `<button class="btn btn-danger mx-1" onclick="disminuir_devuelto(${data.id})">
                                    <i class="fa fa-minus"></i>
                                </button>`

                        if((data.venta_id || data.traspaso_directo) && estatus == 'ENVIADO' && data.cantidad_entregada > 0)
                            btn += `<button class="btn btn-success mx-1" onclick="aumentar_devuelto(${data.id})">
                                    <i class="fa fa-plus"></i>
                                </button>`

                        return btn
                    }
                }
            ]
        })

        $("#ventas").DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            columns: [
                {   
                    data: 'folio',
                    defaultContent: 'S/N'
                },
                {   
                    data: 'cantidad',
                    defaultContent: 'Sin asignar'
                },
                {   
                    data: null,
                    render: (data) => {
                        let buttons

                        if(data)
                            buttons = `<button onclick="addTraspaso(${data.venta_id})" class="btn btn-primary mx-1">
                                <i class="fa fa-plus"></i>
                            </button>`   

                        return buttons
                    }
                },
            ]
        })

        $('#traspaso').DataTable({
            ajax: {
                url: PATH_API + "api/traspasos",
                type: 'GET',
                data: function(data) {
                },
            },
            aaSorting: [],
            language: {
                url: PATH_LANGUAGE
            },
            columns: [
                {   
                    data: 'folio',
                    defaultContent: 'S/N'
                },
                {   
                    data: 'origen',
                    defaultContent: 'Sin asignar'
                },
                {   
                    data: 'destino',
                    defaultContent: 'Sin asignar'
                },
                {   
                    data: 'cantidad',
                    defaultContent: '0'
                },
                {
                    data: 'estatus',
                    defaultContent: 'En espera'
                },
                {
                    data: function(data)
                    {
                        return (new Date(data.created_at)).toLocaleString('ES-MX', {
                            dateStyle: 'full',
                            timeStyle: 'short',
                        })
                    },
                    defaultContent: 'Sin fecha'
                },
                {
                    data: null,
                    render: (data) => {
                        buttons = ''

                        if(data.id)
                            buttons += 
                                `<button onclick="detailsTraspaso(${data.id}, '${data.estatus}')" title="Detalles Traspaso" class="btn btn-primary mx-1">
                                    <i class="fa fa-list"></i>
                                </button>`

                        if(data.estatus == "TRASPASO AUTORIZADO")
                            buttons +=
                                `<button onclick="downloadPDF(${data.id})" title="Descargar Poliza" class="btn btn-primary mx-1">
                                    <i class="fa fa-download"></i>
                                </button>`

                        if((data.id || data.traspaso_directo) && data.estatus == 'GENERADO')
                            buttons +=
                                `<button onclick="showFirmaSolicita(${data.id})" title="Firma Solicitud" class="btn btn-primary mx-1">
                                    <i class="fa fa-signature"></i>
                                </button>`

                        if( (! data.venta_id && ! data.traspaso_directo) && data.estatus == "SOLICITUD TRASPASO FIRMADA")
                            buttons +=
                                `<button onclick="showFirmaAutoriza(${data.id})"  title="Firma Autorizacion" class="btn btn-success mx-1">
                                    <i class="fa fa-signature"></i>
                                </button>`

                        if((! data.venta_id || ! data.traspaso_directo) && data.estatus == 'ENVIO EN DEVOLUCION')
                            buttons += 
                                `<button onclick="devueltoTraspaso(${data.id})" title="Traspaso Devuelto" class="btn btn-success mx-1">
                                    <i class="fa fa-check"></i>
                                </button>`

                        if(data.estatus == 'TRASPASO AUTORIZADO')
                            buttons += 
                                `<button onclick="cancelTraspaso(${data.id})" title="Cancelar Traspaso" class="btn btn-danger mx-1">
                                    <i class="fa fa-times"></i>
                                </button>`

                        if((! data.venta_id && ! data.traspaso_directo) && data.estatus == 'TRASPASO AUTORIZADO')
                            buttons += 
                                `<button onclick="envioTraspaso(${data.id})" title="Enviar Traspaso" class="btn btn-primary mx-1">
                                    <i class="fa fa-truck"></i>
                                </button>`

                        if((data.venta_id || data.traspaso_directo) && data.estatus == 'ENVIADO')
                            buttons += 
                                `<button onclick="showSubirEvidencia(${data.id})" title="Subir Evidencia" class="btn btn-primary mx-1">
                                    <i class="fa fa-upload"></i>
                                </button>`

                        if((data.venta_id || data.traspaso_directo) && data.estatus == 'SUBIR EVIDENCIA')
                            buttons += 
                                `<button onclick="finalizarTraspaso(${data.id})" title="Finalizar Traspaso" class="btn btn-success mx-1">
                                    <i class="fa fa-check"></i>
                                </button>`

                        if((data.venta_id || data.traspaso_directo) && data.estatus == 'FINALIZADO')
                            buttons += 
                                `<button onclick="downloadEvidencia(${data.id})" title="Descargar Evidencia" class="btn btn-primary mx-1">
                                    <i class="fa fa-download"></i>
                                </button>`

                        if((data.venta_id || data.traspaso_directo) && data.estatus == 'ENVIADO')
                            buttons += 
                                `<button onclick="devolucionTraspaso(${data.id})" title="Devolver Traspaso" class="btn btn-danger mx-1">
                                    <i class="fa fa-times"></i>
                                </button>`

                        return buttons
                    },
                }
            ],
        })
    })
</script>
@endsection