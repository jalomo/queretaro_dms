@layout('tema_luna/layout')
<style>
    .btn {
        background-color: #2487d8 !important;
        border-radius: 0px 5px 5px 0px !important;
    }

    .form-control {
        background-color: white !important;
        border-radius: 0px !important;
    }

    .vertical-timeline-content {
        position: relative;
        margin-left: 60px;
        background-color: rgba(68, 70, 79, 0.5);
        border-radius: 0.25em;
        border: 1px solid #c0cdff !important;
        border-radius: 10px !important;
    }
</style>
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">Mis notificaciones</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
        <li class="breadcrumb-item active">{{ ucwords($this->uri->segment(3)) }}</li>
    </ol>
    <div class="row">
        <div class="col-md-10">
            <div class="panel">
                <div>
                    <div id="cargando" class="spin text-center">
                        <i style="font-size:150px" class="fa fa-spinner fa-pulse fa-fw"></i>
                        <span class="sr-only">Loading...</span>
                    </div>
                    <div id="linea_tiempo" class="v-timeline vertical-container">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        getNotificacionesChat();
        //setInterval(getNotificacionesChat, 10000);
    });

    function getNotificacionesChat() {
        ajax.get(`api/notificaciones`, {
            telefono: telefono,
        }, function(response, headers) {
            $("#linea_tiempo").html('');
            $("#cargando").hide();
            if (headers.status == 200) {
                if (response && response.length >= 1) {
                    $.each(response, function(key, value) {
                        console.log(value);
                        suma = (parseInt(key) + parseInt(1))
                        user_title = value.usuario ? '<b>' + value.usuario + '</b>' : '';
                        enviado = value.mensaje_respuesta ? '<p class="text-info">' + user_name + ': ' + value.mensaje_respuesta + '</p>' : '';

                        if (value.celular2) {
                            input = '<div class="row">' +
                                '<textarea type="text" placeholder="Responder mensaje" id="responder_texto" class="form-control col-md-11 texto_' + value.id + '"/></textarea>' +
                                '<button id="envionotificacion" type="button" onclick="enviarNotificacion(this)" data-respuesta="' + value.mensaje + '" data-id="' + value.id + '" data-celular_destinatario="' + value.celular2 + '" class="btn btn-warning col-md-1 pt-1 pb-1"><i class="fa fa-play"></i></button>' +
                                '</div>'
                        } else {
                            input = "No se encontró el contacto";
                        }

                        $("#linea_tiempo").append('<div  class="vertical-timeline-block ' + value.id + '">' +
                            '<div class="vertical-timeline-icon">' +
                            '<i class="fa fa-calendar c-accent"></i>' +
                            '</div>' +
                            '<div class="vertical-timeline-content">' +
                            '<div class="p-sm">' +
                            '<span style="margin-top:-10px" class="pull-right"><i title="Eliminar notificación" onclick="eliminarNotificacion(' + value.id + ')" class="fa fa-times-circle text-danger fa-2x ml-2"></i></span>' +
                            '<span class="vertical-date pull-right"> ' + obtenerFechaMostrar(value.fecha_creacion) + ' </span>' +
                            '<h2>' + user_title + '</h2>' +
                            enviado +
                            '<p>' + user_title + ': ' + value.mensaje + '</p>' +
                            '</div>' +
                            '<div class="panel-footer">' +
                            input +
                            '</div>');
                    });
                } else {
                    $("#linea_tiempo").append("<h2>Sin resultados</h2>");
                    $("#linea_tiempo").removeClass("v-timeline");
                }
            }
        });
    }

    function eliminarNotificacion(id) {
        $.post('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/eliminar_notificacion', {
            id: id,
        }, function(response) {
            toastr.error("Notificación eliminada correctamente");
            $("." + id).remove();
        });
    }

    function enviarNotificacion(_this) {
        let id = $(_this).data('id');
        $("#envionotificacion").attr("disabled", true);
        ajax.post(`api/notificaciones`, {
            mensaje: $('.texto_' + id).val(),
            telefono: $(_this).data('celular_destinatario'),
            celular2: telefono,
            mensaje_respuesta: $(_this).data('respuesta')
        }, function(response, headers) {
            getNotificacionesChat();
            if (headers.status == 200) {
                $('#responder_texto').val('');
                $("#envionotificacion").attr("disabled", false);
                toastr.success("Notificación enviada correctamente");
            } else {
                $("#envionotificacion").attr("disabled", false);
            }
        });
    }

    function obtenerFechaMostrar(fecha) {
        const dia = 2,
            mes = 1,
            anio = 0;
        split = fecha.split(' ');
        fecha = split[0].split('-');
        return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio] + ' ' + split[1];
    }
</script>
@endsection