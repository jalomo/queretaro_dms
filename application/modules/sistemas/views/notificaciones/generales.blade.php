@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <br>
        <div class="row">
            <div class="col-sm-12">
                <div id="tabla-items">
                    <table id="tbl" class="table table-striped table-bordered">
                        <thead>
                            <tr class="tr_principal">
                                <!-- <th>Acciones</th> -->
                                <th>Fecha</th>
                                <th>Asunto</th>
                                <th>Descripción</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($notificaciones as $p => $registro)
                                <tr>
                                    <!-- <td>
           <a href="" data-id="{{ $venta->id }}" class="fa fa-info js_historial" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Historial comentarios" data-tabla="historial_comentarios_ventas"></a>
            </td> -->
                                    <td>{{ $registro->fecha_notificacion }}</td>
                                    <td>{{ $registro->asunto }}</td>
                                    <td>{{ $registro->texto }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        var site_url = "{{ site_url() }}";
        inicializar_tabla_local()

        function inicializar_tabla_local() {
            $('#tbl').DataTable({
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": "Anterior",
                        "sNext": "Siguiente",
                        "sLast": "Última",
                        "sFirst": "Primera"
                    },
                    "sLengthMenu": '<div id="combo_datatable">Mostrar <select>' +
                        '<option value="5">5</option>' +
                        '<option value="10">10</option>' +
                        '<option value="20">20</option>' +
                        '<option value="30">30</option>' +
                        '<option value="40">40</option>' +
                        '<option value="50">50</option>' +
                        '<option value="-1">Todos</option>' +
                        '</select> registros',
                    "sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)",
                    "sInfoFiltered": " - filtrados de _MAX_ registros",
                    "sInfoEmpty": "No hay resultados de búsqueda",
                    "sZeroRecords": "No hay registros para mostrar...",
                    "sProcessing": "Espere, por favor...",
                    "sSearch": "Buscar:"
                },
                "scrollX": true
            });
            $(".tr_principal").addClass('verde');
        }
    </script>
@endsection
