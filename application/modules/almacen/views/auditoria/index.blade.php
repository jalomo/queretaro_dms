@layout('tema_luna/layout')
@section('contenido')
<div>
    <h1 class="font-weight-bold">
        <i class="fa fa-marker"></i>
        Auditorias de Inventario
    </h1>
    <div class="d-flex my-3">
        <button onclick="modalAuditoria()" id="crear_auditoria" class="btn btn-primary ml-auto">
            <i class="fa fa-plus"></i>
            Nuevo Inventario
        </button>
    </div>
</div>
<div class="container-fluid panel-body">
    <table id="auditoria" class="table table-striped mt-4">
        <thead>
            <th>Numero Auditoria</th>
            <th>Usuario</th>
            <th>Fecha de Inicio</th>
            <th>Fecha de Termino</th>
            <th>Fecha de Creación</th>
            <th>Opciones</th>
        </thead>
    </table>
</div>

<div class="modal fade" id="modal-crear-auditoria" data-toggle="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form id="crear-auditoria" class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal">
                    <i class="fa fa-lock"></i>
                Ingresar Credenciales
                </h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Usuario:</label>
                    <input type="text" name="usuario" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Contraseña:</label>
                    <input type="password" name="password" class="form-control">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">
                    <i class="fas fa-plus"></i> 
                    Crear Inventario
                </button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script>
    let auditorias = []

    const search = () => {

        $.get(`${PATH_API}api/auditorias`)
        .then(response => {
            auditorias = response.aaData

            auditoria.clear()
            auditoria.rows.add(auditorias).draw()
        })
    }

    $('#crear-auditoria').submit((event) => {
        event.preventDefault()

        let formData = new FormData(event.target)
        let form = { 
            usuario: formData.get('usuario'),
            password: formData.get('password'),
        }

        ajax.post(`api/auditoria`, form, function(response, headers){

            if(headers.status == 422){
                toastr.error('Verificar Usuario y Contraseña.')
                return;
            }

            if(headers.status == 403){
                toastr.error('Verificar que no existan "Inventarios en Proceso"')
                return;
            }

            if(headers.status == 201){
                toastr.success('Se creo la auditoria correctamente.')
                search()
                $("#modal-crear-auditoria").modal('hide')
                return;
            }

            toastr.error('Ocurio un erro al crear la nueva auditoria.')
        })
    })

    var auditoria = $('#auditoria').DataTable({
        ordering: false,
        language: {
            url: PATH_LANGUAGE
        },
        data: auditorias,
        columns: [
            { data: 'id' },
            { data: 'usuario.nombre' },
            { data: function(data){
                let fecha = (new Date(data.inicio)).toLocaleString('ES-MX', {
                    dateStyle: 'medium',
                    timeStyle: 'short'
                })

                return fecha
            }},
            { data: function(data){

                if (data.terminada){
                    let fecha = (new Date(data.terminada)).toLocaleString('ES-MX', {
                        dateStyle: 'medium',
                        timeStyle: 'short'
                    })

                    return fecha
                }
                
                return 'Inventario en proceso.'
            }},
            { data: function(data){
                let fecha = (new Date(data.created_at)).toLocaleString('ES-MX', {
                    dateStyle: 'medium',
                    timeStyle: 'short',
                })

                return fecha
            }},
            { data: function(data){

                if(data.terminada){
                    return `<a href="${base_url}almacen/auditoria/mostrar/${data.id}" class="btn btn-primary">
                        <i class="fa fa-eye"></i>
                    </a>`
                }

                return `<a href="${base_url}almacen/auditoria/mostrar/${data.id}" class="btn btn-primary">
                        <i class="fa fa-edit"></i>
                    </a>`
            }},
        ]
    })

    const modalAuditoria = () => {
        $("#modal-crear-auditoria").modal('show')
    }

    $(document).ready(function(){
        search()
    })
</script>
@endsection