@layout('tema_luna/layout')
@section('contenido')
<div>
    <h1 class="font-weight-bold">
        <i class="fa fa-list"></i>
        Auditoria
    </h1>
    <div class="d-flex my-3">
        <a href="{{ base_url('almacen/auditoria') }}" id="crear_auditoria" class="btn btn-primary">
            <i class="fa fa-backward"></i>
            Regresar
        </a>
        <div class="ml-auto"></div>
        <button id="cargar_refacciones" class="btn ml-2 btn-primary">
            <i class="fa fa-tasks-alt"></i>
            Cargar Refacciones 
        </button>
        <button id="cancelar_auditoria" class="btn ml-2 btn-danger">
            <i class="fa fa-times"></i>
            Cancelar Auditoria
        </button>
        <button id="finalizar_auditoria" class="btn ml-2 btn-success">
            <i class="fa fa-check"></i>
            Finalizar Auditoria de Inventario
        </button>
        <button id="aplicar_ajustes" class="btn ml-2 btn-success">
            <i class="fa fa-thumbs-up"></i>
            Aplicar Ajustes
        </button>
    </div>
</div>
<div class="my-3">
    <select id="search" class="form-control" name="search"></select>
</div>
<div class="container-fluid panel-body">
    <table id="auditoria" class="table table-striped mt-4">
        <thead>
            <th>#</th>
            <th>No. Identificación</th>
            <th>Ubicación</th>
            <th>Existencia</th>
            <th>Sistema</th>
            <th>Opciones</th>
        </thead>
    </table>
</div>

<div class="modal fade" id="modal-aplicar-ajustes" data-toggle="modal" tabindex="-1" role="dialog"
    aria-labelledby="modalAplicarAjustes" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog modal-md" role="document">
        <form class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal">
                    <i class="fas fa-lock mr-2" title="Credenciales"></i>
                    Ingresar Credenciales
                </h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Usuario:</label>
                    <input name="usuario" type="text" class="form-control" >
                </div>
                <div class="form-group">
                    <label for="">Contraseña:</label>
                    <input name="password" type="password" class="form-control">
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-lock"></i>
                    Autorizar
                </button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script>
    let auditorias = []
    let json = {}

    $(document).ready(function(){

        search()

        $('#auditoria').on('submit', '.form-update', function (event) {
            event.preventDefault()
            let form = new FormData(event.target)
            let id = form.get('id')

            ajax.post(`api/auditoria/refaccion/${id}/update`, Object.fromEntries(form), function(response, headers){

                if(headers.status == 200){
                    event.target.querySelector('input').classList.add('border border-success') 
                    return;
                }

                toastr.error('Ocurio un error al actualizar la refaccion del inventario.')
            })
        })
        
        $('#auditoria').on('submit', '.form-delete', function (event) {
            event.preventDefault()
            let form = new FormData(event.target)
            let id = form.get('id')

            ajax.delete(`api/auditoria/refaccion/${id}/delete`, {}, function(response, headers){

                if(headers.status == 200){
                    search()
                    return;
                }

                toastr.error('Ocurio un error al eliminar la refaccion del inventario.')
            })
        })

        $('#auditoria').on('change', 'input[name="existencia_fisico"]', function(event){
            $(this).parents('form:first').submit();
        })

        $('#finalizar_auditoria').on('click', function(event){
            event.preventDefault()

            ajax.post(`api/auditoria/{{ $id }}/completar`, {}, function(response, headers){

                if(headers.status == 200){
                    toastr.success('Se finalizo la auditoria correctamente.')

                    return;
                }

                toastr.error('Ocurio un erro al finalizar la auditoria.')
            })
        })

        $('#cargar_refacciones').on('click', function(event){
            event.preventDefault()

            $.isLoading({
                text: 'Cargando Refacciones a inventario.'
            })

            ajax.post(`api/auditoria/{{ $id }}/cargar-refacciones`, {}, function(response, headers){

                $.isLoading("hide")

                if(headers.status == 200){
                    toastr.success('Se cargaron las refacciones correctamente.')
                    search()

                    return;
                }

                toastr.error('Ocurio un error al finalizar las refacciones a la auditoria.')
            })
        })

        $('#cancelar_auditoria').on('click', function(event){
            event.preventDefault()

            $.isLoading({
                text: 'Cancelando auditoria.'
            })

            ajax.post(`api/auditoria/{{ $id }}/cancelar`, {}, function(response, headers){

                $.isLoading("hide")

                if(headers.status == 200){
                    toastr.success('Se cancelo la auditoria.')
                    search()

                    return;
                }

                toastr.error('Ocurio un error al cancelar la auditoria.')
            })
        })

        $('#aplicar_ajustes').click(function(event){
            event.preventDefault()

            $('#modal-aplicar-ajustes').modal('show')
        })

        $('#modal-aplicar-ajustes').on('submit', 'form', function(event){
            event.preventDefault()

            let form = new FormData(event.target)

            $.isLoading({
                text: 'Aplicando ajustes.'
            })

            ajax.post(`api/auditoria/{{ $id }}/aplicar-ajustes`, Object.fromEntries(form), function(response, headers){

                $.isLoading("hide")

                if(headers.status == 200){
                    toastr.success('Se aplicaron los ajustes al inventario y a contabilidad.')
                    $('#modal-aplicar-ajustes').modal('show')
                    search()

                    return;
                }

                toastr.error('Ocurio un error al aplicar los ajustes.')
            })
        })

        $('#search').select2({
            placeholder: 'Agregar Refacción',
            minimumInputLength: 3,
            maximumInputLength: 20,
            delay: 500,
            ajax: {
                url: `${PATH_API}api/auditoria/refacciones`,
                data: function (params) {
                    let query = {
                        no_identificacion: params.term,
                    }
                    
                    return query
                },
                processResults: function (data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'

                    let items = data.map(item => { return { id: item.id, text: item.no_identificacion } })

                    return {
                        results: items? items : {}
                    }
                }
            }
        });

        $('#search').on('select2:select', function(event){
            let data = event.params.data
            let form = {
                producto_id: data.id,
                existencia_fisico: 0,
            }

            ajax.post(`api/auditoria/{{ $id }}/refaccion`, form,  function(response, headers){
                if(headers.status == 201){

                    toastr.success('La refaccion se agrego al inventario de la auditoria.')
                    search()
                    return;
                }

                toastr.error('La refaccion no se agrego.')
            })

        })
    })

    const search = () => {

        $.get(`${PATH_API}api/auditoria/` + {{ $id }})
        .then(response => {
            auditorias = response.productos
            json = response

            auditoria.clear()
            auditoria.rows.add(auditorias).draw()
        })
    }

    var auditoria = $('#auditoria').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        data: auditorias,
        columns: [
            { data: 'auditoria.id' },
            { data: 'no_identificacion' },
            { data: 'auditoria.ubicacion' },
            { data: function(data){
                return `<form class="form-update">
                    <input name="existencia_fisico" class="form-control text" type="number" value="${data.auditoria.existencia_fisico}"/>
                    <input name="existencia_sistema" type="hidden" value="${data.auditoria.existencia_sistema}"/>
                    <input name="ubicacion" type="hidden" value="${data.auditoria.ubicacion}"/>
                    <input name="auditoria_id" type="hidden" value="${data.auditoria.auditoria_id}"/>
                    <input name="id" type="hidden" value="${data.auditoria.id}"/>
                    <input name="producto_id" type="hidden" value="${data.id}"/>
                    </form>`
            }},
            { data: 'auditoria.existencia_sistema' },
            { data: function(data){
                return `<form class="form-delete">
                    <input type="hidden" name="id" value="${data.auditoria.id}"/>
                    <button class="btn btn-danger">
                        <i class="fa fa-trash-alt"></i>
                    </button>`
            }},
        ],
        fnDrawCallback: function(oSettings){
            
            if(json.deleted_at){
                $('#cargar_refacciones').hide()
                $('#cancelar_auditoria').hide()
                $('#finalizar_auditoria').hide()
                $('#aplicar_ajustes').hide()
                $('.form-delete button').prop('disabled', true)
                $('.form-update input').prop('disabled', true)
                $('#search').prop('disabled', true)
            }

            if(! json.terminada){
                $('#aplicar_ajustes').hide()
            }

            if(json.terminada && ! json.deleted_at){
                $('#cargar_refacciones').hide()
                $('#cancelar_auditoria').hide()
                $('#finalizar_auditoria').hide()
                $('.form-delete button').prop('disabled', true)
                $('.form-update input').prop('disabled', true)
                $('#search').prop('disabled', true)
                $('#aplicar_ajustes').show()
            }

            if(json.aprobada_por){
                $('#cargar_refacciones').hide()
                $('#cancelar_auditoria').hide()
                $('#finalizar_auditoria').hide()
                $('#aplicar_ajustes').hide()
                $('.form-delete button').prop('disabled', true)
                $('.form-update input').prop('disabled', true)
                $('#search').prop('disabled', true)
            }

        }
    })
</script>
@endsection