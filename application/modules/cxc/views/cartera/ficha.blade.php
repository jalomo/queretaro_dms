@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    
    <div class="row">
        <div class="col-md-12">
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group"> 
                            <label style="font-weight: bold;">Identificador:</label> <br>
                            <label for="" style="color:darkblue;"><?php if(isset($data->clave_identificador)){print_r($data->clave_identificador);} ?></label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label style="font-weight: bold;">Tipo de registro:</label>
                        <br>
                        <label for="" style="color:darkblue;">
                            <?php if(isset($data->tipo_registro)): ?>
                                <?php if($data->tipo_registro == "Contado") echo "Contado"; ?>
                                <?php if($data->tipo_registro == "Credito") echo "Credito"; ?>
                            <?php endif ?>                                
                        </label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label style="font-weight: bold;">Razón Social:</label>
                            <br>
                            <label for="" style="color:darkblue;"><?php if(isset($data->razon_social)){print_r($data->razon_social);} ?></label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label style="font-weight: bold;">RFC:</label>
                            <br>
                            <label for="" style="color:darkblue;"><?php if(isset($data->rfc)){print_r($data->rfc);} ?></label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <label style="font-weight: bold;">Nombre:</label>
                        <br>
                        <label for="" style="color:darkblue;"><?php if(isset($data->proveedor_nombre)){print_r($data->proveedor_nombre);} ?></label>
                    </div>
                    <div class="col-md-4">
                        <label style="font-weight: bold;">Apellido Paterno:</label>
                        <br>
                        <label for="" style="color:darkblue;"><?php if(isset($data->apellido_paterno)){print_r($data->apellido_paterno);} ?></label>
                    </div>
                    <div class="col-md-4">
                        <label style="font-weight: bold;">Apellido Materno:</label>
                        <br>
                        <label for="" style="color:darkblue;"><?php if(isset($data->apellido_materno)){print_r($data->apellido_materno);} ?></label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label style="font-weight: bold;">Domicilio:</label>
                            <br>
                            <label for="" style="color:darkblue;">
                                <?php if ($data->proveedor_calle): ?>
                                    <?php echo $data->proveedor_calle." ".$data->proveedor_numero.", ".$data->proveedor_colonia.", ".$data->proveedor_estado.", ".$data->proveedor_pais ?>
                                <?php endif ?>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label style="font-weight: bold;">Telefono 1:</label>
                            <br>
                            <label for="" style="color:darkblue;"><?php if(isset($data->telefono)){print_r($data->telefono);} ?></label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label style="font-weight: bold;">Telefono 2:</label>
                            <br>
                            <label for="" style="color:darkblue;"><?php if(isset($data->telefono_secundario)){print_r($data->telefono_secundario);} ?></label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label style="font-weight: bold;">Correo Electrónico 1:</label>
                            <br>
                            <label for="" style="color:darkblue;"><?php if(isset($data->correo_electronico)){print_r($data->correo_electronico);} ?></label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="" style="font-weight: bold;">Correo Electrónico 2:</label>
                            <br>
                            <label for="" style="color:darkblue;">
                                <?php if(isset($data->correo_electronico_secundario)){print_r($data->correo_electronico_secundario);} ?>
                            </label>
                        </div>
                    </div>
                </div>

                <br>
                <h4>Datos de pago</h4>
                <hr>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label style="font-weight: bold;">Forma de pago:</label>
                            <br>
                            @if(!empty($cat_tipo_pago))
                                @foreach ($cat_tipo_pago as $pago)
                                    <label for="" style="color:darkblue;">
                                        <?php if(isset($data->metodo_pago_id)){if($data->metodo_pago_id == $pago->id) echo "[".$pago->clave."] ".$pago->nombre;} ?>
                                    </label>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label style="font-weight: bold;">Método de pago:</label>
                            <br>
                            <label for="" style="color:darkblue;">
                                <?php if(isset($data->forma_pago)): ?>
                                    <?php if($data->forma_pago == "PUE") echo "Pago en una sola exhibición"; ?>
                                    <?php if($data->forma_pago == "PPD") echo "Pago en parcialidades o diferido"; ?>
                                <?php endif ?>                                
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label style="font-weight: bold;">CFDI:</label>
                            <br>
                            @if(!empty($cat_cfdi))
                                @foreach ($cat_cfdi as $cfdi)
                                    <label for="" style="color:darkblue;">
                                        <?php if(isset($data->cfdi_id)){if($data->cfdi_id == $cfdi->id) echo "[".$cfdi->clave."] ".$cfdi->nombre;} ?>
                                    </label>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label style="font-weight: bold;">Límite de crédito:</label>
                            <br>
                            <label for="" style="color:darkblue;">
                                $ <?php if(isset($data->limite_credito)){print_r($data->limite_credito);} ?>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label style="font-weight: bold;">Saldo:</label>
                            <br>
                            <label for="" style="color:darkblue;">
                                $ <?php if(isset($data->saldo)){print_r($data->saldo);} ?>
                            </label>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label style="font-weight: bold;">NOTAS:</label>
                            <label for="" style="color:darkblue;">
                                <?php if(isset($data->notas)){print_r($data->notas);} ?>
                            </label>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection