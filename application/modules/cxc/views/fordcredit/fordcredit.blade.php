@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="cliente">Cliente:</label>
                <select name="cliente_id" class="form-control" id="cliente_id">
                    <option value=""> Seleccionar cliente</option>
                    @foreach ($cat_cliente as $cliente)
                    <option value="{{ $cliente->id}}"> {{ $cliente->nombre }}</option>
                    @endforeach
                </select>
                <div id="cliente_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-6">
            <?php renderInputText("text", "direccion", "Direccion", '', true); ?>
        </div>
        <div class="col-md-6">
            <?php renderInputText("text", "estado", "Estado", '', true); ?>
        </div>
        <div class="col-md-6">
            <?php renderInputText("text", "municipio", "Municipio", '', true); ?>
        </div>
        <div class="col-md-12">
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="buro_credito">
                <label class="form-check-label" for="buro_credito">Buro de credito</label>
            </div>
        </div>
    </div>
    <hr>
    <h3 class="mt-3 mb-3">Referencias</h3>

    <div class="row">
        <div class="col-md-6">
            <?php renderInputText("text", "referencia_nombre", "Nombre referencia 1", '', false); ?>
        </div>
        <div class="col-md-6">
            <?php renderInputText("number", "referencia_telefono", "Telefono referencia 1", '', false); ?>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <?php renderInputText("text", "referencia_nombre_2", "Nombre referencia 2", '', false); ?>
        </div>
        <div class="col-md-6">
            <?php renderInputText("number", "referencia_telefono_2", "Telefono referencia 2", '', false); ?>
        </div>
    </div>
    <hr>

    <div class="row">
        <div class="col-md-12">
            <div class="col-md-9">
                <p style="text-align: justify; margin-bottom: 0px;color:black;">
                    1. Identificación vigente (Ine, Pasaporte )
                </p>
                <br>
                <div class="form-group">
                    <label for="">Seleccionar documento:</label>
                    <input type="file" class="form-control" id="identificacion" name="identificacion">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-9">
                <p style="text-align: justify; margin-bottom: 0px;color:black;">
                    2. 3 meses de recibos de nómina.
                </p>
                <br>
                <div class="form-group">
                    <label for="">Seleccionar documento:</label>
                    <input type="file" class="form-control" id="recibo_nomina" name="recibo_nomina">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-9">
                <p style="text-align: justify; margin-bottom: 0px;color:black;">
                    3. Comprobante de domicilio (Agua, luz o Predial)
                </p>
                <br>
                <div class="form-group">
                    <label for="">Seleccionar documento:</label>
                    <input type="file" class="form-control" id="comprobante_domicilio" name="comprobante_domicilio">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-9">
                <p style="text-align: justify; margin-bottom: 0px;color:black;">
                    4. Solicitud de credito firmada
                </p>
                <br>
                <div class="form-group">
                    <label for="">Seleccionar documento:</label>
                    <input type="file" class="form-control" id="solicitud_credito"  name="solicitud_credito">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <button type="button" id="subirSolicitudCredito" class="btn btn-primary">Subir</button>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    var tabla_cxc = $('#tbl_cxc').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        ajax: base_url + "cxc/index/ajax_cxc",
        columns: [{
                'data': function(data) {
                    return data.id;
                },

            },
            {
                'data': function(data) {
                    return data.concepto;
                },

            },
            {
                'data': function(data) {
                    return data.folio;
                },

            },
            {
                'data': function(data) {
                    return data.total;
                },

            },
            {
                'data': function(data) {
                    return data.nombre_cliente;
                },

            },
            {
                'data': function(data) {
                    return data.fecha;
                }
            },
            {
                'data': function(data) {
                    return "<a href='" + site_url + 'cxc/index/detallecuenta/' + data.id + "' class='btn btn-primary'><i class='fas fa-list'></i></a>";
                }
            }
        ]
    });
    $(document).ready(function() {
        // $("#menu_cxc").addClass("show");
        // $("#menu_cxc_saldo").addClass("show");
        // $("#menu_cxc_saldo").addClass("active");
        // $("#menu_cxc_saldo_sub").addClass("show");
        // $("#menu_cxc_saldo_sub").addClass("active");
        // $("#menu_cxc_saldo").addClass("show");
        // $("#menu_cxc_saldo").addClass("active");
        // $("#menu_cxc_Facturas").addClass("active");
        // $("#M07").addClass("active");

        $("#subirSolicitudCredito").on('click', function(e) {
            var paqueteDeDatos = new FormData();
            paqueteDeDatos.append('identificacion', $('#identificacion')[0].files[0]);
            paqueteDeDatos.append('recibo_nomina', $('#recibo_nomina')[0].files[0]);
            paqueteDeDatos.append('comprobante_domicilio', $('#comprobante_domicilio')[0].files[0]);
            paqueteDeDatos.append('solicitud_credito', $('#solicitud_credito')[0].files[0]);
            paqueteDeDatos.append('cliente_id', $('#cliente_id').val());
            paqueteDeDatos.append('buro_credito', $('#buro_credito').prop('checked') ? 1 : 0);
            paqueteDeDatos.append('referencia_nombre', $('#referencia_nombre').val());
            paqueteDeDatos.append('referencia_telefono', $('#referencia_telefono').val());
            paqueteDeDatos.append('referencia_nombre_2', $('#referencia_nombre_2').val());
            paqueteDeDatos.append('referencia_telefono_2', $('#referencia_telefono_2').val());
            ajax.postFile(`api/solicitud-credito`, paqueteDeDatos, function(response, header) {
                console.log(response, header.status);
                if (header.status == 201) {
                    let titulo = "Solicitud creada con exito!"
                    utils.displayWarningDialog(titulo, 'success', function(result) {
                        return window.location.href = base_url + "cxc/Index/solicitud_credito";
                    });
                }
                
                if (header.status == 500) {
                    let titulo = "Error creando solicitud!"
                    utils.displayWarningDialog(titulo, 'warning', function(result) {});
                }
            })

        })
    });
</script>
@endsection