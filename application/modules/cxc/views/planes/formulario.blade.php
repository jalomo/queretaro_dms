@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="cheque_estado"> <!-- enctype="multipart/form-data"  -->
                <h4>Datos de captura</h4>
                <hr>

                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="">Usuario que registra:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->usuario)){print_r($data->usuario);} ?>"  id="usuario" name="usuario" placeholder="">
                            <div id="usuario_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Fecha:</label>
                            <input type="date" class="form-control" min="<?= date('Y-m-d'); ?>" value="<?php if(isset($data->fecha)){print_r($data->fecha);}else {echo date('Y-m-d');} ?>" id="fecha" name="fecha" placeholder="">
                            <div id="fecha_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <br>
                <h4>Datos del plan</h4>
                <hr>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Nombre:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->nombre)){print_r($data->nombre);} ?>"  id="nombre" name="nombre" placeholder="">
                            <div id="nombre_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Descripción corta:</label>
                            <textarea class="form-control" cols="4" id="descripcion" name="descripcion"><?php if(isset($data->descripcion)){print_r($data->descripcion);} ?></textarea>
                            <div id="descripcion_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <h5 style="color:gray;">Plazos</h5>
                
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-check">
                              <input type="checkbox" checked class="form-check-input" id="meses12" value="plazos[]" value="12">
                              <label class="form-check-label" for="meses12">12 meses</label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-check">
                              <input type="checkbox" class="form-check-input" id="meses18" value="plazos[]" value="18">
                              <label class="form-check-label" for="meses18">18 meses</label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-check">
                              <input type="checkbox" class="form-check-input" id="meses24" value="plazos[]" value="24">
                              <label class="form-check-label" for="meses24">24 meses</label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-check">
                              <input type="checkbox" class="form-check-input" id="meses30" value="plazos[]" value="30">
                              <label class="form-check-label" for="meses30">30 meses</label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-check">
                              <input type="checkbox" class="form-check-input" id="meses36" value="plazos[]" value="36">
                              <label class="form-check-label" for="meses36">36 meses</label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-check">
                              <input type="checkbox" class="form-check-input" id="meses42" value="plazos[]" value="42">
                              <label class="form-check-label" for="meses42">42 meses</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-check">
                              <input type="checkbox" class="form-check-input" id="meses48" value="plazos[]" value="48">
                              <label class="form-check-label" for="meses48">48 meses</label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-check">
                              <input type="checkbox" class="form-check-input" id="meses54" value="plazos[]" value="54">
                              <label class="form-check-label" for="meses54">54 meses</label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-check">
                              <input type="checkbox" class="form-check-input" id="meses60" value="plazos[]" value="60">
                              <label class="form-check-label" for="meses60">60 meses</label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-check">
                              <input type="checkbox" class="form-check-input" id="meses66" value="plazos[]" value="66">
                              <label class="form-check-label" for="meses66">66 meses</label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-check">
                              <input type="checkbox" class="form-check-input" id="meses72" value="plazos[]" value="72">
                              <label class="form-check-label" for="meses72">72 meses</label>
                        </div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Comisión por apertura:</label>
                            <select class="form-control" id="comision" name="comision" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                <option value="Si">SI</option>
                                <option value="No">NO</option>
                            </select>
                            <div id="comision_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Comisión por apertura (%):</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->porcentaje_comision)){print_r($data->porcentaje_comision);} ?>"  id="porcentaje_comision" name="porcentaje_comision" placeholder="">
                            <div id="porcentaje_comision_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Tasa anual (%):</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->tasa_anual)){print_r($data->tasa_anual);} ?>"  id="tasa_anual" name="tasa_anual" placeholder="">
                            <div id="tasa_anual_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <h5 style="color:gray;">Tipo de persona que puede aplicar</h5>
                
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-check">
                              <input type="checkbox" checked class="form-check-input" id="Fisica" value="persona[]" value="Fisica">
                              <label class="form-check-label" for="Fisica">Fisica</label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-check">
                              <input type="checkbox" class="form-check-input" id="Moral" value="persona[]" value="Moral">
                              <label class="form-check-label" for="Moral">Moral</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" align="center">
                        <br>
                        <button type="button" id="guardar_formulario" class="btn btn-success">Guardar</button>
                        
                        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
                    </div>
                </div>
                        
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<!--<script src="{{ base_url('js/facturas/index.js') }}"></script>-->
@endsection