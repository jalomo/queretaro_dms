@layout('tema_luna/layout')
@section('contenido')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row mb-3">
        <div class="col-md-3">
            <label for="">Costo del vehículo:</label>
            <div class="input-group">
                <input type="text" class="form-control" value="" id="costo" placeholder="2100000">
            </div>
        </div>
        <div class="col-md-3">
            <label for="">Tipo de plan:</label>
            <div class="input-group">
                <select class="form-control" id="plan" >
                    <option value="">Selecionar ...</option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <label for="">Plazo:</label>
            <div class="input-group">
                <select class="form-control" id="plazo" >
                    <option value="">Selecionar ...</option>
                </select>
            </div>
        </div>
        <div class="col-md-3" align="right">
            <a class="btn btn-primary" href="" style="margin-top: 30px;">Generar Simulación</a>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_ventas" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Fecha pago</th>
                            <th>Saldo</th>
                            <th>Pago</th>
                            <th>Capital</th>
                            <th>Interes</th>
                            <th>Nuevo Saldo</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Fecha pago</th>
                            <th>Saldo</th>
                            <th>Pago</th>
                            <th>Capital</th>
                            <th>Interes</th>
                            <th>Nuevo Saldo</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12" align="center">
            <br>
            <button type="button" id="imprimir" class="btn btn-success">
                <i class="fas fa-print"></i>
                Imprimir simulación
            </button>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script>


    </script>
@endsection