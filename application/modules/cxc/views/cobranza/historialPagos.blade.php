@layout('tema_luna/layout')
@section('contenido')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row mb-3">
        <div class="col-md-4" align="left">
            <a class="btn btn-dark" href="<?php echo base_url('cxc/RelacionCobranza/index') ?>">Regresar</a>
        </div>
        <div class="col-md-8" align="right">
            <a class="btn btn-primary" href="#">
                <i class="fas fa-print"></i>
                Imprimir historial
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Clave:</label>
                <input type="text" class="form-control" value="<?php if(isset($data->clave)){print_r($data->clave);} ?>"  id="clave" name="clave" placeholder="">
                <div id="clave_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="form-group">
                <label for="">Nombre/Razón Social:</label>
                <input type="text" class="form-control" value="<?php if(isset($data->nombre)){print_r($data->nombre);} ?>"  id="nombre" name="nombre" placeholder="">
                <div id="cliente_error" class="invalid-feedback"></div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_ventas" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Estatus</th>
                            <th>Referencia</th>
                            <th>Abono</th>
                            <th>Fecha Pago</th>
                            <th>Fecha Vencimiento</th>
                            <th>Saldo</th>
                            <th>Moratorio</th>
                            <th>Días Moratorio</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($data))
                            @foreach ($data as $item)
                            <tr>
                                <td>#</td>
                                <td>{{$item->estatus_pago}}</td>
                                <td>{{$item->referencia}}</td>
                                <td>{{$item->pago}}</td>
                                <td>{{$item->fecha_pago}}</td>
                                <td>{{$item->fecha_programado}}</td>
                                <td>{{$item->saldo}}</td>
                                <td>{{$item->moratorio}}</td>
                                <td>{{$item->total}}</td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td>1</td>
                                <td style="background-color: #abebc6 ;">Pagado</td>
                                <td>ABONO CREDITO</td>
                                <td>$6,500</td>
                                <td>04/03/2020</td>
                                <td>04/03/2020</td>
                                <td>$6,500</td>
                                <td>$0</td>
                                <td>0</td>
                                <td>$6,500</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td style="background-color: #f5b7b1;">Moratorio</td>
                                <td>ABONO CREDITO</td>
                                <td>$6,500</td>
                                <td>14/04/2020</td>
                                <td>04/04/2020</td>
                                <td>$6,500</td>
                                <td>$650</td>
                                <td>10</td>
                                <td>$7,150</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Pendiente</td>
                                <td>ABONO CREDITO</td>
                                <td>$6,500</td>
                                <td>04/05/2020</td>
                                <td>04/05/2020</td>
                                <td>$6,500</td>
                                <td>$0</td>
                                <td>0</td>
                                <td>$6,500</td>
                            </tr>
                            
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Estatus</th>
                            <th>Referencia</th>
                            <th>Abono</th>
                            <th>Fecha Pago</th>
                            <th>Fecha Vencimiento</th>
                            <th>Saldo</th>
                            <th>Moratorio</th>
                            <th>Días Moratorio</th>
                            <th>Total</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
	<script>


	</script>
@endsection