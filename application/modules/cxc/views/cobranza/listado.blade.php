@layout('tema_luna/layout')
@section('contenido')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row mb-3">
        <div class="col-md-8"></div>
        <div class="col-md-4" align="right">
            <a class="btn btn-primary" href="<?php echo base_url('cxc/RelacionCobranza/crear') ?>">Nuevo registro</a>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-md-4">
            <label for="">Fecha inicio:</label> 
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="">
                        <i class="far fa-calendar-alt"></i>
                    </div>
                </div>

                <input type="date" class="form-control" max="<?= date('Y-m-d'); ?>" value="" id="fecha_inicio">
            </div>
        </div>
        <div class="col-md-4">
            <label for="">Fecha fin:</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="">
                        <i class="far fa-calendar-alt"></i>
                    </div>
                </div>

                <input type="date" class="form-control" max="<?= date('Y-m-d'); ?>" value="" id="fecha_fin">
            </div>
        </div>

        <div class="col-md-4">
            <label for="">Proveedor:</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="">
                        <i class="far fa-user"></i>
                    </div>
                </div>

                <select class="form-control" id="provedoor" >
                    <option value="">Selecionar ...</option>
                </select>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-md-4">
            <label for="">Estatus cuenta:</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="">
                        <i class="fas fa-ellipsis-v"></i>
                    </div>
                </div>

                <select class="form-control" id="estatus" >
                    <option value="">Selecionar ...</option>
                </select>
            </div>
        </div>
        <div class="col-md-4" align="right">
            <a class="btn btn-primary" href="" style="margin-top: 30px;">Buscar</a>
        </div>
        <div class="col-md-4">
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_ventas" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Folio</th>
                            <th>Proveedor</th>
                            <th>Referencia</th>
                            <th>Frecuencia</th>
                            <th>Proxima fecha de pago</th>
                            <th>Monto a pagar</th>
                            <th>Adeudo</th>
                            <th>Estatus Cuenta</th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($data))
                            @foreach ($data as $item)
                            <tr>
                                <td>#</td>
                                <td>{{$item->folio}}</td>
                                <td>{{$item->proveddor}}</td>
                                <td>{{$item->referencia}}</td>
                                <td>{{$item->frecuencia}}</td>
                                <td>{{$item->prox_pago}}</td>
                                <td>{{$item->pago}}</td>
                                <td>{{$item->adeudo}}</td>
                                <td>{{$item->estatus_cta}}</td>
                                <td>
                                    <a href="{{ base_url('cxc/RelacionCobranza/editar/'.$item->id) }}" title="Editar" class="btn btn-success" type="button">
                                        <i class="fas fa-pen"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="#" class="btn btn-danger" type="button" title="Cancelar">
                                        <i class="fas fa-window-close"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ base_url('cxc/RelacionCobranza/historialPagos/'.$item->id) }}" class="btn btn-warning" type="button" title="Historial de pagos">
                                        <i class="fas fa-history"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td>1</td>
                                <td>N° 0001</td>
                                <td>PROVEEDOR DEMO</td>
                                <td>PEDIDO BIMESTRAR REFACCIONES</td>
                                <td>30 DÍAS</td>
                                <td>04/06/2020</td>
                                <td>$6,000</td>
                                <td>$175,000</td>
                                <td>PUNTUAL</td>
                                <td>
                                    <a href="{{ base_url('cxc/RelacionCobranza/editar/1') }}" title="Editar" class="btn btn-success" type="button">
                                        <i class="fas fa-pen"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="#" class="btn btn-danger" type="button" title="Cancelar">
                                        <i class="fas fa-window-close"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ base_url('cxc/RelacionCobranza/historialPagos/1') }}" class="btn btn-warning" type="button" title="Historial de pagos">
                                        <i class="fas fa-history"></i>
                                    </a>
                                </td>
                            </tr>

                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Folio</th>
                            <th>Provedoor</th>
                            <th>Referencia</th>
                            <th>Frecuencia</th>
                            <th>Proxima fecha <br> de pago</th>
                            <th>Monto a pagar</th>
                            <th>Adeudo</th>
                            <th>Estatus Cuenta</th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

@endsection