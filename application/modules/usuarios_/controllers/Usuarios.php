<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Usuarios extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
    $this->load->library('curl');
    $this->load->helper('general');
  }


  public function administrarUsuarios()
  {
    $data['modulo'] = "Administrar";
    $data['subtitulo'] = "Usuarios";
    $data['submodulo'] = "Modulos usuarios";
    $usuarios = $this->curl->curlGet('api/usuarios');
    $dataUsuarios = procesarResponseApiJsonToArray($usuarios);
    $data['usuarios'] = isset($dataUsuarios) ? $dataUsuarios : [];
    $this->blade->render('usuarios/admin_usuarios', $data);
  }

  public function editarUsuario($id = '')
  {
    $data['modulo'] = "Administrar";
    $data['subtitulo'] = "Usuarios";
    $data['submodulo'] = "Modulos usuarios";
    $roles = $this->curl->curlGet('api/roles');
    $dataRoles = procesarResponseApiJsonToArray($roles);
    $data['roles'] = isset($dataRoles) ? $dataRoles : [];
    $usuarios = $this->curl->curlGet('api/usuarios/' . $id);
    $dataUsuarios = procesarResponseApiJsonToArray($usuarios);
    $data['usuarios'] = isset($dataUsuarios) ? $dataUsuarios : [];
    $this->blade->render('usuarios/editar_usuario', $data);
  }

}
