<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Movimientos extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
     $this->load->helper('general');
     $this->load->library('curl');
  }

  public function index()
  {
    $cuentas = $this->curl->curlGet('api/catalogo-cuentas');
    $data['cuentas'] = procesarResponseApiJsonToArray($cuentas);

    $subcuentas = $this->curl->curlGet('api/subcuenta');
    $data['subcuentas'] = procesarResponseApiJsonToArray($subcuentas);

    $cuentas = $this->curl->curlGet('api/polizas-contabilidad');
    $data['polizas'] = procesarResponseApiJsonToArray($cuentas);


  	$data['titulo'] = "Contabilidad/Movimiento/Captura de Poliza";
    $this->blade->render('movimientos/index',$data);
  }

  public function agregar_conceptos($id_poliza){
    $data['id_poliza'] = $id_poliza;
    $this->blade->render('movimientos/conceptos_polizas',$data);
  }


  public function ver()
  {
    $data['titulo'] = "Contabilidad/Movimiento/EditarPoliza";
    $this->blade->render('movimientos/ver',$data);
  }


  public function facturas()
  {
    $data['titulo'] = "Contabilidad/Movimiento/polizas facturas";
    $this->blade->render('movimientos/facturas',$data);
  }

  
}