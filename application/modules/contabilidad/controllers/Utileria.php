<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Utileria extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
  }

  public function cierre_mes()
  {
    $data['titulo'] = "Contabilidad/Utileria/Cierre de mes";
    $this->blade->render('utileria/cierre_mes', $data);
  }

  public function ejercicios()
  {
    $data['titulo'] = "Contabilidad/Reportes/Todos los ejercicios";
    $this->blade->render('utileria/ejercicios', $data);
  }

  public function contabilidad()
  {
    $data['titulo'] = "Módulo de contabilidad";
    $data['modulo'] = '';
    $this->blade->render('utileria/contabilidad_frame', $data);
  }

  public function polizas()
  {
    $data['titulo'] = "Módulo polizas";
    $data['modulo'] = 'asientos';
    $this->blade->render('utileria/contabilidad_frame', $data);
  }

  public function autorizacion_creditos()
  {
    $data['titulo'] = "Módulo autorización crédito";
    $data['modulo'] = 'autorizaciones/clientes';
    $this->blade->render('utileria/contabilidad_frame', $data);
  }
  
  public function cobranza()
  {
    $data['titulo'] = "Módulo cobranza";
    $data['modulo'] = 'cobranza/index';
    $this->blade->render('utileria/contabilidad_frame', $data);
  }

  public function formato_efectivo()
  {
    $data['titulo'] = "Módulo formato flujo efectivo";
    $data['modulo'] = 'formatos_efectivo/flujo_efectivo';
    $this->blade->render('utileria/contabilidad_frame', $data);
  }
  
}
