@layout('tema_luna/layout')
@section('contenido')
<h1 class="mt-4">{{ isset($titulo) ? $titulo : "" }}</h1>
<div class="row">
    <div class="col-md-12 table-responsive ">
        <iframe class="embed-responsive-item" src="<?php echo API_CONTABILIDAD . '/' . $data['modulo'] . '?acceso=1'; ?>" frameborder="0" id="panel_ventanilla" style="height:95vh;width:100%;"></iframe>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(() => {
            $("#panel_ventanilla").contents().find("#accordionSidebar").hide()
            $("#panel_ventanilla").contents().find("#title_contabilidad").hide()
        }, 1000);
    });
</script>
@endsection