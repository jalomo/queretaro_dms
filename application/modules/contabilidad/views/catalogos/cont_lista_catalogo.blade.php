@layout('tema_luna/layout')
@section('contenido')

<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    

    <div class="row">
        <div class="col-md-4">
            <form id="registro_catalogo">
                <div class="form-group">
                 
                     <?php echo renderInputText("text", "NO_CUENTA", "No. de Cuenta",""); ?>
                </div>
                <div class="form-group">
                    <?php echo renderInputText("text", "NOMBRE_CUENTA", "Nombre Cta",""); ?>
                   
                </div>
                <div class="form-group">
                    <?php echo renderInputText("text", "CVE_AUXILIAR", "Cve. Auxiliar",""); ?>
                   
                </div> 

                <div class="form-group">
                    <?php echo renderInputText("text", "TIPO_CUENTA", "Tipo cuenta",""); ?>
                    
                </div> 

                 <div class="form-group">
                    <?php echo renderInputText("text", "CODIGO", "Còdigo agrup.",""); ?>
                   
                </div> 

                <div class="form-group">
                    <?php echo renderInputText("text", "NATURALEZA", "Naturaleza",""); ?>
                   
                </div> 
                
                

               
                
                <br>
                <button type="button" id="guarda_cuenta" class="btn btn-primary">Guardar</button>
                
                <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
            </form>
        </div>
    </div>
</div>


<hr/>

<div class="row">
  <div class="col-md-12">     
  <table class="table table-striped">
    <thead>
      <tr>
        <th>No. de Cuenta</th>
        <th>Nombre Cta</th>
        <th>Cve. Auxiliar</th>
        <th>Tipo cuenta</th>
        <th>Còdigo</th>
        <th>Naturaleza</th>
        <th>Opciones</th>
      </tr>
    </thead>
    <tbody>
     @foreach ($cuentas as $row)
      <tr>
        <td>{{$row->no_cuenta}}</td>
        <td>{{$row->nombre_cuenta}}</td>
        <td>{{$row->cve_auxiliar}}</td>
        <td>{{$row->tipo_cuenta}}</td>
        <td>{{$row->codigo}}</td>
        <td>{{$row->naturaleza}}</td>
        <td>
            <a href="<?php echo base_url()?>contabilidad/catalogos/editar" class="btn btn-primary" type="button" ><i class="fas fa-pen"></i></a>

        		
        		<button class="btn btn-danger btn-borrar" type="button" data-id="{{$row->id}}"><i class="far fa-trash-alt"></i></button>

        </td>
      </tr>
      @endforeach

      
      
    </tbody>
  </table>
  </div>  
</div>


@endsection
@section('scripts')
<script type="text/javascript">
    $("#guarda_cuenta").on('click', function() {

        //$(".invalid-feedback").html("");
        console.log(procesarForm());
       
        ajax.post(`api/catalogo-cuentas`, procesarForm(), function(response, headers) {
            if (headers.status == 400) {
                console.log(headers);
                return ajax.showValidations(headers);
            }

            utils.displayWarningDialog(headers.message, "success", function(data) {
                return window.location.href = base_url + `contabilidad/catalogos/index`;
            })
        })
    });

    let procesarForm = function() {
        let form = $('#registro_catalogo').serializeArray();
        let newArray = {
            no_cuenta: document.getElementById("NO_CUENTA").value,
            nombre_cuenta: document.getElementById("NOMBRE_CUENTA").value,
            cve_auxiliar: document.getElementById("CVE_AUXILIAR").value,
            tipo_cuenta: document.getElementById("TIPO_CUENTA").value,
            codigo: document.getElementById("CODIGO").value,
            naturaleza: document.getElementById("NATURALEZA").value
            
        };
        return newArray;
    }


        $(document).ready(function(){
            $("#menu_contabilidad").addClass("show");
            $("#menu_cont_cat").addClass("show");
            $("#menu_cont_cat").addClass("active");
            $("#menu_cat_conta").addClass("show");
            $("#menu_cat_conta").addClass("active");
            $("#menu_cat_cuentas").addClass("active");
            $("#M05").addClass("active");

            $(".btn-borrar").click(function(event){
                     event.preventDefault();

                      var id = $(this).data('id')
            borrar(id) 



                    
                 });

        });


         
        function borrar(id){
                Swal.fire({
              title: 'Estás seguro?',
              text: "Esta acción no se puede revertir!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, borrar!'
            }).then((result) => {
              if (result.value) {
                            $.ajax({
                            url: base_url+'contabilidad/catalogos/borrarCuenta/'+id,
                            success: function(respuesta) {
                                Swal.fire(
                              'Borrado!',
                              'El registro se ha borrado',
                              'success'
                            )
                            location.reload(true)
                           // 
                            },
                            error: function() {
                                console.log("No se ha podido obtener la información");
                            }
                            });   
                         }
            })
             }
</script>
@endsection