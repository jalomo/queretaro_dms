@layout('tema_luna/layout')
@section('contenido')

<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    

    <div class="row">
        <div class="col-md-12">

          <div class="row">
            <div class="col-md-12">
              <button type="button" class="btn btn-primary">PDF</button>
            </div>
          </div>

          <h3>Diario de polizas</h3>
            
           <div class="row">
              <div class="col-md-12">     
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Ren</th>
                    <th>Cuenta</th>
                    <th>Concepto</th>
                    <th>Importe</th>
                    
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>001</td>
                    <td>1050-01</td>
                    <td>Reembolso Bancomer</td>
                    <td>$8500.00</td>
                  </tr>

                   <tr>
                    <td>002</td>
                    <td>1050-01</td>
                    <td>Reembolso Bancomer</td>
                    <td>$8500.00</td>
                  </tr>

                  
                  
                </tbody>
              </table>
              </div>  
            </div>
        </div>
    </div>
</div>



@endsection
@section('scripts')
<script type="text/javascript">
        $(document).ready(function(){
            $("#menu_contabilidad").addClass("show");
            $("#menu_cont_rep").addClass("show");
            $("#menu_cont_rep").addClass("active");
            $("#menu_cont_re").addClass("show");
            $("#menu_cont_re").addClass("active");
            $("#menu_cont_rep_diario").addClass("active");
            $("#M05").addClass("active");
        });
</script>
@endsection