@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row mb-3">
        <div class="col-md-8"></div>
        <div class="col-md-4" align="right">
            <a class="btn btn-primary" href="<?php echo base_url('autos/ventas/unidades') ?>">Nueva Pre-venta</a>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-md-4">
            <label for="">Fecha inicio:</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="">
                        <i class="far fa-calendar-alt"></i>
                    </div>
                </div>

                <input type="date" class="form-control" max="<?= date('Y-m-d'); ?>" value="" id="fecha_inicio">
            </div>
        </div>
        <div class="col-md-5">
            <label for="">Fecha fin:</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="">
                        <i class="far fa-calendar-alt"></i>
                    </div>
                </div>

                <input type="date" class="form-control" max="<?= date('Y-m-d'); ?>" value="" id="fecha_fin">
            </div>
        </div>
        <div class="col-md-3 mt-4">
            <button class="btn btn-primary btn-block" id="btn-filtrar">  Filtrar</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_ventas">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Fecha solicitud</th>
                            <th>Cliente</th>
                            <th>Unidad</th>
                            <th>Enganche</th>
                            <th>Total</th>
                            <th>Vendedor</th>
                            <th>Estatus</th>
                            <th>Id de cita</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody> </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Fecha solicitud</th>
                            <th>Cliente</th>
                            <th>Unidad</th>
                            <th>Enganche</th>
                            <th>Total</th>
                            <th>Vendedor</th>
                            <th>Estatus</th>
                            <th>Id de cita</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script  type="text/javascript">
    

    var tabla_pre_ventas = $('#tbl_ventas').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        "ajax": {
            url: base_url + "autos/ventas/ajax_preventaEquipoOpcional",
            type: 'POST',
            data: {
                fecha_inicio: () => $('#fecha_inicio').val(),
                fecha_fin: () => $('#fecha_fin').val()
            }
        },
        columns: [
            {
                'data': function(data){
                    return data.id
                }
            },
            {
                'data':function (data){
                    return utils.dateToLetras(data.created_at);
                }
            },
            {
                'data': 'nombre_cliente'
            },
            {
                'data':function (data){
                    return `${data.nombre_marca} - ${data.nombre_modelo} de color  ${data.nombre_color} `;
                }
            },
            {
                'data': 'enganche'
            },
            {
                'data': 'total'
            },
            {
                'data': 'nombre_vendedor'
            },
            {
                'data': 'estatus_venta'
            },
            {
                'data': function(data) {
                    if(data.servicio_cita_id){
                        return data.servicio_cita_id;
                    }else{

                        return "--";
                    }
                }
            },
            {
                'data': function(data) {
                    //entrega unidad
                    return "<a href='"+base_url+'autos/EquipoAdicional/agregarEquipo/'+data.id+"' class='btn btn-warning'><i class='fas fa-car'></i></a>";
                }
            }
        ]
    });

    $("#btn-filtrar").on('click', function(){
        tabla_pre_ventas.ajax.reload();
    });
  
</script>
<script type="text/javascript"></script>
@endsection