@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <h3 class="mt-3 mb-4">Datos de la preventa</h3>
    <div class="row">
        <div class="col-md-4">
            <input type="hidden" name="preventa_id" id="preventa_id" value="{{ isset($detalle_unidad) ? $detalle_unidad->id :'' }}">
            <?php renderInputText("text", "modelo", "Modelo",  isset($detalle_unidad) ? $detalle_unidad->modelo : null, true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "modelo_descripcion", "Modelo descripcion",  isset($detalle_unidad) ? $detalle_unidad->modelo_descripcion : null, true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "linea", "Linea",  isset($detalle_unidad) ? $detalle_unidad->linea : null, true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "economico", "No economico",  isset($detalle_unidad) ? $detalle_unidad->economico : null, true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "transmision", "Transmision",  isset($detalle_unidad) ? $detalle_unidad->transmision : null, true); ?>
        </div>
        <div class="col-md-2">
            <?php renderInputText("text", "cilindros", "Cilindros",  isset($detalle_unidad) ? $detalle_unidad->cilindros : null, true); ?>
        </div>
        <div class="col-md-2">
            <?php renderInputText("text", "puertas", "Puertas",  isset($detalle_unidad) ? $detalle_unidad->puertas : null, true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "combustible", "Combustible",  isset($detalle_unidad) ? $detalle_unidad->combustible : null, true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "motor", "Motor",  isset($detalle_unidad) ? $detalle_unidad->motor : '', true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "transmision", "Transmision",  isset($detalle_unidad) ? $detalle_unidad->transmision : '', true); ?>
        </div>
    </div>


    <h3 class="mt-3 mb-3">Datos del cliente</h3>
    <div class="row">
       <div class="col-md-6">
            <div class="form-group">
                <label for="select">Nombre</label>
                <input class="form-control" type="text" {{ isset($detalle_unidad) ? 'disabled' : '' }} id="nombre_cliente" value="{{ isset($detalle_unidad) ? $detalle_unidad->nombre_cliente . ' ' . $detalle_unidad->cliente_apellido_paterno : '' }}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="select">RFC</label>
                <input class="form-control" type="text" id="rfc_cliente" {{ isset($detalle_unidad) ? 'disabled' : '' }}  value="{{ isset($detalle_unidad) ? $detalle_unidad->cliente_rfc : '' }}">
            </div>
        </div>
    </div>

    <h3 class="mt-3 mb-4">Datos del vendedor</h3>
    <div class="row">
        <div class="col-md-6">
            <?php renderInputText("text", "vendedor", "Vendedor",  isset($detalle_unidad->nombre_vendedor) ? $detalle_unidad->nombre_vendedor . ' ' . $detalle_unidad->apellido_paterno_vendedor : '', isset($detalle_unidad) ? true : false); ?>
            <input type="hidden" id="vendedor_id" name="vendedor_id" value="{{ isset($detalle_unidad) ? $detalle_unidad->id_asesor : '' }}">
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="fecha_alta">Fecha de alta:</label>
                <input type="date" class="form-control" min="{{ date('Y-m-d') }}" value="{{ isset($data->created_at) ? $data->created_at :  date('Y-m-d') }}" id="orden_fecha" name="orden_fecha" placeholder="">
                <div id="fecha_alta_error" class="invalid-feedback"></div>
            </div>
        </div>
    </div>
    
    <h3 class="mt-4 mb-4">Equipo opcional</h3>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered" id="tbl_carrito" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>No identificación</th>
                        <th>Descripcion</th>
                        <th>Total</th>
                        <th>Precio</th>
                        <th>Cantidad</th>
                        <th>Unidad</th>
                        <th>-</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>No identificación</th>
                        <th>Descripcion</th>
                        <th>Total</th>
                        <th>Valor unitario</th>
                        <th>Cantidad</th>
                        <th>Unidad</th>
                        <th>-</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
	<h3 class="mt-4 mb-4">Productos disponibles</h3>
	
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_productos" width="100%" cellspacing="0">
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    var tabla_carrito = $('#tbl_carrito').DataTable({
		language: {
			url: PATH_LANGUAGE
		},
		"ajax": {
			url: base_url + "autos/equipoAdicional/ajax_detalle_equipo_carrito",
			type: 'POST',
			data: {
				preventa_id: function () {
					return $('#preventa_id').val()
				}
			}
		},
		columns: [{
				'data': 'item_id'
			},
			{
				'data': function (data) {
					return utils.isDefined(data.no_identificacion) && data.no_identificacion ? data
						.no_identificacion : null
				}
			},
			{
				'data': function (data) {
					return utils.isDefined(data.descripcion) && data.descripcion ? data
						.descripcion : null
				}
			},
			{
				'data': function (data) {
					return utils.isDefined(data.total) ? "$ " + data.total : null
                    // return data.item_id
				}
			},
			{
				'data': function (data) {
					return utils.isDefined(data.valor_unitario) ? "$ " + data.valor_unitario : null
				}
			},
			{
				'data': function (data) {
					return utils.isDefined(data.cantidad) && data.cantidad ? data.cantidad : null
				}
			},
			{
				'data': function (data) {
					return utils.isDefined(data.unidad) && data.unidad ? data.unidad : null
				}
			},
			{
				'data': function (data) {
					// if (utils.isDefined(estatus_venta_id) && estatus_venta_id != 1) {
						return '--';
					// } else {
					// 	return "<button type='button' class='btn-borrar btn btn-primary' data-id=" + data
					// 		.id + "><i class='fas fa-trash'></i></button>";
					// }
                    // return data.item_id
				}
			}
		]
	});

    var tabla_stock = $('#tbl_productos').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            "ajax": {
                url: PATH_API + "api/productos/listadoStock",
                type: 'GET',
            },
            columns: [{
                    title: "#",
                    data: 'id',
                },
                {
                    title: "Prefijo",
                    data: 'prefijo',
                },
                {
                    title: "Sufijo",
                    data: 'sufijo',
                },
                {
                    title: "Basico",
                    data: 'basico',
                },
                {
                    title: "No. de pieza",
                    data: 'no_identificacion',
                },
                {
                    title: "Descripcion",
                    data: 'descripcion',
                },
                {
                    title: "Precio unitario",
                    data: 'valor_unitario',
                },
                {
                    title: "Unidad",
                    data: 'unidad',
                },
                {
                    title: "Ubicación",
                    data: 'ubicacionProducto',
                },
                {
                    title: "-",
                    render: function(data, type, row) {
                        if(!row.cantidad_almacen_primario || row.cantidad_almacen_primario == 0){
                            return '-';
                        }
                        
                        let btn_cart = '<button data-toggle="modal" data-product_name="'+row.descripcion+'"  data-product_id="'+row.id+'" data-no_identificacion="'+row.no_identificacion+'" data-cantidad_almacen_primario="'+row.cantidad_almacen_primario+'" data-valor_unitario="'+row.valor_unitario+'" class="btn btn-primary" onclick="modalDetalle(this)"><i class="fas fa-shopping-cart"></i> </button>';
                        return btn_cart;

                    }
                }


            ]
        });

    function modalDetalle(_this) {
        $("#producto_id").val($(_this).data('product_id'));
        $("#valor_unitario").val($(_this).data('valor_unitario'));
        $("#no_identificacion").val($(_this).data('no_identificacion'));
        
        var producto_name = $(_this).data('product_name');
        $("#title_modal").text(producto_name);
        $("#modal-producto-detalle").modal('show');
    }

    function agregarproducto(id) {
		toastr.info("Añadiendo producto al carro de compras");
		$.isLoading({
			text: "Añadiendo producto al carro de compras...."
		});
        
		let folio_id = $('#folio_id').val();
		let producto_id = $("#producto_id").val();
		let cantidad = $("#cantidad").val();
		let id_venta_auto = $("#preventa_id").val();
        let precio = $("#valor_unitario").val();
		ajax.post(`api/autos/equipo-opcional`, {
            producto_id,cantidad,id_venta_auto,precio
        }, function (response, headers) {
			if (headers.status == 201 || headers.status == 200) {
				$("#modal-producto-detalle").modal('hide');
				tabla_carrito.ajax.reload();
		// 		$.ajax({
		// 			type: "GET",
		// 			url: base_url + "refacciones/salidas/ajax_calcular_venta_total/" + $('#folio_id')
		// 			.val(),
		// 			dataType: "json",
		// 			success: function (response) {
		// 				let venta = parseInt(response.venta_total);
		// 				let titulo = "Producto añadido al carro de compras correctamente"
		// 				$('#venta_total').val(venta);
		// 				utils.displayWarningDialog(titulo, 'success', function (result) {
		// 					window.location.reload();
		// 				});
		// 			}, complete: function() {
						$.isLoading("hide");
		// 			} 
		// 		});
			} else {
				$.isLoading("hide");
			}
		})
	}
</script>
@endsection

@section('modal')
<div class="modal fade" id="modal-producto-detalle" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo renderInputText("number", "cantidad", "Cantidad", 1); ?>
                        <input type="hidden" name="producto_id" id="producto_id">
                        <input type="hidden" name="valor_unitario" id="valor_unitario">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button onclick="agregarproducto()" id="btn-modal-agregar" type="button" class="btn btn-primary"><i class="fas fa-shopping-cart"></i> Agregar</button>
            </div>
        </div>
    </div>
</div>
@endsection