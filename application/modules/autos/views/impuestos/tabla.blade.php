@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="tabulador"> <!-- enctype="multipart/form-data"  -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="producto_id_sat">No. de Orden:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->impuesto_folio)){print_r($data->impuesto_folio);} ?>"  id="impuesto_folio" name="impuesto_folio" placeholder="">
                            <div id="impuesto_folio_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

				<div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="producto_id_sat">Usuario:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->impuesto_usuario)){print_r($data->impuesto_usuario);} ?>"  id="impuesto_usuario" name="impuesto_usuario" placeholder="">
                            <div id="impuesto_usuario_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="producto_id_sat">Fecha de consulta:</label>
                            <input type="date" class="form-control" min="<?= date('Y-m-d'); ?>" value="<?php if(isset($data->impuesto_fecha)){print_r($data->impuesto_fecha);}else {echo date('Y-m-d');} ?>" id="impuesto_fecha" name="impuesto_fecha" placeholder="">
                            <div id="impuesto_fecha_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <hr>
                <h4>Cálculo ISAN <?= date("Y"); ?> </h4>

                <div class="row" style="margin-top:10px;">
                    <div class="col-md-1" align="center" style="color:white;">

                    </div>
                    <div class="col-md-4">
                        <label for="">Costo del vehículo:</label>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text" id="">
                                    $
                                </div>
                            </div>
                            <input type="text" class="form-control" value="<?php if(isset($data->impuesto_costo)){print_r($data->impuesto_costo);} ?>"  id="impuesto_costo" name="impuesto_costo" placeholder="210,000">
                        </div>
                        <div id="impuesto_costo_error" class="invalid-feedback"></div>
                    </div>
                </div>

                <div class="row" style="margin-top:10px;">
                    <div class="col-md-1" align="center" style="color:white;">
                        <i class="fas fa-equals"></i>
                    </div>
                    <div class="col-md-4">
                        <label for="">Limite inferior:</label>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text" id="">
                                    $
                                </div>
                            </div>
                            <input type="text" class="form-control" value="<?php if(isset($data->impuesto_limInf)){print_r($data->impuesto_limInf);} ?>"  id="impuesto_limInf" name="impuesto_limInf" placeholder="0.01">
                        </div>
                        <div id="impuesto_limInf_error" class="invalid-feedback"></div>
                    </div>
                </div>

                <div class="row" style="margin-top:10px;">
                    <div class="col-md-1" align="center" style="color:white;">
                        <i class="fas fa-minus"></i>
                    </div>
                    <div class="col-md-4">
                        <label for="">Excedente del limite inferior:</label>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text" id="">
                                    $
                                </div>
                            </div>
                            <input type="text" class="form-control" value="<?php if(isset($data->impuesto_excedenteLim)){print_r($data->impuesto_excedenteLim);} ?>"  id="impuesto_excedenteLim" name="impuesto_excedenteLim" placeholder="209999.99">
                        </div>
                        <div id="impuesto_excedenteLim_error" class="invalid-feedback"></div>
                    </div>
                </div>

                <div class="row" style="margin-top:10px;">
                    <div class="col-md-1" align="center" style="color:white;">
                        <i class="fas fa-times"></i>
                    </div>
                    <div class="col-md-4">
                        <label for="">Tasa:</label>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text" id="">
                                    %
                                </div>
                            </div>
                            <input type="text" class="form-control" value="<?php if(isset($data->impuesto_excedenteLim)){print_r($data->impuesto_tasa);} ?>"  id="impuesto_excedenteLim" name="impuesto_excedenteLim" placeholder="2.00">
                        </div>
                        <div id="impuesto_excedenteLim_error" class="invalid-feedback"></div>
                    </div>
                </div>

                <div class="row" style="margin-top:10px;">
                    <div class="col-md-1" align="center" style="color:white;">
                        <i class="fas fa-plus"></i>
                    </div>
                    <div class="col-md-4">
                        <label for="">Cuota Fija:</label>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text" id="">
                                    $
                                </div>
                            </div>
                            <input type="text" class="form-control" value="<?php if(isset($data->impuesto_cuota)){print_r($data->impuesto_tasa);} ?>"  id="impuesto_cuota" name="impuesto_cuota" placeholder="0.00">
                        </div>
                        <div id="impuesto_cuota_error" class="invalid-feedback"></div>
                    </div>
                </div>

                <div class="row" style="margin-top:10px;">
                    <div class="col-md-1" align="center" style="color:white;">
                        <i class="fas fa-equals"></i>
                    </div>
                    <div class="col-md-4">
                        <label for="">ISAN:</label>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text" id="">
                                    $
                                </div>
                            </div>
                            <input type="text" class="form-control" value="<?php if(isset($data->impuesto_isan)){print_r($data->impuesto_tasa);} ?>"  id="impuesto_isan" name="impuesto_isan" placeholder="4,200.00">
                        </div>
                        <div id="impuesto_isan_error" class="invalid-feedback"></div>
                    </div>
                </div>


                <br>
                <hr>
                <div class="row">
                    <div class="col-md-12" align="center" style="border:1px solid yellow;">
                        <h4>Cálculo ISAN <?= date("Y"); ?> (Referencia)</h4>
                        <table class="table">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" align="center" style="width: 25%;">Límite inferior ($)</th>
                                    <th scope="col" align="center" style="width: 25%;">Límite superior ($)</th>
                                    <th scope="col" align="center" style="width: 25%;">Cuota fija ($)</th>
                                    <th scope="col" align="center" style="width: 25%;">Tasa para aplicarse sobre el excedente <br> del limite inferior (%)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th align="center">0.01</th>
                                    <td align="center">274,964.76</td>
                                    <td align="center">0.00</td>
                                    <td align="center">2.0</td>
                                </tr>
                                <tr>
                                    <th align="center">274,964.77</th>
                                    <td align="center">329,957.65</td>
                                    <td align="center">5,499.20</td>
                                    <td align="center">5.0</td>
                                </tr>
                                <tr>
                                    <th align="center">329,957.66</th>
                                    <td align="center">384,950.76</td>
                                    <td align="center">8,248.98</td>
                                    <td align="center">10.0</td>
                                </tr>
                                <tr>
                                    <th align="center">384,950.77</th>
                                    <td align="center">494,936.36</td>
                                    <td align="center">13,748.26</td>
                                    <td align="center">15.0</td>
                                </tr>
                                <tr>
                                    <th align="center">494,936.37</th>
                                    <td align="center">En adelante</td>
                                    <td align="center">30,246.07</td>
                                    <td align="center">17.0</td>
                                </tr>
                                <tr>
                                    <th colspan="4">
                                        Si el precio del automóvil es superior a $759,271.24 se reducirá del monto del impuesto determinado la cantidad que resultede aplicar el 7% sobre la diferencia entre el precio de la unidad y los $759,271.24
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>


            	<div class="row">
                    <div class="col-md-12" align="center">
                        <br>
                        <button type="button" id="guardar_almacén" class="btn btn-success">Guardar</button>
                        
                        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
                    </div>
                </div>
                        
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection