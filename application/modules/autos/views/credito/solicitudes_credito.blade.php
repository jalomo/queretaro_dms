@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    @include('ventas/partial_venta_steps')
    <div class="col-md-12 mt-4">
        <div class="table-responsive">
            <table class="table table-bordered" id="tbl_solicitudes"></table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        var tabla_pre_ventas = $('#tbl_solicitudes').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            "ajax": {
                url: base_url + "autos/credito/ajax_listado_solicitudes_credito",
                type: 'GET'
            },
            columns: [{
                    title: "#",
                    data: function(data) {
                        return data.id
                    }
                },
                {
                    title: "Cliente",
                    data: function(data) {
                        return `${data.nombre} ${data.apellido_paterno}`;
                    }
                },
                {
                    title: "Unidad",
                    data: function(data) {
                        return `${data.unidad_descripcion}`;
                    }
                },
                
                {
                    title: "Vendedor",
                    data: 'nombre_vendedor'
                },
                {
                    title: "Estatus de venta",
                    data: 'estatus_venta'
                },
                {
                    title: "documentacion",
                    data: function({id, id_estatus_venta}) {
                        return "<a alt='documentacion preventa' href='" + base_url + 'autos/ventas/documentacionpreventaunidad/' + id + "' class='btn btn-primary'><i class='far fa-file-alt'></i></a>";
                        
                    }
                },
                {
                    title: "Aprobar",
                    data: function({id, id_estatus_venta}) {
                        return "<a alt='documentacion preventa' href='" + base_url + 'autos/credito/aprobarcredito/' + id + "' class='btn btn-primary'><i class='far fa-file-alt'></i></a>";
                        
                    }
                }
            ]
        });

        $("#btn-filtrar").on('click', function() {
            tabla_pre_ventas.ajax.reload();
        });

    </script>
@endsection
