@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : ''; ?>
            </li>
            <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : ''; ?></li>
            <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : ''; ?></li>
        </ol>

        <div class="row mb-3">
            <div class="col-md-8"></div>
            <div class="col-md-4" align="right">
                <a class="btn btn-primary"
                    href="<?php echo base_url('autos/ventas/unidades'); ?>">Nueva Pre-venta</a>
            </div>
        </div>

        <div class="row mb-3">
            <div class="col-md-4">
                <?php renderInputText('date', 'fecha_inicio', 'Fecha inicio:', null); ?>
            </div>
            <div class="col-md-4">
                <?php renderInputText('date', 'fecha_fin', 'Fecha fin:', null); ?>
            </div>
            <div class="col-md-4 mt-4">
                <button class="btn btn-primary btn-block" id="btn-filtrar"> Filtrar</button>
            </div>
            <div class="col-md-12  mt-4">
                <div class="table-responsive">
                    <table class="table table-bordered text-center" id="tbl_facturacion"></table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        var tabla_pre_ventas = $('#tbl_facturacion').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            "ajax": {
                url: base_url + "autos/credito/ajax_solicitud_facturacion",
                type: 'GET'
            },
            columns: [{
                    title: "#",
                    data: function(data) {
                        return data.id
                    }
                },
                {
                    title: "Fecha solicitud",
                    data: function(data) {
                        return utils.dateToLetras(data.created_at);
                    }
                },
                {
                    title: "Cliente",
                    data: 'nombre_cliente'
                },
                {
                    title: "Unidad",
                    data: function(data) {
                        return `${data.unidad_descripcion}`;
                    }
                },
                {
                    title: "Enganche",
                    data: 'enganche'
                },
                {
                    title: "Total",
                    data: 'total'
                },
                {
                    title: "Vendedor",
                    data: 'nombre_vendedor'
                },
                {
                    title: "-",
                    data: function({id, id_estatus_venta}) {
                        let btn  = ''
                        if([1,2,5].includes(id_estatus_venta)){ // preventa, credito aprobado, abono pagado
                            btn +=  " <a alt='documentacion preventa' href='" + base_url + 'autos/credito/documentacionfacturacion/' + id + "' class='btn btn-primary'><i class='far fa-file-pdf'></i></a>";
                        }

                        return btn;
                    }
                }
            ]
        });

        $("#btn-filtrar").on('click', function() {
            tabla_pre_ventas.ajax.reload();
        });

    </script>
    <script type="text/javascript"></script>
@endsection
