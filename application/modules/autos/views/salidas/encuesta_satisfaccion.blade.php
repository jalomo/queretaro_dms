@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    @include('ventas/partial_venta_steps')
    <h2 class="text-center">Encuesta de Satisfacción del Cliente</h2>
    <div class="row">
        <div class="col-md-12 mt-4">
            <p> ¡Nos interesa conocer su opinión! Para cada una de las siguientes opciones; por favor selecciona la respuesta que mejor describa su experiencia de ventas en <?php echo NOMBRE_SUCURSAL;?>, S.A. DE C.V.</p>
            <p>Por favor califique, en general, como fue su experiencia de ventas en <?php echo NOMBRE_SUCURSAL;?>, S.A. DE C.V.</p>
        </div>
        <div class="col-md-12 text-center">
            <span class="mr-3">
                <b>MALA</b>
            </span>
            <img src="{{base_url('img/estrella_llena.png')}}" onclick="handlestar(1)" id="star_one" class="img-fluid mr-3 cursor" style="width: 4rem;">
            <img src="{{base_url('img/estrella_llena.png')}}" onclick="handlestar(2)" id="star_two" class="img-fluid mr-3" style="width: 4rem;">
            <img src="{{base_url('img/estrella_llena.png')}}" onclick="handlestar(3)" id="star_three" class="img-fluid mr-3" style="width: 4rem;">
            <img src="{{base_url('img/estrella_llena.png')}}" onclick="handlestar(4)" id="star_four" class="img-fluid mr-3" style="width: 4rem;">
            <img src="{{base_url('img/estrella_llena.png')}}" onclick="handlestar(5)" id="star_five" class="img-fluid mr-3" style="width: 4rem;">
            <span class="mr-3">
                <b>EXELENTE</b>
            </span>
        </div>
        <div class="col-md-12 mt-4">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <td colspan="5" align="justify" >
                            1.- La atención recibida por su asesor de ventas fue: <br>
                       </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                            Mala &nbsp;&nbsp;
                            <input type="radio" class="" {{ $data_encuesta->atencion_asesor == 1 ? 'checked' : ''  }}  style="transform: scale(1.7);" value="1" name="atencion_asesor">
                        </td>
                        <td class="text-center">
                            Regular &nbsp;&nbsp;
                            <input type="radio" class="" {{ $data_encuesta->atencion_asesor == 2 ? 'checked' : ''  }} style="transform: scale(1.7);" value="2" name="atencion_asesor">
                        </td>
                        <td class="text-center">
                            Buena &nbsp;&nbsp;
                            <input type="radio" class="" {{ $data_encuesta->atencion_asesor == 3 ? 'checked' : ''  }} style="transform: scale(1.7);" value="3" name="atencion_asesor">
                        </td>
                        <td class="text-center">
                            Muy Buena &nbsp;&nbsp;
                            <input type="radio" class="" {{ $data_encuesta->atencion_asesor == 4 ? 'checked' : '' }} style="transform: scale(1.7);" value="4" name="atencion_asesor">
                        </td>
                        <td class="text-center">
                            Excelente &nbsp;&nbsp;
                            <input type="radio" {{ $data_encuesta->atencion_asesor == 5 ? "checked" : '' }} style="transform: scale(1.7);" value="5" name="atencion_asesor">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" align="justify" >
                            2.- La experiencia del financiamiento en general o del pago de su vehículo fue: <br>
                       </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                            Mala &nbsp;&nbsp;
                            <input type="radio"  {{ $data_encuesta->experiencia_financiamiento == 1 ? 'checked' : ''  }} style="transform: scale(1.7);" value="1" name="experiencia_financiamiento">
                        </td>
                        <td class="text-center">
                            Regular &nbsp;&nbsp;
                            <input type="radio"  {{ $data_encuesta->experiencia_financiamiento == 2 ? 'checked' : ''  }} style="transform: scale(1.7);" value="2" name="experiencia_financiamiento">
                        </td>
                        <td class="text-center">
                            Buena &nbsp;&nbsp;
                            <input type="radio"  {{ $data_encuesta->experiencia_financiamiento == 3 ? 'checked' : ''  }} style="transform: scale(1.7);" value="3" name="experiencia_financiamiento">
                        </td>
                        <td class="text-center">
                            Muy Buena &nbsp;&nbsp;
                            <input type="radio"  {{ $data_encuesta->experiencia_financiamiento == 4 ? 'checked' : ''  }} style="transform: scale(1.7);" value="4" name="experiencia_financiamiento">
                        </td>
                        <td class="text-center">
                            Excelente &nbsp;&nbsp;
                            <input type="radio"  {{ $data_encuesta->experiencia_financiamiento == 5 ? 'checked' : ''  }} style="transform: scale(1.7);" value="5" name="experiencia_financiamiento">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" align="justify" >
                            3.- La experiencia de entrega de su vehículo nuevo fue:
                            <br>
                       </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                            Mala &nbsp;&nbsp;
                            <input type="radio"  {{ $data_encuesta->experiencia_entrega_vehiculo == 1 ? 'checked' : ''  }} style="transform: scale(1.7);" value="1" name="experiencia_entrega_vehiculo">
                        </td>
                        <td class="text-center">
                            Regular &nbsp;&nbsp;
                            <input type="radio"  {{ $data_encuesta->experiencia_entrega_vehiculo == 2 ? 'checked' : ''  }} style="transform: scale(1.7);" value="2" name="experiencia_entrega_vehiculo">
                        </td>
                        <td class="text-center">
                            Buena &nbsp;&nbsp;
                            <input type="radio"  {{ $data_encuesta->experiencia_entrega_vehiculo == 3 ? 'checked' : ''  }} style="transform: scale(1.7);" value="3" name="experiencia_entrega_vehiculo">
                        </td>
                        <td class="text-center">
                            Muy Buena &nbsp;&nbsp;
                            <input type="radio"  {{ $data_encuesta->experiencia_entrega_vehiculo == 4 ? 'checked' : ''  }} style="transform: scale(1.7);" value="4" name="experiencia_entrega_vehiculo">
                        </td>
                        <td class="text-center">
                            Excelente &nbsp;&nbsp;
                            <input type="radio"  {{ $data_encuesta->experiencia_entrega_vehiculo == 5 ? 'checked' : ''  }} style="transform: scale(1.7);" value="5" name="experiencia_entrega_vehiculo">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" align="justify" >
                            4.- ¿En <?php echo NOMBRE_SUCURSAL;?>, S.A. DE C.V. el seguimiento a los compromisos hechos con usted fue?
                            <br>
                       </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                            Mala &nbsp;&nbsp;
                            <input type="radio"  {{ $data_encuesta->seguimiento_compromisos == 1 ? 'checked' : ''  }} style="transform: scale(1.7);" value="1" name="seguimiento_compromisos">
                        </td>
                        <td class="text-center">
                            Regular &nbsp;&nbsp;
                            <input type="radio"  {{ $data_encuesta->seguimiento_compromisos == 2 ? 'checked' : ''  }} style="transform: scale(1.7);" value="2" name="seguimiento_compromisos">
                        </td>
                        <td class="text-center">
                            Buena &nbsp;&nbsp;
                            <input type="radio"  {{ $data_encuesta->seguimiento_compromisos == 3 ? 'checked' : ''  }} style="transform: scale(1.7);" value="3" name="seguimiento_compromisos">
                        </td>
                        <td class="text-center">
                            Muy Buena &nbsp;&nbsp;
                            <input type="radio"  {{ $data_encuesta->seguimiento_compromisos == 4 ? 'checked' : ''  }} style="transform: scale(1.7);" value="4" name="seguimiento_compromisos">
                        </td>
                        <td class="text-center">
                            Excelente &nbsp;&nbsp;
                            <input type="radio"  {{ $data_encuesta->seguimiento_compromisos == 5 ? 'checked' : ''  }} style="transform: scale(1.7);" value="5" name="seguimiento_compromisos">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-12 mb-4">
            <?php renderInputTextArea("comentarios_mejoras", "Sus comentarios son muy valiosos para nosotros. ¿Qué salió particularmente bien durante su experiencia de ventas? ¿Qué podríamos mejorar?",  isset($data_encuesta) ? $data_encuesta->comentarios_mejoras : ''); ?>
        </div>                  
    </div>
    <div class="row mt-4">
        <div class="col-md-12 text-center">
            <h2> ¡¡¡GRACIAS!!! Por hacer EQUIPO con nosotros y AYUDARNOS a ser MEJORES para USTED </h2>
        </div>
        <div class="col-md-6">
            <?php renderInputText("text", "nombre_asesor", "Nombre del asesor",  isset($nombre_asesor) ? $nombre_asesor : '', true); ?>
            <input type="hidden" name="id_venta_unidad" id="id_venta_unidad" value="{{$data_encuesta->id}}">
            <input type="hidden" name="experiencia_ventas" id="experiencia_ventas" value="{{ isset($data_encuesta->experiencia_ventas) ? $data_encuesta->experiencia_ventas : '' }}">
            
        </div> 
        <div class="col-md-6">
            <?php renderInputText("date", "fecha", "Fecha",  date('d-m-Y'), true); ?>
        </div> 
        <div class="col-md-6">
            <?php renderInputText("text", "nombre_cliente", "Nombre de cliente",  isset($nombre_cliente) ? $nombre_cliente : '', true); ?>
        </div> 
        <div class="col-md-6">
            <?php renderInputText("text", "correo_electronico", "Correo electronico",  isset($data_encuesta->correo_electronico) ? $data_encuesta->correo_electronico : '', true); ?>
        </div> 
        <div class="col-md-6">
            <?php renderInputText("text", "telefono", "Telefono",  isset($data_encuesta->telefono) ? $data_encuesta->telefono : '', true); ?>
        </div> 
        <div class="col-md-6">
            <button class="btn btn-danger mt-4" id="btn_enviar_encuesta">
                Enviar encuesta
            </button>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        
        const handlestar =(id_star) =>{
            switch (id_star) {
                case 1:
                    document.getElementById('experiencia_ventas').value = 1;
                    document.getElementById("star_one").src = base_url + "img/estrella_llena.png";
                    document.getElementById("star_two").src = base_url + "img/estrella_vacia.png";
                    document.getElementById("star_three").src = base_url + "img/estrella_vacia.png";
                    document.getElementById("star_four").src = base_url + "img/estrella_vacia.png";
                    document.getElementById("star_five").src = base_url + "img/estrella_vacia.png";
                break;
                case 2:
                    document.getElementById('experiencia_ventas').value = 2
                    document.getElementById("star_one").src = base_url + "img/estrella_llena.png";
                    document.getElementById("star_two").src = base_url + "img/estrella_llena.png";
                    document.getElementById("star_three").src = base_url + "img/estrella_vacia.png";
                    document.getElementById("star_four").src = base_url + "img/estrella_vacia.png";
                    document.getElementById("star_five").src = base_url + "img/estrella_vacia.png";
                break;
                case 3:
                    document.getElementById('experiencia_ventas').value = 3;
                    document.getElementById("star_one").src = base_url + "img/estrella_llena.png";
                    document.getElementById("star_two").src = base_url + "img/estrella_llena.png";
                    document.getElementById("star_three").src = base_url + "img/estrella_llena.png";
                    document.getElementById("star_four").src = base_url + "img/estrella_vacia.png";
                    document.getElementById("star_five").src = base_url + "img/estrella_vacia.png";
                break;
                case 4:
                    document.getElementById("star_one").src = base_url + "img/estrella_llena.png";
                    document.getElementById("star_two").src = base_url + "img/estrella_llena.png";
                    document.getElementById("star_three").src = base_url + "img/estrella_llena.png";
                    document.getElementById("star_four").src = base_url + "img/estrella_llena.png";
                    document.getElementById("star_five").src = base_url + "img/estrella_vacia.png";
                    document.getElementById('experiencia_ventas').value = 4;
                    
                break;
                case 5:
                    document.getElementById('experiencia_ventas').value = 5;
                    document.getElementById("star_one").src = base_url + "img/estrella_llena.png";
                    document.getElementById("star_two").src = base_url + "img/estrella_llena.png";
                    document.getElementById("star_three").src = base_url + "img/estrella_llena.png";
                    document.getElementById("star_four").src = base_url + "img/estrella_llena.png";
                    document.getElementById("star_five").src = base_url + "img/estrella_llena.png";
                break;
            
                default:
                    break;
            }
        }
        
        handlestar(parseInt(document.getElementById('experiencia_ventas').value));

       $("#btn_enviar_encuesta").on('click', function() {
            if(
                !document.querySelector('input[name="atencion_asesor"]:checked') ||
                !document.querySelector('input[name="experiencia_financiamiento"]:checked') ||
                !document.querySelector('input[name="experiencia_entrega_vehiculo"]:checked') ||
                !document.querySelector('input[name="seguimiento_compromisos"]:checked')
            ){
                return toastr.error("Por favor completa correctamente la encuesta!");
            }

            ajax.post('api/encuesta-satisfaccion', {
                'id_venta_unidad':document.getElementById('id_venta_unidad').value,
                'atencion_asesor':document.querySelector('input[name="atencion_asesor"]:checked').value,
                'experiencia_financiamiento':document.querySelector('input[name="experiencia_financiamiento"]:checked').value,
                'experiencia_entrega_vehiculo':document.querySelector('input[name="experiencia_entrega_vehiculo"]:checked').value,
                'seguimiento_compromisos':document.querySelector('input[name="seguimiento_compromisos"]:checked').value,
                'comentarios_mejoras':document.getElementById('comentarios_mejoras').value
            }, function(response, headers) {
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }
                
                utils.displayWarningDialog("Gracias por sus comentarios", "success", function(data) {
                    return window.location.href = base_url + 'autos/ventas/index';
                })
            })
        });
    </script>
@endsection
