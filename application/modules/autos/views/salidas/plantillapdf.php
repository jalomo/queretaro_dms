<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Poliza compras</title>
	<style>
		.contenedor {
			width: 100%;
		}

		.col-12 {
			width: 100%;
			padding: 3px;
		}

		td {
			font-size: 11px !important;
		}

		.col-6 {
			float: left;
			width: 49%;
			padding: 3px;
		}

		.col-5 {
			float: left;
			width: 40%;
			padding: 3px;
		}

		.col-3 {
			float: left;
			width: 30%;
			padding: 3px;
		}

		.col-2 {
			float: left;
			width: 20%;
			padding: 3px;
		}

		table {
			border-collapse: collapse;
			width: 100%;
		}

		table,
		th,
		td {
			border: 1px solid #233a74;
			margin-bottom: 12px
		}
	</style>
</head>

<body>
	<div class="contenedor">
		<div class="col-12">
			<?php echo NOMBRE_SUCURSAL; ?>, S.A. DE C.V.
		</div>
		<div class="col-5" align="right">
			SALIDA DE UNIDAD << DEFINITIVA>> << FORD>>
		</div>
		<div class="col-3" align="right">
			<br>
		</div>
		<div class="col-3" align="right">
			FOLIO: <?php echo isset($data->no_economico) ? substr($data->no_economico, -4) : "0000"; ?>
		</div>
	</div>

	<div class="contenedor">
		<div class="col-5">
			<table class="table" cellpadding="5">
				<thead>
					<tr>
						<th style="font-size: 11px;width: 30%;" scope="col">
							Nombre del Cliente :
						</th>
						<th style="font-size: 11px;width: 69%;border-bottom: 0.5px solid black;" scope="col">
							<?php echo isset($data->nombre_cliente) ? $data->nombre_cliente : "Sin Información"; ?>
						</th>
					</tr>
				</thead>
			</table>
		</div>
		<div class="col-2">
			<table class="table" cellpadding="5">
				<thead>
					<tr>
						<th style="font-size: 11px;width: 30%;" scope="col">
							Catálogo :
						</th>
						<th style="font-size: 11px;width: 69%;border-bottom: 0.5px solid black;" scope="col">
							<?php echo isset($data->catalogo) ? $data->catalogo : "Sin Información"; ?>
						</th>
					</tr>
				</thead>
			</table>
		</div>
		<div class="col-3">
			<table class="table" cellpadding="5">
				<thead>
					<tr>
						<th style="font-size: 11px;width: 30%;" scope="col">
							# Economico :
						</th>
						<th style="font-size: 11px;width: 69%;border-bottom: 0.5px solid black;" scope="col">
							<?php echo isset($data->no_economico) ? $data->no_economico : "Sin Información"; ?>
						</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</body>

</html>