@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <?php renderInputText("text", "nombre_cliente", "Cliente",  isset($detalle_unidad) ? $detalle_unidad->nombre_cliente : null, true); ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-4">
            <input type="hidden" name="id_unidad" id="id_unidad" value="{{ isset($detalle_unidad) ? $detalle_unidad->id :'' }}">
            <?php renderInputText("text", "modelo", "Modelo",  isset($detalle_unidad) ? $detalle_unidad->modelo : null, true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "modelo_descripcion", "Modelo descripcion",  isset($detalle_unidad) ? $detalle_unidad->modelo_descripcion : null, true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "linea", "Linea",  isset($detalle_unidad) ? $detalle_unidad->linea : null, true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "economico", "No economico",  isset($detalle_unidad) ? $detalle_unidad->economico : null, true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "transmision", "Transmision",  isset($detalle_unidad) ? $detalle_unidad->transmision : null, true); ?>
        </div>
        <div class="col-md-2">
            <?php renderInputText("text", "cilindros", "Cilindros",  isset($detalle_unidad) ? $detalle_unidad->cilindros : null, true); ?>
        </div>
        <div class="col-md-2">
            <?php renderInputText("text", "puertas", "Puertas",  isset($detalle_unidad) ? $detalle_unidad->puertas : null, true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "combustible", "Combustible",  isset($detalle_unidad) ? $detalle_unidad->combustible : null, true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "motor", "Motor",  isset($detalle_unidad) ? $detalle_unidad->motor : 'SI', true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "transmision", "Transmision",  isset($detalle_unidad) ? $detalle_unidad->transmision : 'SI', true); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form id="form-salida" data-id="<?php echo isset($formato->id) ? $formato->id : 'SI'?>" method="post">
                <hr>
                <div class="row">
                    <div class="col-md-6 table-responsive">
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <td colspan="4" style="font-weight: bold;">
                                    Explicación del Funcuionamiento de su Vehículo
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    ASISTENCIA CONECTIVIDAD
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    SI
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NO
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NA
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Asistente de Areanque en Pendientes
                                    <input type="hidden" name="asistente_arranque" id="asistente_arranque" value="{{ isset($formato->funcionamiento_vehiculo->asistente_arranque) ? $formato->funcionamiento_vehiculo->asistente_arranque : 'SI'; }}">
                                    <div id="asistente_arranque_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked class="tb1_ck1" name="asistente_arranque_ck" value="SI" {{ isset($formato->funcionamiento_vehiculo->asistente_arranque) && $formato->funcionamiento_vehiculo->asistente_arranque == "SI" ? "checked" : ""; }} >
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck1" name="asistente_arranque_ck" value="NO" {{ isset($formato->funcionamiento_vehiculo->asistente_arranque) && $formato->funcionamiento_vehiculo->asistente_arranque == "NO" ? "checked" : ""; }} >
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck1" name="asistente_arranque_ck" value="NA" {{ isset($formato->funcionamiento_vehiculo->asistente_arranque) && $formato->funcionamiento_vehiculo->asistente_arranque == "NA" ? "checked" : ""; }} >
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Sistema de Preservación de carril
                                    <input type="hidden" name="preservacion_carril" id="preservacion_carril" value="{{ isset($formato->funcionamiento_vehiculo->preservacion_carril) ? $formato->funcionamiento_vehiculo->preservacion_carril : 'SI' }}">
                                    <div id="preservacion_carril_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked class="tb1_ck2" name="preservacion_carril_ck" value="SI" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->preservacion_carril == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck2" name="preservacion_carril_ck" value="NO" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->preservacion_carril == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck2" name="preservacion_carril_ck" value="NA" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->preservacion_carril == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Sensores Delanteros y Laterals
                                    <input type="hidden" name="sensores_del_lat" id="sensores_del_lat" value="<?php echo isset($formato->funcionamiento_vehiculo) ? $formato->funcionamiento_vehiculo->sensores_del_lat : 'SI'?>">
                                    <div id="sensores_del_lat_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  checkedclass="tb1_ck3" name="sensores_del_lat_ck" value="SI" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->sensores_del_lat == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck3" name="sensores_del_lat_ck" value="NO" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->sensores_del_lat == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck3" name="sensores_del_lat_ck" value="NA" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->sensores_del_lat == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Monitoreo de Punto Ciego con Alerta de Tráfico Cruzado (BLIS)
                                    <input type="hidden" name="blis" id="blis" value="<?php echo isset($formato->funcionamiento_vehiculo) ? $formato->funcionamiento_vehiculo->blis : 'SI'?>">
                                    <div id="blis_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked class="tb1_ck4" name="blis_ck" value="SI" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->blis == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck4" name="blis_ck" value="NO" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->blis == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck4" name="blis_ck" value="NA" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->blis == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Uso de Alerta de Tráfico Cruzado
                                    <input type="hidden" name="alert_trafico_cruzado" id="alert_trafico_cruzado" value="<?php echo isset($formato->funcionamiento_vehiculo) ? $formato->funcionamiento_vehiculo->alert_trafico_cruzado : 'SI'?>">
                                    <div id="alert_trafico_cruzado_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked class="tb1_ck5" name="alert_trafico_cruzado_ck" value="SI" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->alert_trafico_cruzado == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck5" name="alert_trafico_cruzado_ck" value="NO" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->alert_trafico_cruzado == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck5" name="alert_trafico_cruzado_ck" value="NA" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->alert_trafico_cruzado == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Asistencia Activa de Estacionamiento
                                    <input type="hidden" name="asistencia_activa_estaci" id="asistencia_activa_estaci" value="<?php echo isset($formato->funcionamiento_vehiculo) ? $formato->funcionamiento_vehiculo->asistencia_activa_estaci : 'SI'?>">
                                    <div id="asistencia_activa_estaci_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb1_ck6" name="asistencia_activa_estaci_ck" value="SI" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->asistencia_activa_estaci == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck6" name="asistencia_activa_estaci_ck" value="NO" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->asistencia_activa_estaci == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck6" name="asistencia_activa_estaci_ck" value="NA" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->asistencia_activa_estaci == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Cámara de Reversa
                                    <input type="hidden" name="camara_reversa" id="camara_reversa" value="<?php echo isset($formato->funcionamiento_vehiculo) ? $formato->funcionamiento_vehiculo->camara_reversa : 'SI'?>">
                                    <div id="camara_reversa_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb1_ck7" name="camara_reversa_ck" value="SI" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->camara_reversa == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck7" name="camara_reversa_ck" value="NO" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->camara_reversa == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck7" name="camara_reversa_ck" value="NA" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->camara_reversa == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Cámara Frontal de 180 Grados
                                    <input type="hidden" name="camara_frontal" id="camara_frontal" value="<?php echo isset($formato->funcionamiento_vehiculo) ? $formato->funcionamiento_vehiculo->camara_frontal : 'SI'?>">
                                    <div id="camara_frontal_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb1_ck8" name="camara_frontal_ck" value="SI" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->camara_frontal == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck8" name="camara_frontal_ck" value="NO" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->camara_frontal == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck8" name="camara_frontal_ck" value="NA" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->camara_frontal == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Cámara de 360 Grados
                                    <input type="hidden" name="camara_360_grados" id="camara_360_grados" value="<?php echo isset($formato->funcionamiento_vehiculo) ? $formato->funcionamiento_vehiculo->camara_360_grados : 'SI'?>">
                                    <div id="camara_360_grados_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb1_ck9" name="camara_360_grados_ck" value="SI" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->camara_360_grados == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck9" name="camara_360_grados_ck" value="NO" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->camara_360_grados == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck9" name="camara_360_grados_ck" value="NA" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->camara_360_grados == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Sistema de Mon. Presión de Llantas y TMS Individual
                                    <input type="hidden" name="sistema_mon_pll_tms" id="sistema_mon_pll_tms" value="<?php echo isset($formato->funcionamiento_vehiculo) ? $formato->funcionamiento_vehiculo->sistema_mon_pll_tms : 'SI'?>">
                                    <div id="sistema_mon_pll_tms_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb1_ck10" name="sistema_mon_pll_tms_ck" value="SI" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->sistema_mon_pll_tms == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck10" name="sistema_mon_pll_tms_ck" value="NO" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->sistema_mon_pll_tms == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck10" name="sistema_mon_pll_tms_ck" value="NA" <?php if(isset($formato->funcionamiento_vehiculo)) {if( $formato->funcionamiento_vehiculo->sistema_mon_pll_tms == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                        </table>

                        <br>
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <td colspan="4" style="font-weight: bold;">
                                    SEGURIDAD
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    A. SEGURIDAD ACTIVA
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    SI
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NO
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NA
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Sistema de Frenado
                                    <input type="hidden" name="sistema_frenado" id="sistema_frenado" value="<?php echo isset($formato->seguridad) ? $formato->seguridad->sistema_frenado : 'SI'?>">
                                    <div id="sistema_frenado_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb2_ck1" name="sistema_frenado_ck" value="SI" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->sistema_frenado == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck1" name="sistema_frenado_ck" value="NO" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->sistema_frenado == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck1" name="sistema_frenado_ck" value="NA" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->sistema_frenado == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Control de Tracción
                                    <input type="hidden" name="control_traccion" id="control_traccion" value="<?php echo isset($formato->seguridad) ? $formato->seguridad->control_traccion : 'SI'?>">
                                    <div id="control_traccion_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb2_ck2" name="control_traccion_ck" value="SI" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->control_traccion == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck2" name="control_traccion_ck" value="NO" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->control_traccion == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck2" name="control_traccion_ck" value="NA" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->control_traccion == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Sistema de Estabilidad Electrónico (ESC)
                                    <input type="hidden" name="esc" id="esc" value="<?php echo isset($formato->seguridad) ? $formato->seguridad->esc : 'SI'?>">
                                    <div id="esc_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb2_ck3" name="esc_ck" value="SI" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->esc == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck3" name="esc_ck" value="NO" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->esc == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck3" name="esc_ck" value="NA" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->esc == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Sistema Anti Volcaduras (RSC)
                                    <input type="hidden" name="rsc" id="rsc" value="<?php echo isset($formato->seguridad) ? $formato->seguridad->rsc : 'SI'?>">
                                    <div id="rsc_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb2_ck4" name="rsc_ck" value="SI" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->rsc == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck4" name="rsc_ck" value="NO" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->rsc == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck4" name="rsc_ck" value="NA" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->rsc == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Control de Torque en Curvas
                                    <input type="hidden" name="control_torque_curvas" id="control_torque_curvas" value="<?php echo isset($formato->seguridad) ? $formato->seguridad->control_torque_curvas : 'SI'?>">
                                    <div id="control_torque_curvas_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb2_ck5" name="control_torque_curvas_ck" value="SI" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->control_torque_curvas == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck5" name="control_torque_curvas_ck" value="NO" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->control_torque_curvas == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck5" name="control_torque_curvas_ck" value="NA" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->control_torque_curvas == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Control de Curvas
                                    <input type="hidden" name="control_curvas" id="control_curvas" value="<?php echo isset($formato->seguridad) ? $formato->seguridad->control_curvas : 'SI'?>">
                                    <div id="control_curvas_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb2_ck6" name="control_curvas_ck" value="SI" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->control_curvas == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck6" name="control_curvas_ck" value="NO" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->control_curvas == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck6" name="control_curvas_ck" value="NA" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->control_curvas == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Control de Balancel de Remolque
                                    <input type="hidden" name="control_balance_remolque" id="control_balance_remolque" value="<?php echo isset($formato->seguridad) ? $formato->seguridad->control_balance_remolque : 'SI'?>">
                                    <div id="control_balance_remolque_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb2_ck7" name="control_balance_remolque_ck" value="SI" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->control_balance_remolque == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck7" name="control_balance_remolque_ck" value="NO" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->control_balance_remolque == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck7" name="control_balance_remolque_ck" value="NA" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->control_balance_remolque == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Frenos electrónicos para remolque
                                    <input type="hidden" name="frenos_electronicos" id="frenos_electronicos" value="<?php echo isset($formato->seguridad) ? $formato->seguridad->frenos_electronicos : 'SI'?>">
                                    <div id="frenos_electronicos_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb2_ck8" name="frenos_electronicos_ck" value="SI" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->frenos_electronicos == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck8" name="frenos_electronicos_ck" value="NO" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->frenos_electronicos == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck8" name="frenos_electronicos_ck" value="NA" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->frenos_electronicos == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Control Electrónico Descenso
                                    <input type="hidden" name="control_electronico_des" id="control_electronico_des" value="<?php echo isset($formato->seguridad) ? $formato->seguridad->control_electronico_des : 'SI'?>">
                                    <div id="control_electronico_des_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb2_ck9" name="control_electronico_des_ck" value="SI" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->control_electronico_des == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck9" name="control_electronico_des_ck" value="NO" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->control_electronico_des == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck9" name="control_electronico_des_ck" value="NA" <?php if(isset($formato->seguridad)) {if( $formato->seguridad->control_electronico_des == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                        </table>

                        <br>
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <td style="width: 70%;">
                                    B. SEGURIDAD PASIVA
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    SI
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NO
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NA
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Cinturones de Seguridad
                                    <input type="hidden" name="cinturon_seguridad" id="cinturon_seguridad" value="<?php echo isset($formato->seguridad_pasiva) ? $formato->seguridad_pasiva->cinturon_seguridad : 'SI'?>">
                                    <div id="cinturon_seguridad_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);"  checked class="tb3_ck1" name="cinturon_seguridad_ck" value="SI" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->cinturon_seguridad == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck1" name="cinturon_seguridad_ck" value="NO" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->cinturon_seguridad == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck1" name="cinturon_seguridad_ck" value="NA" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->cinturon_seguridad == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Bolsas de Aire
                                    <input type="hidden" name="bolsas_aire" id="bolsas_aire" value="<?php echo isset($formato->seguridad_pasiva) ? $formato->seguridad_pasiva->bolsas_aire : 'SI'?>">
                                    <div id="bolsas_aire_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb3_ck2" name="bolsas_aire_ck" value="SI" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->bolsas_aire == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck2" name="bolsas_aire_ck" value="NO" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->bolsas_aire == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck2" name="bolsas_aire_ck" value="NA" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->bolsas_aire == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Chasis y Carrocería
                                    <input type="hidden" name="chasis_carroceria" id="chasis_carroceria" value="<?php echo isset($formato->seguridad_pasiva) ? $formato->seguridad_pasiva->chasis_carroceria : 'SI'?>">
                                    <div id="chasis_carroceria_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);"  checked class="tb3_ck3" name="chasis_carroceria_ck" value="SI" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->chasis_carroceria == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck3" name="chasis_carroceria_ck" value="NO" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->chasis_carroceria == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck3" name="chasis_carroceria_ck" value="NA" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->chasis_carroceria == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Carrosería hidroforma
                                    <input type="hidden" name="carroseria_hidroforma" id="carroseria_hidroforma" value="<?php echo isset($formato->seguridad_pasiva) ? $formato->seguridad_pasiva->carroseria_hidroforma : 'SI'?>">
                                    <div id="carroseria_hidroforma_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);"  checked class="tb3_ck4" name="carroseria_hidroforma_ck" value="SI" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->carroseria_hidroforma == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck4" name="carroseria_hidroforma_ck" value="NO" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->carroseria_hidroforma == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck4" name="carroseria_hidroforma_ck" value="NA" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->carroseria_hidroforma == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Cristales
                                    <input type="hidden" name="cristales" id="cristales" value="<?php echo isset($formato->seguridad_pasiva) ? $formato->seguridad_pasiva->cristales : 'SI'?>">
                                    <div id="cristales_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb3_ck5" name="cristales_ck" value="SI" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->cristales == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck5" name="cristales_ck" value="NO" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->cristales == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck5" name="cristales_ck" value="NA" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->cristales == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Cabeceras
                                    <input type="hidden" name="cabeceras" id="cabeceras" value="<?php echo isset($formato->seguridad_pasiva) ? $formato->seguridad_pasiva->cabeceras : 'SI'?>">
                                    <div id="cabeceras_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb3_ck6" name="cabeceras_ck" value="SI" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->cabeceras == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck6" name="cabeceras_ck" value="NO" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->cabeceras == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck6" name="cabeceras_ck" value="NA" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->cabeceras == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Sistema de Alerta SOS
                                    <input type="hidden" name="sistema_alerta" id="sistema_alerta" value="<?php echo isset($formato->seguridad_pasiva) ? $formato->seguridad_pasiva->sistema_alerta : 'SI'?>">
                                    <div id="sistema_alerta_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb3_ck7" name="sistema_alerta_ck" value="SI" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->sistema_alerta == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck7" name="sistema_alerta_ck" value="NO" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->sistema_alerta == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck7" name="sistema_alerta_ck" value="NA" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->sistema_alerta == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Teclado de puerta SecuriCode
                                    <input type="hidden" name="teclado_puerta" id="teclado_puerta" value="<?php echo isset($formato->seguridad_pasiva) ? $formato->seguridad_pasiva->teclado_puerta : 'SI'?>">
                                    <div id="teclado_puerta_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb3_ck8" name="teclado_puerta_ck" value="SI" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->teclado_puerta == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck8" name="teclado_puerta_ck" value="NO" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->teclado_puerta == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck8" name="teclado_puerta_ck" value="NA" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->teclado_puerta == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Alarma Volumétrica (o sensor sónico)
                                    <input type="hidden" name="alarma_volumetrica" id="alarma_volumetrica" value="<?php echo isset($formato->seguridad_pasiva) ? $formato->seguridad_pasiva->alarma_volumetrica : 'SI'?>">
                                    <div id="alarma_volumetrica_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb3_ck9" name="alarma_volumetrica_ck" value="SI" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->alarma_volumetrica == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck9" name="alarma_volumetrica_ck" value="NO" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->alarma_volumetrica == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck9" name="alarma_volumetrica_ck" value="NA" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->alarma_volumetrica == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Alarma Perimetral
                                    <input type="hidden" name="alarma_perimetral" id="alarma_perimetral" value="<?php echo isset($formato->seguridad_pasiva) ? $formato->seguridad_pasiva->alarma_perimetral : 'SI'?>">
                                    <div id="alarma_perimetral_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb3_ck10" name="alarma_perimetral_ck" value="SI" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->alarma_perimetral == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck10" name="alarma_perimetral_ck" value="NO" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->alarma_perimetral == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck10" name="alarma_perimetral_ck" value="NA" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->alarma_perimetral == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    MyKey
                                    <input type="hidden" name="mikey" id="mikey" value="<?php echo isset($formato->seguridad_pasiva) ? $formato->seguridad_pasiva->mikey : 'SI'?>">
                                    <div id="mikey_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb3_ck11" name="mikey_ck" value="SI" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->mikey == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck11" name="mikey_ck" value="NO" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->mikey == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck11" name="mikey_ck" value="NA" <?php if(isset($formato->seguridad_pasiva)) {if( $formato->seguridad_pasiva->mikey == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                        </table>

                        <br>
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <td colspan="4" style="font-weight: bold;">
                                    CONFORT
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    A. ASIENTOS
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    SI
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NO
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NA
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Accionamiento Manual
                                    <input type="hidden" name="accionamiento_manual" id="accionamiento_manual" value="<?php echo isset($formato->confort) ? $formato->confort->accionamiento_manual : 'SI'?>">
                                    <div id="accionamiento_manual_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb4_ck1" name="accionamiento_manual_ck" value="SI" <?php if(isset($formato->confort)) {if( $formato->confort->accionamiento_manual == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb4_ck1" name="accionamiento_manual_ck" value="NO" <?php if(isset($formato->confort)) {if( $formato->confort->accionamiento_manual == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb4_ck1" name="accionamiento_manual_ck" value="NA" <?php if(isset($formato->confort)) {if( $formato->confort->accionamiento_manual == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Accionamiento Eléctrico
                                    <input type="hidden" name="accionamiento_electrico" id="accionamiento_electrico" value="<?php echo isset($formato->confort) ? $formato->confort->accionamiento_electrico : 'SI'?>">
                                    <div id="accionamiento_electrico_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb4_ck2" name="accionamiento_electrico_ck"  value="SI" <?php if(isset($formato->confort)) {if( $formato->confort->accionamiento_electrico == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb4_ck2" name="accionamiento_electrico_ck"  value="NO" <?php if(isset($formato->confort)) {if( $formato->confort->accionamiento_electrico == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb4_ck2" name="accionamiento_electrico_ck"  value="NA" <?php if(isset($formato->confort)) {if( $formato->confort->accionamiento_electrico == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Asientos Delanteros Multicontour con Masaje
                                    <input type="hidden" name="asiento_delantero_asaje" id="asiento_delantero_asaje" value="<?php echo isset($formato->confort) ? $formato->confort->asiento_delantero_asaje : 'SI'?>">
                                    <div id="asiento_delantero_asaje_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb4_ck3" name="asiento_delantero_asaje_ck" value="SI" <?php if(isset($formato->confort)) {if( $formato->confort->asiento_delantero_asaje == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb4_ck3" name="asiento_delantero_asaje_ck" value="NO" <?php if(isset($formato->confort)) {if( $formato->confort->asiento_delantero_asaje == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb4_ck3" name="asiento_delantero_asaje_ck" value="NA" <?php if(isset($formato->confort)) {if( $formato->confort->asiento_delantero_asaje == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Asientos con Calefacción y Enfriamiento
                                    <input type="hidden" name="asiento_calefaccion" id="asiento_calefaccion" value="<?php echo isset($formato->confort) ? $formato->confort->asiento_calefaccion : 'SI'?>">
                                    <div id="asiento_calefaccion_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb4_ck4" name="asiento_calefaccion_ck" value="SI" <?php if(isset($formato->confort)) {if( $formato->confort->asiento_calefaccion == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb4_ck4" name="asiento_calefaccion_ck" value="NO" <?php if(isset($formato->confort)) {if( $formato->confort->asiento_calefaccion == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb4_ck4" name="asiento_calefaccion_ck" value="NA" <?php if(isset($formato->confort)) {if( $formato->confort->asiento_calefaccion == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Easy Fold / Power Fold
                                    <input type="hidden" name="easy_power_fold" id="easy_power_fold" value="<?php echo isset($formato->confort) ? $formato->confort->easy_power_fold : 'SI'?>">
                                    <div id="easy_power_fold_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb4_ck5" name="easy_power_fold_ck" value="SI" <?php if(isset($formato->confort)) {if( $formato->confort->easy_power_fold == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb4_ck5" name="easy_power_fold_ck" value="NO" <?php if(isset($formato->confort)) {if( $formato->confort->easy_power_fold == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb4_ck5" name="easy_power_fold_ck" value="NA" <?php if(isset($formato->confort)) {if( $formato->confort->easy_power_fold == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Memoria de Asientos
                                    <input type="hidden" name="memoria_asientos" id="memoria_asientos" value="<?php echo isset($formato->confort) ? $formato->confort->memoria_asientos : 'SI'?>">
                                    <div id="memoria_asientos_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb4_ck6" name="memoria_asientos_ck" value="SI" <?php if(isset($formato->confort)) {if( $formato->confort->memoria_asientos == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb4_ck6" name="memoria_asientos_ck" value="NO" <?php if(isset($formato->confort)) {if( $formato->confort->memoria_asientos == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb4_ck6" name="memoria_asientos_ck" value="NA" <?php if(isset($formato->confort)) {if( $formato->confort->memoria_asientos == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                        </table>

                        <br>
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <td style="width: 70%;">
                                    B. ILUMINACIÓN
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    SI
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NO
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NA
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Iluminación Ambiental
                                    <input type="hidden" name="iluminacion_ambiental" id="iluminacion_ambiental" value="<?php echo isset($formato->iluminacion) ? $formato->iluminacion->iluminacion_ambiental : 'SI'?>">
                                    <div id="iluminacion_ambiental_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb5_ck1" name="iluminacion_ambiental_ck" value="SI" <?php if(isset($formato->iluminacion)) {if( $formato->iluminacion->iluminacion_ambiental == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck1" name="iluminacion_ambiental_ck" value="NO" <?php if(isset($formato->iluminacion)) {if( $formato->iluminacion->iluminacion_ambiental == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck1" name="iluminacion_ambiental_ck" value="NA" <?php if(isset($formato->iluminacion)) {if( $formato->iluminacion->iluminacion_ambiental == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Sistema de Iluminación Ambiental (MyColor)
                                    <input type="hidden" name="sistema_iluminacion" id="sistema_iluminacion" value="<?php echo isset($formato->iluminacion) ? $formato->iluminacion->sistema_iluminacion : 'SI'?>">
                                    <div id="sistema_iluminacion_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb5_ck2" name="sistema_iluminacion_ck" value="SI" <?php if(isset($formato->iluminacion)) {if( $formato->iluminacion->sistema_iluminacion == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck2" name="sistema_iluminacion_ck" value="NO" <?php if(isset($formato->iluminacion)) {if( $formato->iluminacion->sistema_iluminacion == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck2" name="sistema_iluminacion_ck" value="NA" <?php if(isset($formato->iluminacion)) {if( $formato->iluminacion->sistema_iluminacion == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Faros Bi-Xenón Adaptativos
                                    <input type="hidden" name="faros_bixenon" id="faros_bixenon" value="<?php echo isset($formato->iluminacion) ? $formato->iluminacion->faros_bixenon : 'SI'?>">
                                    <div id="faros_bixenon_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb5_ck3" name="faros_bixenon_ck" value="SI" <?php if(isset($formato->iluminacion)) {if( $formato->iluminacion->faros_bixenon == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck3" name="faros_bixenon_ck" value="NO" <?php if(isset($formato->iluminacion)) {if( $formato->iluminacion->faros_bixenon == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck3" name="faros_bixenon_ck" value="NA" <?php if(isset($formato->iluminacion)) {if( $formato->iluminacion->faros_bixenon == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    HID
                                    <input type="hidden" name="hid" id="hid" value="<?php echo isset($formato->iluminacion) ? $formato->iluminacion->hid : 'SI'?>">
                                    <div id="hid_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb5_ck4" name="hid_ck" value="SI" <?php if(isset($formato->iluminacion)) {if( $formato->iluminacion->hid == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck4" name="hid_ck" value="NO" <?php if(isset($formato->iluminacion)) {if( $formato->iluminacion->hid == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck4" name="hid_ck" value="NA" <?php if(isset($formato->iluminacion)) {if( $formato->iluminacion->hid == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    LED
                                    <input type="hidden" name="led" id="led" value="<?php echo isset($formato->iluminacion) ? $formato->iluminacion->led : 'SI'?>">
                                    <div id="led_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb5_ck5" name="led_ck" value="SI" <?php if(isset($formato->iluminacion)) {if( $formato->iluminacion->led == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck5" name="led_ck" value="NO" <?php if(isset($formato->iluminacion)) {if( $formato->iluminacion->led == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck5" name="led_ck" value="NA" <?php if(isset($formato->iluminacion)) {if( $formato->iluminacion->led == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                        </table>

                        <br>
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <td style="width: 70%;">
                                    C. ESPECIALES
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    SI
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NO
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NA
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Ingreso y arranque sin llaves PEPS
                                    <input type="hidden" name="peps" id="peps" value="<?php echo isset($formato->especiales) ? $formato->especiales->peps : 'SI'?>">
                                    <div id="peps_error" class="validation_error"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb6_ck1" name="peps_ck" value="SI" <?php if(isset($formato->especiales)) {if( $formato->especiales->peps == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck1" name="peps_ck" value="NO" <?php if(isset($formato->especiales)) {if( $formato->especiales->peps == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck1" name="peps_ck" value="NA" <?php if(isset($formato->especiales)) {if( $formato->especiales->peps == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Apertura de Cajuela Manos Libres
                                    <input type="hidden" name="apertura_cajuela" id="apertura_cajuela" value="<?php echo isset($formato->especiales) ? $formato->especiales->apertura_cajuela : 'SI'?>">
                                    <div id="apertura_cajuela_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked class="tb6_ck2" name="apertura_cajuela_ck" value="SI" <?php if(isset($formato->especiales)) {if( $formato->especiales->apertura_cajuela == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck2" name="apertura_cajuela_ck" value="NO" <?php if(isset($formato->especiales)) {if( $formato->especiales->apertura_cajuela == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck2" name="apertura_cajuela_ck" value="NA" <?php if(isset($formato->especiales)) {if( $formato->especiales->apertura_cajuela == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Encendido Remoto
                                    <input type="hidden" name="encendido_remoto" id="encendido_remoto" value="<?php echo isset($formato->especiales) ? $formato->especiales->encendido_remoto : 'SI'?>">
                                    <div id="encendido_remoto_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb6_ck3" name="encendido_remoto_ck" value="SI" <?php if(isset($formato->especiales)) {if( $formato->especiales->encendido_remoto == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck3" name="encendido_remoto_ck" value="NO" <?php if(isset($formato->especiales)) {if( $formato->especiales->encendido_remoto == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck3" name="encendido_remoto_ck" value="NA" <?php if(isset($formato->especiales)) {if( $formato->especiales->encendido_remoto == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Techo Panorámico
                                    <input type="hidden" name="techo_panoramico" id="techo_panoramico" value="<?php echo isset($formato->especiales) ? $formato->especiales->techo_panoramico : 'SI'?>">
                                    <div id="techo_panoramico_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb6_ck4" name="techo_panoramico_ck" value="SI" <?php if(isset($formato->especiales)) {if( $formato->especiales->techo_panoramico == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck4" name="techo_panoramico_ck" value="NO" <?php if(isset($formato->especiales)) {if( $formato->especiales->techo_panoramico == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck4" name="techo_panoramico_ck" value="NA" <?php if(isset($formato->especiales)) {if( $formato->especiales->techo_panoramico == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Aire Acondicionado
                                    <input type="hidden" name="aire_acondicionado" id="aire_acondicionado" value="<?php echo isset($formato->especiales) ? $formato->especiales->aire_acondicionado : 'SI'?>">
                                    <div id="aire_acondicionado_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb6_ck5" name="aire_acondicionado_ck" value="SI" <?php if(isset($formato->especiales)) {if( $formato->especiales->aire_acondicionado == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck5" name="aire_acondicionado_ck" value="NO" <?php if(isset($formato->especiales)) {if( $formato->especiales->aire_acondicionado == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck5" name="aire_acondicionado_ck" value="NA" <?php if(isset($formato->especiales)) {if( $formato->especiales->aire_acondicionado == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Control Automático Electrónico de Temperatura (EATC)
                                    <input type="hidden" name="eatc" id="eatc" value="<?php echo isset($formato->especiales) ? $formato->especiales->eatc : 'SI'?>">
                                    <div id="eatc_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb6_ck6" name="eatc_ck" value="SI" <?php if(isset($formato->especiales)) {if( $formato->especiales->eatc == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck6" name="eatc_ck" value="NO" <?php if(isset($formato->especiales)) {if( $formato->especiales->eatc == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck6" name="eatc_ck" value="NA" <?php if(isset($formato->especiales)) {if( $formato->especiales->eatc == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Consola con Enfriamiento
                                    <input type="hidden" name="consola_enfriamiento" id="consola_enfriamiento" value="<?php echo isset($formato->especiales) ? $formato->especiales->consola_enfriamiento : 'SI'?>">
                                    <div id="consola_enfriamiento_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb6_ck7" name="consola_enfriamiento_ck" value="SI" <?php if(isset($formato->especiales)) {if( $formato->especiales->consola_enfriamiento == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck7" name="consola_enfriamiento_ck" value="NO" <?php if(isset($formato->especiales)) {if( $formato->especiales->consola_enfriamiento == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck7" name="consola_enfriamiento_ck" value="NA" <?php if(isset($formato->especiales)) {if( $formato->especiales->consola_enfriamiento == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Volante con Ajuste de Altura y Pronfundidad
                                    <input type="hidden" name="volante_ajuste_altura" id="volante_ajuste_altura" value="<?php echo isset($formato->especiales) ? $formato->especiales->volante_ajuste_altura : 'SI'?>">
                                    <div id="volante_ajuste_altura_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb6_ck8" name="volante_ajuste_altura_ck" value="SI" <?php if(isset($formato->especiales)) {if( $formato->especiales->volante_ajuste_altura == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck8" name="volante_ajuste_altura_ck" value="NO" <?php if(isset($formato->especiales)) {if( $formato->especiales->volante_ajuste_altura == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck8" name="volante_ajuste_altura_ck" value="NA" <?php if(isset($formato->especiales)) {if( $formato->especiales->volante_ajuste_altura == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Volante Calefactado
                                    <input type="hidden" name="volante_calefactado" id="volante_calefactado" value="<?php echo isset($formato->especiales) ? $formato->especiales->volante_calefactado : 'SI'?>">
                                    <div id="volante_calefactado_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb6_ck9" name="volante_calefactado_ck" value="SI" <?php if(isset($formato->especiales)) {if( $formato->especiales->volante_calefactado == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck9" name="volante_calefactado_ck" value="NO" <?php if(isset($formato->especiales)) {if( $formato->especiales->volante_calefactado == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck9" name="volante_calefactado_ck" value="NA" <?php if(isset($formato->especiales)) {if( $formato->especiales->volante_calefactado == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Estribos Eléctricos
                                    <input type="hidden" name="estribos_electricos" id="estribos_electricos" value="<?php echo isset($formato->especiales) ? $formato->especiales->estribos_electricos : 'SI'?>">
                                    <div id="estribos_electricos_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb6_ck10" name="estribos_electricos_ck" value="SI" <?php if(isset($formato->especiales)) {if( $formato->especiales->estribos_electricos == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck10" name="estribos_electricos_ck" value="NO" <?php if(isset($formato->especiales)) {if( $formato->especiales->estribos_electricos == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck10" name="estribos_electricos_ck" value="NA" <?php if(isset($formato->especiales)) {if( $formato->especiales->estribos_electricos == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Apertura de Caja de Carga (Remote Tailgate Release)
                                    <input type="hidden" name="apertura_caja_carga" id="apertura_caja_carga" value="<?php echo isset($formato->especiales) ? $formato->especiales->apertura_caja_carga : 'SI'?>">
                                    <div id="apertura_caja_carga_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb6_ck11" name="apertura_caja_carga_ck" value="SI" <?php if(isset($formato->especiales)) {if( $formato->especiales->apertura_caja_carga == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck11" name="apertura_caja_carga_ck" value="NO" <?php if(isset($formato->especiales)) {if( $formato->especiales->apertura_caja_carga == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck11" name="apertura_caja_carga_ck" value="NA" <?php if(isset($formato->especiales)) {if( $formato->especiales->apertura_caja_carga == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Limpiaparabrisas con Sensor de Lluvia
                                    <input type="hidden" name="limpiaparabrisas" id="limpiaparabrisas" value="<?php echo isset($formato->especiales) ? $formato->especiales->limpiaparabrisas : 'SI'?>">
                                    <div id="limpiaparabrisas_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb6_ck12" name="limpiaparabrisas_ck" value="SI" <?php if(isset($formato->especiales)) {if( $formato->especiales->limpiaparabrisas == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck12" name="limpiaparabrisas_ck" value="NO" <?php if(isset($formato->especiales)) {if( $formato->especiales->limpiaparabrisas == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck12" name="limpiaparabrisas_ck" value="NA" <?php if(isset($formato->especiales)) {if( $formato->especiales->limpiaparabrisas == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Espejos Eléctricos con Calefacción
                                    <input type="hidden" name="espejo_electrico" id="espejo_electrico" value="<?php echo isset($formato->especiales) ? $formato->especiales->espejo_electrico : 'SI'?>">
                                    <div id="espejo_electrico_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb6_ck13" name="espejo_electrico_ck" value="SI" <?php if(isset($formato->especiales)) {if( $formato->especiales->espejo_electrico == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck13" name="espejo_electrico_ck" value="NO" <?php if(isset($formato->especiales)) {if( $formato->especiales->espejo_electrico == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck13" name="espejo_electrico_ck" value="NA" <?php if(isset($formato->especiales)) {if( $formato->especiales->espejo_electrico == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Espejos Abatibles Eléctricamente
                                    <input type="hidden" name="espejo_abatible" id="espejo_abatible" value="<?php echo isset($formato->especiales) ? $formato->especiales->espejo_abatible : 'SI'?>">
                                    <div id="espejo_abatible_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb6_ck14" name="espejo_abatible_ck" value="SI" <?php if(isset($formato->especiales)) {if( $formato->especiales->espejo_abatible == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck14" name="espejo_abatible_ck" value="NO" <?php if(isset($formato->especiales)) {if( $formato->especiales->espejo_abatible == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb6_ck14" name="espejo_abatible_ck" value="NA" <?php if(isset($formato->especiales)) {if( $formato->especiales->espejo_abatible == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="col-md-6 table-responsive">
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <td style="width: 70%;">
                                    <br>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    SI
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NO
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NA
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Conectividad, Sistemas de Comunicación y Entretenimiento
                                    <input type="hidden" name="conectividad" id="conectividad" value="<?php echo isset($formato->extras) ? $formato->extras->conectividad : 'SI'?>">
                                    <div id="conectividad_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb7_ck1" name="conectividad_ck" value="SI" <?php if(isset($formato->extras)) {if( $formato->extras->conectividad == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck1" name="conectividad_ck" value="NO" <?php if(isset($formato->extras)) {if( $formato->extras->conectividad == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck1" name="conectividad_ck" value="NA" <?php if(isset($formato->extras)) {if( $formato->extras->conectividad == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    SYNC
                                    <input type="hidden" name="sync" id="sync" value="<?php echo isset($formato->extras) ? $formato->extras->sync : 'SI'?>">
                                    <div id="sync_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb7_ck2" name="sync_ck" value="SI" <?php if(isset($formato->extras)) {if( $formato->extras->sync == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck2" name="sync_ck" value="NO" <?php if(isset($formato->extras)) {if( $formato->extras->sync == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck2" name="sync_ck" value="NA" <?php if(isset($formato->extras)) {if( $formato->extras->sync == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    MyFord Touch
                                    <input type="hidden" name="myFord_touch" id="myFord_touch" value="<?php echo isset($formato->extras) ? $formato->extras->myFord_touch : 'SI'?>">
                                    <div id="myFord_touch_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb7_ck3" name="myFord_touch_ck" value="SI" <?php if(isset($formato->extras)) {if( $formato->extras->myFord_touch == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck3" name="myFord_touch_ck" value="NO" <?php if(isset($formato->extras)) {if( $formato->extras->myFord_touch == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck3" name="myFord_touch_ck" value="NA" <?php if(isset($formato->extras)) {if( $formato->extras->myFord_touch == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    MyFord Touch con Navegación
                                    <input type="hidden" name="myFord_touch_navigation" id="myFord_touch_navigation" value="<?php echo isset($formato->extras) ? $formato->extras->myFord_touch_navigation : 'SI'?>">
                                    <div id="myFord_touch_navigation_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb7_ck4" name="myFord_touch_navigation_ck" value="SI" <?php if(isset($formato->extras)) {if( $formato->extras->myFord_touch_navigation == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck4" name="myFord_touch_navigation_ck" value="NO" <?php if(isset($formato->extras)) {if( $formato->extras->myFord_touch_navigation == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck4" name="myFord_touch_navigation_ck" value="NA" <?php if(isset($formato->extras)) {if( $formato->extras->myFord_touch_navigation == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    SYNC 3 / FordPass
                                    <input type="hidden" name="sync_fordpass" id="sync_fordpass" value="<?php echo isset($formato->extras) ? $formato->extras->sync_fordpass : 'SI'?>">
                                    <div id="sync_fordpass_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb7_ck5" name="sync_fordpass_ck" value="SI" <?php if(isset($formato->extras)) {if( $formato->extras->sync_fordpass == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck5" name="sync_fordpass_ck" value="NO" <?php if(isset($formato->extras)) {if( $formato->extras->sync_fordpass == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck5" name="sync_fordpass_ck" value="NA" <?php if(isset($formato->extras)) {if( $formato->extras->sync_fordpass == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Apple CarPlay
                                    <input type="hidden" name="apple_carplay" id="apple_carplay" value="<?php echo isset($formato->extras) ? $formato->extras->apple_carplay : 'SI'?>">
                                    <div id="apple_carplay_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb7_ck6" name="apple_carplay_ck" value="SI" <?php if(isset($formato->extras)) {if( $formato->extras->apple_carplay == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck6" name="apple_carplay_ck" value="NO" <?php if(isset($formato->extras)) {if( $formato->extras->apple_carplay == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck6" name="apple_carplay_ck" value="NA" <?php if(isset($formato->extras)) {if( $formato->extras->apple_carplay == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Android Auto
                                    <input type="hidden" name="android_auto" id="android_auto" value="<?php echo isset($formato->extras) ? $formato->extras->android_auto : 'SI'?>">
                                    <div id="android_auto_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb7_ck7" name="android_auto_ck" value="SI" <?php if(isset($formato->extras)) {if( $formato->extras->android_auto == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck7" name="android_auto_ck" value="NO" <?php if(isset($formato->extras)) {if( $formato->extras->android_auto == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck7" name="android_auto_ck" value="NA" <?php if(isset($formato->extras)) {if( $formato->extras->android_auto == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Consola Smastphone
                                    <input type="hidden" name="consola_smartphone" id="consola_smartphone" value="<?php echo isset($formato->extras) ? $formato->extras->consola_smartphone : 'SI'?>">
                                    <div id="consola_smartphone_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb7_ck8" name="consola_smartphone_ck" value="SI" <?php if(isset($formato->extras)) {if( $formato->extras->consola_smartphone == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck8" name="consola_smartphone_ck" value="NO" <?php if(isset($formato->extras)) {if( $formato->extras->consola_smartphone == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck8" name="consola_smartphone_ck" value="NA" <?php if(isset($formato->extras)) {if( $formato->extras->consola_smartphone == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Audio SHAKER PRO
                                    <input type="hidden" name="audio_shaker_pro" id="audio_shaker_pro" value="<?php echo isset($formato->extras) ? $formato->extras->audio_shaker_pro : 'SI'?>">
                                    <div id="audio_shaker_pro_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb7_ck9" name="audio_shaker_pro_ck" value="SI" <?php if(isset($formato->extras)) {if( $formato->extras->audio_shaker_pro == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck9" name="audio_shaker_pro_ck" value="NO" <?php if(isset($formato->extras)) {if( $formato->extras->audio_shaker_pro == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck9" name="audio_shaker_pro_ck" value="NA" <?php if(isset($formato->extras)) {if( $formato->extras->audio_shaker_pro == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Radio HD
                                    <input type="hidden" name="radio_hd" id="radio_hd" value="<?php echo isset($formato->extras) ? $formato->extras->radio_hd : 'SI'?>">
                                    <div id="radio_hd_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb7_ck10" name="radio_hd_ck" value="SI" <?php if(isset($formato->extras)) {if( $formato->extras->radio_hd == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck10" name="radio_hd_ck" value="NO" <?php if(isset($formato->extras)) {if( $formato->extras->radio_hd == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck10" name="radio_hd_ck" value="NA" <?php if(isset($formato->extras)) {if( $formato->extras->radio_hd == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Equipo Sony
                                    <input type="hidden" name="equipo_sony" id="equipo_sony" value="<?php echo isset($formato->extras) ? $formato->extras->equipo_sony : 'SI'?>">
                                    <div id="equipo_sony_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb7_ck11" name="equipo_sony_ck" value="SI" <?php if(isset($formato->extras)) {if( $formato->extras->equipo_sony == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck11" name="equipo_sony_ck" value="NO" <?php if(isset($formato->extras)) {if( $formato->extras->equipo_sony == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck11" name="equipo_sony_ck" value="NA" <?php if(isset($formato->extras)) {if( $formato->extras->equipo_sony == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Puertos USB de carga inteligente
                                    <input type="hidden" name="puertos_usb" id="puertos_usb" value="<?php echo isset($formato->extras) ? $formato->extras->puertos_usb : 'SI'?>">
                                    <div id="puertos_usb_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb7_ck12" name="puertos_usb_ck" value="SI" <?php if(isset($formato->extras)) {if( $formato->extras->puertos_usb == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck12" name="puertos_usb_ck" value="NO" <?php if(isset($formato->extras)) {if( $formato->extras->puertos_usb == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck12" name="puertos_usb_ck" value="NA" <?php if(isset($formato->extras)) {if( $formato->extras->puertos_usb == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Bluetooth
                                    <input type="hidden" name="bluetooh" id="bluetooh" value="<?php echo isset($formato->extras) ? $formato->extras->bluetooh : 'SI'?>">
                                    <div id="bluetooh_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb7_ck13" name="bluetooh_ck" value="SI" <?php if(isset($formato->extras)) {if( $formato->extras->bluetooh == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck13" name="bluetooh_ck" value="NO" <?php if(isset($formato->extras)) {if( $formato->extras->bluetooh == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck13" name="bluetooh_ck" value="NA" <?php if(isset($formato->extras)) {if( $formato->extras->bluetooh == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    WIFI
                                    <input type="hidden" name="wifi" id="wifi" value="<?php echo isset($formato->extras) ? $formato->extras->wifi : 'SI'?>">
                                    <div id="wifi_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb7_ck14" name="wifi_ck" value="SI" <?php if(isset($formato->extras)) {if( $formato->extras->wifi == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck14" name="wifi_ck" value="NO" <?php if(isset($formato->extras)) {if( $formato->extras->wifi == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck14" name="wifi_ck" value="NA" <?php if(isset($formato->extras)) {if( $formato->extras->wifi == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Tarjeta SD
                                    <input type="hidden" name="tarjeta_sd" id="tarjeta_sd" value="<?php echo isset($formato->extras) ? $formato->extras->tarjeta_sd : 'SI'?>">
                                    <div id="tarjeta_sd_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tarjeta_sd" name="tarjeta_sd_ck" value="SI" <?php if(isset($formato->extras)) {if( $formato->extras->tarjeta_sd == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tarjeta_sd" name="tarjeta_sd_ck" value="NO" <?php if(isset($formato->extras)) {if( $formato->extras->tarjeta_sd == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tarjeta_sd" name="tarjeta_sd_ck" value="NA" <?php if(isset($formato->extras)) {if( $formato->extras->tarjeta_sd == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Inversor de Corriente de 110v
                                    <input type="hidden" name="inversor_corriente" id="inversor_corriente" value="<?php echo isset($formato->extras) ? $formato->extras->inversor_corriente : 'SI'?>">
                                    <div id="inversor_corriente_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb7_ck16" name="inversor_corriente_ck" value="SI" <?php if(isset($formato->extras)) {if( $formato->extras->inversor_corriente == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck16" name="inversor_corriente_ck" value="NO" <?php if(isset($formato->extras)) {if( $formato->extras->inversor_corriente == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck16" name="inversor_corriente_ck" value="NA" <?php if(isset($formato->extras)) {if( $formato->extras->inversor_corriente == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Espejo electrocrómatico
                                    <input type="hidden" name="espejo_electrocromatico" id="espejo_electrocromatico" value="<?php echo isset($formato->extras) ? $formato->extras->espejo_electrocromatico : 'SI'?>">
                                    <div id="espejo_electrocromatico_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb7_ck17" name="espejo_electrocromatico_ck" value="SI" <?php if(isset($formato->extras)) {if( $formato->extras->espejo_electrocromatico == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck17" name="espejo_electrocromatico_ck" value="NO" <?php if(isset($formato->extras)) {if( $formato->extras->espejo_electrocromatico == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb7_ck17" name="espejo_electrocromatico_ck" value="NA" <?php if(isset($formato->extras)) {if( $formato->extras->espejo_electrocromatico == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                        </table>

                        <br>
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <td colspan="4" style="font-weight: bold;">
                                    DESEMPEÑO
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    A. TREN MOTRIZ
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    SI
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NO
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NA
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Motores
                                    <input type="hidden" name="motores" id="motores" value="<?php echo isset($formato->desempeno) ? $formato->desempeno->motores : 'SI'?>">
                                    <div id="motores_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb8_ck1" name="motores_ck" value="SI" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->motores == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck1" name="motores_ck" value="NO" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->motores == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck1" name="motores_ck" value="NA" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->motores == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Embraque (Clutch)
                                    <input type="hidden" name="clutch" id="clutch" value="<?php echo isset($formato->desempeno) ? $formato->desempeno->clutch : 'SI'?>">
                                    <div id="clutch_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb8_ck2" name="clutch_ck" value="SI" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->clutch == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck2" name="clutch_ck" value="NO" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->clutch == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck2" name="clutch_ck" value="NA" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->clutch == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Transmisiones (Caja de Cambios)
                                    <input type="hidden" name="caja_cambios" id="caja_cambios" value="<?php echo isset($formato->desempeno) ? $formato->desempeno->caja_cambios : 'SI'?>">
                                    <div id="caja_cambios_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb8_ck3" name="caja_cambios_ck" value="SI" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->caja_cambios == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck3" name="caja_cambios_ck" value="NO" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->caja_cambios == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck3" name="caja_cambios_ck" value="NA" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->caja_cambios == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Árbol de Transmisiones (Flecha Cardán)
                                    <input type="hidden" name="arbol_transmisiones" id="arbol_transmisiones" value="<?php echo isset($formato->desempeno) ? $formato->desempeno->arbol_transmisiones : 'SI'?>">
                                    <div id="arbol_transmisiones_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb8_ck4" name="arbol_transmisiones_ck" value="SI" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->arbol_transmisiones == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck4" name="arbol_transmisiones_ck" value="NO" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->arbol_transmisiones == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck4" name="arbol_transmisiones_ck" value="NA" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->arbol_transmisiones == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Diferencial
                                    <input type="hidden" name="diferencial" id="diferencial" value="<?php echo isset($formato->desempeno) ? $formato->desempeno->diferencial : 'SI'?>">
                                    <div id="diferencial_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb8_ck5" name="diferencial_ck" value="SI" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->diferencial == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck5" name="diferencial_ck" value="NO" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->diferencial == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck5" name="diferencial_ck" value="NA" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->diferencial == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Diferentes tipos de Tracción/Propulsión
                                    <input type="hidden" name="diferencial_traccion" id="diferencial_traccion" value="<?php echo isset($formato->desempeno) ? $formato->desempeno->diferencial_traccion : 'SI'?>">
                                    <div id="diferencial_traccion_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb8_ck6" name="diferencial_traccion_ck" value="SI" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->diferencial_traccion == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck6" name="diferencial_traccion_ck" value="NO" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->diferencial_traccion == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck6" name="diferencial_traccion_ck" value="NA" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->diferencial_traccion == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Caja Reductora
                                    <input type="hidden" name="caja_reductora" id="caja_reductora" value="<?php echo isset($formato->desempeno) ? $formato->desempeno->caja_reductora : 'SI'?>">
                                    <div id="caja_reductora_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb8_ck7" name="caja_reductora_ck" value="SI" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->caja_reductora == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck7" name="caja_reductora_ck" value="NO" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->caja_reductora == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck7" name="caja_reductora_ck" value="NA" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->caja_reductora == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Caja de Tranferencia
                                    <input type="hidden" name="caja_transferencia" id="caja_transferencia" value="<?php echo isset($formato->desempeno) ? $formato->desempeno->caja_transferencia : 'SI'?>">
                                    <div id="caja_transferencia_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb8_ck8" name="caja_transferencia_ck" value="SI" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->caja_transferencia == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck8" name="caja_transferencia_ck" value="NO" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->caja_transferencia == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck8" name="caja_transferencia_ck" value="NA" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->caja_transferencia == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Sistema Dinámico de Torque Vectorial
                                    <input type="hidden" name="sistema_dinamico" id="sistema_dinamico" value="<?php echo isset($formato->desempeno) ? $formato->desempeno->sistema_dinamico : 'SI'?>">
                                    <div id="sistema_dinamico_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb8_ck9" name="sistema_dinamico_ck" value="SI" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->sistema_dinamico == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck9" name="sistema_dinamico_ck" value="NO" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->sistema_dinamico == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck9" name="sistema_dinamico_ck" value="NA" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->sistema_dinamico == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Dirección
                                    <input type="hidden" name="direccion" id="direccion" value="<?php echo isset($formato->desempeno) ? $formato->desempeno->direccion : 'SI'?>">
                                    <div id="direccion_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb8_ck10" name="direccion_ck" value="SI" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->direccion == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck10" name="direccion_ck" value="NO" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->direccion == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck10" name="direccion_ck" value="NA" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->direccion == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Suspensión
                                    <input type="hidden" name="suspension" id="suspension" value="<?php echo isset($formato->desempeno) ? $formato->desempeno->suspension : 'SI'?>">
                                    <div id="suspension_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb8_ck11" name="suspension_ck" value="SI" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->suspension == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck11" name="suspension_ck" value="NO" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->suspension == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck11" name="suspension_ck" value="NA" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->suspension == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Toma de poder PTP en Super Duty
                                    <input type="hidden" name="toma_poder" id="toma_poder" value="<?php echo isset($formato->desempeno) ? $formato->desempeno->toma_poder : 'SI'?>">
                                    <div id="toma_poder_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb8_ck12" name="toma_poder_ck" value="SI" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->toma_poder == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck12" name="toma_poder_ck" value="NO" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->toma_poder == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb8_ck12" name="toma_poder_ck" value="NA" <?php if(isset($formato->desempeno)) {if( $formato->desempeno->toma_poder == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                        </table>

                        <br>
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <td style="width: 70%;">
                                    B. SISTEMA DE AHORRO DE COMBUSTIBLE
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    SI
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NO
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NA
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    iVCT
                                    <input type="hidden" name="ivct" id="ivct" value="<?php echo isset($formato->ahorro_combustible) ? $formato->ahorro_combustible->ivct : 'SI'?>">
                                    <div id="ivct_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb9_ck1" name="ivct_ck" value="SI" <?php if(isset($formato->ahorro_combustible)) {if( $formato->ahorro_combustible->ivct == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb9_ck1" name="ivct_ck" value="NO" <?php if(isset($formato->ahorro_combustible)) {if( $formato->ahorro_combustible->ivct == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb9_ck1" name="ivct_ck" value="NA" <?php if(isset($formato->ahorro_combustible)) {if( $formato->ahorro_combustible->ivct == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    TiVCT
                                    <input type="hidden" name="tivct" id="tivct" value="<?php echo isset($formato->ahorro_combustible) ? $formato->ahorro_combustible->tivct : 'SI'?>">
                                    <div id="tivct_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb9_ck2" name="tivct_ck" value="SI" <?php if(isset($formato->ahorro_combustible)) {if( $formato->ahorro_combustible->tivct == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb9_ck2" name="tivct_ck" value="NO" <?php if(isset($formato->ahorro_combustible)) {if( $formato->ahorro_combustible->tivct == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb9_ck2" name="tivct_ck" value="NA" <?php if(isset($formato->ahorro_combustible)) {if( $formato->ahorro_combustible->tivct == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                        </table>

                        <br>
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <td style="width: 70%;">
                                    C. VEHÍCULOS HÍBRIDOS
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    SI
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NO
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NA
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    ¿Qué es un vehículo híbrido?
                                    <input type="hidden" name="vehiculo_hibrido" id="vehiculo_hibrido" value="<?php echo isset($formato->vehiculo_hibrido) ? $formato->vehiculo_hibrido->vehiculo_hibrido : 'SI'?>">
                                    <div id="vehiculo_hibrido_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb10_ck1" name="vehiculo_hibrido_ck" value="SI" <?php if(isset($formato->vehiculo_hibrido)) {if( $formato->vehiculo_hibrido->vehiculo_hibrido == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb10_ck1" name="vehiculo_hibrido_ck" value="NO" <?php if(isset($formato->vehiculo_hibrido)) {if( $formato->vehiculo_hibrido->vehiculo_hibrido == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb10_ck1" name="vehiculo_hibrido_ck" value="NA" <?php if(isset($formato->vehiculo_hibrido)) {if( $formato->vehiculo_hibrido->vehiculo_hibrido == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    ¿Cuándo se utiliza el motor de gasolina?
                                    <input type="hidden" name="motor_gasolina" id="motor_gasolina" value="<?php echo isset($formato->vehiculo_hibrido) ? $formato->vehiculo_hibrido->motor_gasolina : 'SI'?>">
                                    <div id="motor_gasolina_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb10_ck2" name="motor_gasolina_ck" value="SI" <?php if(isset($formato->vehiculo_hibrido)) {if( $formato->vehiculo_hibrido->motor_gasolina == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb10_ck2" name="motor_gasolina_ck" value="NO" <?php if(isset($formato->vehiculo_hibrido)) {if( $formato->vehiculo_hibrido->motor_gasolina == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb10_ck2" name="motor_gasolina_ck" value="NA" <?php if(isset($formato->vehiculo_hibrido)) {if( $formato->vehiculo_hibrido->motor_gasolina == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Tips de manejo
                                    <input type="hidden" name="tips_manejo" id="tips_manejo" value="<?php echo isset($formato->vehiculo_hibrido) ? $formato->vehiculo_hibrido->tips_manejo : 'SI'?>">
                                    <div id="tips_manejo_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb10_ck3" name="tips_manejo_ck" value="SI" <?php if(isset($formato->vehiculo_hibrido)) {if( $formato->vehiculo_hibrido->tips_manejo == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb10_ck3" name="tips_manejo_ck" value="NO" <?php if(isset($formato->vehiculo_hibrido)) {if( $formato->vehiculo_hibrido->tips_manejo == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb10_ck3" name="tips_manejo_ck" value="NA" <?php if(isset($formato->vehiculo_hibrido)) {if( $formato->vehiculo_hibrido->tips_manejo == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Transmisión E-CVT
                                    <input type="hidden" name="transmision_ecvt" id="transmision_ecvt" value="<?php echo isset($formato->vehiculo_hibrido) ? $formato->vehiculo_hibrido->transmision_ecvt : 'SI'?>">
                                    <div id="transmision_ecvt_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb10_ck4" name="transmision_ecvt_ck" value="SI" <?php if(isset($formato->vehiculo_hibrido)) {if( $formato->vehiculo_hibrido->transmision_ecvt == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb10_ck4" name="transmision_ecvt_ck" value="NO" <?php if(isset($formato->vehiculo_hibrido)) {if( $formato->vehiculo_hibrido->transmision_ecvt == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb10_ck4" name="transmision_ecvt_ck" value="NA" <?php if(isset($formato->vehiculo_hibrido)) {if( $formato->vehiculo_hibrido->transmision_ecvt == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                        </table>

                        <br>
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <td style="width: 70%;">
                                    D. OTRAS TECNOLOGÍAS
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    SI
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NO
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NA
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Easy Fuel
                                    <input type="hidden" name="easy_fuel" id="easy_fuel" value="<?php echo isset($formato->otras_tecnologias) ? $formato->otras_tecnologias->easy_fuel : 'SI'?>">
                                    <div id="easy_fuel_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb11_ck1" name="easy_fuel_ck" value="SI" <?php if(isset($formato->otras_tecnologias)) {if( $formato->otras_tecnologias->easy_fuel == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb11_ck1" name="easy_fuel_ck" value="NO" <?php if(isset($formato->otras_tecnologias)) {if( $formato->otras_tecnologias->easy_fuel == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb11_ck1" name="easy_fuel_ck" value="NA" <?php if(isset($formato->otras_tecnologias)) {if( $formato->otras_tecnologias->easy_fuel == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Track apps
                                    <input type="hidden" name="track_apps" id="track_apps" value="<?php echo isset($formato->otras_tecnologias) ? $formato->otras_tecnologias->track_apps : 'SI'?>">
                                    <div id="track_apps_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb11_ck2" name="track_apps_ck" value="SI" <?php if(isset($formato->otras_tecnologias)) {if( $formato->otras_tecnologias->track_apps == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb11_ck2" name="track_apps_ck" value="NO" <?php if(isset($formato->otras_tecnologias)) {if( $formato->otras_tecnologias->track_apps == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb11_ck2" name="track_apps_ck" value="NA" <?php if(isset($formato->otras_tecnologias)) {if( $formato->otras_tecnologias->track_apps == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Auto Start Stop
                                    <input type="hidden" name="auto_start_stop" id="auto_start_stop" value="<?php echo isset($formato->otras_tecnologias) ? $formato->otras_tecnologias->auto_start_stop : 'SI'?>">
                                    <div id="auto_start_stop_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb11_ck3" name="auto_start_stop_ck" value="SI" <?php if(isset($formato->otras_tecnologias)) {if( $formato->otras_tecnologias->auto_start_stop == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb11_ck3" name="auto_start_stop_ck" value="NO" <?php if(isset($formato->otras_tecnologias)) {if( $formato->otras_tecnologias->auto_start_stop == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb11_ck3" name="auto_start_stop_ck" value="NA" <?php if(isset($formato->otras_tecnologias)) {if( $formato->otras_tecnologias->auto_start_stop == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Sistema de Control de Terreno
                                    <input type="hidden" name="sistema_ctr_terreno" id="sistema_ctr_terreno" value="<?php echo isset($formato->otras_tecnologias) ? $formato->otras_tecnologias->sistema_ctr_terreno : 'SI'?>">
                                    <div id="sistema_ctr_terreno_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb11_ck4" name="sistema_ctr_terreno_ck" value="SI" <?php if(isset($formato->otras_tecnologias)) {if( $formato->otras_tecnologias->sistema_ctr_terreno == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb11_ck4" name="sistema_ctr_terreno_ck" value="NO" <?php if(isset($formato->otras_tecnologias)) {if( $formato->otras_tecnologias->sistema_ctr_terreno == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb11_ck4" name="sistema_ctr_terreno_ck" value="NA" <?php if(isset($formato->otras_tecnologias)) {if( $formato->otras_tecnologias->sistema_ctr_terreno == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                        </table>

                        <br>
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <td style="width: 70%;">
                                    DOCUMENTOS
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    SI
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NO
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NA
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Carta Factura
                                    <input type="hidden" name="carta_factura" id="carta_factura" value="<?php echo isset($formato->documentos) ? $formato->documentos->carta_factura : 'SI'?>">
                                    <div id="carta_factura_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb12_ck1" name="carta_factura_ck" value="SI"  <?php if(isset($formato->documentos)) {if( $formato->documentos->carta_factura == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb12_ck1" name="carta_factura_ck" value="NO"  <?php if(isset($formato->documentos)) {if( $formato->documentos->carta_factura == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb12_ck1" name="carta_factura_ck" value="NA"  <?php if(isset($formato->documentos)) {if( $formato->documentos->carta_factura == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Póliza de Garantía
                                    <input type="hidden" name="poliza_garantia" id="poliza_garantia" value="<?php echo isset($formato->documentos) ? $formato->documentos->poliza_garantia : 'SI'?>">
                                    <div id="poliza_garantia_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb12_ck2" name="poliza_garantia_ck" value="SI" <?php if(isset($formato->documentos)) {if( $formato->documentos->poliza_garantia == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb12_ck2" name="poliza_garantia_ck" value="NO" <?php if(isset($formato->documentos)) {if( $formato->documentos->poliza_garantia == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb12_ck2" name="poliza_garantia_ck" value="NA" <?php if(isset($formato->documentos)) {if( $formato->documentos->poliza_garantia == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Póliza de Seguro
                                    <input type="hidden" name="poliza_seguro" id="poliza_seguro" value="<?php echo isset($formato->documentos) ? $formato->documentos->poliza_seguro : 'SI'?>">
                                    <div id="poliza_seguro_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb12_ck3" name="poliza_seguro_ck" value="SI" <?php if(isset($formato->documentos)) {if( $formato->documentos->poliza_seguro == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb12_ck3" name="poliza_seguro_ck" value="NO" <?php if(isset($formato->documentos)) {if( $formato->documentos->poliza_seguro == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb12_ck3" name="poliza_seguro_ck" value="NA" <?php if(isset($formato->documentos)) {if( $formato->documentos->poliza_seguro == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Tenencia
                                    <input type="hidden" name="tenencia" id="tenencia" value="<?php echo isset($formato->documentos) ? $formato->documentos->tenencia : 'SI'?>">
                                    <div id="tenencia_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb12_ck4" name="tenencia_ck" value="SI" <?php if(isset($formato->documentos)) {if( $formato->documentos->tenencia == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb12_ck4" name="tenencia_ck" value="NO" <?php if(isset($formato->documentos)) {if( $formato->documentos->tenencia == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb12_ck4" name="tenencia_ck" value="NA" <?php if(isset($formato->documentos)) {if( $formato->documentos->tenencia == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;">
                                    Herramientas
                                    <input type="hidden" name="herramientas" id="herramientas" value="<?php echo isset($formato->documentos) ? $formato->documentos->herramientas : 'SI'?>">
                                    <div id="herramientas_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb12_ck5" name="herramientas_ck" value="SI" <?php if(isset($formato->documentos)) {if( $formato->documentos->herramientas == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb12_ck5" name="herramientas_ck" value="NO" <?php if(isset($formato->documentos)) {if( $formato->documentos->herramientas == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb12_ck5" name="herramientas_ck" value="NA" <?php if(isset($formato->documentos)) {if( $formato->documentos->herramientas == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                        </table>

                        <br>
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <td style="width: 80%;">
                                    COMENTARIOS
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    SI
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    NO
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 80%;">
                                    ¿Se le explicó la aplicación y funcionamiento de asistencia ford 24 hrs?
                                    <input type="hidden" name="explico_asistencia_ford" id="explico_asistencia_ford" value="<?php echo isset($formato->comentarios) ? $formato->comentarios->explico_asistencia_ford : 'SI'?>">
                                    <div id="explico_asistencia_ford_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb13_ck1" name="explico_asistencia_ford_ck" value="SI" <?php if(isset($formato->comentarios)) {if( $formato->comentarios->explico_asistencia_ford == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb13_ck1" name="explico_asistencia_ford_ck" value="NO" <?php if(isset($formato->comentarios)) {if( $formato->comentarios->explico_asistencia_ford == "NO") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 80%;">
                                    ¿Se le explicó los beneficios de contratar FORD PROTEC ext. Gtia?
                                    <input type="hidden" name="explico_ford_protect" id="explico_ford_protect" value="<?php echo isset($formato->comentarios) ? $formato->comentarios->explico_ford_protect : 'SI'?>">
                                    <div id="explico_ford_protect_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb13_ck2" name="explico_ford_protect_ck" value="SI" <?php if(isset($formato->comentarios)) {if( $formato->comentarios->explico_ford_protect == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb13_ck2" name="explico_ford_protect_ck" value="NO" <?php if(isset($formato->comentarios)) {if( $formato->comentarios->explico_ford_protect == "NO") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 80%;">
                                    ¿Se le ofrecieron los accesorios para personalizar su vehículo Ford?
                                    <input type="hidden" name="accesorios_personalizar" id="accesorios_personalizar" value="<?php echo isset($formato->comentarios) ? $formato->comentarios->accesorios_personalizar : 'SI'?>">
                                    <div id="accesorios_personalizar_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="accesorios_personalizar" name="accesorios_personalizar_ck" value="SI" <?php if(isset($formato->comentarios)) {if( $formato->comentarios->accesorios_personalizar == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="accesorios_personalizar" name="accesorios_personalizar_ck" value="NO" <?php if(isset($formato->comentarios)) {if( $formato->comentarios->accesorios_personalizar == "NO") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 80%;">
                                    ¿Se le ofrecio realizar Prueba de Manejo?
                                    <input type="hidden" name="prueba_manejo" id="prueba_manejo" value="<?php echo isset($formato->comentarios) ? $formato->comentarios->prueba_manejo : 'SI'?>">
                                    <div id="prueba_manejo_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="prueba_manejo" name="prueba_manejo_ck" value="SI" <?php if(isset($formato->comentarios)) {if( $formato->comentarios->prueba_manejo == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="prueba_manejo" name="prueba_manejo_ck" value="NO" <?php if(isset($formato->comentarios)) {if( $formato->comentarios->prueba_manejo == "NO") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 80%;">
                                    ¿Se le proporcionó  información de Mantenimiento?
                                    <input type="hidden" name="informacion_mantenimiento" id="informacion_mantenimiento" value="<?php echo isset($formato->comentarios) ? $formato->comentarios->informacion_mantenimiento : 'SI'?>">
                                    <div id="informacion_mantenimiento_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb13_ck6" name="informacion_mantenimiento_ck" value="SI" <?php if(isset($formato->comentarios)) {if( $formato->comentarios->informacion_mantenimiento == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb13_ck6" name="informacion_mantenimiento_ck" value="NO" <?php if(isset($formato->comentarios)) {if( $formato->comentarios->informacion_mantenimiento == "NO") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 80%;">
                                    ¿Se te presentó a un Asesor de Servicio?
                                    <input type="hidden" name="asesor_servicio" id="asesor_servicio" value="<?php echo isset($formato->comentarios) ? $formato->comentarios->asesor_servicio : 'SI'?>">
                                    <div id="asesor_servicio_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" checked  class="tb13_ck7" name="asesor_servicio_ck" value="SI" <?php if(isset($formato->comentarios)) {if( $formato->comentarios->asesor_servicio == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 10%;" class="text-center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb13_ck7" name="asesor_servicio_ck" value="NO" <?php if(isset($formato->comentarios)) {if( $formato->comentarios->asesor_servicio == "NO") echo "checked"; } ?>>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="offset-md-1 col-md-4 text-center" >
                        <h5 style="color:black;">FIRMA DEL CLIENTE</h5>
                        <br>
                        <img class="marcoImg" src="{{ isset($formato->firma_cliene) ? $formato->firma_cliene : 'SI'}}" id="firma_cliente_img" alt="" style="height:2cm;width:4cm;">
                        <br><br>
                        <input type="hidden" id="firma_cliente" name="firma_cliente" value="<?php if(isset($formato->firma_cliente)) echo $firma_cliente;?>" >
                        <input type="text" class="form-control" value="{{ isset($formato->cliente->nombre_cliente)  ? $formato->cliente->nombre_cliente : "" }}" id="cliente" name="cliente">
                        <?php if (isset($formato->firma_cliene)): ?>
                            <?php if ($formato->firma_cliene == ""): ?>
                                <a id="btn_firma_cliente" class="cuadroFirma btn btn-w-md btn-inf mt-4" style="color:darkblue !important;" data-value="firma_cliente" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma Cliente</a>
                            <?php endif; ?>
                        <?php else: ?>
                            <a id="btn_firma_cliente" class="cuadroFirma btn btn-w-md btn-info mt-4" style="color:darkblue !important;" data-value="firma_cliente" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma Cliente </a>
                        <?php endif; ?>
                        
                    </div>
                    <div class="col-md-4 offset-md-1 text-center">
                        <h5 style="color:black;">NOMBRE Y FIRMA DEL ASESOR</h5>
                        <br>
                        <img class="marcoImg"src="{{ isset($formato->firma_asesor) ? $formato->firma_asesor : 'SI'}}" id="firma_asesor_img" alt="" style="height:2cm;width:4cm;">
                        <input type="hidden" id="firma_asesor" name="firma_asesor" value="<?php if(isset($formato->firma_asesor)) echo $formato->firma_asesor;?>" >
                        <br><br>
                        
                        <input type="text" class="form-control" value="{{ isset($formato->asesor->nombre) ? $formato->asesor->nombre : 'SI' }}" disabled id="nombre_asesor" name="nombre_asesor">
                        <div id="nombre_asesor_error" class="invalid-feedback"></div>

                        <?php if (isset($formato->firma_asesor)): ?>
                            <?php if ($formato->firma_asesor == ""): ?>
                                <a id="btn_firma_asesor" class="cuadroFirma btn btn-w-md btn-info mt-4" style="color:darkblue !important;" data-value="firma_asesor" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma Asesor</a>
                            <?php endif; ?>
                        <?php else: ?>
                            <a id="btn_firma_asesor" class="cuadroFirma btn btn-w-md btn-info mt-4" style="color:darkblue !important;" data-value="firma_asesor" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma Asesor </a>
                        <?php endif; ?>
                        
                    </div>
                </div>
                <div class="row mt-4">
                    @if (isset($detalle_unidad->tipo_venta) && $detalle_unidad->tipo_venta == 1)
                        <div class="col-md-12 text-center" >
                            <br>
                            <h5 style="color:black;">FIRMA CONTABILIDAD</h5>
                            <img class="marcoImg" src="{{ isset($formato->firma_contadora) ? $formato->firma_contadora : 'SI'}}" id="firma_contadora_img" alt="" style="height:2cm;width:4cm;">
                            <br><br>
                            <input type="hidden" id="firma_contadora" name="firma_contadora" value="{{ isset($formato->firma_contadora) ? $formato->firma_contadora:'' }}" >
                            <?php if (isset($formato->firma_contadora)): ?>
                                <?php if ($formato->firma_contadora == ""): ?>
                                    <a id="btn_firma_contadora" class="cuadroFirma btn btn-w-md btn-inf mt-4" style="color:darkblue !important;" data-value="firma_contabilidad" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar Contabilidad</a>
                                <?php endif; ?>
                            <?php else: ?>
                                <a id="btn_firma_contadora" class="cuadroFirma btn btn-w-md btn-info mt-4" style="color:darkblue !important;" data-value="firma_contabilidad" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar Contabilidad </a>
                            <?php endif; ?>
                            
                        </div>
                    @else

                    <div class="col-md-12 text-center">
                        <h5 style="color:black;">FIRMA GERENTE DE CREDITO</h5>
                        <br>
                        <img class="marcoImg"src="{{ isset($formato->firma_credito) ? $formato->firma_credito : 'SI'}}" id="firma_credito_img" alt="" style="height:2cm;width:4cm;">
                        <input type="hidden" id="firma_credito" name="firma_credito" value="<?php if(isset($formato->firma_credito)) echo $formato->firma_credito;?>" >
                        <br><br>
                        <?php if (isset($formato->firma_credito)): ?>
                            <?php if ($formato->firma_credito == ""): ?>
                                <a id="btn_firma_credito" class="cuadroFirma btn btn-w-md btn-info mt-4" style="color:darkblue !important;" data-value="firma_gerente_credito" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma Asesor</a>
                            <?php endif; ?>
                        <?php else: ?>
                            <a id="btn_firma_credito" class="cuadroFirma btn btn-w-md btn-info mt-4" style="color:darkblue !important;" data-value="firma_gerente_credito" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma Asesor </a>
                        <?php endif; ?>
                        
                    </div>
                    @endif
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <button onclick="guardar()" class="btn btn-primary col-md-3" type="button"> Guardar formato</button>
                    </div>
                </div>
                        
            </form>
        </div>
    </div>
</div>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title" id="firmaDigitalLabel"></h4>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" class="text-center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="firma_actual" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
    <script src="{{ base_url('js/autos/checks_salida.js') }}"></script>
    <script src="{{ base_url('js/autos/firmas.js') }}"></script>

    <script type="">
        //Para guardar registro por primera vez
        const guardar = () =>{
            $(".validation_error").html("");
            ajax.post('api/venta-unidades/storeformatosalida', form(), function(response, headers) {
                if (headers.status == 400) {
                    return toastr.error("Todos los campos son requeridos");
                }

                utils.displayWarningDialog("Proceso completado con éxito", "success", function(data) {
                    return window.location.href = base_url + 'autos/ventas/index';
                })
            }, false, false)
        };

        let form = function() {

            return  {
                //Campos generales
                id_venta_auto : $("#form-salida").data('id'),
                firma_cliene: document.getElementById("firma_cliente").value,
                firma_asesor: document.getElementById("firma_asesor").value,
                
                firma_contadora: document.getElementById("firma_contadora") ? document.getElementById("firma_contadora").value : 'SI',
                firma_credito: document.getElementById("firma_credito") ? document.getElementById("firma_credito").value : 'SI',

                asistente_arranque: document.getElementById("asistente_arranque").value,
                preservacion_carril: document.getElementById("preservacion_carril").value,
                sensores_del_lat: document.getElementById("sensores_del_lat").value,
                blis: document.getElementById("blis").value,
                alert_trafico_cruzado: document.getElementById("alert_trafico_cruzado").value,
                asistencia_activa_estaci: document.getElementById("asistencia_activa_estaci").value,
                camara_reversa: document.getElementById("camara_reversa").value,
                camara_frontal: document.getElementById("camara_frontal").value,
                camara_360_grados: document.getElementById("camara_360_grados").value,
                sistema_mon_pll_tms: document.getElementById("sistema_mon_pll_tms").value,
                
                //Segunda tabla (Seguridad)
                sistema_frenado: document.getElementById("sistema_frenado").value,
                control_traccion: document.getElementById("control_traccion").value,
                esc: document.getElementById("esc").value,
                rsc: document.getElementById("rsc").value,
                control_torque_curvas: document.getElementById("control_torque_curvas").value,
                control_curvas: document.getElementById("control_curvas").value,
                control_balance_remolque: document.getElementById("control_balance_remolque").value,
                frenos_electronicos: document.getElementById("frenos_electronicos").value,
                control_electronico_des: document.getElementById("control_electronico_des").value,
                
                //Tercera tabla (seguridad pasiva)
                cinturon_seguridad: document.getElementById("cinturon_seguridad").value,
                bolsas_aire: document.getElementById("bolsas_aire").value,
                chasis_carroceria: document.getElementById("chasis_carroceria").value,
                carroseria_hidroforma: document.getElementById("carroseria_hidroforma").value,
                cristales: document.getElementById("cristales").value,
                cabeceras: document.getElementById("cabeceras").value,
                sistema_alerta: document.getElementById("sistema_alerta").value,
                teclado_puerta: document.getElementById("teclado_puerta").value,
                alarma_volumetrica: document.getElementById("alarma_volumetrica").value,
                alarma_perimetral: document.getElementById("alarma_perimetral").value,
                mikey: document.getElementById("mikey").value,
                
                //Cuarta tabla (confort)
                accionamiento_manual: document.getElementById("accionamiento_manual").value,
                accionamiento_electrico: document.getElementById("accionamiento_electrico").value,
                asiento_delantero_asaje: document.getElementById("asiento_delantero_asaje").value,
                asiento_calefaccion: document.getElementById("asiento_calefaccion").value,
                easy_power_fold: document.getElementById("easy_power_fold").value,
                memoria_asientos: document.getElementById("memoria_asientos").value,
                
                //Quinta tabla (Iluminacion)
                iluminacion_ambiental: document.getElementById("iluminacion_ambiental").value,
                sistema_iluminacion: document.getElementById("sistema_iluminacion").value,
                faros_bixenon: document.getElementById("faros_bixenon").value,
                hid: document.getElementById("hid").value,
                led: document.getElementById("led").value,
                
                //Sexta tabla (especiales)
                peps: document.getElementById("peps").value,
                apertura_cajuela: document.getElementById("apertura_cajuela").value,
                encendido_remoto: document.getElementById("encendido_remoto").value,
                techo_panoramico: document.getElementById("techo_panoramico").value,
                aire_acondicionado: document.getElementById("aire_acondicionado").value,
                eatc: document.getElementById("eatc").value,
                consola_enfriamiento: document.getElementById("consola_enfriamiento").value,
                volante_ajuste_altura: document.getElementById("volante_ajuste_altura").value,
                volante_calefactado: document.getElementById("volante_calefactado").value,
                estribos_electricos: document.getElementById("estribos_electricos").value,
                apertura_caja_carga: document.getElementById("apertura_caja_carga").value,
                limpiaparabrisas: document.getElementById("limpiaparabrisas").value,
                espejo_electrico: document.getElementById("espejo_electrico").value,
                espejo_abatible: document.getElementById("espejo_abatible").value,
                
                //Septima tabla (conectividad)
                conectividad: document.getElementById("conectividad").value,
                sync: document.getElementById("sync").value,
                myFord_touch: document.getElementById("myFord_touch").value,
                myFord_touch_navigation: document.getElementById("myFord_touch_navigation").value,
                sync_fordpass: document.getElementById("sync_fordpass").value,
                apple_carplay: document.getElementById("apple_carplay").value,
                android_auto: document.getElementById("android_auto").value,
                consola_smartphone: document.getElementById("consola_smartphone").value,
                audio_shaker_pro: document.getElementById("audio_shaker_pro").value,
                radio_hd: document.getElementById("radio_hd").value,
                equipo_sony: document.getElementById("equipo_sony").value,
                puertos_usb: document.getElementById("puertos_usb").value,
                bluetooh: document.getElementById("bluetooh").value,
                wifi: document.getElementById("wifi").value,
                tarjeta_sd: document.getElementById("tarjeta_sd").value,
                inversor_corriente: document.getElementById("inversor_corriente").value,
                espejo_electrocromatico: document.getElementById("espejo_electrocromatico").value,
                
                //Octava tabla (desempeño)
                motores: document.getElementById("motores").value,
                clutch: document.getElementById("clutch").value,
                caja_cambios: document.getElementById("caja_cambios").value,
                arbol_transmisiones: document.getElementById("arbol_transmisiones").value,
                diferencial: document.getElementById("diferencial").value,
                diferencial_traccion: document.getElementById("diferencial_traccion").value,
                caja_reductora: document.getElementById("caja_reductora").value,
                caja_transferencia: document.getElementById("caja_transferencia").value,
                sistema_dinamico: document.getElementById("sistema_dinamico").value,
                direccion: document.getElementById("direccion").value,
                suspension: document.getElementById("suspension").value,
                toma_poder: document.getElementById("toma_poder").value,
                
                //Novena-Undecima tabla (ahorro combustible - vehiculo hibrido - otras tecnologias)
                ivct: document.getElementById("ivct").value,
                tivct: document.getElementById("tivct").value,

                vehiculo_hibrido: document.getElementById("vehiculo_hibrido").value,
                motor_gasolina: document.getElementById("motor_gasolina").value,
                tips_manejo: document.getElementById("tips_manejo").value,
                transmision_ecvt: document.getElementById("transmision_ecvt").value,

                easy_fuel: document.getElementById("easy_fuel").value,
                track_apps: document.getElementById("track_apps").value,
                auto_start_stop: document.getElementById("auto_start_stop").value,
                sistema_ctr_terreno: document.getElementById("sistema_ctr_terreno").value,
                
                //Doceava tabla (documentos)
                carta_factura: document.getElementById("carta_factura").value,
                poliza_garantia: document.getElementById("poliza_garantia").value,
                poliza_seguro: document.getElementById("poliza_seguro").value,
                tenencia: document.getElementById("tenencia").value,
                herramientas: document.getElementById("herramientas").value,
                
                //Terceava tabla (comentarios)
                explico_asistencia_ford: document.getElementById("explico_asistencia_ford").value,
                explico_ford_protect: document.getElementById("explico_ford_protect").value,
                accesorios_personalizar: document.getElementById("accesorios_personalizar").value,
                prueba_manejo: document.getElementById("prueba_manejo").value,
                informacion_mantenimiento: document.getElementById("informacion_mantenimiento").value,
                asesor_servicio: document.getElementById("asesor_servicio").value,
            };
        }

        const sincronizar_cliente =() => $("#cliente").val($('#nombre_cliente').val())
        const sincronizar_asesor =() => $("#nombre_asesor").val($('#nombre_vendedor').val());

    </script>
@endsection