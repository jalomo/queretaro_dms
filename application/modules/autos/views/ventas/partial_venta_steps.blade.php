<div class="d-flex justify-content-around bd-highlight mb-4 mt-5">
    <a href="{{ base_url('autos/ventas/unidades')}}" class="btn {{ $id_tab == '1' ? 'btn-step-info' : 'btn-step-light' }}">Prepedido</a>
    <a class="btn {{ $id_tab == '2' ? 'btn-step-info' : 'btn-step-light' }}">Credito</a>
    <a class="btn {{ $id_tab == '3' ? 'btn-step-info' : 'btn-step-light' }}">Abono</a>
    <a class="btn {{ $id_tab == '4' ? 'btn-step-info' : 'btn-step-light' }}">Documentación</a>
    <a class="btn {{ $id_tab == '5' ? 'btn-step-info' : 'btn-step-light' }}">Facturación</a>
    <a class="btn {{ $id_tab == '6' ? 'btn-step-info' : 'btn-step-light' }}">Salida</a>
    <a href="{{ base_url("autos/ventas") }}" class="btn btn-step-light">Regresar</a>
</div>

<hr class="mt-2" style="border-top: 1px solid #ededed;">