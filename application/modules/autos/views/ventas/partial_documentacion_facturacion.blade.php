<div class="row">
    <div class="col-md-12 mb-3">
        <h3>C.: FACTURACION TRADICIONAL</h3>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <input type="hidden" id="id_venta_unidad" value="{{ $id_venta_unidad }}">
                <?php renderFileUpload('file_pedido', 'PEDIDO'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('1', $archivos_subidos))
                    <button onclick='handleUpload(1, "file_pedido")' class="btn btn-primary">
                        <i class="fas fa-cloud-upload-alt"></i>
                    </button>
                @endif

                @if (array_key_exists('1', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[1]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button alt="" onclick='handleUploadUpdate(1, "file_pedido")' class="btn btn-primary">
                        <i class="fas fa-angle-double-up"></i>
                    </button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('file_remision', 'REMISION'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('2', $archivos_subidos))
                    <button onclick='handleUpload(2, "file_remision")' class="btn btn-primary">
                        <i class="fas fa-cloud-upload-alt"></i>
                    </button>
                @endif

                @if (array_key_exists('2', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[2]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    
                    <button onclick='handleUploadUpdate(2, "file_remision")' class="btn btn-primary">
                        <i class="fas fa-angle-double-up"></i>
                    </button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('ife', 'IFE'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('3', $archivos_subidos))
                <button onclick='handleUpload(2, "ife")' class="btn btn-primary"> 
                    <i class="fas fa-cloud-upload-alt"></i>
                </button>
                @endif

                @if (array_key_exists('3', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[3]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button onclick='handleUploadUpdate(2, "ife")' class="btn btn-primary"> 
                        <i class="fas fa-angle-double-up"></i>
                    </button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('curp', 'CURP'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('4', $archivos_subidos))
                    <button  onclick='handleUpload(4, "curp")' class="btn btn-primary"> <i class="fas fa-cloud-upload-alt"></i></button>
                @endif
                @if (array_key_exists('4', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[4]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button  onclick='handleUploadUpdate(4, "curp")' class="btn btn-primary">
                        <i class="fas fa-angle-double-up"></i>
                    </button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('comprobante_domicilio', 'COMPROBANTE DE DOMICILIO'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('5', $archivos_subidos))
                    <button onclick='handleUpload(5, "comprobante_domicilio")'  class="btn btn-primary"> <i class="fas fa-cloud-upload-alt"></i></button>
                @endif
                @if (array_key_exists('5', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[5]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button  onclick='handleUploadUpdate(5, "comprobante_domicilio")' class="btn btn-primary"> 
                        <i class="fas fa-angle-double-up"></i>
                    </button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('formato_id_clt', 'FORMATO ID DEL CLT'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('6', $archivos_subidos))
                    <button onclick='handleUpload(6, "formato_id_clt")' class="btn btn-primary"> <i class="fas fa-cloud-upload-alt"></i></button>
                @endif
                @if (array_key_exists('6', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[6]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button  onclick='handleUploadUpdate(6, "formato_id_clt")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('caja_anticipo', 'RECIBO DE CAJA DEL ANTICIPO'); ?>
            </div>
            <div class="col-md-2 mt-3">
                
                @if (!array_key_exists('7', $archivos_subidos))
                    <button onclick='handleUpload(7, "caja_anticipo")' class="btn btn-primary"> 
                        <i class="fas fa-cloud-upload-alt"></i>
                    </button>
                @endif
                @if (array_key_exists('7', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[7]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button  onclick='handleUploadUpdate(7, "caja_anticipo")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('formato_abono_ford_facturacion', 'FORMATO DE BONO FORD'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('8', $archivos_subidos))
                    <button onclick='handleUpload(8, "formato_abono_ford_facturacion")' class="btn btn-primary"> 
                        <i class="fas fa-cloud-upload-alt"></i>
                    </button>
                @endif
                @if (array_key_exists('8', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[8]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button  onclick='handleUploadUpdate(8, "formato_abono_ford_facturacion")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('situacion_fiscal', 'CONSTANCIA DE SITUACIÓN FISCAL'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('9', $archivos_subidos))
                    <button  onclick='handleUpload(9, "situacion_fiscal")'  class="btn btn-primary"> <i class="fas fa-cloud-upload-alt"></i></button>
                @endif
                @if (array_key_exists('9', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[9]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button  onclick='handleUploadUpdate(9, "situacion_fiscal")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('ficha_seguimiento', 'HOJA SICOP "FICHA DE SEGUIMIENTO"'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('10', $archivos_subidos))
                    <button onclick='handleUpload(10, "ficha_seguimiento")' class="btn btn-primary"> <i class="fas fa-cloud-upload-alt"></i></button>
                @endif

                @if (array_key_exists('10', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[10]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button  onclick='handleUploadUpdate(10, "ficha_seguimiento")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('contacto_cliente', 'HOJA SICOP "CORREO DE CONTACTO DEL CLIENTE"'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('11', $archivos_subidos))
                    <button onclick='handleUpload(11, "contacto_cliente")' class="btn btn-primary"> <i class="fas fa-cloud-upload-alt"></i></button>
                @endif
                
                @if (array_key_exists('11', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[11]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button  onclick='handleUploadUpdate(11, "contacto_cliente")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('vobo_area_procesos', 'VoBo AREA DE PROCESOS (MAIL)'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('12', $archivos_subidos))
                    <button onclick='handleUpload(12, "vobo_area_procesos")' class="btn btn-primary"> <i class="fas fa-cloud-upload-alt"></i></button>
                @endif

                @if (array_key_exists('12', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[12]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button onclick='handleUploadUpdate(12, "vobo_area_procesos")' class="btn btn-primary"><i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('acta_constitutiva', 'ACTA CONSTITUTIVA PERSONA MORAL'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('12', $archivos_subidos))
                    <button onclick='handleUpload(13, "acta_constitutiva")' class="btn btn-primary"> <i class="fas fa-cloud-upload-alt"></i></button>
                    
                @endif
                @if (array_key_exists('13', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[13]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button onclick='handleUploadUpdate(13, "acta_constitutiva")' class="btn btn-primary"><i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('situacion_fiscal', 'CONSTANCIA DE SITUACIÓN FISCAL'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('14', $archivos_subidos))
                    <button onclick='handleUpload(14, "situacion_fiscal")' class="btn btn-primary"> <i class="fas fa-cloud-upload-alt"></i></button>
                @endif

                @if (array_key_exists('14', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[14]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button onclick='handleUploadUpdate(14, "situacion_fiscal")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('comprobante_domicilio', 'COMPROBANTE DE DOMICILIO'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('15', $archivos_subidos))
                    <button onclick='handleUpload(15, "comprobante_domicilio")' class="btn btn-primary"> <i class="fas fa-cloud-upload-alt"></i></button>
                @endif

                @if (array_key_exists('15', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[15]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button onclick='handleUploadUpdate(15, "comprobante_domicilio")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('poder_representante_legal', 'PODER REPESENTANTE LEGAL'); ?>
            </div>
            <div class="col-md-2 mt-3">
                
                @if (!array_key_exists('16', $archivos_subidos))
                    <button onclick='handleUpload(16, "poder_representante_legal")' class="btn btn-primary"> <i class="fas fa-cloud-upload-alt"></i></button>
                @endif

                @if (array_key_exists('16', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[16]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button onclick='handleUploadUpdate(16, "poder_representante_legal")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('ife_representante_legal', 'IFE REPRESENTANTE LEGAL'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('17', $archivos_subidos))
                    <button onclick='handleUpload(17, "ife_representante_legal")' class="btn btn-primary"> <i class="fas fa-cloud-upload-alt"></i></button>
                @endif
                @if (array_key_exists('17', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[17]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button onclick='handleUploadUpdate(17, "ife_representante_legal")' class="btn btn-primary"><i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('curp_representante_legal', 'CURP REPRESENTANTE LEGAL'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('18', $archivos_subidos))
                    <button onclick='handleUpload(18, "curp_representante_legal")' class="btn btn-primary"> <i class="fas fa-cloud-upload-alt"></i></button>
                @endif
                @if (array_key_exists('18', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[18]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    
                    <button onclick='handleUploadUpdate(18, "curp_representante_legal")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('consulta_lista_negra', 'CONSULTA DE LISTAS NEGRAS'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('19', $archivos_subidos))
                    <button onclick='handleUpload(19, "consulta_lista_negra")' class="btn btn-primary"> <i class="fas fa-cloud-upload-alt"></i></button>
                @endif
                @if (array_key_exists('19', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[19]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button onclick='handleUploadUpdate(19, "consulta_lista_negra")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('carta_renuncia', 'CARTA RENUNCIA EXT. GARANTIA'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('20', $archivos_subidos))
                    <button onclick='handleUpload(20, "carta_renuncia")' class="btn btn-primary"> <i class="fas fa-cloud-upload-alt"></i></button>
                @endif
                @if (array_key_exists('20', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[20]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button onclick='handleUploadUpdate(20, "carta_renuncia")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
</div>