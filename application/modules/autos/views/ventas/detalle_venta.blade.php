@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : ''; ?></li>
            <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : ''; ?></li>
            <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : ''; ?></li>
        </ol>
        @include('ventas/partial_venta_steps')
        <div class="row justify-content-end">
            <div class="col-md-2 text-right">
                <a class="btn btn-primary" href="{{ base_url('autos/ventas') }}">
                    <i class="fas fa-list"></i> Regresar a listado
                </a>
            </div>
        </div>
        <h3>Datos de vendedor</h3>
        <div class="row">
            <div class="col-md-6">
                <?php renderInputText('text', 'vendedor', 'Vendedor', isset($detalle_unidad->nombre_vendedor) ? $detalle_unidad->nombre_vendedor . ' ' . $detalle_unidad->apellido_paterno_vendedor : $nombre_usuario, isset($detalle_unidad) ? true : true); ?>
                <input type="hidden" id="id_asesor" name="id_asesor"
                    value="{{ isset($detalle_unidad) ? $detalle_unidad->id_asesor : $id_asesor }}">
                <input type="hidden" name="id_venta" value="{{ $detalle_unidad->id }}" id="id_venta">
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="fecha_alta">Fecha de alta:</label>
                    <input disabled type="date" class="form-control" min="{{ date('Y-m-d') }}"
                        value="{{ isset($data->created_at) ? $data->created_at : date('Y-m-d') }}" id="orden_fecha"
                        name="orden_fecha" placeholder="">
                    <div id="fecha_alta_error" class="invalid-feedback"></div>
                </div>
            </div>
        </div>
        <?php
        // dd($detalle_unidad);
        ?>
        <h3>Datos del Cliente</h3>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="select">Cliente</label>
                    <select name="id_cliente" class="form-control " id="id_cliente">
                        <option value=""> Seleccionar</option>
                        @foreach ($catalogo_clientes as $cliente)
                            @if (isset($detalle_unidad) && $detalle_unidad->cliente_id == $cliente->id)
                                <option selected value="{{ $cliente->id }}">
                                    {{ $cliente->numero_cliente . ' - ' . $cliente->nombre . ' ' . $cliente->apellido_materno }}
                                </option>
                            @else
                                <option value="{{ $cliente->id }}">
                                    {{ $cliente->numero_cliente . ' - ' . $cliente->nombre . ' ' . $cliente->apellido_materno }}
                                </option>
                            @endif
                        @endforeach
                    </select>
                    <div id='id_cliente_error' class='invalid-feedback'></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="select">Nombre completo</label>
                    <input class="form-control" type="text" {{ isset($detalle_unidad) ? 'disabled' : '' }} id="nombre_cliente" value="{{ isset($detalle_unidad) ? $detalle_unidad->nombre_cliente . ' ' . $detalle_unidad->cliente_apellido_paterno : '' }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="select">RFC</label>
                    <input class="form-control" type="text" id="rfc_cliente" {{ isset($detalle_unidad) ? 'disabled' : '' }}  value="{{ isset($detalle_unidad) ? $detalle_unidad->cliente_rfc : '' }}">
                </div>
            </div>
        </div>
        <h3 class="mt-3">Datos de la Unidad</h3>

        @if (empty($detalle_unidad->id_unidad))
            <div class="row">
                <div class="col-md-4 ">
                    <?php renderInputText('text', 'descripcion_modelo', 'Descripción Modelo', isset($detalle_unidad) ? $detalle_unidad->descripcion_modelo : ''); ?>
                </div>
                <div class="col-md-8 mt-4">
                    <div class="alert alert-warning">
                        La unidad aun no se ha cargado en inventario, indica el modelo.
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <?php renderInputText('number', 'precio_venta', 'Precio de venta', isset($detalle_unidad) ? $detalle_unidad->total : '', false, '', 'onKeyup="validarprecioventa()"'); ?>
                </div>
                <div class="col-md-4">
                    <input type="hidden" name="id_estatus" id="id_estatus" value="17">
                    <?php renderInputText('number', 'descuento', 'Descuento', isset($detalle_unidad) ? $detalle_unidad->descuento : ''); ?>
                </div>
                <div class="col-md-4">
                    <?php renderInputText('number', 'impuesto_isan', 'Impuesto ISAN', isset($detalle_unidad) ? $detalle_unidad->impuesto_isan : ''); ?>
                </div>
                <div class="col-md-4">
                    <?php renderInputText('number', 'iva', 'IVA', isset($detalle_unidad) ? $detalle_unidad->iva : '', false, '', 'onChange="validariva()"'); ?>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="id_tipo_auto">Tipo de auto</label>
                        <select disabled class="form-control" name="id_tipo_auto" id="id_tipo_auto">
                            <option value=""> Seleccionar ..</option>
                            <option value="1" selected>Nuevo</option>
                        </select>
                        <div id="id_tipo_auto_error" class="invalid-feedback"></div>
                    </div>
                    <input type="hidden" name="moneda" id="moneda" value="MXN">
                </div>
                <div class="col-md-2">
                    <?php renderInputText('text', 'tasa_interes', 'Tasa de interes', isset($detalle_unidad) ? $detalle_unidad->tasa_interes : ''); ?>
                </div>
            </div>
        @else

            @include('ventas/prepedido/partial_datos_unidad')
        @endif

        <h3>Formas de pago</h3>
        <div class="row">
            <div class="col-md-4">
                <?php renderSelectArray('tipo_forma_pago_id', '¿Compra de credito ó contado?', $tipo_forma_pago, 'id', 'descripcion', isset($detalle_unidad) ? $detalle_unidad->tipo_forma_pago_id : null); ?>

            </div>
            <div class="col-md-4">
                <?php renderSelectArray('tipo_pago_id', '¿De que manera realizará el pago?', $tipo_pago, 'id', 'nombre', isset($detalle_unidad) ? $detalle_unidad->tipo_pago_id : null); ?>
            </div>
            <div class="col-md-4 container-forma-pago">
                <div class="form-group">
                    <label for="select">Plazo de credito</label>
                    <select name="plazo_credito_id" class="form-control " id="plazo_credito_id">
                        <option value=""> Seleccionar</option>
                        @foreach ($plazo_credito as $plazo)
                            @if (isset($detalle_unidad) && $detalle_unidad->plazo_credito_id == $plazo->id)
                                <option selected value="{{ $plazo->id }}"> {{ $plazo->nombre }} </option>
                            @else
                                <option value="{{ $plazo->id }}"> {{ $plazo->nombre }} </option>
                            @endif
                        @endforeach
                    </select>
                    <select name="plazo_credito_id" class="form-control d-none" id="plazo_credito">
                        <option value=""> Seleccionar</option>
                        @foreach ($plazo_credito as $plazo)
                            @if (isset($detalle_unidad) && $detalle_unidad->plazo_credito_id == $plazo->id)
                                <option selected value="{{ $plazo->id }}"> {{ $plazo->nombre }} </option>
                            @else
                                <option value="{{ $plazo->id }}"> {{ $plazo->cantidad_mes }} </option>
                            @endif
                        @endforeach
                    </select>
                    <div id='plazo_credito_id_error' class='invalid-feedback'></div>
                </div>
            </div>
            <div class="col-md-4 ">
                <?php renderInputText('text', 'enganche', 'Enganche', isset($detalle_unidad) ? $detalle_unidad->enganche : ''); ?>
            </div>
            <div class="col-md-4 container-forma-pago">
                <div class="form-group">
                    <label for="precio"
                        class="">Precio total a financiar</label>
                <div>
                    <h2 class="
                        text-danger">
                        <span>$</span>
                        <span id="total_con_enganche"> -- </span>
                        <span>MXN</span>
                        </h2>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-4 text-center">
            <div class="col rounded shadow shadow-sm px-0">
                <h3 class="mt-0 bg-secondary text-white py-2 rounded-top">
                    Plan de Pago:
                </h3>
                <div class="row">
                    <div class="col-md-6 text-center">
                        <span id="label_tipo_pago">Pago mensual:</span>
                        <h4>
                            <span>$</span>
                            <span class="text-danger" id="precio_mensualidad"> -- </span>
                            <span>MXN</span>
                        </h4>
                    </div>
                    <div class="col-md-6 text-center">
                        <span>Tasa fija:</span>
                        <h4 class="">
                            <span></span>
                            <span class=" text-danger"
                            id="label_tasa_fija"> -- </span>
                            <span>%</span>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="accordion" id="acordeonPagos">
                <div class="card">
                    <div class="card-header" id="detalle_pagos">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                            data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                            Detalle de pagos
                        </button>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="detalle_pagos"
                        data-parent="#acordeonPagos">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8 pl-3">
                                    <h3>Enganche</h3>
                                </div>
                                <div class="col-md-4 text-right">
                                    <h2 class="text-danger">
                                        $
                                        <label for="enganche" id="lbl_enganche">
                                            --
                                        </label>
                                    </h2>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 pl-3">
                                    <h3>Mensualidad</h3>
                                </div>
                                <div class="col-md-4 text-right">
                                    <h2 class="text-danger">
                                        $
                                        <label for="enganche" id="lbl_mensualidad">
                                            --
                                        </label>

                                    </h2>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 pl-3">
                                    <h3>Precio total a financiar</h3>
                                </div>
                                <div class="col-md-4 text-right">
                                    <h2 class="text-danger">
                                        $
                                        <label for="enganche" id="lbl_total_con_enganche">
                                            --
                                        </label>

                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="collapse_tabla_amortizacion">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Tabla de amortización
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="collapse_tabla_amortizacion"
                        data-parent="#acordeonPagos">
                        <div class="card-body">
                            <table class="table table-bordered" id="tabla_amortizacion" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Plazo de credito</th>
                                        <th>Saldo insoluto</th>
                                        <th>Pago total en periodo</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Plazo de credito</th>
                                        <th>Saldo insoluto</th>
                                        <th>Pago total en periodo</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-3 row justify-content-end">
        @if (!empty($detalle_unidad->id))            
            <button class="btn btn-success btn-lg" id="procesar_venta">
                <i class="fas fa-car"></i> Actualizar preventa 
            </button>
        @endif

    </div>
    </div>
@endsection

@section('scripts')
    <script>
        // nombre_usuario
        $(".container-forma-pago").hide();
        var tabla_amortizacion = $('#tabla_amortizacion').DataTable({
            // "order": [[ 0, "desc" ]],
            language: {
                url: PATH_LANGUAGE
            },
            "ajax": {
                url: base_url + "autos/ventas/ajax_tabla_amortizacion",
                type: 'POST',
                data: {
                    precio_venta: () => $('#precio_venta').val(),
                    plazo: () => $("#plazo_credito").text() ? $("#plazo_credito").text() : 0,
                    enganche: () => $("#enganche").val()
                }
            },
            columns: [{
                    'data': function(data) {
                        return data.id;
                    }
                },
                {
                    'data': function(data) {
                        return data.plazo_credito;
                    }
                },
                {
                    'data': function(data) {
                        return data.saldo_insoluto;
                    }
                },
                {
                    'data': function(data) {
                        return data.pago_periodo;
                    }
                }
            ]
        });


        $("#tipo_forma_pago_id").on("change", function() {
            let tipo_forma_pago_id = $("#tipo_forma_pago_id").val();
            if (tipo_forma_pago_id == 2) {
                $(".container-forma-pago").show();
            } else {
                $("#plazo_credito_id").val(null)
                $(".container-forma-pago").hide();
            }
        });

        $("#plazo_credito_id").on('change', function(e) {
            $("#plazo_credito").val($("#plazo_credito_id").val())
            calcular_pago();
        });


        let load_data_unidad = () => {
            let id_unidad = $("#id_unidad").val();
            let tasa = $("#tasa_interes").val();
            $("#label_tasa_fija").text(tasa);
            ajax.get(`api/unidades/${id_unidad}`, {}, (data, headers) => {
                $("#precio_number").text(data.precio_venta);
                calcular_pago();
                bloquear_campos();
            })
        };

        if ($("#id_unidad").val() && $("#id_unidad").val() !== ''&& $("#enganche").val() == '') {
            load_data_unidad();
        }

        // load_data_unidad();

        $("#tasa_interes").on('change', function(e) {
            let tasa = $("#tasa_interes").val();
            $("#label_tasa_fija").text(tasa);
        });

        $("#id_cliente").on('change', function(e) {
            let id_cliente = $("#id_cliente").val();
            ajax.get(`api/clientes/${id_cliente}`, {}, (data, headers) => {
                let response = data[0];
                $("#nombre_cliente").val(
                    `${response.nombre} ${response.apellido_materno} ${response.apellido_paterno}`);
                $("#rfc_cliente").val(response.rfc);
            })
        });

        let bloquear_campos = () => {
            let unidad_value = $("#id_unidad").val();
            if (unidad_value.length == 0) {
                return $("#tipo_forma_pago_id").attr('disabled', true);
            }
            return $("#tipo_forma_pago_id").attr('disabled', false);
        };

        if ($("#id_unidad").val()) {
            bloquear_campos();
        }

        let calcular_pago = (valor_enganche = null) => {
            let plazo = $("#plazo_credito").text();
            let precio = $("#precio_venta").val();
            if (plazo !== '' && plazo !== null) {
                let obtener_enganche = valor_enganche === null ? precio * 10 / 100 : valor_enganche;
                let precio_con_enganche = precio - obtener_enganche;
                $("#total_con_enganche").text(precio_con_enganche);
                $("#lbl_total_con_enganche").text(precio_con_enganche);

                $("#enganche").val(obtener_enganche);
                $("#lbl_enganche").text(obtener_enganche);

                let valor_mensualidad = Math.round(precio_con_enganche / plazo);
                $("#precio_mensualidad").text(valor_mensualidad);
                $("#lbl_mensualidad").text(valor_mensualidad);

                $("#label_tipo_pago").text("Pago mensual");
                tabla_amortizacion.ajax.reload();
                return false;
            }

            tabla_amortizacion.ajax.reload();
            $("#precio_number").text(precio);
            $("#precio_mensualidad").text(precio);
            $("#label_tipo_pago").text("Pago contado");
        }

        $("#enganche").on('keyup', function(e) {
            let precio = $("#precio_venta").val();
            let enganche_minimo = precio * 10 / 100;
            let valor_enganche = $("#enganche").val();
            // if (valor_enganche < enganche_minimo) {

            // }
            calcular_pago(valor_enganche);
        });

        $("#procesar_venta").on('click', function(e) {
            if ($("#id_cliente").val() == "") {
                toastr.error("Seleccionar o registrar cliente")
                return false;
            }

            if ($("#precio_venta").val() == "") {
                toastr.error("Indicar el precio de venta")
                return false;
            }

            if (!document.getElementById('id_unidad') && document.getElementById('descripcion_modelo')) {
                if (document.getElementById('descripcion_modelo').value == '') {
                    toastr.error("Proporciona una descripción de la unidad")
                    document.getElementById("descripcion_modelo").focus();
                    return false;
                }
            } else if (document.getElementById('id_unidad') && $("#id_unidad").val() == "") {
                toastr.error("Seleccionar unidad");
                return false;
            }

            if ($("#tipo_forma_pago_id").val() == "" || $("#tipo_pago_id").val() == '') {
                utils.displayWarningDialog("Seleccionar la forma de pago. ", "warning", function(data) {})
                return false;
            }

            let plazo_credito = $("#tipo_forma_pago_id").val() == 1 ? 14 : $("#plazo_credito_id").val();

            if ($("#tipo_forma_pago_id").val() == 2 && $("#plazo_credito_id").val() == '') {
                utils.displayWarningDialog("Indicar plazo de pago. ", "warning", function(data) {})
                return false;
            }

            if ($("#iva").val() > 100) {
                utils.displayWarningDialog("el valor de iva debe ser de 1 - 100", "warning", function(data) {})
                return false;
            }

            const id_venta = document.getElementById('id_venta').value;
            ajax.put(`api/venta-unidades/${id_venta}`, {
                "id_unidad": document.getElementById('id_unidad') ? $("#id_unidad").val() : null,
                "descripcion_modelo": document.getElementById('descripcion_modelo') ? $("#descripcion_modelo").val() : '',
                "id_cliente": $("#id_cliente").val(),
                "id_estatus": $("#id_estatus").val(),
                "id_tipo_auto": $("#id_tipo_auto").val(),
                "tipo_forma_pago_id": $("#tipo_forma_pago_id").val(),
                "tipo_pago_id": $("#tipo_pago_id").val(),
                "plazo_credito_id": plazo_credito,
                "moneda": $("#moneda").val(),
                "tasa_interes": $("#tasa_interes").val(),
                "enganche": $("#enganche").val(),
                "total": $("#precio_venta").val(),
                "impuesto_isan": $("#impuesto_isan").val(),
                "descuento": $("#descuento").val(),
                "id_asesor": $("#id_asesor").val(),
                "iva": $("#iva").val()
            }, (data, headers) => {
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }

                utils.displayWarningDialog("Preventa actualizada", "success", function(
                    datamodal) {
                    return window.location.href = base_url + `autos/ventas/index`;
                })
            })
        });
    </script>
@endsection
