<div class="col-md-6">
    <div class="row">
        <div class="col-md-12">
            <?php renderInputText("text", "tipo_venta", "Tipo de venta",  $info_ventas->tipo_venta == 1 ? 'CONTADO' : 'CREDITO', true); ?>
        </div>
        <div class="col-md-12">
            <?php renderInputText("date", "fecha_factura", "Fecha de factura",  isset($info_ventas->fecha_factura) ? $info_ventas->fecha_factura : ''); ?>
        </div>
        <div class="col-md-12">
            <?php renderInputText("date", "fecha_ultimo_ingreso", "Fecha ultimo ingreso",  isset($info_ventas->fecha_ultimo_ingreso) ? $info_ventas->fecha_ultimo_ingreso : ''); ?>
        </div>
        <div class="col-md-12">
            <?php renderInputText("date", "fecha_contrato", "Fecha contrato F.C",  isset($info_ventas->fecha_contrato) ? $info_ventas->fecha_contrato : ''); ?>
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="row">
        <div class="col-md-12">
            <?php renderInputText("date", "fecha_venta_smart", "Fecha de venta para smart",  isset($info_ventas->fecha_venta_smart) ? $info_ventas->fecha_venta_smart : ''); ?>
        </div>
        <div class="col-md-12">
            <?php renderInputText("date", "fecha_ingreso_smart", "Fecha ingreso en smart",  isset($info_ventas->fecha_ingreso_smart) ? $info_ventas->fecha_ingreso_smart : ''); ?>
        </div>
        <div class="col-md-12">
            <?php renderInputText("date", "fecha_carta_conauto", "Fecha carta conauto",  isset($info_ventas->fecha_carta_conauto) ? $info_ventas->fecha_carta_conauto : ''); ?>
        </div>
        <div class="col-md-12">
            <?php renderInputText("date", "fecha_orden_compra", "Fecha orden compra",  isset($info_ventas->fecha_orden_compra) ? $info_ventas->fecha_orden_compra : ''); ?>
        </div>
    </div>
</div>
<?php 
// dd($info_ventas);
?>
<div class="col-6 mt-4">
    @if( isset($info_ventas->firma_gerente) && strlen($info_ventas->firma_gerente) > 0)
    <center>
        <input type="hidden" id="firmaImgGerente" name="firmaImgGerente" value="{{ isset($info_ventas->firma_gerente) ?  $info_ventas->firma_gerente : '' }}">
        <img style="height:120px;width: 90%; border:1px solid #ddd" class="img-fluid img-firma-big" id="firmaImgGerente" onerror="setTimeout(() => { this.onerror(this) },200);" src="{{ isset($info_ventas->firma_gerente) ?  $info_ventas->firma_gerente : '' }}">
        <div class="sep10"></div>
        <b>FIRMA GERENTE </b>
    </center>
    @else
    <center>
        <input type="hidden" id="firmaImgGerente" name="firmaImgGerente" value="{{ isset($info_ventas->firma_gerente) ?  $info_ventas->firma_gerente : '' }}">
        <img onclick="this.firmar('firmaImgGerente')" style="height:120px;width: 90%; border:1px solid #ddd" class="img-fluid img-firma-big" id="firmaImgGerente" onerror="setTimeout(() => { this.onerror(this) },200);" src="{{ array_key_exists('firma',$data)? $data['firma']['firma_gerente'] : '' }}">
        <div class="sep10"></div>
        <b>FIRMA GERENTE <button type="button" onclick="firmar('firmaImgGerente')" class="btn btn-primary btn-sm"><i class="fa fa-pencil-alt"></i></button></b>
    </center>
    @endif
</div>
<div class="col-6 mt-4">
    @if( isset($info_ventas->firma_otros) && strlen($info_ventas->firma_otros) > 0 )
    <center>
        <input type="hidden" id="firmaOtro" name="firmaOtro" value="{{  isset($info_ventas->firma_otros) ?  $info_ventas->firma_otros : '' }}">
        <img style="height:120px;width: 90%; border:1px solid #ddd" class="img-fluid img-firma-big" id="firmaOtro" onerror="setTimeout(() => { this.onerror(this) },200);" src="{{  isset($info_ventas->firma_otros) ?  $info_ventas->firma_otros : '' }}">
        <div class="sep10"></div>
        <b>FIRMA OTROS </b>
    </center>
    @else
    <center>
        <input type="hidden" id="firmaOtro" name="firmaOtro" value="{{ isset($info_ventas->firma_otros) ?  $info_ventas->firma_otros : '' }}">
        <img onclick="this.firmar('firmaOtro')" style="height:120px;width: 90%; border:1px solid #ddd" class="img-fluid img-firma-big" id="firmaOtro" onerror="setTimeout(() => { this.onerror(this) },200);" src="{{  isset($info_ventas->firma_otros) ?  $info_ventas->firma_otros : '' }}">
        <div class="sep10"></div>
        <b>FIRMA OTROS <button type="button" onclick="firmar('firmaOtro')" class="btn btn-primary btn-sm"><i class="fa fa-pencil-alt"></i></button></b>
    </center>
    @endif
</div>
<div class="col-md-12 text-right">
    <a href="{{ base_url('autos/ventas/reportedocumentacion/'.$id_venta_unidad) }}" target="_blank" class="btn btn-primary">
        <i class="fa fa-file"></i> Generar Pdf
    </a>
    <button type="button" onclick="handleSaveInfoVenta()" class="btn btn-primary"> <i class="fa fa-save"></i> Guardar </button>
</div>