@layout('tema_luna/layout_iframes')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">
        <?php echo isset($modulo) ? $modulo : ''; ?>
    </h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : ''; ?>
        </li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : ''; ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : ''; ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="frmventa" class="row">
                @include('ventas/partial_fechas_factura')
                
            </form>
        </div>
        <div class="col-md-12"></div>
        <hr>
        <div class="col-6">
            @include('ventas/partial_documentacion_facturacion')
        </div>
        <div class="col-6">
            @include("ventas/partial_docs_flotilla_ford_conauto")

        </div>
    </div>

</div>

<!-- Modal para la firma eléctronica-->
<div class="modal" id="firmaDigital">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <canvas id="canvas" width="500" height="200" style='border: 1px solid #CCC;width: 100%;'>
                        Su navegador no soporta el firmado
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" onclick="limpiar();" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" onclick="convertCanvasToImage();" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    const handleUpload = (tipo_archivo = '', input_name) => {
        return handleSaveUpdate(tipo_archivo, input_name, 1)
    }

    const handleUploadUpdate = (tipo_archivo = '', input_name) => {
        return handleSaveUpdate(tipo_archivo, input_name, 2)
    }

    const handleSaveInfoVenta = () => {
        ajax.post('api/informaciondocumentosventa', {
            tipo_venta: document.getElementById('tipo_venta').value,
            id_venta_unidad: document.getElementById('id_venta_unidad').value,
            fecha_factura: document.getElementById('fecha_factura').value,
            fecha_ultimo_ingreso: document.getElementById('fecha_ultimo_ingreso').value,
            fecha_contrato: document.getElementById('fecha_contrato').value,
            fecha_carta_conauto: document.getElementById('fecha_carta_conauto').value,
            fecha_orden_compra: document.getElementById('fecha_orden_compra').value,
            fecha_venta_smart: document.getElementById('fecha_venta_smart').value,
            fecha_ingreso_smart: document.getElementById('fecha_ingreso_smart').value,
            // 
            firma_gerente: document.getElementById('firmaImgGerente').value,
            firma_otros: document.getElementById('firmaOtro').value
        }, function(response, header) {
            if (header.status == 200 || header.status == 204) {
                let titulo = "Informacion actualizada con exito!"
                utils.displayWarningDialog(titulo, 'success', function(result) {
                    return window.location.href = base_url +
                        "autos/ventas/documentacionpreventaunidad/" + document.getElementById('id_venta_unidad').value;
                });
            }

            if (header.status == 500) {
                let titulo = "Error guardando"
                utils.displayWarningDialog(titulo, 'warning', function(result) {});
            }
        })
    }

    const handleSaveUpdate = (tipo_archivo = '', input_name, save_update = 1) => {
        if (tipo_archivo == '') {
            return toastr.error("Indicar tipo de archivo");
        }

        if (!$('#' + input_name)[0].files[0]) {
            return toastr.error("Adjuntar documento");
        }

        let archivo = $('#' + input_name)[0].files[0];
        let paqueteDeDatos = new FormData();
        paqueteDeDatos.append('nombre_archivo', archivo);
        paqueteDeDatos.append('id_venta_unidad', document.getElementById('id_venta_unidad').value);
        paqueteDeDatos.append('id_tipo_documento', tipo_archivo);

        const url = save_update == 1 ? 'api/documentosventa' : 'api/documentosventa/actualizar';

        ajax.postFile(url, paqueteDeDatos, function(response, header) {
            if (header.status == 200 || header.status == 204) {
                let titulo = "Documento subido con exito!"
                utils.displayWarningDialog(titulo, 'success', function(result) {
                    return window.location.href = base_url +
                        "autos/ventas/documentacionpreventaunidad/" + document.getElementById(
                            'id_venta_unidad').value;
                });
            }

            if (header.status == 500) {
                let titulo = "Error subiendo factura!"
                utils.displayWarningDialog(titulo, 'warning', function(result) {});
            }
        })
    }

    let tag_firma = null;
    let tag_modal = null;

    function firmar(dom_firma, dom_modal) {
        if (dom_modal != undefined) {
            this.tag_modal = dom_modal;
            $('div#' + this.tag_modal).modal('hide');
        } else {
            this.tag_modal = null;
        }
        $('#firmaDigital').modal();
        if (dom_modal != undefined) {
            $('#firmaDigital').on('hidden.bs.modal', function() {
                if (this.tag_modal != null) {
                    $('div#' + this.tag_modal).modal('show');
                }
            });
        }


        setTimeout(() => {
            this.tag_firma = dom_firma;


            var canvas = document.getElementById("canvas");
            ctx = canvas.getContext("2d");
            cw = canvas.width = $('canvas#canvas').width(),
                cx = cw / 2;
            ch = canvas.height = $('canvas#canvas').height(),
                cy = ch / 2;

            dibujar = false;
            factorDeAlisamiento = 5;
            Trazados = [];
            puntos = [];
            ctx.lineJoin = "round";

            canvas.addEventListener('mousedown', function(evt) {
                dibujar = true;
                //ctx.clearRect(0, 0, cw, ch);
                puntos.length = 0;
                ctx.beginPath();

            }, false);

            canvas.addEventListener('mouseup', function(evt) {
                redibujarTrazados();
            }, false);

            canvas.addEventListener("mouseout", function(evt) {
                redibujarTrazados();
            }, false);

            canvas.addEventListener("mousemove", function(evt) {
                if (dibujar) {
                    var m = oMousePos(canvas, evt);
                    puntos.push(m);
                    ctx.lineTo(m.x, m.y);
                    ctx.stroke();
                }
            }, false);

        }, 50);
    }

    function onerror($this) {
        $this.onerror = null;
        $this.src = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'
    }
    // ---------------------------------------------------------
    function limpiar() {
        dibujar = false;
        ctx.clearRect(0, 0, cw, ch);
        Trazados.length = 0;
        puntos.length = 0;
    }

    function reducirArray(n, elArray) {
        var nuevoArray = [];
        nuevoArray[0] = elArray[0];
        for (var i = 0; i < elArray.length; i++) {
            if (i % n == 0) {
                nuevoArray[nuevoArray.length] = elArray[i];
            }
        }
        nuevoArray[nuevoArray.length - 1] = elArray[elArray.length - 1];
        Trazados.push(nuevoArray);
    }

    function calcularPuntoDeControl(ry, a, b) {
        var pc = {}
        pc.x = (ry[a].x + ry[b].x) / 2;
        pc.y = (ry[a].y + ry[b].y) / 2;
        return pc;
    }

    function alisarTrazado(ry) {
        if (ry.length > 1) {
            var ultimoPunto = ry.length - 1;
            ctx.beginPath();
            ctx.moveTo(ry[0].x, ry[0].y);
            for (i = 1; i < ry.length - 2; i++) {
                var pc = this.calcularPuntoDeControl(ry, i, i + 1);
                ctx.quadraticCurveTo(ry[i].x, ry[i].y, pc.x, pc.y);
            }
            ctx.quadraticCurveTo(ry[ultimoPunto - 1].x, ry[ultimoPunto - 1].y, ry[ultimoPunto].x, ry[ultimoPunto].y);
            ctx.stroke();
        }
    }


    function redibujarTrazados() {
        dibujar = false;
        ctx.clearRect(0, 0, cw, ch);
        this.reducirArray(factorDeAlisamiento, puntos);
        for (var i = 0; i < Trazados.length; i++)
            this.alisarTrazado(Trazados[i]);
    }

    function oMousePos(canvas, evt) {
        var ClientRect = canvas.getBoundingClientRect();
        return { //objeto
            x: Math.round(evt.clientX - ClientRect.left),
            y: Math.round(evt.clientY - ClientRect.top)
        }
    }

    function convertCanvasToImage() {
        let canvas = document.getElementById("canvas");
        // let image = new Image();
        // image.src = canvas.toDataURL();
        // console.log(image);

        $('img#' + this.tag_firma).attr('src', canvas.toDataURL());
        $('input[name=' + this.tag_firma + ']').val(canvas.toDataURL());

        // return image;
    }
</script>
@endsection