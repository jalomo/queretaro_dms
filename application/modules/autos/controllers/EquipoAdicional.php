<?php

defined('BASEPATH') or exit('No direct script access allowed');

class EquipoAdicional extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time', 300);
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function index()
    {
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Equipo adicional";
        $data['titulo'] = "Agregar equipo";
        $data['subtitulo'] = "Lista unidades en pre venta";

        $this->blade->render('equipoAdicional/index_equipo_adicional', $data);
    }

    public function unidadesParaEquipoOpcional()
    {
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Equipo adicional";
        $data['titulo'] = "Unidades";
        $data['subtitulo'] = "Lista con equipo adicional";

        $this->blade->render('equipoAdicional/unidades_equipo', $data);
    }

    public function agregarEquipo($id_preventa_unidad = '')
    {
        $data['modulo'] = "Equipo adicional";
        $data['submodulo'] = "Inventario";
        $data['titulo'] = "Agregar equipo";
        $data['subtitulo'] = "Lista unidades";

        $detalle_venta = $this->curl->curlGet('api/venta-unidades/' . $id_preventa_unidad);
        $unidad = procesarResponseApiJsonToArray($detalle_venta);
        $data['detalle_unidad'] = count($unidad) > 0 ? $unidad[0] : [];
        $this->blade->render('equipoAdicional/agregar_equipo', $data);
    }


    public function ajax_detalle_equipo_carrito()
    {
        if ($this->input->post('preventa_id') == '') {
            $data['data'] = [];
            echo json_encode($data);
            die();
        }

        $detalleVenta = $this->curl->curlGet('api/autos/equipo-opcional/' . $this->input->post('preventa_id'));
        $detalle = procesarResponseApiJsonToArray($detalleVenta);
        $data['data'] = isset($detalle) ? $detalle : [];
        echo json_encode($data);
    }
}
