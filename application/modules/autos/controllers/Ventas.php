<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Ventas extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time', 300);
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function index()
    {
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Venta";
        $data['titulo'] = "Pre pedidos";
        $data['subtitulo'] = "listado";

        $catalogo_remisiones = $this->curl->curlGet('api/unidades/getbyparameters?estatus_id=1');
        $remisiones = procesarResponseApiJsonToArray($catalogo_remisiones);
        $data['cat_remisiones'] = !empty($remisiones) ? $remisiones : [];
        $this->blade->render('ventas/listado', $data);
    }

    public function detalle_venta($id_venta = '')
    {
        $data['id_tab'] = 1;
        $catalogos = $this->returnCatalogosForVentas();
        // utils::pre($catalogos);
        $data['cat_cfdi'] = $catalogos['cat_cfdi'];
        $data['nombre_usuario'] =  $catalogos['nombre_usuario'];
        $data['id_asesor'] = $catalogos['id_asesor'];
        $data['catalogo_clientes'] = $catalogos['catalogo_clientes'];
        $data['plazo_credito'] = $catalogos['plazo_credito'];
        $data['tipo_forma_pago'] = $catalogos['tipo_forma_pago'];
        $data['tipo_pago'] = $catalogos['tipo_pago'];

        $data['id_tipo_auto'] = 1; //seminuevo

        if ($id_venta != '') {
            $detalle_venta = $this->curl->curlGet('api/venta-unidades/' . $id_venta);
            $unidad = procesarResponseApiJsonToArray($detalle_venta);
            $data['detalle_unidad'] = count($unidad) > 0 ? $unidad[0] : [];
        }

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Ventas";
        $data['titulo'] = "Pre-venta";
        $data['subtitulo'] = "Registro";
        $this->blade->render('ventas/detalle_venta', $data);
    }

    public function returnCatalogosForVentas()
    {
        $cfdi = $this->curl->curlGet('api/cfdi');
        $clientes = $this->curl->curlGet('api/clientes');
        $plazo_credito = $this->curl->curlGet('api/catalogo-plazo-credito');
        $tipo_forma_pago = $this->curl->curlGet('api/catalogo-tipo-forma-pago');
        $tipo_pago = $this->curl->curlGet('api/tipo-pago');

        $data['nombre_usuario'] = $this->session->userdata('nombre') . " " . $this->session->userdata('apellido_paterno') . " " . $this->session->userdata('apellido_materno');
        $data['id_asesor'] = $this->session->userdata('id');

        $data['cat_cfdi'] = procesarResponseApiJsonToArray($cfdi);
        $data['catalogo_clientes'] = procesarResponseApiJsonToArray($clientes);
        $data['plazo_credito'] = procesarResponseApiJsonToArray($plazo_credito);;
        $data['tipo_forma_pago'] = procesarResponseApiJsonToArray($tipo_forma_pago);
        $data['tipo_pago'] = procesarResponseApiJsonToArray($tipo_pago);
        return $data;
    }

    public function iframepreventa($id_venta_unidad = '')
    {

        if ($id_venta_unidad == '') {
            return $this->index();
        }

        $data['modulo'] = "Checklist";
        $data['submodulo'] = "Venta";
        $data['titulo'] = "Pre-pedidos";
        $data['subtitulo'] = "Documentación";
        $data['id_venta_unidad'] = $id_venta_unidad;

        $dataInfoVentaDocs = $this->curl->curlGet('api/informaciondocumentosventa/byidventaunidad/' . $id_venta_unidad);
        $datainfoVentas = procesarResponseApiJsonToArray($dataInfoVentaDocs);
        $data['info_ventas'] = isset($datainfoVentas) ? $datainfoVentas : [];

        $dataFromApi = $this->curl->curlGet('api/documentosventa/byidunidad/' . $id_venta_unidad);
        $dataregistros = procesarResponseApiJsonToArray($dataFromApi);
        $data['archivos_subidos'] = isset($dataregistros) ? (array)$dataregistros : [];
        $this->blade->render('ventas/iframe_documentacion', $data);
    }

    public function documentacionpreventaunidad($id_venta_unidad = '')
    {
        if ($id_venta_unidad == '') {
            return $this->index();
        }

        $data['id_tab'] = 2;
        $data['modulo'] = "Documentación";
        $data['submodulo'] = "Venta";
        $data['titulo'] = "Pre venta";
        $data['subtitulo'] = "Documentación";
        $data['id_venta_unidad'] = $id_venta_unidad;

        $dataInfoVentaDocs = $this->curl->curlGet('api/informaciondocumentosventa/byidventaunidad/' . $id_venta_unidad);
        $datainfoVentas = procesarResponseApiJsonToArray($dataInfoVentaDocs);
        $data['info_ventas'] = isset($datainfoVentas) ? $datainfoVentas : [];
        // dd($data['info_ventas']);
        $dataFromApi = $this->curl->curlGet('api/documentosventa/byidunidad/' . $id_venta_unidad);
        $dataregistros = procesarResponseApiJsonToArray($dataFromApi);
        $data['archivos_subidos'] = isset($dataregistros) ? (array)$dataregistros : [];
        // dd($data);
        $this->blade->render('ventas/documentacion', $data);
    }

    public function reportedocumentacion($id_venta_unidad)
    {
        $dataInfoVentaDocs = $this->curl->curlGet('api/informaciondocumentosventa/byidventaunidad/' . $id_venta_unidad);
        $datainfoVentas = procesarResponseApiJsonToArray($dataInfoVentaDocs);
        $data['info_ventas'] = isset($datainfoVentas) ? $datainfoVentas : [];
        // dd($data['info_ventas']);
        $dataFromApi = $this->curl->curlGet('api/documentosventa/byidunidad/' . $id_venta_unidad);
        $dataregistros = procesarResponseApiJsonToArray($dataFromApi);
        $data['archivos_subidos'] = isset($dataregistros) ? (array)$dataregistros : [];

        // $this->load->view('reportes/pdfChecklist', $data);
        $view = $this->load->view('reportes/pdfChecklist', $data, true);
        return $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
    }

    public function unidades()
    {

        $api_response = $this->curl->curlGet('api/unidades/resumen-contabilidad');
        $data_response = procesarResponseApiJsonToArray($api_response);
        $data['total_unidades'] = isset($data_response) ? $data_response[0] : [];

        $dataFromApi = $this->curl->curlGet('api/unidades');
        $dataregistros = procesarResponseApiJsonToArray($dataFromApi);
        $data['data'] = isset($dataregistros) ? $dataregistros : [];
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Ventas";
        $data['titulo'] = "Unidades";
        $data['subtitulo'] = "Unidades disponibles";

        $this->blade->render('ventas/catalogo_unidades', $data);
    }

    public function ajax_preventas()
    {
        $query = [];
        if ($this->input->post('fecha_inicio')) {
            $query['fecha_inicio'] = $this->input->post('fecha_inicio');
        }
        if ($this->input->post('fecha_fin')) {
            $query['fecha_fin'] = $this->input->post('fecha_fin');
        }

        $responseData = $this->curl->curlGet('api/autos/pre-pedidos?' . http_build_query($query));
        $response = procesarResponseApiJsonToArray($responseData);
        $data['data'] = isset($response) ? $response : [];
        echo json_encode($data);
    }

    public function ajax_preventaEquipoOpcional()
    {
        $query = [];
        if ($this->input->post('fecha_inicio')) {
            $query['fecha_inicio'] = $this->input->post('fecha_inicio');
        }
        if ($this->input->post('fecha_fin')) {
            $query['fecha_fin'] = $this->input->post('fecha_fin');
        }

        $responseData = $this->curl->curlGet('api/autos/pre-pedidos/preventa-equipo-opcional?' . http_build_query($query));
        $response = procesarResponseApiJsonToArray($responseData);
        $data['data'] = isset($response) ? $response : [];
        echo json_encode($data);
    }

    public function preventa_formulario()
    {
        $id_unidad = $this->input->get('id_unidad');
        $data['id_tab'] = 1;
        $catalogos = $this->returnCatalogosForVentas();
        $data['cat_cfdi'] = $catalogos['cat_cfdi'];
        $data['nombre_usuario'] =  $catalogos['nombre_usuario'];
        $data['id_asesor'] = $catalogos['id_asesor'];
        $data['catalogo_clientes'] = $catalogos['catalogo_clientes'];
        $data['plazo_credito'] = $catalogos['plazo_credito'];
        $data['tipo_forma_pago'] = $catalogos['tipo_forma_pago'];
        $data['tipo_pago'] = $catalogos['tipo_pago'];

        $data['id_tipo_auto'] = 1; //seminuevo

        if ($id_unidad != '') {
            $detalle_unidad = $this->curl->curlGet('api/unidades/' . $id_unidad);
            $unidad = procesarResponseApiJsonToArray($detalle_unidad);
            $data['detalle_unidad'] = count($unidad) > 0 ? $unidad[0] : [];
        } else {
            // $cat_modelo = $this->curl->curlGet('api/cat-lineas');
            // $data['cat_lineas'] = !empty($cat_modelo) ? procesarResponseApiJsonToArray($cat_modelo) : [];
            $data['detalle_unidad'] = [];
        }

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Ventas";
        $data['titulo'] = "Pre-venta";
        $data['subtitulo'] = "Registro";
        $this->blade->render('ventas/preventa_alta', $data);
    }



    public function ajax_tabla_amortizacion()
    {
        if ($this->input->post('precio_venta') == '') {
            echo json_encode(['data' => []]);
            return;
        }

        // utils::pre($this->input->post());
        $precio_venta = $this->input->post('precio_venta');
        $plazo = $this->input->post('plazo');
        $enganche = $this->input->post('enganche');
        $detalle = [];

        if ($plazo > 0) {
            $total = $precio_venta - $enganche;
            $mensualidades = number_format($total / $plazo, 2, '.', '');

            for ($i = 1; $i <=  $plazo; $i++) {
                $monto_total_actual = $mensualidades * $i - $total;

                $detalle[] = [
                    'id' => $i,
                    'plazo_credito' => "Mensualidad {$i}",
                    'saldo_insoluto' => "$ ". number_format(abs($monto_total_actual), 2, '.', ''),
                    'pago_periodo' => "$". $mensualidades,
                ];
            }

            echo json_encode([
                'data' => $detalle 
            ]);

            return;
        }

        $detalle[] = [
            'plazo_credito' => "Contado ",
            'saldo_insoluto' => "$ " . 0,
            'id' =>  1,
            'pago_periodo' => "$ " . $precio_venta,
        ];
        
        echo json_encode([
            'data' => $detalle 
        ]);
    }

    public function entrega_unidad($id = '')
    {
        $data['data'] = [];
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Ventas";
        $data['titulo'] = "Entrega de Unidades";
        $data['subtitulo'] = "Entrega";

        $catalogos = $this->returnCatalogosForVentas();
        // utils::pre($catalogos);
        $data['cat_cfdi'] = $catalogos['cat_cfdi'];
        $data['nombre_usuario'] =  $catalogos['nombre_usuario'];
        $data['id_asesor'] = $catalogos['id_asesor'];
        $data['catalogo_unidades'] = $catalogos['catalogo_unidades'];
        $data['catalogo_modelos'] = $catalogos['catalogo_modelos'];
        $data['catalogo_marcas'] = $catalogos['catalogo_marcas'];
        $data['catalogo_anio'] = $catalogos['catalogo_anio'];
        $data['catalogo_color'] = $catalogos['catalogo_color'];
        $data['catalogo_clientes'] = $catalogos['catalogo_clientes'];
        $data['plazo_credito'] = $catalogos['plazo_credito'];
        $data['tipo_forma_pago'] = $catalogos['tipo_forma_pago'];
        $data['tipo_pago'] = $catalogos['tipo_pago'];

        $detalle_venta = $this->curl->curlGet('api/venta-unidades/' . $id);
        $data['venta'] = procesarResponseApiJsonToArray($detalle_venta);

        $this->blade->render('ventas/preventa_alta', $data);
    }

    public function pdfcontrato($id_venta_unidad = '')
    {

        $detalle_venta = $this->curl->curlGet('api/contrato-venta/venta/' . $id_venta_unidad);
        if (empty(procesarResponseApiJsonToArray($detalle_venta))) {
            die("no hay respuesta de la api");
        }

        $validate_response = procesarResponseApiJsonToArray($detalle_venta);
        $data['venta'] = $validate_response;

        $equipo_opcional = $this->curl->curlGet('api/autos/equipo-opcional/' . $id_venta_unidad);
        $valida_equipo_opcional = !empty(procesarResponseApiJsonToArray($equipo_opcional)) ? procesarResponseApiJsonToArray($equipo_opcional) : [];
        $data['equipo_opcional'] = $valida_equipo_opcional;

        $view = $this->load->view('ventas/contratos/contrato_compraventa',  $data, true);
        return $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
        // dd($data);
        // $this->load->view('ventas/contratos/contrato_compraventa', $data);
    }

    public function contratoventa($id_venta_unidad = '')
    {
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Ventas";
        $data['titulo'] = "Unidades";
        $data['subtitulo'] = "Contrato de Compra Venta";
        $data['id_tab'] = 5;
        $detalle_venta = $this->curl->curlGet('api/contrato-venta/venta/' . $id_venta_unidad);
        if (empty(procesarResponseApiJsonToArray($detalle_venta))) {
            die("no hay respuesta de la api");
        }
        $validate_response = procesarResponseApiJsonToArray($detalle_venta);
        $data['venta'] = $validate_response;

        $equipo_opcional = $this->curl->curlGet('api/autos/equipo-opcional/' . $id_venta_unidad);
        $valida_equipo_opcional = !empty(procesarResponseApiJsonToArray($equipo_opcional)) ? procesarResponseApiJsonToArray($equipo_opcional) : [];
        $data['equipo_opcional'] = $valida_equipo_opcional;


        $data['nombre_cliente'] = $validate_response->cliente_apellido_paterno . ' ' . $validate_response->cliente_apellido_materno . ' ' . $validate_response->nombre_cliente;
        $this->blade->render('ventas/contratos/contrato_credito_contado', $data);
    }
}
