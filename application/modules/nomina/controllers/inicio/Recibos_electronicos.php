<?php defined('BASEPATH') or exit('No direct script access allowed');

class Recibos_electronicos extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/inicio/recibos_electronicos/');
        $this->breadcrumb = array(
            'Nomina',
            'e Recibos',
            array('name'=>'Recibos electrónicos','url'=>site_url('nomina/inicio/faltas'))
        );
    }

    public function index($id_tipo_perdiodo = 1 , $id_periodo = false){
        $this->scripts[] = script_tag($this->pathScript.'index.js');

        $this->load->model('General_model');
        $tipo_periodos = $this->General_model->call_api('catalogos/tipo_periodos/store',array(),'get');
        $periodos = $this->General_model->call_api('catalogos/periodos/periodos_cerrados_anio',array('id_tipo_periodo'=>$id_tipo_perdiodo),'get');
        $dataContent = array(
            'id_tipo_perdiodo' => $id_tipo_perdiodo,
            'tipo_periodos' => $tipo_periodos['data'],
            'id_perdiodo' => $id_periodo,
            'perdiodo' => $periodos['data']
        );

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/recibos_electronicos/index', $dataContent,true);
        
        $this->output($html);
    }

    public function get_periodos(){
        $id_tipo_perdiodo = $this->input->get('id_tipo_periodo');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('catalogos/periodos/periodos_cerrados_anio',array('id_tipo_periodo'=>$id_tipo_perdiodo),'get');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;

        $this->response($dataForm,$http_estatus);
    }

    public function index_get()
    {
        $id_periodo = $this->input->get_post('id_periodo');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('catalogos/periodo_pago/recibo_electronico',array('id_periodo'=>$id_periodo),'get');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;

        $this->response($dataForm,$http_estatus);
    }

    public function detalle($id_trabajadorpago,$id_trabajador,$id_periodo){
        $html = $this->detalle_contenido($id_trabajadorpago,$id_trabajador,$id_periodo);
        $this->output($html);
    }
    
    public function detalle_contenido($id_trabajadorpago,$id_trabajador,$id_periodo){

        $this->scripts[] = script_tag($this->pathScript.'detalle.js');
        $this->breadcrumb[] = 'Detalle';

        $this->load->library('NumeroALetras');
        $this->load->model('General_model');
        $contenido = $this->General_model->call_api('configuracion/empresa/store_find',array('id'=>1),'get');

        $trabajador = $this->General_model->call_api('nomina/trabajador/datosgenerales/store_find',array('id'=>$id_trabajador),'get');
        $salario = $this->General_model->call_api('nomina/trabajador/salario/store_find',array('id_Trabajador'=>$id_trabajador),'get');
        $periodo = $this->General_model->call_api('periodos/store_find',array('id'=>$id_periodo),'get');

        $percepciones = $this->General_model->call_api('procesos/trabajador/conceptos_pago/store_findAll',array('id_Trabajador'=>$id_trabajador,'id_Periodo'=>$id_periodo,'ClavePD'=>'P'),'get');
        $deducciones = $this->General_model->call_api('procesos/trabajador/conceptos_pago/store_findAll',array('id_Trabajador'=>$id_trabajador,'id_Periodo'=>$id_periodo,'ClavePD'=>'D'),'get');
        $pago = $this->General_model->call_api('procesos/trabajador/pago/store_find',array('id_Trabajador'=>$id_trabajador,'id_Periodo'=>$id_periodo,'ClavePD'=>'D'),'get');
        // utils::pre($trabajador);
        $content = array(
            'empresa' => $contenido['data'],
            'trabajador' => $trabajador['data'],
            'salario' => $salario['data'],
            'periodo' => $periodo['data'],
            'percepciones' => $percepciones['data'],
            'deducciones' => $deducciones['data'],
            'pago' => $pago['data'],
        );
        // return $content;
        //utils::pre($content);

        $this->load->library('parser');
        return $this->parser->parse('/inicio/recibos_electronicos/detalle', $content,true);

    }

    public function xml($id_trabajadorpago,$id_trabajador,$id_periodo){

        $this->load->model('General_model');
        $form = $this->General_model->call_api('cfdi/timbrado/store_find',array('id_trabajadorpago'=>$id_trabajadorpago));
        $data =$form['data']['xml'];
        $name = 'Comprobante.xml';
        $this->load->helper('download');
        force_download($name, $data);
        exit();
        
    }

    public function recibo($id_trabajadorpago,$id_trabajador,$id_periodo){

        $contenido_datos = $this->detalle_contenido($id_trabajadorpago,$id_trabajador,$id_periodo);
        
        // $hmtl = $this->load->view('/inicio/recibos_electronicos/recibo_pdf',$contenido_datos,true);
       
        $this->load->library('curl');
        $this->curl->curldownloadPdf('api/pdf', base64_encode($contenido_datos));
        exit;
    }

    public function obtener_pdf(){
        
        
        $this->load->library('parser');
        $markdownText = $this->parser->parse('/inicio/recibos_electronicos/recibo_pdf',false,true);
        
        $this->load->library('markdown');
        $contenido = array(
            'html' => $this->markdown->parse($markdownText)
        );

        
        $html = $this->load->view('/markdown_pdf', $contenido,true);
        
        $this->load->library('curl');
        $this->curl->curldownloadPdf('api/pdf', base64_encode($html));
        exit;

    }

    public function timbrado(){
        
        $id_trabajador = $this->input->get_post('id_Trabajador');
        $id_periodo = $this->input->get_post('id_Periodo');
        
        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('cfdi/timbrado/enviar',array('id_periodo'=>$id_periodo,'id_trabajador'=>$id_trabajador),'get');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;

        $this->response($dataForm,$http_estatus);

    }

}