<?php defined('BASEPATH') or exit('No direct script access allowed');

class Trabajador extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = 'js/nomina/inicio/trabajadores/trabajador/';
        $this->breadcrumb = array(
            'Nomina',
            'Trabajadores',// array('name'=>'Trabajadores','url'=>site_url('nomina/inicio/trabajadoresMenu')),
            array('name'=>'Trabajador','url'=>site_url('nomina/inicio/trabajador'))
        );
    }

    public function index()
    {
        $this->scripts[] = script_tag($this->pathScript.'index.js');
        $this->breadcrumb[] = 'Menú';
        $this->title = 'Trabajador';
        $html = $this->load->view('/inicio/trabajador/index',false,true);
        $this->output($html);
    }

    public function form($form){
        $this->load->helper('form');
        if(is_array($form)){
            foreach ($form as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    if($value2['key'] != 'id_Trabajador' && $value2['key'] != 'Clave'){
                        if($value2['input']['catalog'] != false){
                            $opciones = array();
                            foreach ($value2['input']['catalog'] as $value3) {
                                if(array_key_exists('Clave',$value3)){
                                    $opciones[$value3['id']] = $value3['Clave'].' - '.$value3['Descripcion'];
                                }else{
                                    $opciones[$value3['id']] = $value3['Descripcion'];
                                }        
                            }
                            $form[$key][$key2]['input'] = form_dropdown( $value2['input']['name'], $opciones, $value2['input']['value'], 'id="'.$value2['input']['id'].'" class="'.$value2['input']['class'].'" attr-id="'.$value2['input']['value'].'"' );
                        }else{
                            $form[$key][$key2]['input'] = form_input($value2['input']);
                        }
                        $form[$key][$key2]['key'] = $value2['input']['id'];
                        $form[$key][$key2]['value'] = $value2['input']['value'];
                    }else{
                        unset($form[$key][$key2]);
                    }
                }
            }
        }
        return $form;
    }

    public function alta()
    {
        $this->breadcrumb[] = 'Alta';
        $this->title = 'Alta de trabajadores';
        $this->scripts[] = script_tag($this->pathScript.'alta.js');

        $this->load->model('General_model');
        $contenido_form = $this->General_model->form('nomina/trabajador/trabajadorcontrato/store_form',array(),'get');
        $contenido_form = $this->form($contenido_form);
        //utils::pre($contenido_form);

        $this->load->library('parser');
        $contrato_html = $this->parser->parse('/inicio/trabajador/trabajador_contrato_modal', $contenido_form,true);
        

        $dataContent = array(
            'identity' => false,
            'contrato' => $contrato_html
        );
        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/trabajador/form',$dataContent,true);
        $this->output($html);
    }

    public function alta_guardar()
    {
        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('nomina/trabajador/trabajadorcontrato/validate',$parametros,'post');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        $this->response($response,$code);
    }

    public function editar_get_imagen()
    {
        $parametros = $this->input->post('id');
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('nomina/trabajador/imagen_datos',array('id'=>$parametros),'get');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        $this->response($response,$code);
    }



    public function datos_generales()
    {
        $this->breadcrumb[] = 'Datos Generales';
        $this->title = 'Datos Generales';
        $this->scripts[] = script_tag($this->pathScript.'datos_generales.js');

        $dataContent = array(
            'identity' => $this->input->get('id')
        );
        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/trabajador/form',$dataContent,true);
        $this->output($html);
    }

    public function historico_trabajador()
    {
        $this->breadcrumb[] = 'Histórico del trabajador';
        $this->title = 'Salario';
        $this->scripts[] = script_tag($this->pathScript.'historico_trabajador/salarios.js');

        $identity = $this->input->get('id');
        
        $this->load->library('parser');
        $contentForm = array(
            'identity' => $identity,
            'tab_1' => 'active',
            'tab_2' => '',
            'tab_3' => '',
            'url_1' => site_url('nomina/inicio/trabajador/historico_trabajador?id='.$identity),
            'url_2' => site_url('nomina/inicio/trabajador/historico_trayectoria?id='.$identity),
            'url_3' => site_url('nomina/inicio/trabajador/historico_vacaciones_faltas?id='.$identity),
            'content_form' => $this->load->view('/inicio/parametrosnomina/historico_trabajador/salarios',false,true)
        );
        
        $html = $this->parser->parse('/inicio/parametrosnomina/historico_trabajador/form', $contentForm,true);
        $this->output($html);
    }

    public function historico_trabajador_get()
    {
        $identity = $this->input->get_post('identity');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('nomina/trabajador/hist_trab_salario/store_findAll',array('id_Trabajador'=>$identity),'get');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;

        $this->response($dataForm,$http_estatus);
    }

    public function historico_trayectoria()
    {
        $this->breadcrumb[] = 'Histórico del trabajador';
        $this->title = 'Trayectoria';
        $this->scripts[] = script_tag($this->pathScript.'historico_trabajador/trayectoria.js');

        $identity = $this->input->get('id');
        
        $this->load->library('parser');
        $contentForm = array(
            'identity' => $identity,
            'tab_1' => '',
            'tab_2' => 'active',
            'tab_3' => '',
            'url_1' => site_url('nomina/inicio/trabajador/historico_trabajador?id='.$identity),
            'url_2' => site_url('nomina/inicio/trabajador/historico_trayectoria?id='.$identity),
            'url_3' => site_url('nomina/inicio/trabajador/historico_vacaciones_faltas?id='.$identity),
            'content_form' => $this->load->view('/inicio/parametrosnomina/historico_trabajador/trayectoria',false,true)
        );
        
        $html = $this->parser->parse('/inicio/parametrosnomina/historico_trabajador/form', $contentForm,true);
        $this->output($html);
    }

    public function historico_trayectoria_get()
    {
        $identity = $this->input->get_post('identity');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('nomina/trabajador/hist_trab_trayectoria/store_findAll',array('id_Trabajador'=>$identity),'get');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;

        $this->response($dataForm,$http_estatus);
    }

    public function historico_vacaciones_faltas()
    {
        $this->breadcrumb[] = 'Histórico del trabajador';
        $this->title = 'Trayectoria';
        $this->scripts[] = script_tag($this->pathScript.'historico_trabajador/vacaciones_faltas.js');

        $identity = $this->input->get('id');
        
        $this->load->library('parser');
        $contentForm = array(
            'identity' => $identity,
            'tab_1' => '',
            'tab_2' => '',
            'tab_3' => 'active',
            'url_1' => site_url('nomina/inicio/trabajador/historico_trabajador?id='.$identity),
            'url_2' => site_url('nomina/inicio/trabajador/historico_trayectoria?id='.$identity),
            'url_3' => site_url('nomina/inicio/trabajador/historico_vacaciones_faltas?id='.$identity),
            'content_form' => $this->load->view('/inicio/parametrosnomina/historico_trabajador/vacaciones_faltas',false,true)
        );
        
        $html = $this->parser->parse('/inicio/parametrosnomina/historico_trabajador/form', $contentForm,true);
        $this->output($html);
    }



    public function familia_beneficiarios(){
        
        $this->breadcrumb[] = 'Familia y beneficiarios';
        $this->title = 'Familia';
        $this->scripts[] = script_tag($this->pathScript.'familia_beneficiarios/familia.js');

        $identity = $this->input->get('id');

        $this->load->library('parser');
        $contentForm = array(
            'identity' => $identity,
            'tab_1' => 'active',
            'tab_2' => '',
            'url_1' => site_url('nomina/inicio/trabajador/familia_beneficiarios?id='.$identity),
            'url_2' => site_url('nomina/inicio/trabajador/beneficiarios?id='.$identity),
            'content_form' => $this->load->view('/inicio/trabajador/familia_beneficiarios/familia',array('identity'=>$identity),true)
        );
        
        $html = $this->parser->parse('/inicio/trabajador/familia_beneficiarios/form', $contentForm,true);
        $this->output($html);
    }

    public function familia_alta() {

        $identity = $this->input->get_post('identity');

        $this->breadcrumb[] = array('name'=>'Familia y beneficiarios','url'=>site_url('nomina/inicio/trabajador/familia_beneficiarios?id='.$identity));
        $this->breadcrumb[] = 'Alta de familiar';

        $this->scripts[] = script_tag($this->pathScript.'familia_beneficiarios/familia_alta.js');

        $this->load->model('General_model');
        $form = $this->General_model->form('nomina/trabajador/datosfamilia/store_form');


        $this->load->helper('form');
        if(is_array($form)){
            foreach ($form as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    if($value2['key'] != 'id_Trabajador' && $value2['key'] != 'Clave'){
                        if($value2['input']['catalog'] != false){
                            $opciones = array();
                            foreach ($value2['input']['catalog'] as $value3) {
                                if(array_key_exists('Nombre',$value3)){
                                    $opciones[$value3['id']] = $value3['Nombre'];
                                }else{
                                    $opciones[$value3['id']] = $value3['Descripcion'];
                                }
                                
                            }

                            $form[$key][$key2]['input'] = form_dropdown( $value2['input']['name'], $opciones, $value2['input']['value'], 'id="'.$value2['input']['id'].'" class="'.$value2['input']['class'].'" attr-id="'.$value2['input']['value'].'"' );
                        }else{
                            $form[$key][$key2]['input'] = form_input($value2['input']);
                        }
                        $form[$key][$key2]['key'] = $value2['input']['id'];
                    }else{
                        unset($form[$key][$key2]);
                    }
                }
            }
        }

        if(is_array($form)){
            $form = array_merge($form,array('identity'=>$identity));
        }
        
        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/trabajador/familia_beneficiarios/familia_alta', $form,true);
        $this->output($html);
    }

    public function familia_beneficiarios_get()
    {
        $identity = $this->input->get_post('id');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('nomina/trabajador/datosfamilia/store_findAll',array('id_Trabajador'=>$identity),'get');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;

        $this->response($dataForm,$http_estatus);
    }

    public function familia_beneficiarios_post()
    {
        $params = $this->input->post();
        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('nomina/trabajador/datosfamilia/store',$params,'post');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;

        $this->response($dataForm,$http_estatus);
    }

    public function familia_editar() {

        $identity = $this->input->get_post('id');
        $trabajador = $this->input->get_post('trabajador');

        $this->breadcrumb[] = array('name'=>'Familia y beneficiarios','url'=>site_url('nomina/inicio/trabajador/familia_beneficiarios?id='.$identity));
        $this->breadcrumb[] = 'Editar familiar';

        $this->scripts[] = script_tag($this->pathScript.'familia_beneficiarios/familia_editar.js');

        $this->load->model('General_model');
        $form = $this->General_model->form('nomina/trabajador/datosfamilia/store_form',array('id'=>$identity));


        $this->load->helper('form');
        if(is_array($form)){
            foreach ($form as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    if($value2['key'] != 'id_Trabajador' && $value2['key'] != 'Clave'){
                        if($value2['input']['catalog'] != false){
                            $opciones = array();
                            foreach ($value2['input']['catalog'] as $value3) {
                                if(array_key_exists('Nombre',$value3)){
                                    $opciones[$value3['id']] = $value3['Nombre'];
                                }else{
                                    $opciones[$value3['id']] = $value3['Descripcion'];
                                }
                                
                            }

                            $form[$key][$key2]['input'] = form_dropdown( $value2['input']['name'], $opciones, $value2['input']['value'], 'id="'.$value2['input']['id'].'" class="'.$value2['input']['class'].'" attr-id="'.$value2['input']['value'].'"' );
                        }else{
                            $form[$key][$key2]['input'] = form_input($value2['input']);
                        }
                        $form[$key][$key2]['key'] = $value2['input']['id'];
                    }else{
                        unset($form[$key][$key2]);
                    }
                }
            }
        }

        if(is_array($form)){
            $form = array_merge($form,array('identity'=>$identity));
            $form = array_merge($form,array('trabajador'=>$trabajador));
        }
        
        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/trabajador/familia_beneficiarios/familia_editar', $form,true);
        $this->output($html);
    }

    public function familia_beneficiarios_put()
    {
        $params = $this->input->post();
        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('nomina/trabajador/datosfamilia/store',$params,'put');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;

        $this->response($dataForm,$http_estatus);
    }

    public function familia_beneficiarios_delete()
    {
        $id = $this->input->post('identity');
        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('nomina/trabajador/datosfamilia/store',array('id'=>$id),'delete');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;

        $this->response($dataForm,$http_estatus);
    }

    public function beneficiarios(){
        
        $this->breadcrumb[] = 'Familia y beneficiarios';
        $this->title = 'Familia';
        $this->scripts[] = script_tag($this->pathScript.'familia_beneficiarios/beneficiarios.js');

        $identity = $this->input->get('id');

        $this->load->library('parser');
        $contentForm = array(
            'identity' => $identity,
            'tab_1' => '',
            'tab_2' => 'active',
            'url_1' => site_url('nomina/inicio/trabajador/familia_beneficiarios?id='.$identity),
            'url_2' => site_url('nomina/inicio/trabajador/beneficiarios?id='.$identity),
            'content_form' => $this->load->view('/inicio/trabajador/familia_beneficiarios/beneficiarios',array('identity'=>$identity),true)
        );
        
        $html = $this->parser->parse('/inicio/trabajador/familia_beneficiarios/form', $contentForm,true);
        $this->output($html);
    }


    public function beneficiarios_get()
    {
        $identity = $this->input->get_post('id');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('nomina/trabajador/datosbeneficiarios/store_findAll',array('id_Trabajador'=>$identity),'get');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;

        $this->response($dataForm,$http_estatus);
    }

    public function beneficiario_alta() {

        $identity = $this->input->get_post('identity');

        $this->breadcrumb[] = array('name'=>'Familia y beneficiarios','url'=>site_url('nomina/inicio/trabajador/familia_beneficiarios?id='.$identity));
        $this->breadcrumb[] = 'Alta de beneficiario';

        $this->scripts[] = script_tag($this->pathScript.'familia_beneficiarios/beneficiario_alta.js');

        $this->load->model('General_model');
        $form = $this->General_model->form('nomina/trabajador/datosbeneficiarios/store_form');


        $this->load->helper('form');
        if(is_array($form)){
            foreach ($form as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    if($value2['key'] != 'id_Trabajador' && $value2['key'] != 'Clave'){
                        if($value2['input']['catalog'] != false){
                            $opciones = array();
                            foreach ($value2['input']['catalog'] as $value3) {
                                if(array_key_exists('Nombre',$value3)){
                                    $opciones[$value3['id']] = $value3['Nombre'];
                                }else{
                                    $opciones[$value3['id']] = $value3['Descripcion'];
                                }
                                
                            }

                            $form[$key][$key2]['input'] = form_dropdown( $value2['input']['name'], $opciones, $value2['input']['value'], 'id="'.$value2['input']['id'].'" class="'.$value2['input']['class'].'" attr-id="'.$value2['input']['value'].'"' );
                        }else{
                            $form[$key][$key2]['input'] = form_input($value2['input']);
                        }
                        $form[$key][$key2]['key'] = $value2['input']['id'];
                    }else{
                        unset($form[$key][$key2]);
                    }
                }
            }
        }

        if(is_array($form)){
            $form = array_merge($form,array('identity'=>$identity));
        }
        
        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/trabajador/familia_beneficiarios/beneficiario_alta', $form,true);
        $this->output($html);
    }

    public function beneficiarios_post()
    {
        $params = $this->input->post();
        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('nomina/trabajador/datosbeneficiarios/store',$params,'post');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;

        $this->response($dataForm,$http_estatus);
    }

    public function beneficiario_editar() {

        $identity = $this->input->get_post('id');
        $trabajador = $this->input->get_post('trabajador');

        $this->breadcrumb[] = array('name'=>'Familia y beneficiarios','url'=>site_url('nomina/inicio/trabajador/familia_beneficiarios?id='.$identity));
        $this->breadcrumb[] = 'Editar beneficiario';

        $this->scripts[] = script_tag($this->pathScript.'familia_beneficiarios/beneficiario_editar.js');

        $this->load->model('General_model');
        $form = $this->General_model->form('nomina/trabajador/datosbeneficiarios/store_form',array('id'=>$identity));


        $this->load->helper('form');
        if(is_array($form)){
            foreach ($form as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    if($value2['key'] != 'id_Trabajador' && $value2['key'] != 'Clave'){
                        if($value2['input']['catalog'] != false){
                            $opciones = array();
                            foreach ($value2['input']['catalog'] as $value3) {
                                if(array_key_exists('Nombre',$value3)){
                                    $opciones[$value3['id']] = $value3['Nombre'];
                                }else{
                                    $opciones[$value3['id']] = $value3['Descripcion'];
                                }
                                
                            }

                            $form[$key][$key2]['input'] = form_dropdown( $value2['input']['name'], $opciones, $value2['input']['value'], 'id="'.$value2['input']['id'].'" class="'.$value2['input']['class'].'" attr-id="'.$value2['input']['value'].'"' );
                        }else{
                            $form[$key][$key2]['input'] = form_input($value2['input']);
                        }
                        $form[$key][$key2]['key'] = $value2['input']['id'];
                    }else{
                        unset($form[$key][$key2]);
                    }
                }
            }
        }

        if(is_array($form)){
            $form = array_merge($form,array('identity'=>$identity));
            $form = array_merge($form,array('trabajador'=>$trabajador));
        }
        
        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/trabajador/familia_beneficiarios/beneficiario_editar', $form,true);
        $this->output($html);
    }

    public function beneficiario_editar_put()
    {
        $params = $this->input->post();
        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('nomina/trabajador/datosbeneficiarios/store',$params,'put');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;

        $this->response($dataForm,$http_estatus);
    }

    public function beneficiario_editar_delete()
    {
        $id = $this->input->post('identity');
        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('nomina/trabajador/datosbeneficiarios/store',array('id'=>$id),'delete');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;

        $this->response($dataForm,$http_estatus);
    }

}
