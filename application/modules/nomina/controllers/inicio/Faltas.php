<?php defined('BASEPATH') or exit('No direct script access allowed');

class Faltas extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/inicio/faltas/');
        $this->breadcrumb = array(
            'Nomina',
            'Movimientos',
            array('name'=>'Faltas','url'=>site_url('nomina/inicio/faltas'))
        );
    }

    public function index($id_trabajador = false){
        $this->scripts[] = script_tag($this->pathScript.'index.js');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('nomina/trabajador/datosgenerales/store',array(),'get');

        $dataContent = array(
            'trabajadores' => $dataForm['data'],
            'id_trabajador' => $id_trabajador
        );

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/faltas/index', $dataContent,true);
        
        $this->output($html);
    }

    public function form($form){

        $this->load->helper('form');
        if(is_array($form)){
            foreach ($form as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    if($value2['key'] != 'id_Trabajador' && $value2['key'] != 'Clave'){
                        if($value2['key'] == 'FechaAlta'){
                            $data = array(
                                'readonly' => 'readonly',
                                'type'  => '',
                                'name' => $value2['input']['name'],
                                'id'    => $value2['input']['name'],
                                'value' => $value2['input']['value'],
                                'class' => 'form-control-plaintext'
                            );
                            $form[$key][$key2]['input'] = form_input($data);
                        }elseif($value2['key'] == 'Observaciones'){

                            $data_text = array(
                                'name' => $value2['input']['name'],
                                'value' => $value2['input']['value'],
                                'class' => $value2['input']['class']
                            );
                            $form[$key][$key2]['input'] = form_textarea($data_text);

                        }elseif($value2['input']['catalog'] != false){
                            $opciones = array();
                            foreach ($value2['input']['catalog'] as $value3) {
                                if(array_key_exists('Clave',$value3)){
                                    $opciones[$value3['id']] = $value3['Clave'].' - '.$value3['Descripcion'];
                                }else{
                                    $opciones[$value3['id']] = $value3['Descripcion'];
                                }
                                
                            }

                            $form[$key][$key2]['input'] = form_dropdown( $value2['input']['name'], $opciones, $value2['input']['value'], 'id="'.$value2['input']['id'].'" class="'.$value2['input']['class'].'" attr-id="'.$value2['input']['value'].'"' );
                        }else{
                            $form[$key][$key2]['input'] = form_input($value2['input']);
                        }
                        $form[$key][$key2]['key'] = $value2['input']['id'];
                    }else{
                        unset($form[$key][$key2]);
                    }
                }
            }
        }
        return $form;
    }

    public function index_get()
    {
        $identity = $this->input->get_post('id');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('catalogos/faltas/store_findAll',array('id_Trabajador'=>$identity),'get');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;

        $this->response($dataForm,$http_estatus);
    }

    public function alta($id_trabajador){
        $this->scripts[] = script_tag($this->pathScript.'alta.js');
        $this->breadcrumb[] = 'Alta';

        $this->load->model('General_model');
        $trabajador = $this->General_model->call_api('nomina/trabajador/datosgenerales/store_find',array('id'=>$id_trabajador),'get');
        $falta = $this->General_model->form('catalogos/faltas/store_form',array(),'get');
        $dataForm = $this->form($falta);
        $dataForm['id_trabajador'] = $id_trabajador;
        $dataForm['trabajador'] = $trabajador['data'];

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/faltas/alta', $dataForm,true);
        
        $this->output($html);
    }

    public function alta_guardar(){

        $parametros = $this->input->post();
        $id_Trabajador = $this->input->post('id_Trabajador');
        $Fecha = $this->input->post('Fecha');
        
        $this->load->model('General_model');
        $datos = $this->General_model->call_api('catalogos/faltas/store_find',array('id_Trabajador'=>$id_Trabajador,'Fecha'=>$Fecha),'get');

        if($datos['data'] === false){
            $response = $this->General_model->call_api('catalogos/faltas/store',$parametros,'post');
            $code = (array_key_exists('code',$response))? $response['code'] : 200;
        }else{
            $code = 406;
            $response['status'] = 'error';
            $response['message']['Fecha'] = 'Ya se encuentra registrado';
        }
        $this->response($response,$code);
    }


    public function editar($id,$id_trabajador){
        
        $this->scripts[] = script_tag($this->pathScript.'editar.js');
        $this->breadcrumb[] = 'Editar';

        $this->load->model('General_model');
        $trabajador = $this->General_model->call_api('nomina/trabajador/datosgenerales/store_find',array('id'=>$id_trabajador),'get');
        $falta = $this->General_model->form('catalogos/faltas/store_form',array('id'=>$id),'get');
        $dataForm = $this->form($falta);
        $dataForm['id_trabajador'] = $id_trabajador;
        $dataForm['id'] = $id;
        $dataForm['trabajador'] = $trabajador['data'];

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/faltas/editar', $dataForm,true);
        
        $this->output($html);
    }

    public function editar_guardar(){

        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('catalogos/faltas/store',$parametros,'put');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        
        $this->response($response,$code);
    }

    public function delete(){

        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('catalogos/faltas/store',$parametros,'delete');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        
        $this->response($response,$code);
    }
}