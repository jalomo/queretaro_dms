<?php defined('BASEPATH') or exit('No direct script access allowed');

# ca_entidadesnacimiento
$this->forge->drop_table('ca_',TRUE);
$this->forge->add_field(array(
    'id' => array( 'type' => 'CHAR', 'constraint' => 36, 'unique' => TRUE ),
    'Clave' => array( 'type' => 'INT', 'constraint' => 11),
    'Descripcion' => array( 'type' => 'VARCHAR', 'constraint' => 250 )
));
$this->forge->add_key('id', TRUE);
$this->forge->create_table('ca_regimencontratotrabajo',true);

$data = array(
    array('id' => '','Clave'=>'','Descripcion' => 'Sueldos'),
    array('id' => '','Clave'=>'','Descripcion' => 'Jubilados'),
    array('id' => '','Clave'=>'','Descripcion' => 'Pensionados'),
    array('id' => '','Clave'=>'','Descripcion' => 'Asimilados Miembros Sociedades Coorporativas Produccion'),
    array('id' => '','Clave'=>'','Descripcion' => 'Asimilados Integrantes Sociedades Asociaciones Civiles'),
    array('id' => '','Clave'=>'','Descripcion' => 'Asimilados Miembros Consejo'),
    
);
$this->db_nomina->insert_batch('ca_regimencontratotrabajo', $data);