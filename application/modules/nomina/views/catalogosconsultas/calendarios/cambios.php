<script>
	var identity = "{identity}";

</script>

<div class="row mt-5">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-bordered" id="listado" width="100%" cellspacing="0"></table>
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<form id="modal_contenido">
					
				</form>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
				<button type="button" onclick="Apps.modal_guardar(this);" class="btn btn-success">Guardar</button>
			</div>
		</div>
	</div>
</div>


<script id="template_contenido" type="text/template">
  
<div class="form-group row">
    <label for="Periodo" class="col-sm-2 col-form-label">Periodo:</label>
    <div class="col-sm-10">
        <input type="number" readonly class="form-control-plaintext" value="{{ Periodo }}">
        <input type="hidden" id="Periodo" name="Periodo" value="{{ Periodo }}">
        <input type="hidden" id="id_Calendario" name="id_Calendario" value="{{ id_Calendario }}">
        <input type="hidden" id="id" name="id" name="Calendario" value="{{ id }}">
    </div>
</div>
<div class="form-group row">
    <label for="Inicio" class="col-sm-2 col-form-label">Inicio:</label>
    <div class="col-sm-10">
        <input type="date" class="form-control" onblur="Apps.CambioFechaInicio(this);" id="Inicio" name="Inicio" value="{{ Inicio }}">
    </div>
</div>
<div class="form-group row">
    <label for="Fin" class="col-sm-2 col-form-label">Fin:</label>
    <div class="col-sm-10">
        <input type="date" class="form-control" onblur="Apps.CambioFechaFin(this);"  id="Fin" name="Fin" value="{{ Fin }}">
    </div>
</div>
<div class="form-group row">
    <label for="Dias" class="col-sm-2 col-form-label">Días:</label>
    <div class="col-sm-10">
        <input type="number" readonly class="form-control-plaintext" id="Dias" name="Dias" value="{{ Dias }}">
    </div>
</div>

</script>