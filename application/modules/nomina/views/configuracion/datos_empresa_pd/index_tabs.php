<script>
	var id = "{id}";

</script>

<div class="row mt-2 mb-3">
	<div class="col-sm-12">
		<h3>Párametros del sistema</h3>
	</div>
</div>


<div class="row">
	<div class="col-sm-12">
		<nav class="nav nav-pills ">
			<a class="nav-item nav-link {tab_1}" href="{url_1}">Percepciones y deducciones</a>
			<!-- <a class="nav-item nav-link {tab_2}" href="{url_2}">Bases fiscales</a> -->
		</nav>
	</div>

	<div class="col-sm-12">
		<div class="mt-2">
			{content_form}
		</div>
	</div>
</div>
