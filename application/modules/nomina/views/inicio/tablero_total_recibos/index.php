<br/>
<div class="row">
    <div class="col-sm-12">
		<h3 class="mt-2 mb-3">Recibos Electrónicos</h3>
    </div>
</div>
<br/>

<script>
	var id_tipo_perdiodo = "{id_tipo_perdiodo}";
	var id_perdiodo = "{id_perdiodo}";
</script>

<div class="card">
	<div class="card-body">
		<form class="">
			<div class="row">
				<div class="col-sm-4 mb-2">
					<div class="form-group">
						<label for="id_tipo_periodo">Tipo de periodo:</label>
						<select onchange="Apps.buscar_periodos();" class="custom-select" id="id_tipo_periodo">
							<?php foreach ($tipo_periodos as $key => $value) { ?>
							<option <?php echo($id_tipo_perdiodo == $value['id'])? 'selected' : ''; ?>
								value="<?php echo $value['id']; ?>"><?php echo $value['Descripcion']; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>

				<div class="col-sm-4 mb-2">
					<div class="form-group">
						<label for="id_periodo">Periodo:</label>
						<select class="custom-select" id="id_periodo">
							<?php foreach ($perdiodo as $key => $value) { ?>
                                <option <?php echo($id_perdiodo == $value['id'])? 'selected' : ''; ?> value="<?php echo $value['id']; ?>"><?php echo utils::aFecha($value['FechaInicio'],true); ?> al <?php echo utils::aFecha($value['FechaFin'],true); ?></option>
							<?php } ?>
						</select>
					</div>
				</div>


				<div class="col-sm-4 mb-2">
					<div class="form-group">
						<label for="id_periodo">&nbsp;</label>
						<div class="col-sm-3">
						<button type="button" class="btn btn-primary mb-2" onclick="var opcion=$('select#periodo_trabajador option:selected').val(); Apps.cargarDatos(opcion);">Buscar</button>
					</div>
					</div>
				</div>

			</div>
		</form>
	</div>
</div>

<script>
<?php 
function colores_graph($expresion){
    $color = 'blue';
    switch ($expresion) {
        case 1:
            $color = '#ffd562';
            break;
        case 2:
            $color = '#4169E1';
            break;
        case 3:
            $color = '#7F1E57';
            break;
        case 4:
            $color = '#7d8525';
        case 5:
          $color = 'red';
            break;
    }
    return $color;
}
?>
    var contenido = [ <?php $cont = 1; foreach ($contenido as $key => $value) { echo '["'.$value['name'].'",'.$value['value'].',"color:'.colores_graph($cont).'"],';  $cont++; } ?>];
</script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

  

    google.charts.load('current', {packages:['corechart']});
      google.charts.setOnLoadCallback(drawStuff);

        function drawStuff() {
          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Pagos');
          data.addColumn('number', 'Montos');
          data.addColumn({type: 'string', role: 'style'});
          data.addRows(contenido);
          

         var options = {
            colors:['red','#009900'],
            title: 'Total de recibos por categoría',
            legend: 'none',
           
            vAxis: { format:'decimal'},
            height: 500,
            chartArea: {width: '95%', height: '80%'},
            vAxis: {textPosition: 'in', gridlines: {count: 10}, minorGridlines: {count: 10}, textStyle: {fontSize: 12}},
         };

         var chart = new google.visualization.ColumnChart(document.getElementById('piechart'));
         chart.draw(data, options);

      };
</script>

<div class="card mt-3">
  <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <div id="chart_wrap">
                    <div id="piechart"></div>
                </div>
            </div>
        </div>
  </div>
</div>
