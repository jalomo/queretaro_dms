<div class="row">
	<div class="col-sm-12">

		<div class="card">
			<div class="card-body">

				<table id="table_content" class="table">
					<thead>
						<tr>
							<th scope="col">Descripción</th>
							<th scope="col">Tipo de acumulado</th>
							<th scope="col">Periodo actual</th>
							<th scope="col">Periodo anterior</th>
							<th scope="col">Anual</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
					<tfoot>
						<tr>
							<th scope="col">Descripción</th>
							<th scope="col">Tipo de acumulado</th>
							<th scope="col">Periodo actual</th>
							<th scope="col">Periodo anterior</th>
							<th scope="col">Anual</th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>

	</div>

</div>
