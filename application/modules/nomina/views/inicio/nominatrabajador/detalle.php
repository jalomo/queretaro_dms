<script>
	var identity = "{identity}";
	var periodo = "{periodo}";
</script>

<div class="row mt-2 mb-3">
	<div class="col-sm-12">
		<h3>Detalle de nómina del trabajador</h3>
	</div>
</div>

<div class="row mb-4 mt-3">
    <div class="col-sm-12">
        <nav class="nav nav-pills nav-fill">
            <a class="nav-item nav-link {tab_1}" href="{url_1}">Recibo</a>
            <a class="nav-item nav-link {tab_2}" href="{url_2}">Movimientos</a>
            <a class="nav-item nav-link {tab_3}" href="{url_3}">Faltas</a>
            <a class="nav-item nav-link {tab_4}" href="{url_4}">Vacaciones</a>
            <a class="nav-item nav-link {tab_5}" href="{url_5}">Horas extras</a>
            <a class="nav-item nav-link {tab_6}" href="{url_6}">Acumulados</a>
            <a class="nav-item nav-link {tab_7}" href="{url_7}">Recibo electrónico</a>
        </nav>
    </div>
</div>

<?php if(is_array($periodos_trabajador) && count($periodos_trabajador)>0){ ?>
<div class="card">
	<div class="card-body">
		<form class="form-inline">
			<div class="form-group mb-2">
				<label for="staticEmail2" >Periodo: </label>
			</div>
			<div class="form-group mx-sm-3 mb-2">
                <select class="custom-select" id="periodo_trabajador" >
                    <?php foreach ($periodos_trabajador as $key => $value) { ?>
                        <option <?php echo($periodo == $value['id'])? 'selected' : ''; ?> value="<?php echo $value['id']; ?>">[<?php echo $value['Clave']; ?>] <?php echo utils::afecha($value['FechaInicio'],true); ?> al <?php echo utils::afecha($value['FechaFin'],true); ?></option>
                    <?php } ?>
                </select>
			</div>
			<button type="button" class="btn btn-primary mb-2" onclick="var opcion=$('select#periodo_trabajador option:selected').val(); window.location.href = '<?php echo $url; ?>/'+opcion;" >Ver</button>
		</form>
	</div>
</div>
<?php } ?>

<form>

	<div class="row mt-3">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-body">
					{detalle_form}
				</div>
			</div>
		</div>
	</div>

	<div class="row">
	
		<div class="col-sm-12">
			<div class="mt-2">
				{content_form}
			</div>
		</div>
	</div>
