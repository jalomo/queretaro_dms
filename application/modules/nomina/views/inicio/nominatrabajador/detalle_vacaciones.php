<script>
	var id = "<?php echo $id; ?>";
</script>

<?php if(isset($periodo)){ ?>
	<div class="row mt-3">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-sm-3">
							<h4><small>Estatus:</small><br/><?php echo ($periodo['Descripcion_TrabajadorPagoEstatus'] == null)? '-' : $periodo['Descripcion_TrabajadorPagoEstatus']; ?></h4>
						</div>
						<div class="col-sm-3">
							<h4><small>Monto total:</small><br/>$<?php echo ($vacaciones['Monto'] == null)? '0.00' : utils::formatMoney($vacaciones['Monto']); ?></h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>

<div class="row mt-3">
	<div class="col-sm-12">

		<div class="card">
			<div class="card-body">

				<table id="table_content" class="table">
				</table>
			</div>
		</div>

	</div>

</div>
