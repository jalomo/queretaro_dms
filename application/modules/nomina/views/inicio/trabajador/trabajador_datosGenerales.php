<input type="hidden" class="form-control" name="id" value="{{id}}">
<div class="card">
    <div class="card-body">
        <div class="row">

            <div class="col-sm-4 mb-3">
                <label for="Nombre">Fotografia:</label>
                <div class="row">
                    <textarea id="Fotografia" name="Fotografia" style="display:none;" rows="4" cols="50"></textarea>
                    <input id="fotografia_nombre" type="text" name="fotografia_nombre" style="display:none;" />
                    <div class="col-sm-12 mb-3" id="preview_imagen_div" style="display:block;">
                        <center><img id="preview_imagen" style="display:block;max-height: 120px;" onerror="this.src='<?php echo base_url('js/nomina/blanco.png'); ?>'" class="img-fluid" src=""  /></center>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <div class="custom-file">
                                <input onchange="Apps.SelectedFile(this);" type="file" accept='image/*' class="custom-file-input" id="imagen_logo" aria-describedby="imagen_logo" />
                                <label class="custom-file-label text-truncate" for="imagen_logo"></label>
                            </div>
                        </div>
                        <small id="msg_Fotografia" class="form-text text-danger"></small>
                    </div>
                </div>
            </div>
        
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="Nombre">Nombre:</label>
                            <input type="text" class="form-control" id="Nombre" name="Nombre" value="{{Nombre}}">
                            <small id="msg_Nombre" class="form-text text-danger"></small>
                        </div>
                    </div>
                    
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="Apellido_1">Apellido paterno:</label>
                            <input type="text" class="form-control" id="Apellido_1" name="Apellido_1" value="{{Apellido1}}">
                            <small id="msg_Apellido_1" class="form-text text-danger"></small>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="Apellido_2">Apellido materno:</label>
                            <input type="text" class="form-control" id="Apellido_2" name="Apellido_2" value="{{Apellido2}}">
                            <small id="msg_Apellido_2" class="form-text text-danger"></small>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="FechaNacimiento">Fecha nacimiento:</label>
                            <input type="date" class="form-control" id="FechaNacimiento" name="FechaNacimiento" value="{{FechaNacimiento}}">
                            <small id="msg_FechaNacimiento" class="form-text text-danger"></small>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="id_LugarNacimiento">Lugar de nacimiento:</label>
                            <select class="form-control" id="id_LugarNacimiento" name="id_LugarNacimiento" attr-id="{{id_LugarNacimiento}}">
                                {{#EntidadesNacimiento}}
                                <option value="{{id}}" {{select}}>{{Nombre}}</option>
                                {{/EntidadesNacimiento}}
                            </select>
                            <small id="msg_id_LugarNacimiento" class="form-text text-danger"></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="NumeroImss">IMSS:</label>
                    <input type="text" class="form-control" id="NumeroIMSS" name="NumeroImss" value="{{NumeroImss}}">
                    <small id="msg_NumeroImss" class="form-text text-danger"></small>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="id_Genero">Sexo:</label>
                    <select class="form-control" id="id_Genero" name="id_Genero" attr-id="{{id_Genero}}">
                        {{#Genero}}
                        <option value="{{id}}" {{select}}>{{Nombre}}</option>
                        {{/Genero}}
                    </select>
                    <small id="msg_id_Genero" class="form-text text-danger"></small>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="RFC">RFC:</label>
                    <input type="text" class="form-control" id="RFC" name="RFC" value="{{RFC}}">
                    <small id="msg_RFC" class="form-text text-danger"></small>
                </div>
            </div>
            
        </div>
        <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="CURP">CURP:</label>
                    <input type="text" class="form-control" id="CURP" name="CURP" value="{{CURP}}">
                    <small id="msg_CURP" class="form-text text-danger"></small>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="id_Clasificacion">Clasificación:</label>
                    <select class="form-control" id="id_Clasificacion" name="id_Clasificacion" attr-id="{{id_Clasificacion}}" >
                        {{#Clasificaciones}}
                        <option value="{{id}}" {{select}}>{{Clave}} - {{Descripcion}}</option>
                        {{/Clasificaciones}}
                    </select>
                    <small id="msg_id_Clasificacion" class="form-text text-danger"></small>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="id_Departamento">Departamento:</label>
                    <select class="form-control" id="id_Departamento" name="id_Departamento" attr-id="{{id_Departamento}}" >
                        {{#Departamentos}}
                        <option value="{{id}}" {{select}}>{{Clave}} - {{Descripcion}}</option>
                        {{/Departamentos}}
                    </select>
                    <small id="msg_id_Departamento" class="form-text text-danger"></small>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="id_Puesto">Puesto</label>
                    <select class="form-control" id="id_Puesto" name="id_Puesto" attr-id="{{id_Puesto}}" >
                        {{#Puestos}}
                        <option value="{{id}}" {{select}}>{{Clave}} - {{Descripcion}}</option>
                        {{/Puestos}}
                    </select>
                    <small id="msg_id_Puesto" class="form-text text-danger"></small>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="FechaAlta">Fecha alta:</label>
                    <input type="date" class="form-control" id="FechaAlta" name="FechaAlta" value="{{FechaAlta}}">
                    <small id="msg_FechaAlta" class="form-text text-danger"></small>
                </div>
            </div>
        </div>
    </div>
</div>