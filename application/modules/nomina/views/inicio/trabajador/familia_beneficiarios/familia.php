<div class="row mb-3">
	<div class="col-md-8"></div>
	<div class="col-md-4" align="right">
		<a class="btn btn-primary" href="<?php echo site_url('nomina/inicio/trabajador/familia_alta?identity='.$identity) ?>">Registrar</a>
	</div>
</div>

<?php echo form_open('#', 'class="" id="general_form"'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-bordered" id="listado" width="100%" cellspacing="0"></table>
		</div>
	</div>
</div>
<?php echo form_close(); ?>