<input type="hidden" class="form-control" name="id" value="{{id}}">

<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-body">
				<h6 class="text-dark">Domicilio</h6>
				<hr class="style-six" />

				<div class="form-group row"><label for="CalleNumero" class="col-sm-2 col-form-label">Calle y número:</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="CalleNumero" name="CalleNumero" value="{{CalleNumero}}">
						<small id="msg_CalleNumero" class="form-text text-danger"></small>
					</div>
				</div>
				<div class="form-group row">
					<label for="Colonia" class="col-sm-2 col-form-label">Colonia:</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="Colonia" name="Colonia" value="{{Colonia}}">
						<small id="msg_Colonia" class="form-text text-danger"></small>
					</div>
				</div>
				<div class="form-group row"><label for="Poblacion" class="col-sm-2 col-form-label">Poblacion:</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="Poblacion" name="Poblacion" value="{{Poblacion}}">
						<small id="msg_Poblacion" class="form-text text-danger"></small>
					</div>
				</div>

				<div class="form-group row">
					<label for="id_EntidadFederativa1" class="col-sm-2 col-form-label">Entidad federativa:</label>
					<div class="col-sm-8">
						<select class="form-control" id="id_EntidadFederativa1" name="id_EntidadFederativa" attr-id="{{id_EntidadFederativa}}" > 
                            {{#EntidadesFederativa}}<option value="{{id}}" >{{Clave}} - {{Nombre}}</option> {{/EntidadesFederativa}}
                        </select>
						<small id="msg_id_EntidadFederativa" class="form-text text-danger"></small>
					</div>
				</div>

				<div class="form-group row">
					<label for="Pais" class="col-sm-2 col-form-label">País:</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="Pais" name="Pais" value="{{Pais}}">
						<small id="msg_Pais" class="form-text text-danger"></small>
					</div>
				</div>

				<div class="form-group row">
					<label for="CorreoElectronico_1" class="col-sm-2 col-form-label">Correo electrónico 1:</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="CorreoElectronico_1" name="CorreoElectronico_1" value="{{CorreoElectronico_1}}">
						<small id="msg_CorreoElectronico_1" class="form-text text-danger"></small>
					</div>
				</div>

				<div class="form-group row">
					<label for="CorreoElectronico_2" class="col-sm-2 col-form-label">Correo electrónico 2:</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="CorreoElectronico_2" name="CorreoElectronico_2" value="{{CorreoElectronico_2}}">
						<small id="msg_CorreoElectronico_2" class="form-text text-danger"></small>
					</div>
				</div>

				<div class="form-group row">
					<label for="CodigoPostal" class="col-sm-2 col-form-label">Código postal:</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="CodigoPostal" name="CodigoPostal" value="{{CodigoPostal}}">
						<small id="msg_CodigoPostal" class="form-text text-danger"></small>
					</div>
				</div>

				<div class="form-group row">
					<label for="Telefono_1" class="col-sm-2 col-form-label">Teléfono 1:</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="Telefono_1" name="Telefono_1" value="{{Telefono_1}}">
						<small id="msg_Telefono_1" class="form-text text-danger"></small>
					</div>
				</div>

				<div class="form-group row">
					<label for="Telefono_2" class="col-sm-2 col-form-label">Teléfono 2:</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="Telefono_2" name="Telefono_2" value="{{Telefono_2}}">
						<small id="msg_Telefono_2" class="form-text text-danger"></small>
					</div>
				</div>

			</div>
		</div>
	</div>

</div>

<div class="row mt-3">
	<div class="col-sm-6">
		<div class="card">
			<div class="card-body">
				<h6 class="text-dark">Formación académica</h6>
				<hr class="style-six" />

				<div class="form-group row">
					<label for="id_NivelEstudios" class="col-sm-4 col-form-label">Nivel de estudios:</label>
					<div class="col-sm-8">
						<select class="form-control" id="id_NivelEstudios" name="id_NivelEstudios" attr-id="{{id_NivelEstudios}}" > 
                            {{#NivelesEstudio}}<option value="{{id}}" >{{Nombre}}</option> {{/NivelesEstudio}}
                        </select>
						<small id="msg_id_NivelEstudios" class="form-text text-danger"></small>
					</div>
				</div>

				<div class="form-group row">
					<label for="Profesion" class="col-sm-4 col-form-label">Profesión:</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="Profesion" name="Profesion" value="{{Profesion}}">
						<small id="msg_Profesion" class="form-text text-danger"></small>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="col-sm-6">
		<div class="card">
			<div class="card-body">
				
				<div class="form-group row">
					<label for="id_EstadoCivil" class="col-sm-4 col-form-label">Edo. Civil</label>
					<div class="col-sm-8">
						<select class="form-control" id="id_EstadoCivil" name="id_EstadoCivil" attr-id="{{id_EstadoCivil}}"> 
                            {{#EstadoCivil}}<option value="{{id}}" >{{Nombre}}</option> {{/EstadoCivil}}
                        </select>
						<small id="msg_id_EstadoCivil" class="form-text text-danger"></small>
					</div>
				</div>

				<div class="form-group row">
					<label for="id_TipoSangre" class="col-sm-4 col-form-label">Tipo de sangre</label>
					<div class="col-sm-8">
						<select class="form-control" id="id_TipoSangre" name="id_TipoSangre" attr-id="{{id_TipoSangre}}"> 
                            {{#TipoSangre}}<option value="{{id}}" >{{Tipo}} {{Nombre}}</option> {{/TipoSangre}}
                        </select>
						<small id="msg_id_TipoSangre" class="form-text text-danger"></small>
					</div>
				</div>

			</div>
		</div>
	</div>

</div>
