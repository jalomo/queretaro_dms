<?php echo form_open('nomina/inicio/parametrosnomina/index', 'class="" id="general_form"'); ?>
<div class="row">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title text-dark">Cálculo de la nómina</h3>
                {CALCULONOMINA}
                    <div class="form-group">
                        <label>{label}</label>
                        {input}
                        <small id="msg_{key}" class="form-text text-danger"></small>
                    </div>
                {/CALCULONOMINA}
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title text-dark">Tablas del sistema</h3>
                        {TABLASSISTEMA}
                            <div class="form-group">
                                <label>{label}</label>
                                {input}
                                <small id="msg_{key}" class="form-text text-danger"></small>
                            </div>
                        {/TABLASSISTEMA}
                    </div>
                </div>
            </div>

            <div class="col-sm-12 mt-3">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title text-dark">Parámetros del cálculo del I.S.R.</h3>
                        {CALCULOISR}
                            <div class="form-group">
                                <label>{label}</label>
                                {input}
                                <small id="msg_{key}" class="form-text text-danger"></small>
                            </div>
                        {/CALCULOISR}
                    </div>
                </div>
            </div>

            <div class="col-sm-12 mt-3">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title text-dark">Parámetros de previsión social</h3>
                        {PREVISIONSOCIAL}
                            <div class="form-group">
                                <label>{label}</label>
                                {input}
                                <small id="msg_{key}" class="form-text text-danger"></small>
                            </div>
                        {/PREVISIONSOCIAL}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6 mt-3">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title text-dark">Vacaciones y prima vacacional</h3>
                {VACACIONES}
                    <div class="form-group">
                        <label>{label}</label>
                        {input}
                        <small id="msg_{key}" class="form-text text-danger"></small>
                    </div>
                {/VACACIONES}
            </div>
        </div>
    </div>

    <div class="col-sm-6 mt-3">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title text-dark">Jornada de trabajo</h3>
                {JORNADATRABAJO}
                    <div class="form-group">
                        <label>{label}</label>
                        {input}
                        <small id="msg_{key}" class="form-text text-danger"></small>
                    </div>
                {/JORNADATRABAJO}
            </div>
        </div>
    </div>

</div>
<?php echo form_close(); ?>