<?php
class MY_Form_validation extends CI_Form_validation
{
  public $CI;
  function __construct($config = array())
  {
    parent::__construct($config);
    $this->CI = &get_instance();

  }

  function error_array()
  {
    return (count($this->_error_array) === 0)? false : $this->_error_array;
  }

  function exists($str, $value){
    list($table, $column) = explode('.', $value, 2);

    $this->CI->db->from($table);
    $this->CI->db->where($column,$str);
    return ($this->CI->db->count_all_results() > 0) ? TRUE : FALSE;
  }
}