@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="alta_registro" data-id="{{ isset($data->id) ? $data->id : '' }}">
                @if(!isset($data->id))
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">N° de Cliente (Referencia):</label>
                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" id="ref_num_cliente" name="ref_num_cliente" placeholder="0001">
                                <input type="hidden" value="0" id="cliente_nuevo">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <a href="#" style="margin-top: 30px;" onclick="recuperar_cliente()" class="btn btn-info" type="button">
                                Buscar cliente
                            </a>
                        </div>

                        <div class="col-md-4">
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-md-4">
                        <label for="">Tipo de cliente:</label>
                        <select class="form-control" id="tipo_registro" onchange="generar_numero_cliente()" style="width: 100%;" <?php echo ((isset($data->id)) ? 'disabled' : ''); ?> >
                            <option value="">Selecionar ...</option>
                            @if(!empty($cat_clave))
                                @foreach ($cat_clave as $clave)
                                    <option value="{{$clave->id}}" <?php if(isset($data->tipo_registro)){if($data->tipo_registro == $clave->id) echo "selected";} ?>>{{$clave->clave}}-{{$clave->nombre}}</option>
                                @endforeach
                            @endif
                        </select>
                        <div id="tipo_registro_error" class="invalid-feedback"></div>
                    </div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">N° de Cliente:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->numero_cliente)){print_r($data->numero_cliente);} ?>" id="numero_cliente" name="numero_cliente" disabled>
                            <div id="numero_cliente_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Nombre:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->nombre)){print_r($data->nombre);} ?>" id="nombre" name="nombre" placeholder="">
                            <div id="nombre_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Apellido Paterno:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->apellido_paterno)){print_r($data->apellido_paterno);} ?>" id="apellido_paterno" name="apellido_paterno" placeholder="">
                            <div id="apellido_paterno_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Apellido Materno:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->apellido_materno)){print_r($data->apellido_materno);} ?>" id="apellido_materno" name="apellido_materno" placeholder="">
                            <div id="apellido_materno_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <label for="">Régimen Fiscal:</label>
                        <select class="form-control" id="regimen_fiscal" name="regimen_fiscal" onchange="limite_rfc()"style="width: 100%;">
                            <option value="">Selecionar ...</option>
                            <option value="M" <?php if(isset($data->regimen_fiscal)){ if($data->regimen_fiscal == "M") echo "selected";} ?> >Moral </option>
                            <option value="F" <?php if(isset($data->regimen_fiscal)){ if($data->regimen_fiscal == "F") echo "selected";} ?> >Fisica </option>
                        </select>
                        <div id="tipo_registro_error" class="invalid-feedback"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Razón Social:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->nombre_empresa)){print_r($data->nombre_empresa);} ?>" id="nombre_empresa" name="nombre_empresa" placeholder="">
                            <div id="nombre_empresa_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">RFC:</label>
                            <input type="text" class="form-control" minlength="11" value="<?php if(isset($data->rfc)){print_r($data->rfc);} ?>" id="rfc" name="rfc" placeholder="">
                            <div id="rfc_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Fecha de nacimiento:</label>
                            <input type="date" class="form-control" max="<?php echo (date('Y')-16).'-01-01'; ?>" value="<?php if(isset($data->fecha_nacimiento)){print_r($data->fecha_nacimiento);}else{  echo (date('Y')-16).'-01-01';}?>" id="fecha_nacimiento" name="fecha_nacimiento" placeholder="">
                            <div id="fecha_nacimiento_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    
                    <div class="col-md-5" align="right">
                        <table class="" style="width: 100%;margin-top: 30px;background-color: white;">
                            <tr>
                                <td style="width: 56%; vertical-align: middle;font-size:12px;font-weight:bold;height: 36px;">
                                    Flotillero
                                    <input type="hidden" name="flotillero" id="flotillero" value="<?php echo isset($data->flotillero) ? $data->flotillero : '0'?>">
                                    <div id="flotillero_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    SI
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck1" name="flotillero_ck" id="flotillero_si" value="1" <?php if(isset($data->flotillero)) {if( $data->flotillero == "1") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    NO
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck1" name="flotillero_ck" id="flotillero_no" value="0" <?php if(isset($data->flotillero)) {if( $data->flotillero == "0") echo "checked"; } ?>>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Calle:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->direccion)){print_r($data->direccion);} ?>" id="direccion" name="direccion" placeholder="">
                            <div id="direccion_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Número Int.:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->numero_int)){print_r($data->numero_int);}else{ echo 'SN';} ?>" id="numero_int" name="numero_int" placeholder="">
                            <div id="numero_int_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Número Ext.:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->numero_ext)){print_r($data->numero_ext);}else{ echo 'SN';} ?>" id="numero_ext" name="numero_ext" placeholder="">
                            <div id="numero_ext_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Colonial:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->colonia)){print_r($data->colonia);} ?>" id="colonia" name="colonia" placeholder="">
                            <div id="colonia_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Estado:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->estado)){print_r($data->estado);} ?>" id="estado" name="estado" placeholder="">
                            <div id="estado_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Municipio:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->municipio)){print_r($data->municipio);} ?>" id="municipio" name="municipio" placeholder="">
                            <div id="municipio_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Código postal:</label>
                            <input type="text" maxlength="5" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="<?php if(isset($data->codigo_postal)){print_r($data->codigo_postal);} ?>" id="codigo_postal" name="codigo_postal" placeholder="">
                            <div id="codigo_postal_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Celular:</label>
                            <input type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="<?php if(isset($data->telefono)){print_r($data->telefono);} ?>" id="telefono" name="telefono" placeholder="">
                            <div id="telefono_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Telefono Casa:</label>
                            <input type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="<?php if(isset($data->telefono_secundario)){print_r($data->telefono_secundario);} ?>" id="telefono_secundario" name="telefono_secundario" placeholder="">
                            <div id="telefono_secundario_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Telefono Oficina:</label>
                            <input type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="<?php if(isset($data->telefono_oficina)){print_r($data->telefono_oficina);} ?>" id="telefono_oficina" name="telefono_oficina" placeholder="">
                            <div id="telefono_oficina_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Correo Electrónico:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->correo_electronico)){print_r($data->correo_electronico);} else{ echo 'notienecorreo@notienecorreo.com.mx'; } ?>" id="correo_electronico" name="correo_electronico" placeholder="">
                            <div id="correo_electronico_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Correo Electrónico Secundario:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->correo_electronico_secundario)){print_r($data->correo_electronico_secundario);} else{ echo 'notienecorreo@notienecorreo.com.mx'; } ?>" id="correo_electronico_secundario" name="correo_electronico_secundario" placeholder="">
                            <div id="correo_electronico_secundario_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <br>
                <h4>Datos de pago</h4>
                <hr>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Forma de pago:</label>
                            <select class="form-control" id="metodo_pago_id" name="metodo_pago_id" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_tipo_pago))
                                    @foreach ($cat_tipo_pago as $pago)
                                        <option value="{{$pago->id}}" <?php if(isset($data->metodo_pago_id)){if($data->metodo_pago_id == $pago->id) echo "selected";} ?>>[{{$pago->clave}}] {{$pago->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="metodo_pago_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Método de pago:</label>
                            <select class="form-control" id="forma_pago" name="forma_pago" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                <option value="PUE" <?php if(isset($data->forma_pago)){ if($data->forma_pago == "PUE") echo "selected";} ?>>Pago en una sola exhibición</option>
                                <option value="PPD" <?php if(isset($data->forma_pago)){ if($data->forma_pago == "PPD") echo "selected";} ?>>Pago en parcialidades o diferido</option>
                            </select>
                            <div id="forma_pago_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">CFDI:</label>
                            <select class="form-control" id="cfdi_id" name="cfdi_id" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_cfdi))
                                    @foreach ($cat_cfdi as $cfdi)
                                        <option value="{{$cfdi->id}}" <?php if(isset($data->cfdi_id)){if($data->cfdi_id == $cfdi->id) echo "selected";} ?>>[{{$cfdi->clave}}] {{$cfdi->descripcion}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="cfdi_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Límite de crédito:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->limite_credito)){print_r($data->limite_credito);} else{ echo '0.00'; } ?>" id="limite_credito" name="limite_credito" placeholder="">
                            <div id="limite_credito_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Saldo:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->saldo)){print_r($data->saldo);} else{ echo '0.00'; } ?>" id="saldo" name="saldo" placeholder="">
                            <div id="saldo_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <!--<div class="col-md-4">
                        <div class="form-group">
                            <label for="">Moneda:</label>
                            <select class="form-control" id="modena" name="modena" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                <option value="MXN">MXN (Pesos Mexicanos)</option>
                            </select>
                            <div id="modena_error" class="invalid-feedback"></div>
                        </div>
                    </div>-->
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">NOTAS:</label>
                            <textarea class="form-control" cols="4" id="notas" name="notas"><?php if(isset($data->notas)){print_r($data->notas);} ?></textarea>
                            <div id="notas_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" align="center">
                        <br>
                        <input type="hidden" id="consecutivo" value="<?php if (isset($consecutivo)) {echo $consecutivo;} else {echo '0';} ?>">
                        <input type="hidden" id="cons_cliente" value="<?php if (isset($data->id)) {echo $data->id;} else {echo '0';} ?>">
                        <input type="hidden" id="tipo_envio" value="<?php if (isset($data->id)) {echo '1';} else {echo '0';} ?>">
                        <input type="hidden" id="clientes" value="">
                        <input type="hidden" id="tipo_clinete" value="">

                            <button type="button" id="editar_registro" class="btn btn-success col-md-4" <?php if (!isset($data->id)) {echo 'hidden';} ?>>
                                Actualizar
                            </button>
                            <button type="button" id="guardar_registro" class="btn btn-success col-md-4" <?php if (isset($data->id)) {echo 'hidden';} ?>>
                                Guardar
                            </button>
                        
                        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
                    </div>
                </div>
            </form>

            <br>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var formulario = document.getElementById("tipo_envio").value; 
            if (formulario == "1") {
                var cat_clave = document.getElementById("tipo_registro");
                var categoria = cat_clave.options[cat_clave.selectedIndex].text;
                var tipo_cliente = categoria.split("-");
                var cliente = document.getElementById("numero_cliente").value;
                $("#numero_cliente").val(tipo_cliente[0]+""+cliente);
            }
        });

        //Para guardar registro por primera vez
        $("#guardar_registro").on('click', function() {
            $(".invalid-feedback").html("");

            //Validamos campos de telefono y correo
            var validaciones = validar_campos();
            if (validaciones) {
                ajax.post('api/clientes', procesarRegistro(), function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }

                    //var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
                    utils.displayWarningDialog("Proceso completado con éxito", "success", function(data) {
                        guardar_claves();
                        return window.location.href = base_url + 'catalogos/clientesController/index';
                    });
                });
            }
            
        });

        //Para actualizar un registro
        $("#editar_registro").on('click', function() {
            $(".invalid-feedback").html("");

            if ($("#tipo_envio").val() == "2") {
                var id = $("#cons_cliente").val();
            } else {
                var id = $("#alta_registro").data('id');
            }
            

            //Validamos campos de telefono y correo
            var validaciones = validar_campos();
            if (validaciones) {
                ajax.put('api/clientes/'+id, procesarRegistro(), function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    
                    //var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
                    utils.displayWarningDialog("Proceso completado con éxito", "success", function(data) {
                        if ($("#tipo_envio").val() == "2") {
                            guardar_claves();
                        }
                        return window.location.href = base_url + 'catalogos/clientesController/index';
                    })
                })
            }
        });

        function guardar_claves() {
            var cat_clave = document.getElementById("tipo_registro").value;
            var numero_cliente = document.getElementById("numero_cliente").value;
            var clave = numero_cliente.substr(2, numero_cliente.length);

            $.ajax({
                url: base_url+"catalogos/clientesController/recuperar_id_cliente",
                method: 'post',
                data: {
                    clave: clave,
                    cat_clave: cat_clave
                },
                success:function(resp){
                    console.log("OK");

                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                //Cierre del error
                }
            //Cierre del ajax
            });
        }

        //Armamos el formulario a enviar
        let procesarRegistro = function() {
            var numero_cliente = document.getElementById("numero_cliente").value;
            var clave = numero_cliente.substr(2, numero_cliente.length);

            let form = $('#alta_registro').serializeArray();
            let newArray = {
                tipo_registro : document.getElementById("tipo_registro").value !== '' ? document.getElementById("tipo_registro").value : '',
                numero_cliente : clave, //document.getElementById("numero_cliente").value,
                nombre : quitar_tildes(document.getElementById("nombre").value),
                apellido_paterno : quitar_tildes(document.getElementById("apellido_paterno").value),
                apellido_materno : quitar_tildes(document.getElementById("apellido_materno").value),
                nombre_empresa : quitar_tildes(document.getElementById("nombre_empresa").value),
                rfc : document.getElementById("rfc").value,
                regimen_fiscal : document.getElementById("regimen_fiscal").value !== '' ? document.getElementById("regimen_fiscal").value : '',
                direccion : document.getElementById("direccion").value,
                numero_int : document.getElementById("numero_int").value,
                numero_ext : document.getElementById("numero_ext").value,
                colonia : document.getElementById("colonia").value,
                municipio : document.getElementById("municipio").value,
                estado : document.getElementById("estado").value,
                pais : "MÉXICO",
                codigo_postal : document.getElementById("codigo_postal").value,
                telefono : document.getElementById("telefono").value,
                telefono_secundario : document.getElementById("telefono_secundario").value,
                telefono_oficina : document.getElementById("telefono_oficina").value,
                correo_electronico : document.getElementById("correo_electronico").value,
                correo_electronico_secundario : document.getElementById("correo_electronico_secundario").value,
                fecha_nacimiento : document.getElementById("fecha_nacimiento").value,
                flotillero : document.getElementById("flotillero").value,
                cfdi_id : (document.getElementById("cfdi_id").value !== '') ? document.getElementById("cfdi_id").value : '0',
                metodo_pago_id : (document.getElementById("metodo_pago_id").value !== '') ? document.getElementById("metodo_pago_id").value : '0',
                forma_pago : document.getElementById("forma_pago").value,
                limite_credito : document.getElementById("limite_credito").value,
                saldo : document.getElementById("saldo").value,
                notas : document.getElementById("notas").value,
            };

            return newArray;
        } 

        function validar_campos() {
            var validacion = true;

            var correo_1 = $("#correo_electronico").val();
            if (!validar_email(correo_1)) {
                var error_1 = $("#correo_electronico_error");
                $("#correo_electronico_error").css("display","inline-block");
                error_1.append('<label class="form-text text-danger">Formato invalido.</label>');
                validacion = false;
            }else{
                var error_1 = $("#correo_electronico_error");
                error_1.empty();
            }

            var correo_2 = $("#correo_electronico_secundario").val();
            if (!validar_email(correo_2)) {
                var error_2 = $("#correo_electronico_secundario_error");
                $("#correo_electronico_secundario_error").css("display","inline-block");
                error_2.append('<label class="form-text text-danger">Formato invalido.</label>');
                validacion = false;
            }else{
                var error_2 = $("#correo_electronico_secundario_error");
                error_2.empty();
            }

            var telefono_1 = document.getElementById('telefono').value;
            if (telefono_1.length < 10) {
                var error_3 = $("#telefono_error");
                $("#telefono_error").css("display","inline-block");
                error_3.append('<label class="form-text text-danger">Formato invalido.</label>');
                validacion = false;
            }else{
                var error_3 = $("#telefono_error");
                error_3.empty();
            }

            var telefono_2 = document.getElementById('telefono_secundario').value;
            if (telefono_2.length < 10) {
                var error_4 = $("#telefono_secundario_error");
                $("#telefono_secundario_error").css("display","inline-block");
                error_4.append('<label class="form-text text-danger">Formato invalido.</label>');
                validacion = false;
            }else{
                var error_4 = $("#telefono_secundario_error");
                error_4.empty();
            }

            return validacion;
        }

        function validar_email( email ) 
        {
            var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email) ? true : false;
        }

        function quitar_tildes(texto) 
        {
            return texto.normalize('NFD').replace(/[\u0300-\u036f]/g,"");
        }

        //Creamos clave para numero economico
        function generar_numero_cliente() {
            //Verificamos que sea exclusivamente alta para generar numero de orden
            var formulario = document.getElementById("tipo_envio").value;
            if (formulario == "0") {
                var error_4 = $("#tipo_registro_error");
                error_4.empty();

                var cat_clave = document.getElementById("tipo_registro");
                var categoria = cat_clave.options[cat_clave.selectedIndex].text;
                var tipo_cliente = categoria.split("-");

                //Formamos el numero economico
                var consecutivo = parseInt($("#consecutivo").val()); 
                var folio = "";
                consecutivo++;

                if (consecutivo < 10) {
                    folio = tipo_cliente[0]+"0000" + consecutivo;
                } else if ((consecutivo > 9) && (consecutivo < 100)) {
                    folio = tipo_cliente[0]+"000" + consecutivo;
                } else if ((consecutivo > 99) && (consecutivo < 1000)) {
                    folio = tipo_cliente[0]+"00" + consecutivo;
                } else if ((consecutivo > 9999) && (consecutivo < 10000)) {
                    folio = tipo_cliente[0]+"0" + consecutivo;
                } else {
                    folio = tipo_cliente[0]+"" + consecutivo;
                }

                $("#numero_cliente").val(folio);
            }else{
                if (formulario == "2") {
                    validar_clave();
                }
            }
        }

        $('.tb1_ck1').change(function(){
            if($(this).is(':checked')){
                $('.tb1_ck1').not(this).prop('checked', false);
            }
            //Mostramos el valor en un input
            if ($("input[name='flotillero_ck']:radio").is(':checked')) {
                var valor = $("input[name='flotillero_ck']:checked").val();
                $("input[name='flotillero']").val(valor);
            }
        });

        function limite_rfc() {
            var tipo_regimen = document.getElementById("regimen_fiscal").value;
            if (tipo_regimen == 'M') {
                $("#rfc").attr('maxlength','12');
            } else {
                $("#rfc").attr('maxlength','13');
            }
        }

        function recuperar_cliente() {
            var cliente = document.getElementById("ref_num_cliente").value;

            $.ajax({
                url: base_url+"catalogos/clientesController/recuperar_cliente/"+cliente,
                method: 'post',
                /*data: {
                    base_64: base_64,
                },*/
                success:function(resp){
                    //console.log(resp);

                    if (resp.indexOf("handler           </p>")<1) {
                        var campos = resp.split("=");
                        console.log(campos.length);
                        if (campos.length >5) {
                            $("#nombre").val(campos[0]);
                            $("#apellido_paterno").val(campos[1]);
                            $("#apellido_materno").val(campos[2]);
                            document.getElementById('regimen_fiscal').value = campos[3];
                            $("#nombre_empresa").val(campos[4]);
                            $("#rfc").val(campos[5]);
                            
                            $("#direccion").val(campos[6]);
                            $("#numero_int").val(campos[7]);
                            $("#numero_ext").val(campos[8]);
                            $("#colonia").val(campos[9]);
                            $("#municipio").val(campos[10]);
                            
                            $("#codigo_postal").val(campos[11]);
                            $("#telefono").val(campos[12]);
                            $("#telefono_secundario").val(campos[13]);
                            $("#telefono_oficina").val(campos[14]);
                            $("#correo_electronico").val(campos[16]);
                            $("#correo_electronico_secundario").val(campos[17]);
                            $("#fecha_nacimiento").val(campos[18]);
                            
                            document.getElementById('cfdi_id').value = campos[19];
                            document.getElementById('metodo_pago_id').value = campos[20];
                            document.getElementById('forma_pago').value = campos[21];
                            $("#saldo").val(campos[22]);
                            $("#limite_credito").val(campos[23]);
                            $("#notas").val(campos[24]);

                            $("#estado").val(campos[25]);

                            $("#flotillero").val(campos[15]);
                            if (campos[15] == "1") {
                                $("#flotillero_si").attr('checked',true);
                            }else{
                                $("#flotillero_no").attr('checked',true);
                            }

                            //Actualizamos el tipo de formulario
                            $("#tipo_envio").val("2");
                            $("#tipo_clinete").val(campos[26]);
                            $("#cons_cliente").val(campos[27]);
                            $("#clientes").val(campos[28]);

                            //Si se va a asociar un cliente, no se guardara, solo se actualizara
                            $("#editar_registro").attr('hidden',false);
                            $("#guardar_registro").attr('hidden',true);
                        } 
                    }else{
                        console.log(resp);
                    }

                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                //Cierre del error
                }
            //Cierre del ajax
            });
        }

        function validar_clave() {
            var cat_clave = document.getElementById("tipo_registro");
            var select_tipo_cliente = cat_clave.value;

            var registro_tipo_cliente = document.getElementById("tipo_clinete").value;
            var referencia = document.getElementById("ref_num_cliente").value;

            if (select_tipo_cliente != registro_tipo_cliente) {
                var tipo_cliente = cat_clave.options[cat_clave.selectedIndex].text;
                var categoria = tipo_cliente.split("-");

                var error_4 = $("#tipo_registro_error");
                error_4.empty();

                var registros = document.getElementById("clientes").value;
                var registros = registros.split(",");
                
                if (registros.indexOf(select_tipo_cliente) !== 1) {
                    var cliente = categoria[0]+""+referencia;
                    $("#numero_cliente").val(cliente);
                    $("#numero_cliente").val(cliente);
                }else{
                   $("#tipo_registro_error").css("display","inline-block");
                    error_4.append('<label class="form-text text-danger">Ya existe este un cliente con esta clave.</label>'); 
                }
            }else{
                var error_clave = $("#tipo_registro_error");
                error_clave.empty();
                $("#tipo_registro_error").css("display","inline-block");
                error_clave.append('<label class="form-text text-danger">No se puede seleccionar el mismo tipo de cliente.</label>');
            }
        }

    </script>
@endsection