@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="alta_registro" data-id="{{ isset($data->id) ? $data->id : '' }}">
                <div class="row">
                    <div class="col-md-6">
                        <label for="" style="font-size: 13px;">Descripción Unidad:</label>
                        <textarea type="text" class="form-control" id="unidad_descripcion" name="unidad_descripcion" rows="1"><?php if(isset($data->unidad_descripcion)){print_r($data->unidad_descripcion);} ?></textarea>
                        <div id="unidad_descripcion_error" class="invalid-feedback"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Ultimo Kilometraje:</label>
                            <input type="text" maxlength="5" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="<?php if(isset($data->kilometraje)){print_r($data->kilometraje);} ?>"  id="kilometraje" name="kilometraje" placeholder="">
                            <div id="kilometraje_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Marca:</label>
                            <select class="form-control" id="marca_id" name="marca_id" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_marcas))
                                    @foreach ($cat_marcas as $marca)
                                        <option value="{{$marca->id}}" <?php if(isset($data->marca_id)){if($data->marca_id == $marca->id) echo "selected";} ?>>{{$marca->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="id_marca_error" class="invalid-feedback"></div>
                        </div>
                    </div> 
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Catálogo:</label>
                            <select class="form-control" id="catalogo_id" name="catalogo_id" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_unidades))
                                    @foreach ($cat_unidades as $catalogo)
                                        <option value="{{$catalogo->id}}" <?php if(isset($data->catalogo_id)){if($data->catalogo_id == $catalogo->id) echo "selected";} ?>>{{$catalogo->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="catalogo_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Año:</label>
                            <select class="form-control" id="anio_id" name="anio_id" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_anio))
                                    @foreach ($cat_anio as $anio)
                                        <option value="{{$anio->id}}" <?php if(isset($data->anio_id)){if($data->anio_id == $anio->id) echo "selected";} ?> >{{$anio->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="anio_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Combustible:</label>
                            <select class="form-control" id="combustible" name="combustible" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_combustible))
                                    @foreach ($cat_combustible as $combustible)
                                        <option value="{{$combustible->id}}" <?php if(isset($data->combustible)){if($data->combustible == $combustible->id) echo "selected";} ?>>{{$combustible->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="combustible_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Color:</label>
                            <select class="form-control" id="color_id" name="color_id" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_color))
                                    @foreach ($cat_color as $color)
                                        <option value="{{$color->id}}" <?php if(isset($data->color_id)){if($data->color_id == $color->id) echo "selected";} ?> > {{$color->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="color_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Motor:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->motor)){print_r($data->motor);} ?>"  id="motor" name="motor" placeholder="">
                            <div id="motor_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">N° Motor:</label>
                            <input type="text" class="form-control" value="<?php if (isset($data->n_motor)) {print_r($data->n_motor);} ?>" id="n_motor" name="n_motor" placeholder="">
                            <div id="n_motor_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Transmisión:</label>
                            <select class="form-control" id="transmision" name="transmision" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_transmision))
                                    @foreach ($cat_transmision as $transmision)
                                        <option value="{{$transmision->nombre}}" <?php if(isset($data->transmision)){if($data->transmision == $transmision->nombre) echo "selected";} ?>>{{$transmision->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="transmision_error" class="invalid-feedback"></div> 
                        </div> 
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">N° cilindros:</label>
                            <input type="text" class="form-control" maxlength="2" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php if(isset($data->numero_cilindros)){print_r($data->numero_cilindros);} ?>"  id="numero_cilindros" name="numero_cilindros" placeholder="">
                            <div id="numero_cilindros_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Capacidad:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->capacidad)){print_r($data->capacidad);} ?>"  id="capacidad" name="capacidad" placeholder="">
                            <div id="capacidad_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">N° de puertas:</label>
                            <input type="number" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php if(isset($data->numero_puertas)){print_r($data->numero_puertas);} ?>" id="numero_puertas" name="numero_puertas" placeholder="">
                            <div id="numero_puertas_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">VIN:</label>
                            <input type="text" class="form-control" maxlength="17" onblur="generar_serie_corta()" value="<?php if(isset($data->vin)){print_r($data->vin);} ?>"  id="vin" name="vin" style="text-transform: uppercase;">
                            <div id="vin_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Serie Corta:</label>
                            <input type="text" class="form-control" maxlength="9" value="<?php if(isset($data->serie_corta)){print_r($data->serie_corta);} ?>"  id="serie_corta" name="serie_corta" style="text-transform: uppercase;">
                            <div id="serie_corta_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Placas:</label>
                            <input type="text" class="form-control" maxlength="10" value="<?php if(isset($data->placas)){print_r($data->placas);} ?>"  id="placas" name="placas" style="text-transform: uppercase;">
                            <div id="placas_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="">NOTAS:</label>
                            <textarea class="form-control" cols="2" id="notas" name="notas"><?php if(isset($data->notas)){print_r($data->notas);} ?></textarea>
                            <div id="notas_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" align="center">
                        <br>
                        <input type="hidden" id="id_cliente" value="<?php if(isset($data->cliente_id)){print_r($data->cliente_id);} else{ if (isset($id_cliente)) {echo $id_cliente;} else {echo '0';} } ?>">

                        @if (isset($data->id))
                            <button type="button" id="editar_registro" class="btn btn-success col-md-4"> Actualizar</button>
                        @else
                            <button type="button" id="guardar_registro" class="btn btn-success col-md-4"> Guardar</button>
                        @endif
                        
                        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
                    </div>
                </div>
                        
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        //Para guardar registro por primera vez
        $("#guardar_registro").on('click', function() {
            $(".invalid-feedback").html("");
            //Validamos campos de telefono y correo
            var id_cliente = document.getElementById("id_cliente").value;
            ajax.post('api/vehiculos-clientes', procesarRegistro(), function(response, headers) {
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }

                var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
                utils.displayWarningDialog(titulo, "success", function(data) {
                    return window.location.href = base_url + 'catalogos/clientesController/listado_vehiculo/'+id_cliente;
                })
            })
        });

        //Para actualizar un registro
        $("#editar_registro").on('click', function() {
            $(".invalid-feedback").html("");
            //Validamos campos de telefono y correo
            var id_cliente = document.getElementById("id_cliente").value;
            var id = $("#alta_registro").data('id');
            ajax.put('api/vehiculos-clientes/'+id, procesarRegistro(), function(response, headers) {
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }
                
                var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
                utils.displayWarningDialog(titulo, "success", function(data) {
                    return window.location.href = base_url + 'catalogos/clientesController/listado_vehiculo/'+id_cliente;
                })
            })
        });
        
        //Armamos el formulario a enviar
        let procesarRegistro = function() {
            let newArray = {
                kilometraje: document.getElementById("kilometraje").value,
                marca_id: document.getElementById("marca_id").value,
                modelo_id: "1",    //document.getElementById("modelo_id").value,
                anio_id: document.getElementById("anio_id").value,
                color_id: document.getElementById("color_id").value,
                unidad_descripcion: document.getElementById("unidad_descripcion").value,
                numero_puertas: document.getElementById("numero_puertas").value,
                combustible: document.getElementById("combustible").value,
                n_motor: document.getElementById("n_motor").value,
                motor: document.getElementById("motor").value,
                transmision: document.getElementById("transmision").value,
                numero_cilindros: document.getElementById("numero_cilindros").value,
                catalogo_id: document.getElementById("catalogo_id").value,
                capacidad: document.getElementById("capacidad").value,
                vin: document.getElementById("vin").value.toUpperCase(),
                serie_corta: document.getElementById("serie_corta").value.toUpperCase(),
                placas: document.getElementById("placas").value.toUpperCase(),
                cliente_id: document.getElementById("id_cliente").value,
                notas: document.getElementById("notas").value,
            };

            return newArray;
        }

        function generar_serie_corta() {
            var serie = document.getElementById("vin").value;
            //console.log(serie);
            if (serie.length > 8) {
                var serie_corta = serie.substring((serie.length -8),serie.length);
                $("#serie_corta").val(serie_corta);
            } else{
                $("#serie_corta").val(serie);
            }
        }
    </script>
@endsection