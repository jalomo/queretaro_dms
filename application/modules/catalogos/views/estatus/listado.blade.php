@layout('tema_luna/layout')
@section('contenido')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>
    <div class="row mb-3">
        <div class="col-md-8">
        </div>
        <div class="col-md-4" align="right">
            <a class="btn btn-primary" href="<?php echo base_url('catalogos/EstatusController/crear') ?>">Registrar</a>
        </div>
        
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_unidades" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Estatus</th>
                            <th>-</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($data))
                            @foreach ($data as $item)
                            <tr>
                                <td>#</td>
                                <td>{{$item->estatus}}</td>
                                <td>
                                    <a href="{{ base_url('catalogos/EstatusController/editar/'.$item->id) }}" class="btn btn-success" type="button">
                                        <i class="fas fa-pen"></i>
                                    </a>
                                </td>
                                <td>
                                    <button class="btn btn-success" type="button"><i class="far fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td>1</td>
                                <td>Transito Planta</td>
                                <td>
                                    <a href="{{ base_url('catalogos/EstatusController/editar/1') }}" class="btn btn-success" type="button"><i class="fas fa-pen"></i></a>
                                </td>
                                <td>
                                    <a href="#" class="btn btn-danger" type="button"><i class="far fa-trash-alt"></i></a>
                                </td>
                            </tr>

                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Estatus</th>
                            <th>-</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
	<script>
		/*$(document).ready(function() {
            try {
                $('#tbl_unidades').DataTable({
                    "ajax": `${base_url}autos/Unidades/getDataUnidades`,
                    columns: [
                        { 'data':'id'},
                        { 'data':'categoria'},
                        { 'data':'subcategoria'},
                        { 'data':'anio'},
                        { 'data':'color'},
                        { 'data':'transmision'},
                        { 'data':'motor'},
                        {'data':function(data)
                            {
                                return "<a class='btn btn-success' href='"+base_url+'autos/Unidades/editar/'+data.id+"'> Editar </a>";
                            }
                        },
                        {'data':function(data)
                            {
                                return "<button type='button' onclick='borrar("+data.id+")' class='btn-borrar btn btn-danger' data-id="+data.id+">Borrar</button>";
                            }
                        }
                    ]
                })
            }
            catch(err) {
              Swal.fire(
                  'Borrado!',
                  'El registro se ha borrado',
                  'success'
                );
            }
			
        });
        /*$("#tbl_productos").on("click", ".btn-borrar", function(){
            var id = $(this).data('id')
            borrar(id) 
        });*/

        /*    function borrar(id){
                Swal.fire({
              title: 'Estás seguro?',
              text: "Esta acción no se puede revertir!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, borrar!'
            }).then((result) => {
              if (result.value) {
                    $.ajax({
                    url: base_url+'refacciones/productos/delProducto/'+id,
                    success: function(respuesta) {
                    Swal.fire(
                      'Borrado!',
                      'El registro se ha borrado',
                      'success'
                    )
                    location.reload(true)
                   // 
                    },
                    error: function() {
                        console.log("No se ha podido obtener la información");
                    }
                    });   
                 }
            })
             }
             function fnDelay() { // Function is defined here
            setTimeout(function(){ alert("ok");}, 5000);
                                 }
            */


	</script>
@endsection