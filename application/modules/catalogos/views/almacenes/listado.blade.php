@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>
    <div class="row mb-4">
        <div class="col-md-10"></div>
        <div class="col-md-2">
            <a href="{{ base_url('catalogos/almacenesController/crear/') }}" class="btn btn-success col-md-12" type="button">Registrar</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table" id="tabla-facturas">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">nombre</th>
                    <th scope="col">ubicacion</th>
                    <th scope="col">codigo almacen</th>
                    <th scope="col">- </th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data as $item)
                    <tr>
                        <td scope="row">{{ $item->id }}</td>
                        <td>{{ $item->nombre }}</td>
                        <td>{{ $item->ubicacion }}</td>
                        <td>{{ $item->codigo_almacen }}</td>
                        <td><a href="{{ base_url('catalogos/almacenesController/editar/'.$item->id) }}" class="btn btn-primary" type="button">Editar</a></td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$("#tabla-facturas").DataTable({});
</script>
@endsection