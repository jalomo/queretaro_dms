<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MarcasController extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

    public function index()
    {
        $this->load->library('curl');
        $this->load->helper('general');
        
        $dataFromApi = $this->curl->curlGet('api/catalogo-marcas');
        $dataregistros = procesarResponseApiJsonToArray($dataFromApi);
        $data['data'] = isset($dataregistros) ? $dataregistros : [];

        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Marcas";
        $data['subtitulo'] = "Listado";

        $this->blade->render('marca/listado', $data);
    }

    public function crear()
    {
        $this->load->library('curl');
        $this->load->helper('general');
        
        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Marcas";
        $data['subtitulo'] = "Registro marca";

        $this->blade->render('marca/formulario', $data);
    }

    public function editar($id)
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');

            $dataFromApi = $this->curl->curlGet('api/catalogo-marcas/' . $id);
            $dataregistro = procesarResponseApiJsonToArray($dataFromApi);
            $data['data'] = isset($dataregistro)  ? $dataregistro : [];
            
            $data['modulo'] = "Catálogos";
            $data['titulo'] = "Marcas";
            $data['subtitulo'] = "Listado";
            
            $this->blade->render('marca/formulario', $data);
        }
    }

    public function ajax_catalogo()
    {
        $this->load->helper('general');
        $this->load->library('curl');

        $responseData = $this->curl->curlGet('api/catalogo-marcas');
        $response = procesarResponseApiJsonToArray($responseData);
        $data['data'] = count($response) > 0 ? $response : [];
        echo json_encode($data);
    }
}

/* End of file MarcasController.php */
