<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ColoresController extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

    public function index()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $dataFromApi = $this->curl->curlGet('api/catalogo-colores');
        $dataregistros = procesarResponseApiJsonToArray($dataFromApi);
        $data['data'] = isset($dataregistros) ? $dataregistros : [];

        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Colores Exteriores";
        $data['subtitulo'] = "Listado";

        $this->blade->render('colores/listado', $data);
    }

    public function crear()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Colores Exteriores";
        $data['subtitulo'] = "Registro";

        $this->blade->render('colores/formulario', $data);
    }

    public function editar($id)
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');
            
            $dataFromApi = $this->curl->curlGet('api/catalogo-colores/' . $id);
            $dataregistro = procesarResponseApiJsonToArray($dataFromApi);

            $data['data'] = isset($dataregistro)  ? $dataregistro : [];
            
            $data['modulo'] = "Catálogos";
            $data['titulo'] = "Colores Exteriores";
            $data['subtitulo'] = "Editar";
            
            $this->blade->render('colores/formulario', $data);
        }
    }

    public function ajax_catalogo()
    {
        $this->load->helper('general');
        $this->load->library('curl');

        $responseData = $this->curl->curlGet('api/catalogo-colores');
        $response = procesarResponseApiJsonToArray($responseData);
        $data['data'] = count($response) > 0 ? $response : [];
        echo json_encode($data);
    }
}

/* End of file ColoresController.php */
