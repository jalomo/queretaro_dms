<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Oasis extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'url', 'date', 'general'));
        $this->load->library('curl');
        date_default_timezone_set('America/Mexico_City');
    }
    public function listado()
    {
        //$data['info'] = procesarResponseApiJsonToArray($this->curl->curlGet('api/unidades/get-all', []));
        $data['titulo'] = "Listado unidades";
        $data['bitacora'] = "inicial";
        $this->blade->render('listado', $data);
    }
    public function cambiar_estatus_pilot()
    {
        $data['remision_id'] = $_GET['remision_id'];
        $estatus_id_pilot = $_GET['estatus_id_pilot'];
        $data['unidad_disponible'] = $_GET['unidad_disponible'];
        $data['pilot_id'] = $_GET['pilot_id'];
        $data['unidad_disponible'] = $_GET['unidad_disponible'];
        $estatus = procesarResponseApiJsonToArray($this->curl->curlGet('api/estatus-unidades-nuevas', []));

        $data['estatus'] = form_dropdown('estatus_id', array_combos($estatus, 'id', 'estatus', TRUE), $estatus_id_pilot, 'class="form-control " id="estatus_id"');
        $this->blade->render('cambiar_estatus_pilot', $data);

    }
    public function mantenimiento($id = 0)
    {
        $info = procesarResponseApiJsonToArray($this->curl->curlGet('api/unidades/' . $id));
        $historialMantenimientos = procesarResponseApiJsonToArray($this->curl->curlGet('api/historial-mantenimiento/' . $id));
        if (count($info) > 0) {
            $info = $info[0];
        }
        $data['titulo'] = "Mantenimiento de unidades";
        $data['bitacora'] = "inicial";
        $data['unidad'] = $info;
        $data['remision_id'] = $id;
        $data['historialMantenimientos'] = $historialMantenimientos;
        $this->blade->render('mantenimiento_unidad', $data);
    }

    public function mantenimientopdf($id)
    {
        $data['unidad'] = procesarResponseApiJsonToArray($this->curl->curlGet('api/historial-mantenimiento-id/' . $id, []));
        $view = $this->blade->render('pdf_mantenimiento', $data);
        $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
    }
    public function mantenimientospdf()
    {
        $idsMantenimientos = $this->input->post('ids_pdf');
        $id = $this->input->post('remision_id');
        $ArrayPartes = explode("-", $idsMantenimientos);
        $idsMantenimientos = $ArrayPartes;
        $arrayIdsMantenimientos = [];
        for ($i = 0; $i < count($idsMantenimientos); $i++) {
            if ($idsMantenimientos[$i] > 0 && $idsMantenimientos != '') {
                array_push($arrayIdsMantenimientos, $idsMantenimientos[$i]);
            }
        }
        $data['unidad'] = procesarResponseApiJsonToArray($this->curl->curlGet('api/unidades/' . $id, []));
        $data['mantenimientos'] = procesarResponseApiJsonToArray($this->curl->curlGet('api/historial-mantenimiento/' . $id, []));
        $data['mantenimientos-array'] = procesarResponseApiJsonToArray($this->curl->curlPost('api/historial-mantenimientos/', $arrayIdsMantenimientos));
        $historialMantenimientos = $data['mantenimientos-array'];
        for ($i = 0; $i < count($historialMantenimientos); $i++) {
            $fechaPartes = $this->formatoFecha($historialMantenimientos[$i]->fecha_mantenimiento);
            $historialMantenimientos[$i]->fecha_dia = $fechaPartes[0];
            $historialMantenimientos[$i]->fecha_mes = $fechaPartes[1];
            $historialMantenimientos[$i]->fecha_year = $fechaPartes[2];
        }
        $arraySerie = [];
        try {
            $arraySerie = str_split($data['unidad'][0]->serie);
        } catch (\Throwable $th) {
            $vin = "NA";
            $arraySerie = str_split($vin);
        }
        $data['serieUnidad'] = $arraySerie;
        $data['mantenimientos'] = $historialMantenimientos;
        $vista = $this->load->view('oasis/pdf_mantenimiento_php', $data, true);
        $dompdf = new Dompdf\Dompdf();
        $options = new \Dompdf\Options();
        $options->set('isRemoteEnabled', true);
        $dompdf->setOptions($options);
        $dompdf->loadHtml($vista);
        $dompdf->render();
        $dompdf->stream('Mantenimientos');
        $this->load->helper('pdf');
        pdf_create("Formato_Salida.pdf", $vista, 3, false); //Revisar para que es
    }
    public function unidades()
    {
        //$data['info'] = procesarResponseApiJsonToArray($this->curl->curlGet('api/unidades/get-all', []));
        $data['titulo'] = "Listado unidades";
        $data['bitacora'] = "inicial";
        $this->blade->render('listado_mantenimiento', $data);
    }
    /*Separar fecha de mantenimiento por dia, mes y año*/
    public function formatoFecha($fecha)
    {
        try {
            $arrayFecha = explode("-", $fecha);
        } catch (\Throwable $th) {
            return [0 => "01", 1 => "01", 2 => "2000"];
        }
        return $arrayFecha;
    }

    public function agregar($id = 0)
    {
        if ($id == 0) {
            $info = new Stdclass();
            $info_costos = new Stdclass();
            $info_detalle_remision = new Stdclass();
            $info_facturacion = new Stdclass();
            $info_memo_compras = new Stdclass();
            $data['equipo_especial'] = [];
            $data['unique'] = '';
        } else {
            $info = procesarResponseApiJsonToArray($this->curl->curlGet('api/unidades/' . $id));
            $info = $info[0];
            $data['unique'] = $info->unique;
            $info_costos = procesarResponseApiJsonToArray($this->curl->curlGet('api/get-detalle-costos-remision/' . $id));
            $info_detalle_remision = procesarResponseApiJsonToArray($this->curl->curlGet('api/get-detalle-remision/' . $id));
            $info_facturacion = procesarResponseApiJsonToArray($this->curl->curlGet('api/get-facturacion-unidades-nuevas/' . $id));
            $info_memo_compras = procesarResponseApiJsonToArray($this->curl->curlGet('api/get-memo-compra/' . $id));
            $data['equipo_especial'] = procesarResponseApiJsonToArray($this->curl->curlGet('api/get-equipo-especial-remision/' . $id));
        }
        //REMISIÓN
        $lineas = procesarResponseApiJsonToArray($this->curl->curlGet('api/cat-lineas'));
        $array_lineas = [];
        foreach ($lineas as $l => $linea) {
            $item = new \stdClass;
            $item->id = $linea->id;
            $item->nombre = $linea->modelo . ' ' . $linea->descripcion;
            array_push($array_lineas, $item);
        };
        $data['linea_id'] = form_dropdown('linea_id', array_combos($array_lineas, 'id', 'nombre', TRUE), set_value('linea_id', exist_obj($info, 'linea_id')), 'class="form-control busqueda" id="linea_id"');

        $lineas_contabilidad = procesarResponseApiJsonToArray($this->curl->curlGet('http://35.233.204.99/dms/contabilidad_queretaro/index.php/linea_autos/api', true));
        $data['linea_id_contabilidad'] = form_dropdown('linea_id_contabilidad', array_combos($lineas_contabilidad, 'id', 'linea', TRUE), set_value('linea_id_contabilidad', exist_obj($info, 'linea_id_contabilidad')), 'class="form-control busqueda" id="linea_id_contabilidad"');

        $catalogos_precios = procesarResponseApiJsonToArray($this->curl->curlPost('api/precios/unidades/get-parameters', []));
        $array_cat = [];
        foreach ($catalogos_precios as $a => $cc) {
            $new =  new Stdclass();
            $new->cat = $cc->cat;
            $new->descripcion = $cc->cat . ' - ' . str_replace('\"', '', $cc->descripcion);

            $array_cat[$a] = $new;
        }

        $data['cat'] = form_dropdown('cat', array_combos($array_cat, 'cat', 'descripcion', TRUE), set_value('cat', exist_obj($info, 'cat')), 'class="form-control busqueda search_price" id="cat"');
        //$data['cat'] = form_input('cat', set_value('cat', exist_obj($info, 'cat')), 'class="form-control search_price" id="cat"');
        $data['clave_vehicular'] = form_input('clave_vehicular', set_value('clave_vehicular', exist_obj($info, 'clave_vehicular')), 'class="form-control search_price" id="clave_vehicular"');
        $data['unidad_descripcion'] = form_input('unidad_descripcion', set_value('unidad_descripcion', exist_obj($info, 'unidad_descripcion')), 'class="form-control" id="unidad_descripcion"');
        $data['economico'] = form_input('economico', set_value('economico', exist_obj($info, 'economico')), 'class="form-control" id="economico" readonly');
        $data['economico_visual'] = form_input('economico_visual', set_value('economico_visual', exist_obj($info, 'economico_visual')), 'class="form-control" id="economico_visual"');
        $data['descuento_maximo'] = form_input('des_max', set_value('des_max', exist_obj($info, 'des_max')), 'class="form-control numeric" maxlength="10" id="des_max"');
        $data['tenencia'] = form_input('tenencia', set_value('tenencia', exist_obj($info, 'tenencia')), 'class="form-control numeric" maxlength="10" id="tenencia"');
        $data['seguro_cob_amplia'] = form_input('seg_cob_amplia', set_value('seg_cob_amplia', exist_obj($info, 'seg_cob_amplia')), 'class="form-control numeric" maxlength="10" id="seg_cob_amplia"');
        $data['plan_gane'] = form_input('plan_gane', set_value('plan_gane', exist_obj($info, 'plan_gane')), 'class="form-control numeric" maxlength="10" id="plan_gane"');
        $data['orden_reporte'] = form_input('orden_reporte', set_value('orden_reporte', exist_obj($info, 'orden_reporte')), 'class="form-control" id="orden_reporte"');
        $unidad_importada = set_value('unidad_importada', exist_obj($info, 'unidad_importada'));
        $data['unidad_importada_si'] = form_radio('unidad_importada', 1, ($unidad_importada) ? true : false, 'class="" id="unidad_importada_si"');
        $data['unidad_importada_no'] = form_radio('unidad_importada', 0, ($unidad_importada) ? false : true, 'class="" id="unidad_importada_no"');
        $tipos_auto = [
            '' => 'Selecciona',
            'A' => 'Auto',
            'C' => 'Camión'
        ];
        $nacional_importado = [
            '' => 'Selecciona',
            'Nacional' => 'Nacional',
            'Importado' => 'Importado'
        ];

        $data['pedimento'] = form_input('pedimento', set_value('pedimento', exist_obj($info, 'pedimento')), 'class="form-control" id="pedimento"');
        $data['fecha_pedimento'] = form_input('fecha_pedimento', set_value('fecha_pedimento', exist_obj($info, 'fecha_pedimento')), 'class="form-control" id="fecha_pedimento"', 'date');
        $data['fecha_remision'] = form_input('fecha_remision', set_value('fecha_remision', exist_obj($info, 'fecha_remision')), 'class="form-control" id="fecha_remision"', 'date');



        $data['tipo_auto'] = form_dropdown('tipo_auto', $tipos_auto, set_value('tipo_auto', exist_obj($info, 'tipo_auto')), 'class="form-control busqueda" id="tipo_auto"');
        $data['nacional_importado'] = form_dropdown('nacional_importado', $nacional_importado, set_value('nacional_importado', exist_obj($info, 'nacional_importado')), 'class="form-control busqueda" id="nacional_importado"');
        $data['clave_isan'] = form_input('clave_isan', set_value('clave_isan', exist_obj($info, 'clave_isan')), 'class="form-control" id="clave_isan"');
        //Cuentas
        $data['cta_menudeo'] = form_input('cta_menudeo', set_value('cta_menudeo', exist_obj($info, 'cta_menudeo')), 'class="form-control" id="cta_menudeo"');
        $data['cta_flotilla'] = form_input('cta_flotilla', set_value('cta_flotilla', exist_obj($info, 'cta_flotilla')), 'class="form-control" id="cta_flotilla"');
        $data['cta_conauto'] = form_input('cta_conauto', set_value('cta_conauto', exist_obj($info, 'cta_conauto')), 'class="form-control" id="cta_conauto"');
        $data['cta_intercambio'] = form_input('cta_intercambio', set_value('cta_intercambio', exist_obj($info, 'cta_intercambio')), 'class="form-control" id="cta_intercambio"');
        $data['cta_plleno'] = form_input('cta_plleno', set_value('cta_plleno', exist_obj($info, 'cta_plleno')), 'class="form-control" id="cta_plleno"');
        $data['cta_inventario'] = form_input('cta_inventario', set_value('cta_inventario', exist_obj($info, 'cta_inventario')), 'class="form-control" id="cta_inventario"');
        //Ventas
        $data['venta_menudeo'] = form_input('venta_menudeo', set_value('venta_menudeo', exist_obj($info, 'venta_menudeo')), 'class="form-control" id="venta_menudeo"');
        $data['vta_flotilla'] = form_input('vta_flotilla', set_value('vta_flotilla', exist_obj($info, 'vta_flotilla')), 'class="form-control" id="vta_flotilla"');
        $data['vta_conauto'] = form_input('vta_conauto', set_value('vta_conauto', exist_obj($info, 'vta_conauto')), 'class="form-control" id="vta_conauto"');
        $data['vta_intercambio'] = form_input('vta_intercambio', set_value('vta_intercambio', exist_obj($info, 'vta_intercambio')), 'class="form-control" id="vta_intercambio"');
        $data['venta_plleno'] = form_input('venta_plleno', set_value('venta_plleno', exist_obj($info, 'venta_plleno')), 'class="form-control" id="venta_plleno"');



        $data['serie'] = form_input('serie', set_value('serie', exist_obj($info, 'serie')), 'class="form-control" id="serie" maxlength="17"');
        $data['serie_corta'] = form_input('serie_corta', set_value('serie_corta', exist_obj($info, 'serie_corta')), 'class="form-control" id="serie_corta" maxlength="6" readonly');
        $data['motor'] = form_input('motor', set_value('motor', exist_obj($info, 'motor')), 'class="form-control" id="motor"');
        $unidad_intercambio = set_value('intercambio', exist_obj($info, 'intercambio'));
        $data['unidad_intercambio_si'] = form_radio('intercambio', 1, ($unidad_intercambio) ? true : false, 'class="" id="unidad_intercambio_si"');
        $data['unidad_intercambio_no'] = form_radio('intercambio', 0, ($unidad_intercambio) ? false : true, 'class="" id="unidad_intercambio_no"');
        $proveedores = procesarResponseApiJsonToArray($this->curl->curlGet('api/cat-proveedores-un'));
        $data['proveedor_id'] = form_dropdown('proveedor_id', array_combos($proveedores, 'id', 'nombre', TRUE), set_value('proveedor_id', exist_obj($info, 'proveedor_id')), 'class="form-control busqueda" id="proveedor_id"');

        //Precios y costos remisión unidad
        $anios = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-anio'));
        $data['id_anio'] = form_dropdown('id_anio', array_combos($anios, 'id', 'nombre', TRUE), set_value('id_anio', exist_obj($info, 'id_anio')), 'class="form-control busqueda" id="id_anio"');
        $data['vin'] = form_input('vin', set_value('vin', exist_obj($info, 'vin')), 'class="form-control" id="vin" ');
        $ubicaciones = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-ubicacion'));
        $data['id_ubicacion'] = form_dropdown('id_ubicacion', array_combos($ubicaciones, 'id', 'nombre', TRUE), set_value('id_ubicacion', exist_obj($info, 'id_ubicacion')), 'class="form-control busqueda" id="id_ubicacion"');
        $llaves = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-ubicacion-llaves'));
        $data['id_ubicacion_llaves'] = form_dropdown('id_ubicacion_llaves', array_combos($llaves, 'id', 'nombre', TRUE), set_value('id_ubicacion_llaves', exist_obj($info, 'id_ubicacion_llaves')), 'class="form-control busqueda" id="id_ubicacion_llaves"');
        $data['leyenda_dcto'] = form_textarea('leyenda_dcto', set_value('leyenda_dcto', exist_obj($info, 'leyenda_dcto')), 'class="form-control" id="leyenda_dcto"');

        //Detalle remisión
        $data['puertas'] = form_input('puertas', set_value('puertas', exist_obj($info_detalle_remision, 'puertas')), 'class="form-control numeric" maxlength="10" id="puertas"');
        $data['cilindros'] = form_input('cilindros', set_value('cilindros', exist_obj($info_detalle_remision, 'cilindros')), 'class="form-control numeric" maxlength="10" id="cilindros"');
        $data['transmision'] = form_input('transmision', set_value('transmision', exist_obj($info_detalle_remision, 'transmision')), 'class="form-control" maxlength="10" id="transmision"');
        $data['capacidad'] = form_input('capacidad', set_value('capacidad', exist_obj($info_detalle_remision, 'capacidad')), 'class="form-control numeric" maxlength="10" id="capacidad"');
        $data['capacidad_kg'] = form_input('capacidad_kg', set_value('capacidad_kg', exist_obj($info_detalle_remision, 'capacidad_kg')), 'class="form-control numeric" maxlength="10" id="capacidad_kg"');

        $combustible = procesarResponseApiJsonToArray($this->curl->curlGet('api/cat-combustible'));
        $data['combustible_id'] = form_dropdown('combustible_id', array_combos($combustible, 'id', 'descripcion', TRUE), set_value('combustible_id', exist_obj($info_detalle_remision, 'combustible_id')), 'class="form-control busqueda" id="combustible_id"');
        $colores = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-colores'));
        $data['colorIntId'] = form_dropdown('colorIntId', array_combos($colores, 'id', 'nombre', TRUE), set_value('colorIntId', exist_obj($info_detalle_remision, 'color_int_id')), 'class="form-control busqueda" id="colorIntId"');
        $data['colorExtId'] = form_dropdown('colorExtId', array_combos($colores, 'id', 'nombre', TRUE), set_value('colorExtId', exist_obj($info_detalle_remision, 'color_ext_id')), 'class="form-control busqueda" id="colorExtId"');


        //Facturación
        $data['procedencia'] = form_input('procedencia', set_value('procedencia', exist_obj($info_facturacion, 'procedencia')), 'class="form-control" id="procedencia"');
        $data['vendedor'] = form_input('vendedor', set_value('vendedor', exist_obj($info_facturacion, 'vendedor')), 'class="form-control" id="vendedor"');

        $catalogos_aduanas = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-aduanas', []));
        $data['aduana'] = form_dropdown('id_aduana', array_combos($catalogos_aduanas, 'id', 'aduana', TRUE), set_value('id_aduana', exist_obj($info, 'aduana_id')), 'class="form-control busqueda" id="id_aduana"');
        $data['repuve'] = form_input('repuve', set_value('repuve', exist_obj($info_facturacion, 'repuve')), 'class="form-control" id="repuve"');

        //MEMO COMPRAS
        $data['mes'] = form_input('mes', set_value('mes', exist_obj($info_memo_compras, 'mes')), 'class="form-control" max id="mes"');
        $data['no_producto'] = form_input('no_producto', set_value('no_producto', exist_obj($info_memo_compras, 'no_producto')), 'class="form-control numeric" max id="no_producto"');
        $data['memo'] = form_input('memo', set_value('memo', exist_obj($info_memo_compras, 'memo')), 'class="form-control" max id="memo"');
        $data['costo_remision'] = form_input('costo_remision', set_value('costo_remision', exist_obj($info_memo_compras, 'costo_remision')), 'class="form-control" max id="costo_remision"');
        $data['mes_compra'] = form_input('mes_compra', set_value('mes_compra', exist_obj($info_memo_compras, 'mes_compra')), 'class="form-control" max id="mes_compra"');
        $valor_fp = set_value('fp', exist_obj($info_memo_compras, 'fp'));
        $data['fp'] = form_input('fp', ($valor_fp) ? $valor_fp : 'R99', 'class="form-control" max id="fp"');
        $valor_dias = set_value('dias', exist_obj($info_memo_compras, 'dias'));
        $data['dias'] = form_input('dias', ($valor_dias) ? $valor_dias : '45', 'class="form-control numeric" max id="dias"');
        $data['cm'] = form_input('cm', set_value('cm', exist_obj($info_memo_compras, 'cm')), 'class="form-control" max id="cm"');

        $comprar = set_value('comprar', exist_obj($info_memo_compras, 'comprar'));
        $data['comprar_si'] = form_radio('comprar', 1, ($comprar) ? true : false, 'class="" id="comprar_si"');
        $data['comprar_no'] = form_radio('comprar', 0, ($comprar) ? false : true, 'class="" id="comprar_no"');

        //COSTOS / PRECIOS
        $data['costo_valor_unidad'] = form_input('c_valor_unidad', set_value('c_valor_unidad', exist_obj($info_costos, 'c_valor_unidad')), 'class="form-control bgwhite numeric" maxlength="10" id="c_valor_unidad"');

        $data['costo_equipo_base'] = form_input('c_equipo_base', set_value('c_equipo_base', exist_obj($info_costos, 'c_equipo_base')), 'class="form-control bgwhite numeric" maxlength="10" id="c_equipo_base"');
        // $data['costo_total_base'] = form_input('c_total_base', set_value('c_total_base', exist_obj($info_costos, 'c_total_base')), 'class="form-control bgwhite numeric" maxlength="10" id="c_total_base"');
        // $data['costo_deducciones_ford'] = form_input('c_deduccion_ford', set_value('c_deduccion_ford', exist_obj($info_costos, 'c_deduccion_ford')), 'class="form-control bgwhite numeric" maxlength="10" id="c_deduccion_ford"');
        // $data['costo_seguros_traslados'] = form_input('c_seg_traslado', set_value('c_seg_traslado', exist_obj($info_costos, 'c_seg_traslado')), 'class="form-control bgwhite numeric" maxlength="10" id="c_seg_traslado"');
        $data['costo_gastos_traslados'] = form_input('c_gastos_traslado', set_value('c_gastos_traslado', exist_obj($info_costos, 'c_gastos_traslado')), 'class="form-control bgwhite numeric" maxlength="10" id="c_gastos_traslado"');
        // $data['costo_imp_imp'] = form_input('c_imp_import', set_value('c_imp_import', exist_obj($info_costos, 'c_imp_import')), 'class="form-control bgwhite numeric" maxlength="10" id="c_imp_import"');
        // $data['costo_fletes_ext'] = form_input('c_fletes_ext', set_value('c_fletes_ext', exist_obj($info_costos, 'c_fletes_ext')), 'class="form-control bgwhite numeric" maxlength="10" id="c_fletes_ext"');
        $data['costo_isan'] = form_input('c_isan', set_value('c_isan', exist_obj($info_costos, 'c_isan')), 'class="form-control bgwhite numeric" maxlength="10" id="c_isan"');
        $data['c_holdback'] = form_input('c_holdback', set_value('c_holdback', exist_obj($info_costos, 'c_holdback')), 'class="form-control bgwhite numeric" maxlength="10" id="c_holdback"');
        $data['c_cuenta_pub'] = form_input('c_cuenta_pub', set_value('c_cuenta_pub', exist_obj($info_costos, 'c_cuenta_pub')), 'class="form-control bgwhite numeric" maxlength="10" id="c_cuenta_pub"');
        $data['c_donativo_ccf'] = form_input('c_donativo_ccf', set_value('c_donativo_ccf', exist_obj($info_costos, 'c_donativo_ccf')), 'class="form-control bgwhite numeric" maxlength="10" id="c_donativo_ccf"');
        $data['c_plan_piso'] = form_input('c_plan_piso', set_value('c_plan_piso', exist_obj($info_costos, 'c_plan_piso')), 'class="form-control bgwhite numeric" maxlength="10" id="c_plan_piso"');
        $data['costo_subtotal'] = form_input('costo_subtotal', set_value('costo_subtotal', exist_obj($info, 'c_subtotal')), 'class="form-control bgwhite numeric" maxlength="10" id="costo_subtotal"');
        $data['costo_total'] = form_input('costo_total', set_value('costo_total', exist_obj($info, 'c_total')), 'class="form-control bgwhite numeric" maxlength="10" id="costo_total"');
        $data['costo_iva'] = form_input('costo_iva', set_value('costo_iva', exist_obj($info, 'c_iva')), 'class="form-control bgwhite numeric" maxlength="10" id="costo_iva"');
        $data['venta_iva'] = form_input('venta_iva', set_value('venta_iva', exist_obj($info, 'v_iva')), 'class="form-control bgwhite numeric" maxlength="10" id="venta_iva"');


        $data['no_inventario'] = form_input('no_inventario', set_value('no_inventario', exist_obj($info, 'no_inventario')), 'class="form-control" id="no_inventario" disabled');
        $data['clave_sat'] = form_input('clave_sat', set_value('clave_sat', exist_obj($info, 'clave_sat')), 'class="form-control" id="clave_sat"');
        $data['unidad'] = form_input('unidad', set_value('unidad', exist_obj($info, 'unidad')), 'class="form-control" id="unidad"');
        $data['modelo'] = form_input('modelo', set_value('modelo', exist_obj($info, 'modelo')), 'class="form-control" id="modelo"');

        $url_cuentas = "http://35.233.204.99/dms/contabilidad_queretaro/linea_autos/api_cuenta_inventario";
        $catalogos_cuentas = procesarResponseApiJsonToArray($this->curl->curlGet($url_cuentas, true));
        $data['catalogos_cuentas'] = $catalogos_cuentas->data;
        $array_cuentas = [];
        foreach ($catalogos_cuentas->data as $a => $cc) {
            $new =  new Stdclass();
            $new->id = $cc->id;
            $new->descripcion = $cc->cuenta . ' - ' . $cc->descripcion;

            $array_cuentas[$a] = $new;
        }


        $data['cuenta_inventario_unidad'] = form_dropdown('cuenta_inventario_unidad', array_combos($array_cuentas, 'id', 'descripcion', TRUE), set_value('cuenta_inventario_unidad', exist_obj($info, 'cuenta_inventario_unidad')), 'class="form-control busqueda" id="cuenta_inventario_unidad"');

        $estatus_oasis = procesarResponseApiJsonToArray($this->curl->curlGet('api/estatus-unidades-nuevas'));
        $data['id_status'] = form_dropdown('id_status', array_combos($estatus_oasis, 'id', 'estatus', TRUE), set_value('id_status', exist_obj($info, 'id_status')), 'class="form-control busqueda" id="id_status"');
        $data['id'] = $id;
        $data['titulo'] = 'Unidades nuevas';
        $this->blade->render('agregar', $data);
    }
    public function agregar_linea()
    {
        $anios = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-anio'));
        $data['modelo_cat'] = form_dropdown('modelo_cat', array_combos($anios, 'nombre', 'nombre', TRUE), '', 'class="form-control busqueda" id="modelo_cat"');
        $this->blade->render('agregar_linea', $data);
    }
    public function agregar_aduana()
    {
        $data['aduana'] = form_input('aduana', '', 'class="form-control" id="aduana"');
        $this->blade->render('agregar_aduana', $data);
    }
    public function agregar_cat()
    {
        $data['catalogo'] = form_input('catalogo', '', 'class="form-control" id="catalogo"');
        $data['_clave_vehicular'] = form_input('_clave_vehicular', '', 'class="form-control" id="_clave_vehicular"');
        $data['descripcion'] = form_input('descripcion', '', 'class="form-control" id="descripcion"');
        $this->blade->render('agregar_catalogo', $data);
    }
    public function add_color()
    {
        $data['color'] = form_input('color', '', 'class="form-control" id="color"');
        $data['clave_color'] = form_input('clave_color', '', 'class="form-control" id="clave_color"');
        $this->blade->render('agregar_color', $data);
    }
    public function add_equipo()
    {
        $id = $_POST['id'];
        if ($id == 0) {
            $info = new Stdclass();
        } else {
            $info = procesarResponseApiJsonToArray($this->curl->curlGet('api/equipo-especial-remision/' . $id));
        }

        $data['importe_eq'] = form_input('importe_eq', set_value('importe', exist_obj($info, 'importe')), 'class="form-control numeric" maxlength="10" id="importe_eq"');
        $data['descripcion_eq'] = form_input('descripcion_eq', set_value('descripcion', exist_obj($info, 'descripcion')), 'class="form-control" maxlength="10" id="descripcion_eq"');
        $data['id_equipo_especial'] = $id;
        $this->blade->render('agregar_equipo_especial', $data);
    }
    public function buscar_serie_qr($serie)
    {
        $this->recepcion_unidades(0, $serie);
    }
    public function recepcion_unidades($id = 0, $serie = '')
    {
        if ($id == 0) {
            $info = new Stdclass();
            $info_detalle_remision = new Stdclass();
            $today = date('Y-m-d');
            //$data['ultimo_servicio'] = date('Y-m-d', strtotime($today . ' + 14 days'));
            $data['ultimo_servicio'] = $today;
        } else {
            $info = procesarResponseApiJsonToArray($this->curl->curlGet('api/unidades/get-all?id=' . $id));
            $info = $info[0];
            $data['ultimo_servicio'] = '';
        }
        $data['id'] = $id;
        $data['parameter_serie'] = $serie;
        $data['titulo'] = "Recepción de unidades";
        $data['serie'] = form_input('serie', set_value('serie', exist_obj($info, 'serie')), 'class="form-control" id="serie" maxlength="17"');
        $data['unidad_descripcion'] = form_input('unidad_descripcion', set_value('unidad_descripcion', exist_obj($info, 'unidad_descripcion')), 'class="form-control" id="unidad_descripcion" readonly ');
        $data['modelo'] = form_input('modelo', set_value('modelo', exist_obj($info, 'modelo')), 'class="form-control" id="modelo" readonly ');
        $data['color_exterior'] = form_input('color_exterior', set_value('color_exterior', exist_obj($info, 'color_ext_id')), 'class="form-control" id="color_exterior" readonly ');
        $data['color_interior'] = form_input('color_interior', set_value('color_interior', exist_obj($info, 'color_int_id')), 'class="form-control" id="color_interior" readonly ');
        $data['economico'] = form_input('economico', set_value('economico', exist_obj($info, 'economico')), 'class="form-control" id="economico" readonly ');
        $data['comentario'] = form_textarea('comentario', set_value('comentario', exist_obj($info, 'comentario')), 'class="form-control" id="comentario"');
        $data['fecha_recepcion'] = form_input('fecha_recepcion', set_value('fecha_recepcion', exist_obj($info, 'fecha_recepcion')), 'class="form-control" id="fecha_recepcion"', 'date');
        $ubicaciones = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-ubicacion'));
        $data['ubicacion_id'] = form_dropdown('ubicacion_id', array_combos($ubicaciones, 'id', 'nombre', TRUE), set_value('ubicacion_id', exist_obj($info, 'ubicacion_id')), 'class="form-control busqueda" id="ubicacion_id"');
        $llaves = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-ubicacion-llaves'));
        $data['ubicacion_llaves_id'] = form_dropdown('ubicacion_llaves_id', array_combos($llaves, 'id', 'nombre', TRUE), set_value('ubicacion_llaves_id', exist_obj($info, 'ubicacion_llaves_id')), 'class="form-control busqueda" id="ubicacion_llaves_id"');
        $this->blade->render('recepcion_unidades', $data);
    }
    public function listado_unidades_recibidas()
    {
        //$data['info'] = procesarResponseApiJsonToArray($this->curl->curlGet('api/unidades/get-all?recepcion=1', []))->data;
        $data['titulo'] = "Listado recepción de unidades";
        $data['bitacora'] = "inicial";
        $this->blade->render('listado_recepcion_unidades', $data);
    }
    public function exportar_remision($id = '')
    {
        $data['tipos_auto'] = [
            '' => 'Selecciona',
            'A' => 'Auto',
            'C' => 'Camión',
            'caM' => 'Camioneta',
        ];
        $data['unidad'] = procesarResponseApiJsonToArray($this->curl->curlGet('api/unidades/' . $id, []));
        $view = $this->blade->render('pdf_remision', $data);
        $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
    }
    public function exportar_qr($serie = '', $remision_id = '')
    {
        $data['serie'] = $serie;
        $view = $this->blade->render('generar_qr', $data, TRUE);
        // print_r($view);die();
        $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
    }
    public function historial_unidad($unidad_id = '')
    {
        $data['info'] = procesarResponseApiJsonToArray($this->curl->curlGet('api/historial-recepcion-unidad/' . $unidad_id, []));
        $data['titulo'] = 'Historial cambio estatus unidad';
        $this->blade->render('historial_estatus_unidad', $data);
    }
    public function tbl_equipo_especial($id)
    {
        $data['equipo_especial'] = procesarResponseApiJsonToArray($this->curl->curlGet('api/get-equipo-especial-remision/' . $id));
        $this->blade->set_data($data)->render('tbl-equipo-especial');
    }
    public function reporte_unidades()
    {
        $data['titulo'] = "Reporte Unidades nuevas";
        // $info = procesarResponseApiJsonToArray($this->curl->curlGet('api/unidades/get-all', []));
        $info = procesarResponseApiJsonToArray($this->curl->curlGet('http://35.233.204.99/dms/autos_nuevos_queretaro/remisiones/api/unidades', true))->data;
        // dd($info);die();
        $unidades = [];
        foreach ($info as $d => $unidad) {
            $unidades[$unidad->modelo_descripcion][] = $unidad;
        }
        $data['unidades'] = $unidades;
        $this->blade->set_data($data)->render('v_reporte_unidades');
    }
}
