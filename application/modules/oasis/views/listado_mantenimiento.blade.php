@layout('tema_luna/layout')
@section('contenido')
<style type="text/css">
    .my_alert-danger {
        color: #a94442;
        background-color: #f2dede;
        border-color: #ebccd1;
        font-weight: bold;
        padding: 5px !important;
    }

    td a i {
        font-size: 25px !important;
    }

    .visualizar {
        visibility: visible;
    }

    .ocultar {
        display: none;
    }
</style>
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
        </li>
    </ol>
    <div class="row">
        <div class="col-sm-12">
            <div id="tabla-items">
                <table class="table table-bordered table-responsive" id="tbl_remisiones" width="100%" cellspacing="0">
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    inicializar_tabla_local();

    function vistaMantenimiento(_this) {
        console.log("datos", $(_this).data('remision_id'));     
        //mantenimiento   
        window.location.href = PATH + '/oasis/mantenimiento/' + $(_this).data('remision_id');
    }
    
    function inicializar_tabla_local() {
        ajax.get("api/unidades/get-all", {}, function(response, header) {
            var listado = $('table#tbl_remisiones').DataTable();
            listado.clear().draw();
            if (response && response.length > 0) {
                response.forEach(listado.row.add);
                listado.draw();
            }
        });
        var tabla_stock = $('#tbl_remisiones').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            columns: [{
                    title: "QR",
                    data: 'id',
                    render: function(data, type, row) {
                        return '<div data-serie="' + row.serie + '" data-remision_id="' + row.id + '" id="qr_' + row.id + '"></div>';
                    }
                },
                {
                    title: "#",
                    data: 'id',
                },
                {
                    title: "Modelo",
                    data: 'modelo',
                },
                {
                    title: "Unidad",
                    data: 'modelo_descripcion',
                },
                {
                    title: "Serie",
                    data: 'serie',
                },
                {
                    title: "Serie corta",
                    data: 'serie_corta',
                },
                {
                    title: "Línea",
                    data: 'linea',
                },
                {
                    title: "Color interior",
                    data: 'color_interior',
                },
                {
                    title: "Color exterior",
                    data: 'color_exterior',
                },
                {
                    title: "# Económico",
                    data: 'economico',
                },
                {
                    title: "Ubicación",
                    data: 'ubicacion',
                },
                {
                    title: "-",
                    render: function(data, type, row) {
                        btn_mantenimiento =
                            '<button  class="btn btn-info" onclick="vistaMantenimiento(this)" data-remision_id="' +
                            row.id + '"><i class="fa-solid fa-file-lines"></i> </button>';
                        return btn_mantenimiento;
                    }
                }
            ],
        });
    }
</script>
@endsection