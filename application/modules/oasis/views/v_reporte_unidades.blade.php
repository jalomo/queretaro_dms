@layout('tema_luna/layout')
<style>
    .bgwhite {
        background-color: #fff !important
    }
</style>
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <hr>
        <span>
            DV = Disponible, PR = Previado, LV = Lavado, ER = Espera x Refaccion, CL = Daño o Colision, DM = Demo, *P =
            Pagada, p = Propia
        </span>
        <br>
        <br>
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>No. Ec</th>
                            <th>Color</th>
                            <th>Serie</th>
                            <th>Dias Op</th>
                            <th>Apartado cliente</th>
                            <th>Ven Obs</th>
                            <th>Sta</th>
                            <th>CAT</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <tbody>
                        @foreach ($unidades as $u => $unidad)
                            <tr>
                                <td colspan="8"> <strong>{{ $u }}</strong></td>
                            </tr>
                            @foreach ($unidad as $k => $val)
                            <?php
                                $date1 = new DateTime(substr($val->created_at, 0, 10));
                                $date2 = new DateTime(date('Y-m-d'));
                                $diff = $date1->diff($date2);
                                $dias = $diff->d;

                            ?> 
                                <tr>
                                    <td>{{$val->economico}}</td>
                                    <td>{{$val->color_exterior}}</td>
                                    <td>{{$val->serie}}</td>
                                    <td>{{$dias}}</td>
                                    <td>{{$val->cliente_aparto_unidad}}</td>
                                    <td>{{$val->clave_vendedor ? $val->clave_aparto_unidad : 'N/A' .' - '.$val->clave_aparto_unidad}}</td>
                                    <td>{{$val->ubicacion}}</td>
                                    <td>{{$val->cat}}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;"></div>
    </div>
@endsection
@section('scripts')
    <script src="<?php echo base_url(); ?>js/custom/numeric.js"></script>
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script type="text/javascript">
        $('.numeric').numeric();
        $('.positive').numeric();
        $(".busqueda").select2();
        let unique = "{{ uniqid() }}";
    </script>
@endsection
