@layout('tema_luna/layout')
@section('contenido')
<style type="text/css">
    .espacio_row{
        border-style: groove; border-width: 1px; border-color: black;
    }
    input[type=radio] {
    width: 100%; 
    height: 2em;
    opacity: 0;
    cursor: pointer;
    }

    .radio-group div {  
    width: 150px;
    display: inline-block;
    border: 2px solid #AEABAE;
    border-radius: 5px;
    text-align: center;
    position:relative;
    }

    .radio-group label {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    line-height: 2em;
    pointer-events: none;
    }
    .radio-group input[type=radio]:checked + label {
    background: #1C7298;
    color: #fff;
    }
</style>

<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
        </li>
    </ol>
    <div class="col-md-12 text-right">
        <a href="{{base_url('/oasis/oasis/listado')}}" type="button" class="btn btn-primary">Listado</a>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-historial-mantenimiento">Historial</button>
    </div>
    <br><br>
    <form id="formMantenimiento" action="" method="">
        <input type="hidden" name="unidad_id" id="unidad_id" value="<?php echo isset($unidad->id) ? $unidad->id : '' ?>">
        <div class="row">
            <div class="col-sm-3">
                <label for="">Año</label>
                <input class="form-control input-sm" value="<?php echo isset($unidad->modelo) ? $unidad->modelo : '' ?>" type="text" readonly>
            </div>
            <div class="col-sm-3">
                <label for="">Color</label>
                <input class="form-control input-sm" type="text" readonly value="<?php echo isset($unidad->color_exterior) ? $unidad->color_exterior : '' ?>">
            </div>
            <div class="col-sm-3">
                <label for="">Catálogo</label>
                <input class="form-control input-sm" type="text" readonly value="<?php echo isset($unidad->cat) ? $unidad->cat : '' ?>">
            </div>
            <div class="col-sm-3">
                <label for="">VIN</label>
                <input class="form-control input-sm" type="text" readonly value="<?php echo isset($unidad->serie) ? $unidad->serie : '' ?>">
            </div>
            <div class="col-sm-3">
                <label for="">Técnico Mantenimiento</label>
                <input class="form-control input-sm" type="text" id="tecnico_mantenimiento" name="tecnico_mantenimiento">
            </div>
            <div class="col-sm-3">
                <label for="">No. Orden de reparación</label>
                <input class="form-control input-sm" type="text" id="no_orden_reparacion" name="no_orden_reparacion">
            </div>
            <div class="col-sm-3">
                <label for="">Fecha de recibo de unidad</label>
                <input readonly class="form-control input-sm" type="text" value="<?php echo isset($unidad->fecha_remision) ? $unidad->fecha_remision : '' ?>" id="fecha_recibo_unidad">
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-10 text-right">
                <label id="label_fecha_mantenimiento" for="">Fecha de mantenimiento</label>
            </div>
            <div class="col-md-2">
                <input type="date" onchange="colorTextoRadio('fecha_mantenimiento')" class="form-control" id="fecha_mantenimiento" name="fecha_mantenimiento">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="">Estado de la bateria</label>
                <textarea class="form-control" name="estado_bateria" id="estado_bateria" cols="5" rows="2" placeholder="Observaciones"></textarea>
            </div>
            <div class="col-md-4 text-center">
                <br>
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio()" type="radio" value="ok" name="estatus_voltaje_bateria" id="estatus_voltaje_bateria"/>
                      <label for="estatus_voltaje_bateria">Voltaje ≥ 12.5 OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio()" type="radio" value="nok" name="estatus_voltaje_bateria" id="estatus_voltaje_bateria"/>
                      <label for="estatus_voltaje_bateria">Voltaje ≤ 12.49 NOK</label>
                    </div>
                  </div>
            </div>
            <div class="col-md-2">
                <br>
                <div class="row">
                    <div class="col-md-8">
                        <label for="staticEmail">Voltaje inicial</label>
                    </div>
                    <div class="col-md-2">
                        <input type="number" id="voltaje_bateria_inicial" name="voltaje_bateria_inicial">
                    </div>

                    <div class="col-md-8">
                        <label for="staticEmail">Voltaje final</label>
                    </div>
                    <div class="col-md-2">
                        <input type="number" id="voltaje_bateria_final" name="voltaje_bateria_final">
                    </div>
                </div>           
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="">Estado de la bateria vehiculo electrico.</label>
                <textarea class="form-control" name="estado_bateria_electrico" id="estado_bateria_electrico" cols="5" rows="2" placeholder="Observaciones"></textarea>
            </div>
            <div class="col-md-4 text-center">
                <br>
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio()" type="radio" value="ok" name="estatus_voltaje_bateria_electrico" id="estatus_voltaje_bateria_electrico"/>
                      <label for="estatus_voltaje_bateria_electrico">SoC ≤ 90% OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio()" type="radio" value="nok" name="estatus_voltaje_bateria_electrico" id="estatus_voltaje_bateria_electrico"/>
                      <label for="estatus_voltaje_bateria_electrico">SoC ≤ 40% NOK</label>
                    </div>
                </div>
            </div>
            <div class="col-md-2 text-left">
                <br>
                <div class="row">
                    <div class="col-md-8">
                        <label for="staticEmail">Voltaje inicial</label>
                    </div>
                    <div class="col-md-2">
                        <input type="number" id="voltaje_bateria_inicial_electrico" name="voltaje_bateria_inicial_electrico">
                    </div>

                    <div class="col-md-8">
                        <label for="staticEmail">Voltaje final</label>
                    </div>
                    <div class="col-md-2">
                        <input type="numbre" id="voltaje_bateria_final_electrico" name="voltaje_bateria_final_electrico">
                    </div>
                </div>           
            </div>
        </div>
        <br>
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_temperatura_normal" for="">Encender el motor hasta alcanzar la temperatura normal</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('temperatura_normal')" type="radio" value="ok" name="temperatura_normal" id="temperatura_normal"/>
                      <label for="temperatura_normal">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('temperatura_normal')" type="radio" value="nok" name="temperatura_normal" id="temperatura_normal"/>
                      <label for="temperatura_normal">NOK</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_estereo_apagado" for="">Verificar que el estereo de la unidad este apagado</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('estereo_apagado')" type="radio" value="ok" name="estereo_apagado" id="estereo_apagado"/>
                      <label for="estereo_apagado">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('estereo_apagado')" type="radio" value="nok" name="estereo_apagado" id="estereo_apagado"/>
                      <label for="estereo_apagado">NOK</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_aire_apagado" for="">Verificar que el Aire Acondicionado (A/C) este apagado</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('aire_apagado')" type="radio" value="ok" name="aire_apagado" id="aire_apagado"/>
                      <label for="aire_apagado">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('aire_apagado')" type="radio" value="nok" name="aire_apagado" id="aire_apagado"/>
                      <label for="aire_apagado">NOK</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_cambios_transmision" for="">Aplicar todos los cambios de la transmición</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('cambios_transmision')" type="radio" value="ok" name="cambios_transmision" id="cambios_transmision"/>
                      <label for="cambios_transmision">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('cambios_transmision')" type="radio" value="nok" name="cambios_transmision" id="cambios_transmision"/>
                      <label for="cambios_transmision">NOK</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_rotacion_llantas" for="">Marcar llantas y desplazar vehículo para que roten 45°</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('rotacion_llantas')" type="radio" value="ok" name="rotacion_llantas" id="rotacion_llantas"/>
                      <label for="rotacion_llantas">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('rotacion_llantas')" type="radio" value="nok" name="rotacion_llantas" id="rotacion_llantas"/>
                      <label for="rotacion_llantas">NOK</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_estado_elevadores" for="">Verificar elevadores de ventanillas y quemacocos</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('estado_elevadores')" type="radio" value="ok" name="estado_elevadores" id="estado_elevadores"/>
                      <label for="estado_elevadores">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('estado_elevadores')" type="radio" value="nok" name="estado_elevadores" id="estado_elevadores"/>
                      <label for="estado_elevadores">NOK</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_fugas_fluidos" for="">Verificar que no exsitan fugas de fluidos (en caso de que existan reparar)</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('fugas_fluidos')" type="radio" value="ok" name="fugas_fluidos" id="fugas_fluidos"/>
                      <label for="fugas_fluidos">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('fugas_fluidos')" type="radio" value="nok" name="fugas_fluidos" id="fugas_fluidos"/>
                      <label for="fugas_fluidos">NOK</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_protectores_asientos" for="">Verificar que los protectores de los asientos esten debidamente colocados</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('protectores_asientos')" type="radio" value="ok" name="protectores_asientos" id="protectores_asientos"/>
                      <label for="protectores_asientos">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('protectores_asientos')" type="radio" value="nok" name="protectores_asientos" id="protectores_asientos"/>
                      <label for="protectores_asientos">NOK</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_presion_llanta_dd" for="">Verificar presion de llanata. Deleantera Derecha</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('presion_llanta_dd')" type="radio" value="ok" name="presion_llanta_dd" id="presion_llanta_dd"/>
                      <label for="presion_llanta_dd">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('presion_llanta_dd')" type="radio" value="nok" name="presion_llanta_dd" id="presion_llanta_dd"/>
                      <label for="presion_llanta_dd">NOK</label>
                    </div>
                </div>
            </div>
        </div>    
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_presion_llanta_di" for="">Verificar presion de llanata. Deleantera Izquierda</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('presion_llanta_di')" type="radio" value="ok" name="presion_llanta_di" id="presion_llanta_di"/>
                      <label for="presion_llanta_di">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('presion_llanta_di')" type="radio" value="nok" name="presion_llanta_di" id="presion_llanta_di"/>
                      <label for="presion_llanta_di">NOK</label>
                    </div>
                </div>
            </div>
        </div>    
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_presion_llanta_ti" for="">Verificar presion de llanata. Trasera Izquierda</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('presion_llanta_ti')" type="radio" value="ok" name="presion_llanta_ti" id="presion_llanta_ti"/>
                      <label for="presion_llanta_ti">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('presion_llanta_ti')" type="radio" value="nok" name="presion_llanta_ti" id="presion_llanta_ti"/>
                      <label for="presion_llanta_ti">NOK</label>
                    </div>
                </div>
            </div>
        </div>    
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_presion_llanta_td" for="">Verificar presion de llanata. Trasera Derecha</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('presion_llanta_td')" type="radio" value="ok" name="presion_llanta_td" id="presion_llanta_td"/>
                      <label for="presion_llanta_td">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('presion_llanta_td')" type="radio" value="nok" name="presion_llanta_td" id="presion_llanta_td"/>
                      <label for="presion_llanta_td">NOK</label>
                    </div>
                </div>
            </div>
        </div>    
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_caducidad_wrap" for="">Identificar caducidad de wrap guard y removerlo en caso necesario</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('caducidad_wrap')" type="radio" value="ok" name="caducidad_wrap" id="caducidad_wrap"/>
                      <label for="caducidad_wrap">Lo tiene OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('caducidad_wrap')" type="radio" value="nok" name="caducidad_wrap" id="caducidad_wrap"/>
                      <label for="caducidad_wrap">No lo tiene NOK</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_lavado_ford" for="">Lavar con shampo espesificado por Ford</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('lavado_ford')" type="radio" value="ok" name="lavado_ford" id="lavado_ford"/>
                      <label for="lavado_ford">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('lavado_ford')" type="radio" value="nok" name="lavado_ford" id="lavado_ford"/>
                      <label for="lavado_ford">NOK</label>
                    </div>
                </div>
            </div>
        </div>     
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_estado_pintura" for="">Insepaccionar la pintura</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('estado_pintura')" type="radio" value="ok" name="estado_pintura" id="estado_pintura"/>
                      <label for="estado_pintura">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('estado_pintura')" type="radio" value="nok" name="estado_pintura" id="estado_pintura"/>
                      <label for="estado_pintura">NOK</label>
                    </div>
                </div>
            </div>
        </div>     
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_cera_ford" for="">Aplicar cera autorizada por Ford (Omitir en unidades exhbidas)</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('cera_ford')" type="radio" value="ok" name="cera_ford" id="cera_ford"/>
                      <label for="cera_ford">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('cera_ford')" type="radio" value="nok" name="cera_ford" id="cera_ford"/>
                      <label for="cera_ford">NOK</label>
                    </div>
                </div>
            </div>
        </div>     
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_descontaminacion_pintura" for="">Descontaminar pintura (óxido, lluvía ácida, etc.)</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('descontaminacion_pintura')" type="radio" value="ok" name="descontaminacion_pintura" id="descontaminacion_pintura"/>
                      <label for="descontaminacion_pintura">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('descontaminacion_pintura')" type="radio" value="nok" name="descontaminacion_pintura" id="descontaminacion_pintura"/>
                      <label for="descontaminacion_pintura">NOK</label>
                    </div>
                </div>
            </div>
        </div>     
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_gomas_limpiaparabrisas" for="">Inspaccionar gomas de limpiaparabrisas</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('gomas_limpiaparabrisas')" type="radio" value="ok" name="gomas_limpiaparabrisas" id="gomas_limpiaparabrisas"/>
                      <label for="gomas_limpiaparabrisas">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('gomas_limpiaparabrisas')" type="radio" value="nok" name="gomas_limpiaparabrisas" id="gomas_limpiaparabrisas"/>
                      <label for="gomas_limpiaparabrisas">NOK</label>
                    </div>
                </div>
            </div>
        </div>     
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_nivel_gasolina" for="">Inspaccionar nivel de gasolina</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('nivel_gasolina')" type="radio" value="E" name="nivel_gasolina" id="nivel_gasolina"/>
                      <label for="nivel_gasolina">E</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('nivel_gasolina')" type="radio" value="1/2" name="nivel_gasolina" id="nivel_gasolina"/>
                      <label for="nivel_gasolina">1/2</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('nivel_gasolina')" type="radio" value="3/4" name="nivel_gasolina" id="nivel_gasolina"/>
                      <label for="nivel_gasolina">3/4</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('nivel_gasolina')" type="radio" value="F" name="nivel_gasolina" id="nivel_gasolina"/>
                      <label for="nivel_gasolina">F</label>
                    </div>
                </div>
            </div>
        </div>   
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_parte_baja_vieculo" for="">Inspaccionar parte baja del vehículo</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('parte_baja_vieculo')" type="radio" value="ok" name="parte_baja_vieculo" id="parte_baja_vieculo"/>
                      <label for="parte_baja_vieculo">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('parte_baja_vieculo')" type="radio" value="nok" name="parte_baja_vieculo" id="parte_baja_vieculo"/>
                      <label for="parte_baja_vieculo">NOK</label>
                    </div>
                </div>
            </div>
        </div>   
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_nivel_anticongelante" for="">Verificar anticongelante</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('nivel_anticongelante')" type="radio" value="ok" name="nivel_anticongelante" id="nivel_anticongelante"/>
                      <label for="nivel_anticongelante">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('nivel_anticongelante')" type="radio" value="nok" name="nivel_anticongelante" id="nivel_anticongelante"/>
                      <label for="nivel_anticongelante">NOK</label>
                    </div>
                </div>
            </div>
        </div>   
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_nivel_aceite_motor" for="">Verificar nivel de aciete del motor</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('nivel_aceite_motor')" type="radio" value="ok" name="nivel_aceite_motor" id="nivel_aceite_motor"/>
                      <label for="nivel_aceite_motor">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('nivel_aceite_motor')" type="radio" value="nok" name="nivel_aceite_motor" id="nivel_aceite_motor"/>
                      <label for="nivel_aceite_motor">NOK</label>
                    </div>
                </div>
            </div>
        </div>   
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_nivel_liquido_frenos" for="">Verificar nivel de aceite del liquido de frenos</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('nivel_liquido_frenos')" type="radio" value="ok" name="nivel_liquido_frenos" id="nivel_liquido_frenos"/>
                      <label for="nivel_liquido_frenos">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('nivel_liquido_frenos')" type="radio" value="nok" name="nivel_liquido_frenos" id="nivel_liquido_frenos"/>
                      <label for="nivel_liquido_frenos">NOK</label>
                    </div>
                </div>
            </div>
        </div>   
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_nivel_aceite_transmicion" for="">Verificar nivel de aceite de la transmisión</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('nivel_aceite_transmicion')" type="radio" value="ok" name="nivel_aceite_transmicion" id="nivel_aceite_transmicion"/>
                      <label for="nivel_aceite_transmicion">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('nivel_aceite_transmicion')" type="radio" value="nok" name="nivel_aceite_transmicion" id="nivel_aceite_transmicion"/>
                      <label for="nivel_aceite_transmicion">NOK</label>
                    </div>
                </div>
            </div>
        </div>   
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_nivel_chisgueteros" for="">Verificar nivel de los chisgueteros</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('nivel_chisgueteros')" type="radio" value="ok" name="nivel_chisgueteros" id="nivel_chisgueteros"/>
                      <label for="nivel_chisgueteros">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('nivel_chisgueteros')" type="radio" value="nok" name="nivel_chisgueteros" id="nivel_chisgueteros"/>
                      <label for="nivel_chisgueteros">NOK</label>
                    </div>
                </div>
            </div>
        </div>   
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_nivel_aceite_dieccion" for="">Verificar nivel de aceite de la dirección</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('nivel_aceite_dieccion')" type="radio" value="ok" name="nivel_aceite_dieccion" id="nivel_aceite_dieccion"/>
                      <label for="nivel_aceite_dieccion">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('nivel_aceite_dieccion')" type="radio" value="nok" name="nivel_aceite_dieccion" id="nivel_aceite_dieccion"/>
                      <label for="nivel_aceite_dieccion">NOK</label>
                    </div>
                </div>
            </div>
        </div>   
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_luces_delanteras" for="">Luces delanteras</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('luces_delanteras')" type="radio" value="ok" name="luces_delanteras" id="luces_delanteras"/>
                      <label for="luces_delanteras">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('luces_delanteras')" type="radio" value="nok" name="luces_delanteras" id="luces_delanteras"/>
                      <label for="luces_delanteras">NOK</label>
                    </div>
                </div>
            </div>
        </div>   
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_direccionales" for="">Direccionales</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('direccionales')" type="radio" value="ok" name="direccionales" id="direccionales"/>
                      <label for="direccionales">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('direccionales')" type="radio" value="nok" name="direccionales" id="direccionales"/>
                      <label for="direccionales">NOK</label>
                    </div>
                </div>
            </div>
        </div>   
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_luces_traceras" for="">Luces traceras</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('luces_traceras')" type="radio" value="ok" name="luces_traceras" id="luces_traceras"/>
                      <label for="luces_traceras">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('luces_traceras')" type="radio" value="nok" name="luces_traceras" id="luces_traceras"/>
                      <label for="luces_traceras">NOK</label>
                    </div>
                </div>
            </div>
        </div>   
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_luz_toldo" for="">Luz de toldo</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('luz_toldo')" type="radio" value="ok" name="luz_toldo" id="luz_toldo"/>
                      <label for="luz_toldo">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('luz_toldo')" type="radio" value="nok" name="luz_toldo" id="luz_toldo"/>
                      <label for="luz_toldo">NOK</label>
                    </div>
                </div>
            </div>
        </div>   
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_luz_emergencia" for="">Luz de emergencia</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('luz_emergencia')" type="radio" value="ok" name="luz_emergencia" id="luz_emergencia"/>
                      <label for="luz_emergencia">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('luz_emergencia')" type="radio" value="nok" name="luz_emergencia" id="luz_emergencia"/>
                      <label for="luz_emergencia">NOK</label>
                    </div>
                </div>
            </div>
        </div>   
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_luz_vanidad" for="">Luz de vanidad</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('luz_vanidad')" type="radio" value="ok" name="luz_vanidad" id="luz_vanidad"/>
                      <label for="luz_vanidad">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('luz_vanidad')" type="radio" value="nok" name="luz_vanidad" id="luz_vanidad"/>
                      <label for="luz_vanidad">NOK</label>
                    </div>
                </div>
            </div>
        </div>   
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_luz_frenos" for="">Luz de frenos</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('luz_frenos')" type="radio" value="ok" name="luz_frenos" id="luz_frenos"/>
                      <label for="luz_frenos">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('luz_frenos')" type="radio" value="nok" name="luz_frenos" id="luz_frenos"/>
                      <label for="luz_frenos">NOK</label>
                    </div>
                </div>
            </div>
        </div>   
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_luz_reversa" for="">Luz de reversa</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('luz_reversa')" type="radio" value="ok" name="luz_reversa" id="luz_reversa"/>
                      <label for="luz_reversa">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('luz_reversa')" type="radio" value="nok" name="luz_reversa" id="luz_reversa"/>
                      <label for="luz_reversa">NOK</label>
                    </div>
                </div>
            </div>
        </div>   
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_luz_placa" for="">Luz de placa</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('luz_placa')" type="radio" value="ok" name="luz_placa" id="luz_placa"/>
                      <label for="luz_placa">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('luz_placa')" type="radio" value="nok" name="luz_placa" id="luz_placa"/>
                      <label for="luz_placa">NOK</label>
                    </div>
                </div>
            </div>
        </div>   
        <div class="row espacio_row">
            <div class="col-md-8">
                <label id="label_luz_guantera" for="">Luz de guantera</label>
            </div>
            <div class="col-md-4">
                <div class="radio-group">
                    <div>
                      <input onclick="colorTextoRadio('luz_guantera')" type="radio" value="ok" name="luz_guantera" id="luz_guantera"/>
                      <label for="luz_guantera">OK</label>
                    </div>
                    <div>
                      <input onclick="colorTextoRadio('luz_guantera')" type="radio" value="nok" name="luz_guantera" id="luz_guantera"/>
                      <label for="luz_guantera">NOK</label>
                    </div>
                </div>
            </div>
        </div>   
        <br>
        <div class="col-md-12 text-center"><label for="">Despues del mantenimiento todos los sistemas deben quedar apagados (Off Mode)</label></div>
        <br>
        <div class="row espacio_row">
            <div class="col-md-8">
                <label for="">De madrina kilometraje inicial</label>
            </div>
            <div class="col-md-4">
                <input type="number" id="kilometraje_madrina" name="kilometraje_madrina">
            </div>
        </div> 
        <div class="row espacio_row">
            <div class="col-md-8">
                <label for="">De intercambio kilometraje inicial</label>
            </div>
            <div class="col-md-4">
                <input type="number" id="kilometraje_intercambio" name="kilometraje_intercambio">
            </div>
        </div> 
        <div class="row espacio_row">
            <div class="col-md-8">
                <label for="">De traslado kilometraje inicial</label>
            </div>
            <div class="col-md-4">
                <input type="number" id="kilometraje_traslado" name="kilometraje_traslado">
            </div>
        </div> 
        <div class="row espacio_row">
            <div class="col-md-8">
                <label for="">Prueba de carretera</label>
            </div>
            <div class="col-md-4">
                <input type="number" id="kilometraje_prueba" name="kilometraje_prueba">
            </div>
        </div>
        <br>
        <div class="row">        
            <div class="col-md-3 text-left">
                <label for="">Papeleria completa </label>
                <input type="text" class="form-control" id="papeleria_completa" name="papeleria_completa">
            </div>
            <div class="col-md-3 text-left">
                <label for="">Fecha de venta</label>
                <input type="date" class="form-control" id="fecha_venta" name="fecha_venta">
            </div>
            <div class="col-md-4"></div>
            <br>
            <div class="col-md-2 text-right">
                <button type="button" onclick="guardarMantenimiento()" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </form>
</div>
<form method="POST" id="form_crear_pdf" action="{{base_url('/oasis/mantenimientospdf')}}" target="_blank" >
    <input type="text" hidden id="remision_id" name="remision_id" value="{{$remision_id}}">
    <input type="text" hidden id="ids_pdf" name="ids_pdf">
</form>
@endsection

@section('modal')
    <div class="modal fade" id="modal-historial-mantenimiento" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal">Historial mantenimientos</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table class=" table table-bordered" id="tbl_ordenes_abiertas">
                                <thead>
                                    <tr>
                                        <th>Unidad</th>
                                        <th>Modelo</th>
                                        <th>Fecha mantenimiento</th>
                                        <th>-</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        foreach ($historialMantenimientos as $key => $mantenimiento) {
                                            ?>
                                                <tr>
                                                    <td><?php echo $mantenimiento->unidad->unidad_descripcion ?></td>
                                                    <td><?php echo $mantenimiento->unidad->modelo ?></td>
                                                    <td><?php echo $mantenimiento->fecha_mantenimiento ?></td>
                                                    <td><input id="check_{{$mantenimiento->id}}" onclick="crearArryPdf({{$mantenimiento->id}})" type="checkbox" class="form-control" value="<?php echo $mantenimiento->id ?>"></td>
                                                </tr>                                
                                            <?php
                                        } 
                                    ?> 
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" onclick="descargarPdf()" class="btn btn-primary">PDF</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    function guardarMantenimiento() {
          
        ajax.post('api/guardar-mantenimiento', procesarRegistro(), function(response, headers) {
            $('.validation_error').remove();         
            if (headers.status == 400) {
                var errores = JSON.parse(headers.message);
                Object.entries(errores).forEach(([key, value]) => {
                    document.getElementById("label_"+key).style.color = '#BD362F';
                })
            }else if (headers.status == 200) {
                utils.displayWarningDialog("Mantenimiento completado con éxito", "success", function(data) {
                    return window.location.href = base_url + 'oasis/mantenimiento/'+$("#unidad_id").val();
                })               
            }
        })
    }

    let procesarRegistro = function() {
            let datos = {
                unidad_id: document.getElementById("unidad_id").value,
                tecnico_mantenimiento: document.getElementById("tecnico_mantenimiento").value,
                no_orden_reparacion: document.getElementById("no_orden_reparacion").value,
                fecha_recibo_unidad: document.getElementById("fecha_recibo_unidad").value,
                fecha_mantenimiento: document.getElementById("fecha_mantenimiento").value,
                estado_bateria: document.getElementById('estado_bateria').value,
                estatus_voltaje_bateria: $('input[id="estatus_voltaje_bateria"]:checked').val(),
                voltaje_bateria_inicial: document.getElementById("voltaje_bateria_inicial").value,
                voltaje_bateria_final: document.getElementById("voltaje_bateria_final").value,
                estado_bateria_electrico: document.getElementById("estado_bateria_electrico").value,
                estatus_voltaje_bateria_electrico: document.getElementById("estatus_voltaje_bateria_electrico").value,
                voltaje_bateria_inicial_electrico: document.getElementById("voltaje_bateria_inicial_electrico").value,
                voltaje_bateria_final_electrico: document.getElementById("voltaje_bateria_final_electrico").value,

                temperatura_normal: $('input[id="temperatura_normal"]:checked').val(),                
                estereo_apagado: $('input[id="estereo_apagado"]:checked').val(),
                aire_apagado: $('input[id="aire_apagado"]:checked').val(),
                cambios_transmision: $('input[id="cambios_transmision"]:checked').val(),
                rotacion_llantas: $('input[id="rotacion_llantas"]:checked').val(),
                estado_elevadores: $('input[id="estado_elevadores"]:checked').val(),
                fugas_fluidos: $('input[id="fugas_fluidos"]:checked').val(),
                protectores_asientos: $('input[id="protectores_asientos"]:checked').val(),
                presion_llanta_dd: $('input[id="presion_llanta_dd"]:checked').val(),
                presion_llanta_di: $('input[id="presion_llanta_di"]:checked').val(),
                presion_llanta_ti: $('input[id="presion_llanta_ti"]:checked').val(),
                presion_llanta_td: $('input[id="presion_llanta_td"]:checked').val(),
                caducidad_wrap: $('input[id="caducidad_wrap"]:checked').val(),
                lavado_ford: $('input[id="lavado_ford"]:checked').val(),
                estado_pintura: $('input[id="estado_pintura"]:checked').val(),
                cera_ford: $('input[id="cera_ford"]:checked').val(),
                descontaminacion_pintura: $('input[id="descontaminacion_pintura"]:checked').val(),
                gomas_limpiaparabrisas: $('input[id="gomas_limpiaparabrisas"]:checked').val(),
                nivel_gasolina: $('input[id="nivel_gasolina"]:checked').val(),
                parte_baja_vieculo: $('input[id="parte_baja_vieculo"]:checked').val(),
                nivel_anticongelante: $('input[id="nivel_anticongelante"]:checked').val(),
                nivel_aceite_motor: $('input[id="nivel_aceite_motor"]:checked').val(),
                nivel_liquido_frenos: $('input[id="nivel_liquido_frenos"]:checked').val(),
                nivel_aceite_transmicion: $('input[id="nivel_aceite_transmicion"]:checked').val(),
                nivel_chisgueteros: $('input[id="nivel_chisgueteros"]:checked').val(),
                nivel_aceite_dieccion: $('input[id="nivel_aceite_dieccion"]:checked').val(),
                luces_delanteras: $('input[id="luces_delanteras"]:checked').val(),
                direccionales: $('input[id="direccionales"]:checked').val(),
                luces_traceras: $('input[id="luces_traceras"]:checked').val(),
                luz_toldo: $('input[id="luz_toldo"]:checked').val(),
                luz_emergencia: $('input[id="luz_emergencia"]:checked').val(),
                luz_vanidad: $('input[id="luz_vanidad"]:checked').val(),
                luz_frenos: $('input[id="luz_frenos"]:checked').val(),
                luz_reversa: $('input[id="luz_reversa"]:checked').val(),
                luz_placa: $('input[id="luz_placa"]:checked').val(),
                luz_guantera: $('input[id="luz_guantera"]:checked').val(),


                kilometraje_madrina: document.getElementById("kilometraje_madrina").value,
                kilometraje_intercambio: document.getElementById("kilometraje_intercambio").value,
                kilometraje_traslado: document.getElementById("kilometraje_traslado").value,
                kilometraje_prueba: document.getElementById("kilometraje_prueba").value,
                papeleria_completa: document.getElementById("papeleria_completa").value,
                fecha_venta: document.getElementById("fecha_venta").value,
            };
            return datos;
        }
    function colorTextoRadio(radio_id) {
        document.getElementById("label_"+radio_id).style.color = '#404040';        
    }

    var arrayPdf = [];
    var cadenaMantenimientosIds = "";
    function crearArryPdf(mantenimiento_id) {
        let banderaAgregar = true;
        var checkbox = document.getElementById("check_" + mantenimiento_id);        
        if (checkbox.checked == true) {
            if (arrayPdf.length == 7) {
                checkbox.checked = false;
                toastr.error("El limite de mantenimientos por PDF es 7");
                return 0;            
            }
            arrayPdf.forEach(element => {
                if (element == mantenimiento_id) {
                    banderaAgregar = false;                
                }
            });
            if (banderaAgregar) {
                arrayPdf.push(mantenimiento_id);            
            }
        }else{
            arrayPdf.forEach(element => {
                if (element == mantenimiento_id) {
                    arrayPdf.indexOf(mantenimiento_id) !== -1 && arrayPdf.splice(arrayPdf.indexOf(mantenimiento_id), 1)             
                }                
            });            
        }
    }

    function descargarPdf() {
        if (arrayPdf.length == 0) {
                toastr.error("Seleccionar entre [1 - 7] mantenimientos para generar PDF");
                return 0;            
            }
        cadenaMantenimientosIds = "";
        arrayPdf.forEach(element => {
            cadenaMantenimientosIds += "-"+element;            
        }); 
        $("#ids_pdf").val(cadenaMantenimientosIds);   
        $("#form_crear_pdf").submit(); 
    }

</script>
@endsection