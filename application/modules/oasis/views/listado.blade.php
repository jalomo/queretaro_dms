@layout('tema_luna/layout')
@section('contenido')
    <style type="text/css">
        .my_alert-danger {
            color: #a94442;
            background-color: #f2dede;
            border-color: #ebccd1;
            font-weight: bold;
            padding: 5px !important;
        }

        td a i {
            font-size: 25px !important;
        }

        .visualizar {
            visibility: visible;
        }

        .ocultar {
            display: none;
        }
    </style>
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <div class="row">
            <div class="col-sm-6">
                <a href="#" id="enviar-pilot" class="btn btn-sm btn-primary m-t-n-xs"><strong>Enviar a pilot</strong></a>
            </div>
            <div class="col-sm-6">
                <a href="{{ base_url('oasis/agregar/0') }}" id="guardar"
                    class="btn btn-sm btn-primary pull-right m-t-n-xs"><strong>Agregar</strong></a>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-12">
                <div id="tabla-items">
                    <table class="table table-bordered table-responsive" id="tbl_remisiones" width="100%" cellspacing="0">
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script src="{{ base_url('assets/libraries/qr/qrcode.js') }}"></script>
    <script>
        inicializar_tabla_local();
        var site_url = "{{ site_url() }}";
        var cliente = '';
        var aPos = '';
        $("body").on("click", '.js_comentarios', function(e) {
            e.preventDefault();
            aPos = $(this);
            id_financiamiento = $(this).data('id');
            id_estatus = $(this).data('id_estatus');
            id_estatus_piso = $(this).data('id_estatus_piso');
            var url = site_url + "/financiamientos/cambiar_estatus/0";
            customModal(url, {
                    "id_financiamiento": id_financiamiento,
                    "id_estatus": id_estatus,
                    "id_estatus_piso": id_estatus_piso,
                }, "GET", "lg", saveComentario, "", "Guardar", "Cancelar", "Ingresar comentario",
                "modalComentario");
        });

        $("body").on("click", '.js_historial', function(e) {
            e.preventDefault();
            id_financiamiento = $(this).data('id');
            var url = site_url + "/financiamientos/historial_comentarios/";
            customModal(url, {
                "id_financiamiento": id_financiamiento
            }, "POST", "lg", "", "", "", "Cerrar", "Historial de comentarios", "modalHistorialComentarios");
        });

        function saveComentario() {
            const data = {
                id_financiamiento: $("#id_financiamiento").val(),
                comentario: $("#comentario").val(),
                id_usuario: "{{ $this->session->userdata('id') }}",
                tipo_comentario: $("input[name='tipo_comentario']:checked").val(),
            }
            if (data.tipo_comentario == 1) {
                data.id_estatus = $("#id_estatus").val();
            } else {
                data.id_estatus = $("#id_estatus_piso").val();
            }
            ajax.post('api/financiamientos/historial-estatus', data,
                function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    var titulo = (headers.status != 200) ? headers.message : "Información guardada con éxito";
                    utils.displayWarningDialog("Información guardada con éxito", "success", function(result) {
                        if (data.tipo_comentario == 1) {
                            $(aPos).data('id_estatus', data.id_estatus);
                        } else {
                            $(aPos).data('id_estatus_piso', data.id_estatus);
                        }
                        $(".modalComentario").modal('hide')
                    })
                })
        }

        function makeCode(div, text) {
            $("#" + div).empty();
            return new QRCode(document.getElementById(div), {
                width: 80,
                height: 80,
                text: PATH + '/oasis/buscar_serie_qr/' + text.serie,
            });
        }

        function pdf(_this) {
            let url = PATH + '/oasis/exportar_qr/' + $(_this).data('serie') + '/' + $(_this).data('remision_id');
            window.open(url, '_blank');
        }

        function vistaMantenimiento(_this) {
            //mantenimiento   
            window.location.href = PATH + '/oasis/mantenimiento/' + $(_this).data('remision_id');
        }

        function cambiarDisponibilidad(_this) {
            console.log($(_this));
            let remision_id = $(_this).data('remision_id');
            let estatus_id_pilot = $(_this).data('estatus_id_pilot');
            let pilot_id = $(_this).data('pilot_id');
            let unidad_disponible = $(_this).data('unidad_disponible');
            var url = site_url + "/oasis/cambiar_estatus_pilot";
            customModal(url, {
                    remision_id,
                    estatus_id_pilot,
                    pilot_id,
                    unidad_disponible
                }, "GET", "lg", cambiarStatusPilot, "", "Guardar", "Cancelar", "Cambiar estatus",
                "modalStatus");
        }

        function cambiarStatusPilot() {
            let pilot_id = $("#pilot_id").val();
            var availability_status_code = $("input[name='disponible']:checked").val();
            let status_code = $("#estatus_id option:selected").text();
            let unidad_id = $("#unidad_id").val();
            let estatus_id = $("#estatus_id").val();

            if(estatus_id == 4){
                availability_status_code = 6;
            }
            let data = {
                pilot_id,
                availability_status_code,
                status_code,
                unidad_id,
                estatus_id,
            }

            ajax.post('api/pilot/cambiar-disponibilidad-unidad', data,
                function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    var titulo = (headers.status != 200) ? headers.message : "Información actualizada con éxito";
                    utils.displayWarningDialog("Información actualizada con éxito", "success", function(result) {
                        // $(".modalStatus").modal('hide')
                        location.reload();
                    })
                })
        }

        function enviarMultiplePilot() {

            let data = {
                todo: true,
            }

            ajax.post('api/pilot/multiples-envios-pilot', data,
                function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    var titulo = (headers.status != 200) ? headers.message : "Información actualizada con éxito";
                    utils.displayWarningDialog("Información actualizada con éxito", "success", function(result) {
                        // $(".modalStatus").modal('hide')
                        location.reload();
                    })
                })
        }
        $("body").on('click', '#enviar-pilot', function(e) {
            e.preventDefault();
            enviarMultiplePilot();
        });

        function editar(_this) {
            window.location.href = PATH + '/oasis/agregar/' + $(_this).data('remision_id');
        }

        function info(_this) {
            window.location.href = PATH + '/oasis/historial_unidad/' + $(_this).data('remision_id');
        }

        function generandoQrs() {
            $('table#tbl_remisiones tr td div:visible').each(function(index) {
                let identificador = $(this).attr('id');
                let array = {
                    'remision_id': $(this).data('remision_id'),
                    'serie': $(this).data('serie'),
                };
                makeCode(identificador, array);
            });
        }

        function inicializar_tabla_local() {
            ajax.get("api/unidades/get-all", {}, function(response, header) {
                var listado = $('table#tbl_remisiones').DataTable();
                listado.clear().draw();
                if (response && response.length > 0) {
                    response.forEach(listado.row.add);
                    listado.draw();
                }
            });
            var tabla_stock = $('#tbl_remisiones').DataTable({
                language: {
                    url: PATH_LANGUAGE
                },
                columns: [{
                        title: "QR",
                        data: 'id',
                        render: function(data, type, row) {
                            return '<div data-serie="' + row.serie + '" data-remision_id="' + row.id +
                                '" id="qr_' + row.id + '"></div>';
                        }
                    },
                    {
                        title: "#",
                        data: 'id',
                    },
                    {
                        title: "Modelo",
                        data: 'modelo',
                    },
                    {
                        title: "Unidad",
                        data: 'modelo_descripcion',
                    },
                    {
                        title: "Serie",
                        data: 'serie',
                    },
                    {
                        title: "Serie corta",
                        data: 'serie_corta',
                    },
                    {
                        title: "Línea",
                        data: 'linea',
                    },
                    {
                        title: "Color interior",
                        data: 'color_interior',
                    },
                    {
                        title: "Color exterior",
                        data: 'color_exterior',
                    },
                    {
                        title: "# Económico",
                        data: 'economico',
                    },
                    {
                        title: "Económico visual",
                        data: 'economico_visual',
                    },
                    {
                        title: "Estatus",
                        data: 'estatus_unidad_pilot',
                    },
                    {
                        title: "-",
                        render: function(data, type, row) {
                            btn_ficha =
                                '<button  class="btn btn-primary" onclick="pdf(this)" data-remision_id="' +
                                row.id + '" data-serie="' +
                                row.serie + '"><i class="pe pe-7s-file"></i> </button>';
                            btn_editar =
                                '<button  class="btn btn-warning" onclick="editar(this)" data-remision_id="' +
                                row.id + '"><i class="fas fa-edit"></i> </button>';

                            btn_info =
                                '<button  class="btn btn-info" onclick="info(this)" data-remision_id="' +
                                row.id + '"><i class="fas fa-info"></i> </button>';

                            btn_agendar =
                                '<button  class="btn btn-info" onclick="" data-remision_id="' +
                                row.id + '"><i class="fas fa-calendar-alt"></i> </button>';

                            btn_mantenimiento =
                                '<button  class="btn btn-info" onclick="vistaMantenimiento(this)" data-remision_id="' +
                                row.id + '"><i class="fa-solid fa-file-lines"></i> </button>';
                            btn_cambiar_disponibilidad =
                                '<button  class="btn btn-info" onclick="cambiarDisponibilidad(this)" data-remision_id="' +
                                row.id + '" data-estatus_id_pilot="' +
                                row.estatus_id_pilot + '" data-pilot_id="' +
                                row.pilot_id + '" data-unidad_disponible="' +
                                row.unidad_disponible +
                                '"><i class="fa-solid fa-arrows-rotate"></i> </button>';
                            return btn_ficha + ' ' + btn_editar + ' ' + btn_info + ' ' + btn_agendar + ' ' +
                                btn_mantenimiento + ' ' + btn_cambiar_disponibilidad;
                        }
                    }
                ],

                "fnDrawCallback": function(oSettings, json) {
                    generandoQrs();
                    $('#tbl_productos').on('page.dt', function() {
                        setTimeout(() => {
                            generandoQrs();
                        }, 500);
                    });
                }
            });
        }
    </script>
@endsection
