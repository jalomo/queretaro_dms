<table class="table table-striped table-border">
    <thead>
        <tr>
            <th>Importe</th>
            <th>Descripcion</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($equipo_especial as $e => $equipo)
            <tr>
                <td>{{number_format($equipo->importe,2)}}</td>
                <td>{{$equipo->descripcion}}</td>
                <td>
                    <i data-id="{{ $equipo->id }}"
                        class="fa fa-edit js_add_equipo"></i>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>