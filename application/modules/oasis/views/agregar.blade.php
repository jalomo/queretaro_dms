@layout('tema_luna/layout')
<style>
    .bgwhite {
        background-color: #fff !important
    }
</style>
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <hr>
        <div class="col-md-12">
            <form enctype="multipart/form-data" id="subir_factura">
                <div class="form-group">
                    <label for="factura">Factura</label>
                    <input type="file" class="form-control-file" id="xml_remision" name="xml_remision">
                    <div id="xml_remision_error" class="invalid-feedback"></div>
                </div>
                <button type="button" id="btn_subir_remision" class="btn btn-primary">Cargar remision</button>
            </form>
        </div>
        <hr>
        <form id="frm">
            <input type="hidden" name="id" id="id" value="{{ $id }}">
            <div class="row">
                <div class="col-sm-4">
                    <label id="js_add_linea" for="">Línea <i style="cursor: pointer" class="fa fa-plus"></i></label>
                    {{ $linea_id }}
                </div>
                <div class="col-sm-4">
                    <label id="js_add_cat" for="">CAT <i style="cursor: pointer" class="fa fa-plus"></i></label>
                    {{ $cat }}
                </div>
                <div class="col-sm-4">
                    <label for="">Clave vehícular</label>
                    {{ $clave_vehicular }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Unidad descripción</label>
                    {{ $unidad_descripcion }}
                </div>
                <div class="col-sm-4">
                    <label for=""># Económico</label>
                    {{ $economico }}
                </div>
                <div class="col-sm-4">
                    <label for=""># Económico visual</label>
                    {{ $economico_visual }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Unidad importada</label> <br>
                    Si {{ $unidad_importada_si }}
                    No {{ $unidad_importada_no }}
                </div>
                <div class="col-sm-4">
                    <label for="">Tipo auto</label>
                    {{ $tipo_auto }}
                </div>
                <!--<div class="col-sm-4">
                                        <label for="">Clave I.S.A.N</label>
                                        {{ $clave_isan }}
                                    </div>-->
                <div class="col-sm-4">
                    <label for="">Cuenta inventario</label>
                    {{ $cuenta_inventario_unidad }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Fecha remisión</label>
                    {{ $fecha_remision }}
                </div>
                <div class="col-sm-4">
                    <label for="">No. Inventario</label>
                    {{ $no_inventario }}
                </div>
                <div class="col-sm-4">
                    <label for="">Clave SAT</label>
                    {{ $clave_sat }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Unidad de Servicio</label>
                    {{ $unidad }}
                </div>
                <div class="col-sm-4">
                    <label for="">Modelo</label>
                    {{ $modelo }}
                </div>
                <div class="col-sm-4">
                    <label for="">Línea contabilidad</label>
                    {{ $linea_id_contabilidad }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Nacional / Importado</label>
                    {{ $nacional_importado }}
                </div>
            </div>
            <!--<hr>
                                <h2 class="text-center">Cuentas</h2>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="">Cta menudeo</label>
                                        {{ $cta_menudeo }}
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="">Cta Flotilla</label>
                                        {{ $cta_flotilla }}
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="">Cta Conauto</label>
                                        {{ $cta_conauto }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="">Cta Intercambio</label>
                                        {{ $cta_intercambio }}
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="">Cta P lleno</label>
                                        {{ $cta_plleno }}
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="">Cta Inventario</label>
                                        {{ $cta_inventario }}
                                    </div>
                                </div>
                                <hr>
                                <h2 class="text-center">Ventas</h2>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="">Venta menudeo</label>
                                        {{ $venta_menudeo }}
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="">Venta flotilla</label>
                                        {{ $vta_flotilla }}
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="">Venta conauto</label>
                                        {{ $vta_conauto }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="">Venta intercambio</label>
                                        {{ $vta_intercambio }}
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="">Venta plleno</label>
                                        {{ $venta_plleno }}
                                    </div>
                                </div>-->
            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Serie</label>
                    {{ $serie }}
                </div>
                <div class="col-sm-4">
                    <label for="">Serie corta</label>
                    {{ $serie_corta }}
                </div>
                <div class="col-sm-4">
                    <label for="">Motor</label>
                    {{ $motor }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Fecha pedimento</label>
                    {{ $fecha_pedimento }}
                </div>
                <div class="col-sm-4">
                    <label for="">Proveedor</label>
                    {{ $proveedor_id }}
                </div>
                <div class="col-sm-4">
                    <label for="">Pedimento</label>
                    {{ $pedimento }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Intercambio</label> <br>
                    Si {{ $unidad_intercambio_si }}
                    No {{ $unidad_intercambio_no }}
                </div>
            </div>
            <hr>
            <h2 class="text-center">Detalles remisión</h2>
            <div class="row">
                <div class="col-sm-3">
                    <label for="">Puertas</label>
                    {{ $puertas }}
                </div>
                <div class="col-sm-3">
                    <label for="">Cilindros</label>
                    {{ $cilindros }}
                </div>
                <div class="col-sm-3">
                    <label for="">Transmisión</label>
                    {{ $transmision }}
                </div>
                <div class="col-sm-3">
                    <label for="">Capacidad de personas</label>
                    {{ $capacidad }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <label for="">Capacidad en kg</label>
                    {{ $capacidad_kg }}
                </div>
                <div class="col-sm-3">
                    <label for="">Tipo de combustible</label>
                    {{ $combustible_id }}
                </div>
                <div class="col-sm-3">
                    <label data-id="colorIntId" class="js_add_color" for="">Color interior <i
                            style="cursor: pointer" class="fa fa-plus"></i></label>
                    {{ $colorIntId }}
                </div>
                <div class="col-sm-3">
                    <label data-id="colorExtId" class="js_add_color" for="">Color exterior <i
                            style="cursor: pointer" class="fa fa-plus"></i></label>
                    {{ $colorExtId }}
                </div>
            </div>
            <hr>
            <!--<h2 class="text-center">MEMO de compras</h2>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="">Mes</label>
                                        {{ $mes }}
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="">#Producto</label>
                                        {{ $no_producto }}
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="">MEMO</label>
                                        {{ $memo }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="">Costo remisión</label>
                                        {{ $costo_remision }}
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="">Mes compra</label>
                                        {{ $mes_compra }}
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="">FP</label>
                                        {{ $fp }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="">Días</label>
                                        {{ $dias }}
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="">CM</label>
                                        {{ $cm }}
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="">Comprar</label> <br>
                                        Si {{ $comprar_si }}
                                        No {{ $comprar_no }}
                                    </div>
                                </div>-->
            <hr>
            <h2 class="text-center">Datos facturación</h2>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Procedencia</label>
                    {{ $procedencia }}
                </div>
                <div class="col-sm-4">
                    <label for="">Vendedor</label>
                    {{ $vendedor }}
                </div>
                <div class="col-sm-4">
                    <label id="js_add_aduana" for="">Aduana <i style="cursor: pointer"
                            class="fa fa-plus"></i></label>
                    {{ $aduana }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Repuve</label>
                    {{ $repuve }}
                </div>
            </div>
            @if ($id != 0)
                <hr>

                <h2 class="text-center">Equipo especial</h2>
                <div class="row pull-right">
                    <div class="col-sm-12">
                        <button type="button" data-id="0" class="btn btn-default js_add_equipo">Agregar equipo
                            especial</button>
                    </div>
                </div>
                <br>
                <br>
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <div id="table-equipo">
                            <table class="table table-striped table-border">
                                <thead>
                                    <tr>
                                        <th>Importe</th>
                                        <th>Descripcion</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($equipo_especial as $e => $equipo)
                                        <tr>
                                            <td>{{ number_format($equipo->importe, 2) }}</td>
                                            <td>{{ $equipo->descripcion }}</td>
                                            <td>
                                                <i data-id="{{ $equipo->id }}" class="fa fa-edit js_add_equipo"></i>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif
            <hr>
            <h2 class="text-center">Costos Remisión</h2><br>
            <div class="row">
                <div class="col-sm-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Descripción</th>
                                <th>COSTO</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>VALOR UNIDAD</td>
                                <td>{{ $costo_valor_unidad }}</td>
                            </tr>
                            <tr>
                                <td>EQUIPO BASE</td>
                                <td>{{ $costo_equipo_base }}</td>
                            </tr>
                            <!--<tr>
                                                <td>TOTAL BASE</td>
                                                <td>{{ $costo_total_base }}</td>
                                            </tr>
                                            <tr>
                                                <td>DEDUC. FORD</td>
                                                <td>{{ $costo_deducciones_ford }}</td>
                                            </tr>
                                            <tr>
                                                <td>SEG. TRASLADOS</td>
                                                <td>{{ $costo_seguros_traslados }}</td>
                                            </tr>-->
                            <tr>
                                <td>GASTOS TRASLADOS</td>
                                <td>{{ $costo_gastos_traslados }}</td>
                            </tr>
                            <!--<tr>
                                                <td>IMPS. IMPORT.</td>
                                                <td>{{ $costo_imp_imp }}</td>
                                            </tr>
                                            <tr>
                                                <td>FLETES. EXT.</td>
                                                <td>{{ $costo_fletes_ext }}</td>
                                            </tr>-->
                            <!-- <tr>
                                                    <td>I.S.A.N.</td>
                                                    <td>{{ $costo_isan }}</td>
                                                </tr> -->
                            <tr>
                                <td>CUOTA PUB</td>
                                <td>{{ $c_cuenta_pub }}</td>
                            </tr>
                            <tr>
                                <td>HOLDBACK POR CUENTA DE LA A.M.D.F.</td>
                                <td>{{ $c_holdback }}</td>
                            </tr>
                            <tr>
                                <td>DONATIVO POR CUENTA DEL C.C.F. Y A.M.D.F.</td>
                                <td>{{ $c_donativo_ccf }}</td>
                            </tr>
                            <tr>
                                <td>SEGURO DE PLAN PISO</td>
                                <td>{{ $c_plan_piso }}</td>
                            </tr>
                            <tr>
                                <td>SUBTOTAL.</td>
                                <td>{{ $costo_subtotal }}</td>
                            </tr>
                            <tr>
                                <td>I.V.A.</td>
                                <td>{{ $costo_iva }}</td>
                            </tr>
                            <tr>
                                <td>TOTAL.</td>
                                <td>{{ $costo_total }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <label for="">Leyenda DCTO</label>
                    {{ $leyenda_dcto }}
                </div>
            </div>
            <div class="row">
            </div>
        </form>
        <br>
        <button type="button" id="guardar" class="btn btn-primary pull-right">Guardar</button>
        <br><br>
        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;"></div>
    </div>
    </div>


@endsection
@section('scripts')
    <script src="<?php echo base_url(); ?>js/custom/numeric.js"></script>
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script type="text/javascript">
        // alert(API_CONTABILIDAD);
        $('.numeric').numeric();
        $('.positive').numeric();
        $(".busqueda").select2();
        let id = "{{ $id }}"
        let unique = "{{ uniqid() }}";
        if (id != 0) {
            unique = "{{ $unique }}"; // el que tiene guardado la revisión
        }
        // var unique = '';
        //Apis asientos
        const cta_inventario_unidad = () => {
            let data = {
                clave_poliza: 'CN',
                tipo: 2,
                cuenta: $("#cuenta_inventario_unidad").val(),
                total: getTotalInventarioUnidad(),
                concepto: $("#cuenta_inventario_unidad option:selected").text(),
                departamento: 17,
                estatus: "APLICADO",
                folio: unique,
                cliente_id: 0,
                sucursal_id: 1,
            }
            $.ajax({
                dataType: "json",
                type: 'POST',
                url: API_CONTABILIDAD + '/asientos/api/agregar',
                data,
                success: function(response, _, _) {
                    if (response.status == 'success') {
                        console.log("cta_inventario_unidad: ", response)
                        cta_iva_acreditable()
                    }

                }
            });
        }
        const cta_iva_acreditable = () => {
            let data = {
                clave_poliza: 'CN',
                tipo: 2,
                cuenta: "115005",
                total: $("#costo_iva").val(),
                // total: getTotalInventarioUnidad() * .16,
                concepto: "IVA acreditable",
                departamento: 17,
                estatus: "APLICADO",
                folio: unique,
                cliente_id: 0,
                sucursal_id: 1,
            }
            $.ajax({
                dataType: "json",
                type: 'POST',
                url: API_CONTABILIDAD + '/asientos/api/agregar',
                data,
                success: function(response, _, _) {
                    if (response.status == 'success') {
                        console.log("cta_iva_acreditable: ", response)
                        cta_publicidad();
                    }

                }
            });
        }
        const cta_publicidad = () => {
            let data = {
                clave_poliza: 'CN',
                tipo: 2,
                cuenta: "510032",
                total: $("#c_cuenta_pub").val(),
                concepto: "Publicidad",
                departamento: 17,
                estatus: "APLICADO",
                folio: unique,
                cliente_id: 0,
                sucursal_id: 1,
            }
            $.ajax({
                dataType: "json",
                type: 'POST',
                url: API_CONTABILIDAD + '/asientos/api/agregar',
                data,
                success: function(response, _, _) {
                    if (response.status == 'success') {
                        console.log("cta_publicidad: ", response);
                        cta_holdback();
                    }

                }
            });
        }
        const cta_holdback = () => {
            let data = {
                clave_poliza: 'CN',
                tipo: 2,
                cuenta: "110013",
                total: $("#c_holdback").val(),
                concepto: "Holdback",
                departamento: 17,
                estatus: "APLICADO",
                folio: unique,
                cliente_id: 0,
                sucursal_id: 1,
            }
            $.ajax({
                dataType: "json",
                type: 'POST',
                url: API_CONTABILIDAD + '/asientos/api/agregar',
                data,
                success: function(response, _, _) {
                    if (response.status == 'success') {
                        console.log("cta_holdback: ", response);
                        cta_cuotas_suscripciones();
                    }

                }
            });
        }
        const cta_cuotas_suscripciones = () => {
            cta_donativos();
            return
            let data = {
                clave_poliza: 'CN',
                tipo: 2,
                cuenta: 1765,
                total: 0,
                concepto: "cuotas y suscripciones",
                departamento: 17,
                estatus: "APLICADO",
                folio: unique,
                cliente_id: 0,
                sucursal_id: 1,
            }
            $.ajax({
                dataType: "json",
                type: 'POST',
                url: API_CONTABILIDAD + '/asientos/api/agregar',
                data,
                success: function(response, _, _) {
                    if (response.status == 'success') {
                        console.log("cta_cuotas_suscripciones: ", response);
                        cta_donativos();
                    }

                }
            });
        }
        const cta_donativos = () => {
            let data = {
                clave_poliza: 'CN',
                tipo: 2,
                cuenta: "510036",
                total: $("#c_donativo_ccf").val(),
                concepto: "DONATIVOS",
                departamento: 17,
                estatus: "APLICADO",
                folio: unique,
                cliente_id: 0,
                sucursal_id: 1,
            }
            $.ajax({
                dataType: "json",
                type: 'POST',
                url: API_CONTABILIDAD + '/asientos/api/agregar',
                data,
                success: function(response, _, _) {
                    if (response.status == 'success') {
                        console.log("cta_donativos: ", response);
                        cta_redondeos();
                    }
                }
            });
        }

        const cta_redondeos = () => {
            let data = {
                clave_poliza: 'CN',
                tipo: 2,
                cuenta: 2706,
                total: 0,
                concepto: "REDONDEOS",
                departamento: 17,
                estatus: "APLICADO",
                folio: unique,
                cliente_id: 0,
                sucursal_id: 1,
            }
            $.ajax({
                dataType: "json",
                type: 'POST',
                url: API_CONTABILIDAD + '/asientos/api/agregar',
                data,
                success: function(response, _, _) {
                    if (response.status == 'success') {
                        console.log("cta_redondeos: ", response);
                        cta_ford_motor();
                    }
                }
            });
        }
        const cta_ford_motor = () => {
            cta_doc_pagar();
            return;
            let data = {
                clave_poliza: 'CN',
                tipo: 2,
                cuenta: 46,
                total: 0,
                concepto: "FORD MOTOR 6B0",
                departamento: 17,
                estatus: "APLICADO",
                folio: unique,
                cliente_id: 0,
                sucursal_id: 1,
            }
            $.ajax({
                dataType: "json",
                type: 'POST',
                url: API_CONTABILIDAD + '/asientos/api/agregar',
                data,
                success: function(response, _, _) {
                    if (response.status == 'success') {
                        console.log("cta_ford_motor: ", response);
                        cta_doc_pagar();
                    }
                }
            });
        }
        const getTotalDocPagar = () => {
            let valor_unidad = $("#c_valor_unidad").val() ? parseFloat($("#c_valor_unidad").val()) : 0;
            let cuenta_pub = $("#c_cuenta_pub").val() ? parseFloat($("#c_cuenta_pub").val()) : 0;
            let equipo_base = $("#c_equipo_base").val() ? parseFloat($("#c_equipo_base").val()) : 0;
            let gastos_traslado = $("#c_gastos_traslado").val() ? parseFloat($("#c_gastos_traslado").val()) : 0;
            let plan_piso = $("#c_plan_piso").val() ? parseFloat($("#c_plan_piso").val()) : 0;
            let holdback = $("#c_holdback").val() ? parseFloat($("#c_holdback").val()) : 0;
            let donativo_cuenta = $("#c_donativo_ccf").val() ? parseFloat($("#c_donativo_ccf").val()) : 0;
            let costo_iva = $("#costo_iva").val() ? parseFloat($("#costo_iva").val()) : 0;

            let total = valor_unidad + equipo_base + gastos_traslado + plan_piso + holdback + donativo_cuenta +
                cuenta_pub + costo_iva;
            return total;
        }
        const getTotalInventarioUnidad = () => {
            let valor_unidad = $("#c_valor_unidad").val() ? parseFloat($("#c_valor_unidad").val()) : 0;
            let equipo_base = $("#c_equipo_base").val() ? parseFloat($("#c_equipo_base").val()) : 0;
            let gastos_traslado = $("#c_gastos_traslado").val() ? parseFloat($("#c_gastos_traslado").val()) : 0;
            let plan_piso = $("#c_plan_piso").val() ? parseFloat($("#c_plan_piso").val()) : 0;
            let total = valor_unidad + equipo_base + gastos_traslado + plan_piso;
            return total;
        }

        const cta_doc_pagar = () => {
            let data = {
                clave_poliza: 'CN',
                tipo: 1,
                cuenta: "201000",
                total: getTotalDocPagar(),
                concepto: "DOC. X PAGAR F.C. UNIDADES",
                departamento: 17,
                estatus: "APLICADO",
                folio: unique,
                cliente_id: 0,
                sucursal_id: 1,
            }
            $.ajax({
                dataType: "json",
                type: 'POST',
                url: API_CONTABILIDAD + '/asientos/api/agregar',
                data,
                success: function(response, _, _) {
                    if (response.status == 'success') {
                        return window.location.href = base_url +
                            'oasis/listado';
                    }
                }
            });
        }

        $("#guardar").on('click', async function() {
            const data = {
                linea_id: $("#linea_id").val(),
                linea_id_contabilidad: $("#linea_id_contabilidad").val(),
                unidad_descripcion: $("#unidad_descripcion").val(),
                cat: $("#cat").val(),
                clave_vehicular: $("#clave_vehicular").val(),
                economico: $("#economico").val(),
                economico_visual: $("#economico_visual").val(),
                unidad_importada: $("input[name='unidad_importada']:checked").val(),
                tipo_auto: $("#tipo_auto").val(),
                clave_isan: $("#clave_isan").val(),
                cta_menudeo: $("#cta_menudeo").val(),
                cta_flotilla: $("#cta_flotilla").val(),
                cta_conauto: $("#cta_conauto").val(),
                cta_intercambio: $("#cta_intercambio").val(),
                cta_plleno: $("#cta_plleno").val(),
                cta_inventario: $("#cta_inventario").val(),
                venta_menudeo: $("#venta_menudeo").val(),
                vta_flotilla: $("#vta_flotilla").val(),
                vta_conauto: $("#vta_conauto").val(),
                vta_intercambio: $("#vta_intercambio").val(),
                venta_plleno: $("#venta_plleno").val(),
                user_id: "{{ $this->session->userdata('id') }}",
                pedimento: $("#pedimento").val(),
                fecha_pedimento: $("#fecha_pedimento").val(),
                fecha_remision: $("#fecha_remision").val(),
                serie: $("#serie").val(),
                serie_corta: $("#serie_corta").val(),
                motor: $("#motor").val(),
                intercambio: $("input[name='intercambio']:checked").val(),
                proveedor_id: $("#proveedor_id").val(),
                estatus_id: 1,
                ubicacion_id: 1,
                c_subtotal: $("#costo_subtotal").val(),
                c_total: $("#costo_total").val(),
                c_iva: $("#costo_iva").val(),
                c_valor_unidad: $("#c_valor_unidad").val(),
                c_equipo_base: $("#c_equipo_base").val(),
                // c_total_base: $("#c_total_base").val(),
                // c_deduccion_ford: $("#c_deduccion_ford").val(),
                // c_seg_traslado: $("#c_seg_traslado").val(),
                c_gastos_traslado: $("#c_gastos_traslado").val(),
                // c_imp_import: $("#c_imp_import").val(),
                // c_fletes_ext: $("#c_fletes_ext").val(),
                // c_isan: $("#c_isan").val(),
                c_holdback: $("#c_holdback").val(),
                c_cuenta_pub: $("#c_cuenta_pub").val(),
                c_donativo_ccf: $("#c_donativo_ccf").val(),
                c_plan_piso: $("#c_plan_piso").val(),
                puertas: $("#puertas").val(),
                cilindros: $("#cilindros").val(),
                transmision: $("#transmision").val(),
                combustible_id: $("#combustible_id").val(),
                capacidad: $("#capacidad").val(),
                capacidad_kg: $("#capacidad_kg").val(),
                color_int_id: $("#colorIntId").val(),
                color_ext_id: $("#colorExtId").val(),
                mes: $("#mes").val(),
                no_producto: $("#no_producto").val(),
                memo: $("#memo").val(),
                costo_remision: $("#costo_remision").val(),
                comprar: $("input[name='comprar']:checked").val(),
                mes_compra: $("#mes_compra").val(),
                fp: $("#fp").val(),
                dias: $("#dias").val(),
                cm: $("#cm").val(),
                procedencia: $("#procedencia").val(),
                vendedor: $("#vendedor").val(),
                aduana_id: $("#id_aduana").val(),
                repuve: $("#repuve").val(),
                leyenda_dcto: $("#leyenda_dcto").val(),
                no_inventario: $("#no_inventario").val(),
                clave_sat: $("#clave_sat").val(),
                unidad: $("#unidad").val(),
                modelo: $("#modelo").val(),
                cat_consecutivo: $("#cat option:selected").text(), //
                anio: $("#modelo").val(),
                cuenta_inventario_unidad: $("#cuenta_inventario_unidad").val(),
                nacional_importado: $("#nacional_importado").val(),
                unique,

            }
            console.log('data', data);
            if ($("#id").val() != 0) {
                ajax.put('api/unidades/' + $("#id").val(), data,
                    function(response, headers) {
                        if (headers.status == 400) {
                            return ajax.showValidations(headers);
                        }
                        var titulo = (headers.status != 200) ? headers.message :
                            "Información actualizada con éxito";
                        utils.displayWarningDialog("Información actualizada con éxito", "success",
                            function(
                                data) {
                                // console.log('OK: ',data);
                                // unique = data.numero_inventario;
                                // alert(unique);
                                cta_inventario_unidad();
                                // return window.location.href = base_url +
                                //     'oasis/listado';
                            })
                    })
            } else {
                ajax.post('api/unidades', data,
                    function(response, headers) {
                        console.log('response', response)
                        console.log('headers', headers)
                        if (headers.status == 400) {
                            return ajax.showValidations(headers);
                        }
                        var titulo = (headers.status != 200) ? headers.message :
                            "Información guardada con éxito";
                        utils.displayWarningDialog("Información guardada con éxito", "success",
                            function(
                                data) {
                                // console.log('OK: ',data);
                                // unique = data.numero_inventario;
                                // alert(unique);
                                cta_inventario_unidad();
                                // return window.location.href = base_url +
                                //     'oasis/listado';
                            })
                    })
            }

        })
        const saveLinea = () => {
            const data = {
                modelo: $("#modelo_cat").val(),
                descripcion: $("#descripcion_cat").val(),
                linea: $("#linea_cat").val()
            }
            ajax.post('api/cat-lineas', data,
                function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    var titulo = (headers.status != 200) ? headers.message :
                        "Información guardada con éxito";
                    utils.displayWarningDialog("Información guardada con éxito", "success",
                        function(data) {
                            const descript = response.modelo + ' ' + response.descripcion
                            $("#linea_id").append("<option value='" + response.id + "'>" + descript +
                                "</option>");
                            $("#linea_id").val(response.id);
                            $(".close").trigger('click');
                        })
                })
        }
        const saveAduana = () => {
            const data = {
                aduana: $("#aduana").val()
            }
            ajax.post('api/catalogo-aduanas', data,
                function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    var titulo = (headers.status != 200) ? headers.message :
                        "Información guardada con éxito";
                    utils.displayWarningDialog("Información guardada con éxito", "success",
                        function(data) {
                            $("#id_aduana").append("<option value='" + response.id + "'>" + response.aduana +
                                "</option>");
                            $("#id_aduana").val(response.id);
                            $(".close").trigger('click');
                        })
                })
        }
        const saveCat = () => {
            if ($("#catalogo").val() == '' || $("#_clave_vehicular").val() == '' || $("#descripcion").val() == '') {
                alert('Es necesario ingresar todos los campos');
                return;
            }
            const data = {
                cat: $("#catalogo").val(),
                clave_vehicular: $("#_clave_vehicular").val(),
                descripcion: $("#descripcion").val()
            }
            ajax.post('api/lista-precios-un', data,
                function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    var titulo = (headers.status != 200) ? headers.message :
                        "Información guardada con éxito";
                    utils.displayWarningDialog("Información guardada con éxito", "success",
                        function(data) {
                            $("#cat").append("<option value='" + response.id + "'>" + response.cat +
                                "</option>");
                            $("#cat").val(response.id);
                            $("#clave_vehicular").val(response.clave_vehicular);
                            $("#unidad_descripcion").val(response.descripcion);
                            $(".close").trigger('click');
                        })
                })
        }
        $("#js_add_linea").on('click', async function() {
            var url = site_url + "/oasis/agregar_linea/0";
            customModal(url, {}, "GET", "lg", saveLinea, "", "Guardar", "Cancelar", "Guardar línea",
                "modalLinea");
        });
        $("#js_add_aduana").on('click', async function() {
            var url = site_url + "/oasis/agregar_aduana/0";
            customModal(url, {}, "GET", "lg", saveAduana, "", "Guardar", "Cancelar", "Guardar Aduana",
                "modalAduana");
        });
        $("#js_add_cat").on('click', async function() {
            var url = site_url + "/oasis/agregar_cat/0";
            customModal(url, {}, "GET", "lg", saveCat, "", "Guardar", "Cancelar", "Guardar Catálogo",
                "modalAduana");
        });
        const saveGenericCat = () => {}
        const saveColor = () => {
            const data = {
                nombre: $("#color").val(),
                clave: $("#clave_color").val(),
            }
            ajax.post('api/catalogo-colores', data,
                function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    var titulo = (headers.status != 200) ? headers.message :
                        "Información guardada con éxito";
                    utils.displayWarningDialog("Información guardada con éxito", "success",
                        function(data) {
                            $("#colorIntId").append("<option value='" + response.id + "'>" + response.nombre +
                                "</option>");
                            $("#colorExtId").append("<option value='" + response.id + "'>" + response.nombre +
                                "</option>");
                            $("#" + id).val(response.id);
                            $(".close").trigger('click');
                        })
                })
        }
        const saveEquipo = () => {
            let id_equipo_especial = $("#id_equipo_especial").val();
            const data = {
                remision_id: $("#id").val(),
                importe: $("#importe_eq").val(),
                descripcion: $("#descripcion_eq").val(),
            }
            if (id_equipo_especial != 0) {
                ajax.put('api/equipo-especial-remision/' + id_equipo_especial, data,
                    function(response, headers) {
                        if (headers.status == 400) {
                            return ajax.showValidations(headers);
                        }
                        var titulo = (headers.status != 200) ? headers.message :
                            "Información actualizada con éxito";
                        utils.displayWarningDialog("Información actualizada con éxito", "success",
                            function(
                                data) {
                                buscarEquipoEspecial();
                                $(".close").trigger('click');
                            })
                    })
            } else {
                ajax.post('api/equipo-especial-remision', data,
                    function(response, headers) {
                        if (headers.status == 400) {
                            return ajax.showValidations(headers);
                        }
                        var titulo = (headers.status != 200) ? headers.message :
                            "Información guardada con éxito";
                        utils.displayWarningDialog("Información guardada con éxito", "success",
                            function(
                                data) {
                                buscarEquipoEspecial();
                                $(".close").trigger('click');
                            })
                    })
            }

        }
        $(".js_add_color").on('click', async function() {
            id = $(this).data('id');
            customModal(site_url + "/oasis/add_color/0", {}, "GET", "lg", saveColor, "", "Guardar color",
                "Cancelar", "Guardar",
                "modalColor");
        });
        $("body").on('click', '.js_add_equipo', async function() {
            customModal(site_url + "/oasis/add_equipo/", {
                    id: $(this).data('id')
                }, "POST", "lg", saveEquipo, "",
                "Guardar equipo especial",
                "Cancelar", "Guardar",
                "modalEquipo");
        });
        $(".search_price").on('change', function() {
            const data = {
                [$(this).prop('id')]: $(this).val()
            }
            ajax.post('api/precios/unidades/get-parameters', data,
                function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    if (!response.length) {
                        return;
                    } else {
                        const resp = response[0];
                        console.log('response', resp)
                        $("#clave_vehicular").val(resp.clave_vehicular);
                        $("#unidad_descripcion").val(resp.descripcion.replace('\"', ''));
                        $("#costo_valor_unidad").val(resp.precio_lista);
                        $("#venta_valor_unidad").val(resp.precio_cliente);
                    }
                })
        })
        $("#serie").on('change', function() {
            const valor = $(this).val();
            if (valor.length == 17) {
                $("#serie_corta").val(valor.substr(valor.length - 6));
            }
        })
    </script>

    <script>
        $("#btn_subir_remision").on('click', function(e) {
            let factura = $('#xml_remision')[0].files[0];
            let paqueteDeDatos = new FormData();
            paqueteDeDatos.append('xml_remision', factura);
            if (!utils.isDefined(factura) && !factura) {
                return toastr.error("Adjuntar Remision");
            }

            ajax.postFile(`api/unidades/xml-remision`, paqueteDeDatos, function(response, header) {
                console.log("response...")
                console.log("RESPONSE", response);
                // console.log("HEADER",header);
                if (header.status == 200 || header.status == 204) {
                    rellenar_formularios(response)
                }
                if (header.status == 500) {
                    let titulo = "Error subiendo factura!"
                    utils.displayWarningDialog(titulo, 'warning', function(result) {});
                }
            })
        })

        const rellenar_formularios = (response) => {
            $(".busqueda").select2('destroy');
            const {
                emisor
            } = response;
            const {
                concepto0
            } = response;

            const {
                comprobante
            } = response;

            const {
                traslado0
            } = response;

            const {
                pedimento
            } = response;

            //emisor
            $("#clave_vehicular").val(emisor.claveVehicular);
            $("#fp").val(emisor.financiera);
            $("#modelo").val(emisor.anioModelo);
            //Validar si existen colores
            if (!emisor.colorExterior.existe) {
                $("#colorExtId").append("<option value='" + emisor.colorExterior.id + "'>" + emisor.colorExterior
                    .color +
                    "</option>");
            }
            if (!emisor.colorInterior.existe) {
                $("#colorIntId").append("<option value='" + emisor.colorInterior.id + "'>" + emisor.colorInterior
                    .color +
                    "</option>");
            }

            $("#colorExtId").val(emisor.colorExterior.id);
            $("#colorIntId").val(emisor.colorInterior.id);
            $("#vendedor").val(emisor.vendedor);
            $("#leyenda_dcto").val(emisor.leyendaDocto);
            //Concepto0
            $("#serie").val(concepto0.NoIdentificacion);
            $("#unidad_descripcion").val(concepto0.Descripcion);
            $("#unidad").val(concepto0.ClaveUnidad);
            $("#clave_sat").val(concepto0.ClaveProdServ);
            // $("#unidad").val(concepto0.Unidad);
            //Comprobante
            $("#costo_subtotal").val(comprobante.SubTotal);
            $("#costo_total").val(comprobante.Total);
            $("#serie").trigger('change');

            let arr_fecha = comprobante.Fecha.split('T');
            $("#fecha_remision").val(arr_fecha[0])

            //IVA
            //response.donativo_ccf[0]
            if (response.total_impuestos) {
                $("#costo_iva").val(response.total_impuestos[0])
            } else {
                $("#costo_iva").val(traslado0.Importe)
            }
            $("#c_valor_unidad").val(response.valor_unidad[0])
            $("#motor").val(response.motor[0]);
            $("#transmision").val(response.transmision[0]);

            $("#c_gastos_traslado").val(response.gastos_traslado[0]);
            $("#c_holdback").val(response.holdback[0]);
            $("#c_donativo_ccf").val(response.donativo_ccf[0]);
            $("#c_plan_piso").val(response.seguro_plan_piso[0]);

            //pedimento 
            $("#pedimento").val(pedimento ? pedimento.numero : '');
            $("#fecha_pedimento").val(pedimento ? pedimento.fecha : '');
            $(".busqueda").select2();
        }
        const getEconomico = () => {
            let anio = $("#modelo").val();
            let cat = $("#cat").val();
            // let cat.substr(0,2)
        }

        function buscarEquipoEspecial() {
            var url = site_url + "/oasis/tbl_equipo_especial/" + $("#id").val();
            ajaxLoad(url, {}, "table-equipo", "POST", function() {});
        }

        $('#unidad_descripcion').keypress(function(evt) {
            var code = (evt.which) ? evt.which : evt.keyCode;
            if (code == 34 || code == 39 || code == 47 || code == 92 || code == 96 || code == 124 || code ==
                126) { // ",',/,\.
                return false;
            }
        });
    </script>
@endsection
