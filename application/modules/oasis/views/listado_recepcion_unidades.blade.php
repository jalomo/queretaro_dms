@layout('tema_luna/layout')
@section('contenido')
    <style type="text/css">
        .my_alert-danger {
            color: #a94442;
            background-color: #f2dede;
            border-color: #ebccd1;
            font-weight: bold;
            padding: 5px !important;
        }

        td a i {
            font-size: 25px !important;
        }

        .visualizar {
            visibility: visible;
        }

        .ocultar {
            display: none;
        }

    </style>
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <a href="{{ base_url('oasis/recepcion_unidades/0') }}" id="guardar"
            class="btn btn-sm btn-primary pull-right m-t-n-xs"><strong>Agregar</strong></a>
        <br><br>
        <div class="row">
            <div class="col-sm-12">
                <div id="tabla-items">
                    <table class="table table-bordered" id="tbl_remisiones" width="100%" cellspacing="0">
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script src="{{ base_url('assets/libraries/qr/qrcode.js') }}"></script>
    <script>
        inicializar_tabla_local();
        var site_url = "{{ site_url() }}";
        var cliente = '';
        var aPos = '';

        function makeCode(div, text) {
            console.log('entre');
            return new QRCode(document.getElementById(div), {
                width: 80,
                height: 80,
                text: PATH + '/oasis/buscar_serie_qr/' + text.serie,
            });
        }

        function pdf(_this) {
            let url  = PATH + '/oasis/exportar_qr/'+ $(_this).data('serie') + '/' + $(_this).data('remision_id');
            window.open(url, '_blank');
        }

        function editar(_this) {
            window.location.href = PATH + 'oasis/recepcion_unidades/' + $(_this).data('remision_id');
        }

        function generandoQrs() {
            $('table#tbl_remisiones tr td div:visible').each(function(index) {
                let identificador = $(this).attr('id');
                let array = {
                    'remision_id': $(this).data('remision_id'),
                    'serie': $(this).data('serie'),
                };
                makeCode(identificador, array);
            });
        }

        function inicializar_tabla_local() {
            var tabla_stock = $('#tbl_remisiones').DataTable({
                language: {
                    url: PATH_LANGUAGE
                },
                "ajax": {
                    url: PATH_API + "api/unidades/get-all",
                    type: 'GET',
                    dataSrc: ""
                },

                columns: [{
                        title: "QR",
                        data: 'id',
                        render: function(data, type, row) {
                            return '<div data-serie="' + row.serie + '" data-remision_id="' + row.id +
                                '" id="qr_' + row.id + '"></div>';
                        }
                    },
                    {
                        title: "#",
                        data: 'id',
                    },
                    {
                        title: "Modelo",
                        data: 'modelo',
                    },
                    {
                        title: "Unidad",
                        data: 'modelo_descripcion',
                    },
                    {
                        title: "Línea",
                        data: 'linea',
                    },
                    {
                        title: "Color interior",
                        data: 'color_interior',
                    },
                    {
                        title: "Color exterior",
                        data: 'color_exterior',
                    },
                    {
                        title: "# Económico",
                        data: 'economico',
                    },
                    {
                        title: "Ubicación",
                        data: 'ubicacion',
                    },
                    {
                        title: "Último servicio",
                        data: 'ultimo_servicio',
                    },
                    {
                        title: "-",
                        render: function(data, type, row) {
                            btn_ficha =
                                '<button  class="btn btn-primary" onclick="pdf(this)" data-remision_id="' +
                                row.id + '" data-serie="' +
                                row.serie + '"><i class="pe pe-7s-file"></i> </button>';
                            btn_editar =
                                '<button  class="btn btn-warning" onclick="editar(this)" data-remision_id="' +
                                row.id + '"><i class="fas fa-edit"></i> </button>';
                            return btn_ficha + ' ' + btn_editar;
                        }
                    }
                ],

                "fnInitComplete": function(oSettings, json) {
                    generandoQrs();
                    $('#tbl_remisiones').on('page.dt', function() {
                        setTimeout(() => {
                            generandoQrs();
                        }, 500);
                    });
                }
            });
        }
    </script>
@endsection
