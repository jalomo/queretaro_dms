<html lang="es">

<head>
    <title>Mantenimiento</title>
    <meta charset="utf-8" />
    <link rel="shortcut icon" href="/favicon.ico" />

    <style type="text/css">
        table{
            font-size: 10px;
            width: 100%;
        }
        .background-tr{
            background-color:rgba(0, 0, 0, 0.1);
        }
        .estilo-span{
            border: 1px solid;     
            display: inline-block;  
            width: 14px; 
            
        }
        .rotar-txt {
            writing-mode: vertical-lr !important;
            -webkit-transform: rotate(-180deg) !important;
            -moz-transform: rotate(-180deg) !important;
        }   .div-datos{
            border: 1px solid; 
            border-color: black;
            width: 26px;    
            display: inline-block;
            margin: -2px; 
        }   
    </style>
</head>

<body>
    <hr>
    
    <table style="width:100%;">
        <tr>
            <td colspan="10" style="text-align: right;">
                <img style="margin-bottom: -8px" src="<?php echo base_url('img/lincoln-logo.png') ?>" width="90" height="45" alt="">
                <img src="<?php echo base_url('img/logo.png') ?>" width="60" height="30" alt="">
            </td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: left;">
                <strong>CALIDAD DE FORD MEXICO</strong>
            </td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: left; color:blue; line-height: 15px; margin-bottom: 15px;">
                <strong>MANTENIMIENTO DE UNIDADES - DISTRIBUIDORES (15 Y 30 DIAS)</strong>
            </td>
        </tr>
        <tr>
            <td colspan="3">Año: <?php echo isset($unidad[0]->unidad->modelo) ? $unidad[0]->unidad->modelo : '' ?></td>
            <td colspan="3">Color: <?php echo isset($unidad[0]->unidad->color) ? $unidad[0]->unidad->color : '' ?></td>
            <td colspan="4" style="text-align: left">Catalogo: <?php echo isset($unidad[0]->unidad->cat) ? $unidad[0]->unidad->cat : '' ?></td>
        </tr>
        <tr>
            <td colspan="3">Técnico mantenimiento: <?php echo isset($unidad[0]->tecnico_mantenimiento) ? $unidad[0]->tecnico_mantenimiento : '' ?></td>
            <td colspan="3">No Orden de reparación: <?php echo isset($unidad[0]->no_orden_reparacion) ? $unidad[0]->no_orden_reparacion : '' ?></td>
            <td colspan="3">Fecha de recibo de unidad: <?php echo isset($unidad[0]->fecha_recibo_unidad) ? $unidad[0]->fecha_recibo_unidad : '' ?></td>
        </tr>
        <tr>
            <td colspan="5">VIN: <?php echo isset($unidad[0]->vin) ? $unidad[0]->vin : '' ?></td>
            <td colspan="5" style="text-align: right">Fecha de mantenimiento: <?php echo isset($unidad[0]->fecha_mantenimiento) ? $unidad[0]->fecha_mantenimiento : '' ?></td>
        </tr>
        <tr>
            <td colspan="4">Estado de la bateria: <?php echo isset($unidad[0]->estado_bateria) ? $unidad[0]->estado_bateria : '' ?></td>
            <td colspan="2" style="text-align: right">
                <?php 
                    if (isset($unidad[0]->estatus_voltaje_bateria)) {
                        if ($unidad[0]->estatus_voltaje_bateria == 'ok') {
                            echo "Voltaje ≥ 12.5 OK";
                        }else{
                            echo "Voltaje ≥ 12.5 NOK";
                        }
                    }
                ?>
            </td>
            <td colspan="4" style="text-align: right">
                Voltaje inicial: <?php echo isset($unidad[0]->voltaje_bateria_inicial) ? $unidad[0]->voltaje_bateria_inicial : '' ?>v<br>
                Voltaje final: <?php echo isset($unidad[0]->voltaje_bateria_final) ? $unidad[0]->voltaje_bateria_final : '' ?>v
            </td>
        </tr>
        <tr>
            <td colspan="5">Encender el motor hasta alcanzar la temperatura normal</td>
            <td colspan="5">
                <table>
                    <tr>
                      <td>
                        <div class="div-datos">nok</div>
                        <div class="div-datos">nok</div>
                        <div class="div-datos">nok</div>
                      </td>
                    </tr>
                  </table>
                <?php 
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                              ?><span class="estilo-span rotar-txt">17-09-1991</span><?php
                        }                        
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Encender el motor hasta alcanzar la temperatura normal</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->temperatura_normal)) {
                        if ($unidad[0]->temperatura_normal == 'ok') {
                            echo $unidad[0]->temperatura_normal;
                        }else {
                            echo $unidad[0]->temperatura_normal;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Verificar que el estereo de la unidad este apagado</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->estereo_apagado)) {
                        if ($unidad[0]->estereo_apagado == 'ok') {
                            echo $unidad[0]->estereo_apagado;
                        }else {
                            echo $unidad[0]->estereo_apagado;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Verificar que el Aire Acondicionado (A/C) este apagado</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->aire_apagado)) {
                        if ($unidad[0]->aire_apagado == 'ok') {
                            echo $unidad[0]->aire_apagado;
                        }else {
                            echo $unidad[0]->aire_apagado;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Aplicar todos los cambios de la transmición</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->cambios_transmision)) {
                        if ($unidad[0]->cambios_transmision == 'ok') {
                            echo $unidad[0]->cambios_transmision;
                        }else {
                            echo $unidad[0]->cambios_transmision;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Marcar llantas y desplazar vehículo para que roten 45°</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->rotacion_llantas)) {
                        if ($unidad[0]->rotacion_llantas == 'ok') {
                            echo $unidad[0]->rotacion_llantas;
                        }else {
                            echo $unidad[0]->rotacion_llantas;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Verificar elevadores de ventanillas y quemacocos</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->estado_elevadores)) {
                        if ($unidad[0]->estado_elevadores == 'ok') {
                            echo $unidad[0]->estado_elevadores;
                        }else {
                            echo $unidad[0]->estado_elevadores;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Verificar que no exsitan fugas de fluidos (en caso de que existan reparar)</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->fugas_fluidos)) {
                        if ($unidad[0]->fugas_fluidos == 'ok') {
                            echo $unidad[0]->fugas_fluidos;
                        }else {
                            echo $unidad[0]->fugas_fluidos;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Verificar que los protectores de los asientos esten debidamente colocados</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->protectores_asientos)) {
                        if ($unidad[0]->protectores_asientos == 'ok') {
                            echo $unidad[0]->protectores_asientos;
                        }else {
                            echo $unidad[0]->protectores_asientos;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Verificar presion de llanata. Deleantera Derecha</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->presion_llanta_dd)) {
                        if ($unidad[0]->presion_llanta_dd == 'ok') {
                            echo $unidad[0]->presion_llanta_dd;
                        }else {
                            echo $unidad[0]->presion_llanta_dd;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Verificar presion de llanata. Deleantera Izquierda</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->presion_llanta_di)) {
                        if ($unidad[0]->presion_llanta_di == 'ok') {
                            echo $unidad[0]->presion_llanta_di;
                        }else {
                            echo $unidad[0]->presion_llanta_di;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Verificar presion de llanata. Trasera Izquierda</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->presion_llanta_ti)) {
                        if ($unidad[0]->presion_llanta_ti == 'ok') {
                            echo $unidad[0]->presion_llanta_ti;
                        }else {
                            echo $unidad[0]->presion_llanta_ti;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Verificar presion de llanata. Trasera Derecha</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->presion_llanta_td)) {
                        if ($unidad[0]->presion_llanta_td == 'ok') {
                            echo $unidad[0]->presion_llanta_td;
                        }else {
                            echo $unidad[0]->presion_llanta_td;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Identificar caducidad de wrap guard y removerlo en caso necesario</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->caducidad_wrap)) {
                        if ($unidad[0]->caducidad_wrap == 'ok') {
                            echo $unidad[0]->caducidad_wrap;
                        }else {
                            echo $unidad[0]->caducidad_wrap;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Lavar con shampo espesificado por Ford</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->lavado_ford)) {
                        if ($unidad[0]->lavado_ford == 'ok') {
                            echo $unidad[0]->lavado_ford;
                        }else {
                            echo $unidad[0]->lavado_ford;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Insepaccionar la pintura</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->estado_pintura)) {
                        if ($unidad[0]->estado_pintura == 'ok') {
                            echo $unidad[0]->estado_pintura;
                        }else {
                            echo $unidad[0]->estado_pintura;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Aplicar cera autorizada por Ford (Omitir en unidades exhbidas)</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->cera_ford)) {
                        if ($unidad[0]->cera_ford == 'ok') {
                            echo $unidad[0]->cera_ford;
                        }else {
                            echo $unidad[0]->cera_ford;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Descontaminar pintura (óxido, lluvía ácida, etc.)</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->descontaminacion_pintura)) {
                        if ($unidad[0]->descontaminacion_pintura == 'ok') {
                            echo $unidad[0]->descontaminacion_pintura;
                        }else {
                            echo $unidad[0]->descontaminacion_pintura;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Inspaccionar gomas de limpiaparabrisas</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->gomas_limpiaparabrisas)) {
                        if ($unidad[0]->gomas_limpiaparabrisas == 'ok') {
                            echo $unidad[0]->gomas_limpiaparabrisas;
                        }else {
                            echo $unidad[0]->gomas_limpiaparabrisas;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Inspaccionar nivel de gasolina</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->nivel_gasolina)) {
                        if ($unidad[0]->nivel_gasolina == 'ok') {
                            echo $unidad[0]->nivel_gasolina;
                        }else {
                            echo $unidad[0]->nivel_gasolina;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Inspaccionar parte baja del vehículo</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->parte_baja_vieculo)) {
                        if ($unidad[0]->parte_baja_vieculo == 'ok') {
                            echo $unidad[0]->parte_baja_vieculo;
                        }else {
                            echo $unidad[0]->parte_baja_vieculo;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Verificar anticongelante</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->nivel_anticongelante)) {
                        if ($unidad[0]->nivel_anticongelante == 'ok') {
                            echo $unidad[0]->nivel_anticongelante;
                        }else {
                            echo $unidad[0]->nivel_anticongelante;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Verificar nivel de aciete del motor</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->nivel_aceite_motor)) {
                        if ($unidad[0]->nivel_aceite_motor == 'ok') {
                            echo $unidad[0]->nivel_aceite_motor;
                        }else {
                            echo $unidad[0]->nivel_aceite_motor;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Verificar nivel de aceite del liquido de frenos</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->nivel_liquido_frenos)) {
                        if ($unidad[0]->nivel_liquido_frenos == 'ok') {
                            echo $unidad[0]->nivel_liquido_frenos;
                        }else {
                            echo $unidad[0]->nivel_liquido_frenos;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Verificar nivel de aceite de la transmisión</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->nivel_aceite_transmicion)) {
                        if ($unidad[0]->nivel_aceite_transmicion == 'ok') {
                            echo $unidad[0]->nivel_aceite_transmicion;
                        }else {
                            echo $unidad[0]->nivel_aceite_transmicion;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Verificar nivel de los chisgueteros</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->nivel_chisgueteros)) {
                        if ($unidad[0]->nivel_chisgueteros == 'ok') {
                            echo $unidad[0]->nivel_chisgueteros;
                        }else {
                            echo $unidad[0]->nivel_chisgueteros;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Verificar nivel de aceite de la dirección</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->nivel_aceite_dieccion)) {
                        if ($unidad[0]->nivel_aceite_dieccion == 'ok') {
                            echo $unidad[0]->nivel_aceite_dieccion;
                        }else {
                            echo $unidad[0]->nivel_aceite_dieccion;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Luces delanteras</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->luces_delanteras)) {
                        if ($unidad[0]->luces_delanteras == 'ok') {
                            echo $unidad[0]->luces_delanteras;
                        }else {
                            echo $unidad[0]->luces_delanteras;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Direccionales</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->direccionales)) {
                        if ($unidad[0]->direccionales == 'ok') {
                            echo $unidad[0]->direccionales;
                        }else {
                            echo $unidad[0]->direccionales;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Luces traceras</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->luces_traceras)) {
                        if ($unidad[0]->luces_traceras == 'ok') {
                            echo $unidad[0]->luces_traceras;
                        }else {
                            echo $unidad[0]->luces_traceras;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Luz de toldo</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->luz_toldo)) {
                        if ($unidad[0]->luz_toldo == 'ok') {
                            echo $unidad[0]->luz_toldo;
                        }else {
                            echo $unidad[0]->luz_toldo;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Luz de emergencia</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->luz_emergencia)) {
                        if ($unidad[0]->luz_emergencia == 'ok') {
                            echo $unidad[0]->luz_emergencia;
                        }else {
                            echo $unidad[0]->luz_emergencia;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Luz de vanidad</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->luz_vanidad)) {
                        if ($unidad[0]->luz_vanidad == 'ok') {
                            echo $unidad[0]->luz_vanidad;
                        }else {
                            echo $unidad[0]->luz_vanidad;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Luz de frenos</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->luz_frenos)) {
                        if ($unidad[0]->luz_frenos == 'ok') {
                            echo $unidad[0]->luz_frenos;
                        }else {
                            echo $unidad[0]->luz_frenos;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Luz de reversa</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->luz_reversa)) {
                        if ($unidad[0]->luz_reversa == 'ok') {
                            echo $unidad[0]->luz_reversa;
                        }else {
                            echo $unidad[0]->luz_reversa;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Luz de placa</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->luz_placa)) {
                        if ($unidad[0]->luz_placa == 'ok') {
                            echo $unidad[0]->luz_placa;
                        }else {
                            echo $unidad[0]->luz_placa;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr class="background-tr">
            <td colspan="7">Luz de guantera</td>
            <td colspan="3">
                <?php 
                    if (isset($unidad[0]->luz_guantera)) {
                        if ($unidad[0]->luz_guantera == 'ok') {
                            echo $unidad[0]->luz_guantera;
                        }else {
                            echo $unidad[0]->luz_guantera;
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="10">Registro de kilometraje</td>
        </tr>
        <tr class="background-tr">
            <td colspan="5">De madrina kilometraje inicial</td>
            <td colspan="2"><?php echo isset($unidad[0]->kilometraje_madrina) ? $unidad[0]->kilometraje_madrina : '' ?>km</td>
            <td colspan="3">Papaeleria completa: <?php echo isset($unidad[0]->papeleria_completa) ? $unidad[0]->papeleria_completa : '' ?></td>
        </tr>
        <tr class="background-tr">
            <td colspan="5">De intercambio kilometraje inicial</td>
            <td colspan="2"><?php echo isset($unidad[0]->kilometraje_intercambio) ? $unidad[0]->kilometraje_intercambio : '' ?>km</td>
            <td colspan="3">Fecha de venta: <?php echo isset($unidad[0]->fecha_venta) ? $unidad[0]->fecha_venta : '' ?></td>
        </tr>
        <tr class="background-tr">
            <td colspan="5">De traslado kilometraje inicial</td>
            <td colspan="5"><?php echo isset($unidad[0]->kilometraje_traslado) ? $unidad[0]->kilometraje_traslado : '' ?>km</td>
        </tr>
        <tr class="background-tr">
            <td colspan="5">Prueba de carretera</td>
            <td colspan="5"><?php echo isset($unidad[0]->kilometraje_prueba) ? $unidad[0]->kilometraje_prueba : '' ?>km</td>
        </tr>
    </table>
    <hr>
</body>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        var link = document.createElement('a');
        link.href = url;
        link.download = 'file.pdf';
        link.dispatchEvent(new MouseEvent('click'));
    });
</script>

</html>
