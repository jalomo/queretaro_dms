<div class="row">
    <input type="hidden" value="{{ $pilot_id }}" id="pilot_id">
    <input type="hidden" value="{{ $remision_id }}" id="unidad_id">
    <div class="col-sm-6">
        <label for="">Selecciona un estatus</label>
        {{ $estatus }}
    </div>
    <div class="col-sm-6">
        <label for="">¿Disponible?</label> <br>
        Si <input type="radio" name="disponible" value="1" {{ $unidad_disponible ? 'checked' : '' }}>
        No <input type="radio" name="disponible" value="0" {{ !$unidad_disponible ? 'checked' : '' }}>
    </div>
</div>
