<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <div style="text-align: center" id="qr"></div>
    <strong style="font-size: 65px;">{{ $serie }}</strong>
</body>
<script src="{{ base_url('assets/libraries/qr/qrcode.js') }}"></script>

<script>
    const serie = "{{ $serie }}"
    const site_url = "{{ site_url() }}"
    new QRCode(document.getElementById('qr'), {
        width: 180,
        height: 180,
        text: site_url + '/oasis/buscar_serie_qr/' + serie,
    });
</script>

</html>
