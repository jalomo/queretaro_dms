<html lang="es">

<head>
    <title>Mantenimiento</title>
    <meta charset="utf-8" />
    <link rel="shortcut icon" href="/favicon.ico" />

    <style type="text/css">
        table{
            font-size: 7px;
            width: 100%;
        }
        .background-tr{
            background-color:rgba(0, 0, 0, 0.1);
        }
        .estilo-span{
            border: 1px solid;     
            display: inline-block;  
            width: 28px; 
            margin: -1.5px; 
            text-align: center;            
        }
        .estilo-span-inicial{
            width: 28px; 
            margin: -1.5px; 
            display: inline-block; 
        }
        .rotar-txt {
            writing-mode: vertical-lr !important;
            -webkit-transform: rotate(-360) !important;
            -moz-transform: rotate(-360deg) !important;            
        }   
        .estilo-span-voltaje{
            border: 1px solid;     
            display: inline-block;  
            width: 28px; 
            margin: -1.5px; 
            text-align: center;
        }
        .presion-llantas{
            border: 1px solid;     
            display: inline-block;  
            width: 70px; 
            margin: -2px; 
            text-align: center
        }
        .registro-kilometraje{
            border: 1px solid;     
            display: inline-block;  
            width: 35px; 
            margin: -2px; 
            text-align: center;
        }
        .linias{
            font-weight: normal !important;
            text-decoration: underline;
            white-space: pre;
        }
        .estilo-vin{
            border: 1px solid;     
            display: inline-block;  
            width: 18px; 
            height: 10px;
            margin: -1.5px; 
            text-align: center;
        }
    </style>
</head>

<body>
    <br><br><br> 
    <table style="width:100%;">
        <tr>
            <td colspan="10"><hr style="border-top: 0px solid black; margin-bottom: 7px;"></td>
        </tr>
        <tr>
            <td colspan="6" style="text-align: left;">
                <strong>CALIDAD DE FORD MEXICO</strong><br>
                <strong style="color:blue;">MANTENIMIENTO DE UNIDADES - DISTRIBUIDORES (15 Y 30 DIAS)</strong>
            </td>
            <td colspan="4" style="text-align: right;">
                <img src="<?php echo base_url('img/logo.png') ?>" width="50" height="25" alt="">
                <img style="margin-bottom: -8px" src="<?php echo base_url('img/lincoln-logo.png') ?>" width="60" height="30" alt="">
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="4">Año: <b class="linias">     <?php echo isset($unidad[0]->modelo) ? $unidad[0]->modelo : '' ?>       </b></td>
            <td colspan="4"></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="4">Color: <b class="linias">       <?php echo isset($unidad[0]->color_exterior) ? $unidad[0]->color_exterior : '' ?>       </b></td>
            <td colspan="4" style="text-align: left">Catalogo: <b class="linias">       <?php echo isset($unidad[0]->cat) ? $unidad[0]->cat : '' ?>       </b></td>
        </tr>
        <tr>
            <br>
        </tr>
        <tr>
            <td colspan="10" style="text-align: right">VIN 
                <?php 
                    for ($i=0; $i < 17; $i++) {
                        ?>
                            <span class="estilo-vin">
                                <?php
                                    if (array_key_exists($i, $serieUnidad)) {
                                        echo $serieUnidad[$i];
                                    } 
                                ?>
                            </span>
                        <?php
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="5">Técnico mantenimiento: <?php echo isset($mantenimientos[0]->tecnico_mantenimiento) ? $mantenimientos[0]->tecnico_mantenimiento : '' ?></td>
            <td colspan="5">Fecha de recibo de unidad: <?php echo isset($unidad[0]->fecha_remision) ? $unidad[0]->fecha_remision : '' ?></td>
        </tr>
        <tr>
            <td colspan="3">No Orden de reparación: <?php echo isset($unidad[0]->no_orden_reparacion) ? $unidad[0]->no_orden_reparacion : '' ?></td>
        </tr>
        <tr>
            <td colspan="4"></td>
            <td colspan="6">Fecha de mantenimiento</td>
        </tr>
        <tr>
            <td colspan="10"><br></td>
        </tr>
        <tr>
            <td colspan="4"></td>
            <td colspan="6">
                <span class="estilo-span" style="font-size: 8 !important; margin-bottom: 2px;">dia:</span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span" style="font-size: 8 !important; margin-bottom: 2px;">
                                &nbsp;
                                    <?php 
                                        echo  "";
                                    ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4"></td>
            <td colspan="6">
                <span class="estilo-span rotar-txt" style="font-size: 8 !important;">aaaa &nbsp; mm &nbsp; dd</span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span rotar-txt" style="font-size: 8 !important;">
                                &nbsp;
                                    <?php 
                                        echo  $mantenimiento->fecha_year ?> &nbsp; <?php echo $mantenimiento->fecha_mes ?> &nbsp; <?php echo $mantenimiento->fecha_dia 
                                    ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
    <tr>
        <tr>
            <td colspan="2">1 &nbsp;Estado de la bateria. 
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                Voltaje <img style="margin-bottom: -2px" width="14" height="8" src="<?php echo base_url('img/mayor_igual.png') ?>" alt=""> 12.5 V 
                <span class="estilo-span-voltaje">OK</span>, &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                Voltaje &nbsp;<img style="margin-bottom: -1px" width="8" height="6" src="<?php echo base_url('img/menor_igualpng.png') ?>" alt="">
                &nbsp; 12.49V <span class="estilo-span-voltaje">NOK</span>
            </td>
            <td colspan="2" style="text-align: right;">Voltaje inicial</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->voltaje_bateria_inicial . "v"?>
                                </span>
                            <?php
                        }
                    }
                ?>                
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Observaciones:
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <?php //echo " -" . $mantenimiento->estado_bateria ?>
                            <?php
                        }
                    }
                ?>
            </td>
            <td colspan="2" style="text-align: right;">Voltaje Final</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->voltaje_bateria_final . "v"?>
                                </span>
                            <?php
                        }
                    }
                ?>                
            </td>
        </tr>
        <tr>
            <td colspan="10">______________________________________________________________________________________________________________________</td>            
        </tr>
        <tr>
            <td colspan="10">______________________________________________________________________________________________________________________</td>            
        </tr>
        <tr>
            <td colspan="10">______________________________________________________________________________________________________________________</td>            
        </tr>
        <tr>
            <td colspan="2" style="font-weight: bold; font-size: 6px !important;">1.1 Estado de la bateria esclusivo veichulo electrico. Anote el porcentaje del estado de carga (SoC).
                &nbsp;&nbsp;&nbsp;SoC <img style="margin-bottom: -1px" width="8" height="6" src="<?php echo base_url('img/menor_igualpng.png') ?>"
                alt=""> &nbsp;90% &nbsp;<span class="estilo-span-voltaje">&nbsp;OK</span>&nbsp;&nbsp;&nbsp; 
                SoC <img style="margin-bottom: -1px" width="8" height="6" src="<?php echo base_url('img/menor_igualpng.png') ?>"
                alt="">&nbsp; 40% &nbsp;<span class="estilo-span-voltaje">NOK</span>
            </td>
            <td colspan="2" style="text-align: right;">Voltaje inicial</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->voltaje_bateria_inicial_electrico . "%"?>
                                </span>
                            <?php
                        }
                    }
                ?>                
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Observaciones: 
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <?php // echo " -" . $mantenimiento->estado_bateria ?>
                            <?php
                        }
                    }
                ?>
            </td>
            <td colspan="2" style="text-align: right;">Voltaje Final</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->voltaje_bateria_final_electrico . "%"?>
                                </span>
                            <?php
                        }
                    }
                ?>                
            </td>
        </tr>
        <tr>
            <td colspan="10">______________________________________________________________________________________________________________________</td>            
        </tr>
        <tr>
            <td colspan="10">______________________________________________________________________________________________________________________</td>            
        </tr>
        <tr>
            <td colspan="10">______________________________________________________________________________________________________________________</td>            
        </tr>
        <tr>
            <td colspan="4">2 &nbsp;Encender el motor hasta alcanzar la temperatura normal</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->temperatura_normal ?>
                                </span>
                            <?php
                        }
                    }
                ?>                
            </td>
        </tr>
        <tr>
            <td colspan="4">3 &nbsp;Verificar que el estereo de la unidad este apagado</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->estereo_apagado ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">4 &nbsp;Verificar que el Aire Acondicionado (A/C) este apagado</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->aire_apagado ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">5 &nbsp;Aplicar todos los cambios de la transmición</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->cambios_transmision ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">6 &nbsp;Marcar llantas y desplazar vehículo para que roten 45°</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->rotacion_llantas ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">7 &nbsp;Verificar elevadores de ventanillas y quemacocos</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->estado_elevadores ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">8 &nbsp;Verificar que no exsitan fugas de fluidos (en caso de que existan reparar)</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->fugas_fluidos ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">9 &nbsp;Verificar que los protectores de los asientos esten debidamente colocados</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->protectores_asientos ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">10 &nbsp;Verificar presion de llanata. 
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="presion-llantas">&nbsp;Deleantera Derecha&nbsp;</span>
            </td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->presion_llanta_dd ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="presion-llantas">&nbsp;Deleantera Izquierda&nbsp;</span>                
            </td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->presion_llanta_di ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="presion-llantas">&nbsp;Trasera Izquierda&nbsp;</span>                
            </td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->presion_llanta_ti ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="presion-llantas">&nbsp;Trasera Derecha&nbsp;</span>
            </td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->presion_llanta_td ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">11 
                Identificar caducidad de wrap guard y removerlo en caso necesario. 
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Lo tiene &nbsp;&nbsp;<span class="estilo-span-voltaje">OK</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                No lo tiene &nbsp;<span class="estilo-span-voltaje">NOK</span>
            </td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->caducidad_wrap ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">12 &nbsp;Lavar con shampo espesificado por Ford</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->lavado_ford ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">13 &nbsp;Insepaccionar la pintura</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->estado_pintura ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">14 &nbsp;Aplicar cera autorizada por Ford (Omitir en unidades exhbidas)</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->cera_ford ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">15 &nbsp;Descontaminar pintura (óxido, lluvía ácida, etc.)</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->descontaminacion_pintura ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">16 &nbsp;Inspaccionar gomas de limpiaparabrisas</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->gomas_limpiaparabrisas ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">17 &nbsp;Inspaccionar nivel de gasolina</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->nivel_gasolina ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">18 &nbsp;Inspaccionar parte baja del vehículo</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->parte_baja_vieculo ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">19 &nbsp;Verificar anticongelante</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->nivel_anticongelante ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">20 &nbsp;Verificar nivel de aciete del motor</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->nivel_aceite_motor ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">21 &nbsp;Verificar nivel de aceite del liquido de frenos</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->nivel_liquido_frenos ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">22 &nbsp;Verificar nivel de aceite de la transmisión</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->nivel_aceite_transmicion ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">23 &nbsp;Verificar nivel de los chisgueteros</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->nivel_chisgueteros ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">24 &nbsp;Verificar nivel de aceite de la dirección</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->nivel_aceite_dieccion ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">25 &nbsp;Luces delanteras</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->luces_delanteras ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">26 &nbsp;Direccionales</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->direccionales ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">27 &nbsp;Luces traceras</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->luces_traceras ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">28 &nbsp;Luz de toldo</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->luz_toldo ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">29 &nbsp;Luz de emergencia</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->luz_emergencia ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">30 &nbsp;Luz de vanidad</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->luz_vanidad ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">31 &nbsp;Luz de frenos</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->luz_frenos ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">32 &nbsp;Luz de reversa</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->luz_reversa ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">33 &nbsp;Luz de placa</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->luz_placa ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">34 &nbsp;Luz de guantera</td>
            <td colspan="6">
                <span class="estilo-span-inicial"></span>
                <?php
                    if (isset($mantenimientos)) {
                        foreach ($mantenimientos as $key => $mantenimiento) {
                            ?>
                                <span class="estilo-span-voltaje">
                                    <?php echo $mantenimiento->luz_guantera ?>
                                </span>
                            <?php
                        }
                    }
                ?>
            </td>
        </tr>
        <tr><br></tr>
        <tr>
            <td colspan="10">Nota. Despues del mantenimiento todos los sitemas deben quedar en (Off Mode)</td>
        </tr>
        <tr><br></tr>
        <tr>
            <td colspan="4"></td>
            <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;Registro de kilometraje</td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="5">De madrina kilometraje inicial</td>
            <td colspan="2"><span class="registro-kilometraje">&nbsp;<?php echo isset($mantenimientos[0]->kilometraje_madrina) ? $mantenimientos[0]->kilometraje_madrina : '' ?>km&nbsp;</span></td>
            <td colspan="3">Papaeleria completa: <?php echo isset($mantenimientos[0]->papeleria_completa) ? $mantenimientos[0]->papeleria_completa : '' ?></td>
        </tr>
        <tr>
            <td colspan="5">De intercambio kilometraje inicial</td>
            <td colspan="2"><span class="registro-kilometraje">&nbsp;<?php echo isset($mantenimientos[0]->kilometraje_intercambio) ? $mantenimientos[0]->kilometraje_intercambio : '' ?>km&nbsp;</span></td>
            <td colspan="3">Fecha de venta: <?php echo isset($mantenimientos[0]->fecha_venta) ? $mantenimientos[0]->fecha_venta : '' ?></td>
        </tr>
        <tr>
            <td colspan="5">De traslado kilometraje inicial</td>
            <td colspan="5"><span class="registro-kilometraje">&nbsp;<?php echo isset($mantenimientos[0]->kilometraje_traslado) ? $mantenimientos[0]->kilometraje_traslado : '' ?>km&nbsp;</span></td>
        </tr>
        <tr>
            <td colspan="5">Prueba de carretera</td>
            <td colspan="5"><span class="registro-kilometraje">&nbsp;<?php echo isset($mantenimientos[0]->kilometraje_prueba) ? $mantenimientos[0]->kilometraje_prueba : '' ?>km&nbsp;</span></td>
        </tr>
    </table>
    <hr style="border-top: 0px solid black;">    
</body>

</html>
