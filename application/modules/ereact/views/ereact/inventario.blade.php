@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    <form id="form-ereact">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Fecha inicio</label>
                    <input type="date" name="fecha_inicio" id="fecha_inicio" value="<?php echo set_value('fecha_inicio'); ?>" class="form-control">
                    <?php echo form_error('fecha_inicio'); ?>
                </div>
            </div>
            <div class="col-md-4 ">
                <div class="form-group">
                    <label for="">Fecha fin</label>
                    <input type="date" name="fecha_fin" id="fecha_fin" value="<?php echo set_value('fecha_fin'); ?>" class="form-control">
                    <?php echo form_error('fecha_fin'); ?>
                </div>
            </div>
            <div class="text-end mt-4">
                <button class="btn btn-primary mt-1" id="btn-buscars" type="button" onclick="inventario.getBusqueda()"> <i class="fa fa-search"></i> Buscar</button>
                <button type="button" class="btn btn-success mt-1" onclick="inventario.descargar()">
                    <i class="fa fa-cog text-primary"></i> Generar archivo .txt
                </button>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered" id="tabla-administrador" width="100%" cellspacing="0">
                </table>
            </div>
        </div>
    </form>
</div>
@endsection

@section('scripts')
<script src="{{ base_url('js/ereact/inventario.js') }}"></script>
@endsection