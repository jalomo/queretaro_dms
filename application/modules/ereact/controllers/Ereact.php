<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Ereact extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
    $this->load->library('curl');
    $this->load->helper('general');
  }

  public function salidas()
  {
    $data['modulo'] = "Ereact";
    $data['subtitulo'] = "Exportar";
    $data['submodulo'] = "Salidas";
    $this->blade->render('/ereact/salidas', $data);
  }

  public function compras()
  {
    $data['modulo'] = "Ereact";
    $data['subtitulo'] = "Exportar";
    $data['submodulo'] = "Compras";
    $this->blade->render('/ereact/compras', $data);
  }

  public function inventario()
  {
    $data['modulo'] = "Ereact";
    $data['subtitulo'] = "Exportar";
    $data['submodulo'] = "Inventario";
    $this->blade->render('/ereact/inventario', $data);
  }

  
}
