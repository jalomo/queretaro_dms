<form action="" id="frm_comentarios">
	<input type="hidden" name="id_venta" id="id_venta" value="{{$id_venta}}">
	<div class="row">
		<div class="col-sm-6">
			<strong>¿Vehículo vendido?</strong>
			<br>
			<strong>Si</strong> <input type="radio" name="vendido" value="1">
			<strong>No</strong> <input type="radio" name="vendido" value="2" checked="">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<label>Comentario</label>
			{{$input_comentario}}
			<span class="error error_comentario"></span>
		</div>
	</div>
</form>