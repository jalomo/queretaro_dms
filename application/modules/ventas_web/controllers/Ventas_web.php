<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Ventas_web extends MX_Controller
{

    /***
        
     **/
    public function __construct()
    {
        parent::__construct();

        parent::__construct();
        $this->load->model('ventas_web_model', 'vm', TRUE);
        $this->load->model('m_catalogos', 'mcat', TRUE);
        $this->load->library(array('session', 'table', 'form_validation'));
        $this->load->helper(array('form', 'html', 'url', 'date', 'general'));
        $this->load->library('curl');

        date_default_timezone_set('America/Mexico_City');
    }
    public function listado()
    {
        $opciones = array(
            '' => 'Todos los registros',
            '0' => 'Información sin observaciones',
            '1' => 'Información pendiente por corregir',
        );
        $data['drop_opciones'] = form_dropdown('informacion_erronea', $opciones, '', 'class="form-control busqueda" id="informacion_erronea"');
        $vista = 'prospectos';
        $data['ventas'] = procesarResponseApiJsonToArray($this->curl->curlPost('api/telemarketing/get-ventas-web', [
            'no_contactar' => 0,
            'vista' => $vista,
            'id_perfil' =>  $this->session->userdata('rol_id'),
            'id_usuario' => $this->session->userdata('id'),
        ]));
        //var_dump($this->session->userdata('rol_id'),$this->session->userdata('id'));die();
        $data['vista'] = $vista;
        $data['titulo'] = "Listado de prospectos de ventas";
        $this->blade->render('ventas_web/lista_ventas', $data);
    }
    public function telemarketing()
    {
        $vista = 'telemarketing';
        $data['ventas'] = procesarResponseApiJsonToArray($this->curl->curlPost('api/telemarketing/get-ventas-web', [
            'no_contactar' => 0,
            'informacion_erronea' => 0,
            'vista' => $vista,
            'id_perfil' =>  $this->session->userdata('rol_id'),
            'id_usuario' =>  $this->session->userdata('id'),
        ]));
        $data['vista'] = $vista;
        $data['titulo'] = 'Telemarketing';
        $this->blade->render('ventas_web/lista_ventas', $data);
    }
    public function asesores_venta()
    {
        $vista = 'asesores_web';
        $data['ventas'] = procesarResponseApiJsonToArray($this->curl->curlPost('api/telemarketing/get-ventas-web', [
            'no_contactar' => 0,
            'vista' => $vista,
            'id_perfil' =>  $this->session->userdata('rol_id'),
            'id_usuario' =>  $this->session->userdata('id'),
        ]));
        $data['vista'] = $vista;
        $data['titulo'] = 'Asesores ventas';
        $this->blade->render('ventas_web/lista_ventas', $data);
    }
    public function lista_ventas()
    {
        $data['vista'] = $_POST['vista'];
        $data['ventas'] = procesarResponseApiJsonToArray($this->curl->curlPost('api/telemarketing/get-ventas-web', [
            'no_contactar' => 0,
            'vista' => $data['vista'],
            'fecha' => $_POST['fecha'],
            'informacion_erronea' => isset($_POST['informacion_erronea']) ? $_POST['informacion_erronea'] : 0,
            'id_perfil' =>  $this->session->userdata('rol_id'),
            'id_usuario' =>  $this->session->userdata('id'),
        ]));
        $this->blade->set_data($data)->render('tabla_ventas');
    }
    public function historial_proactivo()
    {
        $data['proactivo'] = $this->vm->getHistorialProactivo();
        $content = $this->blade->render('ventas_web/historial_proactivo', $data, TRUE);
        $this->load->view('main/panel', array(
            'content' => $content,
            'included_js' => array('')
        ));
    }
    public function buscar_historial_proactivo()
    {
        $data['proactivo'] = $this->vm->getHistorialProactivo();
        $this->blade->render('ventas_web/tabla_historial_proactivo', $data);
    }
    function agregar_venta($id = 0, $vista = 'prospectos')
    {
        if ($id == 0) {
            $info = new Stdclass();
            $existe_cita = false;
            $id_asesor_telemarketing_siguiente = procesarResponseApiJsonToArray($this->curl->curlPost('api/telemarketing/ventas-web/siguiente-asesor', [
                'rol_id' => 9
            ]));
            $id_asesor_ventas_siguiente = procesarResponseApiJsonToArray($this->curl->curlPost('api/telemarketing/ventas-web/siguiente-asesor', [
                'rol_id' => 10
            ]));
        } else {
            $info = procesarResponseApiJsonToArray($this->curl->curlGet('api/telemarketing/ventas-web/' . $id));
            $cita = 0;
            if ($cita != 0) {
                $existe_cita = true;
            } else {
                $existe_cita = false;
            }
            $id_asesor_telemarketing_siguiente = set_value('id_usuario_telemarketing', exist_obj($info, 'id_usuario_telemarketing'));
            $id_asesor_ventas_siguiente = set_value('id_asesor_web_asignado', exist_obj($info, 'id_asesor_web_asignado'));
        }
        $data['input_id'] = $id;
        $data['existe_cita'] = $existe_cita;
        $data['input_nombre'] = form_input('nombre', set_value('nombre', exist_obj($info, 'nombre')), 'class="form-control" id="nombre" ');
        $data['input_ap'] = form_input('apellido_paterno', set_value('apellido_paterno', exist_obj($info, 'apellido_paterno')), 'class="form-control" id="apellido_paterno" ');
        $data['input_am'] = form_input('apellido_materno', set_value('apellido_materno', exist_obj($info, 'apellido_materno')), 'class="form-control" id="apellido_materno" ');
        $data['input_telefono_casa'] = form_input('telefono_secundario', set_value('telefono_secundario', exist_obj($info, 'telefono_secundario')), 'class="form-control"  id="telefono_secundario" maxlength="10" ');
        $data['input_telefono_celular'] = form_input('telefono', set_value('telefono', exist_obj($info, 'telefono')), 'class="form-control"  id="telefono" maxlength="10" ');
        $data['input_email'] = form_input('correo_electronico', set_value('correo_electronico', exist_obj($info, 'correo_electronico')), 'class="form-control" id="correo_electronico" ');
        $unidades = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-autos'));
        $data['drop_unidad'] = form_dropdown('id_unidad', array_combos($unidades, 'id', 'nombre', TRUE), set_value('id_unidad', exist_obj($info, 'id_unidad')), 'class="form-control busqueda" id="id_unidad"');
        $origenes = procesarResponseApiJsonToArray($this->curl->curlGet('api/telemarketing/catalogo-origenes'));
        $data['drop_origen'] = form_dropdown('id_origen', array_combos($origenes, 'id', 'origen', TRUE), set_value('id_origen', exist_obj($info, 'id_origen')), 'class="form-control busqueda" id="id_origen"');
        $asesores_telemarketing = procesarResponseApiJsonToArray($this->curl->curlPost('api/usuarios/usuario-by-rol', [
            'rol_id' => 9
        ]));
        $data['drop_telemarketing'] = form_dropdown('id_usuario_telemarketing', array_combos($asesores_telemarketing, 'id', 'nombre', TRUE), $id_asesor_telemarketing_siguiente, 'class="form-control busqueda" id="id_usuario_telemarketing"');

        $asesores_web = procesarResponseApiJsonToArray($this->curl->curlPost('api/usuarios/usuario-by-rol', [
            'rol_id' => 10
        ]));
        $data['drop_asesores'] = form_dropdown('id_asesor_web_asignado', array_combos($asesores_web, 'id', 'nombre', TRUE), $id_asesor_ventas_siguiente, 'class="form-control busqueda" id="id_asesor_web_asignado" disabled');
        $clientes = procesarResponseApiJsonToArray($this->curl->curlGet('api/clientes'));
        $array_clientes = [];
        foreach ($clientes as $c => $cliente) {
            $info_user = new stdClass();
            $info_user->id = $cliente->id;
            $info_user->nombre = $cliente->nombre . ' ' . $cliente->apellido_paterno . ' ' . $cliente->apellido_materno;

            $array_clientes[$c] = $info_user;
        }
        $data['drop_clientes'] = form_dropdown('id_cliente', array_combos($array_clientes, 'id', 'nombre', TRUE), set_value('id_cliente', exist_obj($info, 'id_cliente')), 'class="form-control busqueda" id="id_cliente"');
        $data['vista'] = $vista;
        $data['titulo'] = "Agregar venta web";
        $data['id_asesor_telemarketing_siguiente']  = $id_asesor_telemarketing_siguiente;
        $data['id_asesor_ventas_siguiente']  = $id_asesor_ventas_siguiente;
        $this->blade->render('ventas_web/agregar_venta_web', $data);
    }
    public function comentarios()
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('comentario', 'comentario', 'trim|required');
            $this->form_validation->set_rules('cronoFecha', 'fecha', 'trim|required');
            $this->form_validation->set_rules('cronoHora', 'hora', 'trim|required');


            if ($this->form_validation->run()) {
                $this->vm->saveComentario();
                exit();
            } else {
                $errors = array(
                    'comentario' => form_error('comentario'),
                    'cronoFecha' => form_error('cronoFecha'),
                    'cronoHora' => form_error('cronoHora'),
                );
                echo json_encode($errors);
                exit();
            }
        }
        $data['id_venta'] = $this->input->get('id_venta');
        $data['no_contactar'] = $this->input->get('no_contactar');
        $data['fecha'] = $this->input->get('fecha');
        $data['fecha_fin'] = date('Y-m-d', strtotime($data['fecha'] . ' + 8 days'));
        $data['cliente'] = $this->input->get('cliente');
        $data['input_comentario'] = form_textarea('comentario', "", 'class="form-control" id="comentario" rows="2"');
        $this->blade->render('comentarios', $data);
    }
    public function comentarios_asesor()
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('comentario', 'comentario', 'trim|required');
            if ($this->form_validation->run()) {
                $this->vm->saveComentarioAsesor();
                exit();
            } else {
                $errors = array(
                    'comentario' => form_error('comentario'),
                );
                echo json_encode($errors);
                exit();
            }
        }
        $data['id_venta'] = $this->input->get('id_venta');
        $data['input_comentario'] = form_textarea('comentario', "", 'class="form-control" id="comentario" rows="2"');

        $this->blade->render('comentario_asesor', $data);
    }
    function estatus_venta()
    {
        if ($this->input->post()) {
            if ($_POST['vendido'] == 2) {
                $this->form_validation->set_rules('comentario', 'comentario', 'trim|required');
            } else {
                $this->form_validation->set_rules('comentario', 'comentario', 'trim');
            }
            if ($this->form_validation->run()) {
                $venta = array(
                    'vendido' => $_POST['vendido'],
                    'comentario_venta' => $_POST['comentario'],
                    'fecha_comentario' => date('Y-m-d H:i:s')

                );
                $this->db->where('id', $_POST['id_venta'])->update('ventas_web', $venta);
                echo 1;
                exit();
                exit();
            } else {
                $errors = array(
                    'comentario' => form_error('comentario'),
                );
                echo json_encode($errors);
                exit();
            }
        }
        $data['id_venta'] = $this->input->get('id_venta');
        $data['input_comentario'] = form_textarea('comentario', "", 'class="form-control" id="comentario" rows="2"');

        $this->blade->render('estatus_venta', $data);
    }
    public function historial_comentarios()
    {

        $data['comentarios'] =  procesarResponseApiJsonToArray($this->curl->curlPost('api/telemarketing/get-comentarios-ventas', [
            'id_venta_web' => $_POST['id_venta'],
            'id_tipo_comentario' => $_POST['tipo_comentario']
        ]));
        $data['tipo_comentario'] = $_POST['tipo_comentario'];
        $this->blade->render('ventas_web/historial_comentarios', $data);
    }
    function generar_cita($id = 0, $id_venta = 0)
    {
        $existe_cita = procesarResponseApiJsonToArray($this->curl->curlGet('api/telemarketing/citas-ventas-get-by-idventa' . $id_venta));
        if ($existe_cita != 0 && $id == 0) {
            redirect('ventas_web/generar_cita/' . $existe_cita . '/' . $id_venta);
        }
        if ($this->input->post()) {
            $this->form_validation->set_rules('fecha', 'fecha', 'trim|required');
            $this->form_validation->set_rules('horario', 'horario', 'trim|required');
            if ($this->form_validation->run()) {
                $this->vm->guardarCita();
            } else {
                $errors = array(
                    'fecha' => form_error('fecha'),
                    'horario' => form_error('horario')
                );
                echo json_encode($errors);
                exit();
            }
        }
        if ($id_venta == 0) {
            $info = new Stdclass();
        } else {
            $info = procesarResponseApiJsonToArray($this->curl->curlGet('api/telemarketing/ventas-web/' . $id_venta));
            $cliente = procesarResponseApiJsonToArray($this->curl->curlGet('api/clientes/' . $info->id_cliente));
            $info_cliente = $cliente[0];
        }

        if ($id == 0) {
            $info_cita = new Stdclass();
            //$id_asesor_siguiente = $this->vm->getAsesorSiguiente();
            $id_asesor_siguiente = procesarResponseApiJsonToArray($this->curl->curlPost('api/telemarketing/ventas-web/siguiente-asesor', [
                'rol_id' => 10
            ]));
        } else {
            $info_cita = procesarResponseApiJsonToArray($this->curl->curlGet('api/telemarketing/citas-ventas/' . $id));
            $id_asesor_siguiente = set_value('id_asesor_web_asignado', exist_obj($info, 'id_asesor_web_asignado'));
        }

        if (isset($info->id_asesor_web_asignado) && $info->id_asesor_web_asignado != '') {
            $id_asesor_siguiente = $info->id_asesor_web_asignado;
        }

        //Días para la cita
        $fecha = date('Y-m-d');
        $ffin = strtotime('+1 month', strtotime($fecha));
        $ffin = date('Y-m-d', $ffin);
        $array_fechas = array('');
        while ($fecha <= $ffin) {
            $array_fechas[] = date_eng2esp_1($fecha);
            $fecha = strtotime('+1 day', strtotime($fecha));
            $fecha = date('Y-m-d', $fecha);
        }
        $data['id_venta'] = $id_venta;
        $data['id'] = $id;
        $data['input_nombre'] = form_input('nombre', set_value('nombre', exist_obj($info_cliente, 'nombre')), 'class="form-control" id="nombre" disabled ');
        $data['input_ap'] = form_input('apellido_paterno', set_value('apellido_paterno', exist_obj($info_cliente, 'apellido_paterno')), 'class="form-control" id="apellido_paterno" disabled ');
        $data['input_am'] = form_input('apellido_materno', set_value('apellido_materno', exist_obj($info_cliente, 'apellido_materno')), 'class="form-control" id="apellido_materno" disabled ');

        $data['input_telefono_casa'] = form_input('telefono_secundario', set_value('telefono_secundario', exist_obj($info_cliente, 'telefono_secundario')), 'class="form-control"  id="telefono_secundario" maxlength="10" disabled  ');
        $data['input_telefono_celular'] = form_input('telefono', set_value('telefono', exist_obj($info_cliente, 'telefono')), 'class="form-control"  id="telefono" maxlength="10" disabled  ');

        $data['input_email'] = form_input('correo_electronico', set_value('correo_electronico', exist_obj($info_cliente, 'correo_electronico')), 'class="form-control" id="correo_electronico" disabled ');

        $unidades = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-autos'));
        $data['drop_unidad'] = form_dropdown('id_unidad', array_combos($unidades, 'id', 'nombre', TRUE), set_value('id_unidad', exist_obj($info, 'id_unidad')), 'class="form-control busqueda" id="id_unidad"');

        $asesores_web = procesarResponseApiJsonToArray($this->curl->curlPost('api/usuarios/usuario-by-rol', [
            'rol_id' => 10
        ]));
        $data['drop_asesor'] = form_dropdown('id_asesor', array_combos($asesores_web, 'id', 'nombre', TRUE), $id_asesor_siguiente, 'class="form-control busqueda" id="id_asesor"');

        $valor_fecha = date_eng2esp_1(set_value('fecha', exist_obj($info_cita, 'fecha_cita')));

        $data['drop_fecha'] = form_dropdown_fechas('fecha_cita', $array_fechas, $valor_fecha, 'class="form-control" id="fecha_cita"');

        $data['drop_horario'] = form_dropdown('horario', array(), set_value('horario', exist_obj($info_cita, 'horario')), 'class="form-control" id="horario"');

        $data['titulo'] = 'Generar cita';
        $this->blade->render('ventas_web/generar_cita', $data);
    }
    public function getTiempo()
    {
        $fecha = date2sql($this->input->post('fecha'));
        $horarios_ocupados = procesarResponseApiJsonToArray($this->curl->curlPost('api/telemarketing/citas-ventas/horarios-ocupados', [
            "id_asesor" => $_POST['id_asesor'],
            'fecha' => $fecha
        ]));
        $tiempo_actual = date('H:i');
        $tiempo = '09:00';
        while ($tiempo <= '20:00') {
            $NuevoTiempo = strtotime('+1 hour', strtotime($tiempo));
            $tiempo = date('H:i:s', $NuevoTiempo);
            if (!in_array($tiempo, $horarios_ocupados)) {
                if ($fecha == date('Y-m-d')) {
                    if ($tiempo >= $tiempo_actual) {
                        $array_tiempo[] = $tiempo;
                    }
                } else {
                    $array_tiempo[] = $tiempo;
                }
            }
        }
        echo json_encode($array_tiempo);
        exit();
    }
    public function tablero_asesores()
    {
        $datos['fecha'] = date('Y-m-d');
        $datos['tiempo_aumento'] = 60;
        $datos['valor'] = 12;
        $datos['titulo'] = 'Tablero de asesores';
        $datos['tabla1'] = $this->vm->tabla_asesores($datos['fecha'], $datos['valor'], $datos['tiempo_aumento'], 0); // el último argumento es para saber si hace el cociente en base a 0 o a 1
        $datos['tabla2'] = $this->vm->tabla_asesores($datos['fecha'], $datos['valor'], $datos['tiempo_aumento'], 1); // el último argumento es para saber si hace el cociente en base a 0 o a 1
        $this->blade->render('tablero_asesores', $datos);
    }
    public function tabla_horarios_asesores()
    {
        $datos['fecha'] = $this->input->post('fecha');
        $datos['tiempo_aumento'] = 60;
        $datos['valor'] = 12;
        $datos['tabla1'] = $this->vm->tabla_asesores($datos['fecha'], $datos['valor'], $datos['tiempo_aumento'], 0); // el último argumento es para saber si hace el cociente en base a 0 o a 1
        $datos['tabla2'] = $this->vm->tabla_asesores($datos['fecha'], $datos['valor'], $datos['tiempo_aumento'], 1); // el último argumento es para saber si hace el cociente en base a 0 o a 1
        $this->blade->render('tabla_horarios_asesores', $datos);
    }
    public function cambiar_status_cita()
    {
        $this->db->where('id', $_POST['id'])->set('status', $_POST['status'])->update('citas_ventas');
        echo 1;
        exit();
    }
    public function informacion_erronea()
    {

        if ($this->input->post()) {
            $this->form_validation->set_rules('comentario', 'comentario', 'trim|required');
            if ($this->form_validation->run()) {
                $this->vm->saveComentarioInfoErronea();
                exit();
            } else {
                $errors = array(
                    'comentario' => form_error('comentario'),
                );
                echo json_encode($errors);
                exit();
            }
        }
        $data['id_venta'] = $this->input->get('id_venta');
        $data['input_comentario'] = form_textarea('comentario', "", 'class="form-control" id="comentario" rows="2"');

        $this->blade->render('comentario_telemarketing', $data);
    }
    function agregar_unidad_modal($id = 0)
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('unidad', 'unidad', 'trim|required');
            if ($this->form_validation->run()) {
                $this->db->set('tipo', $this->input->post('unidad'))->insert('cat_unidades');
                echo json_encode(array('exito' => true, 'id' => $this->db->insert_id(), 'unidad' => $this->input->post('unidad')));
                exit();
            } else {
                $errors = array(
                    'unidad' => form_error('unidad'),
                );
                echo json_encode(array('exito' => false, 'errors' => $errors));
                exit();
            }
        }

        $info = new Stdclass();

        $data['input_unidad'] = form_input('unidad', set_value('unidad', exist_obj($info, 'unidad')), 'class="form-control" id="unidad" ');
        $this->blade->render('agregar_unidad', $data);
    }
    function agregar_origen($id = 0)
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('origen', 'origen', 'trim|required');
            if ($this->form_validation->run()) {
                $this->db->set('origen', $this->input->post('origen'))->insert('cat_origen');
                echo json_encode(array('exito' => true, 'id' => $this->db->insert_id(), 'origen' => $this->input->post('origen')));
                exit();
            } else {
                $errors = array(
                    'origen' => form_error('origen'),
                );
                echo json_encode(array('exito' => false, 'errors' => $errors));
                exit();
            }
        }

        $info = new Stdclass();

        $data['input_origen'] = form_input('origen', set_value('origen', exist_obj($info, 'origen')), 'class="form-control" id="origen" ');
        $this->blade->render('agregar_origen', $data);
    }
    public function ActualizarInformacionErronea()
    {
        $this->db->where('id', $_POST['id_venta'])->set('informacion_erronea', 0)->update('ventas_web');
        echo 1;
        exit();
    }
    public function reportes()
    {

        $data['ventas'] = procesarResponseApiJsonToArray($this->curl->curlPost('api/telemarketing/get-ventas-web', [
            'no_contactar' => 0,
            'vista' => 'reportes',
            'id_perfil' =>  $this->session->userdata('rol_id'),
            'id_usuario' => $this->session->userdata('id'),
        ]));

        $unidades = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-autos'));
        $data['drop_unidad'] = form_dropdown('id_unidad', array_combos($unidades, 'id', 'nombre', TRUE), '', 'class="form-control busqueda" id="id_unidad"');

        $asesores_web = procesarResponseApiJsonToArray($this->curl->curlPost('api/usuarios/usuario-by-rol', [
            'rol_id' => 10
        ]));
        $data['drop_asesores'] = form_dropdown('id_asesor_web_asignado', array_combos($asesores_web, 'id', 'nombre', TRUE), '', 'class="form-control busqueda" id="id_asesor_web_asignado"');

        $asesores_telemarketing = procesarResponseApiJsonToArray($this->curl->curlPost('api/usuarios/usuario-by-rol', [
            'rol_id' => 9
        ]));
        $data['drop_telemarketing'] = form_dropdown('id_usuario_telemarketing', array_combos($asesores_telemarketing, 'id', 'nombre', TRUE), '', 'class="form-control busqueda" id="id_usuario_telemarketing"');

        $origenes = procesarResponseApiJsonToArray($this->curl->curlGet('api/telemarketing/catalogo-origenes'));
        $data['drop_origen'] = form_dropdown('id_origen', array_combos($origenes, 'id', 'origen', TRUE), '', 'class="form-control busqueda" id="id_origen"');
        $data['titulo'] = 'Reportes ventas';
        $this->blade->render('reportes', $data);
    }
    public function buscar_info_reportes()
    {
        $data['ventas'] = procesarResponseApiJsonToArray($this->curl->curlPost('api/telemarketing/get-ventas-web', [
            'no_contactar' => 0,
            'vista' => 'reportes',
            'id_perfil' =>  $this->session->userdata('rol_id'),
            'id_usuario' => $this->session->userdata('id'),
            'id_origen' => $_POST['id_origen'],
            'id_usuario_telemarketing' => $_POST['id_usuario_telemarketing'],
            'id_asesor' => $_POST['id_asesor_web_asignado'],
            'fecha_inicio' => $_POST['fecha_inicio'],
            'fecha_fin' => $_POST['fecha_fin']
        ]));
        $this->blade->render('tabla_reportes_ventas', $data);
    }
    public function reporte_to_excel()
    {
        // create file name
        $fileName = 'reporte_ventas-' . time() . '.xlsx';
        // load excel library
        $this->load->library('excel');
        $listInfo = $this->vm->getInfoReportes();

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Cliente');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Tel casa');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Tel celular');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Email');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Fecha creción venta');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Unidad');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Asesor');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Asesor telemarketing');
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Origen');
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Fecha comentario venta');
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Comentario venta');
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Fecha creación cita');
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Usuario creó cita');
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Estatus cita');
        // set Row
        $rowCount = 2;
        foreach ($listInfo as $list) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount,  $list->cliente);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $list->telefono_casa);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $list->telefono_celular);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $list->email);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, date_eng2esp_time($list->fecha_creacion));
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $list->unidad);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $list->asesor);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $list->asesor_telemarketing);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $list->origen);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $list->fecha_comentario_venta);
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $list->comentario_venta);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $list->fecha_creacion_cita);
            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $list->usuario_creo_cita);
            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $list->estatus_cita);
            $rowCount++;
        }
        $filename = "reporte_ventas_web_" . date("Y-m-d-H-i-s") . ".csv";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        $objWriter->save('php://output');
    }
}
