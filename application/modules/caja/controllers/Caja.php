<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Caja extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->load->library('curl');
        $this->load->helper('general');
        ini_set('max_execution_time', 300);
    }

    public function index()
    {
        $data['titulo'] = "Caja";
        $data['subtitulo'] = "Recibo de ingreso";

        $this->blade->render('cajas/reciboIngreso', $data);
    }

    public function construccion()
    {
        $this->blade->render('construccion');
    }

    public function realizar_corte_caja()
    {
        $data['titulo'] = "Caja";
        $data['subtitulo'] = "Realizar corte caja";
        $tipo_pago = $this->curl->curlGet('api/tipo-pago');
        $decode_tipo_pago = procesarResponseApiJsonToArray($tipo_pago);

        $data['tipo_pago'] = $decode_tipo_pago;
        $this->blade->render('/caja/realizar_corte_caja', $data);
    }

    /*public function deposito()
    {
    $data['titulo'] = "Caja/deposito";
    $this->blade->render('cajas/caja',$data);
    }

    public function com_bancaria()
    {
    $data['titulo'] = "Caja/Com. Bancaria";
    $this->blade->render('cajas/caja',$data);
    }

    public function mov_bancos()
    {
    $data['titulo'] = "Caja/Mov. Bancos";
    $this->blade->render('cajas/caja',$data);
    }*/
}
