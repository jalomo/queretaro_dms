<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Entradas extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time', 300);
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function pagos()
    {

        $data['titulo'] = "Caja";
        $data['subtitulo'] = "Entradas / Pagos";
        $cat_clientes = $this->curl->curlGet('api/clientes');
        $estatus_cuentas = $this->curl->curlGet('api/estatus-cuentas');
        $api_cuentas = $this->curl->curlGet('api/catalogo-cuentas');
        $data['cat_clientes'] = procesarResponseApiJsonToArray($cat_clientes);
        $data['estatus_cuentas'] = procesarResponseApiJsonToArray($estatus_cuentas);
        $data['catalogo_cuentas'] = procesarResponseApiJsonToArray($api_cuentas);
        $this->blade->render('entradas/pagos/listado', $data);
    }

    public function tipoPago($folio_id)
    {
        $this->load->helper('url');
        $tipo_forma_pago = $this->curl->curlGet('api/catalogo-tipo-forma-pago');
        $tipo_pago = $this->curl->curlGet('api/tipo-pago');
        $plazo_credito = $this->curl->curlGet('api/catalogo-plazo-credito');
        $usuariosAPI = $this->curl->curlPost('api/usuarios/usuario-by-rol', [
            'rol_id' => 5,
        ]);
        $cuentas_por_cobrar = $this->curl->curlGet('api/cuentas-por-cobrar/buscar-por-folio-id/' . $folio_id);

        $folio_data = $this->curl->curlGet('api/ventas/venta-by-folio/' . $folio_id);
        $data['venta'] = procesarResponseApiJsonToArray($folio_data);

        $decode_forma_pago = procesarResponseApiJsonToArray($tipo_forma_pago);
        $decode_tipo_pago = procesarResponseApiJsonToArray($tipo_pago);
        $decode_plazo_credito = procesarResponseApiJsonToArray($plazo_credito);
        $decode_usuarios = procesarResponseApiJsonToArray($usuariosAPI);
        $decode_cuentas = procesarResponseApiJsonToArray($cuentas_por_cobrar);

        if ($decode_cuentas->estatus_cuenta_id != 1) {
            redirect('caja/entradas/pagos', 'refresh');
        }
        $data['plazo_credito'] = $decode_plazo_credito;
        $data['tipo_forma_pago'] = $decode_forma_pago;
        $data['tipo_pago'] = $decode_tipo_pago;
        $data['gestores'] = $decode_usuarios;
        $data['cxc'] = $decode_cuentas;
        $data['titulo'] = "";
        $this->blade->render('entradas/pagos/tipo_pago', $data);
    }

    public function detalle_pago($id)
    {
        $dataFromApi = $this->curl->curlGet('api/cuentas-por-cobrar/' . $id);
        $data_cuentas = procesarResponseApiJsonToArray($dataFromApi);
        if ($data_cuentas->tipo_forma_pago_id == 2) {
            $this->pago_credito($data_cuentas);
        } else {
            $this->pago_contado($data_cuentas);
        }
    }
    public function pago_credito($data_cuentas)
    {
        $data['titulo'] = "Caja";
        $data['modulo'] = "Caja";

        $apiCliente = $this->curl->curlGet('api/clientes/' . $data_cuentas->cliente_id);
        $enganche_api = $this->curl->curlGet('api/abonos-por-cobrar/abonos-by-orden-entrada?orden_entrada_id=' . $data_cuentas->id . '&tipo_abono_id=1&estatus_abono_id=3');
        $abonos_pendientes_api = $this->curl->curlGet('api/abonos-por-cobrar/abonos-by-orden-entrada?orden_entrada_id=' . $data_cuentas->id . '&tipo_abono_id=2&estatus_abono_id=1');
        $abonos_pagados_api = $this->curl->curlGet('api/abonos-por-cobrar/abonos-by-orden-entrada?orden_entrada_id=' . $data_cuentas->id . '&estatus_abono_id=3');

        $anticipos_total_api = $this->curl->curlGet('api/anticipos/get-total-folio/' . $data_cuentas->folio_id);
        $enganche = count(json_decode($enganche_api)) > 0 ? procesarResponseApiJsonToArray($enganche_api) : [];
        $abonos_pendientes = procesarResponseApiJsonToArray($abonos_pendientes_api);
        $abonos_pagados = procesarResponseApiJsonToArray($abonos_pagados_api);

        $contar_abonos_pendientes = count($abonos_pendientes) - 1; // Se obtiene el total de abonos pendientes menos el que se va realizar
        $data['abono_mensual'] = $abonos_pendientes ? current($abonos_pendientes)->total_abono : 0;
        $interes_abono = ((($data['abono_mensual']) * $data_cuentas->tasa_interes) / 100); // Obtenemos el interes que se aplica a los abonos
        $abono_sin_interes = ($data['abono_mensual'] - $interes_abono) * $contar_abonos_pendientes; // Restamos el interes a los abonos y lo multiplicamos por los abonos pendientes
        $clave_cliente = $this->curl->curlGet('api/catalogo-clave-cliente');

        $data['cat_clave'] = procesarResponseApiJsonToArray($clave_cliente);
        $data['cliente'] = current(procesarResponseApiJsonToArray($apiCliente));
        $data['data_cuentas']  = $data_cuentas;
        $data['total_abonado'] = $this->procesar_total_abonos($abonos_pagados);
        $data['enganche'] = $this->procesar_enganche($enganche);
        $data['saldo_actual']  = $data_cuentas->total - $data['total_abonado'];
        $data['liquidar_saldo'] = $abono_sin_interes +  $data['abono_mensual']; // Suma de los abonos pendientes sin el interes + el abono mensual actual.

        $data['anticipos'] = procesarResponseApiJsonToArray($anticipos_total_api);
        $data['asientos'] = $this->getDataAsientos($data_cuentas->tipo_proceso_id, $data_cuentas->folio_id);

        $fechaActual = date('Y/m/d');
        $verificaAbonosPendientesApi = $this->curl->curlGet('api/abonos-por-cobrar/verifica-abonos-pendientes?cuenta_por_cobrar_id=' . $data_cuentas->id . '&fecha_actual=' . $fechaActual);
        procesarResponseApiJsonToArray($verificaAbonosPendientesApi);
        $data['subtitulo'] = "Entradas / Pago Credito";
        $this->blade->render('entradas/pagos/pago_credito', $data);
    }

    public function pago_contado($data_cuentas)
    {
        if ($data_cuentas->estatus_cuenta_id == 1) {
            redirect('caja/entradas/pagos', 'refresh');
        }
        $data['titulo'] = "Caja";
        $data['subtitulo'] = "Entradas / Pago Contado";
        $unico_pago_api = $this->curl->curlGet('api/abonos-por-cobrar/abonos-by-orden-entrada?orden_entrada_id=' . $data_cuentas->id . '&tipo_abono_id=3'); //Obtiene datos del págo al ser un unico pago
        $apiCliente = $this->curl->curlGet('api/clientes/' . $data_cuentas->cliente_id);
        $anticipos_total_api = $this->curl->curlGet('api/anticipos/get-total-folio/' . $data_cuentas->folio_id);
        $clave_cliente = $this->curl->curlGet('api/catalogo-clave-cliente');

        $data['cat_clave'] = procesarResponseApiJsonToArray($clave_cliente);
        $data['cliente'] = current(procesarResponseApiJsonToArray($apiCliente));
        $data['unico_pago'] = $unico_pago_api ? current(procesarResponseApiJsonToArray($unico_pago_api)) : null;
        $data['data_cuentas']  = $data_cuentas;
        $data['asientos'] = $this->getDataAsientos($data_cuentas->tipo_proceso_id, $data_cuentas->folio_id);
        $data['descuentos'] = $this->getDataDescuentos($data_cuentas->folio_id);
        $data['anticipos'] = procesarResponseApiJsonToArray($anticipos_total_api);
        $this->blade->render('entradas/pagos/pago_contado', $data);
    }

    public function pago_factura($id = null)
    {
        $dataFromApi = $this->curl->curlGet('api/cuentas-por-cobrar/' . $id);
        $data['data_cuentas'] = procesarResponseApiJsonToArray($dataFromApi);
        if (!in_array($data['data_cuentas']->estatus_cuenta_id, [2, 8])) {
            redirect('caja/entradas/pagos', 'refresh');
        }
        $apiCliente = $this->curl->curlGet('api/clientes/' . $data['data_cuentas']->cliente_id);

        $data['titulo'] = "Caja";
        $data['subtitulo'] = "Factura / Pagos";
        $data['cliente'] =  current(procesarResponseApiJsonToArray($apiCliente));
        // utils::pre($data);
        $this->blade->render('entradas/pagos/pago_factura', $data);
    }

    public function getDataAsientos($tipo_proceso_id, $folio_id)
    {

        $datos = [];
        if ($tipo_proceso_id == 8) {
            /***********SERVICIO**********************/
            if (SUCURSAL_DMS == 1) {
                $cuenta_refaccion = '350001';
                $cuenta_mano_obra = '350003';
                $iva_por_acreditar = '115015';
            } else if (SUCURSAL_DMS == 2) {
                $cuenta_refaccion = '360001';
                $cuenta_mano_obra = '360003';
                $iva_por_acreditar = '115015';
            }
        } else if ($tipo_proceso_id == 9) {
            /***********HOJALATERIA**********************/
            $cuenta_refaccion = '370002';
            $cuenta_mano_obra = '370003';
        }

        $api_refacciones = $this->curl->curlGet(API_CONTABILIDAD . '/asientos/api/detalle?folio=' . $folio_id . '&cuenta=' . $cuenta_refaccion, true);
        $api_mano_obra = $this->curl->curlGet(API_CONTABILIDAD . '/asientos/api/detalle?folio=' . $folio_id . '&cuenta=' . $cuenta_mano_obra, true);
        $api_iva_acreditar = $this->curl->curlGet(API_CONTABILIDAD . '/asientos/api/detalle?folio=' . $folio_id . '&cuenta=' . $iva_por_acreditar, true);

        $asientos_refacciones = procesarResponseApiJsonToArray($api_refacciones);
        $asientos_mano_obra = procesarResponseApiJsonToArray($api_mano_obra);
        $asientos_iva_acreditar = procesarResponseApiJsonToArray($api_iva_acreditar);
        $datos['total_refacciones'] = $asientos_refacciones ? $asientos_refacciones->data[0]->monto : [];
        $datos['total_mano_obra'] = $asientos_mano_obra->data ? $asientos_mano_obra->data[0]->monto : [];
        $datos['total_iva_acreditar'] = $asientos_iva_acreditar->data ? $asientos_iva_acreditar->data[0]->monto : [];
        return $datos;
    }

    public function getDataDescuentos($folio_id)
    {
        $descuento_refaccion = $this->curl->curlGet('api/descuentos/get-folio-tipo?folio_id=' . $folio_id . '&tipo_descuento=1');
        $descuento_mano_obra = $this->curl->curlGet('api/descuentos/get-folio-tipo?folio_id=' . $folio_id . '&tipo_descuento=2');
        $descuento_general = $this->curl->curlGet('api/descuentos/get-folio-tipo?folio_id=' . $folio_id . '&tipo_descuento=3');

        $data['descuento_refaccion'] = procesarResponseApiJsonToArray($descuento_refaccion);
        $data['descuento_mano_obra'] = procesarResponseApiJsonToArray($descuento_mano_obra);
        $data['descuento_general'] = procesarResponseApiJsonToArray($descuento_general);
        return $data;
    }



    public function imprime_estado_cuenta()
    {
        $cuenta_id = base64_decode($this->input->get('cuenta_id'));
        $dataCuentaApi = $this->curl->curlGet('api/cuentas-por-cobrar/' . $cuenta_id);
        $dataAbonosApi = $this->curl->curlGet('api/abonos-por-cobrar/listado-abonos-by-orden-entrada?orden_entrada_id=' . $cuenta_id);
        $data_cuentas = procesarResponseApiJsonToArray($dataCuentaApi);

        $apiCliente = $this->curl->curlGet('api/clientes/' . $data_cuentas->cliente_id);
        $data_cliente =  procesarResponseApiJsonToArray($apiCliente);

        $abonos_pagados_api = $this->curl->curlGet('api/abonos-por-cobrar/abonos-by-orden-entrada?orden_entrada_id=' . $cuenta_id . '&estatus_abono_id=3');
        $abonos_pagados = procesarResponseApiJsonToArray($abonos_pagados_api);
        $total_abonado = $this->procesar_total_abonos($abonos_pagados);
        $data_cuentas->total_abonado = $total_abonado;
        $data_cuentas->saldo_actual = $data_cuentas->total - $total_abonado;
        $abonos_pendientes_api = $this->curl->curlGet('api/abonos-por-cobrar/abonos-by-orden-entrada?orden_entrada_id=' . $cuenta_id . '&tipo_abono_id=2&estatus_abono_id=1');
        $abonos_pendientes = procesarResponseApiJsonToArray($abonos_pendientes_api);
        $data_cuentas->abono_mensual = $abonos_pendientes ? current($abonos_pendientes)->total_abono : 0;

        $data = [
            'cuenta' => $data_cuentas,
            'cliente' => current($data_cliente),
            'abonos' => procesarResponseApiJsonToArray($dataAbonosApi)
        ];
        $view = $this->load->view('entradas/pagos/formato_estado_cuenta', $data, true);
        $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
    }

    public function imprime_comprobante()
    {
        $abono_id = base64_decode($this->input->get('abono_id'));
        $abonoFromApi = $this->curl->curlGet('api/abonos-por-cobrar/' . $abono_id);
        $data_abono = procesarResponseApiJsonToArray($abonoFromApi);
        $dataCuentaApi = $this->curl->curlGet('api/cuentas-por-cobrar/' . $data_abono->cuenta_por_cobrar_id);
        $data_cuentas = procesarResponseApiJsonToArray($dataCuentaApi);

        $apiCliente = $this->curl->curlGet('api/clientes/' . $data_cuentas->cliente_id);
        $data_cliente =  procesarResponseApiJsonToArray($apiCliente);



        $data = [
            'abono' => $data_abono,
            'cuenta' => $data_cuentas,
            'cliente' => current($data_cliente),
        ];
        $view = $this->load->view('entradas/pagos/formato_comprobante_pago', $data, true);
        $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
    }

    public function asientos($id)
    {
        $data['titulo'] = "Contabilidad";
        $data['modulo'] = "Asientos contabilidad";
        $dataFromApi = $this->curl->curlGet('api/cuentas-por-cobrar/' . $id);
        $data_cuentas = procesarResponseApiJsonToArray($dataFromApi);
        $apiCliente = $this->curl->curlGet('api/clientes/' . $data_cuentas->cliente_id);

        $tipo_pago = $this->curl->curlGet('api/tipo-pago');
        $tipo_asiento = $this->curl->curlGet('api/tipo-asiento');

        $data['cat_tipo_pago'] = procesarResponseApiJsonToArray($tipo_pago);
        $data['cat_tipo_asiento'] = procesarResponseApiJsonToArray($tipo_asiento);
        $data['cliente'] = current(procesarResponseApiJsonToArray($apiCliente));
        $data['data_cuentas']  = $data_cuentas;
        $data['saldo_actual']  = $data_cuentas->total - $data['total_abonado'];
        $api_cuentas = $this->curl->curlGet('api/catalogo-cuentas');
        $data['catalogo_cuentas'] = procesarResponseApiJsonToArray($api_cuentas);


        $this->blade->render('entradas/pagos/asientos', $data);
    }

    private function procesar_enganche($enganches)
    {
        $total_enganche = 0;
        if (isset($enganches) && is_array($enganches)) {
            foreach ($enganches as $enganche) {
                if ($enganche->estatus_abono_id != 3) {
                    $total_enganche += $enganche->total_pago;
                }
            }
        }
        return $total_enganche;
    }

    private function procesar_total_abonos($abonos)
    {
        $total_abono = 0;
        if (isset($abonos) && is_array($abonos)) {
            foreach ($abonos as $abono) {
                if ($abono->estatus_abono_id == 3) {
                    $total_abono += $abono->total_pago;
                }
            }
        }
        return $total_abono;
    }
}
