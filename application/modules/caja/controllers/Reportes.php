<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Reportes extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time', 300);
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function registroCorteCaja()
    {
        $data['titulo'] = "Caja";
        $data['subtitulo'] = "Corte de caja";
        $tipo_pago = $this->curl->curlGet('api/tipo-pago');
        $decode_tipo_pago = procesarResponseApiJsonToArray($tipo_pago);

        $data['tipo_pago'] = $decode_tipo_pago;
        $this->blade->render('reportes/registro_corte_caja', $data);
    }

    public function corteCaja()
    {
        $data['titulo'] = "Caja";
        $data['subtitulo'] = "Corte de caja";
        $tipo_pago = $this->curl->curlGet('api/tipo-pago');
        $decode_tipo_pago = procesarResponseApiJsonToArray($tipo_pago);

        $data['tipo_pago'] = $decode_tipo_pago;
        $this->blade->render('reportes/listado_asientos', $data);
    }

    public function nuevo_corte()
    {
        $caja_id = ($this->input->get("caja_id")) ? $this->input->get("caja_id") : 1;
        $fecha_actual = date('Y-m-d');
        $usuario_registro = $this->session->userdata('id');
        //$fecha_actual = '2022-11-09';
        $api_valida = $this->curl->curlGet('api/corte-caja/mostrarPorUsuarioAndFecha?usuario_registro=' . $usuario_registro . '&fecha_transaccion=' . $fecha_actual);
        $validar = procesarResponseApiJsonToArray($api_valida);
        $catalogo_caja = $this->curl->curlGet('api/catalogo-caja');
        $data['caja_id'] = $caja_id;
        $data['cat_cajas'] = procesarResponseApiJsonToArray($catalogo_caja);
        if (!isset($validar->id)) {
            $data['titulo'] = "Caja";
            $data['subtitulo'] = "Corte de caja";
            $cuentas = $this->curl->curlGet('api/cuentas-por-cobrar/getCierreCaja?fecha_inicio=' . $fecha_actual. '&caja_id=' . $caja_id);
            $data['cuentas'] = procesarResponseApiJsonToArray($cuentas);
            $data['fecha_transaccion'] = $fecha_actual;
            $data['usuario_registro'] = $usuario_registro;
            $this->blade->render('reportes/corte_caja', $data);
        } else {
            $api_detalle_corte = $this->curl->curlGet('api/detalle-corte-caja/getByCajaId?corte_caja_id=' . $validar->id);
            $data['detalle_corte'] = procesarResponseApiJsonToArray($api_detalle_corte);

            $data['caja'] = $validar;
            $data['titulo'] = "Caja";
            $data['subtitulo'] = "Detalle caja";
            $this->blade->render('reportes/detalle_caja', $data);
        }
    }

    public function detalle_corte()
    {

        $fecha_actual = $this->input->get('fecha_corte');
        $usuario_registro = $this->input->get('usuario_id');
        $api_valida = $this->curl->curlGet('api/corte-caja/mostrarPorUsuarioAndFecha?usuario_registro=' . $usuario_registro . '&fecha_transaccion=' . $fecha_actual);
        $validar = procesarResponseApiJsonToArray($api_valida);
        $catalogo_caja = $this->curl->curlGet('api/catalogo-caja');
        $data['cat_cajas'] = procesarResponseApiJsonToArray($catalogo_caja);

        $api_detalle_corte = $this->curl->curlGet('api/detalle-corte-caja/getByCajaId?corte_caja_id=' . $validar->id);
        $data['detalle_corte'] = procesarResponseApiJsonToArray($api_detalle_corte);

        $data['caja'] = $validar;
        $data['titulo'] = "Caja";
        $data['subtitulo'] = "Detalle caja";
        $this->blade->render('reportes/detalle_caja', $data);
    }

    public function polizas() {
        $data['titulo'] = "Poliza de Cajas";
        $catalogo_caja = $this->curl->curlGet('api/catalogo-caja');
        $data['catalogo_caja'] = procesarResponseApiJsonToArray($catalogo_caja);

        $this->blade->render('reportes/listado_poliza', $data);
    }

    public function imprimir_reporte()
    {
        $fecha_inicio = $this->input->get('fecha_inicio');
        $fecha_fin = $this->input->get('fecha_fin');
        // /asientos/api/detalle

        $api = $this->curl->curlGet('localhost/contabilidad_queretaro/asientos/api/detalle?fecha=' . $fecha_inicio,true);
        //$totales = procesarResponseApiJsonToArray($apiTotalesVentas);

        $data = [
            'listado' => procesarResponseApiJsonToArray($api),
            'fecha_inicio' => utils::afecha($fecha_inicio,true),
            'fecha_fin' => utils::afecha($fecha_fin,true)
        ];
        
        // TODO : NO GENERA PDF
        // $this->load->view('polizas/poliza_ventas/pdfByFechas', $data);
        $view = $this->load->view('reportes/imprimir_asientospdf', $data, true);
        $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
    }

}
