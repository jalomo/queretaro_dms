<?php defined('BASEPATH') or exit('No direct script access allowed');

class Cartera_cliente extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
    }

    public function index()
    {
        $this->blade->render('cartera_cliente/index');
    }
}
