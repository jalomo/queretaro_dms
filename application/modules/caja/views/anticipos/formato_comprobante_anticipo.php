<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Comprobante anticipo</title>
	<style>
		.contenedor {
			width: 100%;
		}

		td {
			font-size: 11px !important;
		}

		.col-12 {
			width: 100%;
			padding: 3px;
		}

		.col-6 {
			float: left;
			width: 49%;
			padding: 3px;
		}

		.col-5 {
			float: left;
			width: 40%;
			padding: 3px;
		}

		table {
			border-collapse: collapse;
			width: 100%;
		}

		.text-right {
			text-align: right;
		}

		table,
		td,
		td {
			border-radius: 9px !important;
			border: 0.1px solid #233a74;
			margin-bottom: 12px;
			color: #233a74;
		}

		p,
		b {
			color: #233a74 !important;
			font-size: 10px !important;
		}

		.sep10 {
			width: 100%;
			height: 10px !important;
			clear: both;
		}

		.sep20 {
			width: 100%;
			height: 20px !important;
			clear: both;
		}
	</style>
</head>

<body>
	<div class="contenedor">
		<div class="col-12 text-right">
			<table style="width:100%; border:0px" class="table" cellpadding="5">
				<tr style="border:0px !important">
					<td style="width:35%; border:0px !important">
						<img style="height:50px;" src="<?php echo base_url('img/logo_queretaro_2.png'); ?>" class="img-fluid" />
					</td>
					<td style="width:30%; border:0px !important; font-weight:bold; text-align:right">
						<img style="height:30px;" src="<?php echo base_url('img/logo.png'); ?>" class="img-fluid" />
					</td>
					<td style="border:0px !important; font-weight:bold">
						<table style="width:120px" align="right" cellpadding="4">
							<tr>
								<td><b>NOTA DE CAJA: </b></td>
							</tr>
							<tr>
								<td style="color:red"><?php echo isset($anticipo->folios) && $anticipo->folios ? $anticipo->folios->folio : ''; ?></td>
							</tr>
							<tr>
								<td><b><?php echo (date('d/m/Y')); ?></b></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

		</div>
		<h6 style="margin-top:0px !important; margin-bottom:5px !important"> NOTA DE INGRESO A CAJA </h6><br />
		<div class="col-8">
			<b><?php echo NOMBRE_SUCURSAL; ?> S.A. DE C.V.</b>
			<p style="font-size:10px">AV. CONSTITUYENTE No. 42 OTE.<br />
				ESQUINA SIERRA DE ZIMPAN COL. VILLAS DEL SOL
				C.P. 76040 SANTIAGO DE QUERETARO QUERETARO<br />
				TEL. 01(422) 238-74-00 </p>
			<b>SUC. SAN JUAN DEL RIO</b>
			<p style="font-size:10px">AV. HIDALGO No. 162 COL. CENTRO<br />
				SAN JUAN DEL RIO QRO.
				C.P. 76040 SANTIAGO DE QUERETARO QUERETARO<br />
				TEL. 01(427) 274-73-68 AL 72 </p>
		</div>
	</div>
	<div class="col-6">
		<table style="width:100%;" class="table" cellpadding="5">
			<tr>
				<td colspan="2"><b>RECIBIMOS DE</b></td>
			<tr>
				<td style="width:70%"><?php echo isset($cliente->nombre) ? $cliente->nombre . ' ' . $cliente->apellido_paterno . ' ' . $cliente->apellido_materno : ''; ?></td>
				<td><?php echo isset($anticipo->total) ? '$' . (number_format($anticipo->total, 2)) : ''; ?></td>
			</tr>
		</table>
		<div class="clear"></div>
	</div>
	<div class="col-6">
		<table style="width:100%;" class="table" cellpadding="5">
			<tr>
				<td><b>IMPORTE $</b></td>
			<tr>
				<td><?php
					$total = round($anticipo->total, 2);
					echo isset($anticipo->total) ?  Numeros::convertirPesosEnLetras($total) : ''; ?></td>
			</tr>
		</table>
		<div class="clear"></div>
	</div>
	<div class="contenedor">
		<div class="col-12 mt-4">
			<div class="sep20"></div>
			<b>POR LO SIGUIENTE</b><br /><br />
			<table id="detalle_cuenta" style="width:100%;" class="table" cellpadding="5">
				<tr>
					<td width="120px"><b>Tipo anticipo:</b> </td>
					<td><?php echo isset($anticipo->procesos) && $anticipo->procesos ?  $anticipo->procesos->descripcion : ''; ?></td>
				</tr>
				<tr>
					<td width="120px"><b>Comentario:</b> </td>
					<td><?php echo isset($anticipo->comentario) ? $anticipo->comentario : ''; ?></td>
				</tr>
			</table>
			<div class="clear"></div>
		</div>
	</div>
</body>

</html>