@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    <hr />
    <p><b>1er .- Buscar y seleccionar el cliente al cual asignar el anticipo</b></p>
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <label>Número cliente</label>
                <input type="text" id="numero_cliente_" value="{{ $cliente->numero_cliente ? $cliente->numero_cliente : '' }}" class="form-control buscar_enter" />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>RFC</label>
                <input type="text" id="rfc_" class="form-control buscar_enter" />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Nombres</label>
                <input type="text" id="nombre_" class="form-control buscar_enter" />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Primer apellido</label>
                <input type="text" id="apellido_paterno_" class="form-control buscar_enter" />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Segundo apellido</label>
                <input type="text" id="apellido_materno_" class="form-control buscar_enter" />
            </div>
        </div>

        <div class="col-md-2 mt-4">
            <button type="button" onclick="app.filtrarTablaClientes()" class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
        </div>
        <hr />

    </div>
    <div class="row">
        <div class="col-12 text-right">
            Si no existe el cliente dar clic en el botón
            <button class="btn btn-dark btn-flujo" title="Agregar cliente" onclick="app.openModalAltaCliente()" type="button">
                <i class="fa fa-plus-circle" style="font-size:18px"></i>
                Agregar cliente
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="tbl_cliente" width="100%" cellspacing="0"></table>
            </div>
        </div>
    </div>
    <div class="row mb-4 mt-4">
        <div class="col-md-6">
            <div class="form-group">
                <label>*Proceso</label>
                <select class="form-control" id="tipo_proceso_id" name="tipo_proceso_id" style="width:100%">
                    <option value="">Selecionar ...</option>
                    @foreach ($tipo_procesos as $proceso)
                    <option value="{{$proceso->id}}">{{ $proceso->descripcion }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>*Total:</label>
                <input type="text" name="total" id="total" class="form-control money_format">
            </div>
        </div>
        <div class="col-md-6">
            <label>*Fecha</label>
            <input type="date" name="fecha" id="fecha" class="form-control" value="{{ date('Y-m-d') }}">
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>*Comentario:</label>
                <input type="text" name="comentario" id="comentario" class="form-control">
            </div>
        </div>

        <div class="col-md-12 text-right">
            <button onclick="app.regresar()" class="btn btn-primary">
                <i class="fas fa-arrow-left"></i>
                Regresar
            </button>
            <button onclick="app.generateAnticipo()" class="btn btn-primary btn-flujo" id="btn-guardar-venta">
                <i class="fas fa-save"></i>
                Guardar anticipo
            </button>
        </div>
    </div>

</div>
@endsection
<div class="modal fade" id="modal-alta-cliente" data-backdrop="static" data-keyboard="false" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="modalagregarcliente" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal_cliente"> Agregar cliente</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="form-cliente" class="col-md-12">
                        @include('refacciones/ventas_mostrador/partials/registro_cliente')
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="btn_cerrar_permiso" data-dismiss="modal">Cancelar</button>
                <button id="btn_agregar_cliente" onClick="app.guardarCliente()" type="button" class="btn btn-primary">
                    <i class="fas fa-save"></i> Guardar
                </button>
                <button id="btn_editar_cliente" style="display:none" onClick="app.updateCliente()" type="button" class="btn btn-primary">
                    <i class="fas fa-edit"></i> Actualizar
                </button>

            </div>
        </div>
    </div>
</div>
@section('scripts')
<script src="{{ base_url('js/caja/anticipos/nuevo.js') }}"></script>
@endsection