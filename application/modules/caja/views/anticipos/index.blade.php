@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    <hr />
    <div class="row mt-4">
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Estatus:</label>
                <select name="estatus_id" id="estatus_id" class="form-control">
                    <option value="">Selecionar ...</option>
                    @foreach ($estatus_anticipos as $estatus)
                    <option value="{{ $estatus->id}}"> {{ $estatus->nombre }} </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-8 mt-4">
                <button type="button" id="btn-buscar" onclick="filtrar()" class="btn btn-primary ">
                    <i class="fa fa-search" aria-hidden="true"></i> Filtrar
                </button>
        </div>
    </div>
    <hr />
    <div class="row mb-4">
        <div class="col-md-12 text-right">
            <a href="<?php echo site_url('caja/anticipos/nuevo');?>" type="button" class="btn btn-primary" type="button"><i class="fa fa-plus"></i>&nbsp;Crear anticipo</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-bordered" id="table_anticipos" width="100%" cellspacing="0">
            </table>
        </div>
    </div>

</div>
@endsection
@section('scripts')
<script src="{{ base_url('js/caja/anticipos/index.js') }}"></script>
@endsection