@layout('tema_luna/layout')
@section('contenido')
    <div id="app">
        <div class="container-fluid panel-body rounded">
            <h1 class="font-weight-bold">Actualizar Cuentas Cliente</h1>
            <router-view></router-view>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-router/2.0.2/vue-router.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.26.0/axios.min.js"></script>
    <script src="{{ base_url("/js/custom/moment.js") }}"></script>
    <script src="{{ base_url("/assets/components/caja/index.js") }}"></script>
    <script>
        var routes = [
            { path: "/", component: Caja },
        ]

        var router = new VueRouter({
            routes
        })

        Vue.prototype.moment = moment

        new Vue({
            el: "#app",
            router: router,
        })

    </script>
@endsection