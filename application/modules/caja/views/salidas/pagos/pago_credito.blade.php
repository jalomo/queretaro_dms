@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
	<h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
		<li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
	</ol>
	<div class="row">
		<div class="col-md-12">
			<table width="100%" class="table table-bordered" cellpadding="5">
				<tr>
					<th colspan="2">Datos del proveedor</th>
				</tr>
				<tr>
					<td width="120px">Proveedor: </td>
					<td><?php echo isset($proveedor->proveedor_nombre) ? $proveedor->proveedor_nombre : ''; ?>
					</td>
				</tr>
				<tr>
					<td width="120px">Núm. proveedor: </td>
					<td><?php echo isset($proveedor->proveedor_numero) ? $proveedor->proveedor_numero : ''; ?></td>
				</tr>
				<tr>
					<td width="120px">RFC: </td>
					<td><?php echo isset($proveedor->proveedor_rfc) ? $proveedor->proveedor_rfc : ''; ?></td>
				</tr>
				<tr>
					<td width="120px">Razón social: </td>
					<td><?php echo isset($proveedor->razon_social) ? $proveedor->razon_social : ''; ?></td>
				</tr>
			</table>
		</div>
	
	</div>
	<h3>Detalle de la compra </h3>
	<div class="row mt-4">
		<div class="col-md-12">
			<table width="100%" class="table table-bordered" cellpadding="5">
				<tr>
					<th colspan="4">Detalle de la compra [ <?php echo isset($data_cuentas->estatus_cuenta) ? $data_cuentas->estatus_cuenta : ''; ?> ]</th>
				<tr>
					<th width="120px">Folio: </th>
					<td><?php echo isset($data_cuentas->folio) ? $data_cuentas->folio : ''; ?></td>
					<th width="120px">Concepto: </th>
					<td><?php echo isset($data_cuentas->concepto) ? $data_cuentas->concepto : ''; ?></td>
				</tr>
				<tr>
					<th width="120px">Saldo neto: </th>
					<td><?php echo isset($data_cuentas->importe) ? '$'.(number_format($data_cuentas->importe,2)) : ''; ?></td>
					<th width="120px">Tasa de interes %: </th>
					<td><?php echo isset($data_cuentas->tasa_interes) ? $data_cuentas->tasa_interes : ''; ?></td>
				</tr>
				<tr>
					<th width="120px">Total a pagar: </th>
					<td><?php echo isset($data_cuentas->total) ? '$'.(number_format($data_cuentas->total,2)) : ''; ?></td>
					<th width="120px">Plazo: </th>
					<td><?php echo isset($data_cuentas->plazo_credito) ? $data_cuentas->plazo_credito : ''; ?></td>
				</tr>
				<tr>
					<th width="120px">Monto actual: </th>
					<td><?php echo '$'.(number_format($saldo_actual,2)); ?></td>
					<th width="120px">Monto abonado: </th>
					<td><?php echo isset($total_abonado) ? '$'.(number_format($total_abonado,2)) : ''; ?></td>
				</tr>
				<tr>
					<th width="120px">Abono mensual: </th>
					<?php if($data_cuentas->tipo_forma_pago_id == 2) { ?>
					<td><?php echo  '$'. (number_format($abono_mensual,2)); ?>
					<?php } else { ?>
					<td>0</td>
					<?php } ?>
					</td>
					<th width="120px">Fecha compra: </th>
					<td><?php echo isset($data_cuentas->fecha) ? utils::aFecha($data_cuentas->fecha,true) : ''; ?></td>

				</tr>
			</table>
		</div>
	</div>
	<h3>Listado de abonos</h3>
	<div class="row">
		<div class="col-md-12 mb-1">
			<div class="table-responsive">
				<table class="table table-striped table-bordered" id="tbl_abonos" width="100%" cellspacing="0">
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-12 mt-5 mb-5">
		<div class="text-right">
			<a href="<?php echo site_url('caja/salidas/pagos');?>" type="button" class="btn btn-secondary col-md-2"><i class="fa fa-chevron-left"></i>&nbsp; Regresar</a>
		</div>
	</div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
	let count = 1;
	let cuenta_por_pagar_id = "<?php echo isset($data_cuentas->id) ? $data_cuentas->id : ''; ?>";
	let estatus_cuentas_por_cobrar = "<?php echo isset($data_cuentas->estatus_cuenta_id) ? $data_cuentas->estatus_cuenta_id : ''; ?>";
	let cantidad_mes = "<?php echo isset($data_cuentas->cantidad_mes) ? $data_cuentas->cantidad_mes : ''; ?>";
	let importe = "<?php echo isset($data_cuentas->importe) ? $data_cuentas->importe : ''; ?>";
	let total_pagar = "<?php echo isset($data_cuentas->total) ? $data_cuentas->total : ''; ?>";
	let saldo_actual = "<?php echo isset($saldo_actual) ? $saldo_actual : ''; ?>";
	let abonoacomulado = 0;
</script>
<script src="{{ base_url('js/caja/salidas/pagos/pago_credito.js') }}"></script>
@endsection

@section('modal')
<div class="modal fade" id="modal-abonar" data-toggle="modal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="title_modal">Realizar abono</h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<?php echo renderInputText("number", "total_pago", "Abono mensual", round($abono_mensual,2)); ?>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Pago:</label>
							<input type="number" name="pago" id="pago" class="form-control">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Cambio:</label>
							<input type="number" name="cambio" id="cambio" class="form-control">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Tipo de pago</label>
							<select class="form-control" id="tipo_pago_id" name="tipo_pago_id" style="width: 100%;">
								<option value="">Selecionar ...</option>
								@if(!empty($cat_tipo_pago))
								@foreach ($cat_tipo_pago as $pago)
								<option value="{{$pago->id}}">[{{$pago->clave}}] {{$pago->nombre}}</option>
								@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Tipo de CDFI</label>
							<select class="form-control" id="cfdi_id" name="cfdi_id" style="width: 100%;">
								<option value="">Selecionar ...</option>
								@if(!empty($cat_cfdi))
								@foreach ($cat_cfdi as $cfdi)
								<option value="{{$cfdi->id}}">[{{$cfdi->clave}}] {{$cfdi->descripcion}}</option>
								@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Caja</label>
							<select class="form-control" id="caja_id" name="caja_id" style="width: 100%;">
								<option value="">Selecionar ...</option>
								@if(!empty($catalogo_caja))
								@foreach ($catalogo_caja as $caja)
								<option value="{{$caja->id}}">{{$caja->nombre}}</option>
								@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<?php echo renderInputText("date", "fecha_pago", "Fecha abono", ''); ?>
					</div>
					<label class="mt-3 mb-2"><b>Liquidas con: <?php echo '$'.(number_format($liquidar_saldo,2));?></b></label>

					<input type="hidden" id="tipo_abono_id" value="2"/>
					<input type="hidden" id="minimo_abonar" value=""/>
					<input type="hidden" id="abono_id" value=""/>
					<input type="hidden" id="monto_moratorio" value="0"/>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				<button id="btn-modal-abonar" type="button" class="btn btn-primary"><i class="fas fa-cash-register"></i>
					Guardar</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-enganche" data-toggle="modal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="title_modal">Pagar enganche</h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<?php echo renderInputText("number", "total_pago_enganche", "Enganche", isset($data_cuentas->enganche) ? round($data_cuentas->enganche, 2) : ''); ?>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Pago:</label>
							<input type="number" name="pago" id="pago_enganche" class="form-control">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Cambio:</label>
							<input type="number" name="cambio" id="cambio_enganche" class="form-control">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Tipo de pago</label>
							<select class="form-control" id="tipo_pago_id_enganche" name="tipo_pago_id" style="width: 100%;">
								<option value="">Selecionar ...</option>
								@if(!empty($cat_tipo_pago))
								@foreach ($cat_tipo_pago as $pago)
								<option value="{{$pago->id}}">[{{$pago->clave}}] {{$pago->nombre}}</option>
								@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Caja</label>
							<select class="form-control" id="caja_id_enganche" name="caja_id_enganche" style="width: 100%;">
								<option value="">Selecionar ...</option>
								@if(!empty($catalogo_caja))
								@foreach ($catalogo_caja as $caja)
								<option value="{{$caja->id}}">{{$caja->nombre}}</option>
								@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<?php echo renderInputText("date", "fecha_pago_enganche", "Fecha abono", ''); ?>
					</div>
					<input type="hidden" id="tipo_abono_id_enganche" value="1"/>
					<input type="hidden" id="enganche_id" value=""/>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				<button id="btn-modal-enganche" type="button" class="btn btn-primary"><i class="fas fa-cash-register"></i>
					Pagar</button>
			</div>
		</div>
	</div>
</div>
@endsection



