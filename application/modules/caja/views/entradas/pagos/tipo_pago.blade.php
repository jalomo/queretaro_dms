@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h2 class="mt-4">Elección tipo pago- <small>Credito - contado</small></h2>


    <div class="row mt-5">
        <div class="col-md-4">
            <?php echo renderInputText('text', 'folio', 'Folio', isset($cxc) ? $cxc->folio
                : '', true); ?>
        </div>
        <div class="col-md-4">
            <?php echo renderInputText('text', 'venta_total', 'Total', isset($cxc->total) ? $cxc->total
                : '', true); ?>
        </div>
        <div class="col-md-4">
            <?php echo renderInputText('text', 'cliente', 'Cliente', isset($cxc) &&
                isset($cxc->nombre_cliente) ? $cxc->nombre_cliente . ' ' . $cxc->apellido_paterno . ' ' . $cxc->apellido_materno : '', true); ?>
        </div>
        <input type="hidden" value="{{ isset($cxc->id) ? $cxc->id : '' }}" name="cxc_id" id="cxc_id">
        <input type="hidden" value="{{ isset($cxc) && $cxc->cliente_id ? $cxc->cliente_id : '' }}" id="cliente_id" name="cliente_id">
        <input type="hidden" value="{{ isset($cxc) && $cxc->folio_id ? $cxc->folio_id : '' }}" id="folio_id" name="folio_id">
        <input type="hidden" value="{{ isset($cxc) && $cxc->tipo_proceso_id ? $cxc->tipo_proceso_id : '' }}" id="tipo_proceso_id" name="tipo_proceso_id">
    </div>
    <hr />
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="select">Concepto</label>
                <textarea name="concepto" id="concepto" class="form-control" rows="3" style="min-height:100px" maxlength="500"><?php echo isset($cxc->concepto) ? $cxc->concepto : ''; ?></textarea>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="select">¿Compra de credito ó contado?</label>
                <select name="tipo_forma_pago_id" class="form-control " id="tipo_forma_pago_id">
                    <option value=""> Seleccionar</option>
                    @foreach ($tipo_forma_pago as $tipo)
                    @if($tipo->id == $cxc->tipo_forma_pago_id)
                    <option value="{{ $tipo->id}}" selected="selected"> {{ $tipo->descripcion}} </option>
                    @else
                    <option value="{{ $tipo->id}}"> {{ $tipo->descripcion}} </option>
                    @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-4" style="display:none">
            <div class="form-group">
                <label for="select">¿De que manera realizará el pago?</label>
                <select name="tipo_pago_id" class="form-control " id="tipo_pago_id">
                    <option value=""> Seleccionar</option>
                    @foreach ($tipo_pago as $tipo)
                    @if(isset($cxc->tipo_pago_id) && $cxc->tipo_pago_id == $tipo->id)
                    <option value="{{ $tipo->id}}" selected="selected"> {{ $tipo->clave}} {{ $tipo->nombre}} </option>
                    @else
                    <option value="{{ $tipo->id}}"> {{ $tipo->clave}} {{ $tipo->nombre}} </option>
                    @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-4 container-forma-pago">
            <div class="form-group">
                <label for="select">Plazo de credito</label>
                <select name="plazo_credito_id" class="form-control " id="plazo_credito_id">
                    <option value=""> Seleccionar</option>
                    @foreach ($plazo_credito as $plazo)
                    @if(isset($cxc->plazo_credito_id) && $cxc->plazo_credito_id == $plazo->id)
                    <option data-cantidad_mes="{{$plazo->cantidad_mes}}" selected="selected" value="{{ $plazo->id}}"> {{ $plazo->nombre}} </option>
                    @else
                    <option value="{{ $plazo->id}}"> {{ $plazo->nombre}} </option>
                    @endif
                    @endforeach
                </select>
            </div>
        </div>
        <!-- <div class="col-md-4 container-forma-pago">
            <div class="form-group">
                <label for="select">Enganche</label>
                <input type="text" name="enganche" id="enganche" class="form-control" value="0" />
            </div>
        </div> -->
        <!-- <div class="col-md-4 container-forma-pago">
            <div class="form-group">
                <label for="select">Tasa de interes %</label>
                <input type="text" name="tasa_interes" id="tasa_interes" class="form-control" value="<?php echo isset($cxc->tasa_interes) ? $cxc->tasa_interes : ''; ?>" />
            </div>
        </div> -->
        <div class="col-md-4 container-forma-pago">
            <div class="form-group">
                <label for="select">Cobrador asignar</label>
                <select name="usuario_gestor_id" class="form-control " id="usuario_gestor_id">
                    <option value=""> Seleccionar</option>
                    @foreach ($gestores as $gestor)
                    @if(isset($cxc->usuario_gestor_id) && $cxc->usuario_gestor_id == $gestor->id)
                    <option value="{{ $gestor->id}}" selected="selected"> {{ $gestor->nombre }} - {{ $gestor->apellido_paterno}} </option>
                    @else
                    <option value="{{ $gestor->id}}"> {{ $gestor->nombre }} - {{ $gestor->apellido_paterno}} </option>
                    @endif
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <input type="hidden" name="enganche" id="enganche" class="form-control" value="0" />
    <input type="hidden" name="tasa_interes" id="tasa_interes" class="form-control" value="<?php echo isset($cxc->tasa_interes) ? $cxc->tasa_interes : ''; ?>" />

    <div class="row mb-4 mt-4">
        <div class="col-md-12 text-right">
            <button class="btn btn-primary" id="btn-finalizar"><i class="fas fa-play"></i> Comenzar venta</button>
        </div>
    </div>
    <div class="row mt-3">
        <div class="text-left col-md-2">
            <a href="{{ site_url('caja/entradas/pagos') }}" class="btn btn-primary "><i class="fas fa-arrow-left"></i> Regresar</a>
        </div>
    </div>
    <br>

</div>
@endsection

@section('scripts')
<script>
    $(".container-forma-pago").hide();
    if ($("#tipo_forma_pago_id").val() == 2) {
        $(".container-forma-pago").show();
    }
    $("#tipo_forma_pago_id").on("change", function() {
        let tipo_forma_pago_id = $("#tipo_forma_pago_id").val();
        if (tipo_forma_pago_id == 2) {
            $(".container-forma-pago").show();
            $("#plazo_credito_id option[value='']").attr('selected', true);
        } else {
            $(".container-forma-pago").hide();
            $("#plazo_credito_id option[value=14]").attr('selected', true);
        }

    });

    let estatus_venta_id = $('#estatus_venta_id').val();


    $("#btn-finalizar").on("click", function() {
        // if (jQuery.inArray($("#tipo_proceso_id").val(), ['1', '8']) != -1) {
        //     alert("entra");
        // }
        // return false;
        $.isLoading({
            text: "Realizando proceso de venta ...."
        });

        if (tipo_forma_pago_id.value == 2) {
            realizarVenta();
            /*ajax.post(`api/clientes/tiene-credito`, {
                id: cliente_id.value
            }, function(response, headers) {
                if (headers.status == 201 || headers.status == 200) {
                    $.isLoading("hide");
                    if (!response.aplica_credito) {
                        utils.displayWarningDialog("El cliente no cuenta con credito", "warning", function(data) {
                            if (data.value) {
                                return false;
                            }
                        })
                    } else if (parseFloat(venta_total.value) > parseFloat(response.limite_credito)) {
                        utils.displayWarningDialog("El cliente solo cuenta con un límite de crédito de: " + response.limite_credito, "warning", function(data) {
                            if (data.value) {
                                return false;
                            }
                        })
                    } else if (parseInt($("#plazo_credito_id option:selected").data('cantidad_mes')) > parseInt(response.cantidad_mes)) {
                        utils.displayWarningDialog("El cliente solo cuenta con un plazo de credito de: " + response.plazo_credito, "warning", function(data) {
                            if (data.value) {
                                return false;
                            }
                        })
                    } else {
                        realizarVenta();
                    }
                }
            });*/
        } else if (tipo_forma_pago_id.value == 1) {
            realizarVenta();
        } else {
            $.isLoading("hide");
            toastr.error('Favor de elegir si es compra de credito ó contado');
        }
    });

    function realizarVenta() {
        $.isLoading("hide");
        utils.displayWarningDialog("¿Desea comenzar el proceso de venta ?", "info", function(data) {
            if (data.value) {
                $.isLoading({
                    text: "Realizando el proceso ...."
                });
                let id_venta = $('#venta_id').val();
                ajax.post(`api/cuentas-por-cobrar/registrar-servicio`, {
                    folio_id: $('#folio_id').val(),
                    venta_total: $('#venta_total').val(),
                    tipo_forma_pago_id: $('#tipo_forma_pago_id').val(),
                    concepto: $('#concepto').val(),
                    tipo_pago_id: $('#tipo_pago_id').val() && $('#tipo_pago_id').val() >= 1 ? $('#tipo_pago_id').val() : 22,
                    cliente_id: $('#cliente_id').val(),
                    plazo_credito_id: $("#tipo_forma_pago_id option:selected").val() == 2 ? $('#plazo_credito_id').val() : 14,
                    enganche: $('#enganche').val(),
                    tasa_interes: $('#tasa_interes').val(),
                    usuario_gestor_id: $('#usuario_gestor_id').val(),
                    tipo_proceso: $('#tipo_proceso_id').val(),
                    estatus_cuenta_id: 5,
                }, function(response, headers) {
                    if (headers.status == 201 || headers.status == 200) {
                        $.isLoading("hide");
                        procesarAsientos();

                    } else {
                        $.isLoading("hide");
                        return ajax.showValidations(headers);
                    }
                }, false, false)
            }
        })
    }

    function procesarAsientos(_this) {
        var cuenta_por_cobrar = $("#cxc_id").val();
        $.isLoading({
            text: "Procesando asientos contabilidad ...."
        });

        if ($("#tipo_proceso_id").val() == '1' && $('#tipo_forma_pago_id').val() == '2') {
            ajax.post(`api/asientos/poliza-venta-mostrador-credito`, {
                cuenta_por_cobrar_id: cuenta_por_cobrar
            }, function(response, headers) {
                if (headers.status == 201 || headers.status == 200) {
                    $.isLoading("hide");
                    utils.displayWarningDialog('Proceso realizado correctamente..', 'success', function(result) {
                        $.isLoading({
                            text: "Reedireccionando al módulo de pago ...."
                        });
                        toastr.info('Espere un momento por favor');
                        window.location.href = PATH + 'caja/entradas/detalle_pago/' + cuenta_por_cobrar;
                    });
                } else {
                    $.isLoading("hide");
                }
            })

        } else if ($("#tipo_proceso_id").val() == '8' && $('#tipo_forma_pago_id').val() == '2') {

            ajax.post(`api/asientos/aplicar-asientos`, {
                cuenta_por_cobrar_id: cuenta_por_cobrar
            }, function(response, headers) {
                if (headers.status == 201 || headers.status == 200) {
                    $.isLoading("hide");
                    utils.displayWarningDialog('Proceso realizado correctamente..', 'success', function(result) {
                        $.isLoading({
                            text: "Reedireccionando al módulo de pago ...."
                        });
                        toastr.info('Espere un momento por favor');
                        window.location.href = PATH + 'caja/entradas/detalle_pago/' + cuenta_por_cobrar;
                    });
                } else {
                    $.isLoading("hide");
                }
            })
        } else {

            $.isLoading("hide");
            utils.displayWarningDialog('Proceso realizado correctamente..', 'success', function(result) {
                $.isLoading({
                    text: "Reedireccionando al pago ...."
                });
                toastr.info('Espere un momento por favor');
                window.location.href = PATH + 'caja/entradas/detalle_pago/' + cuenta_por_cobrar;
            });
        }
    }
</script>
@endsection