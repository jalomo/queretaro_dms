@layout('tema_luna/layout')
@section('contenido')
<link href="{{ base_url('css/custom/tabs_custom.css') }}" rel="stylesheet">
<div class="container-fluid pb-4">
	<h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
		<li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
	</ol>
	<div class="row">
		<div class="col-md-4">
			<div class="card p-3" style="background-color:#fff; border-radius:9px;">
				@include('entradas/pagos/partials/datos_cuenta')
			</div>
		</div>
		<div class="col-md-8">
			<div class="card p-3" style="background-color:#fff; border-radius:9px;">
				<div class="c_breadcrumb">
					<ul class="nav nav-pills nav-tabs nav-fill" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#paso5">Asientos</a>
						</li>
						<?php if (in_array($data_cuentas->estatus_cuenta_id, [5, 3])) { ?>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#paso2">Anticipos</a>
							</li>
						<?php } ?>
						<?php if (in_array($data_cuentas->estatus_cuenta_id, [5, 3]) && in_array($data_cuentas->tipo_proceso_id, [8])) { ?>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#paso3">Descuentos</a>
							</li>
						<?php } ?>
						<?php if (in_array($data_cuentas->estatus_cuenta_id, [5, 3])) { ?>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#paso4">Pagar</a>
							</li>
						<?php } ?>
					</ul>
				</div>
				<div class="tab-content p-2">
					<div class="tab-pane active" id="paso5">
						@include('entradas/pagos/partials/asientos')
					</div>
					<div class="tab-pane" id="paso2">
						<?php if (in_array($data_cuentas->estatus_cuenta_id, [5, 3])) { ?>
							@include('entradas/pagos/partials/anticipos')
						<?php } ?>
					</div>
					<?php if (in_array($data_cuentas->estatus_cuenta_id, [5, 3]) && in_array($data_cuentas->tipo_proceso_id, [8, 9])) { ?>
						<div class="tab-pane" id="paso3">
							@include('entradas/pagos/partials/descuentos')
						</div>
					<?php } ?>
					<?php if (in_array($data_cuentas->estatus_cuenta_id, [5, 3])) { ?>
						<div class="tab-pane" id="paso4">
							@include('entradas/pagos/partials/pagar')
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<hr class="mt-5">
	<div class="row mt-3">
		<div class="text-left col-md-2">
			<a href="{{ site_url('caja/entradas/pagos') }}" class="btn btn-dark "><i class="fas fa-arrow-left"></i> Regresar a módulo cajas</a>
		</div>
	</div>
	<input type="hidden" name="fecha_pago" id="fecha_pago" value="{{ isset($unico_pago->fecha_pago) ? $unico_pago->fecha_pago : date('Y-m-d') }}" class="form-control">
	<input type="hidden" id="abono_id" value="{{ isset($unico_pago->id) ? $unico_pago->id : '' }}" />
	<input type="hidden" id="tipo_proceso_id" value="{{ isset($data_cuentas->tipo_proceso_id) ? $data_cuentas->tipo_proceso_id : '' }}" />
	<input type="hidden" id="cliente_id_" value="{{ isset($data_cuentas->cliente_id) ? $data_cuentas->cliente_id : '' }}" />
	<input type="hidden" id="folio_id" value="{{ isset($data_cuentas->folio_id) ? $data_cuentas->folio_id : '' }}" />
	<input type="hidden" id="estatus_cuenta_id" value="{{ isset($data_cuentas->estatus_cuenta_id) ? $data_cuentas->estatus_cuenta_id : '' }}" />
	<input type="hidden" id="tipo_forma_pago_id" value="{{ isset($data_cuentas->tipo_forma_pago_id) ? $data_cuentas->tipo_forma_pago_id : '' }}" />
	<input type="hidden" id="numero_orden" value="{{ isset($data_cuentas->numero_orden) ? $data_cuentas->numero_orden : '' }}" />
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	let cuenta_por_cobrar_id = "<?php echo isset($data_cuentas->id) ? $data_cuentas->id : ''; ?>";
	let orden_entrada_id = "<?php echo isset($data_cuentas->id) ? $data_cuentas->id : ''; ?>";
	let dataForm = [];
</script>
<script src="{{ base_url('js/caja/entradas/pagos/partials/anticipos.js') }}"></script>
<script src="{{ base_url('js/caja/entradas/pagos/partials/descuentos.js') }}"></script>
<script src="{{ base_url('js/caja/entradas/pagos/partials/paga_cuenta.js') }}"></script>
<script src="{{ base_url('js/caja/entradas/pagos/partials/asientos.js') }}"></script>
@endsection