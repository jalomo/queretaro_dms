@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    <div class="row mt-4">
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Folio:</label>
                <input type="text" name="folio" id="folio" class="form-control" />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Número de orden:</label>
                <input type="text" name="numero_orden" id="numero_orden" class="form-control" />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Fecha inicio:</label>
                <input type="date" name="fecha_inicio" id="fecha_inicio" class="form-control" />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Fecha fin:</label>
                <input type="date" name="fecha_fin" id="fecha_fin" class="form-control" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Estatus de cuenta:</label>
                <select class="form-control" id="estatus_cuenta_id" name="estatus_cuenta_id" style="width: 100%;">
                    <option value="">Selecionar ...</option>
                    @if(!empty($estatus_cuentas))
                    @foreach ($estatus_cuentas as $estatus)
                    <option value="{{ $estatus->id}}"> {{ $estatus->nombre }}</option>
                    @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Nombre cliente:</label>
                <input type="text" name="numero_cliente" id="numero_cliente" class="form-control" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Apellidos:</label>
                <input type="text" name="apellidos" id="apellidos" class="form-control" />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="">RFC:</label>
                <input type="text" name="rfc" id="rfc" class="form-control" />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Tipo de proceso:</label>
                <select id="tipo_proceso_id" name="tipo_proceso_id" class="form-control">
                    <option value="">Seleccione una opción</option>
                    <option value="1">Venta mostrador</option>
                    <option value="8">Servicios</option>
                </select>
            </div>
        </div>
        <div class="col-md-2 mt-4">
            <button type="button" id="btn-limpiar" onclick="limpiarfiltro()" class="btn btn-primary mt-1">
                <i class="fa fa-eraser" aria-hidden="true"></i> Limpiar
            </button>
            <button type="button" id="btn-buscar" onclick="filtrar()" class="btn btn-primary mt-1">
                <i class="fa fa-search" aria-hidden="true"></i> Filtrar
            </button>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-12 text-right">
            <button type="button" onclick="openModal()" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar cuenta</button>
        </div>
        <div class="col-md-12 mt-3">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="tbl_cxc" width="100%" cellspacing="0">
                </table>
            </div>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered col-md-5 mt-4" align="right">

                <!-- <tr>
                    <th class="text-right">Saldo neto:</th>
                    <td id="total_saldo_neto_tabla" class="money_format">-</td>
                </tr> -->
                <!-- <tr>
                    <th class="text-right">Total monto a pagar:</th>
                    <td id="total_pagar_tabla" class="money_format">-</td>
                </tr> -->
                <!-- <tr>
                    <th class="text-right">Monto pagado:</th>
                    <td id="total_abonado_tabla" class="money_format">-</td>
                </tr> -->
            </table>
        </div>
    </div>
</div>

@endsection

@section('modal')
<div class="modal fade" id="modal-cuenta" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Agregar cuenta</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Cliente:</label>
                            <select class="form-control select2Modal" id="cliente_modal_id" name="cliente_modal_id" style="width:100%">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_clientes))
                                @foreach ($cat_clientes as $cliente)
                                <option value="{{ $cliente->id}}"> {{$cliente->numero_cliente }} - {{ $cliente->nombre }} | {{ $cliente->aplica_credito && $cliente->aplica_credito == true ? 'Con credito' : 'Sin credito' }}</option>
                                @endforeach
                                @endif
                            </select>
                            <div id="cliente_modal_id_error" class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label>Concepto:</label>
                            <textarea class="form-control" id="concepto" name="concepto" style="min-height:100px"></textarea>
                            <div id="concepto_error" class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label>Total:</label>
                            <input type="text" class="form-control" id="venta_total" name="venta_total" />
                            <div id="venta_total_error" class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label>Fecha:</label>
                            <input type="date" class="form-control" id="fecha" name="fecha" />
                            <div id="fecha_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button id="btn-agregar_cuenta" type="button" class="btn btn-primary"><i class="fas fa-shopping-cart"></i> Agregar</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ base_url('js/caja/entradas/pagos/listado_cuentas_new.js') }}"></script>
@endsection