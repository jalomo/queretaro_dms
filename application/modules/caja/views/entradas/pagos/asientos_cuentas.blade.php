@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
	<h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
		<li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
	</ol>
	<h4>Asientos contabilidad</h4>
	<div class="row mt-4">
		<div class="col-md-4">
			<div class="form-group">
				<label>Tipo de cuenta:</label>
				<select class="form-control select2" id="cuenta_id" name="cuenta_id" style="width:100%">
					<option value="">Selecionar ...</option>
					@if(!empty($catalogo_cuentas))
					@foreach ($catalogo_cuentas as $cuenta)
					<option value="{{ $cuenta->id}}"> {{ $cuenta->no_cuenta}} - {{$cuenta->nombre_cuenta }}</option>
					@endforeach
					@endif
				</select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label>Tipo asiento:</label>
				<select class="form-control select2" id="tipo_asiento_id" name="tipo_asiento_id" style="width: 100%;">
					<option value="">Selecionar ...</option>
					@if(!empty($cat_tipo_asiento))
					@foreach ($cat_tipo_asiento as $tipo_asiento)
					<option value="{{$tipo_asiento->id}}">{{$tipo_asiento->descripcion}}</option>
					@endforeach
					@endif
				</select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label>Total a pagar:</label>
				<input type="text" name="total_pago" id="total_pago" value="<?php echo isset($data_cuentas->total) ? $data_cuentas->total : ''; ?>" class="form-control">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label>Fecha pago:</label>
				<input type="date" name="fecha_pago" id="fecha_pago" class="form-control">
			</div>
		</div>
		<div class="col-md-12 mt-4 mb-4">
			<div class="text-right">
				<button id="btn-agregar" type="button" class="btn btn-primary col-md-2"><i class="fas fa-cash-register"></i> Agregar cuenta</button>
			</div>
		</div>
	</div>
	<hr />
	<div class="col-md-12 mt-3">
		<div class="table-responsive">
			<table class="table table-striped table-bordered" id="tbl_detalle_cuentas" width="100%" cellspacing="0"></table>
		</div>
	</div>
	<input type="hidden" name="importe" id="importe" value="<?php echo isset($data_cuentas->total) ? $data_cuentas->total : ''; ?>" class="form-control">

</div>
@endsection

@section('scripts')
<script type="text/javascript">
	let cuenta_por_cobrar_id = "<?php echo isset($data_cuentas->id) ? $data_cuentas->id : ''; ?>";
	crearTabla();
	getBusqueda();
	$("#btn-agregar").on('click', function() {
		$.isLoading({
			text: "Realizando petición ...."
		});
		let dataForm = {
			cuenta_id: document.getElementById("cuenta_id").value,
			cuenta_por_cobrar_id: cuenta_por_cobrar_id,
			total_pago: document.getElementById("total_pago").value,
			tipo_asiento_id: document.getElementById("tipo_asiento_id").value,
			fecha_pago: document.getElementById("fecha_pago").value,
		}

		let total_pago = $("#total_pago").val();
		let importe = $("#importe").val();
		if (parseFloat(total_pago) > parseFloat(importe)) {
			toastr.error("El total de pago no puede ser mayor al importe de la cuenta " + importe)
			return false;
		}
		/*ajax.post(`api/detalle-asientos-cuentas`, dataForm, function(response, headers) {
			if (headers.status == 200 || headers.status == 201) {
				$.isLoading("hide");
				utils.displayWarningDialog('Cuenta registrada correctamente', "success", function(data) {
					getBusqueda();
				})
			} else {
				$.isLoading("hide");
				return ajax.showValidations(headers);
			}

		})*/
	});

	function crearTabla() {
		$('table#tbl_detalle_cuentas').dataTable(this.configuracionTabla());
	}

	function configuracionTabla() {
		return {
			language: {
				url: PATH_LANGUAGE
			},
			order: [
				[0, 'asc']
			],
			columns: [
				{
					title: "Número abono",
					data: 'abono_id',
				},
				{
					title: "Tipo asiento",
					data: 'tipo_asiento',
				},
				{
					title: "Cuenta",
					data: 'nombre_cuenta',
				},
				{
					title: "Total pago",
					data: 'total_pago',
				},
				{
					title: "Fecha pago",
					data: 'fecha_pago',
				}
			],
		}
	}

	function getBusqueda() {
		ajax.get("api/detalle-asientos-cuentas/buscar-cuenta-por-cobrar/" + cuenta_por_cobrar_id, false, function(response, header) {
			var listado = $('table#tbl_detalle_cuentas').DataTable();
			listado.clear().draw();
			if (response && response.data.length > 0) {
				response.data.forEach(listado.row.add);
				listado.draw();
			}
		});
	}

	$(".select2").select2();
</script>
@endsection