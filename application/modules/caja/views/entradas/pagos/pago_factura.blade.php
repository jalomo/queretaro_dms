@layout('tema_luna/layout')
@section('contenido')
<link href="{{ base_url('css/custom/tabs_custom.css') }}" rel="stylesheet">
<div class="container-fluid pb-4">
	<h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
		<li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
	</ol>
	<div class="row">
		<div class="col-md-4">
			<div class="card p-3" style="background-color:#fff; border-radius:9px;">
				@include('entradas/pagos/partials/datos_cuenta_factura')
			</div>
		</div>
		<div class="col-md-8">
			<div class="card p-3" style="background-color:#fff; border-radius:9px;">
				<form id="form-procesar-factura">
					<div class="row mt-4">
						<div class="col-md-4">
							<div class="form-group">
								<label>Forma de pago</label>
								<select class="form-control" id="tipo_pago_id" name="tipo_pago_id" style="width: 100%;">
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Tipo de CDFI</label>
								<select class="form-control" id="cfdi_id" name="cfdi_id" style="width: 100%;">
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Importe:</label>
								<input type="text" name="importe" id="importe" value="" class="form-control">
							</div>
						</div>
					</div>
					<div class="text-right mt-2">
						<button class="btn btn-primary" type="button" onClick="guardarPagoFactura()"><i class="fas fa-save"></i> Guardar</button>
					</div>
				</form>
				<div class="table-responsive mt-4">
					<table class="table table-striped table-bordered" id="tabla_pago_factura" width="100%" cellspacing="0">
					</table>
				</div>
				<div class="text-end mt-5">
					<a href="{{ site_url('caja/entradas/pagos') }}" class="btn btn-dark "><i class="fas fa-arrow-left"></i> Regresar a módulo cajas</a>
				</div>
			</div>

		</div>
		<hr class="mt-5">
	</div>
	@endsection
	@section('scripts')
	<script type="text/javascript">
		let cuenta_por_cobrar_id = "<?php echo isset($data_cuentas->id) ? $data_cuentas->id : ''; ?>";
		let dataForm = [];
	</script>
	<script src="{{ base_url('js/caja/entradas/pago_factura.js') }}"></script>
	@endsection