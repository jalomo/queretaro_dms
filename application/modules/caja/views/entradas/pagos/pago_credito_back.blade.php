@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
	<h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
		<li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
	</ol>
	<div class="row">
		<div class="col-md-12">
			<table width="100%" class="table table-bordered" cellpadding="5">
				<tr>
					<th colspan="2">Datos del cliente</th>
				</tr>
				<tr>
					<td width="120px">Nombre: </td>
					<td><?php echo isset($cliente->nombre) ? $cliente->nombre . ' ' . $cliente->apellido_paterno . ' ' . $cliente->apellido_materno : ''; ?>
					</td>
				</tr>
				<tr>
					<td width="120px">Núm. cliente: </td>
					<td><?php echo isset($cliente->numero_cliente) ? $cliente->numero_cliente : ''; ?></td>
				</tr>
				<tr>
					<td width="120px">Domicilio: </td>
					<td><?php echo isset($cliente->direccion) ? $cliente->direccion . ' ' . $cliente->colonia . ' ' . $cliente->municipio . ' ' . $cliente->estado : ''; ?>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<h3>Detalle de la compra </h3>
	<div class="row mt-4">
		<div class="col-md-12">
			<table width="100%" class="table table-bordered" cellpadding="5" style="margin-bottom:0px !important">
				<tr>
					<th colspan="4">Detalle de la compra [ <?php echo isset($data_cuentas->estatus_cuenta) ? $data_cuentas->estatus_cuenta : ''; ?> ]</th>
				<tr>
					<th width="120px">Folio: </th>
					<td><?php echo isset($data_cuentas->folio) ? $data_cuentas->folio : ''; ?></td>
					<th width="120px">Concepto: </th>
					<td><?php echo isset($data_cuentas->concepto) ? $data_cuentas->concepto : ''; ?></td>
				</tr>
				<tr>
					<th width="120px">Saldo neto: </th>
					<?php $importe = $data_cuentas->importe + $data['anticipos']->total; ?>
					<td><?php echo isset($data_cuentas->importe) ? '$' . (number_format($importe, 2)) : ''; ?></td>
					<th width="120px">Tasa de interes %: </th>
					<td><?php echo isset($data_cuentas->tasa_interes) ? $data_cuentas->tasa_interes : ''; ?></td>
				</tr>
				<tr>
					<th>Total: </th>
					<?php
					$total = $data_cuentas->total + $data['anticipos']->total; ?>
					<td class=""><?php echo isset($data_cuentas->total) ? '$' . number_format($total, 2) : ''; ?></td>
					<th width="120px">Plazo: </th>
					<td><?php echo isset($data_cuentas->plazo_credito) ? $data_cuentas->plazo_credito : ''; ?></td>

				</tr>
				<tr>
					<th width="120px">Monto actual: </th>
					<td><?php echo '$' . (number_format($saldo_actual, 2)); ?></td>
					<th width="120px">Monto abonado: </th>
					<td><?php echo isset($total_abonado) ? '$' . (number_format($total_abonado, 2)) : ''; ?></td>
				</tr>
				<tr>
					<th width="120px">Abono mensual: </th>
					<?php if ($data_cuentas->tipo_forma_pago_id == 2) { ?>
						<td><?php echo  '$' . (number_format($abono_mensual, 2)); ?>
						<?php } else { ?>
						<td>0</td>
					<?php } ?>
					</td>
					<th width="120px">Fecha compra: </th>
					<td><?php echo isset($data_cuentas->fecha) ? utils::aFecha($data_cuentas->fecha, true) : ''; ?></td>
				</tr>
				<tr>
					<th>Total anticipos aplicados: </th>
					<td><?php echo isset($data['anticipos']->total) ? $data['anticipos']->total : ''; ?></td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<th colspan="4">ANTICIPOS A FAVOR</th>
				</tr>
			</table>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped table-bordered" id="table_anticipos" width="100%" cellspacing="0">
					</table>
				</div>
			</div>
		</div>
	</div>
	<hr />
	<h4>Aplicar Descuentos</h4>
	<?php if (in_array($data_cuentas->tipo_proceso_id, [8, 9])) { ?>
		<div class="row mt-4">
			<div class="col-md-2">
				<div class="form-group">
					<label>Total refacciones:</label>
					<input type="text" id="total_aplicar_1" readonly="readonly" value="<?php echo isset($descuento_refaccion->total_aplicar) ? number_format($descuento_refaccion->total_aplicar, 2) : number_format($total_refacciones, 2); ?>" class="form-control money_format">
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>% Descuento:</label>
					<input type="text" name="porcentaje_1" id="porcentaje_1" maxlength="2" value="<?php echo isset($descuento_refaccion->porcentaje) ? ($descuento_refaccion->porcentaje) : ''; ?>" class="form-control">
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>Cantidad descontada:</label>
					<input type="text" name="cantidad_descontada_1" readonly="readonly" id="cantidad_descontada_1" value="<?php echo isset($descuento_refaccion->cantidad_descontada) ? ($descuento_refaccion->cantidad_descontada) : ''; ?>" class="form-control">
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>Total con descuento:</label>
					<input type="text" name="total_descuento_1" readonly="readonly" id="total_descuento_1" value="<?php echo isset($descuento_refaccion->total_descuento) ? ($descuento_refaccion->total_descuento) : ''; ?>" class="form-control">
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Comentario:</label><br />
					<textarea rows="3" name="comentario_1" class="form-control" id="comentario_1"><?php echo isset($descuento_refaccion->comentario) ? ($descuento_refaccion->comentario) : ''; ?></textarea>
				</div>
			</div>
			<?php if (!$descuento_refaccion->folio_id  && $data_cuentas->estatus_cuenta_id == 5) { ?>
				<div class="col-md-1 mt-4">
					<button type="button" class="btn btn-primary text-right" id="btn-aplicar-1">Aplicar</button>
				</div>
			<?php } ?>
		</div>
	<?php }
	if (in_array($data_cuentas->tipo_proceso_id, [8, 9])) { ?>

		<div class="row mt-4">
			<div class="col-md-2">
				<div class="form-group">
					<label>Total mano obra:</label>
					<input type="text" id="total_aplicar_2" readonly="readonly" value="<?php echo isset($descuento_mano_obra->total_aplicar) ? number_format($descuento_mano_obra->total_aplicar, 2) : number_format($total_mano_obra, 2); ?>" class="form-control money_format">
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>% Descuento:</label>
					<input type="text" name="porcentaje_2" id="porcentaje_2" maxlength="2" value="<?php echo isset($descuento_mano_obra->porcentaje) ? ($descuento_mano_obra->porcentaje) : ''; ?>" class="form-control">
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>Cantidad descontada:</label>
					<input type="text" name="cantidad_descontada_2" readonly="readonly" id="cantidad_descontada_2" value="<?php echo isset($descuento_mano_obra->cantidad_descontada) ? ($descuento_mano_obra->cantidad_descontada) : ''; ?>" class="form-control">
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>Total con descuento:</label>
					<input type="text" name="total_descuento_2" readonly="readonly" id="total_descuento_2" value="<?php echo isset($descuento_mano_obra->total_descuento) ? ($descuento_mano_obra->total_descuento) : ''; ?>" class="form-control">
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Comentario:</label><br />
					<textarea rows="3" name="comentario_2" class="form-control" id="comentario_2"><?php echo isset($descuento_mano_obra->comentario) ? ($descuento_mano_obra->comentario) : ''; ?></textarea>
				</div>
			</div>
			<?php if (!$descuento_mano_obra->folio_id  && $data_cuentas->estatus_cuenta_id == 5) { ?>
				<div class="col-md-1 mt-4">
					<button type="button" class="btn btn-primary text-right" id="btn-aplicar-2">Aplicar</button>
				</div>
			<?php } ?>
		</div>
	<?php } ?>
	<hr />
	<h3>Listado de abonos</h3>
	<div class="row">
		<div class="col-md-12 mb-1">
			<div class="table-responsive">
				<table class="table table-striped table-bordered" id="tbl_abonos" width="100%" cellspacing="0">
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-12 mt-5 mb-5">
		<div class="text-right">
			<a href="<?php echo site_url('caja/entradas/pagos'); ?>" type="button" class="btn btn-secondary col-md-2"><i class="fa fa-chevron-left"></i>&nbsp; Regresar</a>
		</div>
	</div>
</div>
<input type="hidden" id="tipo_proceso_id" value="<?php echo isset($data_cuentas->tipo_proceso_id) ? $data_cuentas->tipo_proceso_id : ''; ?>" />
<input type="hidden" id="cliente_id" value="<?php echo isset($data_cuentas->cliente_id) ? $data_cuentas->cliente_id : ''; ?>" />
<input type="hidden" id="estatus_cuenta_id" value="{{ isset($data_cuentas->estatus_cuenta_id) ? $data_cuentas->estatus_cuenta_id : ''; }}" />

@endsection

@section('scripts')
<script type="text/javascript">
	let count = 1;
	let cuenta_por_cobrar_id = "<?php echo isset($data_cuentas->id) ? $data_cuentas->id : ''; ?>";
	let cantidad_mes = "<?php echo isset($data_cuentas->cantidad_mes) ? $data_cuentas->cantidad_mes : ''; ?>";
	let estatus_cuentas_por_cobrar = "<?php echo isset($data_cuentas->estatus_cuenta_id) ? $data_cuentas->estatus_cuenta_id : ''; ?>";
	let importe = "<?php echo isset($data_cuentas->importe) ? $data_cuentas->importe : ''; ?>";
	let total_pagar = "<?php echo isset($data_cuentas->total) ? $data_cuentas->total : ''; ?>";
	let saldo_actual = "<?php echo isset($saldo_actual) ? $saldo_actual : ''; ?>";
	let liquidar_saldo = "<?php echo isset($liquidar_saldo) ? $liquidar_saldo : ''; ?>";
	let abonoacomulado = 0;
</script>
<script src="{{ base_url('js/caja/entradas/pagos/pago_credito.js') }}"></script>
@endsection

@section('modal')
<div class="modal fade" id="modal-abonar" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="title_modal">Realizar abono</h5>
			</div>
			<div class="modal-body" style="padding:12px !important">
				<div class="row">
					<div class="col-md-12">
						<label>Abono mensual:</label>
						<input type="text" id="total_pago" class="form-control money_format" value="" />
					</div>
					<div class="col-md-12" style="margin-top:6px">
						<div class="form-group">
							<label>Pago:</label>
							<input type="text" name="pago" id="pago" class="form-control money_format">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Cambio:</label>
							<input type="text" readonly="readonly" name="cambio" id="cambio" class="form-control money_format">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Tipo de pago</label>
							<select class="form-control" id="tipo_pago_id" name="tipo_pago_id" style="width: 100%;">
								<option value="">Selecionar ...</option>
								@if(!empty($cat_tipo_pago))
								@foreach ($cat_tipo_pago as $pago)
								<option value="{{$pago->id}}">[{{$pago->clave}}] {{$pago->nombre}}</option>
								@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Cuenta bancaria</label>
							<select class="form-control" id="banco_id" name="banco_id" style="width: 100%;">
								<option value=""> </option>
								@if(!empty($cat_cuentas_bancarias))
								@foreach ($cat_cuentas_bancarias as $cuenta_bancaria)
								<option value="{{$cuenta_bancaria->id}}">{{ $cuenta_bancaria->no_cuenta }} - {{ $cuenta_bancaria->nombre_cuenta }}</option>
								@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Tipo de CDFI</label>
							<select class="form-control" id="cfdi_id" name="cfdi_id" style="width: 100%;">
								<option value="">Selecionar ...</option>
								@if(!empty($cat_cfdi))
								@foreach ($cat_cfdi as $cfdi)
								<option value="{{$cfdi->id}}">[{{$cfdi->clave}}] {{$cfdi->descripcion}}</option>
								@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Caja</label>
							<select class="form-control" id="caja_id" name="caja_id" style="width: 100%;">
								@if(!empty($catalogo_caja))
								@foreach ($catalogo_caja as $caja)
								<option value="{{$caja->id}}">{{$caja->nombre}}</option>
								@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<?php echo renderInputText("date", "fecha_pago", "Fecha abono", date('Y-m-d')); ?>
					</div>
					<label class="mt-3 mb-2"><b>Liquidas con: <?php echo '$' . (number_format($liquidar_saldo, 2)); ?></b></label>

					<input type="hidden" id="tipo_abono_id" value="2" />
					<input type="hidden" id="minimo_abonar" value="" />
					<input type="hidden" id="abono_id" value="" />
					<input type="hidden" id="monto_moratorio" value="0" />
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				<button id="btn-modal-abonar" type="button" class="btn btn-primary"><i class="fas fa-cash-register"></i>
					Guardar</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-enganche" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="title_modal">Pagar enganche</h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<?php echo renderInputText("number", "total_pago_enganche", "Enganche", isset($data_cuentas->enganche) ? round($data_cuentas->enganche, 2) : ''); ?>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Pago:</label>
							<input type="hidden" name="folio_id" id="folio_id" value="{{ isset($data_cuentas->folio_id) ? $data_cuentas->folio_id : ''}}">
							<input type="number" name="pago" id="pago_enganche" class="form-control">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Cambio:</label>
							<input type="number" name="cambio" id="cambio_enganche" class="form-control">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Tipo de pago</label>
							<select class="form-control" id="tipo_pago_id_enganche" name="tipo_pago_id" style="width: 100%;">
								<option value="">Selecionar ...</option>
								@if(!empty($cat_tipo_pago))
								@foreach ($cat_tipo_pago as $pago)
								<option value="{{$pago->id}}">[{{$pago->clave}}] {{$pago->nombre}}</option>
								@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Cuenta bancaria</label>
							<select class="form-control" id="banco_id_enganche" name="banco_id_enganche" style="width: 100%;">
								<option value=""> </option>
								@if(!empty($cat_cuentas_bancarias))
								@foreach ($cat_cuentas_bancarias as $cuenta_bancaria)
								<option value="{{$cuenta_bancaria->id}}">{{ $cuenta_bancaria->no_cuenta }} - {{ $cuenta_bancaria->nombre_cuenta }}</option>
								@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Tipo de CDFI</label>
							<select class="form-control" id="cfdi_id_enganche" name="cfdi_id_enganche" style="width: 100%;">
								<option value="">Selecionar ...</option>
								@if(!empty($cat_cfdi))
								@foreach ($cat_cfdi as $cfdi)
								<option value="{{$cfdi->id}}">[{{$cfdi->clave}}] {{$cfdi->descripcion}}</option>
								@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Caja</label>
							<select class="form-control" id="caja_id_enganche" name="caja_id_enganche" style="width: 100%;">
								@if(!empty($catalogo_caja))
								@foreach ($catalogo_caja as $caja)
								<option value="{{$caja->id}}">{{$caja->nombre}}</option>
								@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<?php echo renderInputText("date", "fecha_pago_enganche", "Fecha abono", date('Y-m-d')); ?>
					</div>
					<input type="hidden" id="tipo_abono_id_enganche" value="1" />
					<input type="hidden" id="enganche_id" value="" />

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				<button id="btn-modal-enganche" type="button" class="btn btn-primary"><i class="fas fa-cash-register"></i>
					Pagar</button>
			</div>
		</div>
	</div>
</div>
@endsection