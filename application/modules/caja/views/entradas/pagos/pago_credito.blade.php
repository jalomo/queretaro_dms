@layout('tema_luna/layout')
@section('contenido')
<link href="{{ base_url('css/custom/tabs_custom.css') }}" rel="stylesheet">
<div class="container-fluid panel-body pb-4">
	<h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
		<li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
	</ol>
	<div class="row">
		<div class="col-md-4">
			<div class="card p-3" style="background-color:#fff; border-radius:9px;">
				@include('entradas/pagos/partials/data_cuenta_credito')
			</div>
		</div>
		<div class="col-md-8">
			<div class="card p-3" style="background-color:#fff; border-radius:9px;">
				<div class="c_breadcrumb">
					<ul class="nav nav-pills nav-tabs nav-fill" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#paso5">Asientos</a>
						</li>
						<?php if (in_array($data_cuentas->estatus_cuenta_id, [6, 3])) { ?>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#paso2">Anticipos</a>
							</li>
						<?php } ?>
						<?php if (in_array($data_cuentas->estatus_cuenta_id, [6, 3]) && in_array($data_cuentas->tipo_proceso_id, [8, 9])) { ?>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#paso3">Descuentos</a>
							</li>
						<?php } ?>
						<?php if (in_array($data_cuentas->estatus_cuenta_id, [6, 3])) { ?>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#paso4">Pagar</a>
							</li>
						<?php } ?>
					</ul>
				</div>
				<div class="tab-content p-2">
					<div class="tab-pane active" id="paso5">
						@include('entradas/pagos/partials/asientos')
					</div>
					<div class="tab-pane" id="paso2">
						<?php if (in_array($data_cuentas->estatus_cuenta_id, [6, 3])) { ?>
							@include('entradas/pagos/partials/anticipos')
						<?php } ?>
					</div>
					<?php if (in_array($data_cuentas->estatus_cuenta_id, [6, 3]) && in_array($data_cuentas->tipo_proceso_id, [8, 9])) { ?>
						<div class="tab-pane" id="paso3">
							@include('entradas/pagos/partials/descuentos')
						</div>
					<?php } ?>
					<?php if (in_array($data_cuentas->estatus_cuenta_id, [6, 3])) { ?>
						<div class="tab-pane" id="paso4">
							@include('entradas/pagos/partials/pagar_abonos')
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<hr class="mt-5">
	</div>
	<div class="row mt-3">
		<div class="text-left col-md-2">
			<a href="{{ site_url('caja/entradas/pagos') }}" class="btn btn-dark "><i class="fas fa-arrow-left"></i> Regresar a módulo cajas</a>
		</div>
	</div>

	<input type="hidden" id="tipo_proceso_id" value="{{ isset($data_cuentas->tipo_proceso_id) ? $data_cuentas->tipo_proceso_id : ''}}" />
	<input type="hidden" id="cliente_id_" value="{{ isset($data_cuentas->cliente_id) ? $data_cuentas->cliente_id : '' }}" />
	<input type="hidden" id="estatus_cuenta_id" value="{{ isset($data_cuentas->estatus_cuenta_id) ? $data_cuentas->estatus_cuenta_id : ''; }}" />
	@endsection
	@section('modal')

	<div class="modal fade" id="modal-genera-factura" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="title_modal">Generar Factura</h5>
				</div>
				<div class="modal-body">
					<div class="col-md-12">
						<div class="form-group">
							<label>Tipo de pago</label>
							<select class="form-control" id="tipo_pago_id_factura" style="width: 100%;">
								<option value="">Selecionar ...</option>
								@if(!empty($cat_tipo_pago))
								@foreach ($cat_tipo_pago as $pago)
								<option value="{{$pago->id}}">[{{$pago->clave}}] {{$pago->nombre}}</option>
								@endforeach
								@endif
							</select>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label>Tipo de CDFI</label>
							<select class="form-control" id="cfdi_id_factura" style="width: 100%;">
								<option value="">Selecionar ...</option>
								@if(!empty($cat_cfdi))
								@foreach ($cat_cfdi as $cfdi)
								<option value="{{$cfdi->id}}">[{{$cfdi->clave}}] {{$cfdi->descripcion}}</option>
								@endforeach
								@endif
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<button id="btn-modal-factura" type="button" class="btn btn-primary"><i class="fas fa-cash-register"></i>
						Procesar</button>
				</div>
			</div>
		</div>
	</div>
	@endsection

	@section('scripts')
	<script type="text/javascript">
		let count = 1;
		let cuenta_por_cobrar_id = "<?php echo isset($data_cuentas->id) ? $data_cuentas->id : ''; ?>";
		let tipo_forma_pago_id = "<?php echo isset($data_cuentas->tipo_forma_pago_id) ? $data_cuentas->tipo_forma_pago_id : ''; ?>";
		let cantidad_mes = "<?php echo isset($data_cuentas->cantidad_mes) ? $data_cuentas->cantidad_mes : ''; ?>";
		let estatus_cuentas_por_cobrar = "<?php echo isset($data_cuentas->estatus_cuenta_id) ? $data_cuentas->estatus_cuenta_id : ''; ?>";
		let importe = "<?php echo isset($data_cuentas->importe) ? $data_cuentas->importe : ''; ?>";
		let total_pagar = "<?php echo isset($data_cuentas->total) ? $data_cuentas->total : ''; ?>";
		let saldo_actual = "<?php echo isset($saldo_actual) ? $saldo_actual : ''; ?>";
		let liquidar_saldo = "<?php echo isset($liquidar_saldo) ? $liquidar_saldo : ''; ?>";
		let abonoacomulado = 0;
		var d = new Date();
		var strDate = d.getFullYear() + "/" + (d.getMonth() + 1) + "/" + d.getDate();
	</script>
	<script src="{{ base_url('js/caja/entradas/pagos/partials/asientos.js') }}"></script>
	<script src="{{ base_url('js/caja/entradas/pagos/partials/anticipos.js') }}"></script>
	<script src="{{ base_url('js/caja/entradas/pagos/partials/descuentos.js') }}"></script>
	<script src="{{ base_url('js/caja/entradas/pagos/partials/pago_abonos.js') }}"></script>
	@endsection