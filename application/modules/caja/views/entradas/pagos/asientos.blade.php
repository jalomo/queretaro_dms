@layout('tema_luna/layout')
@section('contenido')
<style>
	.sep10 {
		width: 100%;
		height: 10px;
		clear: both;
	}
</style>
<div class="container-fluid panel-body">
	<h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
		<li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
	</ol>
	<div class="row p-2">
		<div class="col-md-12">
			<table width="100%" class="table table-bordered" cellpadding="5">
				<tr>
					<th colspan="4">Datos del cliente</th>
				</tr>
				<tr>
					<th width="10%">Nombre: </th>
					<td width="40%"><?php echo isset($cliente->nombre) ? $cliente->nombre . ' ' . $cliente->apellido_paterno . ' ' . $cliente->apellido_materno : ''; ?></td>
					<th width="10%">Núm. cliente: </th>
					<td width="40%"><?php echo isset($cliente->numero_cliente) ? $cliente->numero_cliente : ''; ?></td>
				</tr>
				<tr>
					<th>Domicilio: </th>
					<td><?php echo isset($cliente->direccion) ? $cliente->direccion . ' ' . $cliente->colonia . ' ' . $cliente->municipio . ' ' . $cliente->estado : ''; ?></td>
					<th>Folio: </th>
					<td><?php echo isset($data_cuentas->folio) ? $data_cuentas->folio : ''; ?></td>
				</tr>
				<tr>
					<th>Concepto: </th>
					<td><?php echo isset($data_cuentas->concepto) ? $data_cuentas->concepto : ''; ?></td>
					<th>Total: </th>
					<td class=""><?php echo isset($data_cuentas->total) ? '$' . number_format($data_cuentas->total, 2) : ''; ?></td>
				</tr>
				<tr>
					<th width="120px">Fecha compra: </th>
					<td colspan="3"><?php echo isset($data_cuentas->fecha) ? utils::afecha($data_cuentas->fecha, true) : ''; ?></td>
				</tr>
			</table>
		</div>
	</div>
	<?php
	//PROCESO VARIOS- SOLO SE PUEDE AGREGAR ASIENTOS
	if ($data_cuentas->tipo_proceso_id == 7) { ?>
		<div class="row mt-4 p-2">
			<div class="col-md-3">
				<div class="form-group">
					<label><b>Tipo de cuenta:</b></label>
					<select class="form-control select2" id="cuenta_id" name="cuenta_id" style="width:100%">
						<option value="">Selecionar ...</option>
						@if(!empty($catalogo_cuentas))
						@foreach ($catalogo_cuentas as $cuenta)
						<option value="{{ $cuenta->id}}"> {{ $cuenta->no_cuenta}} - {{$cuenta->nombre_cuenta }}</option>
						@endforeach
						@endif
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label><b>Tipo asiento:</b></label>
					<select class="form-control select2" id="tipo_asiento_id" name="tipo_asiento_id" style="width: 100%;">
						<option value="">Selecionar ...</option>
						@if(!empty($cat_tipo_asiento))
						@foreach ($cat_tipo_asiento as $tipo_asiento)
						<option value="{{$tipo_asiento->id}}">{{$tipo_asiento->descripcion}}</option>
						@endforeach
						@endif
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label><b>Total a pagar:</b></label>
					<input type="text" name="total_pago" id="total_pago" value="<?php echo isset($data_cuentas->total) ? number_format($data_cuentas->total, 2) : ''; ?>" class="form-control money_format">
				</div>
			</div>
			<div class="col-md-2 mt-4">
				<div class="">
					<button id="btn-agregar" type="button" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Agregar asiento</button>
				</div>
			</div>
		</div>

	<?php } ?>
	<hr />
	<div class="row mt-4 p-2">
		<div class="col-md-3">
			<div class="form-group">
				<label for=""><b>Tipo poliza:</b></label>
				<select class="form-control" id="tipo_poliza" name="tipo_poliza" style="width: 100%;">
					<option value="">Selecciona opción</option>
					<?php if (isset($data_cuentas->clave_poliza) && $data_cuentas->clave_poliza) { ?>
						<option value="{{ $data_cuentas->clave_poliza }}">{{ $data_cuentas->clave_poliza }}</option>
					<?php } ?>
					<option value="CG">CG</option>
				</select>
			</div>
		</div>
		<div class="col-md-2 mt-1">
			<button type="button" id="btn-buscar" onclick="getBusqueda()" class="btn btn-primary mt-4">
				<i class="fa fa-search" aria-hidden="true"></i> Filtrar
			</button>
		</div>
	</div>
	<div classs="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-striped table-bordered" id="tbl_asientos" width="100%" cellspacing="0"></table>
			</div>
		</div>
		<div class="col-md-12">
			<table class="table table-bordered col-md-4 mt-4" align="right">
				<tr>
					<th class="text-right">Cargos:</th>
					<td id="total_cargos" class="money_format">-</td>
				</tr>
				<tr>
					<th class="text-right">Abonos:</th>
					<td id="total_abonos" class="money_format">-</td>
				</tr>
			</table>
		</div>
		<div class="sep10"></div>
	</div>
	<div class="row mt-3">
		<div class="text-left col-md-2">
			<a href="{{ site_url('caja/entradas/pagos') }}" class="btn btn-primary "><i class="fas fa-arrow-left"></i> Regresar</a>
		</div>
	</div>
	<input type="hidden" id="abono_id" value="<?php echo isset($unico_pago->id) ? $unico_pago->id : ''; ?>" />
	<input type="hidden" id="tipo_proceso_id" value="<?php echo isset($data_cuentas->tipo_proceso_id) ? $data_cuentas->tipo_proceso_id : ''; ?>" />
	<input type="hidden" id="cliente_id" value="<?php echo isset($cliente->id) ? $cliente->id : ''; ?>" />
	<input type="hidden" id="folio_id" value="{{ isset($data_cuentas->folio_id) ? $data_cuentas->folio_id : ''; }}" />
	<input type="hidden" id="clave_poliza" value="{{ isset($data_cuentas->clave_poliza) ? $data_cuentas->clave_poliza : ''; }}" />
	<input type="hidden" name="referencia" id="referencia" value="<?php echo isset($data_cuentas->concepto) ? $data_cuentas->concepto : ''; ?>" class="form-control">
	<div class="sep10"></div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	let cuenta_por_cobrar_id = "<?php echo isset($data_cuentas->id) ? $data_cuentas->id : ''; ?>";
	crearTabla();
	getBusqueda();

	$("#btn-agregar").on('click', function() {
		if ($('small.form-text.text-danger').length > 0) {
			$('small.form-text.text-danger').each(function() {
				$(this).empty();
			});
		}
		$.isLoading({
			text: "Realizando petición ...."
		});
		let dataForm = {
			concepto: document.getElementById("referencia").value,
			total_pago: document.getElementById("total_pago").value.replace(",", ""),
			cuenta_id: document.getElementById("cuenta_id").value,
			tipo_asiento_id: document.getElementById("tipo_asiento_id").value,
			folio_id: document.getElementById("folio_id").value,
			clave_poliza: 'CG',
			cliente_id: document.getElementById("cliente_id").value,
			estatus: 'POR_APLICAR',
			departamento: 3
		}

		let total_pago = $("#total_pago").val();
		let importe = $("#importe").val();
		if (parseFloat(total_pago) > parseFloat(importe)) {
			toastr.error("El total de pago no puede ser mayor al importe de la cuenta " + importe)
			return false;
		}
		ajax.post(`api/asientos/contabilidad`, dataForm, function(response, headers) {
			if (headers.status == 200 || headers.status == 201) {
				$.isLoading("hide");
				utils.displayWarningDialog('Asiento registrado correctamente', "success", function(data) {
					$("#cuenta_id").val('');
					$("#total_pago").val('');
					$("#tipo_asiento_id").val('');
					$("#fecha_pago").val('');
					$("#total_pago").trigger('change');
					$("#cuenta_id").trigger('change');
					$("#tipo_asiento_id").trigger('change');
					getBusqueda();
				})
			} else {
				$.isLoading("hide");
				return ajax.showValidations(headers);
			}
		})
	});

	function crearTabla() {
		$('table#tbl_asientos').dataTable(this.configuracionTabla());
	}

	function configuracionTabla() {
		return {
			language: {
				url: PATH_LANGUAGE
			},
			bInfo: false,
			paging: false,
			order: [
				[0, 'asc']
			],
			columns: [{
					title: "Poliza",
					render: function(data, type, row) {
						return '<small>' + row.poliza + '</small>';
					},
				},
				{
					title: "Departamento",
					render: function(data, type, row) {
						return '<small>' + row.departamento_descripcion + '</small>';
					},
				},
				{
					title: "Cargo",
					render: function(data, type, row) {
						return '<small>' + parseFloat(row.cargo).toFixed(2) + '</small>';
					},
				},
				{
					title: "Abono",
					render: function(data, type, row) {
						return '<small>' + parseFloat(row.abono).toFixed(2) + '</small>';
					},
				},
				{
					title: "Cuenta",
					render: function(data, type, row) {
						return '<small><b>' + row.cuenta + '</b><br/>' + row.cuenta_descripcion + '</small>';
					},
				},
				{
					title: "Estatus",
					render: function(data, type, row) {
						let badge = '';
						switch (row.estatus_id) {
							case 'POR_APLICAR':
								badge = "badge-warning";
								break;
							case 'APLICADO':
								badge = "badge-success";
								break;
							case 'ANULADO':
								badge = "badge-danger";
								break;

							default:
								break;
						}
						return '<div class="badge ' + badge + '"><small>' + row.estatus_id + '</small></div>';
					},
				},
				{
					title: "Fecha registro",
					render: function(data, type, row) {
						return '<small>' + row.fecha_creacion + '</small>';
					},
				},
			],
		}
	}

	function getBusqueda() {
		let total_cargos = 0;
		let total_abonos = 0;
		let total_asientos = 0;
		var params = $.param({
			'folio': $("#folio_id").val(),
			'nomenclatura': $("#tipo_poliza option:selected").val()
		});
		$.ajax({
			dataType: "json",
			type: 'GET',
			data: params,
			url: API_CONTABILIDAD + '/asientos/api/detalle',
			success: function(response, _, _) {
				var listado = $('table#tbl_asientos').DataTable();
				listado.clear().draw();
				if (response && response.data.length > 0) {
					response.data.forEach(listado.row.add);
					listado.draw();
				}
				$('table#tbl_asientos tbody tr').each(function() {
					total_cargos += parseFloat($(this).find("td").eq(2).text());
					total_abonos += parseFloat($(this).find("td").eq(3).text());
					$(this).find("td").eq(2).addClass('money_format');
					$(this).find("td").eq(3).addClass('money_format');
				});
				$("#total_cargos").html('$<span class="money_format">' + parseFloat(total_cargos).toFixed(2) + '</span>');
				$("#total_abonos").html('$<span class="money_format">' + parseFloat(total_abonos).toFixed(2) + '</span>');
			}
		});
	}

	$(".select2").select2();
	$('.money_format').mask("#,##0.00", {
		reverse: true
	});
</script>
@endsection