<h4>Realizar pago [ <?php echo isset($data_cuentas->estatus_cuenta) ? $data_cuentas->estatus_cuenta : ''; ?> ]</h4>
<div class="row mt-4">
    <div class="col-md-4">
        <div class="form-group">
            <label>Total a pagar:</label>
            <input type="text" name="importe" id="importe" readonly="readonly" value="<?php echo isset($data_cuentas->total) ? number_format($data_cuentas->total, 2) : ''; ?>" class="form-control money_format">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Forma de pago</label>
            <select class="form-control" id="tipo_pago" name="tipo_pago" onchange="validarTipoPago(this)" style="width: 100%;">
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Tipo de CDFI</label>
            <select class="form-control" id="cfdi_id" name="cfdi_id" style="width: 100%;">
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Caja</label>
            <select class="form-control" id="caja_id" name="caja_id" style="width: 100%;">
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Cuenta bancaria</label>
            <select class="form-control" id="banco_id" name="banco_id" style="width: 100%;">
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Régimen fiscal</label>
            <select class="form-control" id="regimen_fiscal_" style="width: 100%;">
                <option value=""></option>
                <option value="F">Persona física</option>
                <option value="M">Persona moral</option>
                <option value="E">Persona física con actividad empresarial</option>
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Importe:</label>
            <input type="text" name="pago" id="pago" value="<?php echo isset($unico_pago->pago) ? $unico_pago->pago : ''; ?>" class="form-control money_format">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Cambio:</label>
            <input type="text" name="cambio" id="cambio" value="<?php echo isset($unico_pago->cambio) ? $unico_pago->cambio : ''; ?>" class="form-control money_format">
        </div>
    </div>
    <div class="col-md-4 banco_pagar_id" style="display:none">
        <div class="form-group">
            <label>Banco cliente</label>
            <select class="form-control" id="banco_pagar_id" name="banco_pagar_id" style="width: 100%;">
            </select>
        </div>
    </div>
    <div class="col-md-4 referencia_tarjeta" style="display:none">
        <div class="form-group">
            <label>Referencia tarjeta: <small>4 últimos digitos</small></label>
            <input type="text" maxlength="4" name="referencia_tarjeta" id="referencia_tarjeta" value="<?php echo isset($unico_pago->referencia_tarjeta) ? $unico_pago->referencia_tarjeta : ''; ?>" class="form-control">
        </div>
    </div>
    <div class="col-md-4 referencia_cheque" style="display:none">
        <div class="form-group">
            <label>Referencia cheque:</label>
            <input type="text" name="referencia_cheque" id="referencia_cheque" value="<?php echo isset($unico_pago->referencia_cheque) ? $unico_pago->referencia_cheque : ''; ?>" class="form-control">
        </div>
    </div>
    <?php if ($data_cuentas->estatus_cuenta_id == 5) { ?>
        <div class="col-md-12 mt-4 mb-4">
            <div class="text-right">
                <button id="btn-pagar" type="button" class="btn btn-primary col-md-2"><i class="fas fa-cash-register"></i> Pagar</button>
            </div>
        </div>
    <?php } ?>
</div>