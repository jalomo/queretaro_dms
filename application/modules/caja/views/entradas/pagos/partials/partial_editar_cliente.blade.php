<div class="row">
    <div class="col-md-6">
        <label for="">Tipo de cliente:</label>
        <select class="form-control" name="tipo_registro" id="tipo_registro" onchange="app.changeRegimen()" style="width: 100%;">
            @if(!empty($cat_clave))
            @foreach ($cat_clave as $clave)
            @if($data->tipo_registro == $clave->id)
            <option value="{{$clave->id}}" selected="selected">{{ $clave->clave }} - {{ $clave->nombre }}</option>
            @else
            <option value="{{$clave->id}}">{{ $clave->clave }} - {{ $clave->nombre }}</option>
            @endif
            @endforeach
            @endif
        </select>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Nombre:</label>
            <input type="text" class="form-control" value="" id="nombre" name="nombre" placeholder="">
            <div id="nombre_error" class="invalid-feedback"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Apellido Paterno:</label>
            <input type="text" class="form-control" value="" id="apellido_paterno" name="apellido_paterno" placeholder="">
            <div id="apellido_paterno_error" class="invalid-feedback"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Apellido Materno:</label>
            <input type="text" class="form-control" value="" id="apellido_materno" name="apellido_materno" placeholder="">
            <div id="apellido_materno_error" class="invalid-feedback"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">*Tipo persona:</label>
            <select class="form-control" id="regimen_fiscal" name="regimen_fiscal" onchange="app.changeRegimen()" style="width: 100%;">
                <option value="">Selecionar ...</option>
                <option value="F">Fisica </option>
                <option value="M">Moral </option>
                <option value="A">Ambas</option>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <label for="">Régimen fiscal:</label>
        <select class="form-control" name="regimen_fiscal_id" id="regimen_fiscal_id" style="width: 100%;">
        </select>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">*Razón Social:</label>
            <input type="text" class="form-control" readonly="readonly" value="" id="nombre_empresa" name="nombre_empresa" placeholder="">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">*RFC:</label>
            <input type="text" class="form-control" minlength="11" value="" id="rfc" name="rfc" placeholder="">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">*Correo electrónico:</label>
            <input type="text" class="form-control" value="" id="correo_electronico" name="correo_electronico" placeholder="">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">*Teléfono:</label>
            <input type="text" class="form-control" value="" id="telefono" name="telefono" maxlenght="10" placeholder="">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">*Dirección:</label>
            <input type="text" class="form-control" value="" id="direccion" name="direccion" placeholder="">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="">Numero exterior:</label>
            <input type="text" class="form-control" value="" id="numero_ext" name="numero_ext" placeholder="">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="">Numero interior:</label>
            <input type="text" class="form-control" value="" id="numero_int" name="numero_int" placeholder="">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Colonia:</label>
            <input type="text" class="form-control" value="" id="colonia" name="colonia" placeholder="">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Codigo postal:</label>
            <input type="text" class="form-control" value="" id="codigo_postal" name="codigo_postal" placeholder="">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Municipio:</label>
            <input type="text" class="form-control" value="" id="municipio" name="municipio" placeholder="">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Estado:</label>
            <input type="text" class="form-control" value="" id="estado" name="estado" placeholder="">
        </div>
    </div>
    <input type="hidden" id="cliente_id" val="">
</div>
<hr />
<div class="row" id="datos_credito" stlye="display:none">
    <div class="col-md-3">
        <div class="form-group">
            <label for="">Aplica crédito:</label>
            <input type="text" disabled="disabled" class="form-control" value="" id="aplica_credito" placeholder="">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="">Plazo crédito:</label>
            <input type="text" disabled="disabled" class="form-control" value="" id="plazo_credito" placeholder="">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="">Limite crédito:</label>
            <input type="text" disabled="disabled" class="form-control" value="" id="limite_credito" placeholder="">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="">Crédito actual:</label>
            <input type="text" disabled="disabled" class="form-control" value="" id="credito_actual" placeholder="">
        </div>
    </div>
</div>