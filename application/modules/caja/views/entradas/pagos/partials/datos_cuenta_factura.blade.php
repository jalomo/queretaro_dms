<style>
    .bold {
        font-weight: bold;
        font-size: 16px !important;
        color: #323232;
    }

    hr {
        margin: 0px !important;
        border: 1px dashed #ccc;
    }

    .text-muted {
        font-size: 14px;
    }
</style>
<h4 class=""><span class="text-info">Datos venta</span> </h4>
<hr />
<div class="row mt-2">
    <div class="col-12">
        <div class="bold">FOLIO: <br /></div>
        <p class="text-danger"><span><?php echo isset($data_cuentas->folio) ? $data_cuentas->folio : ''; ?></span></p>
    </div>
    <div class="col-12">
        <div class="bold">Concepto: <br /></div>
        <p class="text-muted"><span><?php echo isset($data_cuentas->concepto) ? $data_cuentas->concepto : ''; ?></span></p>
    </div>
    <div class="col-6">
        <div class="bold">Total:<br /></div>
        <p class="text-muted"><?php echo isset($data_cuentas->total) ? '$' . number_format($data_cuentas->total, 2) : ''; ?></p>
    </div>
    <div class="col-6">
        <div class="bold">Fecha compra:<br /></div>
        <p class="text-muted"><?php echo isset($data_cuentas->fecha) ? $data_cuentas->fecha : ''; ?></p>
    </div>
</div>
<h4 class=""><span class="text-info">Datos cliente</span> </h4>
<hr />
<div class="row mt-2">
    <div class="col-6">
        <div class="bold">Nombre: <br /></div>
        <p class="text-muted">
            <span>
                <?php
                $numero_cliente = isset($cliente->numero_cliente) ? $cliente->numero_cliente : '';
                $nombre_cliente =  isset($cliente->nombre) ? $cliente->nombre . ' ' . $cliente->apellido_paterno . ' ' . $cliente->apellido_materno : '';
                echo $numero_cliente . ' - ' . $nombre_cliente;
                ?>
            </span>
        </p>
    </div>
    <div class="col-6">
        <div class="bold">RFC:<br /></div>
        <p class="text-muted"><span><?php echo isset($cliente->rfc) ? $cliente->rfc : ''; ?></span></p>
    </div>
    <div class="col-6">
        <div class="bold">Domicilio:<br /></div>
        <p class="text-muted">
            <span>
                <?php echo isset($cliente->direccion) ? $cliente->direccion . ' ' .  $cliente->numero_ext : ''; ?>
            </span>
        </p>
    </div>
    <div class="col-6">
        <div class="bold">Colonia:<br /></div>
        <p class="text-muted">
            <span>
                <?php echo isset($cliente->colonia) ? $cliente->colonia : ''; ?>
            </span>
        </p>
    </div>
    <div class="col-6">
        <div class="bold">Municipio:<br /></div>
        <p class="text-muted">
            <span>
                <?php echo isset($cliente->municipio) ? $cliente->municipio : ''; ?>
            </span>
        </p>
    </div>
    <div class="col-6">
        <div class="bold">Estado:<br /></div>
        <p class="text-muted">
            <span>
                <?php echo isset($cliente->estado) ? $cliente->estado : ''; ?>
            </span>
        </p>
    </div>
    <div class="col-6">
        <div class="bold">Codigo postal:<br /></div>
        <p class="text-muted">
            <span>
                <?php echo isset($cliente->codigo_postal) ? $cliente->codigo_postal : ''; ?>
            </span>
        </p>
    </div>
</div>

