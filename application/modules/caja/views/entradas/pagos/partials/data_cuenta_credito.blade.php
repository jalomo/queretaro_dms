<style>
	.bold {
		font-weight: bold;
		font-size: 16px !important;
		color: #323232;
	}

	hr {
		margin: 0px !important;
		border: 1px dashed #ccc;
	}

	.text-muted {
		font-size: 14px;
	}
</style>
<h4 class=""><span class="text-info">Datos venta</span> </h4>
<hr />
<div class="row mt-2">
	<div class="col-12">
		<div class="bold">Folio: <span class="text-danger"><?php echo isset($data_cuentas->folio) ? $data_cuentas->folio : ''; ?></span></div><br />
	</div>
	<div class="col-12">
		<div class="bold">Concepto: <br /></div>
		<p class="text-muted"><span><?php echo isset($data_cuentas->concepto) ? $data_cuentas->concepto : ''; ?></span></p>
	</div>
	<div class="col-6">
		<div class="bold">Saldo neto:<br /></div>
		<?php $importe = $data_cuentas->importe + $data['anticipos']->total; ?>
		<p class="text-muted"><?php echo isset($data_cuentas->importe) ? '$' . (number_format($importe, 2)) : ''; ?></p>
	</div>
	<div class="col-6">
		<div class="bold">Total:<br /></div>
		<?php $total = $data_cuentas->total + $data['anticipos']->total; ?>
		<p class="text-muted"><?php echo isset($data_cuentas->total) ? '$' . number_format($total, 2) : ''; ?></p>
	</div>
	<div class="col-6">
		<div class="bold">Plazo: <br /></div>
		<p class="text-muted"><span><?php echo isset($data_cuentas->plazo_credito) ? $data_cuentas->plazo_credito : ''; ?></span></p>
	</div>
	<div class="col-6">
		<div class="bold">Monto actual: <br /></div>
		<p class="text-muted"><span><?php echo '$' . (number_format($saldo_actual, 2)); ?></span></p>
	</div>
	<div class="col-6">
		<div class="bold">Monto abonado: <br /></div>
		<p class="text-muted"><span><?php echo isset($total_abonado) ? '$' . (number_format($total_abonado, 2)) : ''; ?></span></p>
	</div>
	<div class="col-6">
		<div class="bold">Abono mensual: <br /></div>
		<p class="text-muted"><span><?php echo $data_cuentas->tipo_forma_pago_id == 2 ? '$' . (number_format($abono_mensual, 2)) : 0.00 ?></span></p>
	</div>
	<div class="col-6">
		<div class="bold">Fecha compra:<br /></div>
		<p class="text-muted"><?php echo isset($data_cuentas->fecha) ? $data_cuentas->fecha : ''; ?></p>
	</div>
	<div class="col-6">
		<div class="bold">Total anticipos aplicados: <br /></div>
		<p class="text-muted">$<span class=""><?php echo isset($data['anticipos']->total) ? $data['anticipos']->total : ''; ?></span></p>
	</div>
	<div class="col-6">
		<div class="bold">Estatus:<br /></div>
		<p class="text-muted proximo_pago_"><?php echo isset($data_cuentas->estatus_cuenta) ? $data_cuentas->estatus_cuenta : ''; ?></span></p>
	</div>
</div>
<div class="btn-group">
	<?php if (in_array($data_cuentas->estatus_cuenta_id, [2,7])) { ?>
		<button id="btn-imprimir_poliza" onclick="imprimir_estado_cuenta(this)" data-cuenta_por_pagar_id="<?php echo $data_cuentas->id; ?>" type="button" class="btn btn-dark btn-lg"><i class="fas fa-file-pdf"></i> Imprimir estado cuenta</button>
	<?php } ?>
	<?php
	if ($data_cuentas->tipo_forma_pago_id == 2 && in_array($data_cuentas->estatus_cuenta_id, [5])) { ?>
		<button id="btn-imprime_factura" onclick="generar_factura(this)" data-id="<?php echo $data_cuentas->id; ?>" type="button" class="btn btn-dark btn-lg"><i class="fas fa-cogs"></i> Generar factura</button>
	<?php } ?>
	<?php if (in_array($data_cuentas->estatus_cuenta_id, [6])) { ?>
		<button id="btn-imprime_factura" onclick="imprimir_comprobante_fiscal(this)" data-folio="<?php echo $data_cuentas->folio_id; ?>" type="button" class="btn btn-dark btn-lg"><i class="fas fa-file-excel"></i> Imprimir factura</button>
	<?php } ?>
</div>
<h4 class=""><span class="text-info">Datos cliente</span> </h4>
<hr />
<div class="row mt-2">
	<div class="col-6">
		<div class="bold">Nombre: <br /></div>
		<p class="text-muted">
			<span>
				<?php
				$numero_cliente = isset($cliente->numero_cliente) ? $cliente->numero_cliente : '';
				$nombre_cliente =  isset($cliente->nombre) ? $cliente->nombre . ' ' . $cliente->apellido_paterno . ' ' . $cliente->apellido_materno : '';
				echo $numero_cliente . ' - ' . $nombre_cliente;
				?>
			</span>
		</p>
	</div>
	<div class="col-6">
		<div class="bold">RFC:<br /></div>
		<p class="text-muted"><span><?php echo isset($cliente->rfc) ? $cliente->rfc : ''; ?></span></p>
	</div>
	<div class="col-6">
		<div class="bold">Domicilio:<br /></div>
		<p class="text-muted">
			<span>
				<?php echo isset($cliente->direccion) ? $cliente->direccion . ' ' .  $cliente->numero_ext : ''; ?>
			</span>
		</p>
	</div>
	<div class="col-6">
		<div class="bold">Colonia:<br /></div>
		<p class="text-muted">
			<span>
				<?php echo isset($cliente->colonia) ? $cliente->colonia : ''; ?>
			</span>
		</p>
	</div>
	<div class="col-6">
		<div class="bold">Municipio:<br /></div>
		<p class="text-muted">
			<span>
				<?php echo isset($cliente->municipio) ? $cliente->municipio : ''; ?>
			</span>
		</p>
	</div>
	<div class="col-6">
		<div class="bold">Estado:<br /></div>
		<p class="text-muted">
			<span>
				<?php echo isset($cliente->estado) ? $cliente->estado : ''; ?>
			</span>
		</p>
	</div>
	<div class="col-6">
		<div class="bold">Codigo postal:<br /></div>
		<p class="text-muted">
			<span>
				<?php echo isset($cliente->codigo_postal) ? $cliente->codigo_postal : ''; ?>
			</span>
		</p>
	</div>
</div>
<button type="button" data-cliente_id="{{ $data_cuentas->cliente_id }}" onclick="openDatosCliente(this)" class="btn btn-primary"><i class="fa fa-pencil-alt"></i> Editar cliente</button>


<div class="modal fade" id="modal-editar-cliente" data-backdrop="static" data-keyboard="false" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="modaleditarcliente" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="title_modal"> Actualizar cliente</h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<form id="form-cliente" class="col-md-12">
						@include('entradas/pagos/partials/partial_editar_cliente')
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" id="btn_cerrar_permiso" data-dismiss="modal">Cancelar</button>
				<button id="btn-modal-permiso-porcentaje" onClick="updateCliente()" type="button" class="btn btn-primary">
					<i class="fas fa-save"></i> Guardar
				</button>

			</div>
		</div>
	</div>
</div>