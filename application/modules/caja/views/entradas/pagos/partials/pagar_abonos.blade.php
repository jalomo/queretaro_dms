<div class="row">
    <div class="col-md-12 mb-1">
        <div class="table-responsive">
            <table class="table table-striped table-bordered" id="tbl_abonos" width="100%" cellspacing="0">
            </table>
        </div>
    </div>
</div>
@section('modal')
<div class="modal fade" id="modal-abonar" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal">Realizar abono</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <label>Abono mensual:</label>
                        <input type="text" id="total_pago" class="form-control money_format" value="" />
                    </div>
                    <div class="col-md-6" style="margin-top:6px">
                        <div class="form-group">
                            <label>Pago:</label>
                            <input type="text" name="pago" id="pago" class="form-control money_format">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Cambio:</label>
                            <input type="text" readonly="readonly" name="cambio" id="cambio" class="form-control money_format">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tipo de pago</label>
                            <select class="form-control" id="tipo_pago_id" onchange="validarTipoPago(this)" name="tipo_pago_id" style="width: 100%;">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Cuenta bancaria</label>
                            <select class="form-control" id="banco_id" name="banco_id" style="width: 100%;">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tipo de CDFI</label>
                            <select class="form-control" id="cfdi_id" name="cfdi_id" style="width: 100%;">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Caja</label>
                            <select class="form-control" id="caja_id" name="caja_id" style="width: 100%;">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 banco_pagar_id" style="display:none">
                        <div class="form-group">
                            <label>Banco cliente</label>
                            <select class="form-control" id="banco_pagar_id" name="banco_pagar_id" style="width: 100%;">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 referencia_tarjeta" style="display:none">
                        <div class="form-group">
                            <label>Referencia tarjeta: <small>4 últimos digitos</small></label>
                            <input type="text" maxlength="4" name="referencia_tarjeta" id="referencia_tarjeta" value="<?php echo isset($unico_pago->referencia_tarjeta) ? $unico_pago->referencia_tarjeta : ''; ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6 referencia_cheque" style="display:none">
                        <div class="form-group">
                            <label>Referencia cheque:</label>
                            <input type="text" name="referencia_cheque" id="referencia_cheque" value="<?php echo isset($unico_pago->referencia_cheque) ? $unico_pago->referencia_cheque : ''; ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <?php echo renderInputText("date", "fecha_pago", "Fecha abono", date('Y-m-d')); ?>
                    </div>
                    <label class="mt-3 mb-2"><b>Liquidas con: <?php echo '$' . (number_format($liquidar_saldo, 2)); ?></b></label>
                    <input type="hidden" id="tipo_abono_id" value="2" />
                    <input type="hidden" id="minimo_abonar" value="" />
                    <input type="hidden" id="abono_id" value="" />
                    <input type="hidden" id="monto_moratorio" value="0" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button id="btn-modal-abonar" type="button" class="btn btn-primary"><i class="fas fa-cash-register"></i>
                    Guardar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-enganche" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal">Pagar enganche</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <?php echo renderInputText("number", "total_pago_enganche", "Enganche", isset($data_cuentas->enganche) ? round($data_cuentas->enganche, 2) : ''); ?>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Pago:</label>
                            <input type="hidden" name="folio_id" id="folio_id" value="{{ isset($data_cuentas->folio_id) ? $data_cuentas->folio_id : ''}}">
                            <input type="number" name="pago" id="pago_enganche" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Cambio:</label>
                            <input type="number" name="cambio" id="cambio_enganche" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tipo de pago</label>
                            <select class="form-control" id="tipo_pago_id_enganche" onchange="validarTipoPago(this)" name="tipo_pago_id" style="width: 100%;">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Cuenta bancaria</label>
                            <select class="form-control" id="banco_id_enganche" name="banco_id" style="width: 100%;">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tipo de CDFI</label>
                            <select class="form-control" id="cfdi_id_enganche" name="cfdi_id" style="width: 100%;">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Caja</label>
                            <select class="form-control" id="caja_id_enganche" name="caja_id" style="width: 100%;">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 banco_pagar_id" style="display:none">
                        <div class="form-group">
                            <label>Banco cliente</label>
                            <select class="form-control" id="banco_pagar_id" name="banco_pagar_id" style="width: 100%;">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 referencia_tarjeta" style="display:none">
                        <div class="form-group">
                            <label>Referencia tarjeta: <small>4 últimos digitos</small></label>
                            <input type="text" maxlength="4" name="referencia_tarjeta" id="referencia_tarjeta" value="<?php echo isset($unico_pago->referencia_tarjeta) ? $unico_pago->referencia_tarjeta : ''; ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6 referencia_cheque" style="display:none">
                        <div class="form-group">
                            <label>Referencia cheque:</label>
                            <input type="text" name="referencia_cheque" id="referencia_cheque" value="<?php echo isset($unico_pago->referencia_cheque) ? $unico_pago->referencia_cheque : ''; ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <?php echo renderInputText("date", "fecha_pago_enganche", "Fecha abono", date('Y-m-d')); ?>
                    </div>
                    <input type="hidden" id="tipo_abono_id_enganche" value="1" />
                    <input type="hidden" id="enganche_id" value="" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button id="btn-modal-enganche" type="button" class="btn btn-primary"><i class="fas fa-cash-register"></i> Pagar</button>
            </div>
        </div>
    </div>
</div>
@endsection