    <div class="row mt-4">
        <div class="col-md-2">
            <div class="form-group">
                <label>Subtotal general:</label>
                <?php $total_general = 0;
                if (isset($asientos['total_refacciones'])) {
                    $total_general = $asientos['total_refacciones'] + $asientos['total_mano_obra'];
                } ?>
                <input type="text" id="total_aplicar_3" readonly="readonly" value="<?php echo isset($descuentos['descuento_general']->total_aplicar) ? number_format($descuentos['descuento_general']->total_aplicar, 2) : number_format($total_general, 3); ?>" class="form-control">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>% Descuento:</label>
                <input type="text" name="porcentaje_3" id="porcentaje_3" maxlength="5" value="<?php echo isset($descuentos['descuento_general']->porcentaje) ? ($descuentos['descuento_general']->porcentaje) : ''; ?>" class="form-control">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Cantidad descontada:</label>
                <input type="text" name="cantidad_descontada_3" readonly="readonly" id="cantidad_descontada_3" value="<?php echo isset($descuentos['descuento_general']->cantidad_descontada) ? ($descuentos['descuento_general']->cantidad_descontada) : ''; ?>" class="form-control">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Total con descuento:</label>
                <input type="text" name="total_descuento_3" readonly="readonly" id="total_descuento_3" value="<?php echo isset($descuentos['descuento_general']->total_descuento) ? ($descuentos['descuento_general']->total_descuento) : ''; ?>" class="form-control">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Comentario:</label><br />
                <input type="text" name="comentario_3" class="form-control" id="comentario_3" value="<?php echo isset($descuentos['descuento_general']->comentario) ? ($descuentos['descuento_general']->comentario) : ''; ?>" class="form-control">
            </div>
        </div>
        <?php
        if (!isset($descuentos['descuento_general']->id)) { ?>
            <div class="col-md-1 mt-4">
                <button type="button" class="btn btn-primary text-right" id="btn-aplicar-3">Aplicar</button>
            </div>
        <?php }  ?>
        <hr class="mt-3">
        <div class="col-md-4">
            <div class="form-group">
                <label>Total con IVA:</label>
                <input type="text" readonly="readonly" id="total_iva" value="<?php echo isset($descuentos) ? number_format(($descuentos['descuento_general']->total_descuento * 1.16), 2) : number_format(($total_general  * 1.16), 3); ?>" class="form-control">
            </div>
        </div>
    </div>

    <!-- <div class="row mt-4">
        <div class="col-md-2">
            <div class="form-group">
                <label>Total refacciones:</label>
                <input type="text" id="total_aplicar_1" readonly="readonly" value="<?php echo isset($descuento_refaccion->total_aplicar) ? number_format($descuento_refaccion->total_aplicar, 2) : number_format($asientos['total_refacciones'], 2); ?>" class="form-control money_format">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>% Descuento:</label>
                <input type="text" name="porcentaje_1" id="porcentaje_1" maxlength="5" value="<?php echo isset($descuento_refaccion->porcentaje) ? ($descuento_refaccion->porcentaje) : ''; ?>" class="form-control">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Cantidad descontada:</label>
                <input type="text" name="cantidad_descontada_1" readonly="readonly" id="cantidad_descontada_1" value="<?php echo isset($descuento_refaccion->cantidad_descontada) ? ($descuento_refaccion->cantidad_descontada) : ''; ?>" class="form-control">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Total con descuento:</label>
                <input type="text" name="total_descuento_1" readonly="readonly" id="total_descuento_1" value="<?php echo isset($descuento_refaccion->total_descuento) ? ($descuento_refaccion->total_descuento) : ''; ?>" class="form-control">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Comentario:</label><br />
                <input type="text" name="comentario_1" class="form-control" id="comentario_1" value="<?php echo isset($descuento_refaccion->comentario) ? ($descuento_refaccion->comentario) : ''; ?>" class="form-control">
            </div>
        </div>
        <?php if (!isset($descuento_refaccion->folio_id) && $data_cuentas->estatus_cuenta_id == 5) { ?>
            <div class="col-md-1 mt-4">
                <button type="button" class="btn btn-primary text-right" id="btn-aplicar-1">Aplicar</button>
            </div>
        <?php } ?>
    </div>
    <hr class="mt-3">
    <div class="row mt-4">
        <div class="col-md-2">
            <div class="form-group">
                <label>Total mano obra:</label>
                <input type="text" id="total_aplicar_2" readonly="readonly" value="<?php echo isset($descuento_mano_obra->total_aplicar) ? number_format($descuento_mano_obra->total_aplicar, 2) : number_format($asientos['total_mano_obra'], 2); ?>" class="form-control money_format">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>% Descuento:</label>
                <input type="text" name="porcentaje_2" id="porcentaje_2" maxlength="5" value="<?php echo isset($descuento_mano_obra->porcentaje) ? ($descuento_mano_obra->porcentaje) : ''; ?>" class="form-control">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Cantidad descontada:</label>
                <input type="text" name="cantidad_descontada_2" readonly="readonly" id="cantidad_descontada_2" value="<?php echo isset($descuento_mano_obra->cantidad_descontada) ? ($descuento_mano_obra->cantidad_descontada) : ''; ?>" class="form-control">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Total con descuento:</label>
                <input type="text" name="total_descuento_2" readonly="readonly" id="total_descuento_2" value="<?php echo isset($descuento_mano_obra->total_descuento) ? ($descuento_mano_obra->total_descuento) : ''; ?>" class="form-control">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Comentario:</label><br />
                <input type="text" name="comentario_2" class="form-control" id="comentario_2" value="<?php echo isset($descuento_mano_obra->comentario) ? ($descuento_mano_obra->comentario) : ''; ?>" />
            </div>
        </div>
        <?php if (!isset($descuento_mano_obra->folio_id) && $data_cuentas->estatus_cuenta_id == 5) { ?>
            <div class="col-md-1 mt-4">
                <button type="button" class="btn btn-primary text-right" id="btn-aplicar-2">Aplicar</button>
            </div>
        <?php } ?>
    </div> -->