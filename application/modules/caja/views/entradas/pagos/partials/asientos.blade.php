<?php
//PROCESO VARIOS- SOLO SE PUEDE AGREGAR ASIENTOS
if ($data_cuentas->tipo_proceso_id == 7) { ?>
	<div class="row mt-4 p-2">
		<div class="col-md-3">
			<div class="form-group">
				<label><b>Tipo de cuenta:</b></label>
				<select class="form-control select2" id="cuenta_id" name="cuenta_id" style="width:100%">
					<option value="">Selecionar ...</option>
					@if(!empty($catalogo_cuentas))
					@foreach ($catalogo_cuentas as $cuenta)
					<option value="{{ $cuenta->id}}"> {{ $cuenta->no_cuenta}} - {{$cuenta->nombre_cuenta }}</option>
					@endforeach
					@endif
				</select>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label><b>Tipo asiento:</b></label>
				<select class="form-control select2" id="tipo_asiento_id" name="tipo_asiento_id" style="width: 100%;">
					<option value="">Selecionar ...</option>
					@if(!empty($cat_tipo_asiento))
					@foreach ($cat_tipo_asiento as $tipo_asiento)
					<option value="{{$tipo_asiento->id}}">{{$tipo_asiento->descripcion}}</option>
					@endforeach
					@endif
				</select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label><b>Total a pagar:</b></label>
				<input type="text" name="total_pago" id="total_pago" value="<?php echo isset($data_cuentas->total) ? number_format($data_cuentas->total, 2) : ''; ?>" class="form-control money_format">
			</div>
		</div>
		<div class="col-md-2 mt-4">
			<div class="">
				<button id="btn-agregar" type="button" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Agregar asiento</button>
			</div>
		</div>
	</div>
<?php } ?>
<hr />
<div class="row mt-4 p-2">
	<div class="col-md-3">
		<div class="form-group">
			<label for=""><b>Tipo poliza:</b></label>
			<select class="form-control" id="tipo_poliza" name="tipo_poliza" style="width: 100%;">
				<option value="">Selecciona opción</option>
				<?php if (isset($data_cuentas->clave_poliza) && $data_cuentas->clave_poliza) { ?>
					<option value="{{ $data_cuentas->clave_poliza }}">{{ $data_cuentas->clave_poliza }}</option>
				<?php } ?>
				<option value="CG">CG</option>
			</select>
		</div>
	</div>
	<div class="col-md-2 mt-1">
		<button type="button" id="btn-buscar" onclick="getBusquedaAsientos()" class="btn btn-primary mt-4">
			<i class="fa fa-search" aria-hidden="true"></i> Filtrar
		</button>
	</div>
</div>
<div classs="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered" id="tbl_asientos" width="100%" cellspacing="0"></table>
		</div>
	</div>
	<div class="col-md-12" style="display:block">
		<table class="table table-bordered col-md-4 mt-4" align="right">
			<tr>
				<th class="text-right">Cargos:</th>
				<td id="total_cargos" class="money_format">-</td>
			</tr>
			<tr>
				<th class="text-right">Abonos:</th>
				<td id="total_abonos" class="money_format">-</td>
			</tr>
		</table>
	</div>
	<div class="sep10"></div>
</div>
<input type="hidden" id="abono_id" value="<?php echo isset($unico_pago->id) ? $unico_pago->id : ''; ?>" />
<input type="hidden" id="tipo_proceso_id" value="<?php echo isset($data_cuentas->tipo_proceso_id) ? $data_cuentas->tipo_proceso_id : ''; ?>" />
<input type="hidden" id="cliente_id" value="<?php echo isset($cliente->id) ? $cliente->id : ''; ?>" />
<input type="hidden" id="folio_id" value="{{ isset($data_cuentas->folio_id) ? $data_cuentas->folio_id : ''; }}" />
<input type="hidden" id="clave_poliza" value="{{ isset($data_cuentas->clave_poliza) ? $data_cuentas->clave_poliza : ''; }}" />
<input type="hidden" name="referencia" id="referencia" value="<?php echo isset($data_cuentas->concepto) ? $data_cuentas->concepto : ''; ?>" class="form-control">
<div class="sep10"></div>