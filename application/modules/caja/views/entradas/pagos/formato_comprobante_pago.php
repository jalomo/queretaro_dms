<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Comprobante pago</title>
	<style>
		.contenedor {
			width: 100%;
		}

		td {
			font-size: 11px !important;
		}

		.col-12 {
			width: 100%;
			padding: 3px;
		}

		.col-6 {
			float: left;
			width: 49%;
			padding: 3px;
		}

		.col-5 {
			float: left;
			width: 40%;
			padding: 3px;
		}

		table {
			border-collapse: collapse;
			width: 100%;
		}

		.text-right {
			text-align: right;
		}

		table#detalle_cuenta,
		td,
		td {
			border-bottom: 0.1px solid #233a74;
			margin-bottom: 12px
		}

	</style>
</head>

<body>
	<div class="contenedor">
		<div class="col-12 text-right">
			<?php echo obtenerFechaEnLetra(date('yy-m-d')); ?>
		</div>
		<div class="col-12">
			<h2>FORD</h2>
			<h2><?php echo NOMBRE_SUCURSAL;?></h2>
			<h4>Comprobante de pago</h4>
		</div>
	</div>
	<div class="col-6">
		<h4>Datos del emisor</h4>
		<table style="width:95%;" class="table" cellpadding="5">
			<tr>
				<td>RFC:</td>
				<td><?php echo RFC_RECEPTOR; ?></td>
			</tr>
			<tr>
				<td>Razón Social:</td>
				<td><?php echo NOMBRE_SUCURSAL;?> , S.A. de C.V</td>
			</tr>
			<tr>
				<td>Regimen Fiscal:</td>
				<td>601 - General de Ley Personas Morales</td>
			</tr>
		</table>
		<div class="clear"></div>
	</div>
	<div class="col-6">
		<h4>Receptor</h4>
		<table style="width:100%;" class="table" cellpadding="5">
			<tr>
				<td>Cliente:</td>
				<td><?php echo isset($cliente->nombre) ? $cliente->nombre : '';?></td>
			</tr>
			<tr>
				<td>RFC:</td>
				<td><?php echo isset($cliente->rfc) ? $cliente->rfc : '';?></td>
			</tr>
			<tr>
				<td>Razón Social:</td>
				<td><?php echo isset($cliente->nombre_empresa) ? $cliente->nombre_empresa : '';?></td>
			</tr>
		</table>
		<div class="clear"></div>
	</div>
	<div class="contenedor">
		<div class="col-12 mt-4">
			<h4>Detalle Pago</h4>
			<table id="detalle_cuenta" style="width:100%;" class="table" cellpadding="5">
				<tr>
					<td  width="120px">Abono: </td>
					<td><?php echo isset($abono->id) ? utils::folio($abono->id) : ''; ?></td>
				
				<tr>
					<td  width="120px">Concepto: </td>
						<td><?php echo isset($cuenta->concepto) ? $cuenta->concepto : ''; ?></td>
					</tr>
				</tr>
				<tr>
					<td  width="120px">Monto del pago: </td>
					<td><?php echo isset($abono->total_pago) ? '$'.(number_format($abono->total_pago,2)) : ''; ?></td>	
				</tr>
				<tr>
					<td  width="120px">Fecha pago: </td>
					<td><?php echo isset($abono->fecha_pago) ? utils::aFecha($abono->fecha_pago,true) : ''; ?></td>
				</tr>
				<tr>
					<td  width="120px">Forma de pago: </td>
					<td><?php echo isset($abono->tipo_pago_id) ? $abono->tipo_pago : ''; ?></td>
				</tr>
				<tr>
					<td  width="120px">CFDI: </td>
					<td><?php echo isset($abono->descripcion_cfdi) ? $abono->clave_cfdi. ' - '. $abono->descripcion_cfdi : ''; ?></td>
				</tr>
			</table>
			<div class="clear"></div>
		</div>
	</div>
</body>

</html>
