@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    <hr />
    <div style="display:block">
        <div class="row mt-4">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">Fecha:</label>
                    <input type="date" name="fecha" id="fecha" value="<?php echo date('Y-m-d'); ?>" class="form-control" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">Estatus:</label>
                    <select name="estatus" id="estatus" class="form-control">
                        <option value="">Selecionar ...</option>
                        <option value="POR_APLICAR">Por aplicar</option>
                        <option value="APLICADO">Aplicado</option>
                        <option value="Anulado">Anulado</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12 mt-4">
                <div class="text-right">
                    <button type="button" id="btn-limpiar" onclick="limpiarfiltro()" class="btn btn-primary">
                        <i class="fa fa-eraser" aria-hidden="true"></i> Limpiar
                    </button>
                    <button type="button" id="btn-buscar" onclick="filtrar()" class="btn btn-primary ">
                        <i class="fa fa-search" aria-hidden="true"></i> Filtrar
                    </button>
                </div>
            </div>
        </div>
        <hr />
        <div class="row mb-4">
            <div class="col-md-12 text-right">
                <button type="button" onclick="autorizacion_multiple();" class="btn btn-primary" type="button"><i class="fa fa-check"></i>&nbsp;Autorizar todos</button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped table-bordered" id="tabla_asientos" width="100%" cellspacing="0">
                </table>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered col-md-5 mt-4" align="right">
                    <tr class="caption_table">
                        <th colspan="2">Totales</th>
                    </tr>
                    <tr>
                        <th class="text-right title_table" style="width:40%">Abonos:</th>
                        <td id="total_abonos">-</td>
                    </tr>
                    <tr>
                        <th class="text-right title_table">Cargos:</th>
                        <td id="total_cargos">-</td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</div>

    @endsection
    @section('scripts')
    <script src="{{ base_url('js/caja/corte_caja/registro_corte_caja.js') }}"></script>
    <script src="{{ base_url('js/caja/corte_caja/listado_asientos.js') }}"></script>
    @endsection