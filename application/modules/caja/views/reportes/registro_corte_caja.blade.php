@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    <div style="display:block">
        <form id="form_registro_corte">
            <div class="row mt-4">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Folio:</label>
                        <input type="text" id="folio" value="" class="form-control" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Fecha registro:</label>
                        <input type="date" id="fecha_registro" name="fecha_registro" value="" class="form-control" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Descripción:</label>
                        <input type="text" id="descripcion" name="descripcion" value="" class="form-control" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Tipo pago:</label>
                        <select id="tipo_pago_id" name="tipo_pago_id" value="" class="form-control">
                            <option value=""></option>
                            <?php
                            foreach ($tipo_pago as $val) { ?>
                                <option value="<?php echo $val->id; ?>"><?php echo $val->clave . ' - ' . $val->nombre; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Cantidad reportada:</label>
                        <input type="text" id="total_cantidad_reportada" name="total_cantidad_reportada" value="" class="form-control" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Cantidad en sistema:</label>
                        <input type="text" id="total_cantidad_caja" readonly="readonly" name="total_cantidad_caja" value="" class="form-control" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Faltantes:</label>
                        <input type="text" id="faltantes" readonly="readonly" name="faltantes" value="" class="form-control" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Sobrantes:</label>
                        <input type="text" id="sobrantes" readonly="readonly" name="sobrantes" value="" class="form-control" />
                    </div>
                </div>
                <input type="hidden" name="folio_id" id="folio_id" />
        </form>
        <div class="col-md-12">
            <div class="text-right">
                <button type="button" id="btn-guardar" onclick="saveRegistroCorteCaja()" class="btn btn-primary ">
                    <i class="fa fa-save" aria-hidden="true"></i> Guardar
                </button>
            </div>
        </div>
    </div>
    <hr />
    <div class="row mt-4">
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Fecha:</label>
                <input type="date" name="fecha" id="fecha" value="" class="form-control" />
            </div>

        </div>
        <div class="col-md-2 mt-4">
            <div class="text-left mt-1">
                <button type="button" id="btn-buscar" onclick="filtrar()" class="btn btn-primary ">
                    <i class="fa fa-search" aria-hidden="true"></i> Buscar
                </button>
            </div>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-bordered" id="tabla_registro_corte" width="100%" cellspacing="0">
            </table>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered col-md-5 mt-4" align="right">
                <tr class="caption_table">
                    <th colspan="2">Totales</th>
                </tr>
                <tr>
                    <th class="text-right title_table" style="width:40%">Total cantidad en sistema:</th>
                    <td id="total_cantidad_caja_">-</td>
                </tr>
                <tr>
                    <th class="text-right title_table">Total cantidad reportada:</th>
                    <td id="total_cantidad_reportada_">-</td>
                </tr>
            </table>

        </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="{{ base_url('js/caja/corte_caja/registro_corte_caja_.js') }}"></script>
@endsection