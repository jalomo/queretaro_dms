<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Asientos PDF</title>
	<style>
		.contenedor {
			width: 100%;
		}

		td {
			font-size: 11px !important;
			/* text-align: right !important; */
		}

		.col-12 {
			width: 100%;
			padding: 3px;
		}

		.col-6 {
			float: left;
			width: 49%;
			padding: 3px;
		}

		.col-5 {
			float: left;
			width: 40%;
			padding: 3px;
		}

		table {
			border-collapse: collapse;
			width: 100%;
		}

		table,
		th,
		td {
			border: 1px solid #233a74;
			margin-bottom: 12px
		}

	</style>
</head>

<body>
	<div class="contenedor">
		<div class="col-12">
			<h2>Reporte de Asientos Contabilidad</h2>
		</div>
	</div>
	<div class="col-12">
		<label>
			Periodo comprendido del <?php echo $fecha_inicio; ?> al <?php echo $fecha_fin; ?>
		</label>
	</div>
	<br/>
	<div class="contenedor">
		<div class="col-12 mt-4">
			<table class="table" cellpadding="5">
				<thead>
					<tr>
						<th style="font-size: 11px" scope="col">Concepto</th>
						<th style="font-size: 11px" scope="col">Cuenta</th>
						<th style="font-size: 11px" scope="col">Abono</th>
						<th style="font-size: 11px" scope="col">Cargo</th>
						<th style="font-size: 11px" scope="col">Fecha pago</th>
						<th style="font-size: 11px" scope="col">Estatus</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$total_asientos = 0;
					$total_cargos = 0;
					foreach ($listado as $key => $item) { 
						if ($item->tipo_asiento_id == 1) {
							$total_asientos = $total_asientos + $item->total_pago;
						} 
						if ($item->tipo_asiento_id == 2) {
							$total_cargos = $total_cargos + $item->total_pago;
						} 
					?>
					<tr>
						<td><?php echo  $item->referencia; ?></td>
						<td><?php echo  $item->no_cuenta . '-' . $item->nombre_cuenta; ?></td>
						<td><?php echo  $item->tipo_asiento_id == 1 ? '$'. number_format($item->total_pago) : '' ?></td>
						<td><?php echo  $item->tipo_asiento_id == 2 ? '$'. number_format($item->total_pago) : '' ?></td>
						<td>
							<?php echo utils::afecha($item->fecha_pago,true) ?>
						</td>						
						<td><?php echo  $item->estatus; ?></td>
					</tr>
					<?php  } ?>
				</tbody>
			</table>
		</div>
		<br/><br/>
		<div class="col-12">
			<table style="width:40%;" align="right" class="table" cellpadding="5">
				<tbody>
					<tr>
						<td colspan="2"><b>Resultados totales</b></td>
					</tr>
					<tr>
						<td>Totales Abonos:</td>
						<td><?php echo isset($total_asientos) ? '$'. number_format($total_asientos) : '';?></td>
					</tr>
					<tr>
						<td>Totales Cargos:</td>
						<td><?php echo isset($total_cargos) ? '$'. number_format($total_cargos) : '';?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>

</html>
