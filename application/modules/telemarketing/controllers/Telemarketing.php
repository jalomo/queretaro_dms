<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Telemarketing extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

    public function index()
    {
        $data['titulo'] = "";
        $this->blade->render('telemarketingMain',$data);
    }

    
}
