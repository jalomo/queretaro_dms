<?php defined('BASEPATH') or exit('No direct script access allowed');

class Auto extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->helper('general');
    $this->load->library('curl');
    date_default_timezone_set('America/Mexico_City');
  }

  /* Listar prepedidos de ventas de autos */
  public function listado()
  {
    $data['modulo'] = "Autos";
    $data['submodulo'] = "Venta";
    $data['titulo'] = "Pre pedidos";
    $data['subtitulo'] = "listado";

    $catalogo_remisiones = $this->curl->curlGet('api/unidades/getbyparameters?estatus_id=1');
    $remisiones = procesarResponseApiJsonToArray($catalogo_remisiones);
    $data['cat_remisiones'] = !empty($remisiones) ? $remisiones : [];
    $this->blade->render('index_preventas', $data);
  }

  /* Cargar vista con breadcrumb de pasos para una nueva preventa */
  public function preventa()
  {
    $step = $this->input->get('step');
    if (!$step) {
      // Mandar el step para poder saber el paso al cual irse.
      redirect('autos_ventas/auto/listado');
    }
    $id_unidad = $this->input->get('id_unidad');
    $data = $this->returnCatalogosForVentas();
    $data['id_tipo_auto'] = 1; //seminuevo

    if ($id_unidad != '') {
      $detalle_unidad = $this->curl->curlGet('api/unidades/' . $id_unidad);
      $unidad = procesarResponseApiJsonToArray($detalle_unidad);
      $data['detalle_unidad'] = count($unidad) > 0 ? $unidad[0] : [];
    } else {
      $data['detalle_unidad'] = [];
    }
    $data['step'] = $step;
    $data['titulo'] = "Pre-venta";
    $data['subtitulo'] = "Crear Pre-venta";
    $this->blade->render('preventa', $data);
  }

  public function returnCatalogosForVentas()
  {
    $cfdi = $this->curl->curlGet('api/cfdi');
    $clientes = $this->curl->curlGet('api/clientes');
    $plazo_credito = $this->curl->curlGet('api/catalogo-plazo-credito');
    $tipo_forma_pago = $this->curl->curlGet('api/catalogo-tipo-forma-pago');
    $tipo_pago = $this->curl->curlGet('api/tipo-pago');

    $data['nombre_usuario'] = $this->session->userdata('nombre') . " " . $this->session->userdata('apellido_paterno') . " " . $this->session->userdata('apellido_materno');
    $data['id_asesor'] = $this->session->userdata('id');

    $data['cat_cfdi'] = procesarResponseApiJsonToArray($cfdi);
    $data['catalogo_clientes'] = procesarResponseApiJsonToArray($clientes);
    $data['plazo_credito'] = procesarResponseApiJsonToArray($plazo_credito);;
    $data['tipo_forma_pago'] = procesarResponseApiJsonToArray($tipo_forma_pago);
    $data['tipo_pago'] = procesarResponseApiJsonToArray($tipo_pago);
    return $data;
  }
}
