<form id="form-venta">
	<input type="hidden" class="form-control" value="{{ isset($cliente->id) ? $cliente->id : '' }}" name="cliente_id_" id="cliente_id_">
	<p class="bold"><b>Buscar y seleccionar el cliente al cual asignar la venta</b></p>
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<label>Número cliente</label>
				<input type="text" id="numero_cliente_" value="{{ $cliente->numero_cliente ? $cliente->numero_cliente : '' }}" class="form-control buscar_enter" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<label>RFC</label>
				<input type="text" id="rfc_" class="form-control buscar_enter" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label>Nombre completo</label>
				<input type="text" id="nombre_completo_" class="form-control buscar_enter" />
			</div>
		</div>
		<div class="col-md-2 mt-4">
			<button type="button" onclick="app.filtrarTablaClientes()" class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
		</div>
	</div>
	<div class="row">
		<div class="col-12 text-right">
			Si no existe el cliente dar clic en el botón
			<button class="btn btn-dark btn-flujo" title="Agregar cliente" onclick="app.openModalAltaCliente()" type="button">
				<i class="fa fa-plus-circle" style="font-size:18px"></i>
				Agregar cliente
			</button>
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-bordered table-striped" id="tbl_cliente" width="100%" cellspacing="0"></table>
			</div>
		</div>
	</div>
</form>


<div class="row mb-4 mt-4">
	<div class="col-md-12 text-right">
		<button onclick="app.validaPaso1()" class="btn btn-primary btn-flujo" id="btn-modificar-venta">
			<i class="fas fa-arrow-right"></i>
			Guardar y continuar
		</button>
	</div>
</div>