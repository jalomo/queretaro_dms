
<h3>Datos de vendedor</h3>
<div class="row">
    <div class="col-md-6">
        <?php renderInputText("text", "vendedor", "Vendedor",  isset($data_venta) ? $data_venta->nombre_vendedor . ' ' . $data_venta->apellido_paterno_vendedor : $nombre_usuario, isset($data_venta) ? true : false); ?>
        <input type="hidden" id="id_asesor" name="id_asesor" value="{{ isset($data_venta) ? $data_venta->id_asesor : $id_asesor }}">

    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="fecha_alta">Fecha de alta:</label>
            <input type="date" class="form-control" min="{{ date('Y-m-d') }}" value="{{ isset($data->created_at) ? $data->created_at :  date('Y-m-d') }}" id="orden_fecha" name="orden_fecha" placeholder="">
            <div id="fecha_alta_error" class="invalid-feedback"></div>
        </div>
    </div>
</div>
<h3>Datos del Cliente</h3>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="select">Cliente</label>
            <select name="id_cliente" class="form-control " id="id_cliente">
                <option value=""> Seleccionar</option>
                @foreach ($catalogo_clientes as $cliente)
                @if (isset($data_venta) && $data_venta->cliente_id == $cliente->id)
                <option selected value="{{ $cliente->id}}"> {{ $cliente->numero_cliente .' - '. $cliente->nombre .' '. $cliente->apellido_materno}} </option>
                @else
                <option value="{{ $cliente->id}}"> {{ $cliente->numero_cliente .' - '. $cliente->nombre .' '. $cliente->apellido_materno}} </option>

                @endif
                @endforeach
            </select>
            <div id='id_cliente_error' class='invalid-feedback'></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="select">Nombre completo</label>
            <input class="form-control" type="text" {{ isset($data_venta) ? 'disabled': '' }} id="nombre_cliente" value="{{ isset($data_venta) ? $data_venta->nombre_cliente . ' '. $data_venta->cliente_apellido_paterno : '' }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="select">RFC</label>
            <input class="form-control" type="text" id="rfc_cliente" {{ isset($data_venta) ? 'disabled': '' }} value="{{ isset($data_venta) ? $data_venta->cliente_rfc  : '' }}">
        </div>
    </div>
</div>

<div class="mt-3 form-row justify-content-end">
    <button type="button" class="btn btn-primary btn-flujo" onclick="app_paso2.modificarVenta()"> <i class="fa fa-arrow-right"></i> Guardar y continuar </button>
</div>

@section('scripts')
    <script>
        var base_url = "{{ base_url('/') }}";
        $(document).ready(function () {
            $("#nav_paso1").click();
        });       
        $("#id_cliente").on('change', function(e) {
            let id_cliente = $("#id_cliente").val();
            ajax.get(`api/clientes/${id_cliente}`, {}, (data, headers) => {
                let response = data[0];
                $("#nombre_cliente").val(`${response.nombre} ${response.apellido_materno} ${response.apellido_paterno}`);
                $("#rfc_cliente").val(response.rfc);
            })
        });
    </script>
@endsection