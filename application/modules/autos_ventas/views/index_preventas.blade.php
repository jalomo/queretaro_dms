@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : ''; ?>
            </li>
            <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : ''; ?></li>
            <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : ''; ?></li>
        </ol>

        <div class="row mb-3">
            <div class="col-md-12 text-right">
                <a class="btn btn-primary" href="<?php echo base_url('autos/ventas/unidades'); ?>">Nueva Pre-venta</a>
                <a class="btn btn-primary" href="<?php echo base_url('autos_ventas/auto/preventa?step=1'); ?>">Pre-venta sin Unidad</a>
            </div>
        </div>

        <div class="row mb-3">
            <div class="col-md-4">
                <?php renderInputText('date', 'fecha_inicio', 'Fecha inicio:', null); ?>
            </div>
            <div class="col-md-4">
                <?php renderInputText('date', 'fecha_fin', 'Fecha fin:', null); ?>
            </div>
            <div class="col-md-4 mt-4">
                <button class="btn btn-primary btn-block" id="btn-filtrar"> Filtrar</button>
            </div>
            <div class="col-md-12  mt-4">
                <div class="table-responsive">
                    <table class="table table-bordered text-center" id="tbl_ventas"></table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        var tabla_pre_ventas = $('#tbl_ventas').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            "ajax": {
                url: base_url + "autos/ventas/ajax_preventas",
                type: 'POST',
                data: {
                    fecha_inicio: () => $('#fecha_inicio').val(),
                    fecha_fin: () => $('#fecha_fin').val()
                }
            },
            columns: [{
                    title: "#",
                    data: function(data) {
                        return data.id
                    }
                },
                {
                    title: "Fecha solicitud",
                    data: function(data) {
                        return utils.dateToLetras(data.created_at);
                    }
                },
                {
                    title: "Cliente",
                    data: 'nombre_cliente'
                },
                {
                    title: "Unidad",
                    data: function(data) {
                        if(data.id_unidad){
                            return `${data.unidad_descripcion}`;
                        }else{
                            return '<button onClick="handleModal('+data.id+')" class="btn btn-primary"> <i class="fas fa-car-side"></i>  </button>';
                        }
                    }
                },
                {
                    title: "Enganche",
                    data: 'enganche'
                },
                {
                    title: "Total",
                    data: 'total'
                },
                {
                    title: "Asesor",
                    data: 'nombre_vendedor'
                },
                {
                    title: "Estatus de venta",
                    data: 'estatus_venta'
                },
                {
                    title: "Editar",
                    data: function({id}) {
                        return "<a alt='Editar'  href='" + site_url + '/autos/ventas/detalle_venta/' + id + "' class='btn btn-primary'><i class='fas fa-pen'></i></a>";
                    }
                },
            ]
        });

        $("#btn-filtrar").on('click', function() {
            tabla_pre_ventas.ajax.reload();
        });

        const handleModal = (venta_id)=>{
            // console.log("element ",venta_id);
            const actual_item = document.getElementById('modal-empatar-unidad');
            actual_item.dataset.venta_id = venta_id;
            $("#title_modal").text('Asignar unidad')
            $("#modal-empatar-unidad").modal('show');
        }

        const btn_empatar_unidad = ()=>{
            const actual_item = document.getElementById('modal-empatar-unidad');
            // return console.log(actual_item);
            // return console.log( actual_item.getAttribute('data-venta_id') );
            if(!actual_item.getAttribute('data-venta_id')){
                return toastr.error("No se ha cargado información correctamente");
            }

            if(document.getElementById('id_unidad').value == ''){
                return toastr.error("No se ha cargado información correctamente");
            }

            ajax.post('api/unidades/ventasinunidad', {
                id_unidad: document.getElementById('id_unidad').value,
                id:actual_item.getAttribute('data-venta_id')
            }, function(response, headers) {
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }
                
                utils.displayWarningDialog("Remisión enlazada correctamente a venta", "success", function(data) {
                    return window.location.href = base_url + 'autos/ventas/index';
                })
            })
        }

    </script>
@endsection


@section('modal')
<div class="modal fade" id="modal-empatar-unidad" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php renderSelectArray('id_unidad', 'Unidades disponibles', $cat_remisiones, 'id', 'modelo_descripcion',  null) ?>
                    </div>
                    @if (empty($cat_remisiones))
                        <div class="col-md-12 alert alert-warning">
                            No hay unidades en el sistema
                        </div>
                    @endif
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                @if (!empty($cat_remisiones))
                    <button onclick="btn_empatar_unidad()" id="btn-modal-agregar" type="button" class="btn btn-primary">
                        <i class="fas fa-shopping-cart"></i> Confirmar
                    </button>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection