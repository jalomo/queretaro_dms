@layout('tema_luna/layout')
<style>
	.nav-fill .nav-item {
		-ms-flex: .3 1 auto !important;
		flex: .3 1 auto !important;
		text-align: center;
	}
</style>
@section('contenido')
<link href="{{ base_url('css/custom/tabs_custom.css') }}" rel="stylesheet">
<div class="container-fluid panel-body">
	<h1 class="mt-4">{{ isset($titulo) ? $titulo : "" }}</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item active">{{ isset($subtitulo) ? $subtitulo : "" }}</li>
	</ol>
	<div class="mt-3 form-row justify-content-end breadcrumb-item">
		<button type="button" class="btn btn-primary btn-flujo"> Cancelar </button>&nbsp
	</div>
	<hr>
	<div class="c_breadcrumb">
		<ul class="nav nav-pills nav-tabs nav-fill" role="tablist">
			<li class="nav-item">
				<a id="nav_paso1" class="nav-link" data-toggle="tab" href="#paso1">Cliente</a>
			</li>
			<li class="nav-item">
				<a id="nav_paso2" class="nav-link" data-toggle="tab" href="#paso2">Auto</a>
			</li>
			<li class="nav-item">
				<a id="nav_paso3" class="nav-link disabled" data-toggle="tab" href="#paso3">Crédito</a>
			</li>
			<li class="nav-item">
				<a id="nav_paso4" class="nav-link disabled" data-toggle="tab" href="#paso4">Abono</a>
			</li>
			<li class="nav-item">
				<a id="nav_paso5" class="nav-link disabled" data-toggle="tab" href="#paso5">Apartado</a>
			</li>
			<li class="nav-item">
				<a id="nav_paso6" class="nav-link disabled" data-toggle="tab" href="#paso6">Contratos</a>
			</li>
			<li class="nav-item">
				<a id="nav_paso7" class="nav-link disabled" data-toggle="tab" href="#paso7">Facturación</a>
			</li>
			<li class="nav-item">
				<a id="nav_paso8" class="nav-link disabled" data-toggle="tab" href="#paso8">Salida</a>
			</li>

		</ul>
	</div>
	<div class="tab-content p-3" style="border:1px solid #d5d5d5; background-color:#fff;">
		<div class="tab-pane" id="paso1">
			@include('autos_ventas/steps/step_cliente')
		</div>
		<div class="tab-pane" id="paso2">
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$("#nav_paso1").click();
		let paso = '{{ $step }}';
		goToTab(paso)
		function goToTab(paso) {
			$(".nav-link").removeClass('active');
			$(".tab-pane").removeClass('active');
			$("#nav_paso" + paso).addClass('active')
			$("#paso" + paso).addClass('active')
		}
	});
</script>
<script src="{{ base_url('js/autos_ventas/preventa/step_cliente.js') }}"></script>
@endsection


@section('modal')
<div class="modal fade" id="modal-alta-cliente" data-backdrop="static" data-keyboard="false" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="modalagregarcliente" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="title_modal_cliente"> Agregar cliente</h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<form id="form-cliente" class="col-md-12">
						@include('autos_ventas/partials/registro_cliente')
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" id="btn_cerrar_permiso" data-dismiss="modal">Cancelar</button>
				<button id="btn_agregar_cliente" onClick="app.guardarCliente()" type="button" class="btn btn-primary">
					<i class="fas fa-save"></i> Guardar
				</button>
				<button id="btn_editar_cliente" style="display:none" onClick="app.updateCliente()" type="button" class="btn btn-primary">
					<i class="fas fa-edit"></i> Actualizar
				</button>

			</div>
		</div>
	</div>
</div>
@endsection