@layout('tema_luna/layout')
@section('contenido')
    <style type="text/css">
        .my_alert-danger {
            color: #a94442;
            background-color: #f2dede;
            border-color: #ebccd1;
            font-weight: bold;
            padding: 5px !important;
        }

        td a i {
            font-size: 25px !important;
        }
        .visualizar{
            visibility: visible;
        }
        .ocultar{
            display: none;
        }

    </style>
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <a href="{{ base_url('logistica/sasseguros/agregar/0') }}" id="guardar"
            class="btn btn-sm btn-primary pull-right m-t-n-xs"><strong>Agregar</strong></a>
            <br><br>
        <div class="row">
            <div class="col-sm-12">
                <div id="tabla-items">
                    <table id="tbl" class="table table-bordered" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Reclamo</th>
                                <th>Tipo</th>
                                <th>Unidad</th>
                                <th>Color</th>
                                <th>Año</th>
                                <th>Serie</th>
                                <th>Asesor</th>
                                <th>Fecha alta</th>
                                <th>Fecha entrega asesor</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $clase_erronea = ''; ?>
                            @foreach ($info as $v => $data)
                                <tr>
                                    <td>{{$data->reclamo}}</td>
                                    <td>{{$data->tipo}}</td>
                                    <td>{{$data->unidad}}</td>
                                    <td>{{$data->color}}</td>
                                    <td>{{$data->anio}}</td>
                                    <td>{{$data->serie}}</td>
                                    <td>{{$data->nombre.' '.$data->apellido_paterno.' '.$data->apellido_materno}}</td>
                                    <td>{{$data->fecha_alta}}</td>
                                    <td>{{$data->fecha_entrega_asesor}}</td>
                                    <td>
                                        <a href="{{ site_url('logistica/sasseguros/agregar/' . $data->id) }}" class="" title="Editar">
                                            <i class="pe pe pe-7s-note"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script>
        inicializar_tabla_local();
        var site_url = "{{ site_url() }}";
        var cliente = '';
        var aPos = '';
        $("body").on("click", '.js_comentarios', function(e) {
            e.preventDefault();
            aPos = $(this);
            id_financiamiento = $(this).data('id');
            id_estatus = $(this).data('id_estatus');
            id_estatus_piso = $(this).data('id_estatus_piso');
            var url = site_url + "/financiamientos/cambiar_estatus/0";
            customModal(url, {
                    "id_financiamiento": id_financiamiento,
                    "id_estatus": id_estatus,
                    "id_estatus_piso": id_estatus_piso,
                }, "GET", "lg", saveComentario, "", "Guardar", "Cancelar", "Ingresar comentario",
                "modalComentario");
        });

        $("body").on("click", '.js_historial', function(e) {
            e.preventDefault();
            id_financiamiento = $(this).data('id');
            var url = site_url + "/financiamientos/historial_comentarios/";
            customModal(url, {
                "id_financiamiento": id_financiamiento
            }, "POST", "lg", "", "", "", "Cerrar", "Historial de comentarios", "modalHistorialComentarios");
        });
        function saveComentario() {
            const data = {
                id_financiamiento: $("#id_financiamiento").val(),
                comentario: $("#comentario").val(),
                id_usuario: "{{ $this->session->userdata('id') }}",
                tipo_comentario : $("input[name='tipo_comentario']:checked").val(),
            }
            if(data.tipo_comentario==1){
                data.id_estatus = $("#id_estatus").val();
            }else{
                data.id_estatus = $("#id_estatus_piso").val();
            }
            ajax.post('api/financiamientos/historial-estatus', data,
                function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    var titulo = (headers.status != 200) ? headers.message : "Información guardada con éxito";
                    utils.displayWarningDialog("Información guardada con éxito", "success", function(result) {
                        if(data.tipo_comentario==1){
                            $(aPos).data('id_estatus', data.id_estatus); 
                        }else{
                            $(aPos).data('id_estatus_piso', data.id_estatus); 
                        }
                        $(".modalComentario").modal('hide')
                    })  
                })
        }
        $("body").on('click', '#buscar', function() {
            buscarInformacion();
        });
        function buscarInformacion() {
            var url = site_url + "/financiamientos/lista_ventas/";
            ajaxLoad(url, {
                "fecha": $("#fecha").val(),
            }, "tabla-items", "POST", function() {
                inicializar_tabla_local();
            });
        }

        function inicializar_tabla_local() {
            $('#tbl').DataTable({
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": "Anterior",
                        "sNext": "Siguiente",
                        "sLast": "Última",
                        "sFirst": "Primera"
                    },
                    "sLengthMenu": '<div id="combo_datatable">Mostrar <select>' +
                        '<option value="5">5</option>' +
                        '<option value="10">10</option>' +
                        '<option value="20">20</option>' +
                        '<option value="30">30</option>' +
                        '<option value="40">40</option>' +
                        '<option value="50">50</option>' +
                        '<option value="-1">Todos</option>' +
                        '</select> registros',
                    "sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)",
                    "sInfoFiltered": " - filtrados de _MAX_ registros",
                    "sInfoEmpty": "No hay resultados de búsqueda",
                    "sZeroRecords": "No hay registros para mostrar...",
                    "sProcessing": "Espere, por favor...",
                    "sSearch": "Buscar:"
                },
                "scrollX": true
            });
        }
        

    </script>
@endsection
