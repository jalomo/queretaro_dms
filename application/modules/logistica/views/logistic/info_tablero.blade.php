<br>
<br>
<table class="table table-bordered" cellspacing="0" width="100%">
    <tbody>

        <?php
        $time = '07:00';
        $contador = 0;
        ?>
        @while ($contador < 14)
            <tr>
                <td width="20%">{{ $time }}</td>
                <?php $datos = procesarResponseApiJsonToArray(
                    $this->curl->curlPost('api/logistica/master/get-all', [
                    'fecha_entrega_cliente' => $fecha_entrega_cliente,
                    'id_carril' => $id_carril,
                    'hora_entrega_cliente' => $time . ':00',
                    ])
                ); 
                // print_r($datos);die();
                ?>
                @if ($datos)
                    <th data-id="{{ $datos[0]->id }}" class="ver_detalles">
                        <label>Serie: </label> {{ $datos[0]->serie }} <br>
                        <label>Unidad: </label> {{ $datos[0]->unidad }} <br>
                        <label>Cliente: </label>
                        {{ $datos[0]->nombre_cliente . ' ' . $datos[0]->ap_cliente . ' ' . $datos[0]->am_cliente }} <br>
                        <label>Vendedor: </label>
                        {{ $datos[0]->nombre_vendedor . ' ' . $datos[0]->ap_vendedor . ' ' . $datos[0]->am_vendedor }}
                        <br>
                    </th>
                @else
                    <th></th>
                @endif

                <?php
                $timestamp = strtotime($time) + 60 * 60;
                $time = date('H:i', $timestamp);
                $contador++;
                ?>
            </tr>
        @endwhile


    <tbody>

    </tbody>
</table>
