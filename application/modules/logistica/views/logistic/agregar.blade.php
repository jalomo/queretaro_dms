@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <form id="frm">
            <input type="hidden" name="id" id="id" value="{{ $id }}">
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Fecha entrega cliente</label>
                    {{ $fecha_entrega_cliente }}
                </div>
                <div class="col-sm-4">
                    <label for="">Carril</label>
                    {{ $id_carril }}
                </div>
                <div class="col-sm-4">
                    <label for="">Hora entrega cliente</label>
                    {{ $hora_entrega_cliente }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Serie</label>
                    {{ $serie }}
                </div>
                <div class="col-sm-4">
                    <label for="">Unidad</label>
                    {{ $id_unidad }}
                </div>
                <div class="col-sm-4">
                    <label for="">Color</label>
                    {{ $id_color }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Eco</label>
                    {{ $eco }}
                </div>
                <div class="col-sm-4">
                    <label for="">Cliente</label>
                    {{ $id_cliente }}
                </div>
                <div class="col-sm-4">
                    <label for="">Vendedor</label>
                    {{ $id_vendedor }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Tipo de operación</label>
                    {{ $tipo_operacion }}
                </div>
                <div class="col-sm-4">
                    <label for="">Iva desglosado</label>
                    {{ $iva_desglosado }}
                </div>
                <div class="col-sm-4">
                    <label for="">Toma</label>
                    {{ $toma }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Fecha promesa vendedor</label>
                    {{ $fecha_promesa_vendedor }}
                </div>
                <div class="col-sm-4">
                    <label for="">Hora promesa vendedor</label>
                    {{ $hora_promesa_vendedor }}
                </div>
                <div class="col-sm-4">
                    <label for="">Ubicación</label>
                    {{ $ubicacion }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Accesorios</label> <br>
                    {{ $accesorios }}
                </div>
                
                <div class="col-sm-4">
                    <label for="">Fecha de programación</label> <br>
                    {{ $fecha_programacion }}
                </div>
                <div class="col-sm-4">
                    <label for="">Hora programación</label> <br>
                    {{ $hora_programacion }}
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Modo</label> <br>
                    {{ $id_modo }}
                </div>
                <div class="col-sm-2">
                    <label for="">Servicio</label> <br>
                    {{ $servicio }}
                </div>
                <div class="col-sm-2">
                    <label for="">Reprogramación</label> <br>
                    {{ $reprogramacion }}
                </div>
                <div class="col-sm-2">
                    <label for="">Seguro</label> <br>
                    {{ $seguro }}
                </div>
                <div class="col-sm-2">
                    <label for="">Permiso / Placas</label> <br>
                    Permiso {{ $permiso }}
                    Placas {{ $placas }}
                </div>
            </div>
            <div class="row">
            </div>
        </form>
        <br>
        <button type="button" id="guardar" class="btn btn-primary pull-right">Guardar</button>
        <br><br>
        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;"></div>
    </div>
    </div>


@endsection
@section('scripts')
    <script src="<?php echo base_url(); ?>js/custom/numeric.js"></script>
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script type="text/javascript">
        $('.numeric').numeric();
        $('.positive').numeric();
        $(".busqueda").select2();
        const url_api = "{{ API_URL_DEV }}";
        const existeCita = () => {
            const arr_fecha = $("#fecha_entrega_cliente").val().split("/");
            const fecha_entrega_cliente = arr_fecha[2] + '-' + arr_fecha[1] + '-' + arr_fecha[0];
            const data = {
                fecha_entrega_cliente: fecha_entrega_cliente,
                hora_entrega_cliente: $("#hora_entrega_cliente").val(),
                id: $("#id").val(),
            }
            ajax.post('api/logistica/master/existe-reservacion', data,
                function(response, headers) {
                    if (response.length > 0) {
                        return true;
                    }
                    return false;
                })
        }
        $("#guardar").on('click', function() {
            if (existeCita()) {
                utils.displayWarningDialog("El horario ya fue ocupado", 'warning', function(result) {});
            } else {
                const arr_fecha = $("#fecha_entrega_cliente").val().split("/");
                const fecha_entrega = arr_fecha[2] + '-' + arr_fecha[1] + '-' + arr_fecha[0];
                const data = {
                    fecha_entrega_cliente: fecha_entrega,
                    hora_entrega_cliente: $("#hora_entrega_cliente").val(),
                    eco: $("#eco").val(),
                    id_unidad: $("#id_unidad").val(),
                    id_color: $("#id_color").val(),
                    serie: $("#serie").val(),
                    id_cliente: $("#id_cliente").val(),
                    id_vendedor: $("#id_vendedor").val(),
                    tipo_operacion: $("#tipo_operacion").val(),
                    iva_desglosado: $("#iva_desglosado").val(),
                    toma: $("#toma").val(),
                    fecha_promesa_vendedor: $("#fecha_promesa_vendedor").val(),
                    hora_promesa_vendedor: $("#hora_promesa_vendedor").val(),
                    ubicacion: $("#ubicacion").val(),
                    reprogramacion: ($("#reprogramacion").prop('checked')) ? 1 : 0,
                    seguro: ($("#seguro").prop('checked')) ? 1 : 0,
                    accesorios: $("#accesorios").val(),
                    servicio: ($("#servicio").prop('checked')) ? 1 : 0,
                    fecha_programacion: $("#fecha_programacion").val(),
                    hora_programacion: $("#hora_programacion").val(),
                    id_modo: $("#id_modo").val(),
                    folio: $("#folio").val(),
                    id_unidad: $("#id_unidad").val(),
                    id_color: $("#id_color").val(),
                    serie: $("#serie").val(),
                    solicitud: $("#solicitud").val(),
                    recibos: $("#recibos").val(),
                    fecha_entrega: $("#fecha_entrega").val(),
                    observaciones: $("#observaciones").val(),
                    permiso_placas: $("input[name='permiso_placas']:checked").val(),
                    id_carril: $("#id_carril").val(),

                };
                if ($("#id").val() != 0) {
                    ajax.put('api/logistica/' + $("#id").val(), data,
                        function(response, headers) {
                            if (headers.status == 400) {
                                return ajax.showValidations(headers);
                            }
                            var titulo = (headers.status != 200) ? headers.message :
                                "Información actualizada con éxito";
                            utils.displayWarningDialog("Información actualizada con éxito", "success", function(
                                data) {
                                return window.location.href = base_url +
                                    'logistica/listado';
                            })
                        })
                } else {
                    ajax.post('api/logistica', data,
                        function(response, headers) {
                            if (headers.status == 400) {
                                return ajax.showValidations(headers);
                            }
                            var titulo = (headers.status != 200) ? headers.message :
                                "Información guardada con éxito";
                            utils.displayWarningDialog("Información guardada con éxito", "success", function(
                                data) {
                                return window.location.href = base_url +
                                    'logistica/listado';
                            })
                        })
                }
            }

        })
        $("#fecha_entrega_cliente").on("change", function() {
            getTiempoCita()
        });
        $("#id_carril").on("change", function() {
            getTiempoCita()
        });

        function getTiempoCita() {
            var url = site_url + "/logistica/getTiempo";
            fecha = $("#fecha_entrega_cliente").val();
            id_carril = $("#id_carril").val();
            if (fecha != '' && id_carril != '') {
                ajaxJson(url, {
                    "id_carril": id_carril,
                    "fecha": fecha
                }, "POST", "", function(result) {
                    result = JSON.parse(result);
                    if (result.length != 0) {
                        $("#hora_entrega_cliente").empty();
                        $("#hora_entrega_cliente").removeAttr("disabled");
                        $("#hora_entrega_cliente").append("<option value=''>-- Selecciona --</option>");
                        $.each(result, function(i, item) {
                            $("#hora_entrega_cliente").append("<option value= '" + result[i] + "'>" +
                                result[i]
                                .substring(0, 5) + "</option>");
                        });
                    } else {
                        $("#hora_entrega_cliente").empty();
                        $("#hora_entrega_cliente").append("<option value='0'>No se encontraron datos</option>");
                    }
                });
            } else {
                $("#hora_entrega_cliente").empty();
                $("#hora_entrega_cliente").append("<option value=''>-- Selecciona --</option>");
                $("#hora_entrega_cliente").attr("disabled", true);
            }
        }
        const buscarRemisionBySerie = (serie) => {
            let url = url_api + 'api/unidades/get-all?serie=' + serie;
            ajaxJson(url, {}, "GET", "", function(result) {
                result = result.data;
                console.log('data',result);
                $(".busqueda").select2('destroy');
                if (result.length > 0) {
                    result = result[0];
                    $("#id_unidad").val(result.id);
                    $("#id_color").val(result.id_color_exterior);
                    $("#id_anio option:contains("+result.modelo+")").attr('selected', 'selected');

                } else {
                }
                $(".busqueda").select2();
            });
        }
        $("#serie").on('change', () => buscarRemisionBySerie($("#serie").val()))

    </script>
@endsection
