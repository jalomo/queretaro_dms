@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <form id="frm">
            <input type="hidden" name="id" id="id" value="{{ $id }}">
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Fecha de arribo</label>
                    {{ $fecha_arribo }}
                </div>
                <div class="col-sm-4">
                    <label for="">Estatus admvo</label>
                    {{ $id_estatus_admvo }}

                </div>
                <div class="col-sm-4">
                    <label for="">Serie</label>
                    {{ $serie }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Unidad</label>
                    <input class="form-control" readonly id="unidad_descripcion" name="unidad_descripcion" />
                </div>
                <div class="col-sm-4">
                    <label for="">Linea / Color</label>
                    {{ $id_color }}
                </div>
                <div class="col-sm-4">
                    <label for="">Daños</label>
                    {{ $danios }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Asesor</label>
                    {{ $id_asesor }}
                </div>
                <div class="col-sm-4">
                    <label for="">ubicación de llaves</label>
                    {{ $id_ubicacion_llaves }}
                </div>
                <div class="col-sm-4">
                    <label for="">Fecha ingreso</label>
                    {{ $fecha_ingreso }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Fecha entrega</label>
                    {{ $fecha_entrega }}
                </div>
                <div class="col-sm-4">
                    <label for="">Estatus</label>
                    {{ $id_estatus }}
                </div>
                <div class="col-sm-4">
                    <label for="">Agencia</label>
                    {{ $id_agencia }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Origen</label>
                    {{ $id_origen }}
                </div>
                <div class="col-sm-4">
                    <label for="">Hora de recepción</label>
                    {{ $hora_recepcion }}
                </div>
                <div class="col-sm-4">
                    <label for="">Cargo a </label>
                    {{ $cargo_a }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Cobro a proveedores</label>
                    {{ $cobro_proveedores }}
                </div>
                <div class="col-sm-4">
                    <label for="">Medio de transportación</label>
                    {{ $medio_transportacion }}
                </div>
                <div class="col-sm-4">
                    <label for="">Área de reparación</label>
                    {{ $id_area_reparacion }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Descripción</label>
                    {{ $descripcion }}
                </div>
                <div class="col-sm-4">
                    <label for="">Faltantes</label>
                    {{ $faltantes }}
                </div>
                <div class="col-sm-4">
                    <label for="">Observaciones</label>
                    {{ $observaciones }}
                </div>
            </div>
        </form>
        <br>
        <button type="button" id="guardar" class="btn btn-primary pull-right">Guardar</button>
        <br><br>
        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;"></div>
    </div>
    </div>


@endsection
@section('scripts')
    <script src="<?php echo base_url(); ?>js/custom/numeric.js"></script>
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script type="text/javascript">
        $('.numeric').numeric();
        $('.positive').numeric();
        $(".busqueda").select2();
        const url_api = "{{ API_URL_DEV }}";
        $("#guardar").on('click', function() {
            const data = {
                fecha_arribo: $("#fecha_arribo").val(),
                id_estatus_admvo: $("#id_estatus_admvo").val(),
                id_asesor: $("#id_asesor").val(),
                serie: $("#serie").val(),
                id_color: $("#id_color").val(),
                danios: $("#danios").val(),
                id_ubicacion_llaves: $("#id_ubicacion_llaves").val(),
                descripcion: $("#descripcion").val(),
                faltantes: $("#faltantes").val(),
                observaciones: $("#observaciones").val(),
                fecha_ingreso: $("#fecha_ingreso").val(),
                fecha_entrega: $("#fecha_entrega").val(),
                id_estatus: $("#id_estatus").val(),
                id_agencia: $("#id_agencia").val(),
                id_origen: $("#id_origen").val(),
                hora_recepcion: $("#hora_recepcion").val(),
                cargo_a: $("#cargo_a").val(),
                cobro_proveedores: $("#cobro_proveedores").val(),
                medio_transportacion: $("#medio_transportacion").val(),
                id_area_reparacion: $("#id_area_reparacion").val()
            };

            if ($("#id").val() != 0) {
                ajax.put('api/logistica/danios-bodyshop/' + $("#id").val(), data,
                    function(response, headers) {
                        if (headers.status == 400) {
                            return ajax.showValidations(headers);
                        }
                        var titulo = (headers.status != 200) ? headers.message :
                            "Información actualizada con éxito";
                        utils.displayWarningDialog("Información actualizada con éxito", "success", function(
                            data) {
                            return window.location.href = base_url +
                                'logistica/danios/listado';
                        })
                    })
            } else {
                ajax.post('api/logistica/danios-bodyshop', data,
                    function(response, headers) {
                        if (headers.status == 400) {
                            return ajax.showValidations(headers);
                        }
                        var titulo = (headers.status != 200) ? headers.message :
                            "Información guardada con éxito";
                        utils.displayWarningDialog("Información guardada con éxito", "success", function(
                            data) {
                            return window.location.href = base_url +
                                'logistica/danios/listado';
                        })
                    })
            }
        })
        const buscarRemisionBySerie = (serie) => {
            let url = url_api + 'api/unidades/get-all?serie=' + serie;
            ajaxJson(url, {}, "GET", "", function(result) {
                result = result.data;
                console.log('data',result);
                $(".busqueda").select2('destroy');
                if (result.length > 0) {
                    result = result[0];
                    $("#unidad_descripcion").val(result.unidad_descripcion);
                    $("#id_color").val(result.id_color_exterior);
                    $("#id_ubicacion_llaves").val(result.ubicacion_llaves_id);
                } else {
                }
                $(".busqueda").select2();
            });
        }
        $("#serie").on('change', () => buscarRemisionBySerie($("#serie").val()))

    </script>
@endsection
