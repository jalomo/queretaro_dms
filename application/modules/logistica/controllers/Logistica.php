<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Logistica extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'url', 'date', 'general'));
        $this->load->library('curl');
        date_default_timezone_set('America/Mexico_City');
    }
    public function tablero()
    {
        $carriles = procesarResponseApiJsonToArray($this->curl->curlGet('api/logistica/catalogos/cat-carriles'));
        $data['id_carril'] = form_dropdown('id_carril', array_combos($carriles, 'id', 'carril', TRUE),'', 'class="form-control busqueda" id="id_carril"');
        $data['titulo'] = 'Tablero entrega unidades';
        $this->blade->render('logistic/tablero',$data);
    }
    public function buscar_tablero(){
        $data['fecha_entrega_cliente'] = $_POST['fecha_entrega_cliente'];
        $data['id_carril'] = $_POST['id_carril'];
        $this->blade->render('logistic/info_tablero',$data);
    }
    public function detalles(){
        $data['registro'] = procesarResponseApiJsonToArray($this->curl->curlPost('api/logistica/master/get-horarios', [
            'id' => $_POST['id']
        ]));
        $this->blade->render('logistic/detalles_tablero',$data);
    }
    public function listado()
    {
        $data['info'] = procesarResponseApiJsonToArray($this->curl->curlPost('api/logistica/master/get-all', []));
        $data['titulo'] = "Listado unidades nuevas";
        $data['bitacora'] = "inicial";
        $this->blade->render('logistic/listado', $data);
    }
    public function agregar($id = 0)
    {
        if ($id == 0) {
            $info = new Stdclass();
            $array_horarios = [];
        } else {
            $info = procesarResponseApiJsonToArray($this->curl->curlGet('api/logistica/' . $id));
        }
        //Días para la cita
        $fecha = date('Y-m-d');
        $ffin = strtotime('+1 month', strtotime($fecha));
        $ffin = date('Y-m-d', $ffin);
        $array_fechas = array('');
        while ($fecha <= $ffin) {
            $array_fechas[] = date_eng2esp_1($fecha);
            $fecha = strtotime('+1 day', strtotime($fecha));
            $fecha = date('Y-m-d', $fecha);
        }
        $valor_fecha = date_eng2esp_1(set_value('fecha', exist_obj($info, 'fecha_entrega_cliente')));
        //dd($valor_fecha);
        $data['fecha_entrega_cliente'] = form_dropdown_fechas('fecha_entrega_cliente', $array_fechas, $valor_fecha, 'class="form-control" id="fecha_entrega_cliente"');
        $data['hora_entrega_cliente'] = form_dropdown('hora_entrega_cliente', array(), set_value('hora_entrega_cliente', exist_obj($info, 'hora_entrega_cliente')), 'class="form-control" id="hora_entrega_cliente"');

        $data['eco'] = form_input('eco', set_value('eco', exist_obj($info, 'eco')), 'class="form-control" id="eco"');
        $unidades = procesarResponseApiJsonToArray($this->curl->curlGet('api/unidades'));
        $data['id_unidad'] = form_dropdown('id_unidad', array_combos($unidades, 'id', 'modelo_descripcion', TRUE), set_value('id_unidad', exist_obj($info, 'id_unidad')), 'class="form-control busqueda" id="id_unidad"');
        $colores = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-colores'));
        $data['id_color'] = form_dropdown('id_color', array_combos($colores, 'id', 'nombre', TRUE), set_value('id_color', exist_obj($info, 'id_color')), 'class="form-control busqueda" id="id_color"');
        $data['serie'] = form_input('serie', set_value('serie', exist_obj($info, 'serie')), 'class="form-control" id="serie" maxlength="17"');

        $clientes = procesarResponseApiJsonToArray($this->curl->curlGet('api/clientes'));
        $array_clientes = [];
        foreach ($clientes as $c => $cliente) {
            $info_user = new stdClass();
            $info_user->id = $cliente->id;
            $info_user->nombre = $cliente->nombre . ' ' . $cliente->apellido_paterno . ' ' . $cliente->apellido_materno;

            $array_clientes[$c] = $info_user;
        }
        $data['id_cliente'] = form_dropdown('id_cliente', array_combos($array_clientes, 'id', 'nombre', TRUE), set_value('id_cliente', exist_obj($info, 'id_cliente')), 'class="form-control busqueda" id="id_cliente"');
        $vendedores = procesarResponseApiJsonToArray($this->curl->curlPost('api/usuarios/usuario-by-rol', [
            'rol_id' => 11
        ]));
        $data['id_vendedor'] = form_dropdown('id_vendedor', array_combos($vendedores, 'id', 'nombre', TRUE), set_value('id_vendedor', exist_obj($info, 'id_vendedor')), 'class="form-control busqueda" id="id_vendedor"');

        $data['tipo_operacion'] = form_input('tipo_operacion', set_value('tipo_operacion', exist_obj($info, 'tipo_operacion')), 'class="form-control" id="tipo_operacion"');
        $data['iva_desglosado'] = form_input('iva_desglosado', set_value('iva_desglosado', exist_obj($info, 'iva_desglosado')), 'class="form-control numeric" id="iva_desglosado"');
        $data['toma'] = form_input('toma', set_value('toma', exist_obj($info, 'toma')), 'class="form-control" id="toma"');
        $data['fecha_promesa_vendedor'] = form_input('fecha_promesa_vendedor', set_value('fecha_promesa_vendedor', exist_obj($info, 'fecha_promesa_vendedor')), 'class="form-control" id="fecha_promesa_vendedor"', 'date');
        $data['hora_promesa_vendedor'] = form_input('hora_promesa_vendedor', set_value('hora_promesa_vendedor', exist_obj($info, 'hora_promesa_vendedor')), 'class="form-control" id="hora_promesa_vendedor"', 'time');
        $data['ubicacion'] = form_input('ubicacion', set_value('ubicacion', exist_obj($info, 'ubicacion')), 'class="form-control" id="ubicacion"');
        $reprogramacion = set_value('reprogramacion', exist_obj($info, 'reprogramacion'));
        $data['reprogramacion'] = form_checkbox('reprogramacion', $reprogramacion, ($reprogramacion) ? true : false, 'id="reprogramacion"');
        $seguro = set_value('seguro', exist_obj($info, 'seguro'));
        $data['seguro'] = form_checkbox('seguro', $seguro, ($seguro) ? true : false, 'id="seguro"');
        $permiso_placas = set_value('permiso_placas', exist_obj($info, 'permiso_placas'));
        $data['placas'] = form_radio('permiso_placas', 1, ($permiso_placas) ? true : false, 'class="" id="se_ingresa_si"');
        $data['permiso'] = form_radio('permiso_placas', 0, ($permiso_placas) ? false : true, 'class="" id="se_ingresa_no"');
        $data['accesorios'] = form_input('accesorios', set_value('accesorios', exist_obj($info, 'accesorios')), 'class="form-control" id="accesorios"');
        $servicio = set_value('servicio', exist_obj($info, 'servicio'));
        $data['servicio'] = form_checkbox('servicio', $servicio, ($servicio) ? true : false, 'id="servicio"');
        $data['fecha_programacion'] = form_input('fecha_programacion', set_value('fecha_programacion', exist_obj($info, 'fecha_programacion')), 'class="form-control" id="fecha_programacion"', 'date');
        $data['hora_programacion'] = form_input('hora_programacion', set_value('hora_programacion', exist_obj($info, 'hora_programacion')), 'class="form-control" id="hora_programacion"', 'time');
        $modos = procesarResponseApiJsonToArray($this->curl->curlGet('api/logistica/catalogos/cat-modos'));
        $data['id_modo'] = form_dropdown('id_modo', array_combos($modos, 'id', 'modo', TRUE), set_value('id_modo', exist_obj($info, 'id_modo')), 'class="form-control busqueda" id="id_modo"');

        $carriles = procesarResponseApiJsonToArray($this->curl->curlGet('api/logistica/catalogos/cat-carriles'));
        $data['id_carril'] = form_dropdown('id_carril', array_combos($carriles, 'id', 'carril', TRUE), set_value('id_carril', exist_obj($info, 'id_carril')), 'class="form-control busqueda" id="id_carril"');
        $data['id'] = $id;
        $data['titulo'] = 'Entrega de unidades nuevas';
        $this->blade->render('logistic/agregar', $data);
    }
    public function getTiempo()
    {
        $fecha = date2sql($this->input->post('fecha'));
        $horarios_ocupados = procesarResponseApiJsonToArray($this->curl->curlPost('api/logistica/master/horarios-ocupados', [
            "id_carril" => $_POST['id_carril'],
            'fecha_entrega_cliente' => $fecha
        ]));
        $tiempo_actual = date('H:i');
        $tiempo = '09:00';
        while ($tiempo <= '20:00') {
            $NuevoTiempo = strtotime('+1 hour', strtotime($tiempo));
            $tiempo = date('H:i:s', $NuevoTiempo);
            if (!in_array($tiempo, $horarios_ocupados)) {
                if ($fecha == date('Y-m-d')) {
                    if ($tiempo >= $tiempo_actual) {
                        $array_tiempo[] = $tiempo;
                    }
                } else {
                    $array_tiempo[] = $tiempo;
                }
            }
        }
        echo json_encode($array_tiempo);
        exit();
    }
}
