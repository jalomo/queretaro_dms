<?php

class Reportes extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time', 300);
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function inventario()
    {
        $this->load->view('formatos/formato_inventario', []);
        // $view = $this->load->view('formatos/formato_inventario', [], true);
        // autos/reportes/pdfCheckList
        // $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
        // $this->blade->render('formatos/formato_inventario');
    }

    public function formatoinventario($id_unidad = '')
    {
        $data['modulo'] = "Seminuevos";
        $data['submodulo'] = ucfirst($this->uri->segment(1));
        $data['subtitulo'] = "Reportes";
        $data['id_unidad'] = $id_unidad;
        $data['titulo'] = "DIAGNÓSTICO Y POSIBLES CONSECUENCIAS";

        $formato = $this->curl->curlGet('api/inventario-unidades/getformulario/'.$id_unidad);
        $response = !empty(procesarResponseApiJsonToArray($formato)) ? procesarResponseApiJsonToArray($formato)[0] : []; 
        $data['datos'] = $response;
        // dd($response);
        $this->blade->render('formatos/formato_inventario', $data);
    }

    public function generarFormatoInventario($id_unidad = '')
    {

        $datos_generales_api = $this->curl->curlGet('api/inventario-unidades/getformulario/'.$id_unidad);

        $datos_generales =  procesarResponseApiJsonToArray($datos_generales_api);
        $data['datos'] = isset($datos_generales) ? $datos_generales[0] : '';
        // utils::pre($data['datos']);
        $view = $this->load->view('formatos/formato_inventario', $data, true);
        $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
    }
}
