<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Reportes extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);

       
        
    }
    
    public function index()
    {
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function reporteinventario()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $data['modulo'] = "Seminuevos";
        $data['submodulo'] = "Reportes";
        $data['titulo'] = "Inventario";
        $data['subtitulo'] = "Consulta";

        $data['menu'] = "menu_semi_reportes_inventario";
        $this->blade->render('reportes/generarReporte', $data);
    }

    public function reportepolizasventas()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $data['modulo'] = "Seminuevos";
        $data['submodulo'] = "Reportes";
        $data['titulo'] = "Pólizas de ventas";
        $data['subtitulo'] = "Consulta";

        $data['menu'] = "menu_semi_reportes_polizaVentas";
        $this->blade->render('reportes/generarReporte', $data);
    }

    public function reportepolizacompras()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $data['modulo'] = "Seminuevos";
        $data['submodulo'] = "Reportes";
        $data['titulo'] = "Póliza de compras";
        $data['subtitulo'] = "Consulta";

        $data['menu'] = "menu_semi_reportes_polizaCompras";
        $this->blade->render('reportes/generarReporte', $data);
    }

    public function reportecomisiones()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $data['modulo'] = "Seminuevos";
        $data['submodulo'] = "Reportes";
        $data['titulo'] = "Comisiones";
        $data['subtitulo'] = "Consulta";

        $data['menu'] = "menu_semi_reportes_comisiones";
        $this->blade->render('reportes/generarReporte', $data);
    }

    public function reporteventadiarias()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $data['modulo'] = "Seminuevos";
        $data['submodulo'] = "Reportes";
        $data['titulo'] = "Ventas diarias";
        $data['subtitulo'] = "Consulta";

        $data['menu'] = "menu_semi_reportes_comisiones";
        $this->blade->render('reportes/generarReporte', $data);
    }

    public function reportependientes()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $data['modulo'] = "Seminuevos";
        $data['submodulo'] = "Reportes";
        $data['titulo'] = "Pendientes por facturar";
        $data['subtitulo'] = "Consulta";

        $data['menu'] = "menu_semi_reportes_pendientes";
        $this->blade->render('reportes/generarReporte', $data);
    }

    public function reporteventavendedor()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $data['modulo'] = "Seminuevos";
        $data['submodulo'] = "Reportes";
        $data['titulo'] = "Por Vendedor";
        $data['subtitulo'] = "Consulta";

        $data['menu'] = "menu_semi_reportes_ventasXvendedoer";
        $this->blade->render('reportes/generarReporte', $data);
    }

    public function generarReporte($parametro = '',$fecha_1 = '',$fecha_2 = '',$campo = '')
    {
        $data['data'] = [];
        $data['titulo'] = "Unidades";
        $this->blade->render('reportes/alta', $data);
    }
}
