<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Consultas extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);

       
        
    }

    public function index()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $dataFromApi = $this->curl->curlGet('api/seminuevos_consultas/');
        $dataRegistro = [];
        $dataRegistro = procesarResponseApiJsonToArray($dataFromApi);
        $data['data'] = $dataRegistro;

        $data['modulo'] = "Seminuevos";
        $data['submodulo'] = "Ventas";
        $data['titulo'] = "Consultas";
        $data['subtitulo'] = "Listado";

        $this->blade->render('consultas/vistaconsulta', $data);
    }

    public function consultas($id='')
    {
        $data['data'] = [];

        $data['modulo'] = "Seminuevos";
        $data['submodulo'] = "Ventas";
        $data['titulo'] = "Consultas";
        $data['subtitulo'] = "Registro";

        $this->blade->render('consultas/alta', $data);
    }
}
