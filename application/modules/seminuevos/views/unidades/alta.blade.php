@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : ''; ?>
            </li>
            <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : ''; ?></li>
            <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : ''; ?></li>
        </ol>

        <div class="row">
            <div class="col-md-12">
                <form id="recepcion_unidad" data-id="{{ isset($data->id) ? $data->id : '' }}">
                    <div class="row">
                        <div class="col-md-6">
                            <?php renderInputText('text', 'usuario_recibe', 'Usuario que recibe:',
                            isset($data->usuario_recibe) ? $data->usuario_recibe : '', isset($data->usuario_recibe) ? true :
                            false); ?>
                        </div>
                        <div class="col-md-3">
                            <?php renderInputText('date', 'fecha_recepcion', 'Fecha de recepción:',
                            isset($data->fecha_recepcion) ? $data->fecha_recepcion : '', isset($data->fecha_recepcion) ?
                            true : false); ?>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Estado:</label>
                                <select disabled class="form-control" id="id_estado" name="id_estado" style="width: 100%;">
                                    <option value="">Selecionar ...</option>
                                    <option value="2" selected>Semi Nuevo </option>
                                </select>
                                <div id="id_estado_error" class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-5">

                            <?php renderInputTextArea('unidad_descripcion', 'Unidad (Descripción Larga):',
                            isset($data->unidad_descripcion) ? $data->unidad_descripcion : ''); ?>
                        </div>
                        <div class="col-md-7">
                            <div class="row">
                                <div class="col-md-6">
                                    <?php renderInputText('text', 'numero_puertas', 'N° de puertas',
                                    isset($data->numero_puertas) ? $data->numero_puertas : ''); ?>
                                </div>
                                <div class="col-md-6">
                                    <?php renderSelectArray('combustible', 'Combustible', $cat_combustible,
                                    'nombre', 'nombre', null); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <?php renderSelectArray('marca_id', 'Marca', $cat_marcas, 'id', 'nombre',
                                    isset($data->marca_id) ? $data->marca_id : ''); ?>
                                </div>
                                <div class="col-md-6">
                                    <?php renderSelectArray('modelo_id', 'Modelo', $cat_modelo, 'id',
                                    'nombre', isset($data->modelo_id) ? $data->modelo_id : '', false,
                                    'onChange="formularEconomico()"'); ?>
                                </div>
                                <div class="col-md-6">
                                    <?php renderSelectArray('anio_id', 'Año', $cat_anio, 'id', 'nombre',
                                    isset($data->anio_id) ? $data->anio_id : ''); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <?php renderInputText('text', 'clave', 'Clave', isset($data->clave) ?
                            $data->clave : '', true); ?>
                        </div>
                        <div class="col-md-4">
                            <?php renderSelectArray('color_id', 'Color', $cat_color, 'id', 'nombre',
                            isset($data->color_id) ? $data->color_id : ''); ?>
                        </div>
                        <div class="col-md-4">
                            <?php renderSelectArray('vestidura_id', 'Vestidura: ', $cat_interiores, 'id',
                            'nombre', isset($data->vestidura_id) ? $data->vestidura_id : ''); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <?php renderInputText('text', 'n_motor', 'N° Motor: ', isset($data->n_motor) ?
                            $data->n_motor : ''); ?>
                        </div>
                        <div class="col-md-4">
                            <?php renderInputText('text', 'motor', 'Motor: ', isset($data->motor) ?
                            $data->motor : ''); ?>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Transmisión:</label>
                                <select class="form-control" id="transmision" name="transmision" style="width: 100%;">
                                    <option value="">Selecionar ...</option>
                                    <option value="AUTOMÁTICA" <?php if (isset($data->transmision)) {
                                        if ($data->transmision == 'AUTOMÁTICA') {
                                        echo 'selected';
                                        }
                                        } ?>>AUTOMÁTICA </option>
                                    <option value="ESTÁNDAR" <?php if (isset($data->transmision)) {
                                        if ($data->transmision == 'ESTÁNDAR') {
                                        echo 'selected';
                                        }
                                        } ?>>ESTÁNDAR </option>
                                    <option value="CVT" <?php if (isset($data->transmision)) {
                                        if ($data->transmision == 'CVT') {
                                        echo 'selected';
                                        }
                                        } ?>>CVT </option>
                                    <option value="SEMI-AUTOMÁTICA" <?php if (isset($data->transmision)) {
                                        if ($data->transmision == 'SEMI-AUTOMÁTICA') {
                                        echo 'selected';
                                        }
                                        } ?>>SEMI-AUTOMÁTICA </option>
                                </select>
                                <div id="transmision_error" class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">N° cilindros:</label>
                                <input type="text" class="form-control" value="<?php if (isset($data->numero_cilindros)) {
                                        print_r($data->numero_cilindros);
                                    } ?>" id="numero_cilindros" name="numero_cilindros" placeholder="">
                                <div id="numero_cilindros_error" class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Capacidad:</label>
                                <input type="text" class="form-control" value="<?php if (isset($data->capacidad)) {
                                        print_r($data->capacidad);
                                    } ?>" id="capacidad" name="capacidad" placeholder="">
                                <div id="capacidad_error" class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">VIN:</label>
                                <input type="text" class="form-control" maxlength="20" value="<?php if (isset($data->vin)) {
                                        print_r($data->vin);
                                    } ?>" id="vin" name="vin" placeholder="">
                                <div id="vin_error" class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Serie Corta:</label>
                                <input type="text" class="form-control" value="<?php if (isset($data->serie_corta)) {
                                        print_r($data->serie_corta);
                                    } ?>" id="serie_corta" name="serie_corta" placeholder="">
                                <div id="serie_corta_error" class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">No de Serie:</label>
                                <input type="text" class="form-control" value="<?php if (isset($data->no_serie)) {
                                        print_r($data->no_serie);
                                    } ?>" id="no_serie" name="no_serie" placeholder="">
                                <div id="no_serie_error" class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <?php renderInputText('text', 'numero_economico', '# Económico: ',
                            isset($data->numero_economico) ? $data->numero_economico : '', true); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Ubicación:</label>
                                <select class="form-control" id="id_ubicacion" name="id_ubicacion" style="width: 100%;">
                                    <option value="">Selecionar ...</option>
                                    @if (!empty($cat_ubicacion))
                                        @foreach ($cat_ubicacion as $ubicacion)
                                            <option value="{{ $ubicacion->id }}" <?php if (isset($data->
                                                id_ubicacion)) {
                                                if ($data->id_ubicacion == $ubicacion->id) {
                                                echo 'selected';
                                                }
                                                } ?>>{{ $ubicacion->nombre }}</option>
                                        @endforeach
                                    @endif
                                    <option value="0" <?php if (isset($data->id_ubicacion)) {
                                        if ($data->id_ubicacion == '0') {
                                        echo 'selected';
                                        }
                                        } ?>>Otro</option>
                                </select>
                                <div id="id_ubicacion_error" class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Ubicación Llaves:</label>
                                <select class="form-control" id="id_ubicacion_llaves" name="id_ubicacion_llaves"
                                    style="width: 100%;">
                                    <option value="">Selecionar ...</option>
                                    @if (!empty($cat_llaves))
                                        @foreach ($cat_llaves as $llaves)
                                            <option value="{{ $llaves->id }}" <?php if (isset($data->
                                                id_ubicacion_llaves)) {
                                                if ($data->id_ubicacion_llaves == $llaves->id) {
                                                echo 'selected';
                                                }
                                                } ?>>{{ $llaves->nombre }}</option>
                                        @endforeach
                                    @endif
                                    <option value="0" <?php if (isset($data->id_ubicacion_llaves)) {
                                        if ($data->id_ubicacion_llaves == '0') {
                                        echo 'selected';
                                        }
                                        } ?>>Otro</option>
                                </select>
                                <div id="id_ubicacion_llaves_error" class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">N° Producción:</label>
                                <input type="text" class="form-control" value="<?php if (isset($data->n_produccion)) {
                                        print_r($data->n_produccion);
                                    } ?>" id="n_produccion" name="n_produccion" placeholder="">
                                <div id="n_produccion_error" class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">N° Remisión:</label>
                                <input type="text" class="form-control" value="<?php if (isset($data->n_remision)) {
                                        print_r($data->n_remision);
                                    } ?>" id="n_remision" name="n_remision" placeholder="">
                                <div id="n_remision_error" class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Folio Remisión:</label>
                                <input type="text" class="form-control" value="<?php if (isset($data->n_folio_remision)) {
                                        print_r($data->n_folio_remision);
                                    } ?>" id="n_folio_remision" name="n_folio_remision" placeholder="">
                                <div id="n_folio_remision_error" class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Pdto.:</label>
                                <input type="text" class="form-control" value="<?php if (isset($data->pdto)) {
                                        print_r($data->pdto);
                                    } ?>" id="pdto" name="pdto" placeholder="">
                                <div id="pdto_error" class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Folio Pedido<b>*</b>:</label>
                                <input type="text" class="form-control" value="<?php if (isset($data->folio_pedido)) {
                                        print_r($data->folio_pedido);
                                    } ?>" id="folio_pedido" name="folio_pedido" placeholder="">
                                <div id="folio_pedido_error" class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-7">
                            <div class="row">
                                <div class="col-md-4">
                                    <br>
                                </div>
                                <div class="col-md-4">
                                    <label for="">Costo:</label>
                                </div>
                                <div class="col-md-4">
                                    <label for="">Venta:</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4" align="right">
                                    <label for="" style="margin-top: 7px;">Valor unidad:</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            onblur="sumatoria_costo()" class="form-control"
                                            value="{{ isset($data->valor_unidad) ? $data->valor_unidad : '' }}"
                                            id="valor_unidad" name="valor_unidad" placeholder="0.00">
                                        <div id="valor_unidad_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            onblur="sumatoria_venta()" class="form-control"
                                            value="{{ isset($data->valor_unidad_venta) ? $data->valor_unidad_venta : '' }}"
                                            id="valor_unidad_venta" name="valor_unidad_venta" placeholder="0.00">
                                        <div id="valor_unidad_venta_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4" align="right">
                                    <label for="" style="margin-top: 7px;">Equipo Base:</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            onblur="sumatoria_costo()" class="form-control"
                                            value="{{ isset($data->equipo_base_costo) ? $data->equipo_base_costo : '' }}"
                                            id="equipo_base_costo" name="equipo_base_costo" placeholder="0.00">
                                        <div id="equipo_base_costo_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            onblur="sumatoria_venta()" class="form-control"
                                            value="{{ isset($data->equipo_base_venta) ? $data->equipo_base_venta : '' }}"
                                            id="equipo_base_venta" name="equipo_base_venta" placeholder="0.00">
                                        <div id="equipo_base_venta_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4" align="right">
                                    <label for="" style="margin-top: 7px;">Seg. Translado:</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            onblur="sumatoria_costo()" class="form-control"
                                            value="{{ isset($data->seg_traslado_costo) ? $data->seg_traslado_costo : '' }}"
                                            id="seg_traslado_costo" name="seg_traslado_costo" placeholder="0.00">
                                        <div id="seg_traslado_costo_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            onblur="sumatoria_venta()" class="form-control"
                                            value="{{ isset($data->seg_traslado_venta) ? $data->seg_traslado_venta : '' }}"
                                            id="seg_traslado_venta" name="seg_traslado_venta" placeholder="0.00">
                                        <div id="seg_traslado_venta_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4" align="right">
                                    <label for="" style="margin-top: 7px;">Impuestos de Importación:</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            onblur="sumatoria_costo()" class="form-control"
                                            value="{{ isset($data->impuesto_import_costo) ? $data->impuesto_import_costo : '' }}"
                                            id="impuesto_import_costo" name="impuesto_import_costo" placeholder="0.00">
                                        <div id="impuesto_import_costo_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            onblur="sumatoria_venta()" class="form-control"
                                            value="{{ isset($data->impuesto_import_venta) ? $data->impuesto_import_venta : '' }}"
                                            id="impuesto_import_venta" name="impuesto_import_venta" placeholder="0.00">
                                        <div id="impuesto_import_venta_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4" align="right">
                                    <label for="" style="margin-top: 7px;">Fletes de Importacion:</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            onblur="sumatoria_costo()" class="form-control"
                                            value="{{ isset($data->fletes_import_costo) ? $data->fletes_import_costo : '' }}"
                                            id="fletes_import_costo" name="fletes_import_costo" placeholder="0.00">
                                        <div id="fletes_import_costo_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            onblur="sumatoria_venta()" class="form-control"
                                            value="{{ isset($data->fletes_import_venta) ? $data->fletes_import_venta : '' }}"
                                            id="fletes_import_venta" name="fletes_import_venta" placeholder="0.00">
                                        <div id="fletes_import_venta_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4" align="right">
                                    <label for="" style="margin-top: 7px;">Gastos de Trasaldo:</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            onblur="sumatoria_costo()" class="form-control"
                                            value="{{ isset($data->gastos_traslado_costo) ? $data->gastos_traslado_costo : '' }}"
                                            id="gastos_traslado_costo" name="gastos_traslado_costo" placeholder="0.00">
                                        <div id="gastos_traslado_costo_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            onblur="sumatoria_venta()" class="form-control"
                                            value="{{ isset($data->fletes_import_venta) ? $data->fletes_import_venta : '' }}"
                                            id="gastos_traslado_venta" name="gastos_traslado_venta" placeholder="0.00">
                                        <div id="gastos_traslado_venta_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4" align="right">
                                    <label for="" style="margin-top: 7px;">Dtto. FORD:</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            onblur="sumatoria_costo()" class="form-control"
                                            value="{{ isset($data->deduccion_ford_costo) ? $data->deduccion_ford_costo : '' }}"
                                            id="deduccion_ford_costo" name="deduccion_ford_costo" placeholder="0.00">
                                        <div id="deduccion_ford_costo_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            onblur="sumatoria_venta()" class="form-control"
                                            value="{{ isset($data->deduccion_ford_venta) ? $data->deduccion_ford_venta : '' }}"
                                            id="deduccion_ford_venta" name="deduccion_ford_venta" placeholder="0.00">
                                        <div id="deducc_ford_venta_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4" align="right">
                                    <label for="" style="margin-top: 7px;">Bonificación:</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            onblur="sumatoria_costo()" class="form-control"
                                            value="{{ isset($data->bonificacion_costo) ? $data->bonificacion_costo : '' }}"
                                            id="bonificacion_costo" name="bonificacion_costo" placeholder="0.00">
                                        <div id="bonificacion_costo_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            onblur="sumatoria_venta()" class="form-control"
                                            value="{{ isset($data->bonificacion_venta) ? $data->bonificacion_venta : '' }}"
                                            id="bonificacion_venta" name="bonificacion_venta" placeholder="0.00">
                                        <div id="bonificacion_venta_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <div class="row">
                                <div class="col-md-4" align="right">
                                    <label for="" style="margin-top: 7px;">Subtotal:</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            class="form-control"
                                            value="{{ isset($data->subtota_costo) ? $data->subtota_costo : 0 }}"
                                            id="subtota_costo" name="subtota_costo" placeholder="0.00">
                                        <div id="subtota_costo_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            class="form-control"
                                            value="{{ isset($data->subtota_venta) ? $data->subtota_venta : 0 }}"
                                            id="subtota_venta" name="subtota_venta" placeholder="0.00">
                                        <div id="subtota_venta_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4" align="right">
                                    <label for="" style="margin-top: 7px;">I.V.A.:</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            class="form-control"
                                            value="{{ isset($data->iva_costo) ? $data->iva_costo : 0 }}" id="iva_costo"
                                            name="iva_costo" placeholder="0.00">
                                        <div id="iva_costo_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            class="form-control"
                                            value="{{ isset($data->iva_venta) ? $data->iva_venta : 0 }}" id="iva_venta"
                                            name="iva_venta" placeholder="0.00">
                                        <div id="iva_venta_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <div class="row">
                                <div class="col-md-4" align="right">
                                    <label for="" style="margin-top: 7px;">TOTAL:</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            class="form-control"
                                            value="{{ isset($data->total_costo) ? $data->total_costo : 0 }}"
                                            id="total_costo" name="total_costo" placeholder="0.00">
                                        <div id="total_costo_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            class="form-control"
                                            value="{{ isset($data->total_venta) ? $data->total_venta : 0 }}"
                                            id="total_venta" name="total_venta" placeholder="0.00">
                                        <div id="total_venta_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-5">
                            <div class="row">
                                <div class="col-md-12"><br></div>
                            </div>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-5" align="right">
                                    <label for="" style="margin-top: 7px;">Otros Gastos:</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            class="form-control"
                                            value="{{ isset($data->otros_gastos) ? $data->otros_gastos : 0 }}"
                                            id="otros_gastos" name="otros_gastos" placeholder="0.00">
                                        <div id="otros_gastos_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-5" align="right">
                                    <label for="" style="margin-top: 7px;">Gastos Acondi.:</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            class="form-control"
                                            value="{{ isset($data->gastos_acondi) ? $data->gastos_acondi : 0 }}"
                                            id="gastos_acondi" name="gastos_acondi" placeholder="0.00">
                                        <div id="gastos_acondi_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-5" align="right">
                                    <label for="" style="margin-top: 7px;">Ap. Fondos Publicos:</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            class="form-control"
                                            value="{{ isset($data->ap_fondos_pub) ? $data->ap_fondos_pub : 0 }}"
                                            id="ap_fondos_pub" name="ap_fondos_pub" placeholder="0.00">
                                        <div id="ap_fondos_pub_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-5" align="right">
                                    <label for="" style="margin-top: 7px;">Ap. Prog. Civi.:</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            class="form-control"
                                            value="{{ isset($data->ap_prog_civil) ? $data->ap_prog_civil : 0 }}"
                                            id="ap_prog_civil" name="ap_prog_civil" placeholder="0.00">
                                        <div id="ap_prog_civil_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-5" align="right">
                                    <label for="" style="margin-top: 7px;">Cuota AMDA:</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            class="form-control"
                                            value="{{ isset($data->cuota_amda) ? $data->cuota_amda : 0 }}"
                                            id="cuota_amda" name="cuota_amda" placeholder="0.00">
                                        <div id="cuota_amda_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-5" align="right">
                                    <label for="" style="margin-top: 7px;">Cuota COPARMEX:</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            class="form-control"
                                            value="{{ isset($data->cuota_coparmex) ? $data->cuota_coparmex : 0 }}"
                                            id="cuota_coparmex" name="cuota_coparmex" placeholder="0.00">
                                        <div id="cuota_coparmex_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-5" align="right">
                                    <label for="" style="margin-top: 7px;">Cuota Asociación FORD:</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57'
                                            class="form-control"
                                            value="{{ isset($data->cuota_asociacion_ford) ? $data->cuota_asociacion_ford : 0 }}"
                                            id="cuota_asociacion_ford" name="cuota_asociacion_ford" placeholder="0.00">
                                        <div id="cuota_asociacion_ford_error" class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12" align="center">
                            <br>
                            @if (isset($data->id))
                                <button type="button" id="editar_registro" class="btn btn-success col-md-4"> Actualizar
                                </button>
                            @else
                                <button type="button" id="guardar_registro" class="btn btn-success col-md-4"> Guardar
                                </button>
                            @endif

                            <input type="hidden" id="consecutivo" value="<?php if (isset($consecutivo)) {
                                    echo $consecutivo;
                                } else {
                                    echo '0';
                                } ?>">

                            <div id="mensaje" class="alert alert-success" role="alert" style="display:none;"></div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        //Para guardar registro por primera vez
        $("#guardar_registro").on('click', function() {
            $(".invalid-feedback").html("");

            var validar_costo = costos_validar();
            if (validar_costo) {
                ajax.post('api/recepcion-unidades', procesarRegistro(), function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    
                    utils.displayWarningDialog("Proceso completado con éxito", "success", function(data) {
                        return window.location.href = base_url + 'seminuevos/unidades/listadorecepcion';
                    })
                })
            }
        });


        //Armamos el formulario a enviar
        let procesarRegistro = function() {
            let newArray = {
                // recepcion_unidades_id: $("#recepcion_unidad").data('id'),
                usuario_recibe: document.getElementById("usuario_recibe").value,
                fecha_recepcion: document.getElementById("fecha_recepcion").value,
                id_estado: document.getElementById("id_estado").value,
                marca_id: document.getElementById("marca_id").value,
                modelo_id: "1", //document.getElementById("modelo_id").value,
                anio_id: document.getElementById("anio_id").value,
                color_id: document.getElementById("color_id").value,
                vestidura_id: document.getElementById("vestidura_id").value,
                unidad_descripcion: document.getElementById("unidad_descripcion").value,
                numero_puertas: document.getElementById("numero_puertas").value,
                combustible: document.getElementById("combustible").value,
                n_motor: document.getElementById("n_motor").value,
                motor: document.getElementById("motor").value,
                transmision: document.getElementById("transmision").value,
                numero_cilindros: document.getElementById("numero_cilindros").value,
                capacidad: document.getElementById("capacidad").value,
                vin: document.getElementById("vin").value,
                serie_corta: document.getElementById("serie_corta").value,
                numero_economico: document.getElementById("numero_economico").value,
                id_ubicacion: document.getElementById("id_ubicacion").value,
                id_ubicacion_llaves: document.getElementById("id_ubicacion_llaves").value,
                n_produccion: document.getElementById("n_produccion").value,
                n_remision: document.getElementById("n_remision").value,
                n_folio_remision: document.getElementById("n_folio_remision").value,
                pdto: document.getElementById("pdto").value,
                folio_pedido: document.getElementById("folio_pedido").value,

                valor_unidad: document.getElementById("valor_unidad").value,
                valor_unidad_venta: document.getElementById("valor_unidad_venta").value,
                equipo_base_costo: document.getElementById("equipo_base_costo").value,
                equipo_base_venta: document.getElementById("equipo_base_venta").value,
                seg_traslado_costo: document.getElementById("seg_traslado_costo").value,
                seg_traslado_venta: document.getElementById("seg_traslado_venta").value,
                impuesto_import_costo: document.getElementById("impuesto_import_costo").value,
                impuesto_import_venta: document.getElementById("impuesto_import_venta").value,
                fletes_import_costo: document.getElementById("fletes_import_costo").value,
                fletes_import_venta: document.getElementById("fletes_import_venta").value,
                gastos_traslado_costo: document.getElementById("gastos_traslado_costo").value,
                gastos_traslado_venta: document.getElementById("gastos_traslado_venta").value,
                deduccion_ford_costo: document.getElementById("deduccion_ford_costo").value,
                deduccion_ford_venta: document.getElementById("deduccion_ford_venta").value,
                bonificacion_costo: document.getElementById("bonificacion_costo").value,
                bonificacion_venta: document.getElementById("bonificacion_venta").value,

                subtota_costo: document.getElementById("subtota_costo").value,
                subtota_venta: document.getElementById("subtota_venta").value,
                iva_costo: document.getElementById("iva_costo").value,
                iva_venta: document.getElementById("iva_venta").value,
                total_costo: document.getElementById("total_costo").value,
                total_venta: document.getElementById("total_venta").value,

                otros_gastos: document.getElementById("otros_gastos").value,
                gastos_acondi: document.getElementById("gastos_acondi").value,
                ap_fondos_pub: document.getElementById("ap_fondos_pub").value,
                ap_prog_civil: document.getElementById("ap_prog_civil").value,
                cuota_amda: document.getElementById("cuota_amda").value,
                cuota_coparmex: document.getElementById("cuota_coparmex").value,
                cuota_asociacion_ford: document.getElementById("cuota_asociacion_ford").value,
            };

            return newArray;
        }

        function sumatoria_venta() {
            var subtotal = 0;

            //Recuperamos los valores y vamos generando la sumatoria
            var valor_1 = ($("#valor_unidad_venta").val() != "") ? parseFloat($("#valor_unidad_venta").val()) : 0;
            subtotal += valor_1;

            var valor_2 = ($("#equipo_base_venta").val() != "") ? parseFloat($("#equipo_base_venta").val()) : 0;
            subtotal += valor_2;

            var valor_3 = ($("#seg_traslado_venta").val() != "") ? parseFloat($("#seg_traslado_venta").val()) : 0;
            subtotal += valor_3;

            var valor_4 = ($("#impuesto_import_venta").val() != "") ? parseFloat($("#impuesto_import_venta").val()) : 0;
            subtotal += valor_4;

            var valor_5 = ($("#fletes_import_venta").val() != "") ? parseFloat($("#fletes_import_venta").val()) : 0;
            subtotal += valor_5;

            var valor_6 = ($("#gastos_traslado_venta").val() != "") ? parseFloat($("#gastos_traslado_venta").val()) : 0;
            subtotal += valor_6;

            var valor_7 = ($("#deduccion_ford_venta").val() != "") ? parseFloat($("#deduccion_ford_venta").val()) : 0;
            subtotal -= valor_7;

            var valor_8 = ($("#bonificacion_venta").val() != "") ? parseFloat($("#bonificacion_venta").val()) : 0;
            subtotal += valor_8;

            //Sacamos el iva y el total de la operación
            var iva = subtotal * 0.16;
            var total = subtotal * 1.16;

            //Vaciamos los valores finales
            $("#subtota_venta").val(subtotal.toFixed(2));
            $("#iva_venta").val(iva.toFixed(2));
            $("#total_venta").val(total.toFixed(2));
        }

        function sumatoria_costo() {
            var subtotal = 0;

            //Recuperamos los valores y vamos generando la sumatoria
            var valor_1 = ($("#valor_unidad").val() != "") ? parseFloat($("#valor_unidad").val()) : 0;
            subtotal += valor_1;

            var valor_2 = ($("#equipo_base_costo").val() != "") ? parseFloat($("#equipo_base_costo").val()) : 0;
            subtotal += valor_2;

            var valor_3 = ($("#seg_traslado_costo").val() != "") ? parseFloat($("#seg_traslado_costo").val()) : 0;
            subtotal += valor_3;

            var valor_4 = ($("#impuesto_import_costo").val() != "") ? parseFloat($("#impuesto_import_costo").val()) : 0;
            subtotal += valor_4;

            var valor_5 = ($("#fletes_import_costo").val() != "") ? parseFloat($("#fletes_import_costo").val()) : 0;
            subtotal += valor_5;

            var valor_6 = ($("#gastos_traslado_costo").val() != "") ? parseFloat($("#gastos_traslado_costo").val()) : 0;
            subtotal += valor_6;

            var valor_7 = ($("#deduccion_ford_costo").val() != "") ? parseFloat($("#deduccion_ford_costo").val()) : 0;
            subtotal -= valor_7;

            var valor_8 = ($("#bonificacion_costo").val() != "") ? parseFloat($("#bonificacion_costo").val()) : 0;
            subtotal += valor_8;

            //Sacamos el iva y el total de la operación
            var iva = subtotal * 0.16;
            var total = subtotal * 1.16;

            //Vaciamos los valores finales
            $("#subtota_costo").val(subtotal.toFixed(2));
            $("#iva_costo").val(iva.toFixed(2));
            $("#total_costo").val(total.toFixed(2));
        }

        $("#color_id").on('change', function() {
            var clave_color = $("#color_id option:selected").text();
            var limpieza = clave_color.split("]");
            limpieza[0] = limpieza[0].replace("[", "");
            limpieza[0] = limpieza[0].replace(" ", "");

            $("#clave").val(limpieza[0]);
        });

        function costos_validar() {
            var validacion = true;

            //Recuperamos los valores totales
            var total_costo = ($("#total_costo").val() != "") ? parseFloat($("#total_costo").val()) : 0;
            var total_venta = ($("#total_venta").val() != "") ? parseFloat($("#total_venta").val()) : 0;

            if (total_costo >= total_venta) {
                var error = $("#total_venta_error");
                $("#total_venta_error").css("display", "inline-block");
                error.append('<label class="form-text text-danger">Costos incoherentes, verificar montos.</label>');
                validacion = false;
            } else {
                var error = $("#total_venta_error");
                error.empty();
            }

            return validacion;
        }

        //Creamos clave para numero economico
        function formularEconomico() {
            var contenedor = document.getElementById("modelo_id");
            var etiqueta = contenedor.options[contenedor.selectedIndex].text;
            //console.log(etiqueta);

            var datos = etiqueta.split(".");
            var unidad = datos[0].replace(/[[\]\\]/gi, '');
            //recuperar_consecutivo();

            //Formamos el numero economico
            var consecutivo = parseInt($("#consecutivo").val());
            var folio = "";
            consecutivo++;

            if (consecutivo < 10) {
                folio = "N000" + consecutivo;
            } else if ((consecutivo > 9) && (consecutivo < 100)) {
                folio = "N00" + consecutivo;
            } else if ((consecutivo > 99) && (consecutivo < 1000)) {
                folio = "N0" + consecutivo;
            } else {
                folio = "N" + consecutivo;
            }

            var numero_economico = unidad + "" + folio;
            console.log(numero_economico)
            $("#numero_economico").val(numero_economico);
            //console.log(n_economico);
        }

        function recuperar_consecutivo() {

            $.ajax({
                url: base_url + "autos/Recepcion/ultimo_id",
                method: 'post',
                data: {
                    dato: "",
                },
                success: function(resp) {
                    console.log("respuesta: " + resp);

                    if (resp.indexOf("handler           </p>") < 1) {
                        resultado = resp;
                    } else {
                        resultado = "0";
                    }

                    $('#consecutivo').val(resultado);
                    //Cierre de success
                },
                error: function(error) {
                    console.log(error);
                    //Cierre del error
                }
                //Cierre del ajax
            });

            return true;
        }

    </script>
@endsection
