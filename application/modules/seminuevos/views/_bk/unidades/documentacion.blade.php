@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="recepcion_documentacion" data-id="{{ isset($data->id) ? $data->id : '' }}" autocomplete="on" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Usuario que recibe documentación:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->usuario_recibe)){print_r($data->usuario_recibe);}else {if(isset($usuario_recibe)) echo $usuario_recibe;} ?>"  id="usuario_recibe" name="usuario_recibe" placeholder="">
                            <div id="usuario_recibe_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Fecha de recepción:</label>
                            <input type="date" class="form-control" value="<?php if(isset($data->fecha_recepcion)){print_r($data->fecha_recepcion);}else {echo date('Y-m-d');} ?>" id="fecha_recepcion" name="fecha_recepcion" placeholder="">
                            <div id="fecha_recepcion_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for=""># Económico:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->numero_economico)){print_r($data->numero_economico);}else {if(isset($economico)) echo $economico;} ?>" id="numero_economico" name="numero_economico" placeholder="">
                            <div id="numero_economico_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">No. cliente:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->numero_cliente)){print_r($data->numero_cliente);} ?>" id="numero_cliente" name="numero_cliente" placeholder="">
                            <div id="numero_cliente_error" class="invalid-feedback"></div>
                        </div>
                    </div> 
                    <div class="col-md-4">
                        <br>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Regimen Fiscal :</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->regimen_fiscal)){print_r($data->regimen_fiscal);} ?>" id="regimen_fiscal" name="regimen_fiscal" placeholder="">
                            <div id="regimen_fiscal_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Nombre :</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->nombre)){print_r($data->nombre);} ?>" id="nombre" name="nombre" placeholder="">
                            <div id="nombre_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Apellido Paterno:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->apellido_paterno)){print_r($data->apellido_paterno);} ?>" id="apellido_paterno" name="apellido_paterno" placeholder="">
                            <div id="apellido_paterno_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Apellido Materno:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->apellido_materno)){print_r($data->apellido_materno);} ?>" id="apellido_materno" name="apellido_materno" placeholder="">
                            <div id="apellido_materno_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Razón Social:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->nombre_empresa)){print_r($data->nombre_empresa);} ?>"  id="nombre_empresa" name="nombre_empresa" placeholder="">
                            <div id="nombre_empresa_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <h4 style="color: black;text-align: center;font-weight: bold;">Documentación requerida del vehículo:</h4>
                <hr>

                <div class="row moral">
                    <div class="col-md-1" align="center">
                        @if (isset($data->copia_factura_origen))
                            @if ($data->copia_factura_origen != "")
                                <i class="far fa-check-square" style="font-size:25px;color:darkgreen;"></i>
                            @else
                                <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                            @endif
                        @else
                            <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p align="justify" style="text-align: justify; margin-bottom: 0px;color:black;">
                            1.(P. MORAL)  Copia Factura Origen 
                        </p>
                        <br>
                        <input type="hidden" id="doc01_moral_ruta" value="<?php if(isset($data->copia_factura_origen)) echo $data->copia_factura_origen;?>">
                        @if (isset($data->copia_factura_origen))
                            @if ($data->copia_factura_origen == "")
                                <div class="form-group">
                                    <label for="">Seleccionar documento:</label>
                                    <input type="file" class="form-control" name="doc01_moral">
                                    
                                </div>
                            @endif
                        @else
                            <div class="form-group">
                                <label for="">Seleccionar documento:</label>
                                <input type="file" class="form-control" name="doc01_moral">
                            </div>
                        @endif
                    </div>
                    <div class="col-md-3" align="center">
                        <br>
                        @if (isset($data->copia_factura_origen))
                            @if ($data->copia_factura_origen != "")
                                <a href="{{ base_url().$data->copia_factura_origen }}" class="btn btn-primary" target="_blank" type="button" title="Ver Documento">
                                    <i class="fas fa-file-alt" style="font-size: 16px; color: #0f83c9;"></i>
                                </a>
                            @else
                                <button type="button" onclick="cargar_archivos('doc01_moral')" class="btn btn-info">
                                    <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                    Cargar Archivo
                                </button>
                            @endif
                        @else
                            <button type="button" onclick="cargar_archivos('doc01_moral')" class="btn btn-info">
                                <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                Cargar Archivo
                            </button>
                        @endif
                        <div class="loader-spin" style="margin-top: 10px;display: none;" id="doc01_moral_carga"></div>
                        <label id="doc01_moral_error" style="font-weight:bold;"></label>
                    </div>
                </div>

                <div class="row moral">
                    <div class="col-md-1" align="center">
                        @if (isset($data->refacturacion_mylsa))
                            @if ($data->refacturacion_mylsa != "")
                                <i class="far fa-check-square" style="font-size:25px;color:darkgreen;"></i>
                            @else
                                <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                            @endif
                        @else
                            <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p align="justify" style="text-align: justify; margin-bottom: 0px;color:black;">
                            1-A (P. MORAL).  Refacturación a <?php echo NOMBRE_SUCURSAL;?> . 
                            (archivos electrónicos PDF y XML) enviar a <u style="color:blue;">l.ramirez@fordmylsaqueretaro.mx</u> copia a  <u style="color:blue;">i.montoya@fordmylsaqueretaro.mx </u>
                        </p>
                        <br>
                        <input type="hidden" id="doc02_moral_ruta" value="<?php if(isset($data->refacturacion_mylsa)) echo $data->refacturacion_mylsa;?>">
                        @if (isset($data->refacturacion_mylsa))
                            @if ($data->refacturacion_mylsa == "")
                                <div class="form-group">
                                    <label for="">Seleccionar documento:</label>
                                    <input type="file" class="form-control" name="doc02_moral" multiple="" accept="">
                                    
                                </div>
                            @endif
                        @else
                            <div class="form-group">
                                <label for="">Seleccionar documento:</label>
                                <input type="file" class="form-control" name="doc02_moral" multiple="" accept="">
                            </div>
                        @endif
                    </div>
                    <div class="col-md-3" align="center">
                        @if (isset($data->refacturacion_mylsa))
                            @if ($data->refacturacion_mylsa != "")
                                <a href="{{ base_url().$data->refacturacion_mylsa }}" class="btn btn-primary" target="_blank" type="button" title="Ver Documento">
                                    <i class="fas fa-file-alt" style="font-size: 16px; color: #0f83c9;"></i>
                                </a>
                            @else
                                <button type="button" onclick="cargar_archivos('doc02_moral')" class="btn btn-info">
                                <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                Cargar Archivo
                                </button>
                            @endif
                        @else
                            <button type="button" onclick="cargar_archivos('doc02_moral')" class="btn btn-info">
                                <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                Cargar Archivo
                            </button>
                        @endif
                        <div class="loader-spin" style="margin-top: 10px;display: none;" id="doc02_moral_carga"></div>
                        <label id="doc02_moral_error" style="font-weight:bold;"></label>
                    </div>
                </div>

                <div class="row fisica">
                    <div class="col-md-1" align="center">
                        @if (isset($data->factura_original_endosada))
                            @if ($data->factura_original_endosada != "")
                                <i class="far fa-check-square" style="font-size:25px;color:darkgreen;"></i>
                            @else
                                <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                            @endif
                        @else
                            <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p align="justify" style="text-align: justify; margin-bottom: 0px;color:black;">
                            1. (P. FISICA)  Factura original endosada. <br>
                            LEYENDA: Cedo Los Derechos De La Presente Factura a <?php echo NOMBRE_SUCURSAL;?> S.A. De C.V. ,nombre, fecha y firma cliente, (archivos electrónicos PDF y XML) enviar a <u style="color:blue;">l.ramirez@fordmylsaqueretaro.mx</u> copia a  <u style="color:blue;">i.montoya@fordmylsaqueretaro.mx </u>
                        </p>
                        <br>
                        <input type="hidden" id="doc01_fisica_ruta" value="<?php if(isset($data->factura_original_endosada)) echo $data->factura_original_endosada;?>">
                        @if (isset($data->factura_original_endosada))
                            @if ($data->factura_original_endosada == "")
                                <div class="form-group">
                                    <label for="">Seleccionar documento:</label>
                                    <input type="file" class="form-control" name="doc01_fisica" multiple="" accept="">
                                </div>
                            @endif
                        @else
                            <div class="form-group">
                                <label for="">Seleccionar documento:</label>
                                <input type="file" class="form-control" name="doc01_fisica" multiple="" accept="">
                            </div>
                        @endif
                    </div>
                    <div class="col-md-3" align="center">
                        @if (isset($data->factura_original_endosada))
                            @if ($data->factura_original_endosada == "")
                                <button type="button" onclick="cargar_archivos('doc01_fisica')" class="btn btn-info">
                                    <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                    Cargar Archivo
                                </button>
                            @else 
                                <a href="{{ base_url().$data->factura_original_endosada }}" class="btn btn-primary" target="_blank" type="button" title="Ver Documento">
                                    <i class="fas fa-file-alt" style="font-size: 16px; color: #0f83c9;"></i>
                                </a>
                            @endif
                        @else
                            <button type="button" onclick="cargar_archivos('doc01_fisica')" class="btn btn-info">
                                <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                Cargar Archivo
                            </button>
                        @endif
                        <div class="loader-spin" style="margin-top: 10px;display: none;" id="doc01_fisica_carga"></div>
                        <label id="doc01_fisica_error" style="font-weight:bold;"></label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-1" align="center">
                        @if (isset($data->tenencias_pagadas))
                            @if ($data->tenencias_pagadas != "")
                                <i class="far fa-check-square" style="font-size:25px;color:darkgreen;"></i>
                            @else
                                <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                            @endif
                        @else
                            <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p align="justify" style="text-align: justify; margin-bottom: 0px;color:black;">
                            2.  Mínimo 6 ultimas tenencias pagadas (TENENCIA 2019)
                        </p>                        
                        
                        <br>
                        <input type="hidden" id="doc02_fisica_ruta" value="<?php if(isset($data->tenencias_pagadas)) echo $data->tenencias_pagadas;?>">
                        @if (isset($data->tenencias_pagadas))
                            @if ($data->tenencias_pagadas == "")
                                <div class="form-group">
                                    <label for="">Seleccionar documento:</label>
                                    <input type="file" class="form-control" name="doc02_fisica" multiple="" accept="">
                                </div>
                            @endif
                        @else
                            <div class="form-group">
                                <label for="">Seleccionar documento:</label>
                                <input type="file" class="form-control" name="doc02_fisica" multiple="" accept="">
                            </div>
                        @endif
                    </div>
                    <div class="col-md-3" align="center">
                        @if (isset($data->tenencias_pagadas))
                            @if ($data->tenencias_pagadas == "")
                                <button type="button" onclick="cargar_archivos('doc02_fisica')" class="btn btn-info">
                                    <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                    Cargar Archivo
                                </button>
                            @else
                                <a href="{{ base_url().$data->tenencias_pagadas }}" class="btn btn-primary" target="_blank" type="button" title="Ver Documento">
                                    <i class="fas fa-file-alt" style="font-size: 16px; color: #0f83c9;"></i>
                                </a>
                            @endif
                        @else
                            <button type="button" onclick="cargar_archivos('doc02_fisica')" class="btn btn-info">
                                <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                Cargar Archivo
                            </button>
                        @endif
                        <div class="loader-spin" style="margin-top: 10px;display: none;" id="doc02_fisica_carga"></div>
                        <label id="doc02_fisica_error" style="font-weight:bold;"></label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-1" align="center">
                        @if (isset($data->copia_tarjeta_circulacion))
                            @if ($data->copia_tarjeta_circulacion != "")
                                <i class="far fa-check-square" style="font-size:25px;color:darkgreen;"></i>
                            @else
                                <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                            @endif
                        @else
                            <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p align="justify" style="text-align: justify; margin-bottom: 0px;color:black;">
                            3.  Copia Tarjeta de circulación (Realizar tramite de baja)
                        </p>                        
                        
                        <br>
                        <input type="hidden" id="doc03_fisica_ruta" value="<?php if(isset($data->copia_tarjeta_circulacion)) echo $data->copia_tarjeta_circulacion;?>">
                        @if (isset($data->copia_tarjeta_circulacion))
                            @if ($data->copia_tarjeta_circulacion == "")
                                <div class="form-group">
                                    <label for="">Seleccionar documento:</label>
                                    <input type="file" class="form-control" name="doc03_fisica">
                                </div>
                            @endif
                        @else
                            <div class="form-group">
                                <label for="">Seleccionar documento:</label>
                                <input type="file" class="form-control" name="doc03_fisica">
                            </div>
                        @endif
                    </div>
                    <div class="col-md-3" align="center">
                        <br>
                        @if (isset($data->copia_tarjeta_circulacion))
                            @if ($data->copia_tarjeta_circulacion != "")
                                <a href="{{ base_url().$data->copia_tarjeta_circulacion }}" class="btn btn-primary" target="_blank" type="button" title="Ver Documento">
                                    <i class="fas fa-file-alt" style="font-size: 16px; color: #0f83c9;"></i>
                                </a>
                            @else
                                <button type="button" onclick="cargar_archivos('doc03_fisica')" class="btn btn-info">
                                    <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                    Cargar Archivo
                                </button>
                            @endif
                        @else
                            <button type="button" onclick="cargar_archivos('doc03_fisica')" class="btn btn-info">
                                <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                Cargar Archivo
                            </button>
                        @endif
                        <div class="loader-spin" style="margin-top: 10px;display: none;" id="doc03_fisica_carga"></div>
                        <label id="doc03_fisica_error" style="font-weight:bold;"></label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-1" align="center">
                        @if (isset($data->certificado_verificacion_vigente))
                            @if ($data->certificado_verificacion_vigente != "")
                                <i class="far fa-check-square" style="font-size:25px;color:darkgreen;"></i>
                            @else
                                <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                            @endif
                        @else
                            <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p align="justify" style="text-align: justify; margin-bottom: 0px;color:black;">
                            4.  Certificado de verificación vigente
                        </p>                        
                        
                        <br>
                        <input type="hidden" id="doc04_fisica_ruta" value="<?php if(isset($data->certificado_verificacion_vigente)) echo $data->certificado_verificacion_vigente;?>">
                        @if (isset($data->certificado_verificacion_vigente))
                            @if ($data->certificado_verificacion_vigente == "")
                                <div class="form-group">
                                    <label for="">Seleccionar documento:</label>
                                    <input type="file" class="form-control" name="doc04_fisica">
                                </div>
                            @endif
                        @else
                            <div class="form-group">
                                <label for="">Seleccionar documento:</label>
                                <input type="file" class="form-control" name="doc04_fisica">
                            </div>
                        @endif
                    </div>
                    <div class="col-md-3" align="center">
                        @if (isset($data->certificado_verificacion))
                            @if ($data->certificado_verificacion != "")
                                <a href="{{ base_url().$data->certificado_verificacion_vigente }}" class="btn btn-primary" target="_blank" type="button" title="Ver Documento">
                                    <i class="fas fa-file-alt" style="font-size: 16px; color: #0f83c9;"></i>
                                </a>
                            @else
                                <button type="button" onclick="cargar_archivos('doc04_fisica')" class="btn btn-info">
                                    <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                    Cargar Archivo
                                </button>
                            @endif
                        @else
                            <button type="button" onclick="cargar_archivos('doc04_fisica')" class="btn btn-info">
                                <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                Cargar Archivo
                            </button>
                        @endif
                        <div class="loader-spin" style="margin-top: 10px;display: none;" id="doc04_fisica_carga"></div>
                        <label id="doc04_fisica_error" style="font-weight:bold;"></label>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-1" align="center">
                        @if (isset($data->manual_propietario))
                            @if ($data->manual_propietario != "")
                                <i class="far fa-check-square" style="font-size:25px;color:darkgreen;"></i>
                            @else
                                <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                            @endif
                        @else
                            <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p align="justify" style="text-align: justify; margin-bottom: 0px;color:black;">
                            5.   Manual de propietario
                        </p>                        
                        
                        <!--<br>
                        <div class="form-group">
                            <label for="">Seleccionar documento:</label>
                            <input type="file" class="form-control" name="doc05_fisica">
                            <input type="hidden" id="doc05_fisica_ruta" value="<?php if(isset($data->manual_propietario)) echo $data->manual_propietario;?>">
                        </div>-->
                        <input type="hidden" class="form-control" name="doc05_fisica" id="doc05_fisica" value="0">
                    </div>
                    <div class="col-md-3" align="center">
                        @if (isset($data->manual_propietario))
                            @if ($data->manual_propietario != "")
                                @if ($data->manual_propietario == "1")
                                    <i class="fas fa-check" style="color: darkgreen;font-size: 20px;"></i>
                                @else
                                    <i class="far fa-window-close" style="color: red;font-size: 20px;"></i>
                                @endif
                            @else
                                <button type="button" onclick="marcar_documentacion('doc05_fisica')" class="btn btn-default" title="Marcar requerimiento">
                                    <i class="fas fa-check" id="doc05_fisica_btn" style="color: #f7af3e;font-size: 16px;"></i>
                                </button>
                            @endif
                        @else
                            <button type="button" onclick="marcar_documentacion('doc05_fisica')" class="btn btn-default" title="Marcar requerimiento">
                                <i class="fas fa-check" id="doc05_fisica_btn" style="color: #f7af3e;font-size: 16px;"></i>
                            </button>
                        @endif
                        <div class="loader-spin" style="margin-top: 10px;display: none;" id="doc05_fisica_carga"></div>
                        <label id="doc05_fisica_error" style="font-weight:bold;"></label>
                    </div>
                </div>

                <br><br>
                <div class="row">
                    <div class="col-md-1" align="center">
                        @if (isset($data->poliza_garantia))
                            @if ($data->poliza_garantia != "")
                                <i class="far fa-check-square" style="font-size:25px;color:darkgreen;"></i>
                            @else
                                <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                            @endif
                        @else
                            <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p align="justify" style="text-align: justify; margin-bottom: 0px;color:black;">
                            6.  Póliza de garantía
                        </p>                        
                        
                        <br>
                        <input type="hidden" id="doc06_fisica_ruta" value="<?php if(isset($data->poliza_garantia)) echo $data->poliza_garantia;?>">
                        @if (isset($data->poliza_garantia))
                            @if ($data->poliza_garantia == "")
                                <div class="form-group">
                                    <label for="">Seleccionar documento:</label>
                                    <input type="file" class="form-control" name="doc06_fisica">
                                </div>
                            @endif
                        @else
                            <div class="form-group">
                                <label for="">Seleccionar documento:</label>
                                <input type="file" class="form-control" name="doc06_fisica">
                            </div>
                        @endif
                    </div>
                    <div class="col-md-3" align="center">
                        @if (isset($data->poliza_garantia))
                            @if ($data->poliza_garantia != "")
                                <a href="{{ base_url().$data->poliza_garantia }}" class="btn btn-primary" target="_blank" type="button" title="Ver Documento">
                                    <i class="fas fa-file-alt" style="font-size: 16px; color: #0f83c9;"></i>
                                </a>
                            @else
                                <button type="button" onclick="cargar_archivos('doc06_fisica')" class="btn btn-info">
                                    <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                    Cargar Archivo
                                </button>
                            @endif
                        @else
                            <button type="button" onclick="cargar_archivos('doc06_fisica')" class="btn btn-info">
                                <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                Cargar Archivo
                            </button>
                        @endif
                        <div class="loader-spin" style="margin-top: 10px;display: none;" id="doc06_fisica_carga"></div>
                        <label id="doc06_fisica_error" style="font-weight:bold;"></label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-1" align="center">
                        @if (isset($data->cancelacion_poliza_vigente))
                            @if ($data->cancelacion_poliza_vigente != "")
                                <i class="far fa-check-square" style="font-size:25px;color:darkgreen;"></i>
                            @else
                                <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                            @endif
                        @else
                            <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p align="justify" style="text-align: justify; margin-bottom: 0px;color:black;">
                            7.  Cancelación de  Póliza de seguro vigente
                        </p>                        
                        
                        <br>
                        <input type="hidden" id="doc07_fisica_ruta" value="<?php if(isset($data->cancelacion_poliza_vigente)) echo $data->cancelacion_poliza_vigente;?>">
                        @if (isset($data->cancelacion_poliza_vigente))
                            @if ($data->cancelacion_poliza_vigente == "")
                                <div class="form-group">
                                    <label for="">Seleccionar documento:</label>
                                    <input type="file" class="form-control" name="doc07_fisica">
                                </div>
                            @endif
                        @else
                            <div class="form-group">
                                <label for="">Seleccionar documento:</label>
                                <input type="file" class="form-control" name="doc07_fisica">
                            </div>
                        @endif
                    </div>
                    <div class="col-md-3" align="center">
                        @if (isset($data->cancelacion_poliza_vigente))
                            @if ($data->cancelacion_poliza_vigente != "")
                                <a href="{{ base_url().$data->cancelacion_poliza_vigente }}" class="btn btn-primary" target="_blank" type="button" title="Ver Documento">
                                    <i class="fas fa-file-alt" style="font-size: 16px; color: #0f83c9;"></i>
                                </a>
                            @else
                                <button type="button" onclick="cargar_archivos('doc07_fisica')" class="btn btn-info">
                                    <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                    Cargar Archivo
                                </button>
                            @endif
                        @else
                            <button type="button" onclick="cargar_archivos('doc07_fisica')" class="btn btn-info">
                                <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                Cargar Archivo
                            </button>
                        @endif
                        <div class="loader-spin" style="margin-top: 10px;display: none;" id="doc07_fisica_carga"></div>
                        <label id="doc07_fisica_error" style="font-weight:bold;"></label>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-1" align="center">
                        @if (isset($data->duplicado_llaves))
                            @if ($data->duplicado_llaves != "")
                                <i class="far fa-check-square" style="font-size:25px;color:darkgreen;"></i>
                            @else
                                <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                            @endif
                        @else
                            <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p align="justify" style="text-align: justify; margin-bottom: 0px;color:black;">
                            8.  Duplicado de llaves
                        </p>                        
                        
                        <br>
                        <!--<div class="form-group">
                            <label for="">Seleccionar documento:</label>
                            <input type="file" class="form-control" name="doc07_fisica">
                            <input type="hidden" id="doc07_fisica_ruta" value="<?php if(isset($data->duplicado_llaves)) echo $data->duplicado_llaves;?>">
                        </div>-->
                        <input type="hidden" class="form-control" id="doc07a_fisica" name="doc07a_fisica" value="<?php if(isset($data->duplicado_llaves)) echo $data->duplicado_llaves; else echo '0';?>">
                    </div>
                    <div class="col-md-3" align="center">
                         @if (isset($data->duplicado_llaves))
                            @if ($data->duplicado_llaves != "")
                                @if ($data->duplicado_llaves == "1")
                                    <i class="fas fa-check" style="color: darkgreen;font-size: 20px;"></i>
                                @else
                                    <i class="far fa-window-close" style="color: red;font-size: 20px;"></i>
                                @endif
                            @else
                                <button type="button" onclick="marcar_documentacion('doc07a_fisica')" class="btn btn-default" title="Marcar requerimiento">
                                    <i class="fas fa-check" id="doc05_fisica_btn" style="color: #f7af3e;font-size: 16px;"></i>
                                </button>
                            @endif
                        @else
                            <button type="button" onclick="marcar_documentacion('doc07a_fisica')" class="btn btn-default" title="Marcar requerimiento">
                                <i class="fas fa-check" id="doc07a_fisica_btn" style="color: #f7af3e;font-size: 16px;"></i>
                            </button>
                        @endif
                        <div class="loader-spin" style="margin-top: 10px;display: none;" id="doc07a_fisica_carga"></div>
                        <label id="doc07a_fisica_error" style="font-weight:bold;"></label>
                    </div>
                </div>

                <br><br>
                <div class="row">
                    <div class="col-md-1" align="center">
                        @if (isset($data->codigo_radio))
                            @if ($data->codigo_radio != "")
                                <i class="far fa-check-square" style="font-size:25px;color:darkgreen;"></i>
                            @else
                                <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                            @endif
                        @else
                            <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p align="justify" style="text-align: justify; margin-bottom: 0px;color:black;">
                            9.  Código de radio (si aplica).
                        </p>                        
                        
                        <br>
                        <div class="form-group">
                            <!--<label for="">Seleccionar documento:</label>
                            <input type="file" class="form-control" name="doc08_fisica">
                            <input type="hidden" id="doc08_fisica_ruta" value="<?php if(isset($data->codigo_radio)) echo $data->codigo_radio;?>">-->
                            <input type="text" class="form-control" name="doc08_fisica" id="doc08_fisica" value="<?php if(isset($data->codigo_radio)){ echo $data->codigo_radio;} else {echo 'NA';}?>" placeholder="CG056">
                        </div>
                    </div>
                    <div class="col-md-3" align="center">
                        <br>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-1" align="center">
                        @if (isset($data->brillo_seguridad))
                            @if ($data->brillo_seguridad != "")
                                <i class="far fa-check-square" style="font-size:25px;color:darkgreen;"></i>
                            @else
                                <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                            @endif
                        @else
                            <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p align="justify" style="text-align: justify; margin-bottom: 0px;color:black;">
                            10. Birlo de seguridad (si aplica).
                        </p>                        
                        
                        <br>
                        <div class="form-group">
                            <!--<label for="">Seleccionar documento:</label>
                            <input type="file" class="form-control" name="doc10_fisica">
                            <input type="hidden" id="doc10_fisica_ruta" value="<?php if(isset($data->brillo_seguridad)) echo $data->brillo_seguridad;?>">-->
                            <input type="hidden" class="form-control" name="doc10_fisica" id="doc10_fisica" value="<?php if(isset($data->brillo_seguridad)){ echo $data->brillo_seguridad;} else echo 'NA';?>">
                        </div>
                    </div>
                    <div class="col-md-1" align="center">
                        <label for="">SI  </label>
                        <input type="radio" style="transform: scale(1.5);" class="doc10_fisica" name="birlo" value="SI" <?php if(isset($brillo_seguridad)) {if( $brillo_seguridad == "SI") echo "checked"; } ?>>
                    </div>
                    <div class="col-md-1" align="center">
                        <label for="">NO  </label>
                        <input type="radio" style="transform: scale(1.5);" class="doc10_fisica" name="birlo" value="NO" <?php if(isset($brillo_seguridad)) {if( $brillo_seguridad == "NO") echo "checked"; } ?>>
                    </div>
                    <div class="col-md-1" align="center">
                        <label for="">NA  </label>
                        <input type="radio" style="transform: scale(1.5);" class="doc10_fisica" name="birlo" value="NA" <?php if(isset($brillo_seguridad)) {if( $brillo_seguridad == "NA") echo "checked"; }else{echo 'checked';}?>>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-1" align="center">
                        @if (isset($data->control))
                            @if ($data->control != "")
                                <i class="far fa-check-square" style="font-size:25px;color:darkgreen;"></i>
                            @else
                                <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                            @endif
                        @else
                            <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p align="justify" style="text-align: justify; margin-bottom: 0px;color:black;">
                            11. Control (si aplica).
                        </p>                        
                        
                        <br>
                        <div class="form-group">
                            <!--<label for="">Seleccionar documento:</label>
                            <input type="file" class="form-control" name="doc11_fisica">
                            <input type="hidden" id="doc11_fisica_ruta" value="<?php if(isset($data->control)) echo $data->control;?>">-->
                            <input type="hidden" class="form-control" name="doc11_fisica" id="doc11_fisica" value="<?php if(isset($data->control)){ echo $data->control;}else echo 'NA';?>">
                        </div>
                    </div>
                    <div class="col-md-1" align="center">
                        <label for="">SI  </label>
                        <input type="radio" style="transform: scale(1.5);" class="doc11_fisica" name="control" value="SI" <?php if(isset($control)) {if( $control == "SI") echo "checked"; } ?>>
                    </div>
                    <div class="col-md-1" align="center">
                        <label for="">NO  </label>
                        <input type="radio" style="transform: scale(1.5);" class="doc11_fisica" name="control" value="NO" <?php if(isset($control)) {if( $control == "NO") echo "checked"; } ?>>
                    </div>
                    <div class="col-md-1" align="center">
                        <label for="">NA  </label>
                        <input type="radio" style="transform: scale(1.5);" class="doc11_fisica" name="control" value="NA" <?php if(isset($control)) {if( $control == "NA") echo "checked"; }else{echo 'checked';}?>>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-1" align="center">
                        @if (isset($data->audifonos))
                            @if ($data->audifonos != "")
                                <i class="far fa-check-square" style="font-size:25px;color:darkgreen;"></i>
                            @else
                                <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                            @endif
                        @else
                            <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p align="justify" style="text-align: justify; margin-bottom: 0px;color:black;">
                            12. Audífonos (si aplica)
                        </p>                        
                        
                        <br>
                        <div class="form-group">
                            <!--<label for="">Seleccionar documento:</label>
                            <input type="file" class="form-control" name="doc12_fisica">
                            <input type="hidden" id="doc12_fisica_ruta" value="<?php if(isset($data->audifonos)) echo $data->audifonos;?>">-->
                            <input type="hidden" class="form-control" name="doc12_fisica" id="doc12_fisica" value="<?php if(isset($data->audifonos)){ echo $data->audifonos; }else echo 'NA';?>">
                        </div>
                    </div>
                    <div class="col-md-1" align="center">
                        <label for="">SI  </label>
                        <input type="radio" style="transform: scale(1.5);" class="doc12_fisica" name="audifonos" value="SI" <?php if(isset($audifonos)) {if( $audifonos == "SI") echo "checked"; } ?>>
                    </div>
                    <div class="col-md-1" align="center">
                        <label for="">NO  </label>
                        <input type="radio" style="transform: scale(1.5);" class="doc12_fisica" name="audifonos" value="NO" <?php if(isset($audifonos)) {if( $audifonos == "NO") echo "checked"; } ?>>
                    </div>
                    <div class="col-md-1" align="center">
                        <label for="">NA  </label>
                        <input type="radio" style="transform: scale(1.5);" class="doc12_fisica" name="audifonos" value="NA" <?php if(isset($audifonos)) {if( $audifonos == "NA") echo "checked"; }else{echo 'checked';} ?>>
                    </div>
                </div>

                <br>
                <h4 style="color: black;text-align: center;font-weight: bold;">Documentación requerida cliente:</h4>
                <hr>

                <div class="row">
                    <div class="col-md-1" align="center">
                        @if (isset($data->ine_frontal))
                            @if ($data->ine_frontal != "")
                                <i class="far fa-check-square" style="font-size:25px;color:darkgreen;"></i>
                            @else
                                <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                            @endif
                        @else
                            <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p align="justify" style="text-align: justify; margin-bottom: 0px;color:black;">
                            1.  IFE/INE representante legal (Cara Frontal)
                        </p>                        
                        
                        <br>
                        <input type="hidden" id="doc01_cliente_ruta" value="<?php if(isset($data->ine_frontal)) echo $data->ine_frontal;?>">
                        @if (isset($data->ine_frontal))
                            @if ($data->ine_frontal == "")
                                <div class="form-group">
                                    <label for="">Seleccionar documento:</label>
                                    <input type="file" class="form-control" name="doc01_cliente">
                                </div>
                            @endif
                        @else
                            <div class="form-group">
                                <label for="">Seleccionar documento:</label>
                                <input type="file" class="form-control" name="doc01_cliente">
                            </div>
                        @endif
                    </div>
                    <div class="col-md-3" align="center">
                        @if (isset($data->ine_frontal))
                            @if ($data->ine_frontal != "")
                                <a href="{{ base_url().$data->ine_frontal }}" class="btn btn-primary" target="_blank" type="button" title="Ver Documento">
                                    <i class="fas fa-file-alt" style="font-size: 16px; color: #0f83c9;"></i>
                                </a>
                            @else
                                <button type="button" onclick="cargar_archivos('doc01_cliente')" class="btn btn-info">
                                    <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                    Cargar Archivo
                                </button>
                            @endif
                        @else
                            <button type="button" onclick="cargar_archivos('doc01_cliente')" class="btn btn-info">
                                <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                Cargar Archivo
                            </button>
                        @endif
                        <div class="loader-spin" style="margin-top: 10px;display: none;" id="doc01_cliente_carga"></div>
                        <label id="doc01_cliente_error" style="font-weight:bold;"></label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-1" align="center">
                        @if (isset($data->ine_trasera))
                            @if ($data->ine_trasera != "")
                                <i class="far fa-check-square" style="font-size:25px;color:darkgreen;"></i>
                            @else
                                <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                            @endif
                        @else
                            <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p align="justify" style="text-align: justify; margin-bottom: 0px;color:black;">
                            1-A.  IFE/INE representante legal (Cara Trasera)
                        </p>                        
                        
                        <br>
                        <input type="hidden" id="doc02_cliente_ruta" value="<?php if(isset($data->ine_trasera)) echo $data->ine_trasera;?>">
                        @if (isset($data->ine_trasera))
                            @if ($data->ine_trasera == "")
                                <div class="form-group">
                                    <label for="">Seleccionar documento:</label>
                                    <input type="file" class="form-control" name="doc02_cliente">
                                </div>
                            @endif
                        @else
                            <div class="form-group">
                                <label for="">Seleccionar documento:</label>
                                <input type="file" class="form-control" name="doc02_cliente">
                            </div>
                        @endif
                    </div>
                    <div class="col-md-3" align="center">
                        @if (isset($data->ine_trasera))
                            @if ($data->ine_trasera != "")
                                <a href="{{ base_url().$data->ine_trasera }}" class="btn btn-primary" target="_blank" type="button" title="Ver Documento">
                                    <i class="fas fa-file-alt" style="font-size: 16px; color: #0f83c9;"></i>
                                </a>
                            @else
                                <button type="button" onclick="cargar_archivos('doc02_cliente')" class="btn btn-info">
                                    <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                    Cargar Archivo
                                </button>
                            @endif
                        @else
                            <button type="button" onclick="cargar_archivos('doc02_cliente')" class="btn btn-info">
                                <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                Cargar Archivo
                            </button>
                        @endif
                        <div class="loader-spin" style="margin-top: 10px;display: none;" id="doc02_cliente_carga"></div>
                        <label id="doc02_cliente_error" style="font-weight:bold;"></label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-1" align="center">
                        @if (isset($data->curp))
                            @if ($data->curp != "")
                                <i class="far fa-check-square" style="font-size:25px;color:darkgreen;"></i>
                            @else
                                <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                            @endif
                        @else
                            <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p align="justify" style="text-align: justify; margin-bottom: 0px;color:black;">
                            2.  CURP
                        </p>                        
                        
                        <br>
                        <input type="hidden" id="doc03_cliente_ruta" value="<?php if(isset($data->curp)) echo $data->curp;?>">
                        @if (isset($data->curp))
                            @if ($data->curp == "")
                                <div class="form-group">
                                    <label for="">Seleccionar documento:</label>
                                    <input type="file" class="form-control" name="doc03_cliente">
                                </div>
                            @endif
                        @else
                            <div class="form-group">
                                <label for="">Seleccionar documento:</label>
                                <input type="file" class="form-control" name="doc03_cliente">
                            </div>
                        @endif
                    </div>
                    <div class="col-md-3" align="center">
                        @if (isset($data->curp))
                            @if ($data->curp != "")
                                <a href="{{ base_url().$data->curp }}" class="btn btn-primary" target="_blank" type="button" title="Ver Documento">
                                    <i class="fas fa-file-alt" style="font-size: 16px; color: #0f83c9;"></i>
                                </a>
                            @else
                                <button type="button" onclick="cargar_archivos('doc03_cliente')" class="btn btn-info">
                                    <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                    Cargar Archivo
                                </button>
                            @endif
                        @else
                            <button type="button" onclick="cargar_archivos('doc03_cliente')" class="btn btn-info">
                                <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                Cargar Archivo
                            </button>
                        @endif
                        <div class="loader-spin" style="margin-top: 10px;display: none;" id="doc03_cliente_carga"></div>
                        <label id="doc03_cliente_error" style="font-weight:bold;"></label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-1" align="center">
                        @if (isset($data->rfc_sat))
                            @if ($data->rfc_sat != "")
                                <i class="far fa-check-square" style="font-size:25px;color:darkgreen;"></i>
                            @else
                                <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                            @endif
                        @else
                            <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p align="justify" style="text-align: justify; margin-bottom: 0px;color:black;">
                            3.  RFC con homoclave SAT (para persona moral, RFC de la empresa)
                        </p>                        
                        
                        <br>
                        <input type="hidden" id="doc04_cliente_ruta" value="<?php if(isset($data->rfc_sat)) echo $data->rfc_sat;?>">
                         @if (isset($data->rfc_sat))
                            @if ($data->rfc_sat == "")
                                <div class="form-group">
                                    <label for="">Seleccionar documento:</label>
                                    <input type="file" class="form-control" name="doc04_cliente">
                                </div>
                            @endif
                        @else
                            <div class="form-group">
                                <label for="">Seleccionar documento:</label>
                                <input type="file" class="form-control" name="doc04_cliente">
                            </div>
                        @endif
                    </div>
                    <div class="col-md-3" align="center">
                        @if (isset($data->rfc_sat))
                            @if ($data->rfc_sat != "")
                                <a href="{{ base_url().$data->rfc_sat }}" class="btn btn-primary" target="_blank" type="button" title="Ver Documento">
                                    <i class="fas fa-file-alt" style="font-size: 16px; color: #0f83c9;"></i>
                                </a>
                            @else
                                <button type="button" onclick="cargar_archivos('doc04_cliente')" class="btn btn-info">
                                    <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                    Cargar Archivo
                                </button>
                            @endif
                        @else
                            <button type="button" onclick="cargar_archivos('doc04_cliente')" class="btn btn-info">
                                <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                Cargar Archivo
                            </button>
                        @endif
                        <div class="loader-spin" style="margin-top: 10px;display: none;" id="doc04_cliente_carga"></div>
                        <label id="doc04_cliente_error" style="font-weight:bold;"></label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-1" align="center">
                        @if (isset($data->comprobante_domicilio))
                            @if ($data->comprobante_domicilio != "")
                                <i class="far fa-check-square" style="font-size:25px;color:darkgreen;"></i>
                            @else
                                <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                            @endif
                        @else
                            <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p align="justify" style="text-align: justify; margin-bottom: 0px;color:black;">
                            4.  Comprobante de domicilio
                        </p>                        
                        
                        <br>
                        <input type="hidden" id="doc05_cliente_ruta" value="<?php if(isset($data->comprobante_domicilio)) echo $data->comprobante_domicilio;?>">
                        @if (isset($data->comprobante_domicilio))
                            @if ($data->comprobante_domicilio == "")
                                <div class="form-group">
                                    <label for="">Seleccionar documento:</label>
                                    <input type="file" class="form-control" name="doc05_cliente">
                                </div>
                            @endif
                        @else
                            <div class="form-group">
                                <label for="">Seleccionar documento:</label>
                                <input type="file" class="form-control" name="doc05_cliente">
                            </div>
                        @endif
                    </div>
                    <div class="col-md-3" align="center">
                        @if (isset($data->comprobante_domicilio))
                            @if ($data->comprobante_domicilio != "")
                                <a href="{{ base_url().$data->comprobante_domicilio }}" class="btn btn-primary" target="_blank" type="button" title="Ver Documento">
                                    <i class="fas fa-file-alt" style="font-size: 16px; color: #0f83c9;"></i>
                                </a>
                            @else
                                <button type="button" onclick="cargar_archivos('doc05_cliente')" class="btn btn-info">
                                    <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                    Cargar Archivo
                                </button>
                            @endif
                        @else
                            <button type="button" onclick="cargar_archivos('doc05_cliente')" class="btn btn-info">
                                <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                Cargar Archivo
                            </button>
                        @endif
                        <div class="loader-spin" style="margin-top: 10px;display: none;" id="doc05_cliente_carga"></div>
                        <label id="doc05_cliente_error" style="font-weight:bold;"></label>
                    </div>
                </div>

                <div class="row fisica">
                    <div class="col-md-1" align="center">
                        @if (isset($data->constancia_situacion_fiscal))
                            @if ($data->constancia_situacion_fiscal != "")
                                <i class="far fa-check-square" style="font-size:25px;color:darkgreen;"></i>
                            @else
                                <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                            @endif
                        @else
                            <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p align="justify" style="text-align: justify; margin-bottom: 0px;color:black;">
                            5.  CONSTANCIA DE SITUACION FISCAL.
                        </p>                        
                        
                        <br>
                        <input type="hidden" id="doc06_cliente_ruta" value="<?php if(isset($data->constancia_situacion_fiscal)) echo $data->constancia_situacion_fiscal;?>">
                        @if (isset($data->constancia_situacion_fiscal))
                            @if ($data->constancia_situacion_fiscal == "")
                                <div class="form-group">
                                    <label for="">Seleccionar documento:</label>
                                    <input type="file" class="form-control" name="doc06_cliente">
                                </div>
                            @endif
                        @else
                            <div class="form-group">
                                <label for="">Seleccionar documento:</label>
                                <input type="file" class="form-control" name="doc06_cliente">
                            </div>
                        @endif
                    </div>
                    <div class="col-md-3" align="center">
                        @if (isset($data->constancia_situacion_fiscal))
                            @if ($data->constancia_situacion_fiscal != "")
                                <a href="{{ base_url().$data->constancia_situacion_fiscal }}" class="btn btn-primary" target="_blank" type="button" title="Ver Documento">
                                    <i class="fas fa-file-alt" style="font-size: 16px; color: #0f83c9;"></i>
                                </a>
                            @else
                                <button type="button" onclick="cargar_archivos('doc06_cliente')"lass="btn btn-info">
                                    <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                    Cargar Archivo
                                </button>
                            @endif
                        @else
                            <button type="button" onclick="cargar_archivos('doc06_cliente')" class="btn btn-info">
                                <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                Cargar Archivo
                            </button>
                        @endif
                        <div class="loader-spin" style="margin-top: 10px;display: none;" id="doc06_cliente_carga"></div>
                        <label id="doc06_cliente_error" style="font-weight:bold;"></label>
                    </div>
                </div>

                <div class="row moral">
                    <div class="col-md-1" align="center">
                        @if (isset($data->acta_constitutiva))
                            @if ($data->acta_constitutiva != "")
                                <i class="far fa-check-square" style="font-size:25px;color:darkgreen;"></i>
                            @else
                                <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                            @endif
                        @else
                            <i class="far fa-square" style="font-size:25px;color:darkorange;"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p align="justify" style="text-align: justify; margin-bottom: 0px;color:black;">
                            5.  Acta constitutiva.
                        </p>                        
                        
                        <br>
                        <input type="hidden" id="doc07_cliente_ruta" value="<?php if(isset($data->acta_constitutiva)) echo $data->acta_constitutiva;?>">
                        @if (isset($data->acta_constitutiva))
                            @if ($data->acta_constitutiva == "")
                                <div class="form-group">
                                    <label for="">Seleccionar documento:</label>
                                    <input type="file" class="form-control" name="doc07_cliente">
                                </div>
                            @endif
                        @else
                            <div class="form-group">
                                <label for="">Seleccionar documento:</label>
                                <input type="file" class="form-control" name="doc07_cliente">
                            </div>
                        @endif
                    </div>
                    <div class="col-md-3" align="center">
                        @if (isset($data->acta_constitutiva))
                            @if ($data->acta_constitutiva == "")
                                <a href="{{ base_url().$data->acta_constitutiva }}" class="btn btn-primary" target="_blank" type="button" title="Ver Documento">
                                    <i class="fas fa-file-alt" style="font-size: 16px; color: #0f83c9;"></i>
                                </a>
                            @else
                                <button type="button" onclick="cargar_archivos('doc07_cliente')" class="btn btn-info">
                                    <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                    Cargar Archivo
                                </button>
                            @endif
                        @else
                            <button type="button" onclick="cargar_archivos('doc07_cliente')" class="btn btn-info">
                                <i class="fas fa-file-upload" style="color: #f7af3e;font-size: 16px;"></i>
                                Cargar Archivo
                            </button>
                        @endif
                        <div class="loader-spin" style="margin-top: 10px;display: none;" id="doc07_cliente_carga"></div>
                        <label id="doc07_cliente_error" style="font-weight:bold;"></label>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12" align="center">
                        <input type="hidden" class="form-control" value="<?php if(isset($data->id_seminuevo)){print_r($data->id_seminuevo);} else { if(isset($id_cita)) {echo $id_seminuevo;} else {echo '0';} } ?>"  id="catalogo_seminuevo" placeholder="">
                        <br>
                        @if (isset($data->id))
                            <button type="button" id="editar_registro" class="btn btn-success col-md-4"> Actualizar </button>
                        @else
                            <button type="button" id="guardar_registro" class="btn btn-success col-md-4"> Guardar </button>
                        @endif

                        <input type="hidden" id="consecutivo" value="<?php if(isset($consecutivo->id)) {echo $consecutivo->id;}else{echo '0';} ?>">
                        
                        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
                    </div>
                </div>
                        
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ base_url('js/seminuevos/control_documentos.js') }}"></script>

    <script type="">
        //Para guardar registro por primera vez
        $("#guardar_registro").on('click', function() {
            $(".invalid-feedback").html("");

                ajax.post('api/seminuevos/control-documentos/documentacion', procesarRegistro(), function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }

                    //var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
                    //var alerta = (headers.status != 200) ? "warning" : "success";
                    utils.displayWarningDialog("Proceso completado con éxito", "success", function(data) {
                        return window.location.href = base_url + 'autos/Recepcion/index';
                    })
                })
        });

        //Para actualizar un registro
        $("#editar_registro").on('click', function() {
            $(".invalid-feedback").html("");
            var id = $("#recepcion_docmentos").data('id');

            ajax.put('api/seminuevos/control-documentos/documentacion/'+id, procesarRegistro(), function(response, headers) {
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }
                
                //var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
                utils.displayWarningDialog("Proceso completado con éxito", "success", function(data) {
                    return window.location.href = base_url + 'autos/Recepcion/index';
                })
            })
        });

        //Armamos el formulario a enviar
        let procesarRegistro = function() {
            let form = $('#recepcion_docmentos').serializeArray();
            let newArray = {
                usuario_recibe : document.getElementById("usuario_recibe").value,
                fecha_recepcion : document.getElementById("fecha_recepcion").value,
                id_seminuevo : document.getElementById("catalogo_seminuevo").value,
                numero_economico : document.getElementById("numero_economico").value,
                numero_cliente : document.getElementById("numero_cliente").value,
                regimen_fiscal : document.getElementById("regimen_fiscal").value,
                nombre : document.getElementById("nombre").value,
                apellido_paterno : document.getElementById("apellido_paterno").value,
                apellido_materno : document.getElementById("apellido_materno").value,
                nombre_empresa : document.getElementById("nombre_empresa").value,

                copia_factura_origen : document.getElementById("doc01_moral_ruta").value,
                refacturacion_mylsa : document.getElementById("doc02_moral_ruta").value,
                factura_original_endosada : document.getElementById("doc01_fisica_ruta").value,
                tenencias_pagadas : document.getElementById("doc02_fisica_ruta").value,
                copia_tarjeta_circulacion : document.getElementById("doc03_fisica_ruta").value,
                certificado_verificacion_vigente : document.getElementById("doc04_fisica_ruta").value,

                manual_propietario : document.getElementById("doc05_fisica").value,
                poliza_garantia : document.getElementById("doc06_fisica_ruta").value,
                cancelacion_poliza_vigente : document.getElementById("doc07_fisica_ruta").value,
                duplicado_llaves : document.getElementById("doc07a_fisica").value,
                codigo_radio : document.getElementById("doc08_fisica").value,
                brillo_seguridad : document.getElementById("doc10_fisica").value,
                control : document.getElementById("doc11_fisica").value,
                audifonos : document.getElementById("doc12_fisica").value,

                ine_frontal : document.getElementById("doc01_cliente_ruta").value,
                ine_trasera : document.getElementById("doc02_cliente_ruta").value,
                curp : document.getElementById("doc03_cliente_ruta").value,
                rfc_sat : document.getElementById("doc04_cliente_ruta").value,
                comprobante_domicilio : document.getElementById("doc05_cliente_ruta").value,
                constancia_situacion_fiscal : document.getElementById("doc06_cliente_ruta").value,
                acta_constitutiva : document.getElementById("doc07_cliente_ruta").value,
                
            };

            return newArray;
        }

        function cargar_archivos(campo_archivo) {
            var archivo = $("input[name='"+campo_archivo+"']");

            //Comprobamos que exista un archivo para cargar
            if ((archivo[0].files).length > 0) {
                $("#"+campo_archivo+"_carga").css("display","inline-block");
                $("#"+campo_archivo+"_error").text("");

                var paquete_archivo = new FormData();
                paquete_archivo.append('archivo', $("input[name='"+campo_archivo+"']")[0].files[0]);

                guardar_archivo(paquete_archivo,campo_archivo);

            //Si no se detecta un archivo mandamos un mensaje
            }else{
                $("#"+campo_archivo+"_error").css("color","red");
                $("#"+campo_archivo+"_error").text("No se selecciono ningun archivo a cargar");
            }
        }

        function guardar_archivo(paquete_archivo,campo_input){
            $.ajax({
                url: base_url+"seminuevos/ControlDocumentos/cargar_documentos",
                type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
                contentType: false,
                data: paquete_archivo, // Al atributo data se le asigna el objeto FormData.
                processData: false,
                cache: false,
                success:function(resp){
                    console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                        if (resp != "") {
                            $("#"+campo_input+"_ruta").val(resp);
                            $("#"+campo_input+"_error").css("color","darkgreen");
                            $("#"+campo_input+"_error").text("Archivo cargado con exito");
                        }else{
                            $("#"+campo_input+"_error").css("color","red");
                            $("#"+campo_input+"_error").text("No se pudo cargar el archivo");
                        }
                        
                    }else {
                        $("#"+campo_input+"_error").css("color","red");
                        $("#"+campo_input+"_error").text("Problemas al cargar el archivo");
                    }

                    $("#"+campo_input+"_carga").css("display","none");
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                    
                    $("#"+campo_input+"_error").css("color","red");
                    $("#"+campo_input+"_error").text("Problemas al enviar el archivo");
                //Cierre del error
                }
            //Cierre del ajax
            });
        }

        function marcar_documentacion(campo_archivo) {
            //Recuperamos el valor actual
            var valor_actual = $("input[name='"+campo_archivo+"']").val();

            if (valor_actual == "1") {
                $("input[name='"+campo_archivo+"']").val("0");
                $("#"+campo_archivo+"_btn").css("color","red");
                $("#"+campo_archivo+"_btn").removeAttr("fas fa-check");
                $("#"+campo_archivo+"_btn").addClass('far fa-window-close');
            } else {
                $("input[name='"+campo_archivo+"']").val("1");
                $("#"+campo_archivo+"_btn").css("color","darkgreen");
                $("#"+campo_archivo+"_btn").addClass("fas fa-check");
                $("#"+campo_archivo+"_btn").removeAttr('far fa-window-close');
            }
        }
    </script>
@endsection