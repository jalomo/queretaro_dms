@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

	<div class="row">
		<div class="col-md-12" style="border:2px solid yellow;background-color: white; margin:10px;" align="center">
			<div class="row">
		        <div class="col-md-3" align="center">
		        	<br>
					<img src="<?php echo base_url()?>img/logo.png" style="width: 200px;">
		        </div>
		        <div class="col-md-6" align="center">
					<strong>
		                <span style="color: darkblue;" style="">
						<?php echo NOMBRE_SUCURSAL; ?> , S.A. DE C.V.
		                </span>
		            </strong>

		            <p class="justify" style="text-align: center;color:black;">
		                AV. CONSTITUYENTES #42, COL. VILLAS DEL SOL, QUERETARO, QRO.<br>
		                Tels. (442) 238-7400, EXT.411, Fax. (442) 238-7400   R.F.C. <?php echo RFC_RECEPTOR; ?><br>
		                Email:  m.cristina@fordmylsaqueretaro.mx / www.fordmylsaqueretaro.mx <br>
		                Horario de Recepción y Entrega de Unidades: Lunes a Viernes de 7 AM a 6 PM Sábado 7 AM a 1 PM <br>
		                Horario de Caja Lunes a Viernes 8 AM a 8 PM Sábado 9 AM a 3 PM<br>
		            </p>
		        </div>
		        <div class="col-md-3" style="padding-top: 30px;color: white;">
					<?php echo isset($data->dia) ? $data->dia : date("d") ?> DE <?php echo isset($data->mes) ? strtoupper($meses[((int)$data->mes-1)]) : strtoupper($meses[(date("m")-1)]) ?> DEL <?php echo isset($data->anio) ? $data->anio : date("Y") ?> 
					
					<br>
					<u style="font-weight: bold;color:darkblue;"> <?php echo isset($data->folio) ? $data->folio : "N° 000001" ?></u>
		        </div>
			</div>
			
			<div class="row">
		        <div class="col-md-12">
					<h3 style="color:black;text-align: center;">
						CARTA FACTURA
					</h3>

					<br>
					<p class="justify" style="text-align: justify;color:black;font-weight: lighter;">
		                Por medio del presente hacemos constar que con fecha 
		                <u style="font-weight: bold;"> 
		                	<?php echo isset($data->dia) ? $data->dia : date("d") ?> DE <?php echo isset($data->mes) ? strtoupper($meses[((int)$data->mes-1)]) : strtoupper($meses[(date("m")-1)]) ?> DEL <?php echo isset($data->anio) ? $data->anio : date("Y") ?> 
		                </u> 
		                el Sr(a). <u style="font-weight: bold;"> <?php echo isset($data->cliente) ? $data->cliente : "Cliente demo pre-factura" ?> </u> con R.F.C. <u style="font-weight: bold;"><?php echo isset($data->rfc) ? $data->rfc : "XXXXJDUR8979" ?></u> con domicilio en <u style="font-weight: bold;"><?php echo isset($data->domicilio) ? $data->domicilio : "Avenida siempre viva #45" ?></u>, ha celebrado contrato de compra-venta sobre un vehículo SEMINUEVO, con la factura N° <u style="font-weight: bold;"><?php echo isset($data->factura) ? $data->factura : "SEMI 2414" ?></u>, N° de inventario <u style="font-weight: bold;"><?php echo isset($data->inventario) ? $data->inventario : "10109" ?></u> y con el pedido N° <u style="font-weight: bold;"><?php echo isset($data->pedido) ? $data->pedido : "MQ1010" ?></u>, la siguiente unidad: 
		            </p>
		        </div>
		    </div>

			<br>
		    <div class="row">
		        <div class="col-md-12">
					<table style="width: 90%;margin: 5%;color:black;">
						<tr>
							<td style="width: 20%;">
								MARCA:
							</td>
							<td style="width: 30%;">
								<?php echo isset($data->marca) ? strtoupper($data->marca) : "FORD" ?>
							</td>
							<td style="width: 20%;">
								N° CILINDROS:
							</td>
							<td style="width: 20%;">
								<?php echo isset($data->cilindors) ? strtoupper($data->cilindors) : "8" ?>
							</td>
						</tr>
						<tr>
							<td style="width: 20%;">
								MODELO:
							</td>
							<td style="width: 30%;">
								<?php echo isset($data->modelo) ? strtoupper($data->modelo) : "2019" ?>
							</td>
							<td style="width: 20%;">
								CAPACIDAD:
							</td>
							<td style="width: 20%;">
								<?php echo isset($data->capacidad) ? strtoupper($data->capacidad) : "6" ?>
							</td>
						</tr>
						<tr>
							<td style="width: 20%;">
								TIPO:
							</td>
							<td style="width: 30%;">
								<?php echo isset($data->subcategoria) ? strtoupper($data->subcategoria) : "F-150 XL CREW" ?>
							</td>
							<td style="width: 20%;">
								N° DE PUERTAS:
							</td>
							<td style="width: 20%;">
								<?php echo isset($data->puertas) ? strtoupper($data->puertas) : "4" ?>
							</td>
						</tr>
						<tr>
							<td style="width: 20%;">
								CATALOGO:
							</td>
							<td style="width: 30%;">
								<?php echo isset($data->catalogo) ? strtoupper($data->catalogo) : "K183" ?>
							</td>
							<td style="width: 20%;">
								COMBUSTIBLE:
							</td>
							<td style="width: 20%;">
								<?php echo isset($data->combustible) ? strtoupper($data->combustible) : "GASOLINA" ?>
							</td>
						</tr>
						<tr>
							<td style="width: 20%;">
								COLOR:
							</td>
							<td style="width: 30%;">
								<?php echo isset($data->color) ? strtoupper($data->color) : "ROJO MANZANA" ?>
							</td>
							<td colspan="2">
								
							</td>
						</tr>
						<tr>
							<td style="width: 20%;">
								MOTOR:
							</td>
							<td style="width: 30%;">
								<?php echo isset($data->motor) ? strtoupper($data->motor) : "KKD2445" ?>
							</td>
							<td colspan="2">
								
							</td>
						</tr>
						<tr>
							<td style="width: 20%;">
								SERIE:
							</td>
							<td style="width: 30%;">
								<?php echo isset($data->serie) ? strtoupper($data->serie) : "XXXIOSOD30063435" ?>
							</td>
							<td colspan="2">
								
							</td>
						</tr>
						<tr>
							<td style="width: 20%;">
								CLAVE VEHÍCULAR:
							</td>
							<td style="width: 30%;">
								<?php echo isset($data->clave) ? strtoupper($data->clave) : "1010149374" ?>
							</td>
							<td colspan="2">
								
							</td>
						</tr>
						<tr>
							<td style="width: 20%;">
								PRECIO:
							</td>
							<td style="width: 30%;">
								$<?php echo isset($data->precio) ? strtoupper($data->precio) : "210,000" ?>
							</td>
							<td colspan="2">
								(00/100 MN)
							</td>
						</tr>
						<tr>
							<td style="width: 20%;">
								PEDIMENTO:
							</td>
							<td style="width: 30%;">
								<?php echo isset($data->pedimento) ? strtoupper($data->pedimento) : "000000000000000000" ?>
							</td>
							<td style="width: 20%;">
								DE FECHA:
							</td>
							<td style="width: 20%;">
								<?php echo isset($data->fecha) ? strtoupper($data->fecha) : date("d/m/Y") ?>
							</td>
						</tr>
						<tr>
							<td style="width: 20%;">
								ADUANA:
							</td>
							<td style="width: 30%;">
								<?php echo isset($data->aduana) ? strtoupper($data->aduana) : "PIEDRAS NEGRAS **" ?>
							</td>
							<td colspan="2">
								
							</td>
						</tr>
					</table>
		        </div>
		    </div>

		    <br>
		    <div class="row">
		        <div class="col-md-12">
		        	<p class="justify" style="text-align: justify;color:black;font-weight: lighter;">
		        		ESTE DOCUMENTO NO ES TITULO DE PROPIEDAD Y SOLO ES VALIDO PARA TRAMITES DE SOLICITUD DE PERMISO ANTE LA DIRECCIÓN GENERAL DE TRANSITO.

		        		<br><br>
		        		ASI MISMO SE MANIFIESTA QUE ESTE DISTRIBUIDOR TIENE LA CAPACIDAD LEGAR PARA VENDER VEHÍCULOS IMPORTADOS POR AUTOS Y ACCESORIOS S.A. DE C.V.
					</p>

					<br><br>
					<p class="justify" style="text-align: center;color:black;font-weight: lighter;">

						ATENTAMENTE

						<br><br>
						<br><br>
						AUTOS Y ACCESORIOS, S.A. DE C.V.
					</p>

					<br><br>
					<p class="justify" style="text-align: center;color:black;font-weight: lighter;">
						ESTA CARTA FACTURA NO ES ENDOSABLE
						<br>
						Este documento no es válido si presenta alteración alguna
					</p>
		        </div>
		    </div>
		</div>
	</div>

	<br>
    <div class="row">
        <div class="col-md-12" align="center">
            <br>
            <button type="button" id="inprimir" class="btn btn-success">Imprimir</button>
        </div>
    </div>

    <br><br>

</div>
@endsection

@section('scripts')
<!--<script src="{{ base_url('js/facturas/index.js') }}"></script>-->
@endsection