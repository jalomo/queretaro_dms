@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="salida_seminuevo"> <!-- enctype="multipart/form-data"  -->
				<div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="">Usuario que entrega:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->salida_usuario)){print_r($data->salida_usuario);} ?>"  id="salida_usuario" name="salida_usuario" placeholder="">
                            <div id="salida_usuario_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Fecha de entrega:</label>
                            <input type="date" class="form-control" min="<?= date('Y-m-d'); ?>" value="<?php if(isset($data->salida_fecha)){print_r($data->salida_fecha);}else {echo date('Y-m-d');} ?>" id="salida_fecha" name="salida_fecha" placeholder="">
                            <div id="salida_fecha_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="">No de pedido:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->salida_pedido)){print_r($data->salida_pedido);} ?>"  id="salida_pedido" name="salida_pedido" placeholder="">
                            <div id="salida_pedido_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <br>
                <h3 style="text-align: center;">Datos de la unidad</h3>
                <hr>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Categoria:</label>
                            <select class="form-control" id="salida_categoria" name="salida_categoria" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                            </select>
                            <div id="salida_categoria_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Sub-categoria:</label>
                            <select class="form-control" id="salida_subcategoria" name="salida_subcategoria" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                            </select>
                            <div id="salida_subcategoria_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Marca:</label>
                            <select class="form-control" id="salida_marca" name="salida_marca" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                            </select>
                            <div id="salida_marca_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Modelo:</label>
                            <select class="form-control" id="salida_modelo" name="salida_modelo" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                            </select>
                            <div id="no_identificacion_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Año:</label>
                            <select class="form-control" id="salida_anio" name="salida_anio" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                <option value="2020">2020</option>
                                <option value="2019">2019</option>
                                <option value="2018">2018</option>
                            </select>
                            <div id="salida_anio_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Color:</label>
                            <select class="form-control" id="salida_color" name="salida_color" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                            </select>
                            <div id="salida_color_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Motor:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->salida_motor)){print_r($data->salida_motor);} ?>"  id="salida_motor" name="salida_motor" placeholder="">
                            <div id="salida_motor_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Transmisión:</label>
                            <!--<input type="text" class="form-control" value="<?php if(isset($data->salida_transmision)){print_r($data->salida_transmision);} ?>"  id="salida_transmision" name="salida_transmision" placeholder="">-->
                            <select class="form-control" id="salida_transmision" name="salida_transmision" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                <option value="AUTOMÁTICA" <?php if(isset($data->salida_transmision)){ if($data->salida_transmision == "AUTOMÁTICA") echo "selected";} ?> >AUTOMÁTICA </option>
                                <option value="ESTÁNDAR" <?php if(isset($data->salida_transmision)){ if($data->salida_transmision == "ESTÁNDAR") echo "selected";} ?> >ESTÁNDAR </option>
                                <option value="CVT" <?php if(isset($data->salida_transmision)){ if($data->salida_transmision == "CVT") echo "selected";} ?> >CVT </option>
                                <option value="SEMI-AUTOMÁTICA" <?php if(isset($data->salida_transmision)){ if($data->salida_transmision == "SEMI-AUTOMÁTICA") echo "selected";} ?> >SEMI-AUTOMÁTICA </option>
                            </select>
                            <div id="salida_transmision_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>   
                
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="">VIN:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->salida_vin)){print_r($data->salida_vin);} ?>"  id="salida_vin" name="salida_vin" placeholder="">
                            <div id="salida_vin_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Serie Corta:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->salida_vincorto)){print_r($data->salida_vincorto);} ?>"  id="salida_vincorto" name="salida_vincorto" placeholder="">
                            <div id="salida_vincorto_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

            	<div class="row">
                    <div class="col-md-12" align="center">
                        <br>
                        <button type="button" id="guardar_formulario" class="btn btn-success">Guardar</button>
                        
                        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
                    </div>
                </div>
                        
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection