@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : ''; ?>
        </li>
        <li class="breadcrumb-item "><?php echo isset($subtitulo) ? $subtitulo : ''; ?>
        </li>
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
        </li>
    </ol>
    <div class="row mb-5">
        @include('formatos/partial_firmas_encabezado')
    </div>
    <div class="row mt-2">
        <div class="col-md-4 text-center">
            @include('formatos/partial_carroceria')
        </div>
        <div class="col-md-4">
            @include('formatos/partial_interiores')
        </div>
        <div class="col-md-4">
            @include('formatos/partial_cajuela')
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-4 text-center">
            @include('formatos/partial_exteriores_documentacion')
        </div>
        <div class="col-md-4 text-center">
            <input type="hidden" id="id_unidad" name="id_unidad" value="{{ $id_unidad }}">
        </div>
        <div class="col-md-4 text-center"></div>
    </div>

    <div class="row">
        <a target="_blank" href="{{ site_url('seminuevos/reportes/generarFormatoInventario/'.$id_unidad) }}" class="btn btn-primary">
            <i class="fa fa-print"></i> Generar formato
        </a>

        <button onclick="guardar()" class="btn btn-primary ml-3">
            <i class="fa fa-save"></i> Guardar
        </button>
    </div>
</div>
<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title" id="firmaDigitalLabel"></h4>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" class="text-center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="firma_actual" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
<script src="{{ base_url('js/seminuevos/reportes/firmas.js') }}"></script>
<script>
    const get_checked_items = () => {
        let items_check = document.getElementsByName('indicadores_activados[]');
        let value = '';
        for (let i = 0; i <= items_check.length - 1; i++) {
            if (items_check[i].checked) {
                value += $(items_check[i]).val() + ',';
            }
        }

        return value;
    }


    const guardar = () => {
        let formdata = {
            id_unidad: document.getElementById('id_unidad').value ? document.getElementById('id_unidad').value : '',
            dias_habiles: document.getElementById('dias_habiles').value ? document.getElementById('dias_habiles').value : '',
            firma_elabora: document.getElementById('firma_elabora').value ? document.getElementById('firma_elabora').value : '',
            fecha_elaboracion: document.getElementById('fecha_elaboracion').value ? document.getElementById('fecha_elaboracion').value : '',
            fecha_aceptacion: document.getElementById('fecha_aceptacion').value ? document.getElementById('fecha_aceptacion').value : '',
            firma_consumidor: document.getElementById('firma_consumidor').value ? document.getElementById('firma_consumidor').value : '',
            herramienta: document.querySelector('input[name="herramienta"]:checked') ? document.querySelector('input[name="herramienta"]:checked').value : '',
            gato_llave: document.querySelector('input[name="gato_llave"]:checked') ? document.querySelector('input[name="gato_llave"]:checked').value : '',
            reflejantes: document.querySelector('input[name="reflejantes"]:checked') ? document.querySelector('input[name="reflejantes"]:checked').value : '',
            cables: document.querySelector('input[name="cables"]:checked') ? document.querySelector('input[name="cables"]:checked').value : '',
            extintor: document.querySelector('input[name="extintor"]:checked') ? document.querySelector('input[name="extintor"]:checked').value : '',
            llanta_refaccion: document.querySelector('input[name="llanta_refaccion"]:checked') ? document.querySelector('input[name="llanta_refaccion"]:checked').value : '',
            carroceria_img: document.getElementById('carroceria_img').value ? document.getElementById('carroceria_img').value : '',
            poliza_garantia: document.querySelector('input[name="poliza_garantia"]:checked') ? document.querySelector('input[name="poliza_garantia"]:checked').value : '',
            seguro_rines: document.querySelector('input[name="seguro_rines"]:checked') ? document.querySelector('input[name="seguro_rines"]:checked').value : '',
            seguro_rines_documentacion: document.querySelector('input[name="seguro_rines_documentacion"]:checked') ? document.querySelector('input[name="seguro_rines_documentacion"]:checked').value : '',
            certificado_verificacion: document.querySelector('input[name="certificado_verificacion"]:checked') ? document.querySelector('input[name="certificado_verificacion"]:checked').value : '',
            tarjeta_circulacion: document.querySelector('input[name="tarjeta_circulacion"]:checked') ? document.querySelector('input[name="tarjeta_circulacion"]:checked').value : '',
            nivel_gasolina: document.getElementById('nivel_gasolina').value ? document.getElementById('nivel_gasolina').value : '',
            llavero: document.querySelector('input[name="llavero"]:checked') ? document.querySelector('input[name="llavero"]:checked').value : '',
            indicadores_activados: get_checked_items() ? get_checked_items() : '',
            indicadores: document.querySelector('input[name="indicadores"]:checked') ? document.querySelector('input[name="indicadores"]:checked').value : '',
            rociador: document.querySelector('input[name="rociador"]:checked') ? document.querySelector('input[name="rociador"]:checked').value : '',
            claxon: document.querySelector('input[name="claxon"]:checked') ? document.querySelector('input[name="claxon"]:checked').value : '',
            luces_del: document.querySelector('input[name="luces_del"]:checked') ? document.querySelector('input[name="luces_del"]:checked').value : '',
            luces_tras: document.querySelector('input[name="luces_tras"]:checked') ? document.querySelector('input[name="luces_tras"]:checked').value : '',
            luces_stop: document.querySelector('input[name="luces_stop"]:checked') ? document.querySelector('input[name="luces_stop"]:checked').value : '',
            radio: document.querySelector('input[name="radio"]:checked') ? document.querySelector('input[name="radio"]:checked').value : '',
            pantallas: document.querySelector('input[name="pantallas"]:checked') ? document.querySelector('input[name="pantallas"]:checked').value : '',
            ac: document.querySelector('input[name="ac"]:checked') ? document.querySelector('input[name="ac"]:checked').value : '',
            encendedor: document.querySelector('input[name="encendedor"]:checked') ? document.querySelector('input[name="encendedor"]:checked').value : '',
            vidrios: document.querySelector('input[name="vidrios"]:checked') ? document.querySelector('input[name="vidrios"]:checked').value : '',
            espejos: document.querySelector('input[name="espejos"]:checked') ? document.querySelector('input[name="espejos"]:checked').value : '',
            seguros_electricos: document.querySelector('input[name="seguros_electricos"]:checked') ? document.querySelector('input[name="seguros_electricos"]:checked').value : '',
            disco_compato: document.querySelector('input[name="disco_compato"]:checked') ? document.querySelector('input[name="disco_compato"]:checked').value : '',
            asiento_vestidura: document.querySelector('input[name="asiento_vestidura"]:checked') ? document.querySelector('input[name="asiento_vestidura"]:checked').value : '',
            tapetes: document.querySelector('input[name="tapetes"]:checked') ? document.querySelector('input[name="tapetes"]:checked').value : '',
            tapones_rueda: document.querySelector('input[name="tapones_rueda"]:checked') ? document.querySelector('input[name="tapones_rueda"]:checked').value : '',
            goma_limpiadores: document.querySelector('input[name="goma_limpiadores"]:checked') ? document.querySelector('input[name="goma_limpiadores"]:checked').value : '',
            antena: document.querySelector('input[name="antena"]:checked') ? document.querySelector('input[name="antena"]:checked').value : '',
            tapon_gasolina: document.querySelector('input[name="tapon_gasolina"]:checked') ? document.querySelector('input[name="tapon_gasolina"]:checked').value : '',
            cuales: document.getElementById('cuales').value ? document.getElementById('cuales').value : '',
            reporte_algo_mas: document.getElementById('reporte_algo_mas').value ? document.getElementById('reporte_algo_mas').value : '',
            articulos_personales: document.querySelector('input[name="articulos_personales"]:checked') ? document.querySelector('input[name="articulos_personales"]:checked').value : ''
        }

        ajax.post(`api/inventario-unidades/guardarformulario`, formdata, (data, headers) => {
            if (headers.status == 400) {
                return utils.displayWarningDialog("Faltan datos por llenar!!",
                    "warning",
                    function(data) {});
            }

            utils.displayWarningDialog("formato actualizado", "success", function(data) {
                return window.location.href = base_url + `autos/ventas/index`;
            })
        }, false, false)
    }
</script>
@endsection