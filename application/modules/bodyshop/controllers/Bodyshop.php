<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Bodyshop extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'url', 'date', 'general'));
        $this->load->library('curl');
        date_default_timezone_set('America/Mexico_City');
    }
    public function listado()
    {
        $data['info'] = procesarResponseApiJsonToArray($this->curl->curlPost('api/logistica/master/get-all', []));
        $data['titulo'] = "Listado bodyshop";
        $data['bitacora'] = "inicial";
        $this->blade->render('listado', $data);
    }
    public function agregar($id = 0)
    {
        if ($id == 0) {
            $info = new Stdclass();
            $info_cita = new Stdclass();
            $array_horarios = [];
        } else {
            $info_cita = new Stdclass();
            //$info_cita = procesarResponseApiJsonToArray($this->curl->curlGet('api/telemarketing/citas-ventas/' . $id));
            $info = procesarResponseApiJsonToArray($this->curl->curlGet('api/logistica/' . $id));
        }

        $nombre_proyectos = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-proyectos-bodyshop'));
        $data['id_proyecto'] = form_dropdown('id_proyecto', array_combos($nombre_proyectos, 'id', 'proyecto', TRUE), set_value('id_proyecto', exist_obj($info, 'id_proyecto')), 'class="form-control busqueda" id="id_proyecto"');
        $data['descripcion'] = form_input('descripcion', set_value('descripcion', exist_obj($info, 'descripcion')), 'class="form-control" id="descripcion"');
        $data['numero_siniestro'] = form_input('numero_siniestro', set_value('numero_siniestro', exist_obj($info, 'numero_siniestro')), 'class="form-control" id="numero_siniestro"');
        $data['numero_poliza'] = form_input('numero_poliza', set_value('numero_poliza', exist_obj($info, 'numero_poliza')), 'class="form-control" id="numero_poliza"');
        $anios = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-anio'));
        $data['id_anio'] = form_dropdown('id_anio', array_combos($anios, 'id', 'nombre', TRUE), set_value('id_anio', exist_obj($info, 'id_anio')), 'class="form-control busqueda" id="id_anio"');
        $data['placas'] = form_input('placas', set_value('placas', exist_obj($info, 'placas')), 'class="form-control" id="placas"');
        $colores = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-colores'));
        $data['id_color'] = form_dropdown('id_color', array_combos($colores, 'id', 'nombre', TRUE), set_value('id_color', exist_obj($info, 'id_color')), 'class="form-control busqueda" id="id_color"');
        $unidades = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-modelos'));
        $data['id_modelo'] = form_dropdown('id_modelo', array_combos($unidades, 'id', 'nombre', TRUE), set_value('id_modelo', exist_obj($info, 'id_modelo')), 'class="form-control busqueda" id="id_modelo"');
        $data['serie'] = form_input('serie', set_value('serie', exist_obj($info, 'serie')), 'class="form-control" id="serie" maxlength="17"');
        $cita = set_value('cita', exist_obj($info, 'cita'));
        $data['cita'] = form_checkbox('cita', $cita, ($cita) ? true : false, 'id="cita"');
        $asesores = procesarResponseApiJsonToArray($this->curl->curlPost('api/usuarios/usuario-by-rol', [
            'rol_id' => 13
        ]));
        $data['id_asesor'] = form_dropdown('id_asesor', array_combos($asesores, 'id', 'nombre', TRUE), set_value('id_asesor', exist_obj($info, 'id_asesor')), 'class="form-control busqueda" id="id_asesor"');

        //Días para la cita
        $fecha = date('Y-m-d');
        $ffin = strtotime('+1 month', strtotime($fecha));
        $ffin = date('Y-m-d', $ffin);
        $array_fechas = array('');
        while ($fecha <= $ffin) {
            $array_fechas[] = date_eng2esp_1($fecha);
            $fecha = strtotime('+1 day', strtotime($fecha));
            $fecha = date('Y-m-d', $fecha);
        }
        $valor_fecha = date_eng2esp_1(set_value('fecha', exist_obj($info_cita, 'fecha_cita')));
        $data['drop_fecha'] = form_dropdown_fechas('fecha_cita', $array_fechas, $valor_fecha, 'class="form-control" id="fecha_cita"');
        $clientes = procesarResponseApiJsonToArray($this->curl->curlGet('api/clientes'));
        $array_clientes = [];
        foreach ($clientes as $c => $cliente) {
            $info_user = new stdClass();
            $info_user->id = $cliente->id;
            $info_user->nombre = $cliente->nombre . ' ' . $cliente->apellido_paterno . ' ' . $cliente->apellido_materno;

            $array_clientes[$c] = $info_user;
        }
        $data['id_cliente'] = form_dropdown('id_cliente', array_combos($array_clientes, 'id', 'nombre', TRUE), set_value('id_cliente', exist_obj($info, 'id_cliente')), 'class="form-control busqueda" id="id_cliente"');
        $data['hora_cita'] = form_dropdown('hora_cita', array(), set_value('hora_cita', exist_obj($info, 'hora_cita')), 'class="form-control" id="hora_cita"');
        
        $estatus = procesarResponseApiJsonToArray($this->curl->curlGet('api/logistica/catalogos/estatus-danios-bodyshop'));
        $data['id_status'] = form_dropdown('id_status', array_combos($estatus, 'id', 'estatus', TRUE), set_value('id_status', exist_obj($info, 'id_status')), 'class="form-control busqueda" id="id_status"');

        $data['fecha_inicio'] = form_input('fecha_inicio', set_value('fecha_inicio', exist_obj($info, 'fecha_inicio')), 'class="form-control" id="fecha_inicio"', 'date');
        $data['fecha_fin'] = form_input('fecha_fin', set_value('fecha_fin', exist_obj($info, 'fecha_fin')), 'class="form-control" id="fecha_fin"', 'date');

        $tipos_golpes = ['A'=>'A','B'=>'B','C'=>'C','D'=>'D'];
        $data['tipo_golpe'] = form_dropdown('tipo_golpe',$tipos_golpes,'', 'class="form-control busqueda" id="tipo_golpe"');
        
        $data['comentarios'] = form_textarea('comentarios', set_value('comentarios', exist_obj($info, 'comentarios')), 'class="form-control" id="comentarios"');

        $data['id'] = $id;
        $data['titulo'] = 'Bodyshop';
        $this->blade->render('agregar', $data);
    }
    public function getTiempo()
    {
        $fecha = date2sql($this->input->post('fecha'));
        $horarios_ocupados = procesarResponseApiJsonToArray($this->curl->curlPost('api/bodyshop/master/horarios-ocupados', [
            "id_asesor" => $_POST['id_asesor'],
            'fecha_cita' => $fecha
        ]));
        $tiempo_actual = date('H:i');
        $tiempo = '09:00';
        while ($tiempo <= '20:00') {
            $NuevoTiempo = strtotime('+1 hour', strtotime($tiempo));
            $tiempo = date('H:i:s', $NuevoTiempo);
            if (!in_array($tiempo, $horarios_ocupados)) {
                if ($fecha == date('Y-m-d')) {
                    if ($tiempo >= $tiempo_actual) {
                        $array_tiempo[] = $tiempo;
                    }
                } else {
                    $array_tiempo[] = $tiempo;
                }
            }
        }
        echo json_encode($array_tiempo);
        exit();
    }
}
