<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Administrador extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
    $this->load->library('curl');
    $this->load->helper('general');
  }

  public function permisos()
  {
    $data['modulo'] = "Administración del DMS";
    $data['subtitulo'] = "Permisos";
    $data['submodulo'] = "Catálogo";
    $roles = $this->curl->curlGet('api/roles');
    $dataRoles = procesarResponseApiJsonToArray($roles);
    $data['roles'] = isset($dataRoles) ? $dataRoles : [];

    $modulos = $this->curl->curlGet('api/menu/modulos');
    $dataModulos = procesarResponseApiJsonToArray($modulos);
    $data['modulos'] = isset($dataModulos) ? $dataModulos : [];
    $this->blade->render('sistema/permisos', $data);
  }

  public function modulos()
  {
    $data['modulo'] = "Administración del DMS";
    $data['subtitulo'] = "Módulos";
    $data['submodulo'] = "Catálogo";
    $this->blade->render('sistema/modulos', $data);
  }

  public function roles()
  {
    $data['modulo'] = "Administración del DMS";
    $data['subtitulo'] = "Roles";
    $data['submodulo'] = "Catálogo";
    $this->blade->render('sistema/roles', $data);
  }

  public function secciones()
  {
    $data['modulo'] = "Administración del DMS";
    $data['subtitulo'] = "Secciones";
    $data['submodulo'] = "Catálogo";
    $this->blade->render('sistema/secciones', $data);
  }

  public function submenu()
  {
    $data['modulo'] = "Administración del DMS";
    $data['subtitulo'] = "Sub menú";
    $data['submodulo'] = "Catálogo";
    $this->blade->render('sistema/submenu', $data);
  }

  public function vistas()
  {
    $data['modulo'] = "Administración del DMS";
    $data['subtitulo'] = "Vistas";
    $data['submodulo'] = "Catálogo";
    $this->blade->render('sistema/vista', $data);
  }

}
