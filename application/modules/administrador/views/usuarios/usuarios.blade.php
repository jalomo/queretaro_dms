@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    <div class="row mb-4">
        <div class="col-md-12 text-right">
            <button type="button" class="btn btn-primary" onclick="app.openModal(usuarios.callbackResponseNuevo)">
                <i class="fa fa-plus"></i> Agregar
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered" id="tabla-usuarios" width="100%" cellspacing="0">
            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-usuarios" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo isset($subtitulo) ? $subtitulo : "" ?></h5>
            </div>
            <div class="p-4">
                <form id="form-modulo" class="row">
                    <div class="col-md-6">
                        <?php echo renderInputText("text", "nombre", "Nombre", ''); ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo renderInputText("text", "apellido_paterno", "Apellido paterno", ''); ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo renderInputText("text", "apellido_materno", "Apellido materno", ''); ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo renderInputText("text", "usuario", "Usuario", ''); ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo renderInputText("password", "password", "Contraseña", ''); ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo renderInputText("text", "email", "Correo electrónico", ''); ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo renderInputText("text", "telefono", "Teléfono", ''); ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo renderInputText("text", "rfc", "RFC", ''); ?>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <lable>Rol</label>
                                <select name="rol_id" id="rol_id" class="form-control">
                                    <option value=""></option>
                                </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <?php echo renderInputText("text", "clave_vendedor", "Clave vendedor", ''); ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo renderInputText("text", "clave_stars", "Clave STARS", ''); ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn-agregar" onclick="app.insert()" style="display:none;" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
                <button type="button" id="btn-editar" onclick="app.update()" style="display:none;" class="btn btn-primary"><i class="fa fa-edit"></i> Editar</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-password" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Cambiar contraseña</h5>
            </div>
            <div class="p-4">
                <form id="form-cambiar-password" class="row">
                    <div class="col-md-12">
                        <?php echo renderInputText("text", "password", "Contraseña", ''); ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn-agregar" onclick="usuarios.cambiar_password(this)" class="btn btn-primary"><i class="fa fa-save"></i> Cambiar contraseña</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ base_url('js/administrador/sistema/crud_javascript.js') }}"></script>
<script src="{{ base_url('js/administrador/sistema/usuarios.js') }}"></script>
@endsection