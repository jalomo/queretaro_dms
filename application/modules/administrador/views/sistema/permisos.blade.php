@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    <h3>Filtros búsqueda</h3>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Módulos:</label>
                <select class="form-control" id="modulo_id" name="modulo_id" style="width: 100%;">
                    @if(!empty($modulos))
                    <option value=""></option>
                    @foreach ($modulos as $modulo)
                    <option value="{{ $modulo->id}}"> {{$modulo->nombre}}</option>
                    @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="col-md-2 mt-4">
            <button type="button" onclick="permisos.filtrar()" class="btn btn-primary mt-1"><i class="fa fa-search"></i> Buscar</button>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Rol:</label>
                <select class="form-control" id="rol_id" name="rol_id" style="width: 100%;">
                    @if(!empty($roles))
                    @foreach ($roles as $rol)
                    <option value="{{ $rol->id}}"> {{$rol->rol}}</option>
                    @endforeach
                    @endif
                </select>
                <div id="rol_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered" id="tbl_permisos_rol" width="100%" cellspacing="0"></table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ base_url('js/administrador/sistema/crud_javascript.js') }}"></script>
<script src="{{ base_url('js/administrador/sistema/permisos.js') }}"></script>
<script type="text/javascript">
    $("#rol_id").on('change', function() {
        permisos.filtrar();
    });
</script>
@endsection