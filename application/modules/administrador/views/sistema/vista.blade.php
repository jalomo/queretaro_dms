@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    <div class="row mb-4">
        <div class="col-md-12 text-right">
            <button type="button" class="btn btn-primary" onclick="app.openModal(submenu.callbackResponseNuevo)">
                <i class="fa fa-plus"></i> Agregar
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered" id="tabla-administrador" width="100%" cellspacing="0">
            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-administrador" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo isset($subtitulo) ? $subtitulo : "" ?></h5>
            </div>
            <div class="p-4">
                <form id="form-modulo" class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <lable>Sub Menú</label>
                                <select name="submenu_id" id="submenu_id" class="form-control">
                                    <option value=""></option>
                                </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <?php echo renderInputText("text", "nombre", "Nombre vista", ''); ?>
                    </div>
                    <div class="col-md-12">
                        <?php echo renderInputText("text", "modulo", "Módulo", ''); ?>
                    </div>
                    <div class="col-md-12">
                        <?php echo renderInputText("text", "controlador", "Controlador", ''); ?>
                    </div>
                    <div class="col-md-12">
                        <?php echo renderInputText("text", "link", "Función", ''); ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn-agregar" onclick="app.insert()" style="display:none;" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
                <button type="button" id="btn-editar" onclick="app.update()" style="display:none;" class="btn btn-primary"><i class="fa fa-edit"></i> Editar</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ base_url('js/administrador/sistema/crud_javascript.js') }}"></script>
<script src="{{ base_url('js/administrador/sistema/vista.js') }}"></script>
@endsection