<div style="overflow-y: scroll;height: 400px;" class="row">
	<div class="col-sm-12">
		<table class="table table-striped table-bordered" cellspacing="0" width="100%">
			<thead>
				<tr>
                    <th>Fecha</th>
                    <th>Hora</th>
                    <th>Comentario</th>
					<th width="30%">Estatus</th>
                    <th>Usuario</th>
				</tr>
			</thead>
			<tbody>
				@if(count($comentarios)>0)
				@foreach($comentarios as $c => $value)
					<tr>
						<td>{{$value->fecha}}</td>
						<td>{{$value->hora}}</td>
						<td>{{$value->observaciones}}</td>
						<td>{{$value->estatus}}</td>
                        <td>{{$value->nombre_usuario.' '.$value->ap_usuario.' ',$value->am_usuario}}</td>
					</tr>
				@endforeach
				@else
				<tr class="text-center">
					<td colspan="5">Aún no se han registrado comentarios... </td>
				</tr>
				@endif
			</tbody>

		</table>
	</div>
</div>