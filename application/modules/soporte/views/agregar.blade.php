@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <form id="frm">
            <input type="hidden" name="id" id="id" value="{{ $id }}">
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Asunto</label>
                    {{ $asunto }}
                </div>
                <div class="col-sm-4">
                    <label for="">Prioridad</label>
                    {{ $prioridad_id }}
                </div>
                <div class="col-sm-4">
                    <label for="">Rol</label>
                    {{ $rol_id }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Evidencia</label>
                    <input type="file" class="form-control-file" id="evidencia" name="evidencia">
                    <div id="evidencia_error" class="invalid-feedback"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <label for="">Comentario</label>
                    {{ $comentario }}
                </div>
            </div>
            <div class="row">
            </div>
        </form>
        <br>
        <button type="button" id="guardar" class="btn btn-primary pull-right">Guardar</button>
        <br><br>
        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;"></div>
    </div>
    </div>


@endsection
@section('scripts')
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script type="text/javascript">
        $("#guardar").on('click', function(e) {
            let evidencia = $('#evidencia')[0].files[0];
            let paqueteDeDatos = new FormData();
            paqueteDeDatos.append('asunto', $("#asunto").val());
            paqueteDeDatos.append('comentario', $("#comentario").val());
            paqueteDeDatos.append('user_id', "{{ $this->session->userdata('id') }}");
            paqueteDeDatos.append('usuario', "{{ $this->session->userdata('usuario') }}");
            paqueteDeDatos.append('prioridad_id', $("#prioridad_id").val());
            paqueteDeDatos.append('rol_id', $("#rol_id").val());
            paqueteDeDatos.append('evidencia', evidencia);
            // if (!utils.isDefined(evidencia) && !evidencia) {
            //     return toastr.error("Adjuntar evidencia");
            // }
            $("#guardar").prop('disabled', true);
            ajax.postFile(`api/tickets/master/upload`, paqueteDeDatos, function(response, header) {
                if (header.status == 200 || header.status == 204) {
                    utils.displayWarningDialog(
                        "Ticket generado con éxito, será atendido y recibirá respuesta sobre su solicitud",
                        "success",
                        function(
                            data) {
                            return window.location.href = base_url + 'soporte/listado';

                        })
                }
                if (header.status == 500) {
                    let titulo = "Error subiendo evidencia!"
                    utils.displayWarningDialog(titulo, 'warning', function(result) {});
                }
            })
            $("#guardar").prop('disabled', false);
        })
    </script>
@endsection
