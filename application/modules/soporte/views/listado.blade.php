@layout('tema_luna/layout')
@section('contenido')
    <style type="text/css">
        .my_alert-danger {
            color: #a94442;
            background-color: #f2dede;
            border-color: #ebccd1;
            font-weight: bold;
            padding: 5px !important;
        }

        td a i {
            font-size: 25px !important;
        }

        .visualizar {
            visibility: visible;
        }

        .ocultar {
            display: none;
        }

        .abierto {

            background-color: #DE0804
        }

        .proceso {
            background-color: #FA947B
        }

        .cerrado {
            background-color: #c3e6cb
        }

        .white {
            color: white !important;
        }

        .etiqueta {
            padding: 7px;
            border-radius: 10px;
        }

    </style>
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <a href="{{ base_url('soporte/agregar') }}" id="guardar"
            class="btn btn-sm btn-primary pull-right m-t-n-xs"><strong>Agregar ticket</strong></a>
        <br>
        <br>
        <div class="row">
            <div class="col-sm-12">
                <strong class="abierto etiqueta">Abierto</strong>
                <strong class="proceso etiqueta">Proceso</strong>
                <strong class="cerrado etiqueta">Cerrado</strong>
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-sm-12">
                <div id="tabla-items">
                    <table id="tbl" class="table table-bordered" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Folio</th>
                                <th>Fecha creación</th>
                                <th>Asunto</th>
                                <th>Comentario</th>
                                <th>Prioridad</th>
                                <th>Estatus ticket</th>
                                <th>Rol</th>
                                <th>Prioridad</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $clase_erronea = ''; ?>
                            @foreach ($info as $v => $data)
                                <?php
                                $clase_ticket = '';
                                if ($data->estatus_id == 1) {
                                    $clase_ticket = 'abierto';
                                } elseif ($data->estatus_id == 2) {
                                    $clase_ticket = 'proceso';
                                } else {
                                    $clase_ticket = 'cerrado';
                                }
                                ?>
                                <tr>
                                    <td>{{ $data->folio }}</td>
                                    <td>{{ formatDate($data->created_at) }}</td>
                                    <td>{{ $data->asunto }}</td>
                                    <td>{{ $data->comentario }}</td>
                                    <td>{{ $data->prioridad }}</td>
                                    <td>{{ $data->estatus_ticket }}</td>
                                    <td>{{ $data->rol }}</td>
                                    <td class="{{ $clase_ticket }}">{{ $data->estatus_ticket }}</td>
                                    <td>
                                        <a href="{{ base_url('soporte/detalle_ticket/' . $data->folio) }}"
                                            class="js_detalle" data-id="{{ $data->id }}" title="Detalle ticket">
                                            <i class="pe pe-7s-info"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script>
        var API_URL = "{{ API_URL_DEV }}";
        inicializar_tabla_local();
        var site_url = "{{ site_url() }}";
        var cliente = '';
        var aPos = '';
        $("body").on("click", '.js_add_historial', function(e) {
            e.preventDefault();
            aPos = $(this);
            var url = site_url + "/proactivo/agregar_historial/0";
            customModal(url, {
                    "id": $(this).data('id'),
                    "estatus_id": $(this).data('estatus_id'),
                }, "GET", "lg", saveHistorial, "", "Guardar", "Cancelar", "Ingresar comentario",
                "modalComentario");
        });

        $("body").on("click", '.js_historial', function(e) {
            e.preventDefault();
            id = $(this).data('id');
            var url = site_url + "/proactivo/historial_comentarios/";
            customModal(url, {
                "id": id
            }, "POST", "lg", "", "", "", "Cerrar", "Historial de comentarios", "modalHistorialComentarios");
        });

        function saveHistorial() {
            const data = {
                observaciones: $("#observaciones").val(),
                estatus_id: $("#estatus_id").val(),
                fecha: $("#fecha").val(),
                hora: $("#hora").val(),
                user_id: "{{ $this->session->userdata('id') }}",
                cp_id: $("#cp_id").val(),
            }
            if (data.fecha == '' || data.hora == '') {
                return toastr.error(
                    "Es necesario ingresar todos los datos");
            }
            var url = site_url + "/proactivo/validarHoras";
            ajaxJson(url, {
                fecha: $("#fecha").val(),
                hora: $("#hora").val(),
            }, "POST", "", function(result) {
                if (result == -1) {
                    return toastr.error(
                        "Solamente se pueden programar notificaciones en un horario de 09:00 a 19:00 hrs");
                } else if (result == -2) {
                    return toastr.error("No se pueden programar notificaciones en fechas anteriores");
                } else {
                    ajax.post('api/historial-contacto-proactivo', data,
                        function(response, headers) {
                            if (headers.status == 400) {
                                return ajax.showValidations(headers);
                            }
                            let newIntentos = parseInt($("#intentos_" + data.cp_id).val()) + 1;
                            $("#lbl_intentos_" + data.cp_id).text(newIntentos);
                            $("#intentos_" + data.cp_id).val(newIntentos)
                            var titulo = (headers.status != 200) ? headers.message :
                                "Comentario guardado con éxito";
                            utils.displayWarningDialog("Comentario guardado con éxito", "success", function(
                                result) {
                                $(".modalComentario").modal('hide')
                            })
                        })
                }

            });

        }
        $("body").on('click', '#buscar', function() {
            buscarInformacion();
        });

        function buscarInformacion() {
            var url = site_url + "/financiamientos/lista_ventas/";
            ajaxLoad(url, {
                "fecha": $("#fecha").val(),
            }, "tabla-items", "POST", function() {
                inicializar_tabla_local();
            });
        }

        function inicializar_tabla_local() {
            $('#tbl').DataTable({
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": "Anterior",
                        "sNext": "Siguiente",
                        "sLast": "Última",
                        "sFirst": "Primera"
                    },
                    "sLengthMenu": '<div id="combo_datatable">Mostrar <select>' +
                        '<option value="5">5</option>' +
                        '<option value="10">10</option>' +
                        '<option value="20">20</option>' +
                        '<option value="30">30</option>' +
                        '<option value="40">40</option>' +
                        '<option value="50">50</option>' +
                        '<option value="-1">Todos</option>' +
                        '</select> registros',
                    "sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)",
                    "sInfoFiltered": " - filtrados de _MAX_ registros",
                    "sInfoEmpty": "No hay resultados de búsqueda",
                    "sZeroRecords": "No hay registros para mostrar...",
                    "sProcessing": "Espere, por favor...",
                    "sSearch": "Buscar:"
                },
                "scrollX": true
            });
        }
    </script>
@endsection
