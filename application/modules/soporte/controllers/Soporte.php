<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Soporte extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'url', 'date', 'general'));
        $this->load->library('curl');
        date_default_timezone_set('America/Mexico_City');
    }
    public function listado()
    {
        $data['info'] = procesarResponseApiJsonToArray($this->curl->curlPost('api/tickets/master/get-all', [
            'user_id' => $this->session->userdata('id')
        ]));
        $data['titulo'] = "Mis tickets";
        $this->blade->render('listado', $data);
    }
    
    public function agregar($id = 0)
    {
        if ($id == 0) {
            $info = new Stdclass();
        } else {
            $info = procesarResponseApiJsonToArray($this->curl->curlGet('api/unidades/' . $id));
            $info = $info[0];
        }
        
        $estatus_prioridades = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-prioridades'));
        $data['prioridad_id'] = form_dropdown('prioridad_id', array_combos($estatus_prioridades, 'id', 'prioridad', TRUE), set_value('prioridad_id', exist_obj($info, 'prioridad_id')), 'class="form-control busqueda" id="prioridad_id"');
        $roles = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-roles-soporte'));
        $data['rol_id'] = form_dropdown('rol_id', array_combos($roles, 'id', 'rol', TRUE), set_value('rol_id', exist_obj($info, 'rol_id')), 'class="form-control busqueda" id="rol_id"');
        $data['asunto'] = form_input('asunto',set_value('asunto', exist_obj($info, 'asunto')), 'class="form-control" maxlength="200" id="asunto"');
        $data['comentario'] = form_textarea('comentario',set_value('comentario', exist_obj($info, 'comentario')), 'class="form-control" id="comentario"');
        $data['id'] = $id;
        $data['titulo'] = 'Crear nuevo Ticket';
        $this->blade->render('agregar', $data);
    }
    public function detalle_ticket($folio){

        $data['info'] = procesarResponseApiJsonToArray($this->curl->curlPost('api/tickets/master/get-all',[
            'folio' => $folio
        ]));
        $usuario = ucfirst($this->session->userdata('nombre')).' '.ucfirst($this->session->userdata('apellido_paterno')).' '.ucfirst($this->session->userdata('apellido_materno'));
        $url_historial_ticket ='api/tickets/historial/get-all-historial/'.$data['info'][0]->id;
        $url_evidencia_ticket ='api/evidencia-tickets/master/evidencia-ticket/'.$data['info'][0]->id;
        $data['historial'] = procesarResponseApiJsonToArray($this->curl->curlGet($url_historial_ticket));
        $data['evidencia'] = procesarResponseApiJsonToArray($this->curl->curlGet($url_evidencia_ticket));
        $estatus_tickets = procesarResponseApiJsonToArray($this->curl->curlGet('api/estatus-tickets'));
        $data['estatus_tickets'] = form_dropdown('estatus_id', array_combos($estatus_tickets, 'id', 'estatus', TRUE),isset($data['info']) ? $data['info'][0]->estatus_id : '', 'class="form-control busqueda" id="estatus_id"');
        $data['usuario'] = form_input('usuario',$usuario, 'class="form-control" id="usuario" readonly');
        $data['titulo'] = 'Detalle ticket #'.$folio;
        $data['ticket_id'] = $data['info'][0]->id;
        $data['folio'] = $folio;
        $data['usuario_actual'] = $usuario;
        $this->blade->render('detalle_ticket', $data);
    }
    public function agregar_evidencia(){
        $data['ticket'] = $_POST['ticket'];
        $this->blade->render('v_agregar_evidencia_ticket', $data);
    }
}
