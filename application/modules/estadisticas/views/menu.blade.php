<ul class="nav nav-tabs mb-2">
    <li class="nav-item">
        <a href="{{ base_url('/estadisticas/ventas') }}" class="nav-link @if(isset($ventas)) active text-white bg-primary @endif">
            <i class="fa fa-cart-plus"></i>
            Venta
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ base_url('/estadisticas/devoluciones') }}" class="nav-link @if(isset($devolucion)) active text-white bg-primary  @endif">
            <i class="fa fa-cart-arrow-down"></i>
            Devolución
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ base_url('/estadisticas/pedidos') }}" class="nav-link @if(isset($entradas)) active text-white bg-primary  @endif">
            <i class="fa fa-cart-arrow-down"></i>
            Pedido Compra
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ base_url('/estadisticas/traspasos') }}" class="nav-link @if(isset($traspasos)) active text-white bg-primary  @endif">
            <i class="fa fa-cart-arrow-down"></i>
            Traspasos
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ base_url('/estadisticas/ereact') }}" class="nav-link @if(isset($ereact)) active text-white bg-primary  @endif">
            <i class="fa fa-cart-arrow-down"></i>
            Ereact
        </a>
    </li>
</ul>