@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
        <div>
            <h1 class="font-weight-bold">
                <i class="fa fa-chart-line"></i>
                Estadisticas
            </h1>
        </div>
        @include('menu', ['ereact' => true ])
        <form class="border rounded p-3 mb-3">
            <legend>Filtros</legend>
            <div class="form-row">
                <div class="form-group col">
                    <label for="">Fecha Inicial Reporte: </label>
                    <input type="date" class="form-control" id="fecha_inicio" value="{{ date('Y-m-d') }}">
                </div>
                <div class="form-group col">
                    <label for="">Fecha Termino Reporte: </label>
                    <input type="date" class="form-control" id="fecha_termino" value="{{ date('Y-m-d') }}">
                </div>
            </div>
            <div class="form-row">
                <button id="download" class="btn btn-primary ml-auto">
                    <i class="fa fa-download"></i>
                    Descargar
                </button>
            </div>
        </form>

        <table id="reporte" class="table table-striped mt-4">
            <thead>
                <th>#</th>
                <th>Fecha de Transmisión</th>
                <th>Nombre de Archivo</th>
                <th>Completo</th>
                <th>Filtros</th>
            </thead>
        </table>
    </div>
@endsection

@section('scripts')
    <script>
        let historial = []

        $(document).ready(function(){
            getHistorial()
        })

        const getHistorial = () => {
            event.preventDefault()

            let search = {
                // fecha_inicio: $('#fecha_inicio').val(),
                fecha_termino: $('#fecha_termino').val(),
                tipo: $('#tipo').val(),
                producto: $('#producto').val(),
            }

            $.getJSON(`${PATH_API}api/estadisticas/ereact`, search)
            .then(response => {
                historial = response

                reporte.clear()
                reporte.rows.add(historial).draw()
            })
            .fail((error) => {
                if(error.status == 422){                    
                    toastr.error(error.responseJSON.errors.fecha_termino)
                }
            })
        }

        var reporte = $('#reporte').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            data: historial,
            columns: [
                { data: 'id' },
                { data: 'fecha_transmision' },
                { data: 'nombre_archivo' },
                { data: 'completo' },
                { data: 'filters' },
            ]
        })

        $('#download').click(function(event){

            event.preventDefault()

            let search = {
                fecha_inicio: $('#fecha_inicio').val(),
                fecha_termino: $('#fecha_termino').val(),
            }

            $.isLoading({ text: "Descargando Reporte." });

            $.ajax({
                url: `${PATH_API}api/estadisticas/ereact/descargar`,
                type: 'GET',
                data: search,    
                // xhrFields: { responseType: 'arraybuffer'},
                xhr: () => {
                    let xhr = new XMLHttpRequest()
                    xhr.onreadystatechange = () => {
                        if(xhr.readyState == 2){
                            xhr.responseType = "arraybuffer"
                        } else {
                            xhr.responseType = "json"
                        }
                    }

                    return xhr
                }
            })
            .done((response, status, xhr) => {
                let disposition = xhr.getResponseHeader("Content-Disposition")
                let filename = 'reportes.txt'
                let type = xhr.getResponseHeader("Content-Type")

                let blobFile = new Blob([response], { type: type })
                let URL = window.URL || window.webkitURL
                let downloadURL = URL.createObjectURL(blobFile)

                let link = document.createElement('a')

                link.href = downloadURL

                link.download = filename

                const clickHandler = () => {
                    setTimeout(() => {
                        URL.revokeObjectURL(downloadURL)
                        this.removeEventListener('click', clickHandler)
                    }, 150);
                }

                link.addEventListener('click', clickHandler, false)

                document.body.appendChild(link)

                link.click()

                return link;
                      
            })
            .fail((error) => {
                toastr.error("Ocurrio un error al descargar el reporte.")
            })
            .always(() => {
                $.isLoading('hide');
            })
        })
    </script>
@endsection