@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
        <div>
            <h1 class="font-weight-bold">
                <i class="fa fa-chart-line"></i>
                Estadisticas
            </h1>
        </div>
        @include('menu', ['devolucion' => true ])
        <form class="border rounded p-3 mb-3">
            <legend>Filtros</legend>
            <div class="form-row">
                <div class="form-group col">
                    <label for="">No. Identificación o No. Pieza: </label>
                    <input type="text" class="form-control" id="producto">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col">
                    <label for="">Fecha Inicial Reporte: </label>
                    <input type="date" class="form-control" id="fecha_inicio">
                </div>
                <div class="form-group col">
                    <label for="">Fecha Termino Reporte: </label>
                    <input type="date" class="form-control" id="fecha_termino">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col">
                    <label for="">Tipo: </label>
                    <select id="tipo" class="form-control">
                        <option value="">Seleccionar</option>
                        {{-- <option value="dia">Por Día</option> --}}
                        <option value="orden">Por Orden</option>
                        {{-- <option value="producto">Por Producto</option> --}}
                    </select>
                </div>
            </div>
            <div class="form-row">
                <button id="download" class="btn btn-primary ml-auto">
                    <i class="fa fa-download"></i>
                    Descargar
                </button>
                <button id="search" class="btn btn-primary ml-2">
                    <i class="fa fa-search"></i>
                    Buscar
                </button>
            </div>
        </form>

        <table id="devolucion" class="table table-striped mt-4">
            <thead>
                <th>Numero Orden o Folio</th>
                <th>Cliente</th>
                <th>No. Identificación</th>
                <th>Descripción</th>
                <th>Cantidad</th>
                <th>Tipo Venta</th>
                <th>Valor Unitario</th>
                <th>IVA</th>
                <th>Total</th>
            </thead>
        </table>
    </div>
@endsection

@section('scripts')
    <script>
        let productos = []

        $('#search').click(function(event){
            event.preventDefault()

            let search = {
                fecha_inicio: $('#fecha_inicio').val(),
                fecha_termino: $('#fecha_termino').val(),
                tipo: $('#tipo').val(),
                producto: $('#producto').val(),
            }

            $.getJSON(`${PATH_API}api/estadisticas/devoluciones/ventas`, search)
            .then(response => {
                productos = response

                devolucion.clear()
                devolucion.rows.add(productos).draw()
            })
            .fail((error) => {
                if(error.status == 422){                    
                    toastr.error(error.responseJSON.errors.fecha_termino)
                }
            })
        })

        var devolucion = $('#devolucion').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            data: productos,
            columns: [
                { data: function(data){ 
                    let fecha = (new Date(data.created_at)).toLocaleString('ES-MX', {
                        dateStyle: 'medium',
                    })

                    return `${data.folio}<br> ${fecha}`
                }},
                { data: 'cliente' },
                { data: 'no_identificacion' },
                { data: 'descripcion' },
                { data: 'cantidad' },
                { data: 'tipo_venta' },
                { 
                    data: function(data){
                        return parseFloat(data.valor_unitario).toFixed(2)
                    }
                },
                { 
                    data: function(data){
                        return parseFloat(data.iva).toFixed(2)
                    }
                },
                { 
                    data: function(data){
                        return parseFloat(data.total).toFixed(2)
                    }
                },
            ]
        })

        $('#download').click(function(event){

            event.preventDefault()

            let search = {
                fecha_inicio: $('#fecha_inicio').val(),
                fecha_termino: $('#fecha_termino').val(),
                tipo: $('#tipo').val(),
                producto: $('#producto').val(),
            }

            $.isLoading({ text: "Descargando Reporte." });

            $.ajax({
                url: `${PATH_API}api/estadisticas/devoluciones/ventas/descargar`,
                type: 'GET',
                data: search,    
                // xhrFields: { responseType: 'arraybuffer'},
                xhr: () => {
                    let xhr = new XMLHttpRequest()
                    xhr.onreadystatechange = () => {
                        if(xhr.readyState == 2){
                            xhr.responseType = "arraybuffer"
                        } else {
                            xhr.responseType = "json"
                        }
                    }

                    return xhr
                }
            })
            .done((response, status, xhr) => {
                let disposition = xhr.getResponseHeader("Content-Disposition")
                let filename = 'poliza-cargamento.pdf'
                let type = xhr.getResponseHeader("Content-Type")

                let blobFile = new Blob([response], { type: type })
                let URL = window.URL || window.webkitURL
                let downloadURL = URL.createObjectURL(blobFile)

                if(typeof window.navigator.msSaveBlob !== 'undefined'){
                    window.navigator.msSaveBlob(blobFile, filename)

                    let link = document.createElement('a')
                    link.href = downloadURL
                    link.download = filename
                    document.body.appendChild(link)
                    link.click()
                    return ;
                }            

                window.open(downloadURL)
            })
            .fail((error) => {
                toastr.error("Ocurrio un error al descargar el reporte.")
            })
            .always(() => {
                $.isLoading('hide');
            })
        })
    </script>
@endsection