<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') or define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  or define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') or define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   or define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  or define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           or define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     or define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       or define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  or define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   or define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              or define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            or define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       or define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        or define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          or define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         or define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   or define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  or define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') or define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     or define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       or define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      or define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      or define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

if ($_SERVER['HTTP_HOST'] == 'localhost') {
    defined('NOMINA_API') or define('NOMINA_API', 'http://localhost/sistema_nomina/');
    defined('NOMINA_UPLOADS') or define('NOMINA_UPLOADS', 'http://localhost/sistema_nomina/writable/uploads/');
} else {
    defined('NOMINA_API') or define('NOMINA_API', 'https://sohex.net/nomina_dms/');
    defined('NOMINA_UPLOADS') or define('NOMINA_UPLOADS', 'https://sohex.net/nomina_dms/writable/uploads/');
}

defined('SUCURSAL_DMS') or define('SUCURSAL_DMS', getenv('SUCURSAL_DMS'));
defined('NOMBRE_SUCURSAL') or define('NOMBRE_SUCURSAL', getenv('NOMBRE_SUCURSAL'));
defined('RFC_RECEPTOR') or define('RFC_RECEPTOR', getenv('RFC_RECEPTOR'));
defined('BID_RECEPTOR') or define('BID_RECEPTOR', getenv('BID_RECEPTOR'));


defined('URL_DOMINIO') or define('URL_DOMINIO', getenv('URL_DOMINIO'));
defined('API_URL_DEV') or define('API_URL_DEV', getenv('URL_DOMINIO') . getenv('API_URL_DEV'));
defined('API_URL_BACKEND') or define('API_URL_BACKEND', getenv('API_URL_BACKEND'));
defined('API_URL_SANJUAN') or define('API_URL_SANJUAN', getenv('URL_DOMINIO') . getenv('API_URL_SANJUAN'));
defined('API_CONTABILIDAD') or define('API_CONTABILIDAD', getenv('URL_DOMINIO') . getenv('API_CONTABILIDAD'));
// defined('API_CONTABILIDAD') or define('API_CONTABILIDAD', 'http://localhost/contabilidad_queretaro');

defined('SERVICIOS_PLAN_MYLSA') or define('SERVICIOS_PLAN_MYLSA', getenv('URL_DOMINIO') . getenv('SERVICIOS_PLAN_MYLSA'));
defined('SERVICIOS_ORDENES_ABIERTAS') or define('SERVICIOS_ORDENES_ABIERTAS',  getenv('URL_DOMINIO') . getenv('SERVICIOS_ORDENES_ABIERTAS'));
defined('SERVICIOS_CITAS') or define('SERVICIOS_CITAS', getenv('URL_DOMINIO') . getenv('SERVICIOS_CITAS'));
defined('SERVICIOS_CLIENTES') or define('SERVICIOS_CLIENTES', getenv('URL_DOMINIO') . getenv('SERVICIOS_CLIENTES'));
defined('SERVICIOS_VENTANILLA') or define('SERVICIOS_VENTANILLA',  getenv('URL_DOMINIO') . getenv('SERVICIOS_VENTANILLA'));
defined('SERVICIOS_LOGIN') or define('SERVICIOS_LOGIN', getenv('URL_DOMINIO') . getenv('SERVICIOS_LOGIN'));
