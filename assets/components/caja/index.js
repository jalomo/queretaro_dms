const Caja = {
    template: `
        <div>
            <div class="custom-file mb-2">
                <input type="file" class="custom-file-input" @change="change" />
                <label class="custom-file-label">
                    <i class="fa fa-upload"></i>
                </label>
            </div>
            <div v-if="errors.archivo" class="small text-danger mb-2">
                <div :key="index" v-for="(message, index) in errors.archivo">
                    {{ message }}
                </div>
            </div>
        </div>
    `,
    data() {
        return {
            file: null,
            errors: {},
            url: 'api/cajas/cliente-cuentas',
        }
    },
    computed: {
        formdata() {
            let formdata = new FormData
            formdata.set('archivo', this.file, `${this.file.name}`)

            return formdata
        },
        config() {
            return {
                method: 'POST',
                data: this.formdata,
                url: API_URL + this.url,
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
            }
        },
    },
    methods: {
        change(event) {
            let files = event.target.files || e.dataTransfer.files

            if (! files.length)
                return

            this.file = files[0]

            this.upload()
        },
        upload() {
            toastr.success('Cargando cuentas clientes...')
            $.isLoading('body', {
                text: "Cargando cuentas clientes..."
            });

            axios(this.config)
                .then(response => {
                    
                    this.is_uploaded = true
                    
                    this.file = null
                    this.$emit('uploaded')
    
                    toastr.success(response.data.message)
                    $.isLoading("hide")
                })
                .catch(error => {
    
                    toastr.error('Ocurrio un error al subir el archivo.')
                   
                    this.is_uploaded = false
                    this.file = null
                    $.isLoading("hide");
                })
        },
    },
}