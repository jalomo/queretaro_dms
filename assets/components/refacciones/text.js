const Text = {
    template: `
        <div>
            <div class="custom-file mb-2">
                <input type="file" class="custom-file-input" @change="change" />
                <label class="custom-file-label">
                    <i class="fa fa-upload"></i>
                </label>
            </div>
            <div v-show="progress > 0" class="progress">
                <div class="progress-bar progress-bar-striped bg-success text-center text-white" :style="'width:'+ progress + '%'">
                    {{ progress }}%
                </div>
            </div>
            <div v-if="errors.archivo" class="small text-danger mb-2">
                <div :key="index" v-for="(message, index) in errors.archivo">
                    {{ message }}
                </div>
            </div>
            <table-stock></table-stock>
        </div>
    `,
    watch: {
        chunks(n, o) {
            if (n.length > 0) {
                this.upload()
            }
        },
    },
    data() {
        return {
            file: null,
            chunks: [],
            uploaded: 0,
            uniqid: null,
            size: 10 * 1024 * 1024,
            progress: 0,
            errors: {},
            url: 'api/stock/text',
        }
    },
    computed: {
        formdata() {
            let formdata = new FormData
            formdata.set('last', this.chunks.length == 1 ? 1 : 0)
            formdata.set('archivo', this.chunks[0], `${this.file.name}`)

            if (this.uniqid) {
                formdata.set('filename', this.uniqid)
            }
            return formdata
        },
        config() {
            return {
                method: 'POST',
                data: this.formdata,
                url: API_URL + this.url,
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                onUploadProgress: event => {
                    this.progress = Math.round((this.uploaded + event.loaded) / this.file.size * 100)

                    if (event.loaded == event.total)
                        this.uploaded += event.total
                }
            }
        },
    },
    methods: {
        change(event) {
            let files = event.target.files || e.dataTransfer.files

            if (! files.length)
                return

            this.file = files[0]

            this.createChunks()
        },
        upload() {
            axios(this.config)
                .then(response => {
                    this.uniqid = response.data.uuid

                    this.chunks.shift(this.chunks[this.chunks.length - 1].size)

                    if (this.chunks.length < 1) {
                        this.uploaded = 0
                        this.is_uploaded = true
                        this.progress = 0

                        this.file = null
                        this.$emit('uploaded')

                        toastr.success(response.data.message)
                    }

                    if(response.data.errors)
                        throw response.data.errors 
                })
                .catch(error => {

                    toastr.error('Ocurrio un error al subir el archivo.')
                   
                    this.uploaded = 0
                    this.progress = 0
                    this.is_uploaded = false
                    this.chunks = []
                    this.file = null
                })
        },
        createChunks() {
            let size = this.size, chunks = Math.ceil(this.file.size / size)

            for (let i = 0; i < chunks; i++) {
                this.chunks.push(this.file.slice(
                    i * size, Math.min(i * size + size, this.file.size), this.file.type
                ))
            }
        },
    }
}