const TableStock = {
    template: `
            <table class="table table-striped">
                <thead>
                    <th>
                        <button @click="getJobs" class="btn btn-primary">
                            <i class="fa fa-sync"></i>
                        </button>
                    </th>
                    <th>Fecha de Actualización</th>
                    <th>Usuario</th>
                    <th>Archivo</th>
                    <th>Estatus</th>
                </thead>
                <tbody>
                    <tr :key="index" v-for="(job, index) in jobs">
                        <td>{{ index + 1}}</td>
                        <td>{{ moment(job.created_at).format('D MMM YYYY, h:mm:ss a') }}</td>
                        <td>{{ job.nombre_usuario || 'Anonimo' }} {{ job.apellido_paterno }} {{ job.apellido_materno }}</td>
                        <td>{{ job.nombre }}</td>
                        <td>{{ job.estatus }}</td>
                    </tr>
                    <tr v-if="! jobs.length" class="text-center" colspan="4">
                        Cargando...
                    </tr>
                </tbody>
            </table>
    `,
    props:{
        update: false,
    },
    data(){
        return {
            jobs: []
        }
    },
    created(){
        this.getJobs()
    },
    watch:{
        update(n){
            if(n == true){
                this.getJobs()
            }
        }
    },
    methods:{
        getJobs(){
            axios.get(API_URL + 'api/stock')
            .then(response => {
                this.jobs = response.data
                this.$emit('updated')
            })
        },
        descargar(){

        }
    }
}